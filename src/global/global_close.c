//��� �� ��� �������� �� �������� ����� � ���

//���������� �������
#include <unistd.h>

//������������ �������
#include <axis_error.h>
#include <axis_keyboard.h>
#include <dispatcher_lib/dispatcher_lib_resource.h>
#include <dispatcher_lib/monitoring.h>

//��������� �������
#include <adapter/global.h>
#include <adapter/network.h>
#include <adapter/httpd.h>
#include <adapter/module.h>
#include <adapter/dmdevice.h>
#include <adapter/recorder.h>
#include <adapter/interface.h>
#include <adapter/copy_data.h>
#include <adapter/log.h>
#include <adapter/system.h>
#include <adapter/debug.h>

#ifdef TMS320DM365
#include <adapter/nand.h>
#endif

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
//#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>

#else
#define DBGLOG( ... )
#endif


//������� ����������� ��������
//����:
// Status - ��������� �� R2AdapterGlobalStatusStruct_t ���������
//      ������� � ������� ��� ����� ������������
//�������:
// 0 - ���������� ���� ���������� �������
// ����� 0 � ������ ������
int AdapterGlobalClose( AdapterGlobalStatusStruct_t *Status )
{

	int ret = AXIS_NO_ERROR;

	DBGLOG( DISPATCHER_LIB_DEBUG__START );

// 	ret = DispatcherLibMonitoringStop( &Status->Monitoring );

	
	if ( ret < 0 )
	{
		DBGERR( "%s: Can't stop monitoring thread\n", __func__ );
	}

	LOG("Closing Adapter\r\n");

	LOG_EVENTS( &Status->ExchangeEvents.Events, 1, 0 );

	
	if( AdapterHttpdDeinit( Status ))
	{
		DBGERR("%s:It wasn't possible to deinit HTTPD Server\n",__func__);
		return -1;
	}

	if ( Status->GlobalConsole >= 0 )
	{
		ret = adapter_keyboard_close( Status->GlobalConsole );
		Status->GlobalConsole = -1;

		if ( ret < 0 )
		{
			DBGERR( "%s: Can't close console -%x\n", __func__, -ret );
		}
	}
	//TODO!!! Something bad in AdapterAllNetworksStop, after this we can't setattr for keyboard
	if(AdapterAllNetworksStop( Status) < 0)
	{
		DBGERR("%s:It wasn't possible to stop network\n",__func__);
	}
	
	if(AdapterCopyDataStop( Status) < 0)
	{
		DBGERR("%s:It wasn't possible to stop copy data\n",__func__);
	}
	if(AdapterRecorderDeInit(&Status->GlobalDevice_t,&Status->Recorder))
	{
		DBGERR("%s:It wasn't possible to deinit recorder\n",__func__);
		return -1;
	}
	
	if(AdapterRemoveDevicesModules(&Status->GlobalDevice_t))
	{
		DBGERR("%s:It wasn't possible o remove modules\n",__func__);
	}
	
	if(AdapterDmDeviceDeInit(&Status->GlobalDevice_t))
	{
		DBGERR("%s:It wasn't possible to deinit device\n",__func__);
	}

	if(AdapterInterfaceDeInit(&Status->GlobalDevice_t))
	{
		DBGERR("%s:It wasn't possible to deinit interface\n",__func__);
	}
	
	AdapterLogDeInit( Status );

	

	DBGLOG( DISPATCHER_LIB_DEBUG__DONE );

	return ret;

}


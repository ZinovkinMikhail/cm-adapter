//��� ����� �� ������� ������� �������� �� ����������
//��������� ������� ����� ��������

//���������� �������
#include <signal.h>

//������������ �������
#include <axis_error.h>
#include <axis_find_from_file.h>
#include <axis_dmlibs_resource.h>

#include <dispatcher_lib/dispatcher_lib_resource.h>
#include <dispatcher_lib/printf.h>
#include <dispatcher_lib/monitoring.h>
#include <dispatcher_lib/system.h>
#include <dispatcher_lib/printf.h>

#include <dmxxx_lib/dmxxx_calls.h>

//��������� �������
#include <adapter/global.h>
#include <adapter/resource.h>
#include <adapter/dispatcher.h>
#include <adapter/httpd.h>
#include <adapter/network.h>
#include <adapter/system.h>
#include <adapter/debug.h>
#include <adapter/settings.h>
#include <adapter/helper.h>
#include <adapter/dmdevice.h>
#include <adapter/leds.h>
#include <adapter/recorder.h>
#include <adapter/sleep.h>
#include <adapter/acc.h>
#include <adapter/copy_data.h>
#include <adapter/diagnostic.h>
#include <adapter/usb.h>
#include <adapter/power.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif

#define GLOBAL_BREAK_LINE { Status->GlobalTimeoutBreakLine = __LINE__; }

//������� ������� ������������ ������� ����������� �����
//����:
// Status - ��������� �� ���������� ��������� � ������� ���
// Count - ���������� ��������� ������� ���������
void AdapterGlobalWialTime( AdapterGlobalStatusStruct_t *Status,
                            unsigned int Count )
{
	int ret = AXIS_NO_ERROR;

	DBGLOG( "%s: While count %08x\n", __func__, Count );

	if( !Status )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return;
	}

	GLOBAL_BREAK_LINE;
	
	if ( ! ( Count & ADAPTER_TIMEOUT__DIAG_STATUS ) )
	{
		if ( Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature &
													DM_DM_ENABLE_DIAGNOSTIC )
		{
			if ( AdapterSaveDiagnostic ( &Status->GlobalDevice_t, Status ) < 0 )
			{
				DBGERR ( "%s: Failed: save diagnostic info\n", __func__ );
			}
		}
	}

	GLOBAL_BREAK_LINE;
	
	//���������� �� ���������
	//����������� ������
	if( !( Count & ADPATER_TIMEOUT__CRITICAL_THREAD ) )
	{
		GLOBAL_BREAK_LINE;

		AdapterHttpdCheck( Status );

		GLOBAL_BREAK_LINE;

		AdapterRecorderCheckAllRecordersWhile( &Status->Recorder );

		GLOBAL_BREAK_LINE;

		if( AdapterNetworkCheckWhile( Status ) < 0 )
		{
			DBGERR( "%s:It wasn't possible to check while net\n", __func__ );
		}
	}

	GLOBAL_BREAK_LINE;

	//������ ������� ����������
	//������ ������ ��� ��-�� � ��� �� ����������� ��� ��� ���� ���������� ������!!!!
	if( !( Count & ADAPTER_TIMEOUT__RECORDER_STATUS ) )
	{
		GLOBAL_BREAK_LINE;

		ret = AdapterRecorderCacheAllRecordersStatus( &Status->GlobalDevice_t, &Status->Recorder );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't get recorder status cache -%08x\n", __func__, -ret );
		}
		GLOBAL_BREAK_LINE;

		if( AdapterRecorderCacheAllRecordersSettings( &Status->GlobalDevice_t, &Status->Recorder ) < 0 )
		{
			DBGERR( "%s: It wasn't possible to read sets timeout\n", __func__ );
		}

		GLOBAL_BREAK_LINE;

		AdapterAccControlModems(Status);

		GLOBAL_BREAK_LINE;

		AdapterRecorderCheckAndRecover(Status);

		GLOBAL_BREAK_LINE;

		//���� ��� ����������� �� ����������, �� ��� ������ ��� ��� �������� 
		//� ������� AdapterRecorderCacheAllRecordersStatus
		ret =  AdapterAccGetStatus( Status );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't get acc device status\n", __func__ );
		}

		GLOBAL_BREAK_LINE;

		if( Status->SystemInfoFlag & PRINT_WAIL_REC_STATUS )
		{
			ret = AdapterRecorderPrintAllRecordersStatusInFile( &Status->Recorder,
																	fileno( stderr ) );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't print in file recorder status -%08x\n",
																	__func__,
																	-ret );
			}
		}

		GLOBAL_BREAK_LINE;

		if( Status->SystemInfoFlag & PRINT_WAIL_ACC_STATUS )
		{


			ret = DispatcherLibPrintACCStatusStderr( &Status->ACCStatus,
					Status->GlobalDevice_t.dmdevice_type );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't print acc staus\n", __func__ );
			}

		}

		GLOBAL_BREAK_LINE;

		if( Status->SystemInfoFlag & PRINT_WAIL_SERVER_STATUS )
		{
			ret = AdapterPrintStderrHTTPClientsInfo( Status );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't print httpd client status\n", __func__ );
			}
		}

		GLOBAL_BREAK_LINE;

		//� ��� ���� �������, ��� ��� ��������� ����� ���������
		//���������� �������� SMS � ���, ��� ������ ���������, � ������ �� ������ ���
		if( Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature & DM_DM_RECORDER_FULL_NAND_SMS )
		{
		 	//��� ������� �������� SMS � ���, ��� ������ ���� ���������
		 	ret = AdapterRecorderCheckFullAllRecorders( Status, &Status->GlobalDevice_t, &Status->Recorder );
			if( ret < 0 )
			{
			 	DBGERR( "%s: Can't check all recorders for full nand\n", __func__ );
			}
		}
		
		
		GLOBAL_BREAK_LINE;
	}

	GLOBAL_BREAK_LINE;

	AdapterSleepCheck( Status );

	GLOBAL_BREAK_LINE;

	//���������� ������ ���������� �������
	if( !( Count & ADAPTER_TIMEOT__SYSTEM_INFO ) )
	{

		if( ( Status->SystemInfoFlag & PRINT_WAIL_SYSTEMINFO_STATUS ) )
		{
			DispatcherLibPrintSystemInfo();
		}

		if( ( Status->SystemInfoFlag & PRINT_WAIL_SYSTEMFILE_STATUS ) )
		{
			DispatcherLibPrintOpenFilesStderr( getpid() );
		}

		if( Status->SystemInfoFlag & PRINT_WAIL_SLEEP_TIME_STATUS )
		{
			AdapterPrintTimeToSleep( Status );
		}
	}

	GLOBAL_BREAK_LINE;

	//������ �� ����� �������
	if( Count % 32 == 0 )
	{
		GLOBAL_BREAK_LINE;

		//���� ��� ���������� ������������ �������� ������� � ���������� !!!!
		if(!(Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature &
												DM_DM_WORK_SEPARATE))
		{
			if(!Status->Recorder.recorderscount && !Status->usbswitches)
			{
				DMXXXHWEvent(&Status->GlobalDevice_t,
											DM_HW_EVENT_RECORDER_DISCONNECT,
											NULL);
			}

		}
		GLOBAL_BREAK_LINE;
		if( Status->CopyStatus.PID >= 0 )
		{
			AdapterCopyDataCheck( &Status->CopyStatus );
		}
	}

	GLOBAL_BREAK_LINE;

	//������� ����� ��������� � �� �������� �� �� USB ����� �����
	if( !( Count & 0x03 ) &&  Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature & DM_DM_ENABLE_DYNAMIC_NET )
	{
		AdapterNetworkCheckNetDevicesIn( Status );
	}

	GLOBAL_BREAK_LINE;

	if( AdapterDmDeviceWatchDogTick( &Status->GlobalDevice_t ) )
	{
		DBGERR( "%s:Failed reset watchdog\n", __func__ );
	}

	GLOBAL_BREAK_LINE;

	if( ((Count & 0x00000003) == 0 ) && AdapterLedHeartBeatBlink( Status ) ) //TODO - ��������� ���� ����� �� ���������� ����������
	{
		DBGERR( "%s: Failed heart beat blink\n", __func__ );
	}

	GLOBAL_BREAK_LINE;

	//��������� ���� �� ������� ��� �������� �� � �������� SMS ��� ����� SMS �� ���������
	if( Count % 4 == 0 ) //TODO - ���������
	{
	 	ret = AdapterGlobalSMS( Status, 1, Count ); //����� ��������� ��� �� �������� ��� � �� �����
		if( ret < 0 )
		{
		 	DBGERR( "%s: Can't work global SMS\n", __func__ );
		}
	}

	GLOBAL_BREAK_LINE;

	//��������� ������� ������� �����, ���� �� �������� ��� �������
	if( !Status->flag_iconsole )
	{
		AdapterDebugLogRotate();
	}

	GLOBAL_BREAK_LINE;

	if( Status->USBPortNeadReset )
	{
		DBGERR( "%s: USB Port %d nead power down and reset\n", __func__, Status->USBPortNeadReset );
		INFO_LOG( 1, "USB Port %d nead power down and reset", Status->USBPortNeadReset );

		ret = AdapterPowerUSBReset( &Status->GlobalDevice_t, Status->USBPortNeadReset );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't power off and on USB port %d\n", __func__, Status->USBPortNeadReset );
		}

		Status->USBPortNeadReset = 0; //��� �� �� ������ �� �����
	}

	GLOBAL_BREAK_LINE

	if( Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature & DM_DM_ENABLE_DYNAMIC_NET &&
			!Status->modemsInSleep && //���� ������ ����, �� � ��������� �� ���������� ��� �� �����
	 		Status->NoModemFoundCount &&
			Status->NoModemFoundCount >= Status->NoModemFoundCountCheck )
	{
		//�� ������� ����� �������� ��� ������� - ��� �� ������� � � ���� ���� ��� �� ������
		DBGERR( "%s: No USB modems at more times %u(%u) Fatal=%u(%u)\n", __func__,
				Status->NoModemFoundCount, Status->NoModemFoundCountCheck,
				Status->NoModemFoundCountFatal, 32 ); //TODO - Rjycnfynf
		INFO_LOG( 1, "No USB modems at more times" );

		Status->NoModemFoundCount = 1; //�������� � 1 ��, ��� ��� ��� ����
		Status->NoModemFoundCountFatal++;
		//���� ������, ��� ������?
		//����� �������� ��� USB ���������� - � ����� ��������� ����� �������� (������ ��������)
		//����� ��������� ������� - ��� ����� ������ ������ (�� ������� �� ������, ����� ���� ���������� ������)
		//
		//������ (����� ������ ��� �����-��2) ���������� USB �� �������� ������� ����������
		//� ���� ��������� ������� ��������� � �������� ��� �� ����
		//����� ������� ������ �� 2 ������ ������ � ���� ����� (������ ��� ���� �� ������� ����������� �� �������)
		if( Status->NoModemFoundCountFatal >= 32 ) //TODO ���������
		{
			DBGERR( "%s: No USB modems at more fatal times %u(%u)\n", __func__, Status->NoModemFoundCountFatal, 32 ); //TODO - ���������
			INFO_LOG( 1, "No USB modems at more fatal times" );

			//�� �� ������� �� ������ USB ������ �� ����� ������ �����
			//��� � ����� ��������� ������ - � ����� �� ��������� ������� ��������
			//��� ����� ������ ��� � �������������� �����
			ret = AdapterUSBNoModemsFatal( Status  );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't work no USB modems found\n", __func__ );
			}
		}

		else
		{

			ret = AdapterUSBNoModemsAtMoreTime( Status );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't work no USB modems found\n", __func__ );
			}
		}
	}

	GLOBAL_BREAK_LINE;

	//���� ���� ��������� ������� �������� �� ���������� ���������� - �� �� ��� ���������
	if( Status->LedNetworkNoConnectionColour && !(Count & 0x00000007) ) //TODO - ���������
	{
		//�� ������ ������ ������� �������� ���� ��� �� ������ ������ �/��� ��� �� ������ ����������� � ����
		int connect = 0;
		int i;

		//������ ��� ��� �� �������������, ���� ������� �������� ������ ������
		if( AdapterNetworkGetConnectionStatuses( Status) < 0)
		{
			DBGERR("%s: It wasn't possible to get all connection status\n",__func__);
		}

		
		for( i = 0; i < NET_NETWORK_INTERFACES_COUNT; i++ )
		{

			if( !Status->Net[i].netlib )
			{
				continue;
			}

			//��� ���� �������� �� ���� - ��� �� �� ������������ ����� ���
			if( Status->Net[i].net_lib_type <= nettype_closed ||
				Status->Net[i].net_lib_type >= nettype_max )
			{
				continue;
			}
			
			if( Status->Net[i].status.status != DM_NET_CONNECTED )
			{
				continue;
			}

			else
			{
				connect = 1;
				break;
			}
		}

		if( !connect )
		{
			DBGLOG( "%s: No one modem connected\n", __func__ );
			ret = AdapterLedNetworkNoConnectionOn( Status );
		}

		else
		{
			DBGLOG( "%s: One in modem connected to network\n", __func__ );
			ret = AdapterLedNetworkNoConnectionOff( Status );
		}

		if( ret < 0 )
		{
			DBGERR( "%s: Can't work no network led %s\n", __func__, connect ? "off":"on" );
		}

	}

	GLOBAL_BREAK_LINE;

	if( Status->AdapterSettings.Block0.AdapterSettings.ServerIPAddress &&	//���� ��� ���������
			Status->LedNetworkNoConnectionColour &&						//���� ��� ����������
			! Status->NoModemFoundCount &&									//���� ���������� �������
			Count % 8 == 0 ) //TODO - ���������
	{
		//� ��� ���� ������ � ���� �������� ��� ��������� ���������� ����� - �������� �����
		//� ������ ������� �������� ����� ���������� �����������
		ret = AdapterNetworkPingServerHost( Status );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't check server host\n", __func__ );
		}
	}

	GLOBAL_BREAK_LINE;

}


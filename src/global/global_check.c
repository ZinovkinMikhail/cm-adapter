//�������, ������� ��������� ���������� ����� �������� �� ���������� ������

//�����: ��������� ����� ��� "����" ������
//������: 2016.09.28 11:22
//�����: i.podkolzin@ross-jsc.ru

#include <axis_error.h>
#include <adapter/global.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>

#else
#define DBGLOG( ... )
#endif

static uint32_t		 			GlobalWhileCount = 0;
static pthread_t				Thread;

/**
 * @brief �������, ������� ���������� � ������ � ������ �� ���������� �������
 *
 * @param Value - ��������� �� ��������� ����������� ������
 * @return void*
 **/
static void *AdapterGlobalCheckThreadRoutine( void *Value )
{

	if( !Value )
	{
		DBGERR( "%s: Invalid argument\n", __func__ );
		return NULL;
	}

	AdapterGlobalStatusStruct_t *Status = ( AdapterGlobalStatusStruct_t * )Value;

	//��� ������ ����������� ����
	sleep( 20 ); //TODO - constants

	while( 1 )
	{
		sleep( 10 ); //TODO - constants

		DBGLOG( "%s: %u: Aim hear. GlobalWhileCount=%u at line=%u. Timeout at line %u\n", __func__, __LINE__,
				Status->AdapterTCount,  Status->GlobalBreakLine, Status->GlobalTimeoutBreakLine );

		if( Status->AdapterTCount == GlobalWhileCount )
		{
			if( Status->GlobalWhileBreakLine )
			{
				DBGERR( "%s: Global while stop at line=%u more 10 sec, timeout line=%u tcount=%u (%u)\n", __func__,
					Status->GlobalWhileBreakLine, Status->GlobalTimeoutBreakLine,
					Status->AdapterTCount,
					GlobalWhileCount );
			}
			else if( Status->GlobalInitBreakLine )
			{
				DBGERR( "%s: Global init stop at line=%u more 10 sec\n", __func__, Status->GlobalInitBreakLine );
			}
			else
			{
			 	DBGERR( "%s: Global stop detected more 10 sec\n", __func__ );
			}
		}

		GlobalWhileCount = Status->AdapterTCount;
	}

}

/**
 * @brief ������� ������� ������ �������� �� ���������� �������
 *
 * @param  ...
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterGlobalCheckStart( void )
{

	AdapterGlobalStatusStruct_t *Status = AdapterGlobalGetStatus();

	if( !Status )
	{
		DBGERR( "%s: Can't get adapter status\n", __func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	if( pthread_create( &Thread, NULL,
						AdapterGlobalCheckThreadRoutine,
						( void * )Status ) < 0 )
	{
		DBGERR( "%s: Can't start modem thread\n", __func__ );
		return -AXIS_ERROR_CANT_CREATE_THREAD;
	}

	return 0;
}

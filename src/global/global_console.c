//����� �� ����� ������������ ������� ����� ���������


//���������� �������

//������������ �������
#include <axis_error.h>
#include <axis_keyboard.h>

#include <dispatcher_lib/dispatcher_lib_resource.h>
#include <dispatcher_lib/printf.h>
#include <dispatcher_lib/sprintf_ini_files.h>

#include <dmxxx_lib/dmxxx_calls.h>

//��������� �������
#include <adapter/global.h>
#include <adapter/resource.h>
#include <adapter/helper.h>
#include <adapter/recorder.h>
#include <adapter/dispatcher.h>
#include <adapter/debug.h>
#include <adapter/acc.h>
#include <adapter/interface.h>
#include <adapter/network.h>
#include <adapter/wifi.h>
#include <adapter/lte.h>
#include <adapter/httpd.h>
#include <adapter/sleep.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
//#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif

//������� ������ �� stderr ��������� ��������
//����:
// AdapterSettings - ���������� ��������� ��������
// TimersSettings - ���������� ��������� ��������
// AdapterDevice - ��� ���������� �� ������� ��� ��������
//�������:
// 0 - ��� ������
// ����� � � ������ ������
static void AdapterGlobalConsolePrintAdapterSettingsStderr( AdapterGlobalStatusStruct_t *Status )
{
	char buffer[ DISPATCHER_LIB_BUFFFER_SIZE ];
	int lcc;
	void *NetTimers;

	//������� ������� �������� � ���� ��������� � � ���� ������ ����������
	if( Status->GlobalDevice_t.dmdev_timer.dmctrl_feature & DM_TIMER_TYPE_DSP )
	{
		NetTimers = &Status->TimersSettingsNet;
	}
	else
	{
		NetTimers = &Status->TimersSettingsNetAxis;
	}

	if( iDMXXXAdapterSettingsIniFileSprintf( &Status->GlobalDevice_t ) )
	{
		//NOTE ����� AIP � ���������� ��������� ���������
		lcc = DMXXXAdapterSettingsIniFileSprintf( &Status->GlobalDevice_t,
					buffer, sizeof( buffer ),
					&Status->AdapterSettings, NetTimers,
					&Status->DHCPNetParam,
					AdapterHttpdGetClientIP( axis_getpid(), Status, NULL ) );
	}
	else
	{
		//NOTE ��� ������� �� �������������� ����� API ����������� �� ��������
		lcc = DispatcherLibIniFileSprintfAdapterSettings(
						buffer, sizeof( buffer ),
						&Status->AdapterSettings, NetTimers,
						&Status->DHCPNetParam,
						AdapterHttpdGetClientIP( axis_getpid(), Status,NULL ),
						Status->GlobalDevice_t.dmdevice_type );
	}

	if( lcc >= sizeof( buffer ) )
	{
		DBGERR( "%s: Output buffer overflow %d (%zu)\n", __func__, lcc, sizeof( buffer ) );
	}
	else
	{
		buffer[ lcc ] = 0x00;
		fprintf( stderr, "%s", buffer );
	}

}

//������� ������� �������� � ����� ��� ������ �������
//����
// b - ��������� �� ����� ���� ���������� ������
// size - ������ ������ ������
// Status - ��������� �� ��������� �������
// Flag - ������� �������� ����� ��� ���
//�������:
// ���������� ����������� ����
size_t DispatcherLibEventPrintAllEx( char *b, size_t bsize, DispatcherLibEventStatus_t *Status,
											unsigned int Flag )
{
	int cc=0;
	int lcc = 0;
	size_t size = bsize;
	unsigned int i;

	if( !Status || !b || !size  )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return 0;
	}

	lcc = axis_sprintf_s(
							b+cc,
							size,
							DISPATCHER_LIB_INTERFACE_INI_SECTION_EVENT );
	cc	   += lcc;
	size   -= lcc;
	if( size <= 0 )
	{
		goto end;
	}

	//���������� ���������
	lcc = axis_sprintf_d(
							b+cc,
							size,
							DISPATCHER_LIB_INTERFACE_INI__EVENTS_COUNT,
						    Status->EventCount ); //���� �� ������ �� �����
	cc	   += lcc;
	size   -= lcc;
	if( size <= 0 ) goto end;


	//��������� �� ���� ������
	for( i=0; i<Status->EventCount; i++ )
	{
		lcc = DispatcherLibEventPrintEx( b+cc, size, Status, i, Flag );

		cc	   += lcc;
		size   -= lcc;
		if( size <= 0 ) goto end;
	}


end:

    b[ bsize - 1 ] = 0x00;
    return cc;
}


//������� ������� �������� � stderr ��� �������
//����
// Status - ��������� �� ��������� �������
//�������:
// 0 - ��� ��
// ����� 0 � ������ ������
static int DispatcherLibEventPrintStderrEx( DispatcherLibEventStatus_t *Status )
{

	char buffer[ DISPATCHER_LIB_BUFFFER_SIZE ];

	int lcc = DispatcherLibEventPrintAllEx( buffer, sizeof( buffer ), Status, 1 );

	buffer[ lcc ] = 0x00;

	fputs( "----------------------------------------------------\n", stderr );
	fputs( buffer, stderr );
	fputs( "----------------------------------------------------\n", stderr );

	fflush( stderr );

	return 0;
}


//������� ��������� ������� � �������
//����:
// Status - ��������� �� R2AdapterGlobalStatusStruct_t ���������
//      ������� � ������� ��� ����� ������������
//�������:
// ������ ���� ���� ������� ��������� ���������
// � ����� �����, ��������, �����
// ����������� ��� ������� �������
// 0 - ��� ������� ����������
// ����� 0 � ������ ������
int AdapterGlobalDoConsole( AdapterGlobalStatusStruct_t *Status )
{

	unsigned int CurrentKey = 0;
	int ret = AXIS_NO_ERROR;

	//�� ������������ �������

	if ( !Status )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	if ( Status->GlobalConsole < 0 )
	{
		DBGERR( "%s: Console not open\n", __func__ );
		return -AXIS_ERROR_DEVICE_NOT_OPEN;
	}


	//���������� � ����������� �� ������
	CurrentKey = keyboard_getcharacter( Status->GlobalConsole );

	//��� ��������� ������ ����
	if ( !CurrentKey )
		return ret;

	switch ( CurrentKey )
	{

		case ADAPTER_KEY__USER_HELP:
		{
			AdapterPrintConsoleHelp( &Status->GlobalDevice_t );
			break;
		}

		case ADAPTER_KEY__USER_SYSTEM_INFO:
		{
			DispatcherLibPrintSystemInfo();
			break;
		}

		case ADAPTER_KEY__WAIL_SYSTEM_INFO:
		{
			if ( Status->SystemInfoFlag & PRINT_WAIL_SYSTEMINFO_STATUS )
			{
				printf( "\t---Disabling System Info print---\n" );
				Status->SystemInfoFlag &= ~PRINT_WAIL_SYSTEMINFO_STATUS;
			}
			else
			{
				printf( "\t---Enabling SystemInfo print---\n" );
				Status->SystemInfoFlag |= PRINT_WAIL_SYSTEMINFO_STATUS;
			}

			AdapterPrintSystemInfoWail( Status->SystemInfoFlag );

			break;
		}

		case ADAPTER_KEY__SLEEP_TIME_PRINT:
		{
			AdapterPrintTimeToSleep( Status );
			break;
		}

		case ADAPTER_KEY__SLEEP_TIME_PRINT_WAIL:
		{
			if( Status->SystemInfoFlag & PRINT_WAIL_SLEEP_TIME_STATUS )
			{
				printf( "\t--Disabling Sleep Time Info print--\n" );
				Status->SystemInfoFlag &= ~PRINT_WAIL_SLEEP_TIME_STATUS;
			}
			else
			{
				printf( "\t---Enabling Sleep Time Info print--\n" );
				Status->SystemInfoFlag |= PRINT_WAIL_SLEEP_TIME_STATUS;
			}
			break;
		}

		case ADAPTER_KEY__USER_RECORDER_STATUS:
		{
		 
		 	//�������� � stderr ������� ���� �����������, �������� ������� info � ����������� � ������ ����������
 			ret = AdapterRecorderPrintAllRecordersStatusInFile( &Status->Recorder, fileno( stderr ) );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't print in file recorders status %d\n", __func__, ret );
			}

			break;
		}

		case ADAPTER_KEY__USER_HELLO:
		{
			AdapterPrintConsoleFast( Status );
			break;
		}

		case ADAPTER_KEY__VIEW_MODE_SCREEN:
		{
			char buff[65536];
			
			//TODO - ��� ����� ���� �� ���� ��������� �� ����� �����������!!!
			ret = AdapterRecorderPrintSettingsIni( Status->Recorder.next, buff, sizeof(buff) );
			
			if( ret < 0)
			{
				DBGERR( "%s: Can't get recorder settings\n", __func__ );
			}
			
			ret = fprintf(stderr, "%s", buff );
			
			if( ret < 0)
			{
				DBGERR( "%s: Can't print recorder settings\n", __func__ );
			}
			break;
		}

		case ADAPTER_KEY__VIEW_NETWORK_INFO:
		{
			// IP �������
			AdapterPrintDeviceIP();
			// � �������� ������� ����
			AdapterPrintCurrentNetwork( Status, &Status->CurrentNetwork  );
			break;
		}

		case ADAPTER_KEY__VIEW_NETWORK_SCREEN:
		{
			AdapterGlobalConsolePrintAdapterSettingsStderr( Status );
			break;
		}

		case ADAPTER_KEY__RESCUE_MODE:
		{
			AdapterGlobalSetRescueMode();
			break;
		}

		case ADAPTER_KEY__USER_RECORDER_STATUS_WAIL:
		{
			if ( Status->SystemInfoFlag & PRINT_WAIL_REC_STATUS )
			{
				printf( "\t---Disabling wail Rec print---\n" );
				Status->SystemInfoFlag &= ~PRINT_WAIL_REC_STATUS;
			}
			else
			{
				printf( "\t---Enabling wail Rec print---\n" );
				Status->SystemInfoFlag |= PRINT_WAIL_REC_STATUS;
			}

			break;
		}

		case ADAPTER_KEY__VIEW_DEVSTAT:
		{
			ret = AdapterPrintCurrentDevice( Status );

			if ( ret < 0 )
			{
				DBGERR( "%s: Can't print device status\n", __func__ );
			}

			break;
		}

		case ADAPTER_KEY__VIEW_ACCSTAT:
		{
		 
			ret = AdapterAccGetStatus( Status );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't get acc device status\n", __func__ );
			}

			ret = DispatcherLibPrintACCStatusStderr( &Status->ACCStatus,

			                Status->AdapterDevice );

			if ( ret < 0 )
			{
				DBGERR( "%s: Can't print acc staus\n", __func__ );
			}

			break;
		}

		case ADAPTER_KEY__VIEW_ACCSTAT_WAIL:
		{
			if ( Status->SystemInfoFlag & PRINT_WAIL_ACC_STATUS )
			{
				printf( "\t---Disabling wail ACC print---\n" );
				Status->SystemInfoFlag &= ~PRINT_WAIL_ACC_STATUS;
			}
			else
			{
				printf( "\t---Enabling wail ACC print---\n" );
				Status->SystemInfoFlag |= PRINT_WAIL_ACC_STATUS;
			}

			break;
		}
		case ADAPTER_KEY__VIEW_HENDELS_WAIL:
		{
			if ( Status->SystemInfoFlag & PRINT_WAIL_SYSTEMFILE_STATUS )
			{
				printf( "\t---Disabling wail Hendel print---\n" );
				Status->SystemInfoFlag &= ~PRINT_WAIL_SYSTEMFILE_STATUS;
			}
			else
			{
				printf( "\t---Enabling wail Hendel print---\n" );
				Status->SystemInfoFlag |= PRINT_WAIL_SYSTEMFILE_STATUS;
			}

			break;
		}
		case ADAPTER_KEY__VIEW_HENDELS:
		{
			
 			DispatcherLibPrintOpenFilesStderr( getpid() );
			break;
		}
		case ADAPTER_KEY__INTERFACE_DEBUG_LOG:
		{
			ret = AdapterInterfacePrintLog( stderr, &Status->AdapterTCount );
			if( ret < 0)
			{
				DBGERR( "%s: It wasn't possible to print interface log\n", __func__ );
				return ret;
			}
			break;
		}
		case ADAPTER_KEY__INTERFACE_ERR_LOG:
		{
			ret = AdapterInterfacePrintErr( stderr, &Status->AdapterTCount );
			if( ret < 0)
			{
				DBGERR( "%s: It wasn't possible to print interface log\n", __func__ );
				return ret;
			}
			break;
		}
		case ADAPTER_KEY__INC_KERN_DEBUG:
		{
			int arg = AdapterGetKernelDebugLevel();
			arg++;

			if ( !(AdapterSetKernelDebugLevel( arg )) )
			{
				DBGLOG( "%s: Inreased debug level. CurDL = %i\n",
															__func__,
															arg );
			}


			break;
		}
		case ADAPTER_KEY__DEC_KERN_DEBUG:
		{
			int arg = AdapterGetKernelDebugLevel();
			arg--;
			
			if ( !(AdapterSetKernelDebugLevel( arg )) )
            {
				DBGLOG( "%s: Decreased debug level. CurDL = %i\n",
															__func__,
															arg );
			}

			break;
		}
		case ADAPTER_KEY__USER_EXIT:
		{
			ret = ADAPTER_KEY__USER_EXIT;
			break;
		}
		case ADAPTER_KEY__INTERFACE_EVENT_PRINT:
		{
			DispatcherLibEventPrintStderrEx( &Status->ExchangeEvents.Events );
			break;
		}
		case ADAPTER_KEY__INTERFACE_SERVER_PRINT:
		{
		 	if( AdapterPrintStderrHTTPClientsInfo( Status ) < 0 )
			{
			 	DBGERR( "%s: Can't print server status\n", __func__ );
			}
			break;
		}
		case ADAPTER_KEY__INTERFACE_SERVER_PRINT_WAIL:
		{
			if ( Status->SystemInfoFlag & PRINT_WAIL_SERVER_STATUS )
			{
				printf( "\t---Disabling wail server status print---\n" );
				Status->SystemInfoFlag &= ~PRINT_WAIL_SERVER_STATUS;
			}
			else
			{
				printf( "\t---Enabling wail server status print---\n" );
				Status->SystemInfoFlag |= PRINT_WAIL_SERVER_STATUS;
			}

			break;
		 	
		}
		case ADAPTER_KEY__INTERFACE_PING:
		{
			//� ������ ������ ���������� ��������� ������� ����� � ��������
			//� �������� �� ������ ������������, ����� � Google DNS �������

			ret = AdapterNetworkPingAllStdout( Status );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't ping some hosts\n", __func__ );
			}

			break;
		}
		case ADAPTER_KEY__WIFI_ON:
		{
			ret = AdapterWiFiPower( &Status->GlobalDevice_t, 1 );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't power on WiFi device\n", __func__ );
			}

			INFO_LOG( ret, "Console command WiFi on" );
			break;
		}
		case ADAPTER_KEY__WIFI_OFF:
		{
			ret = AdapterWiFiPower( &Status->GlobalDevice_t, 0 );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't power off WiFi device\n", __func__ );
			}

			INFO_LOG( ret, "Console command WiFi off" );
			break;
		}
		case ADAPTER_KEY__LTE_ON:
		{
			ret = AdapterLTEPower( &Status->GlobalDevice_t, 1 );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't power on LTE device\n", __func__ );
			}

			INFO_LOG( ret, "Console command LTE on" );
			break;
		}
		case ADAPTER_KEY__LTE_OFF:
		{
			ret = AdapterLTEPower( &Status->GlobalDevice_t, 0 );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't power off LTE device\n", __func__ );
			}

			INFO_LOG( ret, "Console command LTE off" );
			break;
		}
		case ADAPTER_KEY__RECORDER_ON:
		{
			ret = AdapterRecorderPower( &Status->GlobalDevice_t, 1 );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't power on Recorder device\n", __func__ );
			}

			INFO_LOG( ret, "Console command Recorder on" );
			break;
		}
		case ADAPTER_KEY__RECORDER_OFF:
		{
			ret = AdapterRecorderPower( &Status->GlobalDevice_t, 0 );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't power off LTE device\n", __func__ );
			}

			INFO_LOG( ret, "Console command Recorder off" );
			break;
		}
		case ADAPTER_KEY__USB_RESET_1:
		case ADAPTER_KEY__USB_RESET_2:
		case ADAPTER_KEY__USB_RESET_3:
		case ADAPTER_KEY__USB_RESET_4:
		case ADAPTER_KEY__USB_RESET_5:
		case ADAPTER_KEY__USB_RESET_6:
		case ADAPTER_KEY__USB_RESET_7:
		case ADAPTER_KEY__USB_RESET_8:
		case ADAPTER_KEY__USB_RESET_9:
		{
			//�� ������ ������������� ����� i�� USB �����
			DBGERR( "%s: Try set USB port %d to reset\n", __func__, CurrentKey - kb_0 );
			Status->USBPortNeadReset = CurrentKey - kb_0;
		}
		case ADAPTER_KEY__NET_MODE_AUTO:
		{
			Status->AdapterNeedLTECommand = DM_NET_COMMAND_SET_BAND_AUTO;
			INFO_LOG( 0, "Console command LTE band Auto" );
			break;
		}
		case ADAPTER_KEY__NET_MODE_3G:
		{
			Status->AdapterNeedLTECommand = DM_NET_COMMAND_SET_BAND_3G;
			INFO_LOG( 0, "Console command LTE band 3G" );
			break;
		}
		case ADAPTER_KEY__NET_MODE_LTE:
		{
			Status->AdapterNeedLTECommand = DM_NET_COMMAND_SET_BAND_LTE;
			INFO_LOG( 0, "Console command LTE band LTE" );
			break;
		}
		case ADAPTER_KEY__NET_PPP_CONNECT:
		{
			Status->AdapterNeedLTECommand = DM_NET_COMMAND_CONNECT;
			INFO_LOG( 0, "Console command LTE connect" );
			break;
		}
		default:
		{
			DBGLOG( "%s: Pressed key %d\n", __func__, CurrentKey );
			break;
		}

	}

	return ret;

}

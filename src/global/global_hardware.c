//#ifdef TMS320DM365
//���������� �������
#include <unistd.h>
#include <stdio.h>
#include <errno.h>

//������������ �������
#include <axis_error.h>
#include <axis_string.h>
#include <axis_modules.h>
#include <axis_time.h>
#include <axis_find_from_file.h>

#include <dispatcher_lib/dispatcher_lib_resource.h>
#include <dispatcher_lib/system.h>

#include <dmxxx_lib/dmxxx_resource.h>
#include <dmxxx_lib/dmxxx_calls.h>

//���������
#include <adapter/global.h>
#include <adapter/hardware.h>
#include <adapter/nand.h>
#include <adapter/debug.h>
#include <adapter/module.h>
#include <adapter/power.h>
#include <adapter/dmdevice.h>
#include <adapter/firmware.h>
#include <adapter/usb.h>
#include <adapter/recorder.h>
#include <adapter/timer.h>
#include <adapter/leds.h>
#include <adapter/interface.h>
//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
//#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif



//������� ������� �������������� ������ � �����������
//dm365 �� ���������� Texas TMS320DM365
//����:
// Status - ��������� �� ��������� ����������� ������� ����������
//�������:
// ���� �������� �� ������� ���, ��� ��� ��� ������ ������
// ��������� � ������������ ����� ��������� ������ �� �������
// � �������� ����������
int  AdapterGlobalDM365Init( AdapterGlobalStatusStruct_t *Status )
{


	if( !Status )
	{
		DBGLOG( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return -1;
	}

	if( AdapterDmDeviceInit( &Status->GlobalDevice_t ) )
	{
		DBGERR( "%s: It wasn't possible to init dm device\n", __func__ );
		INFO_LOG( 1, "Init dmXXX device" );
		return -1;
	}

	INFO_LOG( 0, "Init dmXXX device" );

	if( Status->GlobalDevice_t.dmdev_interface.dmctrl_ipresent && Status->flag_no_interface )
	{
		//���� ��������� ���� ������, �� �� �������� ��� ����� - �� ����� �������, ��� ��������� ������ ���� ����
		//����� ������������ �������� ��� ��������� �� ������ ������� ����������
		Status->GlobalDevice_t.dmdev_interface.dmctrl_status |= DM_INTERFACE_STATUS_IN_SLEEP;
	}

	if( AdapterLedInit( Status ) )
	{
		DBGERR( "%s: It wasn't possible to init led from device library\n", __func__ );
		INFO_LOG( 1, "Init dmXXX leds" );
		return -1;
	}

	INFO_LOG( 0, "Init dmXXX leds" );

	AdapterLedHeartBeatOn( Status );
	
	if( AdapterPowerInit( &Status->GlobalDevice_t ) )
	{
		DBGERR( "%s: It wasn't possible to power on all devices\n", __func__ );
		INFO_LOG( 1, "Power On devices" );
		return -1;
	}

	INFO_LOG( 0, "Power On devices" );

	//��������� ������� ������ �� ������ ���� �� �������
	//����� �� ��������� �������
	if( Status->GlobalDevice_t.dmdevice_powerupstatus == DISPATCHER_LIB_EVENT__POWER_UP )
	{

		if( AdapterFirmwareCheckInit( Status ) )
		{
			DBGERR("%s: It wasn't possible to check firmware\n",__func__);
			INFO_LOG( 1, "Check if new firmware is" );
			//��� ��� ��� �������� ������
			//return -1;
		}
		else
		{
			INFO_LOG( 0, "Check if new firmware is" );
		}
	}

	if( AdapterInitDevicesModules( &Status->GlobalDevice_t ) )
	{
		DBGERR( "%s: It wasn't possible to init modules\n", __func__ );
		INFO_LOG( 1, "Init devices modules" );
		return -1;
	}

	INFO_LOG( 0, "Init devices modules" );

	if( AdapterDmDeviceMknod( &Status->GlobalDevice_t ) )
	{
		DBGERR( "%s: It wasn't possible to mknod\n", __func__ );
		INFO_LOG( 1, "Mknod devices" );
		return -1;
	}

	INFO_LOG( 0, "Mknod devices" );

	if( AdapterUsbInit( &Status->GlobalDevice_t ) )
	{
		DBGERR( "%s: It wasn't possible to init usb\n", __func__ );
		INFO_LOG( 1, "Usb init" );
		return -1;
	}

	INFO_LOG( 0, "Usb init" );

	Status->GlobalSourceWork = Status->GlobalDevice_t.dmdevice_powerupstatus;

#ifdef RECORDER_NEED_OR_NOT
	if( AdapterRecorderInit( &Status->GlobalDevice_t, &Status->Recorder ) )
	{
		DBGERR( "%s: It wasn't possible to init recorder\n", __func__ );
		INFO_LOG( 1, "Usb recorder init" );
		return -1;
	}

	INFO_LOG( 0, "Usb recorder init" );
#endif
	if( AdapterNandInit( &Status->GlobalDevice_t, &Status->Nand ) )
	{
		DBGERR( "%s: It wasn\t possible to get Nand Status\n", __func__ );
		INFO_LOG( 1, "Nand init" );
		return -1;
	}

	INFO_LOG( 0, "Nand init" );

	if( AdapterTimerClockInit( Status ) )
	{
		DBGERR( "%s: It wasn't possible to init clock\n", __func__ );
		INFO_LOG( 1, "Clock init" );
		return -1;
	}

	INFO_LOG( 0, "Clock init" );

	if ( AdapterDmDeviceAdxlInit( &Status->GlobalDevice_t ) < 0)
	{
		DBGERR( "%s: it wasn\t possible to init adxl\n", __func__ );
		INFO_LOG( 1, "ADXL init" );
		return -1;
	}

	INFO_LOG( 0, "ADXL init" );
	
	Status->GlobalSourceWork = Status->GlobalDevice_t.dmdevice_powerupstatus;
	
	AdapterLedHeartBeatOff( Status );

	//�� ����� �� ��� �� ����
	return 0;
}


#ifdef NOT_USE
//������� ��������� ������� � �2� ������
//����:��������� � �����
//�������: 0 �� ����� �����
int AdapterSystemTimeSet( struct_data *date )
{
	if( !date )
	{
		DBGERR( "%s: Invalid Argument\n", __func__ );
		return -1;
	}

	int dev = 0;

	dev = TimerDeviceOpen();
	int ret;

	if( dev < 0 )
	{
		DBGERR( "%s: It wasn't possible to open i2c clock\n", __func__ );
		return dev;
	}

	ret = TimerSetDateTime( dev, date );

	if( ret < 0 )
	{
		DBGERR( "%s: It wasn't possible to set date ti i2c clock\n", __func__ );
		return ret;
	}

	if( dev > 0 )
	{
		TimerDeviceClose( dev );
	}

	if( DispatcherLibDM3X5SystemRTCTimeSyncNoDev( date ) < 0 )
	{
		DBGERR( "%s: It wasn't possible to sync time \n", __func__ );
		return -1;
	}

	return 0;

}

//������� ��������� ������� � �2� ������
//����:��������� � �����
//�������: 0 �� ����� �����
int AdapterSystemTimeGet( struct_data *date )
{
	if( !date )
	{
		DBGERR( "%s: Invalid Argument\n", __func__ );
		return -1;
	}

	int dev = 0;

	dev = TimerDeviceOpen();
	int ret;

	if( dev < 0 )
	{
		DBGERR( "%s: It wasn't possible to open i2c clock\n", __func__ );
		return dev;
	}

	ret = TimerGetDateTime( dev, date );

	if( ret < 0 )
	{
		DBGERR( "%s: It wasn't possible to set date ti i2c clock\n", __func__ );
		return ret;
	}

	if( dev > 0 )
	{
		TimerDeviceClose( dev );
	}

	return 0;

}
#endif

/**
 * @brief ������� ��������� �������� �������� ����� ��� ��������� ��������
 * ��� ���� �������������� ���������� (����� ������������ ������� �����)
 * � ��� �� ������� �� ����������, ��� �� ���������� ������������ � �������������
 * ������ � �������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������ (-AXIS_ERROR_BUSY_DEVICE - ����� �� ����������)
 */
static int AdapterGlobalCanWorkWiteTimeout( AdapterGlobalStatusStruct_t *Status )
{
	//� ��� ������� - ������ ������� � ���������� ������
	int ret;
	int err;
	int Interface;
	dmdev_user_interface_t *InterfacePriv = NULL;
	fd_set rfds;	//��������� ��� �������

	struct timeval tv;	//��������� ��������
	int retval;		//�������
	int max_sel	= 0;	//������������ ������ ��� �������

	ret = DMXXXWdTick( &Status->GlobalDevice_t );

	if( ret < 0 )
	{
		DBGERR( "%s: Can't tick watch dog\n", __func__ );
		return ret;
	}

	Status->AdapterTCount++;

	InterfacePriv = (dmdev_user_interface_t *)Status->GlobalDevice_t.dmdev_interface.privatedata;
	Interface = Status->GlobalDevice_t.dmdev_interface.dmctrl_ipresent && InterfacePriv &&
				(InterfacePriv->communication_type & DM_INTERFACE_CONNECTION_UNIX );

	//���� ��� �� ���������� � �� ���������� - �� ������ ���� �����
	if( !Interface && Status->GlobalConsole < 0 )
	{
		sleep( 1 );
		return 0;
	}

	//� ��������� ������ ���� ��������
	//� �������� �� ������������� �������

	//���������� �������
	FD_ZERO ( &rfds );

	max_sel = 0;

	//���� ������� ����������
	if( Status->GlobalConsole >= 0 )
	{
		max_sel = AdapterGlobalWileSetHendel( Status->GlobalConsole, &rfds, max_sel );
	}

	if( Interface )
	{
		max_sel = AdapterInterfaceSetHendelUnixSocket( InterfacePriv, &rfds, max_sel );
	}

	max_sel++;

	//���������� �����
	tv.tv_sec = 1;
	tv.tv_usec = 0;

	//�������� ������
	retval = select ( max_sel, &rfds, NULL, NULL, &tv );

	if( retval < 0 )
	{
		//������ �������
		if( errno == EINTR )
		{
			//��� �� ������ - �� ������� ������!!!
			DBGLOG( "%s: Select signal interrupt\n", __func__ );
			sleep( 1 );
		}

		perror( "select" );
		//������ ������� - ����� �� �����
		DBGERR( "%s: Select Error %i \n", __func__, errno );
		return -AXIS_ERROR_CANT_IOCTL_DEVICE;
	}

	if ( !retval )
	{
		//�� ���� ��� - �� ���� ������� ��� ��� �� �� ��� � ����������
		return 0;
	}

	//��������� ���������� - ����������
	if( Status->GlobalConsole >= 0 && FD_ISSET( Status->GlobalConsole, &rfds ) )
	{
		//���� ������� �� ����������
		err = AdapterGlobalDoConsole( Status );

		if( err < 0 )
		{
			DBGERR( "%s: Can't execute console command\n", __func__ );
			return err;
		}
		else if( ADAPTER_KEY__USER_EXIT == err )
		{
			DBGLOG( "%s: Console command exit\n", __func__ );

			INFO_LOG( 0, "Console command exit" );

			return -AXIS_ERROR_BUSY_DEVICE;
		}
	}

	if( Interface )
	{
		err = AdapterInterfaceWorkUnixSockets( InterfacePriv, &rfds, Status );

		if( err < 0 )
		{
			DBGERR( "%s: Can't execute interface unix socket\n", __func__ );
			return err;
		}
	}

	return 0;
}


/**
 * @brief �������, ������� ��� ������ ������� ����� �� ������ �������� � ���������
 * ���������� ���������� � ���������� ����������, ��� ����, ���� �������� �����������
 * ������������ ���������� ���������� ���������� (���� �� ������������ � ��������)
 * �� ��������� � ������������� ������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� ��������, ����� 0 � ������ ������ (AXIS_ERROR_BUSY_DEVICE -  ����� �� ����������)
 */
int AdapterGlobalCanWorkWite( AdapterGlobalStatusStruct_t *Status )
{
	//���� ����� ������� ������:
	// - �� �������� � ���� ����� ���� ��� ���������� �� ������, ��� ��� ������
	// - ���� ���� � ��� ���������, �� �� � ��������� �������� ���������� ��� �������
	// 		� ������ ������ - �� ��� �������, ��� � ��� ���������� ���!!!
	// - ��� �� ������ ���������� ������ � ������� ������

	int ret;
	uint32_t count = 0;

	while( 1 ) //TODO - ���������� ����� ���� �� �����
	{
		ret = DMXXXCanWorkOn( &Status->GlobalDevice_t, 1 ); //TODO - ��������� ������� ��������

		if( !ret )
		{
			//��� ���� ����� �� ������
			return 0; //�� ����
		}

		if( ret == -AXIS_ERROR_TIMEOUT_EVENT )
		{
			DBGERR( "%s: Device not ready for work. wite it %08x\n", __func__, count );

			ret = AdapterGlobalCanWorkWiteTimeout( Status );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't work device timeout %08x\n", __func__, -ret );
				return ret;
			}

			count++;

			//���� �� ������� ���������� ������� ����� - ���� ���������� ���������
			//� ���������
			if( ( count & 0x0000000f ) == 0x0000000 )
			{
				//��� ��������� �������, � � ��� ���������� �� ������ - ������ ��� �� ���������� USB
				int err = AdapterInterfaceSendCommand( Status, AXIS_INTERFACE_EVENT_RECORDER_OUT );

				if( err < 0 )
				{
					DBGERR( "%s:%i: It wasn't possible to send event 'Recorder Out' to interface\n", __func__, __LINE__ );
				}
			}

			continue;
		}

		else
		{
			DBGERR( "%s: Can't ask device for ready\n", __func__ );
			return ret;
		}
	}

	DBGERR( "%s: While unexpected exit\n", __func__ );
	return -AXIS_ERROR_NOT_INIT;
}


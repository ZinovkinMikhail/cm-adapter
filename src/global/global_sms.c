//������� ��������� SMS � �������
//

//������: 2016.08.04 13:02
//�����: ��������� ����� ��� "����" ������
//�����: i.podkolzin@ross-jsc.ru

//������� ����������� ���� �� ����� ������� �� �������� ��� ����� SMS �� �����
//� �������������� �������������� ��

#include <axis_error.h>

#include <dispatcher_lib/events.h>
#include <dispatcher_lib/network.h>

#include <adapter/global.h>
#include <adapter/sms.h>
#include <adapter/power.h>
#include <adapter/debug.h>
#include <adapter/network.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif


static int AdapterGlobalSMSWorkNewEvent( AdapterGlobalStatusStruct_t *Status, int EventNumber )
{
	int ret = 0;
	sms_t SMS;
	int i;
	int j = 0;
	int sms_sent = 0;

	if( !Status || EventNumber < 0 || EventNumber >= DISPATCHER_LIB_EVENT__MAX_EVENTS )
	{
		DBGERR( "%s: Invalid argument\n", __func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	DBGLOG( "%s: Try work new event %d\n", __func__, EventNumber );

	for( j = 0; j < NET_NETWORK_INTERFACES_COUNT; j++ )
	{
		libnetcontrol_t *tmpNetStruct = &Status->Net[j];

		if( !tmpNetStruct )
		{
			DBGLOG( "%s: Network subsystem %i is empty\n", __func__, j );
			continue;
		}

		if( tmpNetStruct->net_lib_type <= nettype_closed ||
			tmpNetStruct->net_lib_type >= nettype_max			)
		{
			DBGLOG( "%s: Network subsystem %i have a invalid type %d\n", __func__, j, tmpNetStruct->net_lib_type_ );
			continue;
		}

		if( !tmpNetStruct->net_send_sms )
		{
			DBGLOG( "%s: Network subsystem %i '%s' not support send SMS\n", __func__, j, AdapterNetworkGetConnectionTypeName( tmpNetStruct->net_lib_type_ ) );
			continue; //��� ���������, ��� ���� �� ������������ �������� SMS
		}

		//TODO - �� ����� � ��� ��� �� �����!!
		uint64_t sn = strtoull( Status->CurrentNetwork.SN, NULL, 16 );

		//���������� ����� � ��������
		ret = AdapterEventPrintSMS( SMS.text, sizeof( SMS.text ),
									&Status->ExchangeEvents.Events.Events[ EventNumber ],
									 ( uint8_t * )&sn, sizeof( uint64_t ) );

		if( !ret )
		{
			DBGERR( "%s: Can't print event %d to send\n", __func__, EventNumber );
			return -AXIS_ERROR_INVALID_ANSED;
		}

		SMS.text[ ret ] = 0x00;

		//�� ��� ������ ������� ���� � ������� ���������, ���� ������� ������
		for( i = 0; i < AXIS_SOFTADAPTER_CDMA_PHONE_COUNT; i++ )
		{

			DBGLOG( "%s: Try send sms '%s' to phone '%s'\n", __func__, SMS.text,
					Status->AdapterSettings.Block1.CDMASettings.Phone[i] );

			if( Status->AdapterSettings.Block1.CDMASettings.Phone[i][0] != 0x00 &&
					DispatcherLibPhoneCheck( Status->AdapterSettings.Block1.CDMASettings.Phone[i] ) )
			{
				strncpy( SMS.number, Status->AdapterSettings.Block1.CDMASettings.Phone[i],
						 sizeof( SMS.number ) );

				ret = tmpNetStruct->net_send_sms( &SMS );

				if( ret < 0 )
				{
					DBGERR( "%s: It wasn't possible to sent sms\n", __func__ );
					return ret;
				}
			}
		}

		sms_sent++;

	}

	if( sms_sent )
	{
		//��� ��� �� ������� �������� ������� � ����� �� ��������
		//���� ������� ������ ��� ��� ��� �������� - �� �� ����
		ret = DispatcherLibEventSetFlag( &Status->ExchangeEvents.Events,
										 EventNumber, DISPATCHER_LIB_EVENT_FLAG__SMS );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't set sending flag to event %d\n", __func__, EventNumber );
		}
	}

	return ret;
}

/**
 * @brief ������� ������ � SMS ����������� �������� �� �������, ��������� ���������� � ��
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @param iResive - ���� ����, ��� ���� ���������� �������� SMS
 * @param TCount - ������� ������� ��������� �����
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterGlobalSMS( AdapterGlobalStatusStruct_t *Status, int iResive, unsigned int TCount )
{

	int ret;

	if( !Status )
	{
		DBGERR( "%s: Invalid argument\n", __func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	//� ��� ���� ��� ���� - 1 �� ������������ ������� �� ������� ���� ���������� SMS
	//����, ������� ������� ���������� ������������ ����� �����������
	//��� ������� ������������ �������� SMS 
	if( iResive  )
	{
		//2 - ���� �������� SMS ��������� �� ������� ����������
		ret = AdapterWorkNextSMS( Status, &Status->SMSControl );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't working incomming SMS\n", __func__ );
			return ret;
		}
	}

	//���� �� �� ���������� �� �� (�� ���� �� SMS), � ��� ���� ����� �������, ����� ��������
	//��� �� �������� �� SMS � ��� ������ �� SMS, ������� ��� �����
	if( Status->GlobalDevice_t.dmdevice_powerupstatus != DISPATCHER_LIB_EVENT__WAKEUP )
	{
		ret = DispatcherLibEventGetNew( &Status->ExchangeEvents.Events );

		if( ret < 0 )
		{
			if( ret == -AXIS_ERROR_INVALID_VALUE )
			{
				DBGLOG( "%s: No new event found\n", __func__ );
			}

			else
			{
				DBGERR( "%s: Can't get new event\n", __func__ );
				return ret;
			}
		}

		else
		{
			ret = AdapterGlobalSMSWorkNewEvent( Status, ret );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't work new event\n", __func__ );
				return ret;
			}
		}
	}

	else
	{
		//������� ����������� �� SMS
		
		//�� ���� �������� - ���� � ��� ��� �� ������ �������� - �� �� � �� ����� ����������
		//�� ������� ����� SMS - ��� ������ ����� ������
		ret = AdapterPhoneCheck( Status );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't check phone for sleep\n", __func__ );
			return ret;
		}

		if( !ret )
		{
			DBGERR( "%s: No Phone to wake up - is PowerUp event\n", __func__ );
			Status->GlobalDevice_t.dmdevice_powerupstatus = DISPATCHER_LIB_EVENT__POWER_UP;
			return 0;
		}

		//������� �������� SMS � ��� ������ �����������
		Status->NoIncomingSMSCounter++;
		DBGERR( "%s: WakeUp but no incoming SMS at %08x cicle\n", __func__, Status->NoIncomingSMSCounter );

		if( Status->NoIncomingSMSCounter > 0x00000020 ) //TODO - ���������
		{
			DBGERR( "%s: Device power up is 'WakeUp' but no incoming SMS detected\n", __func__ );
			INFO_LOG( 1, "Not Incoming SMS detected" );

			//TODO - ������� ���, ��� �� �� ��������� SMS � ���, ��� �� ���������� �� �� ��� SMS
			Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature &= ~DM_DM_ENABLE_INCOMMING_SMS_SLEEP;

			ret = AdapterSleepGo( Status, 0 );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't sleep with no incoming SMS detected\n", __func__ );
				return ret;
			}

		}
	}

	return 0;
}

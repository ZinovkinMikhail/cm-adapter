//������� ���������� ������������� ������ ��������� ��������

//���������� �������
#include <string.h>
#include <signal.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <inttypes.h>

//������������ �������
#include <axis_error.h>
#include <axis_keyboard.h>
#include <axis_string.h>
#include <axis_process.h>


#include <dispatcher_lib/dispatcher_lib_resource.h>
#include <dispatcher_lib/settings.h>
#include <dispatcher_lib/network.h>
#include <dispatcher_lib/printf.h>
#include <dispatcher_lib/requests.h>
#include <dispatcher_lib/system.h>

#include <dmxxx_lib/dmxxx_calls.h>

//��������� �������
#include <adapter/acc.h>
#include <adapter/global.h>
#include <adapter/helper.h>
#include <adapter/resource.h>
#include <adapter/recorder.h>
#include <adapter/settings.h>
#include <adapter/httpd.h>
#include <adapter/network.h>
#include <adapter/hardware.h>
#include <adapter/timer.h>
#include <adapter/debug.h>
#include <adapter/system.h>
#include <adapter/leds.h>
#include <adapter/dmdevice.h>
#include <adapter/power.h>
#include <adapter/log.h>

#include <adapter/usb.h>
#include <adapter/diagnostic.h>
#include <adapter/interface.h>
#include <adapter/sms.h>


//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif

#define GLOBAL_BREAK_LINE { Status->GlobalInitBreakLine = __LINE__; }

/**
 * @brief ������� ���������� ������������� ��������
 *
 * @param iResetValueFlag - ���� ���� ��� ���� �������� ��� ��������� � �������� �� ���������
 * @param iInertactiveFlag - ���� ���� ��� �� �������� � ������������� ������
 * @param AdapterDevice - ��� ����������
 * @param iRescueMode - ���� �������� � ����� ��������������
 * @param ilogging - ���� ��������������� �����������
 * @param iNoDaemon - ���� ����, ��� �� ����� ���������� � ����� ������
 * @param iConsole - ���� ����, ��� ����� �������� ������� ��� �����������
 * @param iNoInterface - ���� ����, ��� �� ����� ��������� ���������
 * @param Status - ��������� �� ��������� ����������� �������
 * @param KernelDebugLevel - ������� ������� � ���� ��� ������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 */
int AdapterGlobalInit( int iResetValueFlag, int iInertactiveFlag, int AdapterDevice,
					   int iRescueMode, int ilogging, int iNoDaemon, int iConsole, int iNoInterface,
					   AdapterGlobalStatusStruct_t *Status, int KernelDebugLevel )
{
	int err = AXIS_NO_ERROR; //��� ��������
	int diagret = AXIS_NO_ERROR;
	int reinittimeount = 1;

	if ( !Status )
	{
		DBGERR ( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	AdapterSystemInitSignals( );

	if( !AdapterDevice )
	{
		DBGERR ( "%s: No adapter device found\n", __func__ );
		AdapterPrintUsage( );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	DBGLOG ( DISPATCHER_LIB_DEBUG__START );

	GLOBAL_BREAK_LINE;

	Status->AdapterNeedReboot = 0;
	
	if( AdapterSystemConfigure( Status, AdapterDevice, iInertactiveFlag, iRescueMode, iResetValueFlag, ilogging,
													iInertactiveFlag || iConsole, iNoInterface, KernelDebugLevel ) )
	{
		DBGERR( "%s: It wasn't possible to configure adapter for work\n", __func__ );
		return -1;
	}

	GLOBAL_BREAK_LINE;

	//� ��� �� ���� ilogging - ������� �� �������� � � ������� ���� �� 5 �����
	if( Status->ilogging )
	{
		if( AdapterLogInit( Status ) < 0 )
		{
			DBGERR( "%s: It wasn't possible to init logger. Temporary disabled it\n", __func__ );
			//���������� ������ ������� ��� �� ���� �� ������
			//�� ����� ������ ����� ��������� ������� ����� ������� �������
			//� ���������� ����������, � ����������� ��� ��� � ��� ��� ������ �������� � ����������
			//� ����� ������ - ������ ����� �� ����� � ���������� ��������
			Status->ilogging = 0; //�������� �������� ������ ������������, ����� ���������� ��� ��������
		}

		else
		{
			INFO_LOG( 0, "Adapter loging configuration" );
		}
	}
	
	INFO_LOG( 0, "Adapter system configuration" ); //��� ����������, � ������ �������, �� ����� ��� �� ���������������
	GLOBAL_BREAK_LINE;

	//�������������� SMS ���������
	err = AdapterInitSmsInterface( &Status->SMSControl );
	if( err < 0 )
	{
		DBGERR( "%s: It wasnt possible to init sms interface\n", __func__ );
		INFO_LOG( 1, "Adapter SMS configuration" );
		goto close;
	}

	INFO_LOG( 0, "Adapter SMS configuration" );
	GLOBAL_BREAK_LINE;

	err = DispatcherLibEventInit ( &Status->ExchangeEvents.Events );
	if( err < 0 )
	{
		DBGERR ( "%s : It wasn't possible to init events struct \n'", __func__ );
		INFO_LOG( 1, "Adapter event configuration" );
		goto close;
	}

	INFO_LOG( 0, "Adapter event configuration" );
	GLOBAL_BREAK_LINE;

	//�������������� ����������
	err = AdapterGlobalDM365Init( Status );
	if( err < 0 )
	{
		DBGERR ( "%s: It wasn't possible to init hardware\n", __func__ );
		INFO_LOG( 1, "Adapter device configuration" );
		goto close;
	}

	INFO_LOG( 0, "Adapter device configuration" );
	GLOBAL_BREAK_LINE;

	err = AdapterSystemDaemonPrepare( &Status->GlobalDevice_t, iInertactiveFlag || iConsole, iNoDaemon );

	INFO_LOG( err, "Adapter daemon configuration" );

	if( err < 0 )
	{
		goto close;
	}

	GLOBAL_BREAK_LINE;

	//��� ������� �� ������ ������� ��� ������
	if( !iInertactiveFlag && !iConsole )
	{
		err = AdapterDmDeviceInit ( &Status->GlobalDevice_t );
		if( err < 0 )
		{
			DBGERR ( "%s: It wasn't possible to reinit hardware for daemon\n", __func__ );
			INFO_LOG( 1, "Adapter daemon hardware configuration" );
			goto close;
		}
	}

	GLOBAL_BREAK_LINE;

	//����� ��������� ��������� ��� ����� ����������� � ����� ������
	err = AdapterInterfaceInit( &Status->GlobalDevice_t, Status->flag_no_interface );

	INFO_LOG( err < 0 ? 1 : 0, "Interface init" );

	if( err < 0 )
	{
		DBGERR( "%s: It wasn't possible to init user interface\n", __func__ );
		goto close;
	}

	GLOBAL_BREAK_LINE;

	if(Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature & DM_DM_WORK_SEPARATE ||
		Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature & DM_DM_RECORDER_NOT_RECORDER
	)
	{
		//GetHuyZnaetOtkudaSeriynik();
		err = AdapterDmDeviceGetSerialNumber( Status );
		if( err < 0 )
		{
			DBGERR( "%s:It wasn't possible to get serial number\n", __func__ );
			INFO_LOG( 1, "Adapter SN configuration" );
			goto close;
		}
	}
	else
	{

		if( Status->Recorder.recorderscount > 0 && Status->Recorder.next )
		{

// 			if(AdapterRecorderSnGet(Status->Recorder.next) < 0)
// 			{
// 				DBGERR("%s:It wasn't possible to get sn from recorder\n",__func__);
// 				goto close;
// 			}

			sprintf( Status->CurrentNetwork.SN, "%016"PRIX64, Status->Recorder.next->sn );
			Status->CurrentNetwork.SN[ sizeof( Status->CurrentNetwork.SN ) - 1 ] = 0x00;
		}
	}

	INFO_LOG( 0, "Adapter SN configuration" );
	GLOBAL_BREAK_LINE;

	if ( Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature & DM_DM_ENABLE_DIAGNOSTIC )
	{
		diagret = AdapterLoadDiagnostic ( &Status->GlobalDevice_t, Status );
		if( diagret < 0 )
		{
			DBGERR ( "%s: It wasn't possible to load diagnostic info\n", __func__ );
		}
		else
		{
			Status->AdapterDiagnostic.starts_total++;

			diagret = AdapterSaveDiagnostic ( &Status->GlobalDevice_t, Status );
			if( diagret < 0 )
			{
				DBGERR ( "%s: It wasn't possible to save diagnostic info\n", __func__ );
			}
		}

		INFO_LOG( diagret, "Adapter diagnostic configuration" );
		GLOBAL_BREAK_LINE;
	}
	
	//��� ������ �� �� ����� ���������� � ������� ��� ���������� �������
	//� ���������� ����������, ���� ������ ��������, �� � ������ �� ����������
	//������ ��� ��, ���� ���, �� �������� ��� ��� ������������ � ��� �������� �� �������
	//� ���� �� � ������ ����������, �� ��� ���� ���������� ��� �������, ��� ������ �� ����������
	//�� �������, ��� ����������� �� ������� � ����� ����� ������
	if( Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature & DM_DM_ENABLE_USER_LOG )
	{
		//������� ������ ������������ ������ - � ���� ��� �� �� ��������� - ������ �� ������!!!
		Status->ilogging = 1;

		if( Status->LogRegistration.hen )
		{
			DBGLOG( "%s: User log daemon already initing\n", __func__ );
		}

		else
		{
			if( AdapterLogInit( Status ) < 0 )
			{
				DBGERR( "%s: It wasn't possible to init logger tow\n", __func__ );
				INFO_LOG( 1, "Adapter loging configuration" );
				Status->ilogging = 0; //������ �� ������ ����� �����������
			}

			else
			{
				INFO_LOG( 0, "Adapter loging configuration" );
				GLOBAL_BREAK_LINE;
			}
			
			//���� �� ������ ����, ������ ����� ���� �� ��������� ��� ���������
			//� ������ ��� ���� ������������
			INFO_LOG( 0, "Adapter system configuration" );
			INFO_LOG( 0, "Adapter signal configuration" );
			INFO_LOG( 0, "Adapter SMS configuration" );
			INFO_LOG( 0, "Adapter device configuration" );
			INFO_LOG( 0, "Adapter daemon configuration" );
			INFO_LOG( 0, "Adapter SN configuration" );
			if ( Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature & DM_DM_ENABLE_DIAGNOSTIC )
				INFO_LOG( diagret, "Adapter diagnostic configuration" );
		}
	}

	else if( Status->ilogging && Status->LogRegistration.hen )
	{
		//�� ���������������� �� ������ ������� - �������
		DBGLOG( "%s: Hardware not support user log. Deinit it\n", __func__ );
		Status->ilogging = 0;

		AdapterLogDeInit( Status );
		INFO_LOG( 0, "Adapter loging stop" );
		GLOBAL_BREAK_LINE;
	}

reinitrecorder:

	//����������� ����������� ���������� �������� � ���������� ����������
	err = AdapterGlobalCanWorkWite( Status );
	if( err < 0 )
	{
		DBGERR ( "%s: It wasn't possible to get devive work status\n", __func__ );
		INFO_LOG( 1, "Device not ready" );
		goto close;
	}

	GLOBAL_BREAK_LINE;

	if ( reinittimeount > 1 )
	{
		err = AdapterUsbReSwitch ( &Status->GlobalDevice_t );
		if( err < 0 )
		{
			DBGERR( "%s: Can't reswitch USB\n", __func__ );
			INFO_LOG( 1, "Reswitch USB" );
			goto close;
		}
	}

	GLOBAL_BREAK_LINE;

	err = AdapterRecorderInit ( Status, &Status->GlobalDevice_t, &Status->Recorder );
	if( err < 0 )
	{
		DBGERR ( "%s: It wasn't possible to init recorder\n", __func__ );
		AdapterLedHeartBeatOff( Status ); //TODO - �� ������� ������ ��������!
		INFO_LOG( 1, "USB recorder configuration" );
		goto close;
	}

	INFO_LOG( 0, "USB recorder configuration" );
	GLOBAL_BREAK_LINE;

	//�������� ������� ���������� ������������ ����������� ��� ��� � ����������
	err = AdapterFirmwareCheckInitInRecorder( Status, &Status->Recorder );
	if( err < 0 )
	{
		DBGERR( "%s: It wasn't possible to check firmware from recorder\n", __func__ );
		INFO_LOG( 1, "New firmware check" );
	}

	//����� ����� ���������� ����� �������� - ��� ��� ����������� ���� ����
	err = AdapterTimerGetStatusFlags( Status );
	if( err < 0 )
	{
		DBGERR("%s:It wasn't possible to get timer status flags\n",__func__);
		INFO_LOG( 1, "Read timer status flag" );
		goto close;
	}

	err = AdapterSettingsAllLoad( iResetValueFlag, Status );
	if( err < 0 )
	{
		INFO_LOG( 1, "Adapter read settings" );

		DBGERR( "%s:It wasn't possible to get settings Reinit timeout = %i\n", __func__, reinittimeount );
		AdapterRecorderDeInit( &Status->GlobalDevice_t, &Status->Recorder );

		if ( reinittimeount < 20 )
		{
			reinittimeount += 3;
			AdapterLedHeartBeatOff( Status ); //TODO - �� ������� �� ���� ��������� - ��� ��������???
			AdapterLedHardwareBlink( Status );
			goto reinitrecorder;
		}
		AdapterLedHeartBeatOff( Status ); //TODO - �� ������� ������ ��������� - ��� ��������??
		goto close;
	}

	INFO_LOG( 0, "Adapter read settings" );
	GLOBAL_BREAK_LINE;
	
	DBGLOG("%s: %i HERE\n",__func__,__LINE__);

	err = AdapterAccGetStatus( Status );
	if( err < 0 )
	{
		INFO_LOG( 1, "Adapter read ACC status" );
		DBGERR( "%s: It wasn't possible to get Acc Status Ignore\n", __func__ );
	}
	else
	{
		INFO_LOG( 0, "Adapter read ACC status" );
	}
		
	DBGLOG("%s: %i HERE\n",__func__,__LINE__);
	GLOBAL_BREAK_LINE;

	if ( Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature & DM_DM_WORK_SEPARATE )
	{
		//GetHuyZnaetOtkudaSeriynik();
		err = AdapterDmDeviceGetSerialNumber( Status );
		if( err < 0 )
		{
			DBGERR ( "%s: It wasn't possible to get serial number\n", __func__ );
			INFO_LOG( 1, "Adapter SN configuration" );
			goto close;
		}
	}
	else
	{
		if ( Status->Recorder.recorderscount > 0 && Status->Recorder.next )
		{

			sprintf ( Status->CurrentNetwork.SN, "%016"PRIX64, Status->Recorder.next->sn );
			Status->CurrentNetwork.SN[ sizeof ( Status->CurrentNetwork.SN ) - 1 ] = 0x00;
		}
	}
	
	INFO_LOG( 0, "Adapter SN configuration" );
	GLOBAL_BREAK_LINE;

	//� ��� ���� ���������� � ��� SN, �� ���� � ��� ���� ���������!!!
	//�� �� ������ ��� �� ��� ������������ ��� �������� - � ��� �����
	//�� ������ ����� �� ����� ����� ���������� ������ ������� � ������� �������
	if( Status->GlobalDevice_t.dmdev_interface.dmctrl_ipresent &&
		Status->Recorder.recorderscount > 0 && Status->Recorder.next &&
		Status->Recorder.next->recorder_data.recordersettings_ex )
	{

		//NOTE - ���� �� �� �������� ����� ��� ���������� - ��� ��� ����� ������� ��������
		Status->InterfaceSleep = AdapterRecorderGetInterfaceSleepTimeFistRecorder( &Status->Recorder );
		DBGLOG( "%s: Intrafce enable. Fist recorder interface sleep = %d\n", __func__, Status->InterfaceSleep );

		//�� ������ ������������� ����� � ������ ������ ����������
		//� ��� �� �������� �������, ���� ������� ��������� �� ����
		if( !(Status->GlobalDevice_t.dmdev_interface.dmctrl_status & DM_INTERFACE_STATUS_IN_SLEEP) )
		{
			int ret = AdapterInterfaceLcdOn( Status, iNoInterface );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't set LCD on\n", __func__ );
			}

			INFO_LOG( ret, "interface LCD configuration" );
		}

	}

	GLOBAL_BREAK_LINE;

	DBGLOG("%s: %i HERE\n",__func__,__LINE__);

	//����� �������� �������� ��� ���� ��������� �������,�� �� ������ ��� ����� ��� �������� ����
	//������ ���
	err = AdapterPowerControl( &Status->GlobalDevice_t, &Status->AdapterSettings );
	if( err < 0 )
	{
		DBGERR ( "%s:It wasn't possible to power control\n", __func__ );
		INFO_LOG( 1, "Adapter power control" );
		goto close;
	}

	INFO_LOG( 0, "Adapter power control" );
	GLOBAL_BREAK_LINE;

	//������� ���� ���� ���������!!!

	err = AdapterNetworkInit( Status, 0 );
	if( err < 0 )
	{
		DBGERR ( "%s: Can't init network adapter\n", __func__ );
		INFO_LOG( 1, "Adapter network configuration" );
		goto close;
	}
	DBGLOG("%s: %i HERE\n",__func__,__LINE__);

	INFO_LOG( 0, "Adapter network configuration" );
	GLOBAL_BREAK_LINE;

	err = AdapterHttpdInit( Status );
	if( err < 0 )
	{
		DBGERR ( "%s: It wasn't possible to init httpd server\n", __func__ );
		INFO_LOG( 1, "Adapter HTTPd configuration" );
		goto close;
	}

	INFO_LOG( 0, "Adapter HTTPd configuration" );
	GLOBAL_BREAK_LINE;

	err = AdapterLedControlSettings( Status );
	if( err < 0 )
	{
		DBGERR( "%s: Can't init led control\n", __func__ );
		INFO_LOG( 1, "Adapter led control configuration" );
		goto close;
	}

	INFO_LOG( 0, "Adapter led control configuration" );
	GLOBAL_BREAK_LINE;

	err = AdapterTimerWork( Status );
	if( err < 0 )
	{
		DBGERR ( "%s: It wasn't possible to work timer\n", __func__ );
		INFO_LOG( 1, "Adapter timer configuration" );
		goto close;
	}

	INFO_LOG( 0, "Adapter timer configuration" );
	GLOBAL_BREAK_LINE;

	err = AdapterGlobalSensorWork( Status );
	if( err < 0 )
	{
		DBGERR ( "%s : It wasn't possible to init events  \n'", __func__ );
		INFO_LOG( 1, "Adapter sensor configuration" );
		goto close;
	}

	INFO_LOG( 0, "Adapter sensor configuration" );
	GLOBAL_BREAK_LINE;

	err = AdapterGlobalOutputSensorPowerWork( Status );
	if( err < 0 )
	{
		DBGERR ( "%s: It wasn't possible to work io\n", __func__ );
		INFO_LOG( 1, "Adapter outup configuration" );
		goto close;
	}

	INFO_LOG( 0, "Adapter outup configuration" );
	GLOBAL_BREAK_LINE;

	err = AdapterGlobalPowerUpEvent ( Status );
	if( err < 0 )
	{
		DBGERR ( "%s: It Wan't possible to add power Up event\n", __func__ );
		INFO_LOG( 1, "Adapter powerup configuration" );
		goto close;
	}

	INFO_LOG( 0, "Adapter powerup configuration" );
	GLOBAL_BREAK_LINE;

	if ( Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature & DM_DM_ENABLE_DIAGNOSTIC )
	{
		Status->AdapterDiagnostic.starts_good++;

		if ( AdapterSaveDiagnostic ( &Status->GlobalDevice_t, Status ) < 0 )
		{
			DBGERR ( "%s: It wasn't possible to load diagnostic info\n", __func__ );
		}
	}

	AdapterRecorderResetErrors(Status);

	Status->GlobalPID = axis_getpid();

	GLOBAL_BREAK_LINE;

	Status->LastInterfaceQuire = time ( NULL );

	DBGLOG ( DISPATCHER_LIB_DEBUG__DONE );

	AdapterLedHeartBeatOff( Status ); //TODO - �� ������� �� ���� ��������� - ��� ��������???

	if( Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature & DM_DM_ENABLE_DYNAMIC_NET )
	{
		//�� �������� � ����������� ������������� �������� - ������
		//���� �������� ��� ������� �� ������ ������ �� ����� �������
		//��� �� ������ ����� ���������������� - ���� � ���������� �� ����� ����, �� �� � �� ��������
		AdapterLedUSBNoModemsOn( Status );
		Status->NoModemFoundCount = 1; //����� �������, ��� ������� ���, ������� - ��������

		//�� �������� ������� �� ������� � ���������� ����������
		if( iDMXXXUsbGetNoModemCounter( &Status->GlobalDevice_t ) )
		{
			Status->NoModemFoundCountCheck = DMXXXUsbGetNoModemCounter( &Status->GlobalDevice_t );
			if( !Status->NoModemFoundCountCheck )
			{
				Status->NoModemFoundCountCheck = 8; //TODO - ���������!!!
			}
		}
		else
		{
			Status->NoModemFoundCountCheck = 8; //TODO - ���������!!!
		}
	}
	else
	{
		AdapterLedUSBNoModemsOff( Status );
	}

	INFO_LOG( 0, "Adapter global configuration" );
	GLOBAL_BREAK_LINE;

	return err;

close:

	//����� ������� ��� �� ��������� ��� ����
	if ( Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature & DM_DM_ENABLE_DIAGNOSTIC )
	{

		Status->AdapterDiagnostic.starts_errored++;
		if ( AdapterSaveDiagnostic ( &Status->GlobalDevice_t, Status ) < 0 )
		{
			DBGERR ( "%s: It wasn't possible to load diagnostic info\n", __func__ );
		}
	}

	if( Status->GlobalConsole > 0 )
	{
		keyboard_close ( Status->GlobalConsole );
	}

	return err;
}
 

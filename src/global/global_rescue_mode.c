//������� �������� �������� � ����� �������������� ������� ��������


//�����: ��������� ����� ��� "����"
//�����: i.podkolzin@ross-jsc.ru
//������: 2010.09.15 18:50

//���������� �������
#include <string.h>

//������������ �������
#include <axis_error.h>
#include <axis_dhcp_client.h>
#include <axis_pppoe.h>
#include <axis_pptp.h>


#include <dispatcher_lib/dispatcher_lib_resource.h>
#include <dispatcher_lib/monitoring.h>
#include <dispatcher_lib/settings.h>
//��������� �������
#include <adapter/global.h>
#include <adapter/helper.h>
#include <adapter/resource.h>
#include <adapter/httpd.h>
#include <adapter/network.h>
#include <adapter/leds.h>
#include <adapter/debug.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
//#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif


//������� �������� �������� � ����� �������
//�������� �� ���������
void AdapterGlobalSetRescueMode( void )
{

	int ret;

	AdapterGlobalStatusStruct_t *Status = AdapterGlobalGetStatus();


	//������� ������� ��������� �������� ����� ������ ������ �����
	DBGLOG("%s: Try set rescue mode for device\n",__func__);

	//������ � ��� ���� ������ ��� ��� �� �� ���� ������

	//������ ��� �� � ������
	if(Status->RescueMode == 0)
	{
		Status->RescueMode = 1;
		//� ���� �� � ������ - �� ����� ��� �� ��� ������!!!
		Status->icangosleep = 0;
	}
	else
	{
		//Status->RescueMode = 0;
		return;
	}

	//������� ��������� ��� ������� ������� � ����
	AdapterAllNetworksStop( Status );

	//��������� http �������
	ret = AdapterHTTPdStop( &Status->HTTPDMain );
	if( ret < 0 )
	{
		DBGERR("%s: Can't stop main httpd server! %d\n",__func__, ret);
	}

	ret = AdapterHTTPdStop( &Status->HTTPDSec );
	if( ret < 0 )
	{
		DBGERR("%s: Can't stop second httpd server! %d\n",__func__, ret);
	}

	//� ����� �������� ���� ������
	//��������� �������������� ��������� - ���������� �� ���������
	Status->CurrentNetwork.ClientMode 	=  0;
	
	Status->CurrentNetwork.ControlPort =\
				AXIS_SOFTADAPTER_CONTROL_PORT__DEFAULT;
				
	Status->CurrentNetwork.UploadPort =\
				AXIS_SOFTADAPTER_UPLOAD_PORT__DEFAULT;
				
	Status->CurrentNetwork.MonitoringPort =\
				AXIS_SOFTADAPTER_MONITORING_PORT__DEFAULT;
;
	memcpy( Status->CurrentNetwork.Login,
			AXIS_SOFTADAPTER_ADAPTER_USER__DEFAULT,
			sizeof( Status->CurrentNetwork.Login ) );
	
	memcpy( Status->CurrentNetwork.Passwd,
			AXIS_SOFTADAPTER_ADAPTER_PASSWD__DEFAULT,
			sizeof( Status->CurrentNetwork.Passwd ) );

	//������ ������������������ ����
	ret = AdapterNetworkInit( Status, Status->RescueMode );
	if( ret < 0 )
	{
		DBGERR("%s: Can't init network %d\n",__func__, ret);
	}

	//������ �������� �� ����� ������� �������
	//�� ��� HTTP �������� ����� ������������
	ret = AdapterHTTPdStart( &Status->HTTPDMain,  //TODO - � ������� �������� ��� ��������� ���� ��� ���������
					&Status->CurrentNetwork,
					Status->AdapterDevice,
					&Status->LastInterfaceQuire,
					0);
	if( ret < 0 )
	{
		DBGERR("%s: Can't start main httpd server! %d\n",__func__, ret);
	}

	//��������� ��� �� � ������
	AdapterPrintConsoleFast( Status );

	AdapterLedRescueModeOn( Status );
	INFO_LOG( 0, "Rescue mode" );
}

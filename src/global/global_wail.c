//��� ����� �������� ��� ���� � ������� �� � ����� ��������

//���������� �������
#ifndef YOCTO	//TODO - ��� �����-4� ��� Yocto - � ��� ��� �������� �������
#include <execinfo.h>
#endif

#include <signal.h>
#include <errno.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>

//������������ �������
#include <axis_error.h>

#include <dispatcher_lib/network.h>
#include <dispatcher_lib/dispatcher_lib_resource.h>


//��������� �������
#include <adapter/global.h>
#include <adapter/helper.h>
#include <adapter/resource.h>
#include <adapter/txt_files.h>
#include <adapter/dispatcher.h>
#include <adapter/recorder.h>
#include <adapter/leds.h>
#include <adapter/interface.h>
#include <adapter/debug.h>
#include <adapter/interface.h>
#include <adapter/usb.h>
#include <adapter/diagnostic.h>
#include <adapter/new_command.h>
#include <adapter/network.h>
#include <adapter/lte.h>
#include <adapter/power.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>

#else
#define DBGLOG( ... )
#endif

#if 1
    #include <sys/select.h>
    /* According to earlier standards */
    #include <sys/time.h>
    #include <sys/types.h>
    #include <unistd.h>
#endif

#if 1
    #include <sys/types.h>
    #include <sys/stat.h>
    #include <fcntl.h>
#endif

//���������� ��������� ������� - ��� ����� �������� ��� �������������
static AdapterGlobalStatusStruct_t					AdapterGlobalStatus;


//������� ������� ���������� ��������� �� ���������� ���������
AdapterGlobalStatusStruct_t	*AdapterGlobalGetStatus ( void )
{
	return &AdapterGlobalStatus;
}

#ifdef YOCTO
static inline void BacktracePrint( void )
{
	DBGERR( "%s: No backtrace symbols for YOCTO project\n", __func__ );
}
#else

static inline void BacktracePrint( void )
{
	int j, nptrs;
	int size = 20; // ������� backtrace, 10-�� ������, � �����

	void *buffer[size];
	char **strings;
	int btfd = open( "/tmp/logdev/crash.log", O_WRONLY |
					 O_TRUNC |
					 O_CREAT,
					 S_IRUSR |
					 S_IWUSR |
					 S_IRGRP |
					 S_IWGRP );

	// ������� ������� ��������
	nptrs = backtrace( buffer, size );

	if( !nptrs )
	{
		DBGERR( "%s: No backtrace symbols detected - try set debug mode from main project (Enable GDB)\n", __func__ );
	}

	// ��� ��� ����������
	strings = backtrace_symbols( buffer, nptrs );

	if( strings == NULL )
	{
		perror( "backtrace_symbols" );
	}

	fprintf( stderr, "\n" );
	fprintf( stderr, "----------backtrace start----------\n" );

	// ������� � ���� backtrace
	if( btfd > 0 )
	{
		backtrace_symbols_fd( buffer, nptrs, btfd );
	}

	// �� � �������� ���� ������� � �������� �������,
	// ��� ������� ���� ������� ������������ �����
	// �� �������� ��� ���������,
	// �. �. ��� �������� ( ���� � ���������� ������ libc )
	for( j = ( nptrs - 1 ); j > 1 ; j-- )
	{
		fprintf( stderr, "%s\n", strings[j] );
	}

	free( strings );

	if( btfd > 0 )
	{
		close( btfd );
	}

	fprintf( stderr, "----------backtrace end----------\n" );
	fprintf( stderr, "\n" );

}

#endif

//��������� ���������� ���������� ������
//�� ���������� ����� ������ ����� ������� ��� �������
//� ���� ����� ��� ��� ���� ��������� ���������

//������� ��������� ��������
//����:
// sig - ���������� ����� ��������������� �������
void AdapterGlobalSignalHendler ( int sig )
{
	DBGLOG ( "%s: %d\n", __func__, sig );

	//��� ����� ������� ��������� ���������� ���
	//��� �������

	switch ( sig )
	{
		case SIGINT:
		{
			DBGERR ( "%s: ------ User terminated\n", __func__ );
			INFO_LOG( 0, "User terminated" );
			break;
		}

		case SIGILL:
		{
			DBGERR ( "%s: ------ Incorrect CPU instruction\n", __func__ );
			INFO_LOG( 1, "Incorrect CPU instruction" );
			BacktracePrint();
			break;
		}

		case SIGABRT:
		{
			DBGERR ( "%s: ------ Abort\n", __func__ );
			break;
		}

		case SIGSEGV:
		{

			INFO_LOG( 1, "Memory error" );
			
			DBGERR ( "%s: ------ Memory Error\n", __func__ );

			BacktracePrint();

			if ( AdapterGlobalStatus.GlobalDevice_t.dmdev_dm3xx.dmctrl_feature
													& DM_DM_ENABLE_DIAGNOSTIC )
			{
				AdapterGlobalStatus.AdapterDiagnostic.memerrors++;
			}
			
			break;
		}

		case SIGTERM:
		{
			DBGERR ( "%s: ------ Terminated\n", __func__ );
			break;
		}

		case SIGPIPE:
		{
			DBGERR ( "%s: ------ Broken PIPE\n", __func__ );
			return;
		}

		case SIGBUS:
		{
			DBGERR( "%s: ------ Bus Error\n", __func__ );
			return;
		}

		case SIGUSR1:
		{
			return;
		}

		case SIGUSR2:
		{
			return;
		}

		default:
		{
			DBGERR ( "%s: ------- Unknown signal = %d\n", __func__,
																	sig );
			break;
		}
	}

	if( AdapterGlobalStatus.GlobalDevice_t.dmdev_dm3xx.dmctrl_feature &
			DM_DM_ENABLE_DIAGNOSTIC )
	{
		AdapterGlobalStatus.AdapterDiagnostic.signals_catched++;

		if( AdapterSaveDiagnostic( &AdapterGlobalStatus.GlobalDevice_t,
								   &AdapterGlobalStatus ) < 0 )
		{
			DBGERR( "%s: Failed: save diagnostic info\n", __func__ );
		}
	}


	//���������� �������� �� ������ ��� �� ���������
	//�� ��������� ������� ��� �� ��� �� �� ���������������� �������

	if(AdapterGlobalStatus.GlobalPID == axis_getpid())
	{
		AdapterGlobalClose ( &AdapterGlobalStatus );
		exit ( -999 );
	}
	else
	{
		pthread_exit(NULL);
	}
}

int AdapterGlobalWileSetHendel ( int dev, fd_set *rfds, int maxsel )
{


	if( dev >= 0 )
	{
		FD_SET( dev, rfds );

		if( dev > maxsel )
		{
			FD_SET ( dev, rfds );

			if ( dev > maxsel )
				{
					maxsel = dev;
				}
		}
	}
	return maxsel;
}

static int adapter_global_wail_check_recorder_info_hendel ( AdapterGlobalStatusStruct_t *Status )
{

	if( !Status )
	{
		DBGERR( "%s: Invalid Argument\n", __func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	if( Status->GlobalDevice_t.dmdev_recorder.dmctrl_ipresent &&
	                Status->GlobalDevice_t.dmdev_recorder.dmctrl_feature & DM_RECORDER_USB_INFO )
	{
		if ( Status->Recorder.hen > 0 )
		{
			return 0;
		}
	}

	return 1;
}

#include <string.h>
#include <stdio.h>

#define GLOBAL_BREAK_LINE { AdapterGlobalStatus.GlobalWhileBreakLine = __LINE__; }

/**
 * @brief - ������� ����������� ����� ��������
 *
 * @param iResetValueFlag - ���� ����, ��� ���� �������� ��� ��������� � �������� �� ���������
 * @param iInertactiveFlag - ���� ���� ��� �� �������� � ������������� ������
 * @param AdapterDevice - ��� ����������
 * @param iRescueMode - ���� �������� � ����� ��������������
 * @param ilogging - ���� ��������������� �����������
 * @param iNoDaemon - ���� ������� �������� � ����� ������
 * @param iConsole - ���� �������� ����� �� �������
 * @param iNoInterface - ���� �� ��������� ���������
 * @param FileName - ��� ���������� ����� ��� ���������, ����� ���� NULL
 * @param KernelDebugLevel - ������� ������� ���� ��� ������
 * @return int - 0 ���������� ���� ���������� �������, ����� 0 � ������ ������
 */
int AdapterGlobalWail( int iResetValueFlag, int iInertactiveFlag, int AdapterDevice,
					   int iRescueMode, int ilogging, int iNoDaemon, int iConsole, int iNoInterface,
					   const char *FileName, int KernelDebugLevel )
{

	int err = AXIS_NO_ERROR;

	fd_set rfds;	//��������� ��� �������

	struct timeval tv;	//��������� ��������
	int retval;		//�������
	int max_sel	= 0;	//������������ ������ ��� �������
	int Interface;							//���� ������� ���������� � �������
	dmdev_user_interface_t *InterfacePriv;	//��������� ������ ����������, ����� ���� NULL

	time_t OldTime = 0;
	time_t NewTime = 0;
	uint32_t SecCount = 0;

	//������� ��� ������
	memset(&AdapterGlobalStatus, 0 ,sizeof(AdapterGlobalStatus));
	AdapterGlobalStatus.GlobalPID = axis_getpid();

	//������� �������������� ��� ��� �����

	err = AdapterGlobalInit(
								iResetValueFlag,
								iInertactiveFlag,
								AdapterDevice,
								iRescueMode,
								ilogging,
								iNoDaemon,
								iConsole,
								iNoInterface,
								&AdapterGlobalStatus,
								KernelDebugLevel );

	if( err == -AXIS_ERROR_BUSY_DEVICE )
	{
		goto close; //��� ������� ����� �� ���������
	}

	if( err < 0 )
	{
		DBGERR( "%s: Can't init global\n", __func__ );
		return err;
	}

	GLOBAL_BREAK_LINE;
	
	if( FileName )
	{
		//���� ��� ����� - ���� ��� ����������
		err = AdapterFileTxtDoIt( FileName, fileno( stdout ), &AdapterGlobalStatus );

		if( err < 0 )
		{
			//���� ��� ����� - ���� ��� ����������
			err = AdapterFileTxtDoIt ( FileName, fileno ( stdout ), &AdapterGlobalStatus );

			if ( err < 0 )
				{
					DBGERR ( "%s: Can't process input file\n", __func__ );
					goto close;
				}

		if ( err < 0 )
		{
				DBGERR ( "%s: Can't process input file\n", __func__ );
				goto close;
		}
	}

	}
	//���������� �� ������� �� �������� ������ ���� � ��������
	//� �� ���� ����� ������������� ����
	//�� � ����� ������ �� ����� ������������ �� ��������

	//���������� ����� ��������� ��� �������

	GLOBAL_BREAK_LINE;

	if( iInertactiveFlag )
	{
		max_sel = AdapterGlobalStatus.GlobalConsole + 1;
		//����� ��� ��� �������� ���������� ����
		AdapterPrintConsoleFast( &AdapterGlobalStatus );
	}

	//����������� ������� ���������� � ��� ������ ����� UnixSocket
	InterfacePriv = (dmdev_user_interface_t *)AdapterGlobalStatus.GlobalDevice_t.dmdev_interface.privatedata;
	Interface = AdapterGlobalStatus.GlobalDevice_t.dmdev_interface.dmctrl_ipresent &&
				InterfacePriv && (InterfacePriv->communication_type & DM_INTERFACE_CONNECTION_UNIX );

	DBGLOG ( DISPATCHER_LIB_DEBUG__START );

	//���������� �����
	tv.tv_sec = 1;
	tv.tv_usec = 0;

	//���������� ����������� ���������� ����

	while( 1 )
	{

		GLOBAL_BREAK_LINE;

		//� ������ ����� �� ���� ��������, � �� ���� �� ��� ������ ��������
		if( AdapterGlobalStatus.PowerDeviceMask )
		{
			GLOBAL_BREAK_LINE;

			//������� ���������� ���� ��� ��� ������ ��������
			if( AdapterPowerMask( &AdapterGlobalStatus ) < 0 )
			{
				DBGERR( "%s: Can't work adapter power mask\n", __func__ );
			}
		}

		GLOBAL_BREAK_LINE;

		//� �������� �� ������������� �������

		//���������� �������
		FD_ZERO ( &rfds );

		max_sel = 0;

		//���� ������� ����������


		if( iInertactiveFlag &&
		                AdapterGlobalStatus.GlobalConsole >= 0 )
		{
			//FD_SET( AdapterGlobalStatus.GlobalConsole, &rfds);
			max_sel = AdapterGlobalWileSetHendel(
			                  AdapterGlobalStatus.GlobalConsole,
			                  &rfds,
			                  max_sel );
		}

		if( AdapterGlobalStatus.GlobalDevice_t.dmdevice_hen > 0 )
		{
			max_sel = AdapterGlobalWileSetHendel(
			                  AdapterGlobalStatus.GlobalDevice_t.dmdevice_hen,
			                  &rfds,
			                  max_sel );
		}

		max_sel = AdapterNetworkSetSelectHendelPoll(&AdapterGlobalStatus,&rfds,max_sel);	
	
		if( Interface )
		{
			max_sel = AdapterInterfaceSetHendelUnixSocket( InterfacePriv, &rfds, max_sel );
		}

		if( !adapter_global_wail_check_recorder_info_hendel( &AdapterGlobalStatus ) )
		{
			max_sel = AdapterGlobalWileSetHendel(
			                  AdapterGlobalStatus.Recorder.hen,
			                  &rfds,
			                  max_sel );
		}


		max_sel++;

		//���������� ����� ������
		if( !tv.tv_sec && !tv.tv_usec )
		{
			tv.tv_sec	= 1;
			tv.tv_usec	= 0;
		}
	
		GLOBAL_BREAK_LINE;

		//�������� ������
		retval = select ( max_sel, &rfds, NULL, NULL, &tv );

		if( retval < 0 )
		{
			//������ �������
			if( errno == EINTR )
			{
				//��� �� ������ - �� ������� ������!!!
				DBGLOG( "%s: Select signal interrupt\n", __func__ );
				continue;
			}

			//������ ������� - ����� �� �����
			DBGERR( "%s: Select Error %i (%s)\n", __func__, errno, strerror( errno ) );
			LOG( "Global While Select Error\r\n" );
			err = -AXIS_ERROR_CANT_IOCTL_DEVICE;

			break;
		}

		GLOBAL_BREAK_LINE;

		NewTime = time( NULL );

		//if( !retval )
		if( NewTime != OldTime )
		{
			//���������� ������ ����������, ������� � ������� ������� � �� �������
			//����� ����� ���������� ��� ��, ��� ������� ������������� ���������

			SecCount++; //NOTE - TCount ������ ��� �� ��������, ��� ��� �� ����� ����������
						//������� ���� ��� ��� � ������� - � ��� ������� ������ � �����

			OldTime = NewTime;

			AdapterGlobalStatus.AdapterTCount++;

			AdapterGlobalWialTime ( &AdapterGlobalStatus, SecCount );

			//��� ����� ������� ����������� �������

			if( AdapterGlobalStatus.AdapterNeedReboot )
			{
				DBGLOG ( "%s: Adapter Reboot command\n", __func__ );
				
				AdapterCommandReboot( &AdapterGlobalStatus );
				//���� �� � ������ ������ �� ����� ����� � �����������
				//�� �� ������ ������� ����������
				//� ��������� ������� ��� �� �����

				//NOTE - ����� ������� ���� ��������� ������ � �����������
				sem_wait( &AdapterGlobalStatus.GlobalUsbRecorderSemaphore );
				break;
			}

			GLOBAL_BREAK_LINE;

			//��� ����� ������� ��� ��� LTE ������ ������������ ����� ���� �������
			if( AdapterGlobalStatus.AdapterNeedLTECommand &&
				AdapterGlobalStatus.GlobalDevice_t.dmdev_lte.dmctrl_icontrol )
			{
				const char *message;
				if( AdapterGlobalStatus.AdapterNeedLTECommand >= DM_NET_COMMAND_SET_BAND_AUTO &&
					AdapterGlobalStatus.AdapterNeedLTECommand <= DM_NET_COMMAND_SET_BAND_3G )
				{
					message = "LTE switch band";
				}
				else if( AdapterGlobalStatus.AdapterNeedLTECommand == DM_NET_COMMAND_CONNECT )
				{
					message = "LTE command connect";
				}
				else
				{
					DBGERR( "%s: Invalid LTE command %u\n", __func__, AdapterGlobalStatus.AdapterNeedLTECommand );
					AdapterGlobalStatus.AdapterNeedLTECommand = 0;
					message = "Invalid LTE command";
				}

				DBGLOG( "%s: %s\n", __func__, message );

				err = AdapterLTESendCommand( &AdapterGlobalStatus, AdapterGlobalStatus.AdapterNeedLTECommand );

				if( err < 0 )
				{
					DBGERR( "%s: Can't %s\n", __func__, message );
				}
				else
				{
					AdapterGlobalStatus.AdapterNeedLTECommand = 0;
				}

				INFO_LOG( err, message );
			}
		}

		GLOBAL_BREAK_LINE;

		//��������� ����������
		if( iInertactiveFlag &&
		                AdapterGlobalStatus.GlobalConsole >= 0 &&
		                FD_ISSET( AdapterGlobalStatus.GlobalConsole, &rfds ) )
		{
			//���� ������� �� ����������
			AdapterGlobalStatus.AdapterTCount++;
			err = AdapterGlobalDoConsole( &AdapterGlobalStatus );

			if( err < 0 )
			{
				DBGERR( "%s: Can't execute console command\n", __func__ );
			}
			else if( ADAPTER_KEY__USER_EXIT == err )
			{
				DBGLOG( "%s: Console command exit\n", __func__ );
				
				INFO_LOG( 0, "Console command exit" );
				
				//���� �� � ������ ������ �� ����� ����� � �����������
				//�� �� ������ ������� ����������
				//� ��������� ������� ��� �� �����
// 				AdapterGlobalClose(&AdapterGlobalStatus);
// 				sem_wait( &AdapterGlobalStatus.GlobalUsbRecorderSemaphore );

				break;
			}
		}

		GLOBAL_BREAK_LINE;

		if( AdapterNetworkWorkISSETPoll( &AdapterGlobalStatus, &rfds ) )
		{
			DBGERR( "%s: Can't work network USB polling\n", __func__ );
		}


#if 0 //Leave for debug
		if( AdapterGlobalStatus.PollingModem.fdpoll > 0 &&
		                FD_ISSET( AdapterGlobalStatus.PollingModem.fdpoll, &rfds ) )
		{

			LOG( "Usb device select EVENT\r\n" );
			AdapterGlobalStatus.AdapterTCount++;

			// 			DBGLOG("%s: %i Polling event\n",__func__,__LINE__);
			if( AdapterNetworkGetConnectionStatus( &AdapterGlobalStatus ) < 0 )
			{
				DBGERR( "%s:Can't get status exit\n", __func__ );
// 				continue;
			}

			if( AdapterGlobalStatus.Net.status.imodemreset != DM_MODEM_ENABLE_RESET )
			{
				DBGLOG( "%s: %i Reset Modem\n", __func__, __LINE__ );
				AdapterNetworkStop( &AdapterGlobalStatus );
				AdapterNetworkUsbOut(&AdapterGlobalStatus);
// 				AdapterUsbDeinitPolling(&AdapterGlobalStatus.PollingModem);
			}
			else
			{
				DBGLOG( "%s: %i Reset Poll\n", __func__, __LINE__ );
				AdapterUsbResetPoll( &AdapterGlobalStatus );
			}
		}
#endif

		GLOBAL_BREAK_LINE;

		if( AdapterGlobalStatus.GlobalDevice_t.dmdevice_hen >= 0 &&
				FD_ISSET( AdapterGlobalStatus.GlobalDevice_t.dmdevice_hen, &rfds ) )
		{
			AdapterGlobalStatus.AdapterTCount++;
			AdapterLedHardwareBlink( &AdapterGlobalStatus );
			LOG( "Hardware EventSelected\r\n" );
			DBGLOG( "%s: Event Selected\n", __func__ );

			if( AdapterGlobalDoEvent( &AdapterGlobalStatus ) < 0 )
			{
				DBGERR( "%s: Can't do global hardware event\n", __func__ );
			}
		}

		GLOBAL_BREAK_LINE;

		if ( !adapter_global_wail_check_recorder_info_hendel ( &AdapterGlobalStatus ) &&
					FD_ISSET ( AdapterGlobalStatus.Recorder.hen, &rfds ) )
		{
			AdapterGlobalStatus.AdapterTCount++;
			if ( AdapterRecorderProcessEvent ( &AdapterGlobalStatus.GlobalDevice_t, &AdapterGlobalStatus.Recorder ) )
				{
					DBGERR ( "%s:It wasn't possible to process event\n", __func__ );
					return -1;
				}
		}

		GLOBAL_BREAK_LINE;

		if( Interface )
		{
			AdapterGlobalStatus.AdapterTCount++;
			int err = AdapterInterfaceWorkUnixSockets(
									AdapterGlobalStatus.GlobalDevice_t.dmdev_interface.privatedata,
									&rfds, &AdapterGlobalStatus );

			if( err < 0 )
			{
				DBGERR( "%s: Can't work interface unit sockets\n", __func__ );
			}
		}

		GLOBAL_BREAK_LINE;

	}

	GLOBAL_BREAK_LINE;

close:

	//�� ������� �� �� �� ������ ��� ��� - �������� �����������
	if( AdapterGlobalStatus.GlobalDevice_t.dmdev_dm3xx.dmctrl_feature & DM_DM_ENABLE_DIAGNOSTIC )
	{
		if( AdapterSaveDiagnostic( &AdapterGlobalStatus.GlobalDevice_t, &AdapterGlobalStatus ) < 0 )
		{
			DBGERR( "%s: It wasn't possible to save diagnostic info\n", __func__ );
		}
	}


	//����� ������� ��� ��� �� ���������
	err = AdapterGlobalClose ( &AdapterGlobalStatus );

	if ( err < 0 )
	{
		DBGERR ( "%s: Can't close global\n", __func__ );
		return err;
	}


	DBGLOG ( DISPATCHER_LIB_DEBUG__START );

	return err;
}

//���� �� �������� ���� ��� ������� ������������
//������� �� ��������


//���������� �������
#include <signal.h>
#include <sys/wait.h>
#include <errno.h>
//������������ �������
#include <axis_error.h>


#include <dispatcher_lib/dispatcher_lib_resource.h>
#include <dispatcher_lib/network.h>
#include <dispatcher_lib/requests.h>

#include <dmxxx_lib/dmxxx_calls.h>

#include <adapter/dispatcher.h>
#include <adapter/global.h>
#include <adapter/hardware.h>
#include <adapter/timer.h>
#include <adapter/power.h>
#include <adapter/debug.h>
#include <adapter/recorder.h>
#include <adapter/interface.h>
#include <adapter/sleep.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif

//������� ��������� �������
//����:
// dev - ������ ��������� ���������� ������ � �����������
// Status - ��������� �� ��������� ������� ����������
//�������:
// 0 - ��� ������� ����������
// ����� 0 � ������ ������
int AdapterGlobalDoEvent( AdapterGlobalStatusStruct_t *Status )
{
	int ret = AXIS_NO_ERROR;

	uint32_t	eventfromdevice = 0x00000000;
	int		value = 0; //����� �������� � ����� �������
	char *string_message;
	int ToMonitoringClients = 0; //����� �� ���������� ������ ������� �� ������ �����������

	if( !Status )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}


	if( Status->GlobalDataNotSave < 0 )
	{
		DBGERR( "%s: Device not open\n", __func__ );
		return -AXIS_ERROR_DEVICE_NOT_OPEN;
	}

	DBGLOG( "%s: Texas Event\n", __func__ );

	//���������� ���� ��� �������!!!!
	ret = DMXXXEventsGet( &Status->GlobalDevice_t, &eventfromdevice , &value );

	if( ret < 0 )
	{
		DBGERR( "%s: Cant get next hardware event\n", __func__ );
		return ret;
	}

	//������ ����������
	switch( eventfromdevice )
	{

		case DISPATCHER_LIB_EVENT__NONE:
		{
			//������ �������� ������ � ������� �� ������ �� ����
			DBGLOG( "%s: Event.type = DM355_DEVICE_EVENT__NONE\n", __func__ );
			//������ �������
			DBGERR( "%s: No Event Found\n", __func__ );
			return AXIS_NO_ERROR;
		}

		case DISPATCHER_LIB_EVENT__BUTTON:
		{
			INFO_LOG( 0, "Rescue event" );
			DMXXXModeButtonDisable( &Status->GlobalDevice_t );
			AdapterGlobalSetRescueMode();
			return 0;
		}

		case DISPATCHER_LIB_EVENT__SENSOR:
		{
			DBGLOG( "%s: Event.type = DEVICE_EVENT__SENSOR %i \n", __func__ ,value);
			string_message = "Sensor event";
			Status->LastInterfaceQuire = time( NULL ); //������� �������� - ���������� ���������
			ToMonitoringClients = 1; //�� ������ ��������������� ������� �� ������ �����������
			break;
		}

		case DISPATCHER_LIB_EVENT__ADXL:
		{

			DBGLOG( "%s:Event.type: EVENT ADXL\n", __func__ );
			// ��������� ADXL � �������� ���������(��������)
// 			ret = DMXXXSensorAccelerometrEnable( &Status->GlobalDevice_t );
			string_message = "Move event";
			Status->LastInterfaceQuire = time( NULL ); //������� �������� - ���������� ���������
			ToMonitoringClients = 1; //�� ������ ��������������� ������� �� ������ �����������
			break;
		}

		case DISPATCHER_LIB_EVENT__TIMER:
		{
			//���� ������������ ���������� - ������ ������� � ����� ���������
			DBGLOG( "%s: Event.type = DEVICE_EVENT__ALARM\n", __func__ );

			ret = AdapterTimerWork( Status );

			if( ret < 0 )
			{
				DBGERR( "%s: It wasn't possible to work timer\n", __func__ );
				return -1;
			}

			return ret;
		}
		case DISPATCHER_LIB_EVENT__POWEROFF:
		{
			AdapterCommandSleep( Status );
			string_message = "Power event";
			break;
		}
		case DISPATCHER_LIB_EVENT__EXT_POWER_BAD:
		{
			int iret = 0;

			//������� - ��� �� ��� � �������� - ������������ ������ ���, ��� �� ����� ���������� ���������
			DBGLOG( "%s: Worked power event!!!\n", __func__ );

			//��� �� ��� ����� ���� �� ����� ������ - � ����������
			//TODO - ���� ������� � ��������� �������
			if( Status->GlobalDevice_t.dmdev_acc.dmctrl_feature & DM_DM_ACC_STATUS_IN_RECORDER )
			{
				DBGLOG( "%s: Acc status in recorder (new version)\n", __func__ );

				//���� � ��� ������ ��� � ���������� - �� �� � ��� ������������ � ������
				iret = AdapterRecorderGetAccStatus___( &Status->GlobalDevice_t, &Status->Recorder );

				if( iret < 0 )
				{
					DBGERR( "%s: Can't get ACC status from fist recorder\n", __func__ );
				}
			}

			//��� �� �� ����� ���� � ��������
			else if( Status->GlobalDevice_t.dmdev_acc.dmctrl_ipresent )
			{
				DBGLOG( "%s: Acc status in adapter\n", __func__ );

				iret = DMXXAccGetStatus( &Status->GlobalDevice_t, &Status->ACCStatus );

				if( iret < 0 )
				{
					DBGERR( "%s: It wasn't possible to get acc status from device\n", __func__ );
				}
			}

			if( iret < 0 )
			{
				DBGERR( "%s: Can't load new ACC status\n", __func__ );
				//������ ���������� �� � ��� �� ������� ��� ���������� ������
			}

			//����� ��������� ���������, �� �� ������� �����
			if( (time(NULL) - Status->LastPowerEvent) >= 5)
			{
				int iret = AdapterInterfaceSendCommand( Status, AXIS_ADAPTER_CMD_GET_ACC_STATE );

				if( iret < 0 )
				{
					DBGERR( "%s:%i: It wasn't possible to send event 'Get ACC' to interface\n", __func__, __LINE__ );
				}

				Status->LastPowerEvent = time(NULL);
			}

			string_message = "Power bad event";

			ToMonitoringClients = 1; //�� ������ ��������������� ������� �� ������ �����������
			break;
		}

		case DISPATCHER_LIB_EVENT_STORAGE:
		{
			DBGLOG( "%s:%i: Storage event\n", __func__, __LINE__ );

			//������� � ���, ��� � ���������� ��� ���������
			//���� ������� ������ ���� ���������� - ���������� ���������, ���
			//��� �� ���������� ������������ � ��������� ���������

			int iret = AdapterInterfaceSendCommand( Status, AXIS_INTERFACE_EVENT_STORAGE );

			if( iret < 0 )
			{
				DBGERR( "%s:%i: It wasn't possible to send event 'Storage' to interface\n", __func__, __LINE__ );
			}


			string_message = "Storage event";
			break;
		}

		case DISPATCHER_LIB_EVENT__DSP_EVENT:
		{
			int iret;

			DBGLOG( "%s:%i: Dsp event\n", __func__, __LINE__ );

			//������� � ���������� - ���� ���������� ��� ������
			//����� � ���� ����� ������ �� ������ ��� ��� �� ���� ����
			 iret = AdapterRecorderGetAllRecordersStatus( &Status->GlobalDevice_t, &Status->Recorder );

			if( iret < 0 )
			{
				DBGERR( "%s: Can't load all recorder new status\n", __func__ );
				//������ ���������� �� � ��� �� ������� ��� ���������� ������
			}

			//����� ��������� ��������� � ���, ��� ������ ��� �������, ��� �� �� ������� ���
			iret = AdapterInterfaceSendCommand( Status, AXIS_INTERFACE_EVENT_READ_STATUS_REC );

			if( iret < 0 )
			{
				DBGERR( "%s:%i: It wasn't possible to send event 'Read Status' to interface\n", __func__, __LINE__ );
			}

			string_message = "DSP event";
			break;
		}

		case DISPATCHER_LIB_EVENT_VOLUME:
		{
			DBGLOG( "%s:%i: Volume event - %d\n", __func__, __LINE__, value );

			//������� - � ���������� ��������� ��������� ��������� �� ����� �� ��� �������
			//��� ��� ������� �� ��������� - ���� ������ � ��� � ������ - � ��� ���������
			//�� ������ ����������, ��� �� �� ��������� � ���� ���������
			int iret = AdapterInterfaceSendCommandAndValue( Status, AXIS_INTERFACE_EVENT_VOLUME, value );

			if( iret < 0 )
			{
				DBGERR( "%s:%i: It wasn't possible to send event 'Volume' to interface\n", __func__, __LINE__ );
			}


			return AXIS_NO_ERROR;
		}

		case DISPATCHER_LIB_EVENT__INCOMMING_SMS:
		{
			DBGLOG( "%s:%i: Incomming SMS event\n", __func__, __LINE__ );
			DBGERR( "%s:%i: Incomming SMS event not supported\n", __func__, __LINE__ );
			//TODO - � ������ ������ �� �������� �������� - � ������ ��������� ��� ���
			//�� �� ������� �� ������ ���� ���
			eventfromdevice = DISPATCHER_LIB_EVENT__WAKEUP; //��� �� �� ������ �� ����
			return -AXIS_ERROR_NOT_INIT;
		}

		case DISPATCHER_LIB_EVENT__INT_ACC:
		{
			DBGLOG( "%s:%i: Int ACC event\n", __func__, __LINE__ );
			//��� ������� ��������, ��� � ���������� ��� ��������
			//��������, � ������� �����-��1/��2 ��� ��������, ��� ���������� �� ��� ����
			//�������������� ������ -��� ���� ������ ������������� ��� �������
			//TODO - ���� ��� �� �������� ���� ������� ��� �� ������ �����������
			//TODO - ���������� value
			value = 1; //TODO Constants
			string_message = "int ACC event";
			ToMonitoringClients = 1; //�� ������ ��������������� ������� �� ������ �����������
			break;
		}

		case DISPATCHER_LIB_EVENT__SLEEP:
		{
			//��� ������� ��������, ��� ������������ ������ ��������� � ��� ��� ����
			//��� �������� ���������
			//���� ���� ��������� � �� ���� (������� ��������) - ������ ��� ���� ��������
			//� ���� �� ���� - �� ��������� ��� - ����� ��� �����������
			if( Status->GlobalDevice_t.dmdev_interface.dmctrl_ipresent )
			{
				//TODO - �������
				DBGERR( "%s:%i: Sleep interface event\n", __func__, __LINE__ );
				string_message = "power button event";

				//����� ������� ���������, �� �� ������� �����
				if( (time(NULL) - Status->LastInterfaceSleepEvent) > 0 ) //��� �� ������� �� ���� ��� 1 ��� � �������
				{

					if( !(Status->GlobalDevice_t.dmdev_interface.dmctrl_status & DM_INTERFACE_STATUS_IN_SLEEP) )
					{
						//��������� ��������� - ���������� ��� ������� �����
						int iret = AdapterInterfaceSendCommand( Status, AXIS_INTERFACE_CMD_GO_SLEEP );

						if( iret < 0 )
						{
							DBGERR( "%s:%i: It wasn't possible to send event 'Go Sleep' to interface\n", __func__, __LINE__ );
						}

						Status->GlobalDevice_t.dmdev_interface.dmctrl_status |= DM_INTERFACE_STATUS_IN_SLEEP;
						AdapterInterfaceLcdOff( Status );
					}
					else
					{
						//��������� ���� - ��� ���� ���������
						int iret = AdapterInterfaceLcdOn( Status, 0 ); //��� ������� - ����� ������� ��� ��������� ����

						if( iret < 0 )
						{
							DBGERR( "%s: Can't turn on device display\n", __func__ );
						}
					}

					Status->LastInterfaceSleepEvent = time(NULL);
				}
				else
				{
					//������ ������ ���������� �� ������� ������ ��� - ��� ��� �������
					return AXIS_NO_ERROR;
				}
			}
			else
			{
				DBGLOG( "%s: Power button event without interface\n", __func__ );
				return AXIS_NO_ERROR;
			}

			break;
		}

		default:
		{
			//����������� ��� ��� �������
			DBGLOG( "%s: Invalid Event.type = %08x\n", __func__, eventfromdevice );
			return -AXIS_ERROR_INVALID_ANSED;
		}
	}

	//����, �� ��� ���, �� �� ����� ���� ���������� �� �������� SMS
	//�� � ���������������� �� ����� SMS �� ����, �� �� �����
	//�������� � ����� ������ � ����� SMS - �� ������� �� ������������ - �� ����� �����
	if( Status->GlobalDevice_t.dmdevice_powerupstatus == DISPATCHER_LIB_EVENT__WAKEUP )
	{
		//��� �� ����� �� ��������� �� ������� ���� ���������� �� ������� �������
		Status->GlobalDevice_t.dmdevice_powerupstatus = eventfromdevice;
	}

	if( ToMonitoringClients )
	{
		//����� ����������� ����� ����������� ���������� ������� - �� ���� ������ ���������������
		ret = AdapterRecorderMonitoringSendAlarmAllRecorders( Status );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't send monitoring alarm message to client\n", __func__ );
			//��� ���� � ������, �� ������ �� �����������
		}
	}

	ret = AdapterGlobalEventAddAndLog( Status, string_message, eventfromdevice, value, 0 );

	if( ret < 0 )

	{
		DBGERR( "%s: Can't add hardware event\n", __func__ );
	}

	return ret;
}


int AdapterGlobalSensorWork( AdapterGlobalStatusStruct_t *Status )
{


	if( !Status )
	{
		DBGERR( "%s: Invalid Argument\n", __func__ );
		return -1;
	}

	if( !Status->GlobalDevice_t.dmdev_event.dmctrl_ipresent )
	{
		return 0;
	}

	int value = DMXXXSensorsWork( &Status->GlobalDevice_t, &Status->AdapterSettings );

	//��� ������� ����� �����, ���� �������� ��� �������, �.�. ���������, ����� ����
	//������������ �������� �������� ���� ��� ��� �������� � �������� ������ �� �����
	if( !value )
	{
		//����� ��� ������� � ��������� ���� ������
		int i;

		for( i = 0; i < Status->ExchangeEvents.Events.EventCount; i++ )
		{
			if( Status->ExchangeEvents.Events.Events[i].Type == DISPATCHER_LIB_EVENT__SENSOR )
			{
				Status->ExchangeEvents.Events.Events[i].Value2 = 1;
			}

			if( Status->ExchangeEvents.Events.Events[i].Type == DISPATCHER_LIB_EVENT__ADXL )
			{
				Status->ExchangeEvents.Events.Events[i].Value2 = 1;
			}
		}

	}



	return 0;
}

/**
 * @brief �������, ������� ���� ������ �� ������������ ������� �����������
 *
 * @param Events - ��������� �� ��������� �������
 * @return DispatcherLibEvent_t*
 */
DispatcherLibEvent_t *AdapterGlobalGetFirstPowerUpEvent( DispatcherLibEventStatus_t *Events )
{
	int i;

	if( !Events )
	{
		DBGERR( "%s: Invalid argument\n", __func__ );
		return NULL;
	}

	if( !Events->EventCount )
	{
		DBGLOG( "%s: No events to seach\n", __func__ );
		return NULL;
	}


	for( i = 0; i < Events->EventCount; i++ )
	{
		if( !Events->Events[ i ].Flag )
		{
			if( Events->Events[ i ].Type == DISPATCHER_LIB_EVENT__WAKEUP ||
				Events->Events[ i ].Type == DISPATCHER_LIB_EVENT__POWER_UP ||
				Events->Events[ i ].Type == DISPATCHER_LIB_EVENT__TIMER ||
				Events->Events[ i ].Type == DISPATCHER_LIB_EVENT__REBOOT )
			{
				return &Events->Events[ i ];
			}
		}
	}

	return NULL; //�� ���� �� �����
}


/**
 * @brief �������, ������� ���������� ������ ������� � ������ � �������� ��������� type � 
 * @brief value (���� ��� value ������) � ������� value2 != 1
 *
 * @param Events - ��������� �� ���������� ��������� �������
 * @param type - ��� ������� ����� �������� �������
 * @param value - �������� �������, ������� ������, ����� ���� 0 � ���� ������ �� ������������ 
 * @return DispatcherLibEvent_t* - ��������� �� ��������� �������, NULL ���� �� ������� ��� ������
 **/
DispatcherLibEvent_t *AdapterGlobalGetFirstEventbyType( DispatcherLibEventStatus_t *Events, int type, int value )
{
	if( !Events || type < 0 )
	{
		DBGERR( "%s: Invalid argument\n", __func__ );
		return NULL;
	}

	DBGLOG( "%s: %i Events Count = %i\n",__func__,__LINE__,Events->EventCount );

	if( Events->EventCount > 0 )
	{
		int i;

		for( i = 0; i < Events->EventCount; i++ )
		{
			DBGLOG( "%s:%i: Event type=%02x(%02x) value=%08x(%08x) vlaue2=%08x\n", __func__, __LINE__,
					Events->Events[i].Type,
					type,
					Events->Events[i].Value,
					value,
					Events->Events[i].Value2 );

			if( Events->Events[i].Type == type )

			{
				if( value > 0 )
				{
					if( Events->Events[i].Value == value )
					{
						if( Events->Events[i].Value2 == 1 ) //TODO - ����� ��� �������???? ����� �� �����, ��� ��� DispatcherLibEventGetNew ���������� � �������� ��� �����
						{
							continue;
						}

						return &Events->Events[i];
					}
				}

				else
				{
					if( Events->Events[i].Value2 == 1 )
					{
						continue;
					}

					return &Events->Events[i];
				}

			}
		}
	}

	else
	{
		DBGLOG( "%s: No any events in struct\n", __func__ );
	}

	DBGLOG( " %s: No events in struct\n", __func__ );

	return NULL;

}

//������� ��������� ������� ������ �� ������� �� ����������
//����:
// Status - ��������� �� ��������� ������� ����������
//�������:
// 0 - ��� ������� ����������
// ����� 0 � ������ ������
int AdapterGlobalPowerUpEvent( AdapterGlobalStatusStruct_t *Status )
{
	if( !Status )
	{
		DBGERR( "%s:Invalid Argument\n", __func__ );
		return -1;
	}


	int err = 0;

	if(Status->GlobalDevice_t.dmdev_acc.dmctrl_feature & DM_ACC_FEATURE_RECORDER)
	{
		if( Status->Recorder.recorderscount > 0 && Status->Recorder.next )
		{
			if(AdapterRecorderConvertAcctoStandartStruct(Status->Recorder.next,&Status->ACCStatus) < 0)
			{
				DBGERR("%s %i : It wasn't possible to convert ACC Struct\n",__func__,__LINE__);
			}
		}
	}

	//�� ������ �������� ������� �� ��� ������ � �����, � ������� �� ��, ��� ��� �������
	//��� ��������� ������� � ���������� - ���� ��� - �� �� ��� �� ������ ��� ������
	//� �������� ���������� - ��� ������� ��������� ����������
	if( !Status->GlobalDevice_t.dmdevice_powerupstatus )
	{
		Status->GlobalDevice_t.dmdevice_powerupstatus = DISPATCHER_LIB_EVENT__POWER_UP;
	}

	err = AdapterGlobalEventAddAndLog( Status, "Adapter Power Up Event",
								 Status->GlobalDevice_t.dmdevice_powerupstatus,
								 Status->GlobalSourceWork, 0 );

	if( err < 0 )
	{
		DBGERR( "%s: It Wasn't possible to add first event\n", __func__ );
		return err;
	}

	return 0;
}

int AdapterGlobalEventCangoSleepSensor1( AdapterGlobalStatusStruct_t *Status )
{

	if( !Status )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	uint16_t 	alarmmask = 0x0003; //���� �� 2 ����
	int alarmshift = 4; //������� 4 ���� �������� IO

// 	if ( Status->GlobalDevice_t.dmdevice_powerupstatus == DISPATCHER_LIB_EVENT__SENSOR  )
// 	{


		if( ( ( Status->AdapterSettings.Block0.AdapterSettings.IOAlarmUSB >> alarmshift ) & alarmmask ) == 1 )
		{
			//ADXL �������, ������ ���� �����
			DispatcherLibEvent_t *Event =
				AdapterGlobalGetFirstEventbyType( &Status->ExchangeEvents.Events,
								DISPATCHER_LIB_EVENT__SENSOR,
								1 ); //TODO - ���������

			if( Event )
			{
				DBGLOG( "%s: There was DU1 event, can't sleep \n", __func__ );
				return  1;
			}
			else
			{
				DBGLOG( "%s: There wasn't DU1event, can sleep \n", __func__ );
				return 0;
			}
		}
// 	}

	return 0;
}

int AdapterGlobalEventCangoSleepSensor2( AdapterGlobalStatusStruct_t *Status )
{
	if( !Status )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}


	uint16_t 	alarmmask = 0x0003; //���� �� 2 ����
	int alarmshift = 6; //������� 4 ���� �������� IO

// 	if ( Status->GlobalDevice_t.dmdevice_powerupstatus == DISPATCHER_LIB_EVENT__SENSOR )
// 	{
		if ((( Status->AdapterSettings.Block0.AdapterSettings.IOAlarmUSB >> alarmshift ) & alarmmask ) == 1 )

		{
			//ADXL �������, ������ ���� �����
			DispatcherLibEvent_t *Event =
				AdapterGlobalGetFirstEventbyType( &Status->ExchangeEvents.Events,
								DISPATCHER_LIB_EVENT__SENSOR,
								2 ); //TODO - constants

			if( Event )
			{
				DBGLOG( "%s: There was DU2 event, can't sleep \n", __func__ );
				return  1;
			}
			else
			{
				DBGLOG( "%s: There wasn't DU2event, can sleep \n", __func__ );
				return 0;
			}
		}
// 	}

	return 0;
}

int AdapterGlobalEventCangoSleepSensor3( AdapterGlobalStatusStruct_t *Status )
{
	if( !Status )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}


	uint16_t 	alarmmask = 0x0003; //���� �� 2 ����
	int alarmshift = 8; //������� 4 ���� �������� IO


// 	if ( Status->GlobalDevice_t.dmdevice_powerupstatus == DISPATCHER_LIB_EVENT__SENSOR )
// 	{
		if ((( Status->AdapterSettings.Block0.AdapterSettings.IOAlarmUSB >>
					alarmshift ) & alarmmask ) == 1 )
		{
			//ADXL �������, ������ ���� �����
			DispatcherLibEvent_t *Event = AdapterGlobalGetFirstEventbyType( 
							&Status->ExchangeEvents.Events,
							DISPATCHER_LIB_EVENT__SENSOR,
							3 ); //TODO - ���������
			
			if( Event )
			{
				DBGLOG( "%s: There was DU3 event, can't sleep \n", __func__ );
				return  1;
			}
			else
			{
				DBGLOG( "%s: There wasn't DU3event, can sleep \n", __func__ );
				return 0;
			}
		}
// 	}

	return 0;
}

int AdapterGlobalEventCangoSleepSensor4( AdapterGlobalStatusStruct_t *Status )
{
	if( !Status )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	uint16_t 	alarmmask = 0x0003; //���� �� 2 ����
	int alarmshift = 10; //������� 4 ���� �������� IO



	if( Status->GlobalDevice_t.dmdevice_powerupstatus == DISPATCHER_LIB_EVENT__SENSOR )
	{
		if ((( Status->AdapterSettings.Block0.AdapterSettings.IOAlarmUSB >>
					alarmshift ) & alarmmask ) == 1 )
		{
			//ADXL �������, ������ ���� �����
			DispatcherLibEvent_t *Event = AdapterGlobalGetFirstEventbyType( 
					&Status->ExchangeEvents.Events,
					DISPATCHER_LIB_EVENT__SENSOR,
					4 ); //TODO - constants

			if( Event )
			{
				DBGLOG( "%s: There was DU4 event, can sleep \n", __func__ );
				return  0;
			}
			else
			{
				DBGLOG( "%s: There wasn't DU4event, can't sleep \n", __func__ );
				return 1;
			}
		}
	}

	return 0;
}

int AdapterGlobalOutputSensorPowerWork( AdapterGlobalStatusStruct_t *Status )
{
	if( !Status )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}


//	int iocount = 4; //��� ���� ��������
	int ret = 0;
	uint16_t	iomask = AXIS_SOFTADAPTER_IO__ENABLE_01;
// 	uint16_t 	alarmmask = 0x0003; //���� �� 2 ����
// 	int alarmshift = 8; //������� 4 ���� �������� IO
	int i;

	for( i = 0; i < DMXXXSensorOutputCount( &Status->GlobalDevice_t ); i++ ) //���������� ������� ������� � ����������
	{
		if( Status->AdapterSettings.Block0.AdapterSettings.IOAlarmUSB & ( 1 << i ) )
		{
// 			DBGLOG("%s:%i:ENABLE SENSOR %i\n",__func__,__LINE__,i);
			if( DMXXXSensorOutputOn( &Status->GlobalDevice_t, i + 1 ) < 0 )
			{
				DBGERR( "%s:It wasn't possible to io power\n", __func__ );
				return -1;
			}

		}
		else
		{
// 			DBGLOG("%s:%i:DISABLE SENSOR %i\n",__func__,__LINE__,i);
			if( DMXXXSensorOutputOff( &Status->GlobalDevice_t, i + 1 ) < 0 )
			{
				DBGERR( "%s:It wasn't possible to io power off\n", __func__ );

				return -1;
			}
			DBGLOG("%s:%i:DISABLE DU\n", __func__, __LINE__ );
		}
	}
	
	for( i=0; i< DMXXXSensorOutputCount( &Status->GlobalDevice_t ); i++ )
	{
		if(Status->AdapterSettings.Block0.AdapterSettings.IOAlarmUSB & (iomask ))
		{
			ret = DMXXXSensorOutputOn( &Status->GlobalDevice_t, iomask );
			
			if( ret < 0 )
			{
				if( errno == EINVAL )
				{
					DBGERR("%s:%i: It wasn't possible to io power on, because DU enabled.\n",__func__, __LINE__ );
				}
				else
				{
					DBGERR("%s:%i: It wasn't possible to io power on. Errno = %i\n",__func__, __LINE__, errno );
					return -1;
				}
			}
			
			DBGLOG("%s:%i:ENABLE SENSOR %i\n", __func__, __LINE__, i);
		}
		else
		{
			ret = DMXXXSensorOutputOff(&Status->GlobalDevice_t, iomask );
			
			if( ret < 0 )
			{
				if( errno == EINVAL )
				{
					DBGERR("%s:%i: It wasn't possible to io power off, because DU enabled.\n",__func__, __LINE__ );
				}
				else
				{
					DBGERR("%s:%i: It wasn't possible to io power off. Errno = %i\n",__func__, __LINE__, errno );
					return -1;
				}
			}
			
			DBGLOG("%s:%i:DISABLE SENSOR %i\n", __func__, __LINE__, i);
		}

		iomask <<= 1;
	}
	
	if( Status->AdapterSettings.Block0.AdapterSettings.IOAlarmUSB & AXIS_SOFTADAPTER_IO__MASK )

	{
		if( AdapterPowerRcOn( &Status->GlobalDevice_t ) < 0 )
		{

			DBGERR( "%s:It wasn't possible to power on IO\n", __func__ );

			return -1;
		}
	}
	else
	{
		if( AdapterPowerRcOff( &Status->GlobalDevice_t ) < 0 )
		{

			DBGERR( "%s:It wasn't possible to power on IO\n", __func__ );

			return -1;
		}
	}

	return 0;

}

int AdapterGlobalEventCheckOnSleep( AdapterGlobalStatusStruct_t *Status )
{
	if( !Status )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	if( AdapterGlobalEventCangoSleepSensor1( Status ) )
	{
		DBGERR( "%s:Was work on DU1 can't sleep\n", __func__ );
		return 1;
	}

	if( AdapterGlobalEventCangoSleepSensor2( Status ) )
	{
		DBGERR( "%s:Was work on DU2 can't sleep\n", __func__ );
		return 1;
	}

	if( AdapterGlobalEventCangoSleepSensor3( Status ) )
	{
		DBGERR( "%s:Was work on DU3 can't sleep\n", __func__ );
		return 1;
	}

	if( AdapterGlobalEventCangoSleepSensor4( Status ) )
	{
		DBGERR( "%s:Was work on DU4 can't sleep\n", __func__ );
		return 1;
	}

	return 0;

}

int AdapterGlobalSensorGetState( AdapterGlobalStatusStruct_t* Status, int sensnum )
{
	if( !Status )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	return DMXXXSensorGetState( &Status->GlobalDevice_t, sensnum );
}

int AdapterGlobalSensorIsActive( AdapterGlobalStatusStruct_t* Status )
{
	if( !Status )
	{
		DBGERR( "%s:Invalid Argument\n", __func__ );
		return -1;
	}

	int alarmmask = 0x03;
	int maxevent = 4;
	int alarmshift = 4;
	int i;

	for( i = 0; i < maxevent; i++ )
	{
		switch( ( Status->AdapterSettings.Block0.AdapterSettings.IOAlarmUSB >> alarmshift ) & alarmmask )
		{
			case 1:
			{
				if( !( AdapterGlobalSensorGetState( Status, i + 1 ) ) )
				{
					DBGLOG( "%s: Sensor %d is active\n", __func__, i );
					return 1;
				}

				break;
			}
			case 2:
			{
				if( AdapterGlobalSensorGetState( Status, i + 1 ) )
				{
					DBGLOG( "%s: Sensor %d is active high\n", __func__, i );
					return 1;
				}

				break;
			}
			default:
			{
				DBGLOG( "%s: Sensor %d is off\n", __func__, i );
				break;
			}
		}

		alarmshift += 2; //�������� �� 2 ���
	}

	DBGLOG( "%s; Np active sensor found\n", __func__ );

	return 0;
}

/**
 * @brief ������� ������� ��������� ������� � ����� Type � ������ � ������ ��� ��� ������� � ���������� Message
 *
 * @param Status - ��������� �� ���������� ��������� ������� ��������
 * @param Message - ��������� �� ������ � ����������
 * @param Type - ��� ������������ �������
 * @param Value1 - ������ �������� ��� �������
 * @param Value2 - ������ �������� ��� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������.
 **/
int AdapterGlobalEventAddAndLog( AdapterGlobalStatusStruct_t *Status, const char *Message, uint32_t Type, int Value1, int Value2 )
{
	int ret = 0;

	if( !Status || !Type || !Message )
	{
		DBGERR( "%s: Invalid argument\n", __func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	DBGLOG( "%s: Try add event '%s' 0x%08x\n", __func__, Message, Type );

	ret = DispatcherLibEventAdd( &Status->ExchangeEvents.Events,
								 Type, Value1, Value2,
								 &Status->ACCStatus );

	if( ret < 0 )
	{
		DBGERR( "%s: Can't add new event to status\n", __func__ );
		return ret;
	}

	//�� ������ �������� � ������ ���� ��� ������
	INFO_LOG( 0, Message );
	LOG_EVENT( &Status->ExchangeEvents.Events, 0, ret );

	return 0;

}

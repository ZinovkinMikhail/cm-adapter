//������� ��������� �������� ���������� �� �������� ������ ���������� ����� HTTP

//�����: ��������� ����� ��� "����"
//�����: i.podkolzin@ross-jsc.ru
//������: 2010.09.14 18:18

#define _GNU_SOURCE

//#include <asm/unistd.h>

#include <sys/uio.h>

#ifndef __NR_splice
#define __NR_splice
#define SPLICE_FOR_TEST
#endif

#ifndef __NR_vmsplice
#define __NR_vmsplice
#define VMSPLICE_FOR_TEST
#endif


#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/resource.h>
#include <axis_error.h>
#include <axis_ssl_httpi_server_error.h>
#include <axis_ssl_httpi_download.h>
#include <axis_httpi_tags.h>

#include <adapter/recorder.h>

#ifdef TMS320DM355
#include <dm355_leds.h>
#endif

#include <dispatcher_lib/dispatcher_lib_resource.h>
#include <dispatcher_lib/upload.h>
#include <dispatcher_lib/splicecalls.h>

#include <adapter/interface.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
//#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif

#define MAX_BLOCK_SIZE 			131072
#define MIN_BLOCK_SIZE 			1
#define STANDART_BLOCK_SIZE 	4096

#define TEST_FILE_SIZE			1073741824	/*104857600*/
//100mb



#define TEST1
#ifdef TEST1

typedef struct TestUpload_s
{

	pthread_t Thread;
	pid_t PID;
	uint32_t speed;
	uint32_t countbytes;
	uint32_t time;

	
	
	AdapterGlobalStatusStruct_t *Status;
	
}TestUpload_t;

static void *TestUploadUSBThreadRoutine( void *Value )
{
	if( !Value )
	{
		DBGERR("%s:%i: Invalid Argumentn\n",__func__,__LINE__);
		return NULL;
	}

	TestUpload_t *Test = (TestUpload_t *)Value;

	int thcount = 0;

	int ret = 0;
	
	int upload_type = 0;


	Test->PID = axis_getpid();

	struct Recorder_s * rec = Test->Status->Recorder.next;

	if(!rec)
	{
		DBGERR("%s:No recorder\n",__func__);
		return NULL ;
	}

	axis_ssl_download_request UploadRequest; //��������� � ������� ������� ��������

	 axis_ssl_wrapper_ex_sock sock;

	 sock.sock = open("/dev/null",O_RDWR);

	 if(sock.sock <= 0)
	 {
		 DBGERR("%s:It wasn't possible to open sock\n",__func__);
		 return NULL ;
	 }

	UploadRequest.from = 0x0000;

	UploadRequest.to = 1073741824;


// 	int upload_type = 0;

	rec->recorder_data.private_data = &upload_type;
	
	ret = AdapterRecorderUploadStart(&sock,&UploadRequest,rec,(unsigned int*)&thcount);
	if ( ret < 0 )
	{
		DBGERR( "%s: Can't upload data to client\n", __func__ );
		close(sock.sock);
	}

	close(sock.sock);

	return 0;

	


	
}

int TestUploadUsbThreadStart(TestUpload_t *Test)
{
	if( !Test )
	{
		DBGERR("%s:%i: Invalid Argumentn\n",__func__,__LINE__);
		return -1;
	}

	if( pthread_create( &Test->Thread, NULL,
							TestUploadUSBThreadRoutine,
							(void *)Test ) < 0 )
	{
		DBGERR("%s: Can't start upload thread\n",__func__);
		return -AXIS_ERROR_CANT_CREATE_THREAD;
	}

	return 0;
}



//������� ������� �������� �������� �������� ������
//����
//	sock - ��������� �� ��������� ������ ����� ������� ��������� ����������
//	Requests - ��������� �� ��cc�� �������� �������
//	Count - ���������� ��������� � ������ �������
//  tCount - ��������� �� ������� ������ ������� ������ ������� ���� ������� ����������
//�������
//���������� ���� ���������� ������� ��� < 0 � ������ ������
int AdapterInterfaceTestPrint(
        axis_ssl_wrapper_ex_sock *sock,
        axis_httpi_request *Requests,
        int Count,
        unsigned int *tCount )
{

	setpriority( PRIO_PROCESS, 0, -20 );
	int ret = 0; //��� ��������
	axis_ssl_download_request UploadRequest; //��������� � ������� ������� ��������
	//������� �������� ����� HTTP �������

	uint64_t	iCount = 0; //������� ���� ����� ����������
	uint64_t	iFileSize = 0; //������ ������
	int block_size_in	= 0;
	int  pipe_fds[2] = { 0 };
	// int RRCount;

	uint8_t Data[ MAX_BLOCK_SIZE  ] = { 0 };
	AdapterGlobalStatusStruct_t *Status = AdapterGlobalGetStatus();

	if( !sock || !Requests || Count <= 0 || !Status )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );

		ret = axis_ssl_httpi_server_send_error( sock,
		                                        AXIS_SSL_HTTPI_ERROR__409_CONFLICT );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't print server error\n", __func__ );
		}

		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	DBGLOG( DISPATCHER_LIB_DEBUG__START );

	//�������� ����� ������
	Status->LastInterfaceQuire = time( NULL );


	char *block_size_ch = axis_httpi_get_caserequest_value( Requests, Count,
	                      AXIS_HTTPI_DIR_NAME_0 );

	if( !block_size_ch )
	{
		DBGERR( "%s: Can't get block size, using standart 4096\n", __func__ );
		block_size_in = STANDART_BLOCK_SIZE;
	}
	else
	{
		block_size_in = atoll( block_size_ch );

		if( block_size_in > MAX_BLOCK_SIZE || block_size_in < MIN_BLOCK_SIZE )
		{
			DBGLOG( "%s: Block size is too big, using standart 4096\n", __func__ );
			block_size_in = STANDART_BLOCK_SIZE;
		}
	}

	if( axis_ssl_download_parsing_http_request(
	                        Requests, Count,
	                        &UploadRequest ) < 0 )
	{
		DBGERR( "%s: Can't parsing request\n", __func__ );
		ret = axis_ssl_httpi_server_send_error( sock,
		                                        AXIS_SSL_HTTPI_ERROR__404_NOT_FOUND );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't print server error\n", __func__ );
		}

		return -AXIS_ERROR_INVALID_ANSED;
	}

	if( UploadRequest.ftype == AXIS_SSL_DOWNLOAD_PAGE_TEST_PAGE_TYPE )
	{

		iFileSize = TEST_FILE_SIZE ;
	}
	else
	{
		DBGERR( "%s: Invalid Request file type %d\n", __func__, UploadRequest.ftype );

		ret = axis_ssl_httpi_server_send_error( sock,
		                                        AXIS_SSL_HTTPI_ERROR__404_NOT_FOUND );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't print server error\n", __func__ );
		}

		return -AXIS_ERROR_INVALID_ANSED;
	}


	//������ � ����������� �� ���� ��� ������
	if( UploadRequest.type == head )
	{
		ret = axis_ssl_download_send_head( sock, iFileSize, 1 );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't send head to client %d\n", __func__, ret );
		}

		return ret; //������� ��������� ������ - ������ ���������� ����
	}
	else if( UploadRequest.type != get )
	{
		DBGERR( "%s: Invalid Request type %d\n", __func__, UploadRequest.type );

		ret = axis_ssl_httpi_server_send_error( sock,
		                                        AXIS_SSL_HTTPI_ERROR__404_NOT_FOUND );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't print server error\n", __func__ );
		}

		return -AXIS_ERROR_INVALID_ANSED;

	}


	ret = axis_ssl_download_send_head( sock, iFileSize, 1 );

	if( ret < 0 )
	{
		DBGERR( "%s: Can't send head for upload\n", __func__ );
		goto end;
	}

	if( pipe( pipe_fds ) < 0 )
	{
		DBGERR( "%s: Can't create pipe pair\n", __func__ );
		return -AXIS_ERROR_CANT_OPEN_DEVICE;
	}


	TestUpload_t Test;

	memset(&Test,0,sizeof(TestUpload_t));

	Test.Status = Status;

// 	if(TestUploadUsbThreadStart(&Test) < 0)
// 	{
// 		DBGERR("%s:It wasn't possible \n",__func__);
// 		return -1;
// 	}


	//�������� ���� ������
	while( iFileSize )
	{

		( *tCount )++;
		pthread_testcancel();

		iCount =  axis_ssl_wrapper_ex_write( sock, Data, MAX_BLOCK_SIZE, 60 );
// 			 struct iovec mv = { Data, block_size_in };
//
//
// 			RRCount = vmsplice( pipe_fds[1], &mv, 1, 0 );
//
//
// 			if( RRCount < 0 )
// 			{
// 				DBGERR("%s: Can't vmsplice to pipe\n",__func__);
// 					//������ ���������� ������������� ����� - ��� �������� � ��������
// 					//���������� �������� ������
// 				ret =  -AXIS_ERROR_CANT_READ_DEVICE;
// 				goto end;
// 			}
//
// 			iCount = splice( pipe_fds[0], NULL, sock->sock, NULL, RRCount, SPLICE_F_MOVE | SPLICE_F_NONBLOCK );
//
// 			if( iCount < 0)
// 			{
// 				DBGERR( "%s: It wasn't possible to send splice i\n'", __func__);
// 				goto end;
// 		    }

		iFileSize -= iCount;


	}


	DBGLOG( DISPATCHER_LIB_DEBUG__DONE );
end:

	if(Test.PID > 0)
	{
		if( pthread_cancel( Test.Thread) )
		{
			DBGERR("%s: Can't send signal to canseled thread at pid %d\n",
				__func__,
				Test.PID );
			return -AXIS_ERROR_INVALID_ANSED;
		}

		//����� ��������
		if( pthread_join( Test.Thread, NULL ) < 0 )
		{
			DBGERR("%s: Can't join to thread\n",__func__);
			return -AXIS_ERROR_INVALID_ANSED;
		}
	}

	close( pipe_fds[0] );
	close( pipe_fds[1] );

	ret = 1; //��� ��� ������ ������� ���������� ���������� ����

	return ret;
}

#else

//������� ������� �������� �������� �������� ������
//����
//	sock - ��������� �� ��������� ������ ����� ������� ��������� ����������
//	Requests - ��������� �� ��cc�� �������� �������
//	Count - ���������� ��������� � ������ �������
//  tCount - ��������� �� ������� ������ ������� ������ ������� ���� ������� ����������
//�������
//���������� ���� ���������� ������� ��� < 0 � ������ ������
int AdapterInterfaceTestPrint(
        axis_ssl_wrapper_ex_sock *sock,
        axis_httpi_request *Requests,
        int Count,
        unsigned int *tCount )
{

	setpriority( PRIO_PROCESS, 0, -20 );
	int ret = 0; //��� ��������
	axis_ssl_download_request UploadRequest; //��������� � ������� ������� ��������
	//������� �������� ����� HTTP �������

	uint64_t	iCount = 0; //������� ���� ����� ����������
	uint64_t	iFileSize = 0; //������ ������
	int block_size_in	= 0;
	int  pipe_fds[2] = { 0 };
	// int RRCount;

	uint8_t Data[ MAX_BLOCK_SIZE  ] = { 0 };
	AdapterGlobalStatusStruct_t *Status = AdapterGlobalGetStatus();

	if( !sock || !Requests || Count <= 0 || !Status )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );

		ret = axis_ssl_httpi_server_send_error( sock,
		                                        AXIS_SSL_HTTPI_ERROR__409_CONFLICT );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't print server error\n", __func__ );
		}

		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	DBGLOG( DISPATCHER_LIB_DEBUG__START );

	//�������� ����� ������
	Status->LastInterfaceQuire = time( NULL );


	char *block_size_ch = axis_httpi_get_caserequest_value( Requests, Count,
	                      AXIS_HTTPI_DIR_NAME_0 );

	if( !block_size_ch )
	{
		DBGERR( "%s: Can't get block size, using standart 4096\n", __func__ );
		block_size_in = STANDART_BLOCK_SIZE;
	}
	else
	{
		block_size_in = atoll( block_size_ch );

		if( block_size_in > MAX_BLOCK_SIZE || block_size_in < MIN_BLOCK_SIZE )
		{
			DBGLOG( "%s: Block size is too big, using standart 4096\n", __func__ );
			block_size_in = STANDART_BLOCK_SIZE;
		}
	}

	if( axis_ssl_download_parsing_http_request(
	                        Requests, Count,
	                        &UploadRequest ) < 0 )
	{
		DBGERR( "%s: Can't parsing request\n", __func__ );
		ret = axis_ssl_httpi_server_send_error( sock,
		                                        AXIS_SSL_HTTPI_ERROR__404_NOT_FOUND );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't print server error\n", __func__ );
		}

		return -AXIS_ERROR_INVALID_ANSED;
	}

	if( UploadRequest.ftype == AXIS_SSL_DOWNLOAD_PAGE_TEST_PAGE_TYPE )
	{

		iFileSize = TEST_FILE_SIZE ;
	}
	else
	{
		DBGERR( "%s: Invalid Request file type %d\n", __func__, UploadRequest.ftype );

		ret = axis_ssl_httpi_server_send_error( sock,
		                                        AXIS_SSL_HTTPI_ERROR__404_NOT_FOUND );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't print server error\n", __func__ );
		}

		return -AXIS_ERROR_INVALID_ANSED;
	}


	//������ � ����������� �� ���� ��� ������
	if( UploadRequest.type == head )
	{
		ret = axis_ssl_download_send_head( sock, iFileSize, 1 );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't send head to client %d\n", __func__, ret );
		}

		return ret; //������� ��������� ������ - ������ ���������� ����
	}
	else if( UploadRequest.type != get )
	{
		DBGERR( "%s: Invalid Request type %d\n", __func__, UploadRequest.type );

		ret = axis_ssl_httpi_server_send_error( sock,
		                                        AXIS_SSL_HTTPI_ERROR__404_NOT_FOUND );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't print server error\n", __func__ );
		}

		return -AXIS_ERROR_INVALID_ANSED;

	}


	ret = axis_ssl_download_send_head( sock, iFileSize, 1 );

	if( ret < 0 )
	{
		DBGERR( "%s: Can't send head for upload\n", __func__ );
		goto end;
	}

	if( pipe( pipe_fds ) < 0 )
	{
		DBGERR( "%s: Can't create pipe pair\n", __func__ );
		return -AXIS_ERROR_CANT_OPEN_DEVICE;
	}

	int fd = open( "/dev/mtd4", O_RDONLY );

	if( fd < 0 )
	{
		DBGERR( "%s:It wasn't possible to open MTD!\n", __func__ );
		return -1;
	}

//	int ret;
	int RCount;
	int RRCount;
	int WCount;


	//�������� ���� ������
	while( iFileSize )
	{

		( *tCount )++;
		pthread_testcancel();

		if( iFileSize > sizeof( Data ) )
		{
			RCount = sizeof( Data );
		}
		else
		{
			RCount = iFileSize;
		}

		RRCount = read( fd, Data, RCount );

		if( RRCount < 0 )
		{
			DBGERR( "%s: Can't read from source\n", __func__ );
			//������ ���������� ������������� ����� - ��� �������� � ��������
			//���������� ��������� ������
			return -AXIS_ERROR_CANT_READ_DEVICE;
		}
		else if( !RRCount )
		{
			DBGERR( "%s: Recorder close data socket\n", __func__ );
			return RRCount;
		}

		WCount =  axis_ssl_wrapper_ex_write( sock, Data, MAX_BLOCK_SIZE, 60 );

		if( WCount != RRCount )
		{
			DBGERR( "%s: Can't write to client\n", __func__ );
			return -AXIS_ERROR_CANT_WRITE_DEVICE;
		}

		iFileSize -= RRCount;
		
	}


	

	DBGLOG( DISPATCHER_LIB_DEBUG__DONE );
end:

	close( pipe_fds[0] );
	close( pipe_fds[1] );

	close(fd);
	
	ret = 1; //��� ��� ������ ������� ���������� ���������� ����

	return ret;
}

#endif

#ifdef SPLICE_FOR_TEST
#undef __NR_splice
#undef SPLICE_FOR_TEST
#endif
#ifdef VMSPLICE_FOR_TEST
#undef __NR_vmsplice
#undef VMSPLICE_FOR_TEST
#endif

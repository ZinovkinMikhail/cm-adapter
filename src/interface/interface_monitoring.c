//������� ��������� �������� ���������� �� �������� ����������� ����� HTTP

//�����: ��������� ����� ��� "����"
//�����: i.podkolzin@ross-jsc.ru
//������: 2010.09.14 18:18

#include <axis_error.h>
#include <axis_ssl_httpi_server_error.h>
#include <axis_ssl_httpi_download.h>

#ifdef TMS320DM355
#include <dm355_leds.h>
#endif

#include <dispatcher_lib/dispatcher_lib_resource.h>
#include <dispatcher_lib/upload.h>
#include <adapter/interface.h>
#include <adapter/recorder.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif
static void adapter_inteface_monitoring_cath_on_cancel( struct Recorder_s **recorder )
{
	if ( !recorder || !( *recorder ) )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return ;
	}

	DBGLOG( "%s: Monitoring thread cenceld... Try release recorder....\n", __func__ );

	if (( *recorder )->usecount > 0 )
	{
 		DBGLOG("%s:%i: Decrement use rec count %i \n",__func__,__LINE__,(*recorder)->usecount);
		AdapterRecorderRelease( *recorder );
	}

		//pthread_exit(NULL);

	DBGLOG( "%s: Release done\n", __func__ );
}

//������� ������� �������� �������� �������� ������
//����
//	sock - ��������� �� ��������� ������ ����� ������� ��������� ����������
//	Requests - ��������� �� ��cc�� �������� �������
//	Count - ���������� ��������� � ������ �������
//  tCount - ��������� �� ������� ������ ������� ������ ������� ���� ������� ����������
//�������
//���������� ���� ���������� ������� ��� < 0 � ������ ������
int AdapterInterfaceMonitoringPrint(
        axis_ssl_wrapper_ex_sock *sock,
        axis_httpi_request *Requests,
        int Count,
        unsigned int *tCount )
{

	int ret = 0; //��� ��������
	//int upload_type = 0;
	axis_ssl_download_request UploadRequest; //��������� � ������� ������� ��������

	struct Recorder_s **recorder = NULL;

	AdapterGlobalStatusStruct_t *Status = AdapterGlobalGetStatus();

	if ( !sock || !Requests || Count <= 0 || !Status )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );

		ret = axis_ssl_httpi_server_send_error( sock,
		                                        AXIS_SSL_HTTPI_ERROR__409_CONFLICT );

		if ( ret < 0 )
		{
			DBGERR( "%s: Can't print server error\n", __func__ );
		}

		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	DBGLOG( DISPATCHER_LIB_DEBUG__START );

	//�������� ����� ������
	Status->LastInterfaceQuire = time( NULL );



	//�������� �������� �����
	ret = AdapterInterfaceGetRecorderBySN( Requests, Count, Status, &recorder );

	if ( ret < 0 || !recorder )
	{
		DBGERR( "%s: Can't Check SN from request\n", __func__ );
		return ret;
	}

	if ( !ret )
	{
		//��� ��������� ������ �� ������ ������ �� ����������!

		ret = axis_ssl_httpi_server_send_error( sock,
		                                        AXIS_SSL_HTTPI_ERROR__404_NOT_FOUND );

		if ( ret < 0 )
		{
			DBGERR( "%s: Can't print server error\n", __func__ );
		}

		return -AXIS_ERROR_INVALID_ANSED;
	}

	pthread_cleanup_push(( void* )adapter_inteface_monitoring_cath_on_cancel, recorder );


	if ( axis_ssl_download_parsing_http_request(
	                        Requests, Count,
	                        &UploadRequest ) < 0 )
	{
		DBGERR( "%s: Can't parsing upload request\n", __func__ );
		ret = axis_ssl_httpi_server_send_error( sock,
		                                        AXIS_SSL_HTTPI_ERROR__404_NOT_FOUND );

		if ( ret < 0 )
		{
			DBGERR( "%s: Can't print server error\n", __func__ );
		}

		ret = -AXIS_ERROR_INVALID_ANSED;
	}

	else
	{
		ret = AdapterRecorderHttpMonitoringStart( sock, &UploadRequest, *recorder, tCount, Status, Requests, Count );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't upload data to client\n", __func__ );
		}

		else
		{
			DBGLOG( DISPATCHER_LIB_DEBUG__DONE );
		}
	}

	pthread_cleanup_pop( 0 );

	//NOTE - ���������� ����� ���������� � ������������� �� ����, ��� ���������� �����
	adapter_inteface_monitoring_cath_on_cancel( recorder );

	return ret;
}

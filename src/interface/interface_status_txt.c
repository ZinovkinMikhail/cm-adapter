//������� ��������� �������� ������ ��� ���������� ����� HTTP

//�����: ��������� ����� ��� "����"
//�����: i.podkolzin@ross-jsc.ru
//������: 2010.09.14 18:18

#include <axis_error.h>
#include <axis_ssl_httpi_server_error.h>
#include <axis_httpi_tags.h>
#include <axis_ssl_httpi_file_download.h>

#include <dispatcher_lib/dispatcher_lib_resource.h>
#include <dispatcher_lib/sprintf_ini_files.h>
#include <dispatcher_lib/dispatcher_lib_resource.h>
#include <dispatcher_lib/command.h>
#include <dispatcher_lib/system.h>
#include <adapter/interface.h>
#include <adapter/dispatcher.h>
#include <adapter/acc.h>
#include <adapter/recorder.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif

static void adapter_inteface_status_cath_on_cancel(struct Recorder_s **recorder)
{
 	DBGLOG("%s:%i:Cancel THREAD Catch\n",__func__,__LINE__);
	
	if( !recorder || !(*recorder) )
	{
		DBGERR("%s:%i: Invalid Argumentn\n",__func__,__LINE__);
		return ;
	}

	if( (*recorder)->usecount > 0)
	{
		DBGLOG("%s:%i:Decrement use rec count %i \n",__func__,__LINE__,(*recorder)->usecount);
		AdapterRecorderRelease(*recorder);
	}
	//pthread_exit(NULL);

}

//������� ������� �������� �������� status.txt
//����
//	sock - ��������� �� ��������� ������ ����� ������� ��������� ����������
//	Requests - ��������� �� ��cc�� �������� �������
//	Count - ���������� ��������� � ������ �������
//  tCount - ��������� �� ������� ������ ������� ������ ������� ���� ������� ����������
//�������
//���������� ���� ���������� ������� ��� < 0 � ������ ������
int AdapterInterfaceStatusTxtPrint(
        axis_ssl_wrapper_ex_sock *sock,
        axis_httpi_request *Requests,
        int Count,
        unsigned int *tCount )
{

	int ret = 0;

	AdapterGlobalStatusStruct_t *Status = AdapterGlobalGetStatus();
	struct Recorder_s **recorder = NULL;
	char sendbuffer[DISPATCHER_LIB_BUFFFER_SIZE];
	int len = 0;


	if ( !sock || !Requests || Count <= 0 || !Status )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	DBGLOG( DISPATCHER_LIB_DEBUG__START );

	ret = AdapterInterfaceGetRecorderBySN(Requests,Count,Status,&recorder);
	DBGLOG("%s:%i:RECCORDER POIN = %p\n",__func__,__LINE__,recorder);
	if ( ret == -AXIS_ERROR_INVALID_ANSED )
	{

		DBGLOG( "%s: No SN from request - Adapter Status\n", __func__ );
		//��� ��������� ������!!!
		ret = AdapterAccGetStatus( Status );
		if (ret < 0)
		{
			ret = axis_ssl_httpi_server_send_error( sock, AXIS_SSL_HTTPI_ERROR__404_NOT_FOUND );

			if ( ret < 0 )
			{
				DBGERR( "%s:%i: Can't print server error\n", __func__, __LINE__ );
			}

			return ret;
		}

		ret = AdapterInterfaceAdapterStatusTxtPrint( sock, Requests, Count, tCount,
		                Status );
	}
	else if ( ret < 0 )
	{
		DBGERR( "%s: Can't Check SN from request\n", __func__ );
		return ret;
	}
	else if ( ret == 1 && (*recorder))
	{
 		DBGLOG( "%s: SN equalent - Recorder Status %i\n", __func__,(*recorder)->hen );
		//�������� ����� ������
		pthread_cleanup_push(( void* )adapter_inteface_status_cath_on_cancel, recorder );

		ret = AdapterInterfaceRecorderStatusTxtPrint( sock, Requests, Count, tCount,
		                Status,recorder );

        DBGLOG("%s:%i: After print %i\n",__func__,__LINE__,ret);
		pthread_cleanup_pop( 0 );
        DBGLOG("%s:%i: After pop\n",__func__,__LINE__);
	}
	else/* if(Status->GlobalDevice_t.dmdev_dm3xx & DM_DM_WORK_SEPARATE)*/
	{
		
		if(Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature & DM_DM_WORK_SEPARATE)
		{
			//����� ���� �������� ��������
			ret = AdapterInterfaceCheckSNRequest( Requests, Count, Status );

			if ( ret == 1 )
			{

				ret = AdapterAccGetStatus( Status );

			if ( ret < 0 )
			{
				ret = axis_ssl_httpi_server_send_error( sock, AXIS_SSL_HTTPI_ERROR__404_NOT_FOUND );

				if ( ret < 0 )
				{
					DBGERR( "%s:%i: Can't print server error\n", __func__, __LINE__ );
				}

				return ret;
			}

			ret = AdapterInterfaceAdapterStatusTxtPrint( sock, Requests, Count, tCount,

			                Status );
			}
			else
			{
				//�������� ����� �� ������
				DBGLOG( "%s: SN No equalent - 404 Error\n", __func__ );
				ret = axis_ssl_httpi_server_send_error( sock, AXIS_SSL_HTTPI_ERROR__404_NOT_FOUND );

				if ( ret < 0 )
				{
					DBGERR( "%s:%i: Can't print server error\n", __func__, __LINE__ );
				}

				return ret;
			}
		}
		else
		{
			//���� � ��� ���� � ��� ��� ��������������� ���� �� ���� ���������� ����� ����� ������� ��� �������� � ���� ��� �� ������ ��� ������

			ret = AdapterInterfaceCheckSNRequest( Requests, Count, Status );

			if ( ret == 1 )
			{
				ret = -AXIS_ERROR_BUSY_DEVICE ;

			}
			else
			{
			
				//�������� ����� �� ������
			DBGLOG( "%s: SN No equalent - 404 Error\n", __func__ );
			ret = axis_ssl_httpi_server_send_error( sock, AXIS_SSL_HTTPI_ERROR__404_NOT_FOUND );

			if ( ret < 0 )
			{
				DBGERR( "%s:%i: Can't print server error\n", __func__, __LINE__ );
			}
			
			}

			
		}

	}
	DBGLOG("%s: %i ERROR = %i\n",__func__,__LINE__,ret);
	//��� ���� ������� ���� ������ ����� �������
	if ( sock->ip != 0x7f000001 )//127.0.0.1
	{
		//�������� ����� ������
		Status->LastInterfaceQuire = time( NULL );
	}
	
	if ( ret == -AXIS_ERROR_TIMEOUT_EVENT )
	{
		DBGLOG( "%s: C5515 is busy equalent - 409 Error\n", __func__ );

		ret = axis_ssl_httpi_server_send_error( sock, AXIS_SSL_HTTPI_ERROR__409_CONFLICT );

		if ( ret < 0 )
		{
			DBGERR( "%s:%i: Can't print server error\n", __func__, __LINE__ );
		}

		return ret;
	}
	else if ( ret == -AXIS_ERROR_BUSY_DEVICE )
	{
		DBGLOG( "%s: C5515 is busy equalent - 404 Error\n", __func__ );

		if ( ret == -AXIS_ERROR_BUSY_DEVICE )
		{
			len = DispatcherLibIniFileSprintfCommandRequestsResponse(
			              sendbuffer, DISPATCHER_LIB_BUFFFER_SIZE,
			              Requests,
			              Count,
			              ADAPTER_COMMAND__RETURN_NODEVICE,
			              "Read" );
			ret = axis_ssl_buffer_text_file_download( sock, sendbuffer, len );
		}
		

		//return ret;
	}
	else if ( ret < 0 )
	{
		DBGERR( "%s: Can't print status\n", __func__ );
	}

	if( recorder ) //���� ��� ��� ���������� �� �� ��� ������ ����������
		adapter_inteface_status_cath_on_cancel(recorder);

	DBGLOG( DISPATCHER_LIB_DEBUG__DONE );
	


	
	return ret;
}

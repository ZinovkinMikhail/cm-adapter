//������� ��������� �������� log.txt ���������� ����� HTTP

//�����: ��������� ����� ��� "����"
//�����: i.podkolzin@ross-jsc.ru
//������: 2010.09.14 18:18
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#include <axis_error.h>
#include <axis_ssl_httpi_server_error.h>
#include <axis_ssl_httpi_file_download.h>
#include <axis_httpi_tags.h>

#include <dispatcher_lib/dispatcher_lib_resource.h>
#include <dispatcher_lib/debug.h>

#include <adapter/interface.h>
#include <adapter/log.h>
#include <adapter/recorder.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif

static void adapter_inteface_log_cath_on_cancel(struct Recorder_s **recorder)
{

	if( !recorder || !(*recorder) )
	{
		DBGERR("%s:%i: Invalid Argumentn\n",__func__,__LINE__);
		return ;
	}

	if( (*recorder)->usecount > 0)
	{
		AdapterRecorderRelease(*recorder);
	}

	DBGERR( "%s: Thread was canceled\n", __func__ );
}

/**
 * @brief �������, ������� ����� �������� ������ �������� 0 �����
 *
 * @param Buffer - ��������� �� �����
 * @param Size - ������ ������� ������
 * @return void
 */
static void workaroundzerobytes( char *Buffer, size_t Size )
{
	//�� ���� � ������ 0 ���� - � ���� ����� ��� ����� 0x0d - ������ ��� �� 0x0a
	//� ���� ����� ���� ����� 0x0a �������� �� 0x0d
	size_t i;
	size_t count = 0;

	for( i = 0; i < Size; i++ )
	{
		if( !Buffer[i] && i && i < Size - 1 && Buffer[ i - 1 ] == 0x0d && Buffer[ i + 1 ] == 0x0a )
		{
			Buffer[ i ] = 0x0a;
			Buffer[ i + 1 ] = 0x20;
			count++;
		}
		if( !Buffer[i] )
		{
			Buffer[i] = 0x20;
			if( !count )
			{
				DBGERR( "%s: Change 0x00 <-> 0x20\n", __func__ );
#ifdef CONFIG_ENCODER_DEBUG_MSG
				DispatcherLibDebugPrintBuffer( (uint16_t *)Buffer, Size >> 1 );
#endif
			}
			count++;
		}
		pthread_testcancel();
	}
	if( count )
		DBGERR( "%s: Change 0x00 <-> 0x0a Counts: %zu\n", __func__, count );
}

//������� ������ ���������� ����� � �����
//����:
// sock - ��������� �� ��������� ��������� ������ ����� ������� ����������
// name - ��������� �� ������ � ������ �����
// TCount - ��������� �� ������� ������
//�������:
// ���������� ���������� ����
// 0 - ���� ����� �� ����������
// ����� 0 � ������ ������
//����������:
// HTTP ���������� �� ����������, ������ ������
static int adapter_log_upload_raw_file( axis_ssl_wrapper_ex_sock *sock,
							const char *name, unsigned int *TCount )
{
	uint32_t nTCount;

	if( !sock || !name || !TCount )
	{
		DBGERR( "LogUploadEawFile: Invalid argument\n" );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	int fl = open( name, O_RDONLY );

	if( fl <= 0 )
	{
		DBGERR( "%s: Can't open file\n", __func__ );
		return 0; //�� ������ ������� �� � ���� � ���
	}


    char buffer[32000]; //����� ��� ������
    int ret = 0;
	int rCount;
    int rcount = 0;

	//���� ��������
	while( ( rCount=read( fl, buffer, sizeof( buffer ) ) ) > 0 )
	{
		DBGLOG( "%s: Try write %i bytes to socket %i\n", __func__, rCount, sock->sock );

		//ATTENTION - ����� �������� ������ �������
		workaroundzerobytes( buffer, rCount );

		ret = axis_ssl_wrapper_ex_write_tcount( sock, buffer, rCount, 256, TCount, &nTCount ); //TODO - ���������

		if( ret < 0 )
		{
			DBGERR( "%s: OutPut Data FAILED! (ret = %d)\n", __func__, ret );
		}

		if( ret < 0 )
		{
			//�� ��������� �������� � �����
			DBGERR( "%s: Can't write to socket\n", __func__ );
			close( fl );
			return -AXIS_ERROR_CANT_WRITE_SOCKET;
		}

		rcount += rCount;
	}

	if( rCount < 0 )
	{
		DBGERR( "%s: Can't read from log file\n", __func__ );
		close( fl );
		return -AXIS_ERROR_CANT_READ_DEVICE;
	}

	close( fl );

    return rcount;


}


//������� ������ ���� ������ � �����!
//����:
// sock - ��������� �� ��������� SSL ������
// TCount - ��������� �� ������� ������
//�������:
// ���������� ���������� ���� ��� ����� 0 � ������ ������
//����������:
// ���������� ������ ����� ������ HTTP ��������� �� ����������
static int adapter_log_upload_raw( axis_ssl_wrapper_ex_sock *sock, unsigned int *TCount )
{

	if( !sock || !TCount )
	{
		DBGERR( "%s:: Invalid argument\n", __func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	int cc = 0;
	int ret;

	//��������� ������ ����
	(*TCount)++;
	ret = adapter_log_upload_raw_file( sock, LOG_ADAPTER_PATH_5, TCount );

	if( ret < 0 )
	{
		DBGERR( "%s: Can't upload 1 file\n", __func__ );
		return ret;
	}

	cc += ret;

	//��������� ��������� ����
	(*TCount)++;
	ret = adapter_log_upload_raw_file( sock, LOG_ADAPTER_PATH_4, TCount );

	if( ret < 0 )
	{
		DBGERR( "%s: Can't upload 2 file\n", __func__ );
		return ret;
	}

	cc += ret;

	//��������� ���������
	(*TCount)++;
	ret = adapter_log_upload_raw_file( sock, LOG_ADAPTER_PATH_3, TCount );

	if( ret < 0 )
	{
		DBGERR( "%s: Can't upload 3 file\n", __func__ );
		return ret;
	}

	cc += ret;

	//��������� ���������
	(*TCount)++;
	ret = adapter_log_upload_raw_file( sock, LOG_ADAPTER_PATH_2, TCount );

	if( ret < 0 )
	{
		DBGERR( "%s: Can't upload 4 file\n", __func__ );
		return ret;
	}

	cc += ret;

	//��������� ��������� - ����� ������
	(*TCount)++;
	ret = adapter_log_upload_raw_file( sock, LOG_ADAPTER_PATH_1, TCount );

	if( ret < 0 )
	{
		DBGERR( "%s: Can't upload 4 file\n", __func__ );
		return ret;
	}

	cc += ret;
	DBGLOG( "%s: Upload done\n", __func__ );

	return cc;

}

//������� ������������ ������ �����
//����:
// name - ��������� �� ������ � ������ �����
//�������:
// ������ ����� � ������
// 0 - ���� ���� �� ����������
//����������:
// ����� ������ 2�� �� ������������
static off64_t adapter_log_get_file_size( const char *name )
{

	if( !name )
	{
		DBGERR( "%s: Invalid Argument\n", __func__ );
		return 0;
	}

	struct stat ms;

	if( stat( name, &ms ) )
	{
		//�� �� ������ ������� ���� - ����� ���
		DBGERR( "%s: Can't Stat file\n", __func__ );
		return 0;
	}

	return ms.st_size;
}


//������� �������� ������� ���������� ������
//�������:
// ���������� ���� ������� ����������� �������
static off64_t adapter_log_get_files_size( void )
{

	off64_t ret = 0;

	ret += adapter_log_get_file_size( LOG_ADAPTER_PATH_1 );
	ret += adapter_log_get_file_size( LOG_ADAPTER_PATH_2 );
	ret += adapter_log_get_file_size( LOG_ADAPTER_PATH_3 );
	ret += adapter_log_get_file_size( LOG_ADAPTER_PATH_4 );
	ret += adapter_log_get_file_size( LOG_ADAPTER_PATH_5 );

	return ret;
}


//������� ������ ��� ����� � �����
//����:
// sock - ��������� �� ��������� SSL ������
// TCount - ��������� �� ������� ����� ������
//�������:
// ���������� ���������� ���� ��� ����� 0 � ������ ������
static int adapter_log_upload( axis_ssl_wrapper_ex_sock *sock, unsigned int *TCount )
{

	if( !sock || !TCount )
	{
		DBGERR( "%s: Invalid Argument\n", __func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	(*TCount)++;
	//������������ ���� ���������� ������ ����� ����� ������ ����������
	//���������

	//�� ������ ���������� ������������ ��������� � �������
	//���� �� � ������ ����� ����� � ��� ����

	off64_t ret = adapter_log_get_files_size();

	if( ret < 0 )
	{
		DBGERR( "%s: Can't get file size\n", __func__ );
		goto end;
	}

	if( !ret )
	{
		ret = axis_ssl_httpi_server_send_error( sock, AXIS_SSL_HTTPI_ERROR__404_NOT_FOUND );
		goto end;
	}

	ret = axis_ssl_file_download_send_header( sock, ret,
			AXIS_HTTPI_CONTENT_TEXT_PLAN );

	if( ret < 0 )
	{
		DBGERR( "%s: Can't send log header\n", __func__ );
		goto end;
	}

	(*TCount)++;
	ret = adapter_log_upload_raw( sock, TCount );

	if( ret < 0 )
	{
		DBGERR( "%s: Can't send log body\n", __func__ );
		goto end;
	}

	DBGLOG( "%s: Log upload done\n", __func__ );

end:

	return ret;
}

/**
 * @brief �������, ������� ����� �������, ���� ������� ����� �������� ������� �������� �������
 *
 * @param data - ��������� �� �����, ��� �������� ���� ����������� ���
 * @return void
 **/
static void UploadLogCancelCallback( void *data )
{
	AdapterGlobalStatusStruct_t *Status = AdapterGlobalGetStatus();

	if( !data )
	{
		DBGERR( "%s: Invalid argument\n", __func__ );
		return;
	}

	if( !Status )
	{
		DBGERR( "%s: Can't get Global adapter status\n", __func__ );
		return;
	}

	DBGERR( "%s: Upload Logfile.txt page was canceled\n", __func__ );

	Status->icangosleep = *( ( int * )data );

}

//������� ������� �������� �������� log.txt
//����
//	sock - ��������� �� ��������� ������ ����� ������� ��������� ����������
//	Requests - ��������� �� ��cc�� �������� �������
//	Count - ���������� ��������� � ������ �������
//  tCount - ��������� �� ������� ������ ������� ������ ������� ���� ������� ����������
//�������
//���������� ���� ���������� ������� ��� < 0 � ������ ������
int AdapterInterfaceLogTxtPrint( axis_ssl_wrapper_ex_sock *sock,
								 axis_httpi_request *Requests,
								 int Count,
								 unsigned int *tCount )
{
	int ret;
	int iCanGoSleep;
	AdapterGlobalStatusStruct_t *Status = AdapterGlobalGetStatus();

	if( !sock || !Requests || Count <= 0 )
	{
		DBGERR( "%s: Invalid argument\n", __func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}


	if( !Status )
	{
		DBGERR( "%s: Can't get Global adapter status\n", __func__ );
		return -AXIS_ERROR_INVALID_VALUE;
	}

	//�� ������ ������������� ����� ������
	Status->LastInterfaceQuire = time( NULL );

	//�������� ������� �������� ������� ����� �������� ����������
	//������ ����� - ��� �� ������� �� ����� �� ��������� ��������������� ����
	//������ ������� �����, � ����� ������ �������
	iCanGoSleep = Status->icangosleep;
	Status->icangosleep = 0; //�� ����� ���� ���� �������� ������

	//�������� �� ��, ��� ���� ������� ����� ������� ������� tCount
	//��� ��� ����� ����� �������� ������� - ������ ����, ��� �� ��� ������ ������� ���� �������
	pthread_cleanup_push( UploadLogCancelCallback, &iCanGoSleep );

	( *tCount )++;
	ret = adapter_log_upload( sock, tCount );
	( *tCount )++;

	//������ ����������� �������������� �����
	pthread_cleanup_pop( 0 );

	//������ ���� �������
	Status->icangosleep = iCanGoSleep;

	//��� ��� �������, ��� �� ���� ����� � ������� �������
	Status->LastInterfaceQuire = time( NULL );

	return ret;
}

//������� ������� �������� �������� log.txt
//����
//	sock - ��������� �� ��������� ������ ����� ������� ��������� ����������
//	Requests - ��������� �� ��cc�� �������� �������
//	Count - ���������� ��������� � ������ �������
//  tCount - ��������� �� ������� ������ ������� ������ ������� ���� ������� ����������
//�������
//���������� ���� ���������� ������� ��� < 0 � ������ ������
int AdapterInterfaceLogDspTxtPrint(
        axis_ssl_wrapper_ex_sock *sock,
        axis_httpi_request *Requests,
        int Count,
        unsigned int *tCount )
{

	if(!sock || !Requests || !tCount)
	{
		DBGERR("Invalid Argument\n");
		return -1;

	}

	AdapterGlobalStatusStruct_t *Status = AdapterGlobalGetStatus();

	struct Recorder_s **recorder = NULL;

	DBGLOG( DISPATCHER_LIB_DEBUG__START );
// DispatcherLibPrintSystemInfo();

	int ret = 0;


	ret = AdapterInterfaceGetRecorderBySN(Requests,Count,Status,&recorder);

	if ( ret == -AXIS_ERROR_INVALID_ANSED )
	{

		DBGLOG( "%s: No SN from request - Adapter Status\n", __func__ );
		//��� ��������� ������!!!

		ret = axis_ssl_httpi_server_send_error( sock, AXIS_SSL_HTTPI_ERROR__404_NOT_FOUND );

		if ( ret < 0 )
		{
			DBGERR( "%s: Can't print server error\n", __func__ );
		}

		return ret;

	}
	else if ( ret < 0 )
	{
		DBGERR( "%s: Can't Check SN from request\n", __func__ );
		return ret;
	}
	else if ( ret == 1 && (*recorder))
	{
// 		DBGLOG( "%s: SN equalent - Recorder Status %i\n", __func__,(*recorder)->hen );
		//�������� ����� ������
		pthread_cleanup_push(( void* )adapter_inteface_log_cath_on_cancel, recorder );

		ret = AdapterRecorderPrintLogs( sock, *recorder,  tCount);

		pthread_cleanup_pop( 0 );
		
		if( ret == -AXIS_ERROR_NOT_INIT )
		{
			//���������� ������������ ��� ������� ����� ����������� �������� �������� ������
			DBGLOG( "%s: Log DSP file print in upload\n", __func__ );
			ret = AdapterInterfaceUploadPrint( sock, Requests, Count, tCount );
		}

		if( ret < 0 )
		{
			DBGERR( "%s: Can't print recorder logs\n", __func__ );
		}
		
		AdapterRecorderRelease(*recorder);
	}
	else/* if(Status->GlobalDevice_t.dmdev_dm3xx & DM_DM_WORK_SEPARATE)*/
	{

			//�������� ����� �� ������
			DBGLOG( "%s: SN No equalent - 404 Error\n", __func__ );
			ret = axis_ssl_httpi_server_send_error( sock, AXIS_SSL_HTTPI_ERROR__404_NOT_FOUND );

			if ( ret < 0 )
			{
				DBGERR( "%s: Can't print server error\n", __func__ );
			}

			return ret;


	}


	return ret;


}

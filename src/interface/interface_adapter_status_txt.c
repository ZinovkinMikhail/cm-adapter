//������� ������� ���������� �� ������ ������ ��������

//�����: ��������� ����� ��� "����"
//������: 2010.09.16 12:46
//�����: i.podkolzin@ross-jsc.ru


#include <axis_error.h>
#include <axis_ssl_httpi_server_error.h>
#include <axis_ssl_httpi_file_download.h>
#include <axis_httpi_tags.h>
#include <axis_sprintf.h>
#include <dispatcher_lib/dispatcher_lib_resource.h>
#include <dispatcher_lib/command.h>
#include <dispatcher_lib/sprintf_ini_files.h>

#include <adapter/printf_ini_files.h>
#include <adapter/interface.h>
#include <adapter/new_command.h>
#include <adapter/resource.h>
#include <adapter/recorder.h>


//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
//#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>

#else
#define DBGLOG( ... )
#endif

//������� ������� �������� �������� status.txt
//����
//	sock - ��������� �� ��������� ������ ����� ������� ��������� ����������
//	Requests - ��������� �� ��cc�� �������� �������
//	Count - ���������� ��������� � ������ �������
//  tCount - ��������� �� ������� ������ ������� ������ ������� ���� ������� ����������
//	Status - ��������� �� ���������� ��������� ������ ��������
//�������
//���������� ���� ���������� ������� ��� < 0 � ������ ������
int AdapterInterfaceAdapterStatusTxtPrint(
        axis_ssl_wrapper_ex_sock *sock,
        axis_httpi_request *Requests,
        int Count,
        unsigned int *tCount,
        AdapterGlobalStatusStruct_t *Status )
{

	char send_buffer[ DISPATCHER_LIB_BUFFFER_SIZE ];
	//int ret;
	char *b = send_buffer;
	int cc = 0;
	int lcc = 0;
	size_t size = sizeof( send_buffer );

	if( !sock || !Requests || Count <= 0 || !Status )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	DBGLOG( DISPATCHER_LIB_DEBUG__START );


	//����� ������� ��������� ��������� � ������� ��� ��������
	//�� ����� ���� ������ ������� ������������ ��������
	//� ��� � ��� - ��� ����� ��������� - ����� ������ ������ ���������� ����� ��������� ������������ ���������� ����������
	unsigned int CommandMask = Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature >> DM_RECORDER_COMMAND_MASK_SHIFT;

	if( !CommandMask )
	{
		DBGERR( "%s: Device not support any ������� command. Command mask is empty. Only Read support\n", __func__ );
		//�� ����� ���� ��� �� ������ - ��� ��� ���� ������� ������ - �� �� �� � ����� ���������

		//NOTE - ��� ������������� �� ������� ����������
		CommandMask = ADAPTER_INTERFACE_COMMAND__REBOOT;
	}

	//������� ��������� ��������� ������������ ��� ������� ��������
	//�� ����� ����� ����� � ����� - �� ��������
	CommandMask &= ~( ADAPTER_INTERFACE_COMMAND__NETSAVE | ADAPTER_INTERFACE_COMMAND__NETSAVE2 | ADAPTER_INTERFACE_COMMAND__NETAPPLY );

	DBGLOG("%s: %i field\n",__func__,__LINE__);

	//������ ���� ��������� �� ��� ��� ������ � �������
	lcc = AdapterCommandWorkRequest(
	              b + cc, size,
	              Requests,
	              Count,
	              CommandMask,
	              Status, NULL, sock );

	cc	   += lcc;
	size   -= lcc;

	if( size <= 0 ) goto end;

	DBGLOG("%s: %i field\n",__func__,__LINE__);
	//��� �������� �� ������ ����������

	//netinfo
	//�������� ��� ����
	lcc = AdapterIniFileSprintfStatusNetInfo(
	              b + cc,
	              size,
	              Status,
	              axis_httpi_get_caserequest_value(
	                      Requests,
	                      Count,
	                      AXIS_HTTPI_CONTENT_CLIENT_ID ), NULL ); //��� ���������� - ��� ������

	cc	   += lcc;

	size   -= lcc;

	if( size <= 0 ) goto end;

	DBGLOG("%s: %i field\n",__func__,__LINE__);

	//DeviceList
	lcc = AdapterIniFileSprintfStatusDeviceList(
	              b + cc,
	              size,
	              Status );

	cc	   += lcc;

	size   -= lcc;

	if( size <= 0 ) goto end;

DBGLOG("%s: %i field\n",__func__,__LINE__);

	//Info
	//�������� ����
	lcc = AdapterIniFileSprintfStatusInfo(
	              b + cc,
	              size,
	              Status );

	cc	   += lcc;

	size   -= lcc;

	if( size <= 0 ) goto end;

	//� ������� �������� ���������� � ���������� ��� ��� ��� � ��� ���
	//� ����������� �� ���� � ���������� �� �� ��� ���
	//��� ���� ����, ���� � ������ ����� �� ������
	if( Status->GlobalDevice_t.dmdev_acc.dmctrl_feature & DM_DM_ACC_STATUS_IN_RECORDER )
	{
		DBGLOG( "%s: Acc status in recorder\n", __func__ );
		// - ����� �� ����������� ��� �� ������ ���� - ��� ��� ��������� ����� �� ������������
		lcc = AdapterRecorderPrintAccStatusIni( Status->Recorder.next, b+cc, size );
		cc	   += lcc;
		size   -= lcc;

		if( size <= 0 ) goto end;
	}

	else
	{
		DBGLOG( "%s: Acc status in adapter\n", __func__ );
		lcc = axis_ini_file_sprintf_status_all_acc(
				  b + cc,
				  size,
				  &Status->ACCStatus.AccStatus,
				  Status->ACCStatus.DCIntACCRecord,
				  Status->ACCStatus.DCExtACCRecord,
				  Status->ACCStatus.DCIntACCWork,
				  Status->ACCStatus.DCExtACCWork );

		cc	   += lcc;
		size   -= lcc;

		if( size <= 0 ) goto end;
	}
	
	//� ����� ��� ������ ����������� ������ ������� (�� ����)
	lcc = DispatcherLibIniFileSprintfStatusPower( b + cc, size, &Status->ACCStatus.AccStatus, Status->ACCStatus.Voltage );
	cc	   += lcc;
	size   -= lcc;


	//�������� ���������� ������
	lcc = AdapterIniFileSprintfStatusNet(
	              b + cc,
	              size,
	              Status->GlobalDataNotSave &
	              ADAPTER_NOT_SAVE_NETWORK_DATA );

	//��� ������ ���� �������� ����
	cc	   += lcc;

	size   -= lcc;

	if( size <= 0 ) goto end;


	lcc = AdapterIniFileSprintfStatusAlarm( b+cc,
						size,
						Status,
						Status->AdapterDevice,
						NULL);

	cc	   += lcc;
	size   -= lcc;

	if( size <= 0 ) goto end;

	//�������� ������ ����������� � �����
	lcc = AdapterIniFileSprintfStatusDevInfo( b + cc, size, Status->AdapterDevice, Status );

	cc	   += lcc;

	size   -= lcc;

	if( size <= 0 ) goto end;

	lcc = DispatcherLibEventPrintAll( b + cc, size, &Status->ExchangeEvents.Events, 1 );

	cc	   += lcc;

	size   -= lcc;

	if( size <= 0 ) goto end;

end:

	//���������� ��������
	send_buffer[ sizeof( send_buffer ) - 1 ] = 0x00;

	DBGLOG( DISPATCHER_LIB_DEBUG__DONE );

	return axis_ssl_buffer_text_file_download( sock, send_buffer, cc );

}

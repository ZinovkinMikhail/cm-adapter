//������� ��������� �������� mode ��� ���������� ����� HTTP

//�����: ��������� ����� ��� "����"
//�����: i.podkolzin@ross-jsc.ru
//������: 2010.09.15 13:50

#include <axis_error.h>
#include <axis_ssl_httpi_server_error.h>
#include <axis_ssl_httpi_file_download.h>

#include <dispatcher_lib/dispatcher_lib_resource.h>

#include <adapter/interface.h>
#include <adapter/txt_files.h>
#include <adapter/recorder.h>
//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif


static void adapter_inteface_mode_cath_on_cancel(struct Recorder_s **recorder)
{
	if( !recorder || !(*recorder) )
	{
		DBGERR("%s:%i: Invalid Argumentn\n",__func__,__LINE__);
		return ;
	}

	if( (*recorder)->usecount > 0)
	{
// 		DBGLOG("%s:%i:Decrement use rec count %i \n",__func__,__LINE__,(*recorder)->usecount);
		AdapterRecorderRelease(*recorder);
	}

	//	pthread_exit(NULL);


}

//������� ������� �������� �������� mode.txt
//����
//	sock - ��������� �� ��������� ������ ����� ������� ��������� ����������
//	Requests - ��������� �� ��cc�� �������� �������
//	Count - ���������� ��������� � ������ �������
//  tCount - ��������� �� ������� ������ ������� ������ ������� ���� ������� ����������
//�������
//���������� ���� ���������� ������� ��� < 0 � ������ ������
int AdapterInterfaceModeTxtPrint(
        axis_ssl_wrapper_ex_sock *sock,
        axis_httpi_request *Requests,
        int Count,
        unsigned int *tCount )
{
	int ret = 0;

	char send_buffer[ DISPATCHER_LIB_BUFFFER_SIZE ];

	int lcc = 0;

	AdapterGlobalStatusStruct_t *Status = AdapterGlobalGetStatus();

	struct Recorder_s **recorder = NULL;

	if ( !sock || !Requests || Count <= 0 || ! Status )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	DBGLOG( DISPATCHER_LIB_DEBUG__START );
	

	
	//�������� ����� ������
	Status->LastInterfaceQuire = time( NULL );

	if ( Status->NoUsbIgnore )
	{
		ret = axis_ssl_httpi_server_send_error( sock,
		                                        333 );

		if ( ret < 0 )
		{
			DBGERR( "%s: Can't print server error\n", __func__ );
		}

		return ret;
	}

	//�������� �������� �����
	ret = AdapterInterfaceGetRecorderBySN( Requests, Count, Status, &recorder );

	if ( ret == -AXIS_ERROR_INVALID_ANSED )
	{
		//��� ��������� ������!!!
		DBGLOG( "%s: SN No equalent - 404 Error\n", __func__ );
		ret = axis_ssl_httpi_server_send_error( sock, AXIS_SSL_HTTPI_ERROR__404_NOT_FOUND );

		if ( ret < 0 )
		{
			DBGERR( "%s: Can't print server error\n", __func__ );
		}

	}
	else if ( ret < 0 )
	{
		DBGERR( "%s: Can't Check SN from request\n", __func__ );
		return ret;
	}
	else if ( ret == 1 && ( *recorder ) )
	{
		pthread_cleanup_push(( void* )adapter_inteface_mode_cath_on_cancel, recorder );
		DBGLOG( "%s: SN equalent - Recorder Status %i\n", __func__, ( *recorder )->hen );
		//�������� ����� ������
		lcc = AdapterTxtFileWorkModeTxt(
		              send_buffer,
		              sizeof( send_buffer ),
		              Requests,
		              Count,
		              Status, recorder );
		pthread_cleanup_pop( 0 );
		adapter_inteface_mode_cath_on_cancel(recorder);
	}
	else
	{
		//�������� ����� �� ������
		DBGLOG( "%s: SN No equalent - 404 Error\n", __func__ );
		ret = axis_ssl_httpi_server_send_error( sock, AXIS_SSL_HTTPI_ERROR__404_NOT_FOUND );

		if ( ret < 0 )
		{
			DBGERR( "%s: Can't print server error\n", __func__ );
		}

		return ret;

	}


	//���������� ��������
	//���������� ��� - ����������!
	//���������� ������ ���� ���������� ��� ��� �������
	send_buffer[ lcc ] = 0x00;

	//�������� ����� ������ - ��� ���, ��� ��� ��� ����� ����������
	//��� ������� ���������� ����������
	Status->LastInterfaceQuire = time( NULL );


	DBGLOG( DISPATCHER_LIB_DEBUG__DONE );



	return axis_ssl_buffer_text_file_download( sock, send_buffer, lcc );

}


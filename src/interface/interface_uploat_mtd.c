#ifdef TMS320DM355
#include <dm355_leds.h>
#endif


#include <sys/ioctl.h>

#include <axis_error.h>
#include <axis_ssl_httpi_server_error.h>
#include <axis_ssl_httpi_download.h>
#include <axis_httpi_tags.h>

#include <dispatcher_lib/dispatcher_lib_resource.h>
#include <dispatcher_lib/upload.h>
#include <dispatcher_lib/splicecalls.h>



#include <adapter/interface.h>
#include <adapter/mtdupltest.h>
#include <adapter/helper.h>
#ifdef TMS320DM365
#include <mtdseq_ioctl.h>
#endif

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
//#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif

#define UPLOAD_FILE_SIZE 104857600

#define SELECT_TIMEOUT 5

int AdapterInterfaceUploadMtdTest(
        axis_ssl_wrapper_ex_sock *sock,
        axis_httpi_request *Requests,
        int Count,
        unsigned int *tCount )
{
#ifdef TMS320DM365
	int ret = 0;
	int fd = 0;

 	int max_select = 0;
	fd_set rfds;

	struct timeval tv;
	int retval;

	FD_ZERO( &rfds );

	FD_SET( sock->sock, &rfds );

// 	max_select =  sock->sock;



//	axis_ssl_download_request UploadRequest; //��������� � ������� ������� ��������

	AdapterGlobalStatusStruct_t *Status = AdapterGlobalGetStatus();

	int command;

	if ( !sock || !Requests || Count <= 0 || !Status )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );

		ret = axis_ssl_httpi_server_send_error( sock,
		                                        AXIS_SSL_HTTPI_ERROR__409_CONFLICT );

		if ( ret < 0 )
		{
			DBGERR( "%s: Can't print server error\n", __func__ );
		}

		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	DBGLOG( DISPATCHER_LIB_DEBUG__START );


	DBGLOG( DISPATCHER_LIB_DEBUG__DONE );



	char *todo = axis_httpi_get_caserequest_value( Requests, Count,
	                AXIS_HTTPI_DIR_NAME_0 );


	if ( !todo )
	{
		DBGERR( "%s: Can't get block size, using standart 4096\n", __func__ );
		command = 0;
	}
	else
	{
		command = atoll( todo );

	}

	ret = axis_ssl_download_send_head( sock, UPLOAD_FILE_SIZE * 3, 1 );

	if ( ret < 0 )
	{
		DBGERR( "%s: Can't send head for upload\n", __func__ );
		return -1;
	}



	struct mtdseq_kernel_upload kups;

	kups.offset = 0;

	kups.size = UPLOAD_FILE_SIZE * 3;

	kups.socket = sock->sock;

	DBGERR( "%s:SOCKET -= = = = = %i\n", __func__, sock->sock );


	fd =  open( "/dev/mtdseq0", O_RDONLY );

	if ( fd <= 0 )
	{
		DBGERR( "%s:It Wasb't possible to open mtd\n", __func__ );
		ret = axis_ssl_httpi_server_send_error( sock,
		                                        AXIS_SSL_HTTPI_ERROR__409_CONFLICT );

		if ( ret < 0 )
		{
			DBGERR( "%s: Can't print server error\n", __func__ );
		}

		return -1;
	}

	if ( command )
	{
		if ( ioctl( fd, MEMSEQUPLOADSTART, &kups ) < 0 )
		{
			DBGERR( "%s:It wasn't possible to ioctl\n", __func__ );
			ret = axis_ssl_httpi_server_send_error( sock,
			                                        AXIS_SSL_HTTPI_ERROR__409_CONFLICT );

			if ( ret < 0 )
			{ 
				DBGERR( "%s: Can't print server error\n", __func__ );
			}

			close( fd );

			return -1;
		}
	}
	else
	{
		int ccc;

		if ( ioctl( fd, MEMSEQUPLOADSTOP, &ccc ) < 0 )
		{
			DBGERR( "%s:It wasn't possible to ioctl\n", __func__ );
			ret = axis_ssl_httpi_server_send_error( sock,
			                                        AXIS_SSL_HTTPI_ERROR__409_CONFLICT );

			if ( ret < 0 )
			{
				DBGERR( "%s: Can't print server error\n", __func__ );
			}

			close( fd );

			return -1;
		}
	}

	close( fd );

	while ( 1 )
	{
		FD_ZERO( &rfds );

		FD_SET( sock->sock, &rfds );
		tv.tv_sec = SELECT_TIMEOUT;
		tv.tv_usec = 0;

		retval = select( max_select + 1, &rfds, NULL, NULL, &tv );

		if ( retval )
		{
			DBGLOG( "%s:%i:Retval select = %i \n", __func__, __LINE__, retval );
			DBGERR("%s:Error on socket exit\n",__func__);
			return -1;
		}
		else
		{
			//�������
			DBGLOG( "%s:%i:rETVAL tIMEOUT = %i\n", __func__, __LINE__, retval );
		}
	}

//	close( fd );
#endif

	return 0;
}

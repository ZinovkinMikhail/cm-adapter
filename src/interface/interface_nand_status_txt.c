// //������� ��������� �������� ������ ���� ����� ��� ���������� ����� HTTP

#include <axis_error.h>
#include <axis_ssl_httpi_server_error.h>
#include <axis_ssl_httpi_file_download.h>
#include <axis_httpi_tags.h>
#include <axis_time.h>

#include <dispatcher_lib/dispatcher_lib_resource.h>
#include <dispatcher_lib/command.h>
#include <dispatcher_lib/sprintf_ini_files.h>

#include <adapter/interface.h>
#include <adapter/new_command.h>
#include <adapter/printf_ini_files.h>
#include <adapter/dispatcher.h>
#include <adapter/resource.h>
#include <adapter/recorder.h>
#include <adapter/acc.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
#undef	CONFIG_ENCODER_DEBUG_MSG

#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif


/**
 * @brief ������� ������ ������� ����� ������
 * 
 * @param sock ��������� �� ��������� ������ ����� ������� ��������� ����������
 * @param Requests ��������� �� ������ �������� �������
 * @param Count ���������� ��������� � ������ �������
 * @param tCount ��������� �� ������� ������ ������� ������ �������� ���� ������� ����������
 * @return ���������� ���� ���������� ������� ��� < 0 � ������ ������
 */
int AdapterInterfaceNandTestLogTxtPrint(	axis_ssl_wrapper_ex_sock *sock, axis_httpi_request *Requests, int Count,	unsigned int *tCount )
{
	int ret = 0;
	
	char send_buffer[DISPATCHER_LIB_BUFFFER_SIZE];
		
	int cc = 0;
	int lcc = 0;
	size_t size = sizeof( send_buffer );
	
	AdapterGlobalStatusStruct_t *Status = AdapterGlobalGetStatus();
	
	if ( !sock || !Requests || Count <= 0 || !Status )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}
	
	DBGLOG( DISPATCHER_LIB_DEBUG__START );
	
	if ( Status->NoUsbIgnore )
	{
		ret = axis_ssl_httpi_server_send_error( sock,
		                                        AXIS_SSL_HTTPI_ERROR__409_CONFLICT );
		if ( ret < 0 )
		{
			DBGERR( "%s: Can't print server error\n", __func__ );
		}
		return -AXIS_ERROR_INVALID_ANSED;
	}

	//��������, ������������ �� ���������� ������� ������ � ������� ������ ������ ����������� NAND � ���������
	uint32_t CommandMask = Status->GlobalDevice_t.dmdev_recorder.dmctrl_feature >> DM_RECORDER_COMMAND_MASK_SHIFT;

	if( !CommandMask )
	{
		DBGERR( "%s: Device not support any recorder command. Command mask is empty\n", __func__ );

		ret = axis_ssl_httpi_server_send_error( sock, AXIS_SSL_HTTPI_ERROR__404_NOT_FOUND );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't print server error\n", __func__ );
		}

		return -AXIS_ERROR_INVALID_VALUE;
	}

	if( !( CommandMask & ADAPTER_INTERFACE_COMMAND__NAND_TEST_GET_LOG ) )
	{
		DBGERR( "%s: Device not support NAND test log command\n", __func__ );
		ret = axis_ssl_httpi_server_send_error( sock, AXIS_SSL_HTTPI_ERROR__404_NOT_FOUND );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't print server error\n", __func__ );
		}

		return -AXIS_ERROR_INVALID_VALUE;
	}

#ifdef CONFIG_ENCODER_DEBUG_MSG
	axis_httpi_print_request( Requests, Count );
#endif

	DspRecorderExchangeFlashTestStatusStruct_t TestNandStatus;
	DspRecorderExchangeFlashTestStatusStruct_t* pTestNandStatus = &TestNandStatus;
	struct Recorder_s **recorder = NULL;

	// ���������� �������� ����� ����������
	ret = AdapterInterfaceGetRecorderBySN( Requests,Count,Status,&recorder );
	DBGLOG( "%s:%i: RECCORDER POIN = %p\n", __func__, __LINE__, recorder );

	if( ret == -AXIS_ERROR_INVALID_ANSED )
	{

		DBGLOG( "%s: No SN from request - Adapter Status\n", __func__ );
		//��� ��������� ������!!!

		ret = axis_ssl_httpi_server_send_error( sock, AXIS_SSL_HTTPI_ERROR__404_NOT_FOUND );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't print server error\n", __func__ );
		}

		return ret;

	}

	// �����
	else if( ret < 0 )
	{
		DBGERR( "%s: Can't Check SN from request\n", __func__ );

		//���� ��� ���������� � ����� �������� ������� - ���� ���������
		//404 ������ ���� ��� ������
		ret = axis_ssl_httpi_server_send_error( sock, AXIS_SSL_HTTPI_ERROR__404_NOT_FOUND );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't print server error\n", __func__ );
		}

		return ret;
	}

	// �������� �������� �����, �������� ����_����
	else
	{
		if( AdapterRecorderSendCommand( Status, *recorder,
										ADAPTER_INTERFACE_COMMAND__NAND_TEST_GET_LOG,
										( uint8_t * ) pTestNandStatus,
										sizeof( DspRecorderExchangeFlashTestStatusStruct_t ) ) < 0 )
		{
			DBGERR( "%s: It wasn't possible to send command to recorder\n",__func__ );
			return -1;
		}

		lcc = AdapterIniFileSprintfNandTestGetLog( send_buffer, sizeof( send_buffer ), pTestNandStatus );
		cc	   += lcc;
		size   -= lcc;
		DBGLOG( "%s:%i:Ready to print\n",__func__,__LINE__ );
	}

	//���������� ��������
	//���������� ��� - ����������!
	//���������� ������ ���� ��������� ��� ��� �������
	send_buffer[ sizeof( send_buffer ) - 1 ] = 0x00;

	DBGLOG( DISPATCHER_LIB_DEBUG__DONE );

	return axis_ssl_buffer_text_file_download( sock, send_buffer, cc );
	
}

//������� ��������� �������� network.txt ���������� ����� HTTP

//�����: ��������� ����� ��� "����"
//�����: i.podkolzin@ross-jsc.ru
//������: 2010.09.14 18:18

#include <axis_error.h>
#include <axis_ssl_httpi_server_error.h>
#include <axis_ssl_httpi_file_download.h>
#include <axis_httpi_tags.h>

#include <dispatcher_lib/dispatcher_lib_resource.h>

#include <adapter/interface.h>
#include <adapter/txt_files.h>
#include <adapter/recorder.h>

#include <dispatcher_lib/dispatcher_lib_resource.h>

#include <adapter/interface.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
//#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif

static void adapter_inteface_network_cath_on_cancel( struct Recorder_s **recorder )
{
	if( !recorder || !( *recorder ) )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return ;
	}

	if( ( *recorder )->usecount > 0 )
	{
// 		DBGLOG("%s:%i:Decrement use rec count %i \n",__func__,__LINE__,(*recorder)->usecount);
		AdapterRecorderRelease( *recorder );
	}

//	pthread_exit(NULL);

}

//������� ������� �������� �������� network.txt
//����
//	sock - ��������� �� ��������� ������ ����� ������� ��������� ����������
//	Requests - ��������� �� ��cc�� �������� �������
//	Count - ���������� ��������� � ������ �������
//  tCount - ��������� �� ������� ������ ������� ������ ������� ���� ������� ����������
//�������
//���������� ���� ���������� ������� ��� < 0 � ������ ������
int AdapterInterfaceNetworkTxtPrint(
        axis_ssl_wrapper_ex_sock *sock,
        axis_httpi_request *Requests,
        int Count,
        unsigned int *tCount )
{

	int ret = 0;

	char send_buffer[ DISPATCHER_LIB_BUFFFER_SIZE ];

	int lcc = 0;
	int WorkCommand = 0; //������������ �� �� �������??? ���� ���

	AdapterGlobalStatusStruct_t *Status = AdapterGlobalGetStatus();
	struct Recorder_s **recorder = NULL;

	if( !sock || !Requests || Count <= 0 || ! Status )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	DBGLOG( DISPATCHER_LIB_DEBUG__START );

#ifdef CONFIG_ENCODER_DEBUG_MSG
	DBGLOG( "%s: Network requests:\n", __func__ );
	axis_httpi_print_request( Requests, Count );
#endif

	//�������� ����� ������
	Status->LastInterfaceQuire = time( NULL );

	if( Status->NoUsbIgnore )
	{
		ret = axis_ssl_httpi_server_send_error( sock,
		                                        333 );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't print server error\n", __func__ );
		}

		return ret;
	}

	//�������� ������ �� ����������� ������, ������ ���� ���:
	//���� ��������� ������ ����� ��� - ��� ������� �� ������������ �������� network.txt � 
	// ������ ������ (����� ������, � ����� ������)
	//���� �������� ����� ����������� ���� - ��� ��� �� ������������ � ������ ������
	//� ���� �������� ����� ����������� ������ ������ ����������, �� �� ������ ������ ����������
	// ������ ��������, �� �� ��������� �� ��������������
	//���� �� �������� ��������� � �����������, �� ���� ����� ���� �������� ����� �� �����
	// �� � ���� ������ �� ������ ����� ������� �� ����� �������
	//��� ���� ���� �� �����, ���������� ���� �� ���������� � �������� �� �� �����������
	// ��� ������ ���� ��� �� ��� �����
	
		//�������� �������� ����� - �� �������� ��������
	ret = AdapterInterfaceCheckSNRequest( Requests, Count, Status );

	if( ret == -AXIS_ERROR_INVALID_ANSED )
	{
		//��� ��������� ������ �� � ��� � ���
		DBGLOG( "%s: No SN from request - Adapter full Network\n", __func__ );

		//�� ������������ �������� � ������ ������ (����)
		WorkCommand = 1;
	}
	else if( ret < 0 )
	{
		DBGERR( "%s: Can't Check SN from request\n", __func__ );
		return ret;
	}
	else if( ret == 1 )
	{
		DBGLOG( "%s: SN equalent - Adapter full Network\n", __func__ );

		//�������� ����� ������ - �� ������������ �������� � ������ ������ (����)
		WorkCommand = 1;
	}
	else
	{
		//�������� ����� �� ������ - �� ����� ��� �������� ����� ������ �� �����������
		ret = AdapterInterfaceGetRecorderBySN( Requests, Count, Status, &recorder );

		if( ret < 0 )
		{
			//���� �� ������ ��������, ��� ��� ��������� ������, ��� ��� ��� ������ ���� ������������ ����
			DBGERR( "%s: Can't Check SN from request\n", __func__ );
			return ret;
		}
		else if( ret == 1 )
		{
			adapter_inteface_network_cath_on_cancel( recorder );
			DBGLOG( "%s:%i: Sn Found from recorders - Adapter simple network\n", __func__, __LINE__ );
			//�������� ����� ������ ��� ������ �� ����������� - ��� ��������� ������
			WorkCommand = 0;
		}
		else
		{
			//�������� ����� �� ������
			DBGLOG( "%s: SN No equalent - 404 Error\n", __func__ );
			ret = axis_ssl_httpi_server_send_error( sock, AXIS_SSL_HTTPI_ERROR__404_NOT_FOUND );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't print server error\n", __func__ );
			}

			return ret;
		}

	}

	lcc = AdapterTxtFileWorkNetworkTxt(
								send_buffer,
								sizeof( send_buffer ),
								Requests,
								Count,
								Status, WorkCommand );

	//���������� ��������
	//���������� ��� - ����������!
	//���������� ������ ���� ���������� ��� ��� �������
	send_buffer[ lcc ] = 0x00;

	DBGLOG( DISPATCHER_LIB_DEBUG__DONE );

	return axis_ssl_buffer_text_file_download( sock, send_buffer, lcc );
}


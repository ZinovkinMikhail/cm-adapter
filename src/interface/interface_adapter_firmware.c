#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <sys/mount.h>
#include <sys/vfs.h>
#include <sys/types.h>
#include <wait.h>

#include <axis_error.h>
#include <axis_dmlibs_resource.h>
#include <axis_ssl_httpi_server_error.h>
#include <axis_error.h>
#include <axis_ssl_httpi_file_download.h>
#include <axis_httpi_tags.h>
#include <axis_time.h>
#include <axis_sprintf.h>

#include <dispatcher_lib/getopt.h>
#include <dispatcher_lib/dispatcher_lib_resource.h>

#include <adapter/interface.h>
#include <adapter/printf_ini_files.h>
#include <adapter/firmware.h>
#include <adapter/global.h>
#include <adapter/leds.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
//#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#include <unistd.h>
#else
#define DBGLOG( ... )
#endif


static int status;

#define TARGET_DIR_PATTERN		"/tmp/firmware"
#define UIMAGE_PATH_PATTERN_TMP		"/tmp/uImage"
#define VERSION_PATH_PATTERN_TMP	"/tmp/version.ini"

#define ROOTDISK_PATH_PATTERN_TMP	"/tmp/rootdisk.%s"
#define CONTENT_LENGHT			"Content-Length"
#define METHOD_NAME			"MethodName"
#define METHOD_POST			"POST"
#define METHOD_PUT			"PUT"

#define INI_SECTION_FILES		"[Files]"
#define INI_SECTION_STATUS		"[Status]"

#define VERSION_INI			"version.ini"
#define UIMAGE_INI			"uImage"
#define ROOTDISK_INI			"Rootdisk"
#define FILE_EXIST_INI			"Exist"
#define FILE_DO_NOT_EXIST_INI		"No"

#define STATUS_FIRMWARE_INI		"Status"
#define STATUS_WORK_INI			"Work"
#define STATUS_DONE_INI			"Done"
#define STATUS_WAIT_INI			"Wait"
#define STATUS_ERROR_INI		"Error"
#define STATUS_NOTHING_INI		"Nothing"
#define STATUS_REBOOT_INI		"Reboot"

#define VERSION_EXIST			0x001
#define UIMAGE_EXIST			0x010
#define ROOTDISK_EXIST			0x100

/**
 * @brief ������� �������� ������� �����
 * 
 * @param filename - ��������� �� ������ ������� ���� � �����
 * @return int: 0 - exist, <0 - fail
 */
int file_exists(const char* filename)
{
	struct stat st;
	
	if ( stat( filename, &st ) == -1 && errno == ENOENT)
	{
		return -1;
	}
	
	return 0;
}

// ������� �������� ������� ������ ��������
//
// @param dev - ��� ��������
//
// @return int: ����� ���������� ������
int check_frimware( int dev )
{
	int ret;
	unsigned int res = 0;
	char rootdisk[ 20 ];

	const char *rootdiskName = DispatcherLibStringRootDiskAdapterNameFromDevice( dev );

	ret = snprintf( rootdisk, sizeof( rootdisk ), ROOTDISK_PATH_PATTERN_TMP, rootdiskName );

	if( ret < 0 || ret >= sizeof( rootdisk ) )
	{
		DBGERR( "%s: Can't create root disk path\n", __func__ );
		return 0;
	}

	ret = file_exists( VERSION_PATH_PATTERN_TMP );
	if ( !ret )
		res |= VERSION_EXIST;

	ret = file_exists( UIMAGE_PATH_PATTERN_TMP );
	if ( !ret )
		res |= UIMAGE_EXIST;
	
	ret = file_exists( rootdisk );
	if ( !ret )
		res |= ROOTDISK_EXIST;

	return res;
}

/**
 * @brief ������� ��������� ��������� �������� �/� Wi-Fi
 * 
 * @param sock ...
 * @param Requests ...
 * @param Count ...
 * @param tCount ...
 * @return int
 */
int AdapterInterfaceFirmware( axis_ssl_wrapper_ex_sock *sock,
	      axis_httpi_request *Requests, int Count, 	unsigned int *tCount )
{
	
	DBGLOG("%s:%i: Wi - Fi wirmwrite start\n", __func__, __LINE__ );
	
	int i;
	int size = 0;
	int ret = 0;
	char buf[512];
	size_t size_ret;
	size_t size_buf = sizeof( buf );
	memset((void*)buf, 0, sizeof(buf));
	AdapterGlobalStatusStruct_t *Status = AdapterGlobalGetStatus();

	// �������� ������� ������
	ret = check_frimware( Status->GlobalDevice_t.dmdevice_type );
	
	// ���� ��� ����, �������� �������
	if( (ret & VERSION_EXIST) && ( ret & UIMAGE_EXIST) &&
						(ret & ROOTDISK_EXIST ) )
	{
		DBGLOG("%s:%i: All files loaded\n", __func__, __LINE__ );
		size_ret = axis_sprintf_s( buf + size,
						size_buf,
						INI_SECTION_FILES );
		size += size_ret;
		size_buf -= size;
		if ( size_buf < 0 )
			goto close;
		
		//������� ������� ������
		//� ��������� �� ������ � ������������� �����
		size_ret = axis_sprintf_t( buf + size,
						size_buf,
						VERSION_INI, FILE_EXIST_INI );
		size += size_ret;
		size_buf -= size;
		if ( size_buf < 0 )
			goto close;
		size_ret = axis_sprintf_t( buf + size,
						size_buf,
						UIMAGE_INI, FILE_EXIST_INI );
		size += size_ret;
		size_buf -= size;
		if ( size_buf < 0 )
			goto close;
		size_ret = axis_sprintf_t( buf + size,
						size_buf,
						ROOTDISK_INI, FILE_EXIST_INI );
		size += size_ret;
		size_buf -= size;
		if ( size_buf < 0 )
			goto close;
		
		size_ret = axis_sprintf_s( buf + size,
						size_buf,
						INI_SECTION_STATUS );
		size += size_ret;
		size_buf -= size;
		if ( size_buf < 0 )
			goto close;
		
		if ( status == 1 )
		{
			// ��� �����������
			size_ret =axis_sprintf_t( buf + size ,
						size_buf, 
						STATUS_FIRMWARE_INI,
						STATUS_WORK_INI );
			size += size_ret;
			size_buf -= size;
			if ( size_buf < 0 )
				goto close;
		}
		else if ( status < 0 )
		{
			DBGERR("%s:%i: Firmwrite is done. DON'T SEND ME A FILE\n",
																__func__,
																__LINE__ );
			
			// ��� ���������, ����������� ����
			size_ret = axis_sprintf_t( buf + size ,
						size_buf,
						STATUS_FIRMWARE_INI,
						STATUS_REBOOT_INI );
			size += size_ret;
			size_buf -= size;
			if ( size_buf < 0 )
				goto close;
		}
		else
		{
			
			// � �3, ��������, ��� �������� �� SD
			// ���� ��� �3, �� ������� ������������ SD
			// �������� ������ ����� � ������ /tmp ,�� ��� ������
			// ����������� ������� �� ������

			// ��� ����� � ������������� � ���������� dm_dev_...
			//�������� ������������
			status = 1;
			ret = 0;
			
			ret = AdapterFirmwarePrepareGo( 
							&Status->GlobalDevice_t,
							1 );
			
			if ( ret < 0 )
			{
				DBGLOG("%s:%i: It wasn't possible to firmwarite.\n", __func__, __LINE__ );
				status = -1;
				// ���������� ������
				size_ret = axis_sprintf_t( buf + size ,
							size_buf,
							STATUS_FIRMWARE_INI,
							STATUS_ERROR_INI );
				size += size_ret;
				size_buf -= size;
				// ������� �����, ��� ��������� ��� �����-�� ��������
				goto close;
			}
			else
			{
				DBGLOG("%s:%i: Firmwrite OK. I wanna be reboot.\n", __func__, __LINE__ );
				status = -1;
				// ������� ������ ������
				size_ret = axis_sprintf_t( buf + size,
							size_buf,
							STATUS_FIRMWARE_INI,
							STATUS_DONE_INI );
				size += size_ret;
				size_buf -= size;
				
				// ������ �� ��������� ���� ���
				for ( i = 0; i < 5; i++ )
				{
					AdapterLedFirmwareBlink( Status );
					sleep( 1);
				}
				
				// ������� �����, ��� �������
				goto close;
				
			}
		}
	}
	else
	{
		DBGLOG("%s:%i: Some firmwares files missing\n", __func__, __LINE__ );
		size_ret = axis_sprintf_s( buf + size,
						size_buf,
						INI_SECTION_FILES );
		size += size_ret;
		size_buf -= size;
		if ( size_buf < 0 )
			goto close;
		
		//������� ������� ������
		//� ��������� �� ������ � ������������� �����
		if ( ret & VERSION_EXIST )
		{
			size_ret = axis_sprintf_t( buf + size,
						size_buf,
						VERSION_INI,
						FILE_EXIST_INI );
			size += size_ret;
			size_buf -= size;
			if ( size_buf < 0 )
				goto close;
		}
		else
		{
			size_ret = axis_sprintf_t( buf + size,
						size_buf,
						VERSION_INI,
						FILE_DO_NOT_EXIST_INI );
			size += size_ret;
			size_buf -= size;
			if ( size_buf < 0 )
				goto close;
		}
		
		if ( ret & UIMAGE_EXIST )
		{
			size_ret = axis_sprintf_t( buf + size,
						size_buf,
						UIMAGE_INI,
						FILE_EXIST_INI );
			size += size_ret;
			size_buf -= size;
			if ( size_buf < 0 )
				goto close;
		}
		else
		{
			size_ret = axis_sprintf_t( buf + size,
						size_buf,
						UIMAGE_INI,
						FILE_DO_NOT_EXIST_INI );
			size += size_ret;
			size_buf -= size;
			if ( size_buf < 0 )
				goto close;
		}
		
		if ( ret & ROOTDISK_EXIST )
		{
			size_ret = axis_sprintf_t( buf + size,
						size_buf,
						ROOTDISK_INI,
						FILE_EXIST_INI );
			size += size_ret;
			size_buf -= size;
			if ( size_buf < 0 )
				goto close;
		}
		else
		{
			size_ret = axis_sprintf_t( buf + size,
						size_buf,
						ROOTDISK_INI,
						FILE_DO_NOT_EXIST_INI );
			size += size_ret;
			size_buf -= size;
			if ( size_buf < 0 )
				goto close;
		}
		
		size_ret = axis_sprintf_s( buf + size,
						size_buf,
						INI_SECTION_STATUS );
		size += size_ret;
		size_buf -= size;
		if ( size_buf < 0 )
			goto close;
		
		if( (ret & VERSION_EXIST) || ( ret & UIMAGE_EXIST) ||
						(ret & ROOTDISK_EXIST ) )
		{
			size_ret = axis_sprintf_t( buf + size,
						size_buf,
						STATUS_FIRMWARE_INI,
						STATUS_WAIT_INI );
			size += size_ret;
			size_buf -= size;
			if ( size_buf < 0 )
				goto close;
		}
		else
		{
			size_ret = axis_sprintf_t( buf + size,
						size_buf,
						STATUS_FIRMWARE_INI,
						STATUS_NOTHING_INI );
			size += size_ret;
			size_buf -= size;
			if ( size_buf < 0 )
				goto close;
		}
		
	}
close:
// 	if ( flfd > 0 )
// 		close( flfd );
	
	DBGLOG("%s:%i: Wi - Fi wirmwrite done\n", __func__, __LINE__ );
	
	return axis_ssl_buffer_text_file_download( sock, buf, size );
}

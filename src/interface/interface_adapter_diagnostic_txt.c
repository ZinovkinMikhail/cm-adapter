//������� ��������� �������� log.txt ���������� ����� HTTP

//�����: ��������� ����� ��� "����"
//�����: i.podkolzin@ross-jsc.ru
//������: 2010.09.14 18:18

#include <axis_error.h>
#include <axis_ssl_httpi_file_download.h>

#include <dispatcher_lib/dispatcher_lib_resource.h>

#include <adapter/global.h>
#include <adapter/diagnostic.h>
#include <adapter/printf_ini_files.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
//#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif


//������� ������� �������� �������� network.txt
//����
//	sock - ��������� �� ��������� ������ ����� ������� ��������� ����������
//	Requests - ��������� �� ������ �������� �������
//	Count - ���������� ��������� � ������ �������
//  tCount - ��������� �� ������� ������ ������� ������ �������� ���� ������� ����������
//�������
//���������� ���� ���������� ������� ��� < 0 � ������ ������
int AdapterInterfaceDiagnosticTxtPrint (
	axis_ssl_wrapper_ex_sock *sock,
	axis_httpi_request *Requests,
	int Count,
	unsigned int *tCount )
{
	AdapterGlobalStatusStruct_t *Status = AdapterGlobalGetStatus();


	if( !sock || !Requests || Count <= 0 || !Status )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	char send_buffer[DISPATCHER_LIB_BUFFFER_SIZE];

	int cc = 0;
	int lcc = 0;
	size_t size = sizeof ( send_buffer );

	DBGLOG ( DISPATCHER_LIB_DEBUG__START );

	lcc = AdapterIniFileSprintfDiagnostic ( send_buffer, sizeof ( send_buffer ), Status );
	cc	   += lcc;
	size   -= lcc;
	
	DBGLOG ( "%s:%i:Ready to print\n", __func__, __LINE__ );

	send_buffer[ sizeof ( send_buffer ) - 1 ] = 0x00;

	DBGLOG ( DISPATCHER_LIB_DEBUG__DONE );

	return axis_ssl_buffer_text_file_download ( sock, send_buffer, cc );
}

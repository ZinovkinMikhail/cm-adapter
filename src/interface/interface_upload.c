//������� ��������� �������� ���������� �� �������� ������ ���������� ����� HTTP

//�����: ��������� ����� ��� "����"
//�����: i.podkolzin@ross-jsc.ru
//������: 2010.09.14 18:18
#include <fcntl.h>
#include <inttypes.h>

#include <axis_error.h>
#include <axis_ssl_httpi_server_error.h>
#include <axis_ssl_httpi_download.h>

#ifdef TMS320DM355
#include <dm355_leds.h>
#endif

#include <dispatcher_lib/dispatcher_lib_resource.h>
#include <dispatcher_lib/upload.h>

#include <adapter/dispatcher.h>
#include <adapter/interface.h>
#include <adapter/recorder.h>
#include <adapter/nand.h>
#if defined TMS320DM355 || defined TMS320DM365
#include <mtdseq_ioctl.h>
#endif

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif

#define FLASH_PEAK_SIZE 1032847360
#define MTD_BLOCK_SIZE 1048576

#ifdef NOT_USE
static off64_t StorageGetSeek( int Openflash )
{
	off64_t rSeek = 0;

	rSeek = lseek64( Openflash, 0, SEEK_CUR );

	if ( rSeek < 0 )
	{
		DBGERR( "%s: It wasn't posiible to get seek\n'", __func__ );
		return 0;
	}

	return rSeek;
}



static off64_t GetFlashSize( int Openflash, struct mtdseq_info_user* infoMtdSeq )
{
	if ( Openflash < 0 || !infoMtdSeq )
	{
		DBGLOG( "%s: Invalid argument\n", __func__ );
		return -1;
	}

	//struct mtdseq_info_user infoMtdSeq;
	off64_t flashsize = 0;

#if defined TMS320DM355 || defined TMS320DM365
	DBGLOG( "GET FLASH INFO\n" );

	memset( infoMtdSeq, 0 , sizeof( struct mtdseq_info_user ) );

	if ( ioctl( Openflash, MEMSEQINFO, infoMtdSeq ) < 0 )
	{
		DBGERR( "%s: It wasn't possible to ioct memseqinfo\n", __func__ );
		return -AXIS_ERROR_CANT_IOCTL_DEVICE;
	}

	flashsize = ( off64_t )infoMtdSeq->blocks * ( off64_t )infoMtdSeq->blocksize;

#endif
	return flashsize;
}

static int GetHeadInfoSize( int Openflash, struct mtdseq_info_flash_head* infoMtdSeq )
{
	if ( Openflash < 0 || !infoMtdSeq )
	{
		DBGLOG( "%s: Invalid argument\n", __func__ );
		return -1;
	}

#if defined TMS320DM355 || defined TMS320DM365
	DBGLOG( "GET FLASH INFO\n" );

	memset( infoMtdSeq, 0 , sizeof( struct mtdseq_info_user ) );

	if ( ioctl( Openflash, MEMSEQHEADINFO, infoMtdSeq ) < 0 )
	{
		DBGERR( "%s: It wasn't possible to ioct memseqinfo\n", __func__ );
		return -AXIS_ERROR_CANT_IOCTL_DEVICE;
	}


#endif
	return 0;
}
#endif

static void adapter_inteface_upload_cath_on_cancel(struct Recorder_s **recorder)
{
	if( !recorder || !(*recorder) )
	{
		DBGERR("%s:%i: Invalid Argumentn\n",__func__,__LINE__);
		return ;
	}

	if( (*recorder)->usecount > 0)
	{
// 		DBGLOG("%s:%i:Decrement use rec count %i \n",__func__,__LINE__,(*recorder)->usecount);
		AdapterRecorderRelease(*recorder);
	}

// 	pthread_exit(NULL);

}

int AdapterInterfaceUploadPrint(
        axis_ssl_wrapper_ex_sock *sock,
        axis_httpi_request *Requests,
        int Count,
        unsigned int *tCount )
{

	int ret = 0; //��� ��������
	int upload_type = 0;
	axis_ssl_download_request UploadRequest; //��������� � ������� ������� ��������
	struct Recorder_s **recorder = NULL;

	AdapterGlobalStatusStruct_t *Status = AdapterGlobalGetStatus();

	if ( !sock || !Requests || Count <= 0 || !Status )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );

		ret = axis_ssl_httpi_server_send_error( sock,
		                                        AXIS_SSL_HTTPI_ERROR__409_CONFLICT );

		if ( ret < 0 )
		{
			DBGERR( "%s: Can't print server error\n", __func__ );
		}

		return -AXIS_ERROR_INVALID_ARGUMENT;
	}
	
	DBGLOG( DISPATCHER_LIB_DEBUG__START );

	//�������� ����� ������
	Status->LastInterfaceQuire = time( NULL );
	

	
	//�������� �������� �����


	if(Status->GlobalDevice_t.dmdev_interface.dmctrl_ipresent)
	{
		if(Status->iplay)
		{
			ret = axis_ssl_httpi_server_send_error( sock,
		                                        AXIS_SSL_HTTPI_ERROR__409_CONFLICT );

			if ( ret < 0 )
			{
				DBGERR( "%s: Can't print server error\n", __func__ );
			}

			return 0;
		}
	}
	
	//��� ���� �������� ��� ������� ������ �� ���� ����� �������

	//��� ���� �������� ������ �� ������� ������ �� ���� ����� ������ �� ��������

	//��� ���� �������� ��� ������� �� ���� �� ����� �������� ������ ��������� ��� ������

	//���� �������� ������ ������� � ��� �� �� �� ����������

	if ( axis_ssl_download_parsing_http_request(
	                        Requests, Count,
	                        &UploadRequest ) < 0 )
	{
		DBGERR( "%s: Can't parsing upload request\n", __func__ );
		ret = axis_ssl_httpi_server_send_error( sock,
		                                        AXIS_SSL_HTTPI_ERROR__404_NOT_FOUND );

		if ( ret < 0 )
		{
			DBGERR( "%s: Can't print server error\n", __func__ );
		}

		return -AXIS_ERROR_INVALID_ANSED;
	}

	ret = AdapterInterfaceGetRecorderBySN(Requests,Count,Status,&recorder);

	if ( ret < 0 )
	{
		DBGERR( "%s: Can't Check SN from request\n", __func__ );
		return ret;
	}

	if ( !ret || !recorder )
	{
		//��� ��������� ������ ��� ��� ���������� �� ������ ������ �� ����������!

		ret = axis_ssl_httpi_server_send_error( sock,
		                                        AXIS_SSL_HTTPI_ERROR__404_NOT_FOUND );

		if ( ret < 0 )
		{
			DBGERR( "%s: Can't print server error\n", __func__ );
		}

		return -AXIS_ERROR_INVALID_ANSED;
	}

	//������ ��� ����������� ������ �������� � ������� ���������� �����
	pthread_cleanup_push(( void* )adapter_inteface_upload_cath_on_cancel, recorder );
	
	//���� ���� ������� ��� ���������� ������ ������ ����� ��������������� �
	//����� � ��� �� �������� ������� ������� ������
	if(Status->GlobalDevice_t.dmdev_nand.dmctrl_icontrol)
	{

		upload_type = 1;
		
		if ( UploadRequest.ftype == AXIS_SSL_DOWNLOAD_PAGE_NVELOP_TYPE ||
		UploadRequest.ftype == AXIS_SSL_DOWNLOAD_PAGE_NVELOP_USB_TYPE)
		{
			
			DBGLOG("%s: UPLOAAD   %i:%"PRIu64"    \n\n\n",__func__,__LINE__,Status->Nand.flashpeakstart);
			UploadRequest.from += Status->Nand.flashpeakstart;
			if(UploadRequest.to)
			{
				UploadRequest.to +=  Status->Nand.flashpeakstart;
			}
			else
			{
				 UploadRequest.to = Status->Nand.flashpeakend;
				 
				UploadRequest.full = 1;
			}
		}
	}
	//���� �� ���� � ������ ������� - ���� �������� �������� ������ ��
	//������� ���� �������� �������� HTTP ��������� ��� ��� �����
	//� ��� ����� ������ ��� �����!!!
	//��� ����� ������� ��������




	(*recorder)->recorder_data.private_data = &upload_type;
	adapter_inteface_upload_cath_on_cancel(recorder);
	ret = AdapterRecorderUploadStart(sock,&UploadRequest,*recorder,tCount);
	if ( ret < 0 )
	{
		DBGERR( "%s: Can't upload data to client\n", __func__ );
	}

	//adapter_inteface_cath_on_cancel(recorder);

	DBGLOG( DISPATCHER_LIB_DEBUG__DONE );

	pthread_cleanup_pop( 0 );
	
	if ( !ret )
	{
		ret = 1;
	}

	//�������� ����� ��������� ������ - ��� �� �� ������ ����� ����� �������� ������
	Status->LastInterfaceQuire = time( NULL );

	return ret;
}



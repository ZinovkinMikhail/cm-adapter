//������� ������� ����� �������� �� ��������� �������� �� ��������
//������� � ��� ������� ��� � �� �����

//������: 2010.09.15 16:03
//�����: ��������� ����� ��� "����"
//�����: i.podkolzin@ross-jsc.ru

#include <axis_error.h>
#include <axis_ssl_httpi_server_error.h>
#include <axis_ssl_httpi_file_download.h>
#include <dispatcher_lib/dispatcher_lib_resource.h>

#include <adapter/interface.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif


//������� ������� �������� �������� default ��������� �����
//�������, ��� � ��� ����� ���!!!
//����
//	sock - ��������� �� ��������� ������ ����� ������� ��������� ����������
//	Requests - ��������� �� ��cc�� �������� �������
//	Count - ���������� ��������� � ������ �������
//  tCount - ��������� �� ������� ������ ������� ������ ������� ���� ������� ����������
//�������
//���������� ���� ���������� ������� ��� < 0 � ������ ������
int AdapterInterfaceDefaultTxtPrint( 
				axis_ssl_wrapper_ex_sock *sock, 
				axis_httpi_request *Requests, 
				int Count,
				unsigned int *tCount )
{ 
#ifdef PREV
	int ret = 0;

	if( !sock || !Requests || Count <= 0  )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

    DBGLOG( DISPATCHER_LIB_DEBUG__START );
	

	char buffer[10];
#warning ������ �������� �� ���������� ���������
	ret = axis_ssl_buffer_text_file_download( sock, buffer, 10 );
	
	if( ret < 0 )
	{
		DBGERR("%s: Can't send to server\n",__func__);
		return axis_ssl_httpi_server_send_error( sock, AXIS_SSL_HTTPI_ERROR__404_NOT_FOUND );
	}
	
	
	
    DBGLOG( DISPATCHER_LIB_DEBUG__DONE );
	
	return ret;
#else
	int ret = 0;

	if( !sock || !Requests || Count <= 0  )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

    DBGLOG( DISPATCHER_LIB_DEBUG__START );

	ret = axis_ssl_httpi_server_send_error( sock, AXIS_SSL_HTTPI_ERROR__404_NOT_FOUND );

	if( ret < 0 )
	{
		DBGERR("%s: Can't print server error\n",__func__);
	}

    DBGLOG( DISPATCHER_LIB_DEBUG__DONE );

	return ret;
#endif
}



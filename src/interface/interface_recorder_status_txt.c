//������� ������� ���������� �� ������ ������ ���������

//�����: ��������� ����� ��� "����"
//������: 2010.09.16 12:46
//�����: i.podkolzin@ross-jsc.ru


#include <axis_error.h>
#include <axis_sprintf.h>
#include <axis_ssl_httpi_server_error.h>
#include <axis_ssl_httpi_file_download.h>
#include <axis_httpi_tags.h>
#include <axis_time.h>

#include <dispatcher_lib/dispatcher_lib_resource.h>
#include <dispatcher_lib/command.h>
#include <dispatcher_lib/sprintf_ini_files.h>

#include <adapter/interface.h>
#include <adapter/new_command.h>
#include <adapter/printf_ini_files.h>
#include <adapter/dispatcher.h>
#include <adapter/resource.h>
#include <adapter/recorder.h>
#include <adapter/acc.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif

//��� ������������ ������ �� ���������� �����
#define TEST_STACK
#undef TEST_STACK

#ifdef TEST_STACK
static void testPrintBuff(char* buf, size_t size)
{
    int i = 0;
    uint32_t address = 0;
     fprintf(stderr,"%04i:\t",address);
    for(;i< size;i++)
    {
        fprintf(stderr,"%02x ",buf[i]);
        if(i > 0 &&  !(i % 32))
        {
            address+=32;
            fprintf(stderr,"\n ");
            fprintf(stderr,"%04i:\t",address);
        }
    }
}
#endif

//������� ������� �������� �������� status.txt ��� ���������
//����
//	sock - ��������� �� ��������� ������ ����� ������� ��������� ����������
//	Requests - ��������� �� ��cc�� �������� �������
//	Count - ���������� ��������� � ������ �������
//  tCount - ��������� �� ������� ������ ������� ������ ������� ���� ������� ����������
//	Status - ��������� �� ���������� ��������� ������ ��������
//�������
//���������� ���� ���������� ������� ��� < 0 � ������ ������
// - AXIS_ERROR_TIMEOUT_EVENT - �� �������� �� �������� (409 ������)
// - AXIS_ERROR_BUSY_DEVICE - �� ��������� �� �������� (������� NoDevice)
int AdapterInterfaceRecorderStatusTxtPrint(
        axis_ssl_wrapper_ex_sock *sock,
        axis_httpi_request *Requests,
        int Count,
        unsigned int *tCount,
        AdapterGlobalStatusStruct_t *Status, struct Recorder_s **recorder )
{

	char send_buffer[ DISPATCHER_LIB_BUFFFER_SIZE ];
    

	int ret;

	char *b = send_buffer;
	int cc = 0;
	int lcc = 0;
	size_t size = sizeof( send_buffer );
    
	if ( !sock || !Requests || Count <= 0 || !Status || !(*recorder))
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}
#ifdef TEST_STACK
	int z = 0;
    for(z = 0;z < sizeof(send_buffer); z++)
        send_buffer[z] = 0xDD;
	//testPrintBuff(send_buffer,sizeof(send_buffer));
#endif
	DBGLOG( DISPATCHER_LIB_DEBUG__START );

	if ( Status->NoUsbIgnore )
	{
		ret = axis_ssl_httpi_server_send_error( sock,
		                                        AXIS_SSL_HTTPI_ERROR__409_CONFLICT );
		if ( ret < 0 )
		{
			DBGERR( "%s: Can't print server error\n", __func__ );
		}
		return -AXIS_ERROR_INVALID_ANSED;
	}


	//� ������������ ��� �������, ������� ����� ��������� ����������
	//������ ������� ����� ������� ��������� � �������� �������
	//� ������� ������

	unsigned int CommandMask = Status->GlobalDevice_t.dmdev_recorder.dmctrl_feature >> DM_RECORDER_COMMAND_MASK_SHIFT;

	if( !CommandMask )
	{
		DBGERR( "%s: Device not support any recorder command. Command mask is empty. Only Read support\n", __func__ );
		//�� ����� ���� ��� �� ������ - ��� ��� ���� ������� ������ - �� �� �� � ����� ���������
	}

	//������� ��� �������� - ��� � ������������ ��� �� �������� ����� ���� ���������
	//����� ���������� ��������� � ������ ������� (�� ���� ��� ������� ���� � �������� �������)
	//������ ������� �� ����, ���� ��� ��������� ���
	CommandMask |= (Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature >> DM_RECORDER_COMMAND_MASK_SHIFT) & (ADAPTER_INTERFACE_COMMAND__REBOOT | ADAPTER_INTERFACE_COMMAND__SLEEP);

	//������� ��������� ��������� ������������ ��� ������� ����������
	//�� ����� ����� ����� � ����� - �� ��������

	CommandMask &= ~( ADAPTER_INTERFACE_COMMAND__SAVE | ADAPTER_INTERFACE_COMMAND__APPLY );

	//������ ���� ���������� ������� ���� ��� ������ � �������� �� ����
	//� ������
	//���� ������� �� �������������� �� ���� � ����� �� ����
	//���� ������� ��� �� �� ������� ��� ��� ������� ������ � ����
	//�������� �� ����
	//���� �� �� ����� �������� ������� �� ������� ��� ��� ����������

	//���������� ���� � ����� ���������� �������
#ifdef CONFIG_ENCODER_DEBUG_MSG
	axis_httpi_print_request( Requests, Count );
#endif
	DBGLOG("%s: %i field\n",__func__,__LINE__);
	lcc = AdapterCommandWorkRequest(
	              b + cc, size,
	              Requests,
	              Count,
	              CommandMask,
	              Status,recorder,sock );

	cc	   += lcc;
	size   -= lcc;
	if ( size <= 0 ) goto end;

	ret = AdapterRecorderChacheRecorderStatus( &Status->GlobalDevice_t, *recorder );

	if( ret )
	{
		DBGERR( "%s: It wasn't possible to cache recorder status\n", __func__ );
		return ret;
	}
	
	DBGLOG( "%s:%i:Ready to print %i\n", __func__, __LINE__, Count );

	ret = AdapterAccGetStatus( Status );

	if( ret < 0 )
	{
		DBGERR( "%s: Can't get acc device status\n", __func__ );
		return ret;
	}

	//�������� ��� ���������
	//�������� ��� ����
	lcc = AdapterIniFileSprintfStatusNetInfo(
	              b + cc,
	              size,
	              Status,
	              axis_httpi_get_caserequest_value(
	                      Requests,
	                      Count,
	                      AXIS_HTTPI_CONTENT_CLIENT_ID ), *recorder ); //���� ���������� - ��������

	cc	   += lcc;

	size   -= lcc;

	if ( size <= 0 ) goto end;

	DBGLOG("%s:%i: Ready to print size = %i\n",__func__,__LINE__, cc );

	// 	//�������� ������
	lcc = AdapterIniFileSprintfStatusAlarm(
		              b + cc,
		              size,
		              Status,
		              Status->AdapterDevice,*recorder );
		cc	   += lcc;
		size   -= lcc;

	if ( size <= 0 ) goto end;

	DBGLOG("%s:%i: Ready to print size = %i\n",__func__,__LINE__, cc );

	
		
	//Info
	//�������� ���� ������ ����������
	lcc = AdapterRecorderSprintRecorderInfo( 
					b + cc,
					size,
					*recorder,
					Status->Build, Status->BuildTime );
	cc	   += lcc;
	size   -= lcc;

	if ( size <= 0 ) goto end;

		
	DBGLOG("%s:%i: Ready to print size=%i \n",__func__,__LINE__, cc);

	DBGLOG( "%s: Status->GlobalDataNotSave = %08x\n", __func__, Status->GlobalDataNotSave );

	//���������� �� ���������� - ����������� � �����
	lcc = AdapterIniFileSprintfStatusDevInfo( b + cc, size, Status->AdapterDevice, Status );
	cc	   += lcc;
	size   -= lcc;

	if ( size <= 0 ) goto end;

	DBGLOG("%s:%i: Ready to print size=%i \n",__func__,__LINE__, cc);

	//�������� ������� ������ �������
	lcc = DispatcherLibEventPrintAll( b + cc, size, &Status->ExchangeEvents.Events, 1 );
	cc	   += lcc;
	size   -= lcc;

	if ( size <= 0 ) goto end;

	DBGLOG("%s:%i: Ready to print size=%i \n",__func__,__LINE__, cc);

	//�������� ������ ���������� - ���� � ������
	lcc = AdapterRecorderPrintStatusIni(*recorder,b+cc,size);
	cc	   += lcc;

	size   -= lcc;

	if ( size <= 0 ) goto end;

	DBGLOG("%s:%i: Ready to print size=%i \n",__func__,__LINE__, cc);

	//�������� �������������� ����������
	lcc = AdapterRecorderPrintMoreInfo(b+cc,size,*recorder);
	cc	   += lcc;
	size   -= lcc;

	if ( size <= 0 ) goto end;

	DBGLOG("%s:%i: Ready to print size=%i \n",__func__,__LINE__, cc);

	//� ������� �������� ���������� � ���������� ��� ��� ��� � ��� ���
	//� ����������� �� ���� � ���������� �� �� ��� ���
	//��� ���� ����, ���� � ������ ����� �� ������
	if( Status->GlobalDevice_t.dmdev_acc.dmctrl_feature & DM_DM_ACC_STATUS_IN_RECORDER )
	{
		DBGLOG( "%s: Acc status in recorder\n", __func__ );
		// - ����� �� ����������� ��� �� ������ ���� - ��� ��� ��������� ����� �� ������������
		lcc = AdapterRecorderPrintAccStatusIni( *recorder,b+cc,size );
		cc	   += lcc;
		size   -= lcc;

		if( size <= 0 ) goto end;
	}

	else
	{
		DBGLOG( "%s: Acc status in adapter\n", __func__ );
		lcc = axis_ini_file_sprintf_status_all_acc(
				  b + cc,
				  size,
				  &Status->ACCStatus.AccStatus,
				  Status->ACCStatus.DCIntACCRecord,
				  Status->ACCStatus.DCExtACCRecord,
				  Status->ACCStatus.DCIntACCWork,
				  Status->ACCStatus.DCExtACCWork );

		cc	   += lcc;
		size   -= lcc;

		if( size <= 0 ) goto end;
	}
	
	//� ����� ��� ������ ����������� ������ ������� (�� ����)
	lcc = DispatcherLibIniFileSprintfStatusPower( b + cc, size, &Status->ACCStatus.AccStatus, Status->ACCStatus.Voltage );
	cc	   += lcc;
	size   -= lcc;

#ifdef TEST_STACK
    for(z = cc + 1 ; z < sizeof(send_buffer)-1;z++)
    {
        if(send_buffer[z] != 0xDD)
        {
                DBGERR("Error On %i %lu \n",z,sizeof(send_buffer));
                testPrintBuff(&send_buffer[z],sizeof(send_buffer)- z);
                abort();
                break;
        }
    }
#endif

	if ( size <= 0 ) goto end;
	
	DBGLOG("%s:%i: Ready to print size=%i \n",__func__,__LINE__, cc);

end:

	//���������� ��������
	//���������� ��� - ����������!
	//���������� ������ ���� ���������� ��� ��� �������
	send_buffer[ sizeof( send_buffer ) - 1 ] = 0x00;

	DBGLOG( DISPATCHER_LIB_DEBUG__DONE );
    ret = axis_ssl_buffer_text_file_download( sock, send_buffer, cc );
   
	return ret;

}

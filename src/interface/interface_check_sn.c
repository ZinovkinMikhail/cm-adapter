//������� �������� ��������� ������ �� ����������

//�����: ��������� ����� ��� "����"
//�����: i.podkolzin@ross-jsc.ru
//������: 2010.09.16 12:22

#include <string.h>

#include <axis_error.h>
#include <axis_ssl_httpi_server_error.h>
#include <axis_httpi_tags.h>
#include <pthread.h>
#include <dispatcher_lib/dispatcher_lib_resource.h>

#include <adapter/interface.h>
#include <adapter/recorder.h>
//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif


//������� ��������� ��������� ������ �� ����������
//����:
// Request - ��������� �� ������ ������� ��������
// Count - ���������� ��������� � ��������
// Status - ��������� �� ���������� ��������� ������� 
//�������:
// 0 - �������� ����� �� ������
// 1 - �������� ����� ������
// ����� 0 � ������ ������
//  -AXIS_ERROR_INVALID_ANSED - ����� ��� ��������� ������
int AdapterInterfaceCheckSNRequest( axis_httpi_request *Requests, size_t Count,
									AdapterGlobalStatusStruct_t *Status )
									
{

	if( !Status || !Requests || Count <= 0 )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

    DBGLOG( DISPATCHER_LIB_DEBUG__START );

    //���������� ������� ����� �������
    //���� �������� ����� ��� �� �� ������� ������ ��������
    char *value = axis_httpi_get_caserequest_value( Requests, Count, 
			    AXIS_HTTPI_DIR_NAME_0 );
    
	if( !value )
	{
		DBGLOG("%s: No SN from requests\n",__func__);
		return -AXIS_ERROR_INVALID_ANSED;
	}
	
	if( strcmp( value, Status->CurrentNetwork.SN ) != 0 )
	{
		DBGLOG("%s: SN no equal from request\n",__func__);
		DBGLOG("%s: Request: '%s'\n",__func__, value );
		DBGLOG("%s: Network: '%s'\n",__func__, Status->CurrentNetwork.SN );
		return 0;
	}
	
    DBGLOG( DISPATCHER_LIB_DEBUG__DONE );
	
	return 1;

}

int  AdapterInterfaceGetRecorderBySN(axis_httpi_request *Requests, size_t Count,
			                AdapterGlobalStatusStruct_t *Status, struct Recorder_s ***recorder )
{
	if ( !Requests || !Status || Count <= 0 )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	DBGLOG( DISPATCHER_LIB_DEBUG__START );

	//���������� ������� ����� �������
	//���� �������� ����� ��� �� �� ������� ������ ��������
	char *value = axis_httpi_get_caserequest_value( Requests, Count,
	                AXIS_HTTPI_DIR_NAME_0 );

	if ( !value )
	{
		DBGLOG( "%s: No SN from requests\n", __func__ );
		return -AXIS_ERROR_INVALID_ANSED;
	}

	uint64_t sn = strtoull(value,NULL,16);

	*recorder = AdapterRecorderFindAndUpRecorder(&Status->Recorder,sn);

// 	DBGLOG("%s:%i:ADDDRESS = %p\n",__func__,__LINE__,**recorder);
	
	if(!(*recorder))
	{
		return 0;
	}

	return 1;
}

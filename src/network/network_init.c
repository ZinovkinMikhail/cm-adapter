
#include <dlfcn.h>
#include <string.h>
#include <stdio.h>

// #include <usb.h>
#include <axis_error.h>
#include <axis_dmlibs_resource.h>
#include <axis_modules.h>

#include <dispatcher_lib/dispatcher_lib_resource.h>
#include <dispatcher_lib/settings.h>

#include <dmxxx_lib_net/dmxxx_lib_net_cals.h>

//��������� �������
#include <adapter/network.h>
#include <adapter/cdma.h>
#include <adapter/settings.h>
#include <adapter/power.h>
#include <adapter/usb.h>
#include <adapter/debug.h>
#include <adapter/leds.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif

#define USB_ROSS_CICADA_VENDOR_ID	0x0501

/**
 * @brief �������, ������� ��������� ������� ����������
 *
 * @param control - ��������� �� ��������� ����������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
static int adapter_net_deconfig_lib( libnetcontrol_t *control )
{
	if( !control )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	if( control->net_close )
	{
		control->net_close();
	}

	control->net_init			= NULL;
	control->net_start			= NULL;
	control->net_check_while	= NULL;
	control->net_stop			= NULL;
	control->net_get_status		= NULL;
	control->net_do_command		= NULL;

	dlclose( control->netlib );
	control->netlib = NULL;

	return 0;
}

/**
 * @brief �������, ������� ��������� ���������� � ����������� ��
 *
 * @param control - ��������� �� ��������� �������� ����������
 * @param libtype - ��������� �� ��� ����������
 * @return int - 0 ��� �� ���������� ���������, ����� 0 � ������ ������
 **/
static int adapter_net_config_lib( libnetcontrol_t *control,  const char *libtype )
{
	char path[32];

	if( !control || !libtype || !control )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	snprintf( path, sizeof( path ), LIB_NET_PATH_MASK, libtype );

	control->netlib = dlopen( path, RTLD_NOW );

	if( !control->netlib )
	{
		DBGERR( "%s: Failed to open net lib: %s %s\n", __func__, path, dlerror() );
		return -1;
	}

	control->net_init			= dlsym( control->netlib, DMXXX_NETWORK__NET_LIB_INIT );

	if( !control->net_init )
	{
		DBGLOG( "%s: Not found  '%s' in %s\n", __func__, DMXXX_NETWORK__NET_LIB_INIT, path );
	}

	control->net_start			= dlsym( control->netlib, DMXXX_NETWORK__NET_LIB_START_CONNECTION );

	if( !control->net_start )
	{
		DBGLOG( "%s: Not found  '%s' in %s\n", __func__, DMXXX_NETWORK__NET_LIB_START_CONNECTION, path );
	}

	control->net_check_while	= dlsym( control->netlib, DMXXX_NETWORK__NET_LIB_CHECK_WHILE );

	if( !control->net_check_while )
	{
		DBGLOG( "%s: Not found  '%s' in %s\n", __func__, DMXXX_NETWORK__NET_LIB_CHECK_WHILE, path );
	}

	control->net_stop			= dlsym( control->netlib, DMXXX_NETWORK__NET_LIB_STOP_FUNC );

	if( !control->net_stop )
	{
		DBGLOG( "%s: Not found  '%s' in %s\n", __func__, DMXXX_NETWORK__NET_LIB_STOP_FUNC, path );
	}

	control->net_get_status		= dlsym( control->netlib, DMXXX_NETWORK__NET_LIB_GET_STATUS );

	if( !control->net_get_status )
	{
		DBGLOG( "%s: Not found  '%s' in %s\n", __func__, DMXXX_NETWORK__NET_LIB_GET_STATUS, path );
	}

	control->net_do_command		= dlsym( control->netlib, DMXXX_NETWORK__NET_LIB_DO_COMMAND );

	if( !control->net_do_command )
	{
		DBGLOG( "%s: Not found  '%s' in %s\n", __func__, DMXXX_NETWORK__NET_LIB_DO_COMMAND, path );
	}

	control->net_close			= dlsym( control->netlib, DMXXX_NETWORK__NET_LIB_CLOSE );

	if( !control->net_close )
	{
		DBGLOG( "%s: Not found  '%s' in %s\n", __func__, DMXXX_NETWORK__NET_LIB_CLOSE, path );
	}

	control->net_send_sms		= dlsym( control->netlib, DMXXX_NETWORK__NET_LIB_SEND_SMS );

	if( !control->net_send_sms )
	{
		DBGLOG( "%s: Not found  '%s' in %s\n", __func__, DMXXX_NETWORK__NET_LIB_SEND_SMS, path );
	}

	if( control->net_init )
	{
		int ret = control->net_init();

		if( ret < 0 )
		{
			DBGERR( "%s: Can't call init on library '%s'\n", __func__, path );
			//�� �� ������ ���������������� ���������� - ���� ������� ��� ��� ���
			adapter_net_deconfig_lib( control );
			return ret;
		}

		else
		{
			//��� ����� �������� ������ ���������� - ��� �� ����� �� ��� ���������
			if( control->net_do_command )
			{
				ret = control->net_do_command( DM_NET_COMMAND_GET_VERSION, NULL );

				if( ret < 0 )
				{
					DBGERR( "%s: Can't get '%s' network library version\n", __func__, libtype );
					//��� �� ����������� ������ - ������� ��� ��� ����
				}
				else
				{
					DBGLOG( "%s: For network library '%s' type is '%s'\n", __func__, libtype, ret == DM_NET_COMMAND__VERSION_NO_VPN ? "Separeate VPN library" : "Include VPN functions" );
					control->net_lib_version = ret == DM_NET_COMMAND__VERSION_NO_VPN ? netversionSeparateVPN : netversionold;
				}
			}
			return 0;
		}
	}

	dlclose( control->netlib );
	control->netlib = NULL;

	DBGERR( "%s: No init function\n", __func__ );

	return -AXIS_ERROR_INVALID_VALUE;
}

/**
 * @brief �������, ������� ��������� ��������� ���������� �� VPN PPtP
 *
 * @param IPSettings - ��������� �� ��������� IP ��������
 * @param Net - ��������� �� ��������� ���������� ��� ��������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 */
static int adapter_net_config_pptpt_settings( const SoftAdapterSettingsStruct_t *IPSettings, libnetcontrol_t *Net )
{
	if( !IPSettings || !Net )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	memset( &Net->config, 0, sizeof( ipconfig_t ) );

	//TODO - �� ���� �������������� ���������, ��� PPtP �������� ��������� ��������� ���������
	//Net->config.IPaddress	= ;
	//Net->config.GWAddress	= ;
	//Net->config.NetMask	= ;

	memcpy( Net->config.mac, IPSettings->HWAddress, sizeof( Net->config.mac ) );

	memcpy( Net->config.VPNServer, IPSettings->VPNServer,	sizeof( Net->config.VPNServer ) );
	memcpy( Net->config.VPNPasswd, IPSettings->VPNPasswd,	sizeof( Net->config.VPNPasswd ) );
	memcpy( Net->config.VPNUser, 	IPSettings->VPNUser,	sizeof( Net->config.VPNUser ) );


	if( (IPSettings->Connect & AXIS_SOFTADAPTER_CONNECT__PROTOCOL_TYPE) == AXIS_SOFTADAPTER_CONNECT__PROTOCOL_TYPE_PPTP )
	{
		Net->config.connection_config |= DM_NET_CONFIG_IP_PPTP;
	}
	else
	{
		DBGERR( "%s: Try configure PPtP connection without PPtP enabled\n", __func__ );
		return -AXIS_ERROR_NOT_INIT;
	}

	Net->config.calls.event_callback	= &AdapterNetworkEventPPtP;

	return 0;
}

/**
 * @brief �������, ������� ��������� ��������� ���������� �� PPoE
 *
 * @param IPSettings - ��������� �� ��������� IP ��������
 * @param Net - ��������� �� ��������� ���������� ��� ���������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
static int adapter_net_config_ppoe_settings( const SoftAdapterSettingsStruct_t *IPSettings, libnetcontrol_t *Net )
{
	if( !IPSettings || !Net )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	memset( &Net->config, 0, sizeof( ipconfig_t ) );

	//TODO - �� ���� �������������� ���������, ��� PPtP �������� ��������� ������ �������������
	//Net->config.IPaddress	= ;
	//Net->config.GWAddress	= ;
	//Net->config.NetMask	= ;

	memcpy( Net->config.mac, IPSettings->HWAddress, sizeof( Net->config.mac ) );

	memcpy( Net->config.VPNServer, IPSettings->VPNServer,	sizeof( Net->config.VPNServer ) );
	memcpy( Net->config.VPNPasswd, IPSettings->VPNPasswd,	sizeof( Net->config.VPNPasswd ) );
	memcpy( Net->config.VPNUser, 	IPSettings->VPNUser,	sizeof( Net->config.VPNUser ) );


	if( (IPSettings->Connect & AXIS_SOFTADAPTER_CONNECT__PROTOCOL_TYPE) == AXIS_SOFTADAPTER_CONNECT__PROTOCOL_TYPE_PPTP )
	{
		Net->config.connection_config |= DM_NET_CONFIG_IP_PPTP;
	}

	if( (IPSettings->Connect & AXIS_SOFTADAPTER_CONNECT__PROTOCOL_TYPE) == AXIS_SOFTADAPTER_CONNECT__PROTOCOL_TYPE_PPPOE )
	{
		Net->config.connection_config |= DM_NET_CONFIG_IP_PPOE;
	}
	else
	{
		DBGERR( "%s: Try configure PPoE connection without PPoE enabled\n", __func__ );
		return -AXIS_ERROR_NOT_INIT;
	}

	Net->config.calls.event_callback	= &AdapterNetworkEventPPoE;

	return 0;
}

/**
 * @brief �������, ������� ��������� ��������� ���������� �� Ethernet
 *
 * @param IPSettings - ��������� �� ��������� IP ��������
 * @param Net - ��������� �� ��������� ���������� ��� ���������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
static int adapter_net_config_ethernet_settings( const SoftAdapterSettingsStruct_t *IPSettings, libnetcontrol_t *Net )
{
	if( !IPSettings || !Net )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	memset( &Net->config, 0, sizeof( ipconfig_t ) );

	Net->config.IPaddress	= IPSettings->IPAddress;
	Net->config.GWAddress	= IPSettings->GWAddress;
	Net->config.NetMask		= IPSettings->NetMask;

	memcpy( Net->config.mac, IPSettings->HWAddress, sizeof( Net->config.mac ) );

	memcpy( Net->config.VPNServer, IPSettings->VPNServer,	sizeof( Net->config.VPNServer ) );
	memcpy( Net->config.VPNPasswd, IPSettings->VPNPasswd,	sizeof( Net->config.VPNPasswd ) );
	memcpy( Net->config.VPNUser, 	IPSettings->VPNUser,	sizeof( Net->config.VPNUser ) );


	DBGLOG( "%s: %i Protocol Type = %x %x\n", __func__, __LINE__, IPSettings->Connect,
			IPSettings->Connect & AXIS_SOFTADAPTER_CONNECT__PROTOCOL_TYPE_TCP_IP );

	if( !( IPSettings->Connect & AXIS_SOFTADAPTER_CONNECT__IP_STATIC ) )
	{
		Net->config.connection_config |= DM_NET_CONFIG_IP_DHCP;
	}

	else
	{
		Net->config.connection_config |= DM_NET_CONFIG_IP_STATIC;
	}

	if( (IPSettings->Connect & AXIS_SOFTADAPTER_CONNECT__PROTOCOL_TYPE) == AXIS_SOFTADAPTER_CONNECT__PROTOCOL_TYPE_PPTP )
	{
		Net->config.connection_config |= DM_NET_CONFIG_IP_PPTP;
	}

	if( (IPSettings->Connect & AXIS_SOFTADAPTER_CONNECT__PROTOCOL_TYPE) == AXIS_SOFTADAPTER_CONNECT__PROTOCOL_TYPE_PPPOE )
	{
		Net->config.connection_config |= DM_NET_CONFIG_IP_PPOE;
	}

	Net->config.calls.get_led_num		= &AdapterNetworkGetLedEthernet;
	Net->config.calls.led_blink			= &AdapterNetworkLedBlink;
	Net->config.calls.led_on			= &AdapterNetworkLedOn;
	Net->config.calls.led_off			= &AdapterNetworkLedOff;
	Net->config.calls.led_fastblink		= &AdapterNetworkLedFastBlink;
	Net->config.calls.event_callback	= &AdapterNetworkEventEthernet;

	return 0;
}


/**
 * @brief �������, ������� ����������� ���������� ���������� ��� CDMA
 *
 * @param NetSettings - ��������� �� ��������� ������ ���������� ����
 * @param Net - ��������� �� ��������� ����������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
static int adapter_net_config_cdma_settings( const SoftAdapterExchangeSettingsStruct_t *NetSettings, libnetcontrol_t *Net )
{
	if( !NetSettings || !Net )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	memset( &Net->config, 0, sizeof( ipconfig_t ) );

	//��������� ����������� � ������������ ���� ������� �� ������ �����
	memcpy( Net->config.VPNServer,	NetSettings->Block1.CDMASettings.ModemServer, 	sizeof( Net->config.VPNServer ) );
	memcpy( Net->config.VPNPasswd,	NetSettings->Block1.CDMASettings.ModemPasswd, 	sizeof( Net->config.VPNPasswd ) );
	memcpy( Net->config.VPNUser, 	NetSettings->Block1.CDMASettings.ModemUser, 	sizeof( Net->config.VPNUser   ) );

	Net->config.calls.get_led_num		= &AdapterNetworkGetLedCDMANum;
	Net->config.calls.led_blink			= &AdapterNetworkLedBlink;
	Net->config.calls.led_on			= &AdapterNetworkLedOn;
	Net->config.calls.led_off			= &AdapterNetworkLedOff;
	Net->config.calls.led_fastblink		= &AdapterNetworkLedFastBlink;
	Net->config.calls.event_callback	= &AdapterNetworkEventCDMA;

	//�� ������ ����� ������ PPtP ������ PPP - ������ ����� ������� ��� � ����������
	if( (NetSettings->Block0.AdapterSettings.Connect & AXIS_SOFTADAPTER_CONNECT__PROTOCOL_TYPE) == AXIS_SOFTADAPTER_CONNECT__PROTOCOL_TYPE_PPTP )
	{
		Net->config.connection_config |= DM_NET_CONFIG_IP_PPTP;
	}


	return 0;
}


/**
 * @brief �������, ������� ����������� ���������� ���������� ��� LTE
 *
 * @param NetSettings - ��������� �� ��������� � ������� ����������� ����
 * @param Net - ��������� �� �� ����������� ��������� ����������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
static int adapter_net_config_lte_settings( const SoftAdapterExchangeSettingsStruct_t *NetSettings,libnetcontrol_t *Net )
{
	if( !NetSettings || !Net )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	// ����� ���� ��1, � � ��� lte ������ �����������
	// no change
	libnetcontrol_t *tmpNetStruct = Net;
	uint32_t wait_command = Net->config.connection_config & DM_NET_CONFIG_WAIT_COMMAND; //��� ����� ������� ���������� ����� - � �� ��� ������ ���������

	memset( &tmpNetStruct->config, 0,sizeof( ipconfig_t ) );

	tmpNetStruct->config.IPaddress	=  NetSettings->Block0.AdapterSettings.IPAddress;
	tmpNetStruct->config.GWAddress	=  NetSettings->Block0.AdapterSettings.GWAddress;
	tmpNetStruct->config.NetMask	=  NetSettings->Block0.AdapterSettings.NetMask;

	memcpy( tmpNetStruct->config.mac, NetSettings->Block0.AdapterSettings.HWAddress, sizeof( tmpNetStruct->config.mac ) );

	//����� ����������� �� ����
	tmpNetStruct->config.ModemBand	=  NetSettings->Block0.AdapterSettings.ModemBand;

	//���, ������ � APN ��� ����������� � ����
	memcpy( Net->config.VPNServer,	NetSettings->Block1.CDMASettings.ModemServer, 	sizeof( Net->config.VPNServer ) );
	memcpy( Net->config.VPNPasswd,	NetSettings->Block1.CDMASettings.ModemPasswd, 	sizeof( Net->config.VPNPasswd ) );
	memcpy( Net->config.VPNUser, 	NetSettings->Block1.CDMASettings.ModemUser, 	sizeof( Net->config.VPNUser   ) );

	tmpNetStruct->config.connection_config |= DM_NET_CONFIG_IP_DHCP | wait_command;

	tmpNetStruct->config.calls.get_led_num		= &AdapterNetworkGetLedEthernet;
	tmpNetStruct->config.calls.led_blink		= &AdapterNetworkLedBlink;
	tmpNetStruct->config.calls.led_on			= &AdapterNetworkLedOn;
	tmpNetStruct->config.calls.led_off			= &AdapterNetworkLedOff;
	tmpNetStruct->config.calls.led_fastblink	= &AdapterNetworkLedFastBlink;
	tmpNetStruct->config.calls.event_callback	= &AdapterNetworkEventLTE;

	//�� ������ ����� ������ PPtP ������ PPP - ������ ����� ������� ��� � ����������
	if( (NetSettings->Block0.AdapterSettings.Connect & AXIS_SOFTADAPTER_CONNECT__PROTOCOL_TYPE) == AXIS_SOFTADAPTER_CONNECT__PROTOCOL_TYPE_PPTP )
	{
		Net->config.connection_config |= DM_NET_CONFIG_IP_PPTP;
	}

	return 0;
}

#define CERTPATH "/etc/ssl/cacert.pem"

/**
 * @brief �������, ������� ����������� ���������� ���������� ��� WiFi
 *
 * @param WiFiSettings - ��������� �� ��������� ���������� WiFi
 * @param IPSettings - ��������� �� ��������� IP
 * @param Net - ��������� �� ������� ���������, ������� ���������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
static int adapter_net_config_wifi_settings( const SoftAdapterWiFiSettingsStruct_t *WiFiSettings,
											 const SoftAdapterSettingsStruct_t *IPSettings, libnetcontrol_t *Net )
{
	if( !WiFiSettings || !IPSettings || !Net )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	memset( &Net->config, 0, sizeof( ipconfig_t ) );

	strncpy( Net->config.ssid, WiFiSettings->SSID, sizeof( Net->config.ssid ) );

	Net->config.ssid[ sizeof( Net->config.ssid ) - 1 ] = 0x00;

	if( WiFiSettings->EncryptType == AXIS_SOFTADAPTER_WIFI_ENCRYPTIONS__NONE )
	{
		//������ ���� "NONE" �� �� ��������� ���������� ��� �� ��������� � ������� "None"
		Net->config.encription_type = ENC_TYPE_NONE;
		strncpy( Net->config.encription_code, WiFiSettings->Key, sizeof( Net->config.encription_code ) );
		Net->config.encription_code[ sizeof( Net->config.encription_code ) - 1 ] = 0x00;
	}

	else if( WiFiSettings->EncryptType == AXIS_SOFTADAPTER_WIFI_ENCRYPTIONS__WEP64_HEX )
	{
		//������������� ��� ���������� WEP �� ���������� ��������� WEP
		Net->config.encription_type = ENC_TYPE_WEP;
		strncpy( Net->config.encription_code, WiFiSettings->Key, sizeof( Net->config.encription_code ) );
		Net->config.encription_code[ sizeof( Net->config.encription_code ) - 1 ] = 0x00;
	}

	else if( WiFiSettings->EncryptType == AXIS_SOFTADAPTER_WIFI_ENCRYPTIONS__WEP64 )
	{
		Net->config.encription_type = ENC_TYPE_WEP;

		if( WiFiSettings->WepType == AXIS_SOFTADAPTER_WEP_PASSPHRASE )
		{
			Net->config.weptype = DM_WEP_TYPE_PASSPHRASE;
		}

		else if( WiFiSettings->WepType == AXIS_SOFTADAPTER_WEP_ASCII )
		{
			Net->config.weptype = DM_WEP_TYPE_ASCII;
		}

		else
		{
			DBGERR( "%s: Bad wep type for key\n", __func__ );
			return -1;
		}

		strncpy( Net->config.encription_code, WiFiSettings->Key, sizeof( Net->config.encription_code ) );
		Net->config.encription_code[ sizeof( Net->config.encription_code ) - 1 ] = 0x00;
	}

	else if( WiFiSettings->EncryptType == AXIS_SOFTADAPTER_WIFI_ENCRYPTIONS__WEP128_HEX )
	{
		Net->config.encription_type = ENC_TYPE_WEP;
		strncpy( Net->config.encription_code, WiFiSettings->Key, sizeof( Net->config.encription_code ) );
		Net->config.encription_code[ sizeof( Net->config.encription_code ) - 1 ] = 0x00;
	}

	else if( WiFiSettings->EncryptType == AXIS_SOFTADAPTER_WIFI_ENCRYPTIONS__WEP128 )
	{
		Net->config.encription_type = ENC_TYPE_WEP;

		if( WiFiSettings->WepType == AXIS_SOFTADAPTER_WEP_PASSPHRASE )
		{
			Net->config.weptype = DM_WEP_TYPE_PASSPHRASE128;
		}

		else if( WiFiSettings->WepType == AXIS_SOFTADAPTER_WEP_ASCII )
		{
			Net->config.weptype = DM_WEP_TYPE_ASCII128;
		}

		else
		{
			DBGERR( "%s: Bad wep type for key\n", __func__ );
			return -1;
		}

		strncpy( Net->config.encription_code, WiFiSettings->Key, sizeof( Net->config.encription_code ) );
		Net->config.encription_code[ sizeof( Net->config.encription_code ) - 1 ] = 0x00;
	}

	else if( WiFiSettings->EncryptType == AXIS_SOFTADAPTER_WIFI_ENCRYPTIONS__WPA )
	{
		snprintf( Net->config.encription_code, sizeof( Net->config.encription_code ), "\"%s\"", WiFiSettings->Key );
		Net->config.encription_type = DISPATCHER_LIB_INTERFACE_INI_VALUE_WPA;
	}

	else if( WiFiSettings->EncryptType == AXIS_SOFTADAPTER_WIFI_ENCRYPTIONS__WPA2 )
	{
		snprintf( Net->config.encription_code, sizeof( Net->config.encription_code ), "\"%s\"", WiFiSettings->Key );
		Net->config.encription_type = DISPATCHER_LIB_INTERFACE_INI_VALUE_WPA2;
	}

	else if( WiFiSettings->EncryptType == AXIS_SOFTADAPTER_WIFI_ENCRYPTIONS__WPA_PEAP )
	{
		snprintf( Net->config.password, sizeof( Net->config.password ), "\"%s\"", WiFiSettings->Password );
		snprintf( Net->config.login, sizeof( Net->config.login ), "\"%s\"", WiFiSettings->Login );
#warning ���������� ����� ����� ����� �� ������ ���������������� � ������ ������������ ��� �� ����� ������
		snprintf( Net->config.cerpath, sizeof( Net->config.cerpath ), "\"%s\"", CERTPATH );
	}

	//�� ���������� ���
	else
	{
		DBGERR( "%s: Invalid encription type\n", __func__ );
		return -AXIS_ERROR_INVALID_VALUE;
	}

	Net->config.IPaddress	= IPSettings->IPAddress;
	Net->config.GWAddress	= IPSettings->GWAddress;
	Net->config.NetMask		= IPSettings->NetMask;

	memcpy( Net->config.mac, IPSettings->HWAddress, sizeof( Net->config.mac ) );

	memcpy( Net->config.VPNServer, IPSettings->VPNServer,	sizeof( Net->config.VPNServer ) );
	memcpy( Net->config.VPNPasswd, IPSettings->VPNPasswd,	sizeof( Net->config.VPNPasswd ) );
	memcpy( Net->config.VPNUser, 	IPSettings->VPNUser,	sizeof( Net->config.VPNUser ) );

	if( !( IPSettings->Connect & AXIS_SOFTADAPTER_CONNECT__IP_STATIC ) )
	{
		Net->config.connection_config |= DM_NET_CONFIG_IP_DHCP;
	}

	else
	{
		Net->config.connection_config |= DM_NET_CONFIG_IP_STATIC;
	}

	if( (IPSettings->Connect & AXIS_SOFTADAPTER_CONNECT__PROTOCOL_TYPE) == AXIS_SOFTADAPTER_CONNECT__PROTOCOL_TYPE_PPTP )
	{
		Net->config.connection_config |= DM_NET_CONFIG_IP_PPTP;
	}

	//NOTE - �� ����� �� �������, �� �� ������������ PPoE ������ WiFi

	Net->config.calls.get_led_num		= &AdapterNetworkGetLedWiFiNum;
	Net->config.calls.led_blink			= &AdapterNetworkLedBlink;
	Net->config.calls.led_on			= &AdapterNetworkLedOn;
	Net->config.calls.led_off			= &AdapterNetworkLedOff;
	Net->config.calls.led_fastblink 	= &AdapterNetworkLedFastBlink;
	Net->config.calls.event_callback 	= &AdapterNetworkEventWiFi;

	return 0;
}

/**
 * @brief �������, ������� ��������� ����� ���������� ����� ������� ����������
 *
 * @param NetSettings - ��������� �� ��������� ������� ����������
 * @param Net - ��������� �� ��������� ����������� ����������
 * @param Type - ��������� �� ��� ����������, ������ ������� ����������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 */
static int adapter_net_config_start_any_net( const SoftAdapterExchangeSettingsStruct_t *NetSettings, libnetcontrol_t *Net, libnettype_t Type )
{
	int ret;
	const char *libnamestr;
	const char *libname;

	if( !NetSettings || !Net || Type <= nettype_closed || Type >= nettype_max )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}
	
#if 1 //==== set only tcpip
    Type = nettype_eth;
#endif

	Net->net_lib_type	= Type;
	libnamestr			= AdapterNetworkGetConnectionTypeName( Type, 0 );
	libname				= AdapterNetworkGetConnectionTypeName( Type, 1 );

	//�������������� ������ ����������

	//���������� � ����� LTE � Modem - ����� ����� ����������� ��� � ��������� ������
	if( Type == nettype_lte ||
		Type == nettype_modem )
	{
		if( Net->config.private_data )
		{
			libname =  (const char * )Net->config.private_data;
			DBGLOG( "%s:%i Network %s library have a individual library name %s \n", __func__, __LINE__, libnamestr, libname );
		}
	}

	ret = adapter_net_config_lib( Net, libname ); //�� �������� ��� ����������, � �� ������ ��� ������

	if( ret < 0 )
	{
		DBGERR( "%s: It wasn't possible to init '%s' lib\n", __func__, libnamestr );
	}

	if( !ret )
	{
		//������������� ��������� ��� ������ ����������
		switch( Type )
		{
			case nettype_eth:
			{
				ret = adapter_net_config_ethernet_settings( &NetSettings->Block0.AdapterSettings , Net );
				break;
			}
			case nettype_wifista:
			{
				ret = adapter_net_config_wifi_settings( &NetSettings->Block1.WiFiSettings, &NetSettings->Block0.AdapterSettings, Net );
				break;
			}
			case nettype_lte:
			{
				ret = adapter_net_config_lte_settings( NetSettings, Net ); //��� ��� ����� ������ ��������� � ��� ���������� - �������� ������ ������������
				break;
			}
			case nettype_modem:
			{
				ret = adapter_net_config_cdma_settings( NetSettings, Net ); //��� ��� ����� ������ ��������� � ��� ���������� - �������� ������ ������������
				break;
			}
			case nettype_pptp:
			{
				ret = adapter_net_config_pptpt_settings( &NetSettings->Block0.AdapterSettings, Net );
				break;
			}
			case nettype_ppoe:
			{
				ret = adapter_net_config_ppoe_settings( &NetSettings->Block0.AdapterSettings, Net );
				break;
			}
			default:
			{
				DBGERR( "%s: Network library '%s' not supported\n", __func__, libnamestr );
				ret = -AXIS_ERROR_NOT_INIT;
			}
		}

		if( ret < 0 )
		{
			DBGERR( "%s: It wassn't possible to configure '%s' lib\n", __func__, libnamestr );
		}
	}

	if( !ret )
	{
		//��������� ����������� ��� ����������
		if( Net->net_start )
		{
			if( Net->net_start( &Net->config ) < 0 )
			{
				DBGERR( "%s: It wasn't possible to start '%s' lib\n", __func__, libnamestr );
			}
		}
		else
		{
			DBGERR( "%s: Network '%s' libry not support start function\n", __func__, libnamestr );
		}
	}

	if( ret )
	{
		INFO_LOG( 1, "Start %s connection", libnamestr );
	}
	else
	{
		INFO_LOG( 0, "%s connection started", libnamestr );
	}

	return ret;

}

/**
 * @brief �������, ������� ��������� ���������� ����� Ethernet
 *
 * @param NetSettings - ��������� �� ��������� ���������� ����
 * @param Net - ��������� �� ��������� ����������
 * @return int
 **/
static inline int adapter_net_config_start_ethernet( const SoftAdapterExchangeSettingsStruct_t *NetSettings, libnetcontrol_t *Net )
{
	return adapter_net_config_start_any_net( NetSettings, Net, nettype_eth );
}

/**
 * @brief ������� ������� ��������� ���������� �� WiF
 *
 * @param NetSettings - ��������� �� �������� ������� ��������
 * @param Net - ��������� �� ��������� ��������� ����
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
static inline int adapter_net_config_start_wifi( const SoftAdapterExchangeSettingsStruct_t *NetSettings, libnetcontrol_t *Net )
{
	return adapter_net_config_start_any_net( NetSettings, Net, nettype_wifista );
}

/**
 * @brief �������, ������� ��������� ���������� �� LTE
 *
 * @param NetSettings - ��������� �� ��������� ����
 * @param Net - ��������� �� ��������� ��������� ����
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
static inline int adapter_net_config_start_lte( const SoftAdapterExchangeSettingsStruct_t *NetSettings, libnetcontrol_t *Net )
{
	return adapter_net_config_start_any_net( NetSettings, Net, nettype_lte );
}

/**
 * @brief �������, ������� ��������� ���������� �� CDMA
 *
 * @param NetSettings - ��������� �� ��������� �������� ����
 * @param Net - ��������� �� �������� ����������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
static inline int adapter_net_config_start_cdma( const SoftAdapterExchangeSettingsStruct_t *NetSettings, libnetcontrol_t *Net )
{
	return adapter_net_config_start_any_net( NetSettings, Net, nettype_modem );
}

/**
 * @brief �������, ������� ��������� ���� � ���������� USB
 *
 * @param Status - ��������� �� ��������� ����������� �������
 * @param Net - ��������� �� �������� �����������
 * @return int
 **/
int AdapterNetworkUsbOut( const AdapterGlobalStatusStruct_t *Status, libnetcontrol_t *Net )
{
	if( !Status ||  !Net )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n",__func__,__LINE__ );
		return -1;
	}

	if( Status->GlobalDevice_t.dmdev_eth.dmctrl_feature & DM_NET_ETH_HW &&
			Status->GlobalDevice_t.dmdev_eth.dmctrl_ipresent )
	{
		//��������� ������ ���� �� ���������� ����������� Ethernet
        //�� ���� ��� ������, ����� ��������� ����� � ��� ��� ���� ETH
// 		if( adapter_net_config_start_ethernet( &Status->AdapterSettings, Net ) < 0 )
// 		{
// 			DBGERR( "%s: It wasn't possible to start internal ethernet\n", __func__ );
// 			return -1;
// 		}
	}

	return 0;
}

/**
 * @brief �������, ������� ��������� ������������ ��������� USB ��������� �������� � ���������� ����������
 * @brief � ���������� ������ �� �������� ���� � �������
 *
 * @param dev - ��������� �� ��������� ����������
 * @param devices - ��������� �� ��������� ��������� �������
 * @param count - ���������� ��������� USB ���������
 * @param num - ����� ��� ������� ������
 * @param modnum - ����� ��� ������� ������
 * @return int - 0 ������� �� �������, 1 ����� ������, ����� 0 � ������ ������
 **/
int AdapterNetworkDevicesCheckForInModem( const dmdev_ctrl_hw_t *dev, const AxisUsbDesc_t *devices,
		int count, int *num,int *modnum )
{
	if( !devices || count <= 0 || !dev || !num )
	{
		DBGERR( "%s:%i: Invalid Argumentn %i %p %p %p %p\n", __func__, __LINE__,count,dev,devices,num,modnum );
		return -1;
	}

	int i = 0;
	int j = 0;

	DBGLOG( "%s: Try check device for '%s'\n", __func__, dev->dmdev_device_name );
	for( i = 0; i < count; i++ )
	{
		for( j = 0; j < dev->dmctrl_imodulescount; j++ )
		{
			DBGLOG( "%s:%i: Try find: 0x%04x 0x%04x in: 0x%04x 0x%04x \n",__func__,__LINE__, devices[i].ProdId, devices[i].DevId,dev->dmctrl_modules[j].idProduct,dev->dmctrl_modules[j].idDevice );

			if( devices[i].ProdId == dev->dmctrl_modules[j].idProduct && devices[i].DevId == dev->dmctrl_modules[j].idDevice )
			{
				*num = i;
				*modnum = j;
				DBGLOG( "%s: Found...\n", __func__ );
				return 1;
			}
		}
	}

	DBGLOG( "%s: Not found...\n", __func__ );
	return 0;
}


/**
 * @brief �������, ������� ��������� ���� �� ���������� ������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @param CurrentNetworkSettings - ��������� �� ��������� ������� ������� ��������
 * @param dev - ��������� �� ��������� ������� ���������� ������������
 * @param Type - ��� ����������� ����
 * @param Num - ����� USB ���������� � ������� ���������
 * @param modnum - ����� ������ � ������� ���������� ������������
 * @param devices - ������ USB ���������, ��������� � �������� ��������
 * @return int - 0 - ����� �� ������, 1 ����� ������ � �������, ����� 0 � ������ ������
 */
static int adapter_net_start_checkd_net( AdapterGlobalStatusStruct_t *Status,
										 SoftAdapterExchangeSettingsStruct_t *CurrentNetworkSettings,
										 dmdev_ctrl_hw_t *dev, libnettype_t Type, int Num, int modnum, AxisUsbDesc_t *devices )
{
	libnetcontrol_t *Net;
	int modem_found = 0;

	if( !Status || !dev || Type <= nettype_closed || Type >= nettype_max )
	{
		DBGERR( "%s: Invalid argument\n", __func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	DBGLOG( "%s:%i %s In %i \n", __func__, __LINE__, AdapterNetworkGetConnectionTypeName( Type, 0 ), Num );

	//�������, ����� ����� ���� ��� ����
	Net = AdapterNetworkGetLibControlByConnectionType( Status, Type );

	if( !Net )
	{
		//��������� ����� ����
		DBGLOG( "%s: %s net not found. Try start new\n", __func__, AdapterNetworkGetConnectionTypeName( Type, 0 ) );

		Net = AdapterNetworkGetFreeLibControlStruct( Status );

		if( !Net )
		{
			DBGERR( "%s: No free network struct for new network\n", __func__ );
			return -AXIS_ERROR_INVALID_VALUE;
		}

		Net->EnablePollingModem  = 1; //����� ��� ���� ���� ������ ���������� �� ���������� �� USB

		memcpy( &Net->PollingModem, &devices[ Num ], sizeof( AxisUsbDesc_t ) );

		Net->PollingModem.connecttype = AdapterNetworkNetLibTypeToSoftAdapterType( Type );

		if( AdapterUsbInitPolling( &Net->PollingModem ) < 0 )
		{
			DBGERR( "%s: It wasn't possible to init polling\n", __func__ );
		}

		if( Type == nettype_modem )
		{
			static int reset_done = 0; //ATTENTION - ��� ���������� ���������� � �� ��� ���� ���������!!!

			if( !reset_done )
			{
				AdapterPowerUSB( &Status->GlobalDevice_t, Net->PollingModem.port + 1, 0 );
				sleep( 2 );
				AdapterPowerUSB( &Status->GlobalDevice_t, Net->PollingModem.port + 1, 1 );
				sleep( 1 );
				reset_done = 1;
				return 0;
			}
			else
			{
				reset_done = 0;
			}

		}

		//��� ����� ��������� ������, ��� ����� ���� ��� ����������
		Net->config.private_data = dev->dmctrl_modules[ modnum ].privatedatamodule;

		int ret = adapter_net_config_start_any_net( CurrentNetworkSettings, Net, Type );

		if( ret < 0 )
		{
			DBGERR( "%s: It wasn't possible to start %s\n", __func__, AdapterNetworkGetConnectionTypeName( Type, 0 ) );

			AdapterUsbDeinitPolling( &Net->PollingModem );

			AdapterNetworkFreeLibControlStruct( Status, Net );

			return ret;
		}

		DBGLOG( "%s: %s modem on port %d\n", __func__, AdapterNetworkGetConnectionTypeName( Type, 0 ), Net->PollingModem.port + 1 );

		modem_found = 1;
	}
	else
	{
		//����� ���� ��� ���� ���������� �� ��������� �������
		if( dev->dmctrl_feature & DM_NET_ONE_LIB_FOR_MORE_PID )
		{
			modem_found = 1;

			if( Type == nettype_modem || Type == nettype_lte )
			{
				//�� ���� ������ ������, ��� ��� ������ ����� CDMA (� ����� LTE) �����
				//�� ��� ���� ���������� CDMA ���� ��� ���������, ��������,
				//������� �� ����� ����� ��� CD-ROM, ��������� ����������, � ����� ����� ��� �� �����,
				//��� ��� �����, �� ��� ���� ���������� ������ �������� ��������
				//(� ��� ��� �������� ���� ��� ���������� �� ��������� PID)
				//�� ���� ����� ���������� �� USB ������ � �� ��� ��������� ������ �� ��� �� ������
				//������ ��� ����� �� ����� ��������� �������� �� �������
				//��� �� � ���������� �������, ���� ������� ��������
				if( !Net->PollingModem.connecttype ) //���� ��� �� ����������� ������, ��� ����� �����!
				{

					//��� �������� �� ����� - ��� �� ������ ������!!!
					memcpy( &Net->PollingModem, &devices[ Num ], sizeof( AxisUsbDesc_t ) );

					Net->PollingModem.connecttype = AdapterNetworkNetLibTypeToSoftAdapterType( Type );

					DBGLOG( "%s: %i PORT = %i\n",__func__,__LINE__,Net->PollingModem.port );

					DBGLOG( "%s: Try Start polling new descriptor\n", __func__ );

					if( AdapterUsbInitPolling( &Net->PollingModem ) < 0 )
					{
						DBGERR( "%s: It wasn't possible to init new polling\n", __func__ );
					}
				}
			}

		}
		else
		{
			DBGLOG( "%s: %s net found at %p - no more\n", __func__, AdapterNetworkGetConnectionTypeName( Type, 0 ), Net );
		}
	}

	return modem_found;
}


/**
 * @brief �������, ������� ��������� � �� �������� �� ��������� USB ���������� ��� ������� � ������� ��� ���� ��������
 * @brief � ���� ��� ���, �� ����������� ��������������� ����������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @param devices ...
 * @param count ...
 * @return int
 **/
static int AdapterNetworkCheckInDevices( AdapterGlobalStatusStruct_t *Status, AxisUsbDesc_t *devices, int count )
{
	int num = 0;
	int modnum = 0;
	int modem_found = 0;
	SoftAdapterExchangeSettingsStruct_t DefaultNetworkSettings;
	SoftAdapterExchangeSettingsStruct_t *CurrentNetworkSettings = NULL;

	DBGLOG( "%s: Try check in device. Count=%d\n", __func__, count );

	if( !Status || !devices || count <= 0 )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	//���� �� ��� ������� ����� ������������ �����, �� � ������ �������������� ��� ����� ���������
	//���������, ������� �� ���������, � ��������� ������ �������� ���������
	if( Status->RescueMode )
	{
		if( AdapterSettingsLoadDefault( &Status->GlobalDevice_t, &DefaultNetworkSettings ) < 0 )
		{
			DBGERR( "%s: It wasn't possible to load default settings\n", __func__ );
			return -1;
		}

		CurrentNetworkSettings = &DefaultNetworkSettings;
	}

	else
	{
		CurrentNetworkSettings = &Status->AdapterSettings;
	}

	//�������� ��������� ���������� � ��������� WiFi
	if( AdapterNetworkDevicesCheckForInModem( &Status->GlobalDevice_t.dmdev_wifi,
													devices, count, &num, &modnum ) > 0 )
	{
		int ret = adapter_net_start_checkd_net( Status, CurrentNetworkSettings, &Status->GlobalDevice_t.dmdev_wifi, nettype_wifista, num, modnum, devices );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't check and start WiFi modems\n", __func__ );
			return ret;
		}

		modem_found = ret;
	}

	//�������� ��������� ���������� � ��������� LTE
	if( AdapterNetworkDevicesCheckForInModem( &Status->GlobalDevice_t.dmdev_lte, devices, count, &num, &modnum ) > 0 )
	{
		int ret = adapter_net_start_checkd_net( Status, CurrentNetworkSettings, &Status->GlobalDevice_t.dmdev_lte, nettype_lte, num, modnum, devices );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't check and start LTE modems\n", __func__ );
			return ret;
		}

		modem_found = ret;
	}

	//�������� ��������� ���������� � ��������� CDMA
	if( AdapterNetworkDevicesCheckForInModem( &Status->GlobalDevice_t.dmdev_cdma, devices, count, &num, &modnum ) > 0 )
	{
		int ret = adapter_net_start_checkd_net( Status, CurrentNetworkSettings, &Status->GlobalDevice_t.dmdev_cdma, nettype_modem, num, modnum, devices );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't check and start CDMA modems\n", __func__ );
			return ret;
		}

		modem_found = ret;
	}

	//��������� ��������� ������ � ��������� Eth
	if( AdapterNetworkDevicesCheckForInModem( &Status->GlobalDevice_t.dmdev_eth, devices, count, &num, &modnum ) > 0 )
	{
		int ret = adapter_net_start_checkd_net( Status, CurrentNetworkSettings, &Status->GlobalDevice_t.dmdev_eth, nettype_eth, num, modnum, devices );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't check and start Eth modems\n", __func__ );
			return ret;
		}

		modem_found = ret;
	}

	DBGLOG( "%s: device count = %d, modem found = %d, NoModemFoundCount = %u(%u), NoModemFoundCountFatal = %u(%u)\n", __func__,
			count, modem_found,
			Status->NoModemFoundCount, Status->NoModemFoundCountCheck,
			Status->NoModemFoundCountFatal, 32 ); //TODO - ���������

	if( modem_found )
	{
		//�� ��������� ����� ����������� - ������� ������� SMS
		Status->NoIncomingSMSCounter = 0x00;
	}

	if( modem_found && Status->NoModemFoundCount )
	{
		//����� ������, � � ���������� ������ ��� �� ���� - ����� ���������
		AdapterLedUSBNoModemsOff( Status );
		Status->NoModemFoundCount = 0; //���������� ������� ������ ������
		Status->NoModemFoundCountFatal = 0;
	}

	else if( !modem_found && Status->NoModemFoundCount )
	{
		int q;
		DBGERR( "%s: No driver found for devece (NoModemFoundCount=%u):\n", __func__, Status->NoModemFoundCount );
		for( q = 0; q < count; q++ )
		{
			DBGERR( "%s:\tDevice %d PID=%04x VID=%04x\n", __func__, q, devices->DevId, devices->ProdId );
		}
		//����� �� ������, ��������, �������� �� �������������� �����
		Status->NoModemFoundCount++;
	}

	DBGLOG("%s:%i: Exit \n",__func__,__LINE__);
	
	return 0;
}

int AdapterNetworkCheckNetDevicesIn( AdapterGlobalStatusStruct_t *Status )
{
	AxisUsbDesc_t devices[ DEVICE_MAX_NUM ];
	int devcount;
	int real_dev_count;
	int i = 0;

	if( !Status )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	memset( devices, 0, sizeof( devices ) );

	devcount = axis_usb_enumerate_all_bus( devices, DEVICE_MAX_NUM );

	if( devcount < 0 )
	{
		DBGERR( "%s: It wasn't possible to check devices\n", __func__ );
		//������ ������ ��������� == ���������� ���������
		devcount = 0;
	}

	real_dev_count = devcount;

	for( i = 0; i < devcount; i++ )
	{
		DBGLOG( "%s: %i DEVICE = %x  %x \n", __func__, __LINE__, devices[i].DevId, devices[i].ProdId );

		if( devices[i].ProdId == USB_ROSS_CICADA_VENDOR_ID )
		{
			//��� ������ USB ���������� - ��� ���� ��������� �� ������������
			real_dev_count--;
		}
	}

	if( !real_dev_count )
	{
		DBGERR( "%s: No USB device found on board (Count=%u/%u TCount=%u/%u) ACC Modem %s\n", __func__,
										Status->NoModemFoundCount, Status->NoModemFoundCountCheck,
										Status->NoModemFoundCountFatal, 32, //TODO - ���������
										Status->modemsInSleep ? "Sleep" : "Work" );

		//��� ��������� �� ���� USB ��� ���� ��� ���???
		//��� ����� ��������� - ����, ��� ������ ���
		//��� ������ ���� ���������� ��� ���� ��� ������������
		if( !Status->NoModemFoundCount )
		{
			int ret = AdapterLedUSBNoModemsOn( Status );

			if( ret < 0 )
			{
				DBGERR( "%s: Cannot on error led\n", __func__ );
			}
		}

		Status->NoModemFoundCount++;
		//��� ������ - ������� � ������� �������� ������ SMS �� � ��� ����������
		Status->NoIncomingSMSCounter = 0; //� ����� ������ ������ ����� ������

	}

	else
	{
		if( AdapterNetworkCheckInDevices( Status, devices, devcount ) < 0 )
		{
			DBGERR( "%s: Can't check In Device\n", __func__ );
		}
	}
	
	DBGLOG("%s:%i: Exit \n",__func__,__LINE__);

	return 0;
}
int AdapterAllNetworksStop( AdapterGlobalStatusStruct_t *Status)
{
	if(!Status)
	{
		DBGERR("%s:%i: Invlid argument \n",__func__,__LINE__);
		return -1;
	}	

	int i = 0;

	for( i = 0; i < NET_NETWORK_INTERFACES_COUNT; i ++ )
	{
		if( !Status->Net[i].netlib )
		{
			continue;
		}

		//��� ���� �������� �� ���� - ��� �� �� ������������ ����� ���
		if( Status->Net[i].net_lib_type <= nettype_closed )
		{
			continue;
		}

		if( AdapterNetworkStop( Status,&Status->Net[i] ) < 0 )
		{
			DBGERR( "%s: Can't stop network at index %d\n", __func__, i );
		}
	}

	return 0; 	

}

/**
 * @brief �������, ������� ������������� ������� �����������
 *
 * @param Status - ��������� �� ���������� ������ ��������
 * @param Net - ��������� �� ����, ������� ������ ���� �����������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 */
int AdapterNetworkStop( AdapterGlobalStatusStruct_t *Status, libnetcontrol_t* Net )
{
	if( !Status || !Net)
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	//���� ���� ����������� - ������ � �� �� ���� �������������
	if( Net->net_lib_type <= nettype_closed )
	{
		DBGERR( "%s: Try stop not initing network\n", __func__ );
		return -1;
	}
	
	//���� ������������ ����� ���� � ��� ���� ���������
	
	INFO_LOG( 0, "%s %sdisconnected", AdapterNetworkGetConnectionTypeName( Net->net_lib_type, 0 ), Net->EnablePollingModem ? "modem " : "" );
	
	if( Net->net_stop )
	{
		Net->net_stop( );
		adapter_net_deconfig_lib( Net );

	}

	if( Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature & DM_DM_ENABLE_DYNAMIC_NET &&
		Net->EnablePollingModem )
	{
		AdapterUsbDeinitPolling( &Net->PollingModem );
		//return 0;
	}

	//������������� �������� ����, ���� ����
	int ret = AdapterNetworkStopChildNetwork( Status, Net );

	if( ret < 0 )
	{
		DBGERR( "%s: Can't stop child netowrk\n", __func__ );
	}


	AdapterNetworkFreeLibControlStruct(Status,Net);

	return 0;

}
int AdapterNetworkCheckWhile( AdapterGlobalStatusStruct_t *Status )
{
	if( !Status )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	int ret;

	int i = 0;

	for(i = 0; i < NET_NETWORK_INTERFACES_COUNT; i++)
	{
		if( !Status->Net[i].netlib )
			continue;

		if(Status->Net[i].net_check_while)
		{
			ret = Status->Net[i].net_check_while( );

			if( ret == 1 )
			{
				DBGERR( "%s: Net Tread Stopped - Try restart it\n", __func__ );

				//����� ���������� - �� ���� ��� ����������� ����, �� ��� �� ���� ���������??
				//�� ������ ��???
				ret = Status->Net[i].net_start( &Status->Net[i].config );

				if( ret < 0 )
				{
					DBGERR( "%s: Can't restart static network\n", __func__ );
				}

			}

			else if( ret < 0 )
			{
				DBGERR( "%s: Error checking net\n", __func__ );
			}

		}

	}

	return 0;
}
int AdapterNetworkGetConnectionStatuses( AdapterGlobalStatusStruct_t *Status )
{
	if( !Status )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	int ret;

	int i = 0;

	for(i = 0; i < NET_NETWORK_INTERFACES_COUNT; i ++)
	{
		libnetcontrol_t* Net = &Status->Net[i];
		
		if( !Net )
		{
			DBGLOG( "%s: Network subsystem %i is empty\n", __func__, i );
			continue;
		}

		if( Net->net_lib_type <= nettype_closed ||
			Net->net_lib_type >= nettype_max )
		{
			DBGLOG( "%s: Network subsystem %i have a invalid type %d\n", __func__, i, Net->net_lib_type );
			continue;
		}

		ret = AdapterNetworkGetConnectionStatus(Net);	
		if( ret < 0 )
			{
				DBGERR( "%s:Error geting status\n", __func__ );
				// Ok try next device 
				continue;
			}
	}
	

	return 0;
}
int AdapterNetworkGetConnectionStatus( libnetcontrol_t* Net )
{
	if( !Net )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	int ret;

	if( Net->net_get_status )
	{
		ret = Net->net_get_status( &Net->status );

		if( ret < 0 )
		{
			DBGERR( "%s:Error geting status\n", __func__ );

			return ret;
		}
	}

	return 0;
}


static int initNetworkLibStruct( libnetcontrol_t *Net, int  count )
{
	int i = 0;

	if( !Net || count <= 0 )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	for( ; i < count; i++ )
	{
		Net[i].net_lib_type = nettype_closed;
	}

	return 0;
}

//static 	SoftAdapterExchangeSettingsStruct_t Sets;

//���������� ������� ������������� ����
//����:
// Status - ����������� ���������� ��������� �������
//irescue  - ���� ����������� ������
//�������:1
// 0 - ��� ������� ����������
// ����� 0 � ������ ������
int AdapterNetworkInit( AdapterGlobalStatusStruct_t *Status, int irescue )
{

	int ret = AXIS_NO_ERROR;
	SoftAdapterExchangeSettingsStruct_t DefaultAdapterSettings;
	SoftAdapterExchangeSettingsStruct_t *CurrentNetworkSettings = NULL;

	if( !Status || irescue < 0 )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

    initNetworkLibStruct(Status->Net,NET_NETWORK_INTERFACES_COUNT);
	
	//���� ������ �� ��������� ������ ��������!!!
	if( irescue )
	{
		//!TODO - ����� ��� ���� ���������� ���� ��������� ���������
		if( AdapterSettingsLoadDefault( &Status->GlobalDevice_t, &DefaultAdapterSettings ) < 0 )
		{
			DBGERR( "%s: It wasn't possible to load default settings\n", __func__ );
			return -1;
		}

		if( AdapterPowerControlEthernet( &Status->GlobalDevice_t, &DefaultAdapterSettings ) < 0 )
		{
			DBGERR( "%s: It wasn't possible to control ethernet\n", __func__ );
			return -1;
		}

		CurrentNetworkSettings = &DefaultAdapterSettings;
	}

	else
	{
		CurrentNetworkSettings = &Status->AdapterSettings;
	}

#if 1 //===== set only tcpip
    Status->AdapterSettings.Block0.AdapterSettings.Connect &= ~AXIS_SOFTADAPTER_CONNECT__PROTOCOL_TYPE;
    Status->AdapterSettings.Block0.AdapterSettings.Connect |= AXIS_SOFTADAPTER_CONNECT__PROTOCOL_TYPE_TCP_IP;
#endif
	
	
	//���� ������� �� ����� ����� - ������ ��� ���� � ����
	if( Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature & DM_DM_DISABLE_ALL_NET )
	{
		DBGLOG( "%s: This device have disabled network\n", __func__ );
		return 0;
	}

	if( Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature & DM_DM_ENABLE_DYNAMIC_NET )
	{
        DBGLOG( "%s: Start Init dynamic NET \n", __func__  );
		if( Status->GlobalDevice_t.dmdev_eth.dmctrl_feature & DM_NET_ETH_HW &&
				Status->GlobalDevice_t.dmdev_eth.dmctrl_ipresent )
		{
            DBGLOG( "%s: Configuring HW Net \n", __func__  );
			libnetcontrol_t *Net = AdapterNetworkGetFreeLibControlStruct( Status );

			if( !Net )
			{
				DBGERR( "%s: No free net struct\n", __func__ );
				return -1;
			}

			if( adapter_net_config_start_ethernet( CurrentNetworkSettings , Net ) < 0 )
			{
				DBGERR( "%s: It wasn't possible to start ethernet\n", __func__ );
				AdapterNetworkFreeLibControlStruct( Status, Net );
				return -1;
			}

		}
		INFO_LOG( 0, "Start Dynamic modem search" );
		return 0;
	}

	libnetcontrol_t *Net = AdapterNetworkGetFreeLibControlStruct( Status );

	if( !Net )
	{
		DBGERR( "%s: No free net struct\n", __func__ );
		return -1;
		INFO_LOG( 1, "Start Statuc modem activated" );
	}

	DBGLOG( "%s: Status->AdapterSettings.Block0.AdapterSettings.Connect = %i (%i)\n", __func__, 
				Status->AdapterSettings.Block0.AdapterSettings.Connect,
				Status->AdapterSettings.Block0.AdapterSettings.Connect & AXIS_SOFTADAPTER_CONNECT__ADAPTER_TYPE );

	switch( Status->AdapterSettings.Block0.AdapterSettings.Connect & AXIS_SOFTADAPTER_CONNECT__ADAPTER_TYPE )
	{

		case AXIS_SOFTADAPTER_CONNECT__ADAPTER_TYPE_ETH:
			{
				//��� ������ � ��� ���� ����� ������ �������
				//������� �� ����� ��� ��� � ����� ������ ������ ���� ������
				// � ��� ������ �� ����� ����� �������� ������� ���� �������
				if( adapter_net_config_start_ethernet( CurrentNetworkSettings, Net ) < 0 )
				{
					DBGERR( "%s: It wasn't possible to start ethernet\n", __func__ );
					AdapterNetworkFreeLibControlStruct( Status,Net );
					ret = -1;
				}

				break;

			}
		case AXIS_SOFTADAPTER_CONNECT__ADAPTER_TYPE_WIFI:
			{
				if( adapter_net_config_start_wifi( CurrentNetworkSettings, Net ) < 0 )
				{
					DBGERR( "%s: It wasn't possible to start wifi\n", __func__ );
					AdapterNetworkFreeLibControlStruct( Status,Net );
					ret =-1;
				}

				break;
			}
		case AXIS_SOFTADAPTER_CONNECT__ADAPTER_TYPE_MODEM:
			{
				if( adapter_net_config_start_cdma( CurrentNetworkSettings, Net ) < 0 )
				{
					DBGERR( "%s: It wasn't possible to start modem\n", __func__ );
					AdapterNetworkFreeLibControlStruct( Status,Net );
					ret = -1;
				}

				break;
			}
		case AXIS_SOFTADAPTER_CONNECT__ADAPTER_TYPE_LTE:
			{
				if( adapter_net_config_start_lte( CurrentNetworkSettings, Net ) < 0 )
				{
					DBGERR( "%s: It wasn't possible to start modem\n", __func__ );
					AdapterNetworkFreeLibControlStruct( Status,Net );
					ret= -1;
				}

				break;
			}
		default:
			{
				DBGERR( "%s: Invalid type of connection \n", __func__ );
				AdapterNetworkFreeLibControlStruct( Status,Net );
				ret =  -1;
			}
	}

	INFO_LOG( ret, "Start Statuc modem activated" );
	DBGLOG( "%s: %i field\n",__func__,__LINE__ );


	return ret;
}

/**
 * @brief �������, ������� ��������� ��������� �������� ����
 *
 * @param StatusAdapter - ��������� �� ��������� ��������
 * @param CurrenNet - ��������� �� ������� ����
 * @param NewNetType - ��� ����� ���� ��� �������
 * @return int - 0 ��� ��, ����� ���� ����� ������
 */
static int adapter_net_start_same_child( AdapterGlobalStatusStruct_t *StatusAdapter, libnetcontrol_t *CurrenNet, libnettype_t NewNetType )
{
	const char *NewNetName = AdapterNetworkGetConnectionTypeName( NewNetType, 0 );
	int ret;

	DBGLOG( "%s: Nead start %s library\n", __func__, NewNetName );

	//��������� � �� ��������� �� ��� ���������� PPPoE - ���� �� �� �������
	if( AdapterNetworkGetLibControlByConnectionType( StatusAdapter, NewNetType ) )
	{
		DBGLOG( "%s: %s library already started\n", __func__, NewNetName );
	}
	else
	{
		DBGLOG( "%s: Nead to start new %s library\n", __func__, NewNetName );

		libnetcontrol_t *NewNet = AdapterNetworkGetFreeLibControlStruct( StatusAdapter );

		if( !NewNet )
		{
			DBGERR( "%s: No free network struct for start %s\n", __func__, NewNetName );

			return -AXIS_ERROR_BUSY_DEVICE;
		}

		//�� �� � ������ �������������� - ���������� ����������� ���������
		ret = adapter_net_config_start_any_net( &StatusAdapter->AdapterSettings, NewNet, NewNetType );

		if( ret < 0 )
		{
			DBGERR( "%s: It wasn't possible to start %s\n", __func__, NewNetName );

			AdapterNetworkFreeLibControlStruct( StatusAdapter, NewNet );

			return ret;
		}
	}

	return 0;
}

/**
 * @brief �������, ������� ��������� �������� ����������� � ����
 *
 * @param StatusAdapter - ��������� �� ���������� ��������� ������� ��������
 * @param CurrenNet - ��������� �� ������� ����
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 */
int AdapterNetworkStartChildNetwork( AdapterGlobalStatusStruct_t *StatusAdapter, libnetcontrol_t *CurrenNet )
{
	int ret = 0;

	if( !StatusAdapter || !CurrenNet )
	{
		DBGERR( "%s: Invalid argument\n", __func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	if( StatusAdapter->RescueMode )
	{
		//�� � ������ �������������� - �� ����� VPN �� ���������
		DBGLOG( "%s: Connect event on reacue mode - nothing to do\n", __func__ );
	}
	else if( CurrenNet->net_lib_version == netversionSeparateVPN )
	{
		DBGLOG( "%s: Connect event from separeate VPN library - try check VPN network\n", __func__ );

		//� ��� ����������� �� ����������, ������� �� ����� ������ VPN - ��� ����� ������
		//��� ����� ���������, � ������� ��������� ������� VPN?
		//���� �������, �� ����� ����� VPN ���������� ��� ��������?

		//NOTE ����� ���� ��������, ����� ������ ������ VPN �� ������ ��������� ������!!!
		//� ��� ����� ���������!!!
		// - ��������, ������ PPoE ���������� ��������� PPtP VPN ����������
		// 0  - � PPoE ���������� �� �������, ��� ��� ����� ������ � ����� ��������� VPN ������
		// 1  - � Ethernet ���������� �� ������� ��� ��� ����� ������ � ��� VPN ����������� ��������
		// 2  - � Ethernet ���������� �����, ��� ����� ��������� PPoE ���������� - ������ �� ���������� IP �� ����������
		// 3  - Ethernet ��������� �������� ������� ������� - �� �������� ����
		// 4  - �� �����, ��� ����� ��������� PPoE ��� ����������
		// 5  - �� �������, ��� PPoE ���������� �� �������� - � ��������� ��
		// 6  - PPoE ���������� �������� ������� ������� - �� �������� ����
		// 7  - �� �����, ��� ����� ��������� PPoE � ��� ��� �������� - �� ���� �����
		// 8  - �� �����, ��� ����� ��������� PPtP ���������� � ��� �� �������� - ��������� ��
		// 9  - PPtP ��������� �������� ������� connect - �� �������� ����
		// 10 - �� �����, ��� ����� ��������� PPoE � ��� ��� �������� - �� ���� �����
		// 11 - �� �����, ��� ����� ��������� PPtP � ��� ��� �������� - �� ���� �����
		// 12 - ������!!!

		if( CurrenNet->config.connection_config & DM_NET_CONFIG_IP_PPOE )
		{
			ret = adapter_net_start_same_child( StatusAdapter, CurrenNet, nettype_ppoe );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't start new PPoE network\n", __func__ );
			}

			//��� ��� PPoE ��� ����� ������ ���������� �� �� ������ ��������� ������
			//������� - ��� �����, ����� PPoE ������� ������� �� �������� �����������
			return ret;
		}

		if( CurrenNet->config.connection_config & DM_NET_CONFIG_IP_PPTP )
		{
			ret = adapter_net_start_same_child( StatusAdapter, CurrenNet, nettype_pptp );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't start new PPtP network\n", __func__ );
			}
		}

		//TODO - ��� ������ ���� �������� �� ��������� ���� VPN � �������������� ���������
	}
	else
	{
		DBGLOG( "%s: Connect event from normal library - do nothing\n", __func__ );
	}

	return ret;
}

/**
 * @brief �������, ������� ������������� �������� ����, ���� ����
 *
 * @param StatusAdapter - ��������� �� ��������� ������� ��������
 * @param CurrenNet - ��������� �� ������� ����, ��� ������� ������������� ��������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 */
int AdapterNetworkStopChildNetwork( AdapterGlobalStatusStruct_t *StatusAdapter, libnetcontrol_t *CurrenNet )
{
	libnetcontrol_t *VPNNet = NULL;

	if( !StatusAdapter || !CurrenNet )
	{
		DBGERR( "%s: Invalid argument\n", __func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	if( CurrenNet->net_lib_version == netversionSeparateVPN )
	{
		DBGLOG( "%s: Disconnect event from separeate VPN library - try check VPN network\n", __func__ );

		//NOTE - ��� ����������� ���� �� ������� ��������� ������� ������ ������ ��� ����� ��� ����

		//������� �� ��� - �� ������ �� ���� ����
		if( CurrenNet->net_lib_type != nettype_ppoe && CurrenNet->config.connection_config & DM_NET_CONFIG_IP_PPOE )
		{
			DBGLOG( "%s: Need stop ppoe library\n", __func__ );

			VPNNet = AdapterNetworkGetLibControlByConnectionType( StatusAdapter, nettype_ppoe );
		}

		if( CurrenNet->net_lib_type != nettype_pptp && CurrenNet->config.connection_config & DM_NET_CONFIG_IP_PPTP )
		{
			DBGLOG( "%s: Need stop pptp library\n", __func__ );

			VPNNet = AdapterNetworkGetLibControlByConnectionType( StatusAdapter, nettype_pptp );
		}

		//� ��� ����, ��� ���������!!!
		if( VPNNet )
		{
			const char *netlibname = AdapterNetworkGetConnectionTypeName( VPNNet->net_lib_type, 0 );

			DBGLOG( "%s: Try stop %s network library\n", __func__, netlibname );

			//���� �� ���������� �������� ���� ��� ��� ���� �������, ������, ���
			//�������� ���� ���� ����������� - ������ �� ������ ������ ���������
			//������� ���������� - � ������� ��� � ���!!!
			int iret = AdapterNetworkStop( StatusAdapter, VPNNet );

			if( iret < 0 )
			{
				DBGERR( "%s: Can't stop %s network\n", __func__, netlibname );
			}
		}
	}
	else
	{
		DBGLOG( "%s: Disconnect event from normal library - do nothing\n", __func__ );
	}

	return 0;
}

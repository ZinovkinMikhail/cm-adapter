//������� ������������� ���� ����� WiFi �������

//����������

#include <sys/ioctl.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <net/if_arp.h>		// For ARPHRD_ETHER 
//��������� �������
#include <axis_softadapter.h>
#include <axis_pppoe.h>
#include <axis_pptp.h>
#include <axis_dhcp_client.h>
#include <axis_routes.h>
#include <axis_error.h>

#include <adapter/network.h>


//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
//#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif


//������� ������� ������������ ������� ����������, �� �� �������
//����: ��������� �� ��������� � �����������
//�������:
// 0 -�� �������
// ������ 0 - � ������ ������
int AdapterNetworkConnectionRestart( AdapterGlobalStatusStruct_t* Status )
{
	int ret;

	if ( !Status )
	{
		DBGERR( "%s: Invalid argument\n", __func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	ret = AdapterAllNetworksStop( Status );

	if( ret < 0 )
	{
		DBGERR( "%s: Can't stop all networks\n", __func__ );
		return ret;
	}

	ret = AdapterNetworkInit( Status, 0 );

	if ( ret < 0 )
	{
		DBGERR( "%s: It wasn't possible to start network", __func__ );
		return ret;
	}

	return 0;
}

//�������, ������� ��������� ������� ������� � �������� �� ������
//������������

//������: 2016.12.08 19:12
//�����: ��������� ����� ��� "����" ������
//�����: i.podkolzin@ross-jsc.ru

#include <netinet/in.h>

#include <axis_error.h>
#include <axis_icmp.h>
#include <axis_routes.h>

#include <adapter/network.h>
#include <adapter/global.h>
#include <adapter/leds.h>
#include <adapter/recorder.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
//#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif

/**
 * @brief �������, ������� ������� ���� � ������� ���������, ���� ��� �����
 *
 * @param Status - ��������� �� ���������� ��������� ������� ��������
 * @param host - ����� ����� � ��������� ������� ����
 * @return int -  ����� 0 � ������ ������, 0 - ������ �� ��������, ����� 0 - ������ ��������
 **/
static int AdapterNetworkPing( AdapterGlobalStatusStruct_t *Status, uint32_t host )
{
	int ret = 0;

	if( !Status || !host )
	{
		DBGERR( "%s: Invalid argumen\n", __func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	ret = axis_icmp_ping_i( host );

	if( ret < 0 )
	{
		DBGERR( "%s: Axis ping return error\n", __func__ );
	}

	//��� ������� � ���������� ���� - ������� ��������������� �����������
	if( ret <= 0 )
	{
		//� � ����� ����� �������� - ��� ����������
		ret = AdapterLedNetworkNoConnectionBlink( Status );
	}

	return ret;
}


/**
 * @brief �������, ������� ��������� ������� ����� � Google DNS �������� 8.8.8.8
 *
 * @param Status ...
 * @return int
 **/
int AdapterNetworkPingGoogleHost( AdapterGlobalStatusStruct_t *Status )
{
	return AdapterNetworkPing( Status, 0x08080808 ); //TODO - ���������
}

/**
 * @brief ������� ������� ��������� ������� ����� � �������� ���� �� ������ ������������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - ����� 0 � ������ ������, 0 - ������ �� ��������, ����� 0 - ������ ��������
 **/
int AdapterNetworkPingServerHost( AdapterGlobalStatusStruct_t *Status )
{

	if( !Status )
	{
		DBGERR( "%s: Invalid argumen\n", __func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	if( !Status->AdapterSettings.Block0.AdapterSettings.ServerIPAddress )
	{
		DBGERR( "%s: Server Host not defined\n", __func__ );
		return 0;
	}

	return AdapterNetworkPing( Status,Status->AdapterSettings.Block0.AdapterSettings.ServerIPAddress );
}

/**
 * @brief �������, ������� ������� �������� ���� LTE �����������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - ����� 0 � ������ ������, 0 - ������ �� ��������, ����� 0 - ������ ��������
 **/
int AdapterNetworkPingLteGateWay( AdapterGlobalStatusStruct_t *Status )
{
	libnetcontrol_t *LteConnection;

	if( !Status )
	{
		DBGERR( "%s: Invalid argumen\n", __func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	LteConnection = AdapterNetworkGetLibControlByConnectionType( Status, nettype_lte );

	if( !LteConnection )
	{
		DBGERR( "%s: No LTE connection found\n", __func__ );
		return 0;
	}

	return AdapterNetworkPing( Status, LteConnection->config.GWAddress );
}

/**
 * @brief �������, ������� ������� ���� �� ���������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - ����� 0 � ������ ������, 0 - ������ �� ��������, ����� 0 - ������ ��������
 **/
int AdapterNetworkPingDefaultGateWay( AdapterGlobalStatusStruct_t *Status )
{

	struct struct_net_param ifp;
	int ret;

	if( !Status )
	{
		DBGERR( "%s: Invalid argument\n", __func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	ret = axis_routes_get_default_gw( &ifp );

	if( ret < 0 )
	{
		DBGERR( "%s: Can't get default gw\n", __func__ );
		return ret;
	}

	if( !ifp.gw_addr )
	{
		DBGERR( "%s: No default gate way detected\n", __func__ );
		return -AXIS_ERROR_INVALID_VALUE;
	}

	return AdapterNetworkPing( Status, ifp.gw_addr );
}

/**
 * @brief �������, ������� ������� ���� � �������� ��������� �� �����
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @param host - IP ����� ����� � ������� ������� ����
 * @param Message - ��������� ������� ����� ���������� (�������� �����)
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
static int AdapterNetworkPingAndPrint( AdapterGlobalStatusStruct_t *Status, uint32_t host, const char *Message )
{
	char hbuffer[ 64 ];
	int ret;

	if( !Status || !host || !Message )
	{
		DBGERR( "%s: Invalid argument\n", __func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	axis_sprint_ip( hbuffer, host );

	printf( "Try ping %s '%s'\n", Message, hbuffer );

	ret = AdapterNetworkPing( Status, host );

	if( ret < 0 )
	{
		DBGERR( "%s: Can't ping '%s'\n", __func__, hbuffer );
	}

	else if( !ret )
	{
		printf( "Host %s '%s' is dead\n", Message, hbuffer );
	}

	else
	{
		printf( "Host %s '%s' is alive\n", Message, hbuffer );
	}

	return ret;
}

/**
 * @brief ������� ���������� ���� ��������� ������ � ������� ���������� �� �������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterNetworkPingAllStdout( AdapterGlobalStatusStruct_t *Status )
{
	//������� ��� �� ���� ����� ���������

	struct struct_net_param ifp;
	int ret;

	if( !Status )
	{
		DBGERR( "%s: Invalid argument\n", __func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	ret = axis_routes_get_default_gw( &ifp );

	if( ret < 0 )
	{
		DBGERR( "%s: Can't get default gw\n", __func__ );
		return ret;
	}

	if( !ifp.gw_addr )
	{
		DBGLOG( "%s: No default gate way detected\n", __func__ );
	}

	if( ifp.gw_addr )
	{

		ret = AdapterNetworkPingAndPrint( Status, ifp.gw_addr, "default GW" );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't ping default GW\n", __func__ );
		}

		ret = AdapterNetworkPingAndPrint( Status, 0x08080808, "Google DNS" );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't ping Google DNS\n", __func__ );
		}

		if( Status->AdapterSettings.Block0.AdapterSettings.ServerIPAddress )
		{
			ret = AdapterNetworkPingAndPrint( Status, Status->AdapterSettings.Block0.AdapterSettings.ServerIPAddress, "server host" );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't print server host\n", __func__ );
			}
		}

		else
		{
			fprintf( stdout, "No server host set\n" );
		}

	}

	else
	{
		fprintf( stdout, "No default GW detected\n" );
	}

	return 0;

}

//������� ��������� � �����

//������: 2010.09.16 13:56
//�����: ��������� ����� ��� "����"
//�����: i.podkolzin@ross-jsc.ru

#include <sys/ioctl.h>
#include <net/if.h>

#include <axis_error.h>
#include <axis_dmlibs_resource.h>
#include <axis_routes.h>
#include <axis_sprintf.h>

#include <dispatcher_lib/dispatcher_lib_resource.h>

#include <dispatcher_lib/events.h>
#include <dispatcher_lib/network.h>

#include <dmxxx_lib/dmxxx_calls.h>

#include <adapter/leds.h>
#include <adapter/usb.h>
#include <adapter/interface.h>
#include <adapter/power.h>
#include <adapter/debug.h>
#include <adapter/sms.h>

#include <adapter/network.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif

/**
 * @brief �������, ������� �������� �������� ����� ����� ��� ������������ �� ���������� DM
 *
 * @param type - ��� ������� ����������
 * @param dev - ��������� �� ��������� ������������
 * @return int - ����� �����, 0 �����������, ����� 0 � ������ ������
 */
static int adapter_net_get_dmxx_port( libnettype_t type, dmdev_t *dev )
{
	int DevPort = -1;

	if( !dev )
	{
		DBGERR( "%s: Invalid argument\n", __func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	switch( type )
	{
		case nettype_eth:
		{
			//NOTE - � ���������� ������������ ��� ������� ��� ��������� ����� Ethernet
			return 0;
		}
		case nettype_wifista:
		{
			DevPort = DMXXXUsbGetWiFiPort( dev );
			break;
		}
		case nettype_modem:
		{
			DevPort = DMXXXUsbGetCDMAPort( dev );
			break;
		}
		case nettype_lte:
		{
			DevPort = DMXXXUsbGetLTEPort( dev );
			break;
		}
		default:
		{
			DBGERR( "%s: Not support Get USB port for library '%s'\n", __func__, AdapterNetworkGetConnectionTypeName( type, 0 ) );
			return 0;
		}
	}

	if( DevPort < 0 )
	{
		DBGERR( "%s: Can't get static %s modem USB port\n", __func__, AdapterNetworkGetConnectionTypeName( type, 0 ) );
	}

	return DevPort;
}

/**
 * @brief �������, ������� ����������� ��� ���������� � ��������� �� ���������� axis
 *
 * @param type - ��� ������� ����������
 * @return int - ��������� �� ���������� axis_lib, ����� 0 � ������ ������
 */
int AdapterNetworkNetLibTypeToSoftAdapterType( libnettype_t type )
{
	switch( type )
	{
		case nettype_eth:
		{
			return AXIS_SOFTADAPTER_CONNECT__ADAPTER_TYPE_ETH;
		}
		case nettype_wifista:
		{
			return AXIS_SOFTADAPTER_CONNECT__ADAPTER_TYPE_WIFI;
		}
		case nettype_modem:
		{
			return AXIS_SOFTADAPTER_CONNECT__ADAPTER_TYPE_MODEM;
		}
		case nettype_lte:
		{
			return AXIS_SOFTADAPTER_CONNECT__ADAPTER_TYPE_LTE;
		}
		default:
		{
			DBGERR( "%s: Converted invalid network library type %u\n", __func__, type );
			return -AXIS_ERROR_INVALID_VALUE;
		}
	}
}

/**
 * @brief �������, ������� ���������� ������������ ���� ������� ����������
 *
 * @param type - ��� ������� ����������
 * @param lib - ���� ����, ��� ����� ������� � ������ ��������
 * @return const char* - ��������� �� ������ � ������
 */
const char *AdapterNetworkGetConnectionTypeName( libnettype_t type, int lib )
{
	switch( type )
	{
		case nettype_eth:
		{
			return lib ? "ethernet" : "Ethernet"; //TODO - ���������!!!
		}
		case nettype_wifista:
		{
			return lib ? "wifi" : "WiFi";
		}
		case nettype_modem:
		{
			return lib ? "cdma" : "Modem";
		}
		case nettype_lte:
		{
			return lib ? "lte" : "LTE";
		}
		case nettype_wifiap:
		{
			return lib ? "wifiap" : "WiFi Access Point" ;
		}
		case nettype_vlan:
		{
			return lib ? "vlan" : "VLan";
		}
		case nettype_bond:
		{
			return lib ? "bond" : "Interface Bonding";
		}
		case nettype_reserv:
		{
			return lib ? "reserv" : "Interface Reserving";
		}
		case nettype_tun:
		{
			return lib ? "tun" : "Tun interface";
		}
		case nettype_tap:
		{
			return lib ? "tap" : "Tap interface";
		}
		case nettype_pptp:
		{
			return lib ? "pptp" : "PPtP VPN";
		}
		case nettype_ppoe:
		{
			return lib ? "ppoe" : "PPoE VPN";
		}
		case nettype_openvpn:
		{
			return lib ? "openvpn" : "OpenVPN";
		}
		case nettype_ipsec:
		{
			return lib ? "ipsec" : "IPSec tunnel";
		}
		case nettype_wireguard:
		{
			return lib ? "wireguard" : "WireGuard VPN";
		}
		case nettype_max:
		case nettype_closed:
		default:
		{
			DBGERR( "%s: Converted invalid network library type %u\n", __func__, type );
			return "Invalid";
		}
	}
}

int AdapterNetworkWorkISSETPoll(AdapterGlobalStatusStruct_t* Status, 
															fd_set *rfds)
{
	//some stupid decision but no time
	int poll_select = 0;

	int z =0;

	for(z = 0; z < NET_NETWORK_INTERFACES_COUNT; z++)
	{

		if( !Status->Net[z].netlib )
			continue;

		if( ( Status->Net[z].net_lib_type <= nettype_closed ||
			  Status->Net[z].net_lib_type >= nettype_max		) &&
			Status->Net[z].EnablePollingModem &&
			Status->Net[z].PollingModem.fdpoll )
		{
			DBGERR( "%s: Not init network at index %i have a pulling handle (%i)\n", __func__, z, Status->Net[z].PollingModem.fdpoll );
		}
		
		if( Status->Net[z].PollingModem.fdpoll > 0 &&
					FD_ISSET( Status->Net[z].PollingModem.fdpoll, rfds ) )
		{
			
			DBGLOG( "%s: Usb device select EVENT\n", __func__ );

			// 			DBGLOG("%s: %i Polling event\n",__func__,__LINE__);
			if( AdapterNetworkGetConnectionStatus( &Status->Net[z] ) < 0 )
			{
				DBGERR( "%s: Can't get status exit\n", __func__ );
				//continue;
			}
			if( Status->Net[z].status.imodemreset != DM_MODEM_ENABLE_RESET )
			{
				DBGLOG( "%s: %i Reset Modem at net %p\n", __func__, __LINE__, &Status->Net[z] );
				if( AdapterNetworkStop( Status, &Status->Net[z] ) < 0 )
				{
					DBGERR( "%s: Can't stop network at index %u\n", __func__, z );
				}
				AdapterNetworkUsbOut(Status, &Status->Net[z] );
// 				AdapterUsbDeinitPolling(&AdapterGlobalStatus.PollingModem);
			}
			else
			{
				DBGLOG( "%s: %i Reset Poll\n", __func__, __LINE__ );
				AdapterUsbResetPoll( &Status->Net[z] );

				//���� ���������� ������� - ��� ��� ��� ����� ��-�� ��� ���������
				//� ����� ��������� �� �����!!!
				DBGLOG( "%s: Stop poling old descriptor\n", __func__ );
				if( AdapterUsbDeinitPolling( &Status->Net[z].PollingModem ) < 0 )
				{
					DBGERR( "%s: Can't deinit old polling\n", __func__ );
				}

			}
			poll_select = 1;
			break;

		}

	}
	
	return poll_select;

}

int AdapterNetworkSetSelectHendelPoll(AdapterGlobalStatusStruct_t* Status, 
													fd_set *rfds,int max_sel)
{

	int z = 0;

	for(z = 0; z < NET_NETWORK_INTERFACES_COUNT; z++)
	{

		if( !Status->Net[z].netlib )
			continue;

		if( (Status->Net[z].net_lib_type <= nettype_closed ||
			 Status->Net[z].net_lib_type >= nettype_max			) &&
			Status->Net[z].EnablePollingModem )
		{
			DBGERR( "%s: Try set pulling for not initing network at index %d handle=%d\n", __func__, z, Status->Net[z].PollingModem.fdpoll );
			continue;
		}

		if( Status->Net[z].PollingModem.fdpoll > 0 )
		{
				max_sel = AdapterGlobalWileSetHendel(
		                  Status->Net[z].PollingModem.fdpoll,
		                  rfds,
		                  max_sel );
		}
	}

	return max_sel;

}

/**
 * @brief �������, ������ � ����� ������� ���������� ����������� � ����
 *
 * @param b - ��������� �� �����, ���� ���������� ������
 * @param bsize - ������ ���������� ����� � ������
 * @param status - ��������� �� ��������� ������� �������� �����������
 * @param Connect - ��� ���������� � �����
 * @param num - ����� �������� ����������
 * @return int - ���������� ���������� � ����� ����
 **/
static int AdapterIniFileSprintfStatusDevInfo__One( char *b, size_t bsize, connection_status_t *status, libnettype_t Connect_, int num )
{
	int cc = 0;
	int lcc = 0;
	size_t size = bsize;

	if( !b || !bsize || bsize >= SIZE_MAX || !status )
	{
		DBGERR( "%s: Invalid argument\n", __func__ );
		return 0;
	}

	uint32_t uptimesec;

	DBGLOG( "%s: Status = %i \n",__func__, status->status );

	lcc = axis_sprintf_shablon_t( b + cc, size,
								DEV_STATUS_TYPE_SHABLON, num, AdapterNetworkGetConnectionTypeName( Connect_, 0 ) );

	cc += lcc;
	size -= lcc;

	if( !size || size >= SIZE_MAX ) goto end;

	// ��� ������������
	if( status->status == DM_NET_CONNECTED )
	{
		lcc = axis_sprintf_shablon_t( b+cc, size, DEV_STATUS_STATUS_SHABLON,num,
							  DEV_STATUS_STATUS_VALUE_CONNECTED );
		cc += lcc;
		size -= lcc;

		if( !size || size >= SIZE_MAX ) goto end;

		//������ ������� ����������
		uptimesec = time( NULL ) - status->connection_time;

		lcc = axis_sprintf_shablon_d( b+cc, size, DEV_STATUS_TIME_SHABLON,num,  uptimesec );
		cc += lcc;
		size -= lcc;

		if( !size || size >= SIZE_MAX ) goto end;

		
		lcc = axis_sprintf_shablon_d( b+cc, size, DEV_STATUS_LEVEL_SHABLON,
															num, status->level );
		cc += lcc;
		size -= lcc;

		if( !size || size >= SIZE_MAX ) goto end;

		lcc = axis_sprintf_shablon_d( b+cc, size, DEV_STATUS_QUALITY_SHABLON,
														num, status->quality );
		cc += lcc;
		size -= lcc;

		if( !size || size >= SIZE_MAX ) goto end;

		//��� ��������� ���������� LTE ����������� ����� �������� � ������� ��������
		if( Connect_ == nettype_lte )
		{
			const char *value = NULL;

			value = AdapterLTEModemBandToString( status->band );

			if( value )
			{
				lcc = axis_sprintf_shablon_t( b+cc, size, DEV_STATUS_BAND_SHABLON, num, value );

				cc += lcc;
				size -= lcc;

				if( !size || size >= SIZE_MAX ) goto end;
			}
		}
	}

	else if( status->status == DM_NET_DISCONNECTED )
	{
		DBGLOG("%s: Print disconnected status \n", __func__ );
		lcc = axis_sprintf_shablon_t( b+cc, size, DEV_STATUS_STATUS_SHABLON,num,
							  DEV_STATUS_STATUS_VALUE_DISCONNECTED );
		cc += lcc;
		size -= lcc;

		if( !size || size >= SIZE_MAX ) goto end;
	}
	else if( status->status == DM_NET_NO_NETWORKS )
	{
		DBGLOG("%s: Print no networks status \n", __func__ );
		lcc = axis_sprintf_shablon_t( b+cc, size, DEV_STATUS_STATUS_SHABLON,num,
							  DEV_STATUS_STATUS_VALUE_NO_NETWORKS );
		cc += lcc;
		size -= lcc;

		if( !size || size >= SIZE_MAX ) goto end;
	}
	else if( status->status == DM_NET_SCANNING )
	{
		DBGLOG("%s: Print scannng status \n", __func__ );
		lcc = axis_sprintf_shablon_t( b+cc, size, DEV_STATUS_STATUS_SHABLON,num,
							  DEV_STATUS_STATUS_VALUE_SCANNING );
		cc += lcc;
		size -= lcc;

		if( !size || size >= SIZE_MAX ) goto end;
	}
	else if( status->status == DM_NET_CONNECTION_PROGRESS )
	{
		DBGLOG("%s: Print progress status \n", __func__ );
		lcc = axis_sprintf_shablon_t( b+cc, size, DEV_STATUS_STATUS_SHABLON,num,
							  DEV_STATUS_STATUS_VALUE_PROGRESS );
		cc += lcc;
		size -= lcc;

		if( !size || size >= SIZE_MAX ) goto end;
	}
	else if( status->status == DM_NET_APSENT )
	{
		DBGLOG("%s: Print apsent status \n", __func__ );
		lcc = axis_sprintf_shablon_t( b+cc, size, DEV_STATUS_STATUS_SHABLON,num,
							  DEV_STATUS_STATUS_VALUE_APSENT );
		cc += lcc;
		size -= lcc;

		if( !size || size >= SIZE_MAX ) goto end;
	}
	else if( status->status == DM_NET_ERROR )
	{
		DBGLOG("%s: Print error status \n", __func__ );
		lcc = axis_sprintf_shablon_t( b+cc, size, DEV_STATUS_STATUS_SHABLON,num,
							  DEV_STATUS_STATUS_VALUE_ERROR );
		cc += lcc;
		size -= lcc;

		if( !size || size >= SIZE_MAX ) goto end;
	}


end:
	b[ bsize - 1 ] = 0x00;
	return cc;

}



int AdapterNetworkPrintIniNetworkInfo(char* b,size_t bsize, AdapterGlobalStatusStruct_t* Status)
{
	int cc = 0;
	int lcc = 0;
	size_t size = bsize;


	if( AdapterNetworkGetConnectionStatuses( Status) < 0)
	{
		DBGERR("%s: It wasn't possible to get connection status\n",__func__);
		goto end;
	}

#warning "May call from different thread must set mutex"

	int i  = 0;
	//������
	lcc = axis_sprintf_s( b+cc, size, DEV_STATUS_SECTION );
	cc	   += lcc;
	size   -= lcc;

	if( size <= 0 ) goto end;
	
	int PCount = 1;

	for( i = 0; i < NET_NETWORK_INTERFACES_COUNT; i++)
	{

		if( !Status->Net[i].netlib )
		{
			DBGLOG( "%s: network %d can't have a net\n", __func__, i );
			continue;
		}

		if( Status->Net[i].net_lib_type <= nettype_closed )
		{
			DBGLOG( "%s: nework %d is closed\n", __func__, i );
			continue;
		}
		
		DBGLOG( "%s: Try print %d network lbb=%p\n",  __func__, i, Status->Net[i].netlib );
	
		lcc = AdapterIniFileSprintfStatusDevInfo__One( b+cc, size, &Status->Net[i].status,
														Status->Net[i].net_lib_type, PCount++ );
		cc += lcc;
		size -= lcc;
		if( size <= 0 ) goto end;

		//��� ��� �� ����� ��������� ������� ��� ����������, �� ��� ������ ���� �����:
		//[DeviceStatus]
		//DeviceStatus.Count=2
		//001_DeviceStatus.Status=Disconnected
		//002_DeviceStatus.Status=Connected
		//002_DeviceStatus.ConnectionTime=99
		//002_DeviceStatus.Type=Ethernet
		//002_DeviceStatus.Level=19
		//002_DeviceStatus.Qualiti=3
	}
	
	if( PCount > 0 ) //����� ��� �� ����� ����, �������� ��� ������, �� ���������� ���� �������� ��� �� �� ���������� ������ ������
	{
		lcc = axis_sprintf_d( b + cc, size, DEV_STATUS_COUNT,
							  PCount-1 );
		cc += lcc;
		size -= lcc;

		if( size <= 0 ) goto end;
	}

end:
	b[ bsize - 1 ] = 0x00;
	return cc;
	

}
libnetcontrol_t* AdapterNetworkGetFreeLibControlStruct(AdapterGlobalStatusStruct_t* Status)
{
	if(!Status)
	{
		DBGERR("Invalid argument\n");
		return NULL;

	}
// 	DBGLOG( "%s: Net Interface count is %d\n", __func__, Status->net_interface_count );

	int i = 0;
	for(i = 0; i < NET_NETWORK_INTERFACES_COUNT; i++)
	{
		if( !Status->Net[i].netlib )
		{
			Status->Net[i].net_lib_type = nettype_closed;
// 			DBGLOG( "%s: Net Interface count is %d\n", __func__, Status->net_interface_count );
		 	DBGLOG( "%s: Returned %p [%d]\n", __func__, &Status->Net[i], i  );
			return &Status->Net[i];
		}
	}
	
	return NULL;
}

void AdapterNetworkFreeLibControlStruct(AdapterGlobalStatusStruct_t *Status,libnetcontrol_t* Net)
{

	if(!Status || !Net)
	{
		DBGERR("Invalid argument");
		return;
	}

	Net->net_lib_type = nettype_closed;
	Net->netlib = NULL;
}

/**
 * @brief �������, ������� ���������� ��������� �������� ����������� �� ����
 *
 * @param Status - ��������� �� ���������� ��������� ������� ��������
 * @param type - ��� �������� ����������� ��� �������� �������������� �����
 * @return libnetcontrol_t* - ��������� �� ���������, NULL ���� �� ������� ��� ������
 **/
libnetcontrol_t* AdapterNetworkGetLibControlByConnectionType( AdapterGlobalStatusStruct_t* Status, libnettype_t type )
{
	int i = 0;

	if( !Status || type <= nettype_closed || type >= nettype_max )
	{
		DBGERR( "%s: Invalid argument\n", __func__ );
		return NULL;
	}

	for( i = 0; i < NET_NETWORK_INTERFACES_COUNT; i++ )
	{
		if( Status->Net[i].net_lib_type == type )
		{
			DBGLOG( "%s: Returned %p [%d]\n", __func__, &Status->Net[i], i );

			return &Status->Net[i];
		}
	}

	return NULL;
}

int EventGetNew( DispatcherLibEventStatus_t *Status, int evtype )
{

	int i;

	if( !Status || !Status->EventCount )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	for( i = 0; i < Status->EventCount; i++ )
	{
		if( Status->Events[ i ].Type == evtype && !Status->Events[i].Flag )
		{
			Status->Events[i].Flag = 1;
			return i;
		}
	}

	return -AXIS_ERROR_INVALID_VALUE;
}

/**
 * @brief ������� ���������� ����� USB ����� �� ������� ��������� ����� ��������� ����
 *
 * @param Status - ��������� �� ���������� ��������� ������� ��������
 * @param Type - ��� ����������, ��� �������� ���� ����
 * @return int
 **/
static int AdapterNetworkGetNetworkUSBPort( AdapterGlobalStatusStruct_t *Status, libnettype_t Type )
{
	libnetcontrol_t *Net = AdapterNetworkGetLibControlByConnectionType( Status, Type );

	if( !Net && !Net->EnablePollingModem )
	{
		return -AXIS_ERROR_INVALID_VALUE;
	}

	return Net->PollingModem.port + 1;

}

/**
 * @brief �������, ������� �� IP ������ ��������� ��������� � ���������� ��� ��������� ���������
 * 	����������, ��� �� ������ ����� ��������� ������� ������� �� DHCP
 *
 * @param dhcp - ��������� �� ��������� � ����������� ����������
 * @param DeviceIP - IP �����, �� �������� ���������� �����
 * @return int - 1 ��� �� ��������� �������, 0 ��������� �� ������, ����� 0 � ������ ������
 */
static int AdapterNetworkGetDHCPConfig( struct struct_net_param *dhcp, uint32_t DeviceIP )
{
	int						ret = 0;
	int						n;
	int						cs;
	struct ifconf			ifc;
	struct ifreq			*ifr;
	int						numreqs = 30;
	struct struct_net_param	ifp; //��� �����������

	if( !dhcp || !DeviceIP )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	memset( dhcp, 0, sizeof( struct struct_net_param ) );

	cs = socket( AF_INET, SOCK_DGRAM,0);
	if( cs < 0)
	{
		DBGERR( "%s: No inet socket available\n", __func__ );
		ret = -AXIS_ERROR_CANT_CREATE_SOCKET;
	}

	ifc.ifc_buf = NULL;

	//�� ���������� ����� ��� ����� ���������
	for (;;)
	{
		ifc.ifc_len = sizeof( struct ifreq ) * numreqs;
		ifc.ifc_buf = realloc( ifc.ifc_buf, ifc.ifc_len) ;

		if( ioctl( cs, SIOCGIFCONF, &ifc ) < 0 )
		{
			DBGERR( "%s: Can't get SIOCGIFCONG\n", __func__ );
			ret = -AXIS_ERROR_CANT_IOCTL_DEVICE;
			goto close;
		}

		if( (unsigned int)ifc.ifc_len == sizeof(struct ifreq) * numreqs )
		{
			//��������� - �������� ��� 10
			numreqs += 10;
			continue;
		}
		break;
	}

	ifr = ifc.ifc_req;

	for( n = 0; n < ifc.ifc_len; n += sizeof( struct ifreq ) )
	{

		//���������� ifr->ifr_name - ����� ������� ��� � ��������
		//�� �� �� � �������������
		memset( &ifp, 0 , sizeof( ifp ) );

		//�������� ���������
		ret = axis_get_ifconfig( &ifp, ifr->ifr_name );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't get ifconfig for '%s'\n",__func__, ifr->ifr_name );
			break;
		}

		if( ifp.ip_addr == DeviceIP )
		{
			//�������� - ������ ���� - �� ������� � ������ �����
			ret = axis_routes_get_default_gw( &ifp );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't get defult system gateway\n", __func__ );
				break;
			}

			//�� ���� ����� - ������
			memcpy( dhcp, &ifp, sizeof( struct struct_net_param ) );

			ret = 1;

			break;
		}
		else
		{
			ret = 0; //NOTE - ��� ��� axis_get_ifconfig ���� ���������� 1
		}

		ifr++;
	}

close:

	close( cs );
	free( ifc.ifc_buf );

	return ret;
}

/**
 * @brief ������� ��������� ������, ����� � ������� ���������� ���������� �������
 *
 * @param status - ��������� �� ������ �������
 * @param data - ��������� �� ������������� ��� ������� ������
 * @param  type - ��� ���������� �� �������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 */
static int adapter_net_library_event_callback( connection_status_t *status, void *data, libnettype_t type )
{
	int ret;
	char MessageBuffer[ 1024 ];
	const char *netlibraryname = AdapterNetworkGetConnectionTypeName( type, 0 );

	DBGLOG( "%s:%i: Event From '%s' network library\n", __func__, __LINE__, netlibraryname );

	if( !status )
	{
		DBGERR( "%s:%i: %s invalid Argumentn\n", __func__, __LINE__, netlibraryname );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	//�������� ��������� �� ���������� ������ � ���� ������� ���������� � ����� ����� - ���� ��� ������
	AdapterGlobalStatusStruct_t *StatusAdapter = AdapterGlobalGetStatus();

	libnetcontrol_t *CurrenNet = AdapterNetworkGetLibControlByConnectionType( StatusAdapter, type );

	if( !CurrenNet )
	{
		DBGERR("%s:%i: %s no network library in pull\n", __func__, __LINE__, netlibraryname );
		return -AXIS_ERROR_NOT_INIT;
	}

	//�� ���� �������������� ��������
	switch( status->status )
	{
		case DM_NET_CONNECTED:
		{
			DBGLOG( "%s: %s modem have connect\n", __func__, netlibraryname );

//TODO - �� ������� - �� ��� LTE ������ DHCP �� ���������� - ������ ��� eth � wifi ��������?
//----------------------------------------------------------------------------------------------------
			//���� �� ������������ �� DHCP - �� ���� ������� ��� ���������, � �� ������ IP �����
			ret = AdapterNetworkGetDHCPConfig( &StatusAdapter->DHCPNetParam, status->ip );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't receive dhcp paramters from interface\n", __func__ );
				//NOTE - ��� �� ����������� ������ - ������ ����� ���� � ����������
			}
			else if( !ret )
			{
				DBGERR( "%s: Can't find device interface from connected IP\n", __func__ );
				//NOTE - ��� ���� �� ����������� ������ - ��������� ��� �� ����� ���� ������� �����������
				//������ � ���������� ����� ����
			}
//----------------------------------------------------------------------------------------------------------

			//�������� �������� ����������� � ����
			//������� ������ �������
			ret = snprintf( MessageBuffer, sizeof( MessageBuffer ), "%s connection establishad", netlibraryname );

			if( ret < 0 || ret >= sizeof( MessageBuffer ) )
			{
				DBGERR( "%s: Can't create message for event\n", __func__ );
				return -AXIS_ERROR_INVALID_ANSED;
			}

			ret = AdapterGlobalEventAddAndLogConnect( StatusAdapter, MessageBuffer, status->ip,  status->level, 0 );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't add new event to log\n", __func__ );
				//NOTE - ��� �� ����������� ������
			}

			//������ ������� �� ��� ������ ����� � ����� �������
			StatusAdapter->LastInterfaceQuire = time( NULL );

			//������ �������� �������� ����, ���� ��� �������
			int iret = AdapterNetworkStartChildNetwork( StatusAdapter, CurrenNet );

			if( iret < 0 )
			{
				DBGERR( "%s: Can't started child network\n", __func__ );
				//NOTE - �������� �������� ������� ���������� �� �������
			}

			break;
		}
		case DM_NET_DISCONNECTED:
		{
			DBGLOG( "%s: %s modem disconnected\n", __func__, netlibraryname );

			INFO_LOG( 0, "%s disconnected", netlibraryname );

			//������� DHCP ������ ���
			memset( &StatusAdapter->DHCPNetParam, 0, sizeof( struct struct_net_param ) );

			//��� �� ������� �������� ����, ���� ����
			int iret = AdapterNetworkStopChildNetwork( StatusAdapter, CurrenNet );

			if( iret < 0 )
			{
				DBGERR( "%s: Can't stop child network on primary disconnected\n", __func__ );
				//NOTE - �������� �������� ������� ���������� �� �������
			}

			break;
		}

		case DM_MODEM_ENABLE_RESET:
		{
			DBGERR( "%s: %s modem have a reset event\n", __func__, netlibraryname );

			//��� ����� ���� ��� ���� ��� �� �������� � ������� - �� ����� ��������� ��� � ���������� ������������
			int DevPort = adapter_net_get_dmxx_port( type, &StatusAdapter->GlobalDevice_t );

			if( DevPort < 0 )
			{
				DBGERR( "%s: Can't get static %s modem USB port\n", __func__, netlibraryname );
				return DevPort;
			}

			if( !DevPort )
			{
				//��� ��������� �� ���� ����������
				DBGLOG( "%s: No static %s modem USB port. Try search dinamic\n", __func__, netlibraryname );

				DevPort = AdapterNetworkGetNetworkUSBPort( StatusAdapter, type );

				if( DevPort < 0 )
				{
					DBGERR( "%s: Can't get dinamic %s modem USB port\n", __func__, netlibraryname );
					return DevPort;
				}

				if( !DevPort )
				{
					DBGERR( "%s: No dinamic %s modem USB port. Exit\n", __func__, netlibraryname );
					return -AXIS_ERROR_INVALID_VALUE;
				}
			}

			DBGLOG( "%s: Try shedule power off USB port %d\n", __func__, DevPort );

			StatusAdapter->USBPortNeadReset = DevPort;
			break;
		}

		case DM_MODEM_HAVE_INCOMING_SMS:
		{
			DBGLOG( "%s: %s modem have incomming SMS\n", __func__, netlibraryname );

			if( !data )
			{
				DBGERR( "%s: No data from incoming SMS call\n", __func__ );
				return -AXIS_ERROR_INVALID_VALUE;
			}

			sms_t *SMS = (sms_t *)data;

			ret = AdapterAddNewSMSToReciveQueue( StatusAdapter, SMS );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't add new SMS to received queue\n", __func__ );
			}

			return ret;
		}

		if(status->status == DM_NEED_HARD_RESET)
		{
			DBGLOG( "%s: Need hard reset %s modem on port %d\n", __func__, netlibraryname, CurrenNet->PollingModem.port + 1 );

			ret = AdapterPowerUSBReset( &StatusAdapter->GlobalDevice_t, CurrenNet->PollingModem.port + 1 );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't reset USB port %d\n", __func__, CurrenNet->PollingModem.port + 1 );
			}

			return ret;
		}

		default:
		{
			DBGERR( "%s: Invalid event %d\n", __func__, status->status );
			break;
		}
	}

	ret = AdapterNetworkGetConnectionStatus( CurrenNet );

	if( ret < 0 )
	{
		DBGERR( "%s: It wasn't possible to get connection status\n", __func__ );
		return ret;
	}

	//�������� ������� � ��������� - ����� ��������� ����� ������
	ret = AdapterInterfaceSendCommandAndValue( StatusAdapter, AXIS_INTERFACE_EVENT_NETWORK, status->status ==  DM_NET_CONNECTED ? 1 : 0 );

	if( ret < 0 )
	{
		DBGERR( "%s:%i: It wasn't possible to send event 'Network' to interface\n", __func__, __LINE__ );
	}

	return 0;

}

/* ATTENTION - �� �������� ���� ���, ��� �� ���� ����� �������� ��� CDMA �� ����� �������� ���������� ��� ���� ������� �� �������
int AdapterNetworkEventCDMA( connection_status_t *status, void *data )
{

	if( !status )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}
	DBGLOG( "%s:%i:Event From CDMA Lib %x\n", __func__, __LINE__,status->status );

	int ret;

	int i;
	AdapterGlobalStatusStruct_t *Statusg = AdapterGlobalGetStatus();
	int current = -1;
	sms_t sms;

	libnetcontrol_t* Net = 	 AdapterNetworkGetLibControlByConnectionType(Statusg,
								AXIS_SOFTADAPTER_CONNECT__ADAPTER_TYPE_MODEM);		//TODO - ��� �������� ���
	if(!Net)
	{
		DBGERR( "%s:It wasn't possible to find lib control\n", __func__ );
		return -1;
	}

	if(status->status == DM_NEED_HARD_RESET)
    {
        DBGLOG( "%s: Need reset cdma modem on port %d\n", __func__, Net->PollingModem.port + 1 );
        AdapterPowerUSBReset(&Statusg->GlobalDevice_t,Net->PollingModem.port + 1);
        return 0;
    }


	if( status->status == DM_MODEM_ENABLE_RESET )
	{
		AxisUsbDesc_t devices[ DEVICE_MAX_NUM ];
		int devcount =  0;
		int num = 0;
		int modnum;

		memset( devices, 0, sizeof( devices ) );

		devcount = axis_usb_enumerate_all_bus( devices, DEVICE_MAX_NUM );

		if( devcount < 0 )
		{
			DBGERR( "%s:It wasn't possible to check devices\n", __func__ );
			return -1;
		}
		if(devcount > 0)
		{
			if( AdapterNetworkDevicesCheckForInModem( &Statusg->GlobalDevice_t.dmdev_cdma, devices, devcount, &num ,&modnum) > 0 )
			{
				DBGLOG( "%s: %i CDMA In\n", __func__, __LINE__ );
				memcpy( &Net->PollingModem, &devices[num], sizeof( AxisUsbDesc_t ) );
				
						Net->PollingModem.connecttype = AXIS_SOFTADAPTER_CONNECT__ADAPTER_TYPE_MODEM;

				if( AdapterUsbInitPolling( &Net->PollingModem ) < 0 )
				{
					DBGERR( "%s:It wasn't possible to init polling\n", __func__ );
	// 			return -1;
				}
			}
		}
		else
		{
			DBGLOG( "%s: %i No modem stop connection %i %i %i \n",
							__func__, __LINE__,Net->PollingModem.port,Net->PollingModem.connecttype, Net->PollingModem.fdpoll);
			AdapterUsbDeinitPolling( &Net->PollingModem );
		}

		Net->status.imodemreset = 0;

		return 0;
	}

	uint64_t sn = strtoull( Statusg->CurrentNetwork.SN, NULL, 16 );

	if( status->status == DM_NET_CONNECTED )
	{
		DBGLOG( "%s: %i Event Connected\n", __func__, __LINE__ );

		ret = AdapterGlobalEventAddAndLogConnect( Statusg, "CDMA conncetion established",
										   status->ip,  status->level, status->band );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't add new event - connect \n", __func__ );
		}

		current = EventGetNew( &Statusg->ExchangeEvents.Events, DISPATCHER_LIB_EVENT__CONNECT );

		//TODO - ��� ���� ������� - ��� CDMA ������ ���, � �������� �������
		//���������� ������ ������ ����� ����� global_sms
		ret = AdapterEventPrintSMS( sms.text, sizeof( sms.text ),
									&Statusg->ExchangeEvents.Events.Events[ current ],
									( uint8_t* )&sn, sizeof( uint64_t ) );

		if( !ret )
		{
			DBGERR( "%s: Can't print event %d to send\n", __func__, current );
			return -AXIS_ERROR_INVALID_ANSED;
		}

		sms.text[ ret ] = 0x00;

		if( Net->net_do_command )
		{
			for( i = 0; i < AXIS_SOFTADAPTER_CDMA_PHONE_COUNT; i++ )
			{
				if( Statusg->AdapterSettings.Block1.CDMASettings.Phone[i] != 0x00 &&
				                DispatcherLibPhoneCheck( Statusg->AdapterSettings.Block1.CDMASettings.Phone[i] ) )
				{
					strncpy( sms.number, Statusg->AdapterSettings.Block1.CDMASettings.Phone[i],
					         sizeof( sms.number ) );

					if( Net->net_do_command( DM_NET_COMMAND_SEND_SMS, &sms ) < 0 )
					{
						DBGERR( "%s:It wasn't possible to sent sms\n", __func__ );
						return -1;
					}
				}
			}
		}

		//������ ������� ��� ����� � ����� �����
		Statusg->LastInterfaceQuire = time( NULL );

	}

	return 0;
}
 */


/**
 * @brief ������� ��������� ������ ��� �������� �� ������� ���������� CDMA
 *
 * @param status - ��������� �� ��������� ������� �������
 * @param data - ��������� �� ������ �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 */
int AdapterNetworkEventCDMA( connection_status_t *status, void *data )
{
	//TODO - ��� ��� ��������� CDMA ��� �� ������� - �� ��� �� ���� ������� �������� ���������������� - ����
	return adapter_net_library_event_callback( status, data, nettype_modem );
}

/**
 * @brief ������� ��������� ������ ��� ������� �� ������� ���������� Ethernet
 *
 * @param status - ��������� �� ��������� ������� �������
 * @param data - ��������� �� ������ �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 */
int AdapterNetworkEventEthernet( connection_status_t *status, void *data )
{
	return adapter_net_library_event_callback( status, data, nettype_eth );
}

/**
 * @brief ������� ��������� ������ ��� ������� �� ������� ���������� PPtP
 *
 * @param status - ��������� �� ��������� ������� �������
 * @param data - ��������� �� ������ �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 */
int AdapterNetworkEventPPtP( connection_status_t *status, void *data )
{
	return adapter_net_library_event_callback( status, data, nettype_pptp );
}

/**
 * @brief ������� ��������� ������ ��� ������� �� ������� ���������� PPoE
 *
 * @param status - ��������� �� ��������� ������� �������
 * @param data - ��������� �� ������ �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 */
int AdapterNetworkEventPPoE( connection_status_t *status, void *data )
{
	return adapter_net_library_event_callback( status, data, nettype_ppoe );
}

/**
 * @brief ������� ��������� ������ ��� ������� �� ������� ���������� WiFi
 *
 * @param status - ��������� �� ��������� ������� �������
 * @param data - ��������� �� ������ �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 */
int AdapterNetworkEventWiFi( connection_status_t *status, void *data )
{
	return adapter_net_library_event_callback( status, data, nettype_wifista );
}

/**
 * @brief ������ ��� lte ����������
 *
 * @param status - ��������� ����������
 * @param data - �������������� ������
 * @return int: 0 - OK, <0 - fail
 */
int AdapterNetworkEventLTE( connection_status_t *status, void *data )
{
	return adapter_net_library_event_callback( status, data, nettype_lte );
}

int AdapterNetworkLedOn( int lednum, const char* color )
{
	if( !color )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	AdapterGlobalStatusStruct_t *Status = AdapterGlobalGetStatus();

	//TODO - ��� ������ ������, ��� ��� ����� �������������� ����������� � ������� ����������� ����� �� ���������
	return AdapterLedOn( Status, lednum, color, __func__ );
}

int AdapterNetworkLedOff( int lednum, const char* color )
{
	if( !color )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	AdapterGlobalStatusStruct_t *Status = AdapterGlobalGetStatus();

	//TODO - ��� ������ ������, ��� ��� ����� �������������� ����������� � ������� ����������� ����� �� ���������
	return AdapterLedOff( Status, lednum, color, __func__ );
}

int AdapterNetworkLedBlink( int lednum, const char* color )
{
	if( !color )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	AdapterGlobalStatusStruct_t *Status = AdapterGlobalGetStatus();

	//TODO - ��� ������ ������, ��� ��� ����� �������������� ����������� � ������� ����������� ����� �� ���������
	return AdapterLedBlink( Status, lednum, color, __func__ );
}

int AdapterNetworkLedFastBlink( int lednum, const char* color )
{
	if( !color )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	AdapterGlobalStatusStruct_t *Status = AdapterGlobalGetStatus();

	//TODO - ��� ������ ������, ��� ��� ����� �������������� ����������� � ������� ����������� ����� �� ���������
	return AdapterLedBlinkFast( Status, lednum, color, __func__ );
}

int AdapterNetworkGetLedCDMANum( void )
{
	AdapterGlobalStatusStruct_t *Status = AdapterGlobalGetStatus();

	libnetcontrol_t* Net = AdapterNetworkGetLibControlByConnectionType( Status, nettype_modem );

	if( !Net )
	{
		return -1;
	}

	return Net->PollingModem.port + 2 ;
}

int AdapterNetworkGetLedWiFiNum( void )
{
	AdapterGlobalStatusStruct_t *Status = AdapterGlobalGetStatus();
	int ret;
	ret = AdapterUsbPortNumber(Status->GlobalDevice_t.dmdev_wifi.dmdev_device_name) ;
	
	DBGLOG("%s:%i:Ret = %i\n",__func__,__LINE__,ret);
	if(ret >=0)
	{
		ret+= 2;
	}
	else
	{
		AdapterLedNetworkConnectBlink( Status );
	}

	return ret ;
}

int AdapterNetworkGetLedEthernet( void )
{
	return 1 ;
}
//������� �������� ��������� ip ������ � ����������� ����
//����:
// ip - ���������� IP ������� ������ ���� ��������
// net - ��������� �� ������ ����������� �����
// mask - ��������� �� ������ ������������ �����
// count - ���������� ��������� � �������
//�������:
// 0 - IP �� ����� � ������ �����������
// 1 - IP ����� � ������ ��������
// 0 - � ������ ������
int AdapterNetworkChaeckIPForAllowNetwork(
        uint32_t ip,
        uint32_t *net,
        uint32_t *mask,
        size_t   Count )
{

	int i;

	if( !ip || !net || !mask || !Count )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return 0;
	}

	for( i = 0; i < Count; i++ )
	{
		DBGLOG( "%s: Try check %08x in %08x/%08x\n", __func__,
		        ip,
		        net[i],
		        mask[i] );

		if( ( ip & mask[i] ) == net[i] )
		{
			DBGLOG( "%s: Ip %08x is granted\n", __func__, ip );
			return 1;
		}

	}

	return 0;
}

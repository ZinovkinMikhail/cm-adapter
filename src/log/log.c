//������� ��� ������ � ������� �����������

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <adapter/log.h>
#include <adapter/global.h>

#include <dm_lib_log_control/command_resource.h>
#include <dm_lib_log_control/command_lib_exchange.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
//#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>

#else
#define DBGLOG( ... )
#endif


/**
 * @brief ������� ������������� �����������
 * 
 * @param Status - ��������� �� ���������� ��������� ��������
 * @return int: 0 - OK, <0 - fail
 */
int AdapterLogInit( AdapterGlobalStatusStruct_t *Status )
{
	if( !Status )
	{
		DBGERR("%s:%i: Invalid Argument\n", __func__, __LINE__ );
		return -1;
	}
	
	if(!Status->ilogging)
	{
		DBGLOG("%s:%i: Logger has not been initialized\n",	__func__, __LINE__ );
		return 0;
	}
	
	if( Status->LogRegistration.hen )
	{
		DBGERR( "%s: Logger already inited\n", __func__ );
		return -1;
	}
	
	
	strncpy( Status->LogRegistration.filename, LOG_ADAPTER_FILE_NAME,
				sizeof( Status->LogRegistration.filename ) );

	strncpy( Status->LogRegistration.clientname, LOG_ADAPTER_FILE_NAME,
				sizeof( Status->LogRegistration.clientname ) );
	
	strncpy( Status->LogRegistration.socketname, LOG_DAEMON_SOCKET,
				sizeof( Status->LogRegistration.socketname ) );

	Status->LogRegistration.flag = WRITE_HEADER;

	Status->LogRegistration.rotatesize = LOG_ROTATE_SIZE;

	Status->LogRegistration.maxfiles = 5;
	
	// � ���� ���-������ ��������?
	char start_test[64];

	strncpy( start_test, "START ADAPTER LOGGING\r\n\r\n\r\n", sizeof( start_test ) );
	
	if( CommandLibDoLogDaemon( &Status->LogRegistration, start_test,
						strlen( start_test ) ) < 0 )
	{
		DBGERR( "%s: It wasn't possible to register adapter to logger\n", __func__ );
		return -1;
	}
	
	return 0;
}

/**
 * @brief ������� ��������������� �����������
 * 
 * @param Status - ��������� �� ���������� ��������� ��������
 * @return int: 0 - OK, <0 - fail
 */
void AdapterLogDeInit( AdapterGlobalStatusStruct_t* Status )
{
	if( !Status )
	{
		DBGERR("%s:%i: Invalid Argument\n", __func__, __LINE__ );
		return;
	}
	
	if(!Status->ilogging)
	{
		//� ������ � �� ����������
		return;
	}
	
	//����������� � ����������
	CommandLibDeInitConnection( &Status->LogRegistration );
	
	memset( &Status->LogRegistration, 0 , sizeof( Status->LogRegistration ));
}


/**
 * @brief ������� ������ ��������������� ��������� ��� ������ � � �������
 * 
 * @param Status - ��������� �� ���������� ��������� ��������
 * @param res - ������ ��� ������: 1 = [Error], 0 - [Ok]
 * @param msg - ���������
 * @param  ...
 * @return void
 */
void AdapterLogInfo( AdapterGlobalStatusStruct_t* Status, int res,
							const char* msg, ... )
{
	if( !Status )
	{
		DBGERR( "%s:%i: Invalid Argument\n", __func__, __LINE__ );
		return ;
	}
	
	if(!Status->ilogging)
	{
		//� ������ � �� ����������
		return;
	}
	
	if( !msg )
	{
		DBGERR( "%s:%i: Bad message\n", __func__, __LINE__ );
		return;
	}

	if( !Status->LogRegistration.socketname[0] )
	{
		DBGERR( "%s: Loger not initing\n", __func__ );
		return;
	}

	char buff[256];
	char stringin[256];
	int sizeprint = 0;

	va_list ap;

	va_start( ap, msg );
	vsnprintf( buff, sizeof( buff ), msg, ap );
	va_end( ap );

	sizeprint = snprintf( stringin, sizeof( stringin ), "%-60s\t%s\r\n",
						buff, res ? "[Error]" : "[Ok]" );
	if( sizeprint < 0 || sizeprint >= sizeof( stringin ) )
	{
	 	sizeprint = sizeof( stringin ) - 1;
		stringin[ sizeof( stringin ) - 1 ] = 0x00;
	}
	
	if( CommandLibDoLogDaemon( &Status->LogRegistration,
						stringin, sizeprint ) < 0 )
	{
		DBGERR( "%s: It wasn't possible to send message to logger\n",
									__func__ );
		return;
	}
}

/**
 * @brief ������� ����������� �����
 * 
 * @param Status - ��������� �� ���������� ��������� ��������
 * @param msg - ���������
 * @param  ...
 * @return void
 */
void AdapterLogLog( AdapterGlobalStatusStruct_t *Status, const char* msg, ... )
{
	if( !Status )
	{
		DBGERR( "%s:%i: Invalid Argument\n", __func__, __LINE__ );
		return;
	}
	if(!Status->ilogging)
	{
		//� ������ � �� ����������
		return;
	}
	if( !msg )
	{
		DBGERR( "%s:%i: Bad message\n", __func__, __LINE__ );
		return;
	}

	char buff[65536];
	char stringin[65536];
	uint32_t sizeprint;
// 	struct struct_data sd;
// 
// 	sd = system_time_get();

	va_list ap;

	va_start( ap, msg );
	vsnprintf( buff, sizeof( buff ), msg, ap );

	va_end( ap );

	sizeprint = snprintf( stringin, sizeof( stringin ), "%s", buff );

//TODO - ����������� �������� �� ����� �� ������� ������
	if( CommandLibDoLogDaemon( &Status->LogRegistration, stringin,
							sizeprint ) < 0 )
	{
		DBGERR( "%s:%i: It wasn't possible to register adapter logger\n",
							__func__, __LINE__ );
		return;
	}
}

/**
 * @brief ������� ����������� ������� ���������
 * 
 * @param Status - ��������� �� ���������� ��������� ��������
 * @param msg - ���������
 * @param size - ������ ������
 * @return void
 */
void AdapterLogBuff( AdapterGlobalStatusStruct_t *Status, const char* msg,
								size_t size )
{
	if( !Status || !msg )
	{
		DBGERR( "%s:%i: Invalid Argument\n", __func__, __LINE__ );
		return;
	}
	
	if(!Status->ilogging)
	{
		//� ������ � �� ����������
		return;
	}

	if( !size )
	{
		return; //��� ������
	}

	//���������� �����������, ������� 2 �������� �������� � ����
	if( CommandLibDoLogDaemon( &Status->LogRegistration, (char *) msg,
								size ) < 0 )
	{
		DBGERR( "%s:It wasn't possible to register adapter to logger\n",
									__func__ );
		return;
	}
}

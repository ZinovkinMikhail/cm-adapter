
#include <axis_pid.h>
#include <axis_httpi_tags.h>

#include <dispatcher_lib/dispatcher_lib_resource.h>
#include <dispatcher_lib/command.h>

#include <adapter/new_command.h>
#include <adapter/httpd.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
//#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif


//������� ������� ���������� ���� - ������� ������� ��������� � ���
//����
// Requests - ��������� �� ������ �������� � ������� ����� ������
// RCount - ��������� �� ���������� ��������� � �������
//	����� �������� � �������� ������ � �������� �������� ����� ������ ��������
// command_mask - ����� ����������� ������
// command - ��������� �� ��������� �� ������ � ������� ������������ ��� �������
// NewDate - ��������� �� ��������� ���� � ������� ��������� ����
//	������� ������ ���������� � ���������� ��� ��� �� ����������
//	����� ������� ������� ������ ��������� ���������� �������� ��� ����������� ��������
// Channels - ��������� �� ������ ����� ������������ ����� � �������
//  � ���� ���������� ��������� � ������� ������
//  ����� ������� ������� ������ ������ ���� ���������
// Status - ��������� �� ���������� ��������� �������
//�������
// 0 - ������ �� ���������
// > 0 - ��� �������� ������� ��� �������� (�� command.h)
// < 0 - ������ ���������� ���� ���� (�� command.h)
int AdapterInterfaceDoCommand( 
				axis_httpi_request *Requests,
				size_t *RCount, 
			    unsigned int command_mask, 
				char **command,
				struct_data *NewData,
				int *Channels,
				size_t CCount,
				AdapterGlobalStatusStruct_t *Status ,
			       DspRecorderExchangeTimerSettingsStruct_t 	*TimersSettingsCache)
{

    //�������� �� �������
    if( !Requests || !RCount || *RCount <= 0 || !Status )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return ADAPTER_COMMAND__RETURN_ERROR;
	}
    
	DBGLOG( DISPATCHER_LIB_DEBUG__START );

	int error_flag = 0;
	int warning_flag = 0;
	 int save_flag = 0;
	 
	DBGLOG("%s: Input count is %zu\n",__func__, *RCount);
	DBGLOG("%s: CommandMask = %08x\n",__func__, command_mask );
	
    //���� �� �������� ���������� �������� �������
	save_flag = DispatcherLibInterfaceGetCommand( 
						Requests,
						*RCount,
						command_mask,
						command );
	
	if( save_flag & ADAPTER_INTERFACE_COMMAND__NETSAVE &&( Status->AdapterDevice == DM355_DEVICE_HARDWARE_TYPE__R2 ||
			 Status->AdapterDevice == DM355_DEVICE_HARDWARE_TYPE__220))
	{
		 if( !Status->RescueMode )
		 {
		   //���� ����� �� ������
		//   return ADAPTER_COMMAND__RETURN_ERROR;
		  }
		
    }
    
    //�������� �������
    int ret = DispatcherLibInterfaceDoCommandWrapper( 
						Requests,		//��������
						RCount,			//�� ����������
						command_mask,	//����� ������
						command,		//������� ����� �������
						NewData,		//������� ��������� ����
						Status->AdapterDevice, //��� ������� ����������
						1/*Status->PowerOn*/,	//���� �� ��������� �� ������
						&Status->RecorderSettings.Settings, //��������� ����������
						TimersSettingsCache, //��������� ��������
						&Status->AdapterSettings, //�������� ��������
						AdapterHttpdGetClientIP( axis_getpid(), Status,NULL ),//IP ����� �������
						Channels,		//������ � �������
						CCount,			//�� ����������
						&error_flag,	//���� �� ������
						&warning_flag	//���� �� ��������������
						
				);

	DBGLOG("%s: error_flag=%d warning_flag=%d\n",__func__,error_flag,warning_flag);
	DBGLOG("%s: Output Count is %zu\n", __func__, *RCount);


	DBGLOG("%s: After command ret = %08x\n",__func__, ret );


    //���� ��� ���� �������� �������� - � �� ������ ������ �������
    //�� ���� ���� ������ �� � ���
    if( !ret )
		return ret;


#ifdef NOT_IN_USE_NOW
    //������� ����������� ��� ����������� ��������
    char *agent		= axis_httpi_get_caserequest_value( 
							Requests, *RCount, 
							AXIS_HTTPI_CONTENT_USER_AGENT );
    char *user 		= axis_httpi_get_caserequest_value( 
							Requests, *RCount, 
							AXIS_HTTPI_CONTENT_USER_TYPE );
    char *clientID	= axis_httpi_get_caserequest_value( 
							Requests, *RCount, 
							AXIS_HTTPI_CONTENT_CLIENT_ID );
#endif
    //	NOT_IN_USE_NOW
// 	int response	= DISPATCHER_LIB_INTERFACE_RESPONSE_OK;
// 
// 	switch( ret & ADAPTER_COMMAND__RETURN )
// 	{
// 		case ADAPTER_COMMAND__RETURN_ERROR:
// 		{
// 			
// 			response = DISPATCHER_LIB_INTERFACE_RESPONSE_ERROR;
// 			DBGLOG("%s: return Error value\n",__func__);
// 			break;
// 		}
// 		case ADAPTER_COMMAND__RETURN_WARNING:
// 		{
// 			response = DISPATCHER_LIB_INTERFACE_RESPONSE_WARNING;
// 			DBGLOG("%s: return Warning value\n",__func__);
// 			break;
// 		}
// 		case ADAPTER_COMMAND__RETURN_NODEVICE:
// 		{
// 			response = DISPATCHER_LIB_INTERFACE_RESPONSE_NODEVICE;
// 			DBGLOG("%s: return NoDevice value\n",__func__);
// 			break;
// 		}
// 		default:
// 		{
// 			DBGLOG("%s: return Unknown value %08x\n",__func__, ret & ADAPTER_COMMAND__RETURN );
// 		}
// 	}
	
    char *cmd = NULL;
    
    if( command )
		cmd = *command;

	DBGLOG("%s: Command return %08x '%s' try create command log\n", __func__ ,ret, cmd);

/*
    //���� ��� �� � ������ �����
    AdapterLogCreateCommandLog( 
						agent,
						user,
						clientID,
						cmd,
						response );
 
    //� ������� ���� ���� ��� � ������ ��� �� ��������� � ������������
    //��������������� ��������
    if( ret & ADAPTER_INTERFACE_COMMAND__SAVE ||
		ret & ADAPTER_INTERFACE_COMMAND__SAVE2 )
	{
		AdapterLogCreateRecorderSaveLog();
    }
    if( ret & ADAPTER_INTERFACE_COMMAND__APPLY )
	{
		AdapterLogCreateRecorderAplayLog();
    }
    if( ret & ADAPTER_INTERFACE_COMMAND__NETSAVE || 
		ret & ADAPTER_INTERFACE_COMMAND__NETSAVE2 ){
		AdapterLogCreateAdapterSaveLog();
    }
 */
	DBGLOG( DISPATCHER_LIB_DEBUG__DONE );

    return ret;					     
}

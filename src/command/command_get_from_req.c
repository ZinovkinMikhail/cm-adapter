#include <axis_pid.h>
#include <axis_httpi_tags.h>

#include <dispatcher_lib/dispatcher_lib_resource.h>
#include <dispatcher_lib/command.h>

#include <adapter/new_command.h>
#include <adapter/httpd.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
//#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif

int AdapterInterfaceGetCommand(axis_httpi_request *Requests,size_t *RCount, unsigned int command_mask,
				char **command,
				AdapterGlobalStatusStruct_t *Status )
{
	   //�������� �� �������
	if( !Requests || !RCount || *RCount <= 0 || !Status )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return ADAPTER_COMMAND__RETURN_ERROR;
	}
	
	return DispatcherLibInterfaceGetCommand(
						Requests,
						*RCount,
						command_mask,
						command );
}



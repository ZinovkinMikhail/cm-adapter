#include <axis_pid.h>
#include <axis_httpi_tags.h>
#include <axis_error.h>

#include <dispatcher_lib/dispatcher_lib_resource.h>
#include <dispatcher_lib/command.h>
#include <dispatcher_lib/sprintf_ini_files.h>
#include <dispatcher_lib/debug.h>

#include <adapter/new_command.h>
#include <adapter/httpd.h>
#include <adapter/recorder.h>
#include <adapter/settings.h>
#include <adapter/sleep.h>
#include <adapter/interface.h>
#include <adapter/debug.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>

#else
#define DBGLOG( ... )
#endif

//��� �����������
#define INFO_IP(a,b,c)		{char ips[32]; \
								axis_sprint_ip(ips,b);\
								INFO_LOG(a,"Command \"%s\" from client %s ",c,ips);\
						}


/**
 * @brief ������� ���������� ���������� �������� ����� ��� ������� - �� ������
 *
 * @param flag - �������� �����
 * @return const char* - ���������� �������� �����
**/
static const char *AdapterRequestFlagToStringRu( unsigned int flag )
{
	switch( flag )
	{
		case AXIS_HTTPI_REQUEST_FLAG__SYSTEM_REQUEST:
			return "���������";
		case AXIS_HTTPI_REQUEST_FLAG__CLEARED_REQUEST:
			return "������";
		case AXIS_HTTPI_REQUEST_FLAG__ERRORED_REQUEST:
			return "���������";
		case AXIS_HTTPI_REQUEST_FLAG__APPLAYED_REQUEST:
			return "������";
		default:
			return "��������";
	}
}

/**
 * @brief �������, ������� ���������� ������ �������� � ���� �������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @param Requests - ��������� �� ������ ��������
 * @param RCount - ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
static int AdapterRequestsLog( AdapterGlobalStatusStruct_t *Status, axis_httpi_request *Requests, size_t RCount )
{
	char buffer[ 4096 ];
	int ret = 0;
	int i;
	char *client;

	size_t cc = 0;

	size_t size = sizeof( buffer );

	if( !Requests || !RCount )
	{
		DBGERR( "%s: Invalid argument\n", __func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	client = axis_httpi_get_caserequest_value( Requests, RCount, AXIS_HTTPI_CONTENT_USER_TYPE );

	ret = snprintf( buffer+cc, size, "\r\n������� �� ������� '%s':\r\n", client );

	if( ret < 0 || ret >= size )
	{
		buffer[ sizeof( buffer ) - 1 ] = 0x00;
		DBGERR( "%s: End of buffer detected on print header\n", __func__ );
		return 0;
	}

	cc += ret;
	size -= ret;

	for( i = 0; i < RCount; i++ )
	{
		if( Requests[i].flag == AXIS_HTTPI_REQUEST_FLAG__SYSTEM_REQUEST )
			continue; //��������� ������� ����������

		if( Requests[i].name[0] == '[' )
			ret = snprintf( buffer+cc, size, "%s\r\n", Requests[i].name );
		else
			ret = snprintf( buffer+cc, size, "    %s = %s (%s)\r\n",
						Requests[i].name, Requests[i].value, AdapterRequestFlagToStringRu( Requests[i].flag ) );

		if( ret < 0 || ret >= size )
		{
			DBGERR( "%s: End of buffer detected at %i request\n", __func__, i );
			buffer[ sizeof( buffer ) - 1 ] = 0x00;
			break;
		}

		cc += ret;
		size -= ret;
	}

	DBGLOG( "%s: Try save LOG %zu bytes '%s'\n", __func__, cc, buffer );
#ifdef CONFIG_ENCODER_DEBUG_MSG
	DispatcherLibDebugPrintBuffer( (unsigned short *)buffer, cc >> 1 );
#endif
	
	AdapterLogBuff( Status, buffer, cc );

	return 0;
}

static int AdapterCommandWorkOnMask( int command_mask, AdapterGlobalStatusStruct_t *Status, struct Recorder_s **recorder, int *answer_flag )
{
	if( !Status || !answer_flag )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	int ret;

	*answer_flag &= ~ADAPTER_COMMAND__RETURN;
	*answer_flag |= ADAPTER_COMMAND__RETURN_OK;

	if( command_mask == ADAPTER_INTERFACE_COMMAND__NETSAVE )
	{
		ret = AdapterSettingsLoadAdapterSettings( Status );

		if( ret == -AXIS_ERROR_TIMEOUT_EVENT )
		{
			DBGLOG( "%s:  timedout equalent - 404 Error\n", __func__ );
			*answer_flag &= ~ADAPTER_COMMAND__RETURN;
			*answer_flag |= ADAPTER_COMMAND__RETURN_WARNING;
		}
		else	if( ret ==  -AXIS_ERROR_BUSY_DEVICE )
		{
			DBGERR( "%s: Can't Load Settings \n", __func__ );
			*answer_flag &= ~ADAPTER_COMMAND__RETURN;
			*answer_flag |= ADAPTER_COMMAND__RETURN_NODEVICE;
		}
		else if( ret < 0 )
		{
			DBGERR( "%s: Can't load adapter settings \n", __func__ );
			*answer_flag &= ~ADAPTER_COMMAND__RETURN;
			*answer_flag |= ADAPTER_COMMAND__RETURN_ERROR;
		}

		ret = AdapterSettingsLoadAdapterTimers( Status );

		if( ret == -AXIS_ERROR_TIMEOUT_EVENT )
		{
			DBGLOG( "%s: C5515 is timedout equalent - 404 Error\n", __func__ );
			*answer_flag &= ~ADAPTER_COMMAND__RETURN;
			*answer_flag |= ADAPTER_COMMAND__RETURN_WARNING;
		}
		else	if( ret ==  -AXIS_ERROR_BUSY_DEVICE )
		{
			DBGERR( "%s: Can't LOAD SETS \n", __func__ );
			*answer_flag &= ~ADAPTER_COMMAND__RETURN;
			*answer_flag |= ADAPTER_COMMAND__RETURN_NODEVICE;
		}
		else if( ret < 0 )
		{
			DBGERR( "%s: Can't load adapter settings \n", __func__ );
			*answer_flag &= ~ADAPTER_COMMAND__RETURN;
			*answer_flag |= ADAPTER_COMMAND__RETURN_ERROR;
		}
	}
	else if( command_mask == ( ADAPTER_INTERFACE_COMMAND__APPLY | ADAPTER_INTERFACE_COMMAND__SAVE ) )
	{

		ret = AdapterRecorderChacheRecorderSettings( &Status->GlobalDevice_t, *recorder );

		if( ret == -AXIS_ERROR_TIMEOUT_EVENT )
		{
			DBGLOG( "%s: C5515 is timedout equalent - 404 Error\n", __func__ );
			*answer_flag &= ~ADAPTER_COMMAND__RETURN;
			*answer_flag |= ADAPTER_COMMAND__RETURN_WARNING;
		}
		else	if( ret ==  -AXIS_ERROR_BUSY_DEVICE )
		{
			DBGERR( "%s: Can't LOAD SETS \n", __func__ );
			*answer_flag &= ~ADAPTER_COMMAND__RETURN;
			*answer_flag |= ADAPTER_COMMAND__RETURN_NODEVICE;
		}
		else if( ret < 0 )
		{
			DBGERR( "%s: Can't load adapter settings \n", __func__ );
			*answer_flag &= ~ADAPTER_COMMAND__RETURN;
			*answer_flag |= ADAPTER_COMMAND__RETURN_ERROR;
		}

	}
	else
	{
		DBGLOG( "%s:%i:Unknown Mask\n", __func__, __LINE__ );
	}

	return 0;

}


int AdapterCommandWork( int command, axis_httpi_request *Requests, size_t *RCount, AdapterGlobalStatusStruct_t *Status,
                        struct Recorder_s **recorder, int *answer_flag,  axis_ssl_wrapper_ex_sock *sock )
{
	if( !Requests || !RCount || RCount <= 0 || !Status || !answer_flag )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	int ret = 0;

	unsigned int clientip = AdapterHttpdGetClientIP( axis_getpid(), Status, NULL );

	//���������� �������� ���������

	if( command & ADAPTER_COMMAND__RETURN_SAVE )
	{
		DBGLOG( "%s:%i: Save Recorder Settings\n", __func__, __LINE__ );

		if( !( *recorder ) )
		{
			*answer_flag &= ~ADAPTER_COMMAND__RETURN;
			*answer_flag |= ADAPTER_COMMAND__RETURN_NODEVICE;
		}
		else
		{
			( *recorder )->recorder_data.private_data = &clientip;

			ret = AdapterRecorderSaveFromRequest( Requests, RCount, Status, *recorder );

			if( ret < 0 )
			{
				*answer_flag &= ~ADAPTER_COMMAND__RETURN;
				*answer_flag |= ADAPTER_COMMAND__RETURN_ERROR;
			}
			
			INFO_IP( ret, clientip, "Save Recorder Settings" );
			AdapterRequestsLog( Status, Requests, *RCount );

			if( AdapterRecorderGetStatus( &Status->GlobalDevice_t, *recorder ) )
			{
				DBGERR( "%s:%i: It wasn't possible to get status for cache\n", __func__, __LINE__ );
				return -1;
			}

			if( AdapterInterfaceSendCommand( Status, AXIS_INTERFACE_EVENT_SAVE_MODE ) < 0 )
			{
				DBGERR( "%s:%i: It wasn't possible to send event 'Save Mode' to interface\n", __func__, __LINE__ );
			}
		}


	}

	//���������� �������� ���������
	if( command & ADAPTER_COMMAND__RETURN_APPLY )
	{
		DBGLOG( "%s:%i: Command apply for recorder\n", __func__, __LINE__ );

		if( !( *recorder ) )
		{
			*answer_flag &= ~ADAPTER_COMMAND__RETURN;
			*answer_flag |= ADAPTER_COMMAND__RETURN_NODEVICE;
		}
		else
		{
			( *recorder )->recorder_data.private_data = &clientip;

			ret = AdapterRecorderApplyFromRequest( Requests, RCount, Status, *recorder );

			if( ret < 0 )
			{
				*answer_flag &= ~ADAPTER_COMMAND__RETURN;
				*answer_flag |= ADAPTER_COMMAND__RETURN_ERROR;
			}
			
			INFO_IP( ret, clientip, "Apply Recorder Settings" );
			AdapterRequestsLog( Status, Requests, *RCount );

			if( AdapterRecorderGetStatus( &Status->GlobalDevice_t, *recorder ) )
			{
				DBGERR( "%s:%i: It wasn't possible to get status for cache\n", __func__, __LINE__ );
				return -1;
			}
		}
	}

	//������ ������
	if( command & ADAPTER_COMMAND__RETURN_START_RECORD )
	{
		DBGLOG( "%s:%i: Command start record\n", __func__, __LINE__ );

		if( !recorder )
		{
			*answer_flag &= ~ADAPTER_COMMAND__RETURN;
			*answer_flag |= ADAPTER_COMMAND__RETURN_NODEVICE;
		}
		else
		{
			ret = AdapterRecorderStartRecord( Requests, RCount, Status, *recorder );

			if( ret < 0 )
			{
				*answer_flag &= ~ADAPTER_COMMAND__RETURN;
				*answer_flag |= ADAPTER_COMMAND__RETURN_ERROR;
			}
			
			INFO_IP( ret, clientip, "Start record" );
			AdapterRequestsLog( Status, Requests, *RCount );

			if( AdapterRecorderGetStatus( &Status->GlobalDevice_t, *recorder ) )
			{
				DBGERR( "%s:%i: It wasn't possible to get status for cache\n", __func__, __LINE__ );
				return -1;
			}

			if( AdapterInterfaceSendCommand( Status, AXIS_INTERFACE_EVENT_READ_STATUS ) < 0 )
			{
				DBGERR( "%s:%i: It wasn't possible to send event 'Read Status' to interface\n", __func__, __LINE__ );
			}
		}
	}

	if( command & ADAPTER_COMMAND__RETURN_CONNECTRF )
	{
		DBGLOG( "%s:%i: Command connect recorder\n", __func__, __LINE__ );

		if( !recorder )
		{
			*answer_flag &= ~ADAPTER_COMMAND__RETURN;
			*answer_flag |= ADAPTER_COMMAND__RETURN_NODEVICE;
		}
		else
		{
			ret = AdapterRecorderConnect( Requests, RCount, Status, *recorder );

			if( ret < 0 )
			{
				*answer_flag &= ~ADAPTER_COMMAND__RETURN;
				*answer_flag |= ADAPTER_COMMAND__RETURN_ERROR;
			}

			INFO_IP( ret, clientip, "Connect RF" );

			if( AdapterRecorderGetStatus( &Status->GlobalDevice_t, *recorder ) )
			{
				DBGERR( "%s:%i: It wasn't possible to get status for cache\n", __func__, __LINE__ );
				return -1;
			}
		}
	}

	if( command & ADAPTER_COMMAND__RETURN_DISCONNECTRF )
	{
		DBGLOG( "%s:%i: Command disconnect\n", __func__, __LINE__ );

		if( !recorder )
		{
			*answer_flag &= ~ADAPTER_COMMAND__RETURN;
			*answer_flag |= ADAPTER_COMMAND__RETURN_NODEVICE;
		}
		else
		{
			ret = AdapterRecorderDisconnect( Requests, RCount, Status, *recorder );

			if( ret < 0 )
			{
				*answer_flag &= ~ADAPTER_COMMAND__RETURN;
				*answer_flag |= ADAPTER_COMMAND__RETURN_ERROR;
			}

			INFO_IP( ret, clientip, "Disconnect RF" );

			if( AdapterRecorderGetStatus( &Status->GlobalDevice_t, *recorder ) )
			{
				DBGERR( "%s:%i: It wasn't possible to get status for cache\n", __func__, __LINE__ );
				return -1;
			}
		}
	}
	
	if ( command &  ADAPTER_INTERFACE_COMMAND__RESETMICALARM )
	{
		DBGLOG( "%s:%i: Command reset mic alarm\n", __func__, __LINE__ );

		if ( !recorder )
		{
			*answer_flag &= ~ADAPTER_COMMAND__RETURN;
			*answer_flag |= ADAPTER_COMMAND__RETURN_NODEVICE;
		}
		else
		{
			ret = AdapterRecorderResetMicState( Requests, RCount, Status, *recorder );

			if ( ret < 0 )
			{
				*answer_flag &= ~ADAPTER_COMMAND__RETURN;
				*answer_flag |= ADAPTER_COMMAND__RETURN_ERROR;
			}

			INFO_IP( ret, clientip, "Reset MIC Alarm" );

			if ( AdapterRecorderGetStatus( &Status->GlobalDevice_t, *recorder ) )
			{
				DBGERR( "%s:%i: It wasn't possible to get status for cache\n", __func__, __LINE__ );
				return -1;
			}
		}
	}
	
	// Nand test write
	if (command & ADAPTER_INTERFACE_COMMAND__NAND_TEST_WRITE)
	{
		DBGLOG( "%s:%i: Command nand test write\n", __func__, __LINE__ );
		
		if ( !recorder )
		{
			*answer_flag &= ~ADAPTER_COMMAND__RETURN;
			*answer_flag |= ADAPTER_COMMAND__RETURN_NODEVICE;
		}
		else
		{
			ret = AdapterRecorderNandTestWrite( Requests, RCount, Status, *recorder );

			if ( ret < 0 )
			{
				*answer_flag &= ~ADAPTER_COMMAND__RETURN;
				*answer_flag |= ADAPTER_COMMAND__RETURN_ERROR;
			}

			INFO_IP( ret, clientip, "NAND Test Write" );

			if ( AdapterRecorderGetStatus( &Status->GlobalDevice_t, *recorder ) )
			{
				DBGERR( "%s:%i: It wasn't possible to get status for cache\n", __func__, __LINE__ );
				return -1;
			}
		}
	}
	
	// Nand test read
	if (command & ADAPTER_INTERFACE_COMMAND__NAND_TEST_READ)
	{
		DBGLOG( "%s:%i: Command nand test read\n", __func__, __LINE__ );
		
		if ( !recorder )
		{
			*answer_flag &= ~ADAPTER_COMMAND__RETURN;
			*answer_flag |= ADAPTER_COMMAND__RETURN_NODEVICE;
		}
		else
		{
			ret = AdapterRecorderNandTestRead( Requests, RCount, Status, *recorder );

			if ( ret < 0 )
			{
				*answer_flag &= ~ADAPTER_COMMAND__RETURN;
				*answer_flag |= ADAPTER_COMMAND__RETURN_ERROR;
			}

			INFO_IP( ret, clientip, "NAND Test Read" );

			if ( AdapterRecorderGetStatus( &Status->GlobalDevice_t, *recorder ) )
			{
				DBGERR( "%s:%u: It wasn't possible to get status for cache\n", __func__, __LINE__ );
				return -1;
			}
		}
	}
	
	// Nand test status
	if (command & ADAPTER_INTERFACE_COMMAND__NAND_TEST_GET_LOG)
	{
		DBGLOG( "%s:%i: Command nand test get log\n", __func__, __LINE__ );
		
		if ( !recorder )
		{
			*answer_flag &= ~ADAPTER_COMMAND__RETURN;
			*answer_flag |= ADAPTER_COMMAND__RETURN_NODEVICE;
		}
		else
		{
			ret = AdapterRecorderNandTestStatus( Requests, RCount, Status, *recorder );

			if ( ret < 0 )
			{
				*answer_flag &= ~ADAPTER_COMMAND__RETURN;
				*answer_flag |= ADAPTER_COMMAND__RETURN_ERROR;
			}

			INFO_IP( ret, clientip, "NAND Test Get Log" );

			if ( AdapterRecorderGetStatus( &Status->GlobalDevice_t, *recorder ) )
			{
				DBGERR( "%s:%i: It wasn't possible to get status for cache\n", __func__, __LINE__ );
				return -1;
			}
		}
		
	}
	
	// Nand test stop
	if (command & ADAPTER_INTERFACE_COMMAND__NAND_ABORT_TEST)
	{
		DBGLOG( "%s:%i: Command nand test abort\n", __func__, __LINE__ );
		
		if ( !recorder )
		{
			*answer_flag &= ~ADAPTER_COMMAND__RETURN;
			*answer_flag |= ADAPTER_COMMAND__RETURN_NODEVICE;
		}
		else
		{
			ret = AdapterRecorderNandTestStop( Requests, RCount, Status, *recorder );

			if ( ret < 0 )
			{
				*answer_flag &= ~ADAPTER_COMMAND__RETURN;
				*answer_flag |= ADAPTER_COMMAND__RETURN_ERROR;
			}

			INFO_IP( ret, clientip, "NAND Test Abort" );

			if ( AdapterRecorderGetStatus( &Status->GlobalDevice_t, *recorder ) )
			{
				DBGERR( "%s:%i: It wasn't possible to get status for cache\n", __func__, __LINE__ );
				return -1;
			}
		}
	}

	//���� ������
	if( command & ADAPTER_COMMAND__RETURN_STOP_RECORD )
	{
		DBGLOG( "%s:%i: Command stop record\n", __func__, __LINE__ );

		if( !recorder )
		{
			*answer_flag &= ~ADAPTER_COMMAND__RETURN;
			*answer_flag |= ADAPTER_COMMAND__RETURN_NODEVICE;
		}
		else
		{
			ret = AdapterRecorderStopRecord( Requests, RCount, Status, *recorder );

			if( ret < 0 )
			{
				*answer_flag &= ~ADAPTER_COMMAND__RETURN;
				*answer_flag |= ADAPTER_COMMAND__RETURN_ERROR;
			}
			
			INFO_IP( ret, clientip, "Stop Record" );
			AdapterRequestsLog( Status, Requests, *RCount );

			if( AdapterRecorderGetStatus( &Status->GlobalDevice_t, *recorder ) )
			{
				DBGERR( "%s:%i: It wasn't possible to get status for cache\n", __func__, __LINE__ );
				return -1;
			}

			if( AdapterInterfaceSendCommand( Status, AXIS_INTERFACE_EVENT_READ_STATUS ) < 0 )
			{
				DBGERR( "%s:%i: It wasn't possible to send event 'Read Status' to interface\n", __func__, __LINE__ );
			}
		}
	}

	//������ �����������
	if( command & ADAPTER_COMMAND__RETURN_START_MONITORING )
	{
		DBGLOG( "%s:%i: Command start monitoring\n", __func__, __LINE__ );

		if( !recorder )
		{
			*answer_flag &= ~ADAPTER_COMMAND__RETURN;
			*answer_flag |= ADAPTER_COMMAND__RETURN_NODEVICE;
		}
		else
		{
			ret = AdapterRecorderStartMonitoring( Requests, RCount, Status, *recorder );

			if( ret < 0 )
			{
				*answer_flag &= ~ADAPTER_COMMAND__RETURN;
				*answer_flag |= ADAPTER_COMMAND__RETURN_NODEVICE;
			}
			
			INFO_IP( ret, clientip, "Start Monitoring" );
			AdapterRequestsLog( Status, Requests, *RCount );

			if( AdapterRecorderGetStatus( &Status->GlobalDevice_t, *recorder ) )
			{
				DBGERR( "%s:%i: It wasn't possible to get status for cache\n", __func__, __LINE__ );
				return -1;
			}
		}
	}

	//���� �����������
	if( command & ADAPTER_COMMAND__RETURN_STOP_MONITORING )
	{
		DBGLOG( "%s:%i: Command stop monitorig\n", __func__, __LINE__ );

		if( !recorder )
		{
			*answer_flag &= ~ADAPTER_COMMAND__RETURN;
			*answer_flag |= ADAPTER_COMMAND__RETURN_NODEVICE;
		}
		else
		{
			ret = AdapterRecorderStopMonitoring( Requests, RCount, Status, *recorder, sock->ip );

			if( ret < 0 )
			{
				*answer_flag &= ~ADAPTER_COMMAND__RETURN;
				*answer_flag |= ADAPTER_COMMAND__RETURN_NODEVICE;
			}
			
			INFO_IP( ret, clientip, "Stop Monitoring" );
			AdapterRequestsLog( Status, Requests, *RCount );

			if( AdapterRecorderGetStatus( &Status->GlobalDevice_t, *recorder ) )
			{
				DBGERR( "%s:%i: It wasn't possible to get status for cache\n", __func__, __LINE__ );
				return -1;
			}
		}
	}

	//������ ��������
	if( command & ADAPTER_COMMAND__RETURN_START_ERASE )
	{
		DBGLOG( "%s:%i: Command start erase\n", __func__, __LINE__ );

		if( !recorder )
		{
			*answer_flag &= ~ADAPTER_COMMAND__RETURN;
			*answer_flag |= ADAPTER_COMMAND__RETURN_NODEVICE;
		}
		else
		{
			ret = AdapterRecorderStartErase( Requests, RCount, Status, *recorder );

			if( ret < 0 )
			{
				*answer_flag &= ~ADAPTER_COMMAND__RETURN;
				*answer_flag |= ADAPTER_COMMAND__RETURN_NODEVICE;
			}
			
			INFO_IP( ret, clientip, "Start Erase" );
			AdapterRequestsLog( Status, Requests, *RCount );

			if( AdapterRecorderGetStatus( &Status->GlobalDevice_t, *recorder ) )
			{
				DBGERR( "%s:%i: It wasn't possible to get status for cache\n", __func__, __LINE__ );
				return -1;
			}
		}
	}
	//������ ��������
	if( command & ADAPTER_COMMAND__RETURN_WAKE_UP)
	{
		DBGLOG( "%s:%i: Command wake up\n", __func__, __LINE__ );

		if( !recorder )
		{
			*answer_flag &= ~ADAPTER_COMMAND__RETURN;
			*answer_flag |= ADAPTER_COMMAND__RETURN_NODEVICE;
		}
		else
		{
			ret = AdapterRecorderWakeUp( Requests, RCount, Status, *recorder );

			if( ret < 0 )
			{
				*answer_flag &= ~ADAPTER_COMMAND__RETURN;
				*answer_flag |= ADAPTER_COMMAND__RETURN_NODEVICE;
			}

			INFO_IP( ret, clientip, "Wake Up" );
		}
	}
	//������� ����, ������� ���������� ��� ���
	if( command & ADAPTER_INTERFACE_COMMAND__PING_ADAPTER)
	{
		DBGLOG( "%s:%i: Command ping \n", __func__, __LINE__ );

		if( !recorder )
		{
			*answer_flag &= ~ADAPTER_COMMAND__RETURN;
			*answer_flag |= ADAPTER_COMMAND__RETURN_NODEVICE;
		}
		else
		{
			ret = AdapterRecorderPing( Requests, RCount, Status, *recorder );

			if( ret < 0 )
			{
				*answer_flag &= ~ADAPTER_COMMAND__RETURN;
				*answer_flag |= ADAPTER_COMMAND__RETURN_NODEVICE;
			}

			INFO_IP( ret, clientip, "Ping" );
		}
	}

	if( command & ADAPTER_COMMAND__RETURN_CLEAR_LOG)
	{
		DBGLOG( "%s:%i: Command Clear Log\n", __func__, __LINE__ );

		if( !recorder )
		{
			*answer_flag &= ~ADAPTER_COMMAND__RETURN;
			*answer_flag |= ADAPTER_COMMAND__RETURN_NODEVICE;
		}
		else
		{
			ret = AdapterRecorderClearLog( Requests, RCount, Status, *recorder );

			if( ret < 0 )
			{
				*answer_flag &= ~ADAPTER_COMMAND__RETURN;
				*answer_flag |= ADAPTER_COMMAND__RETURN_NODEVICE;
			}

			INFO_IP( ret, clientip, "Clear Log" );
		}
	}

	if( command & ADAPTER_COMMAND__RETURN_CLEAR_GPS )
	{
		DBGLOG( "%s:%i: Command Clear GPS Log\n", __func__, __LINE__ );

		if( !recorder )
		{
			*answer_flag &= ~ADAPTER_COMMAND__RETURN;
			*answer_flag |= ADAPTER_COMMAND__RETURN_NODEVICE;
		}
		else
		{
			ret = AdapterRecorderClearGPSLog( Requests, RCount, Status, *recorder );

			if( ret < 0 )
			{
				*answer_flag &= ~ADAPTER_COMMAND__RETURN;
				*answer_flag |= ADAPTER_COMMAND__RETURN_NODEVICE;
			}

			INFO_IP( ret, clientip, "Clear GPS Log" );
		}
	}

	//���������� ��������
	if( command & ADAPTER_COMMAND__RETURN_STOP_ERASE )
	{
		DBGLOG( "%s:%i: Command stop erase\n", __func__, __LINE__ );
		
		INFO_IP( 1, clientip, "Stop Erase - not implementation" );
		AdapterRequestsLog( Status, Requests, *RCount );

		DBGERR( "%s: Command stop erase not implementation\n", __func__ );
	}

	//������ ���������)))
	if( command & ADAPTER_COMMAND__RETURN_START_CLEAR )
	{
		DBGLOG( "%s:%i: Start clear\n", __func__, __LINE__ );

		if( !( *recorder ) )
		{
			*answer_flag &= ~ADAPTER_COMMAND__RETURN;
			*answer_flag |= ADAPTER_COMMAND__RETURN_NODEVICE;
		}
		else
		{
			ret = AdapterRecorderStartClear( Requests, RCount, Status, *recorder );

			if( ret < 0 )
			{
				*answer_flag &= ~ADAPTER_COMMAND__RETURN;
				*answer_flag |= ADAPTER_COMMAND__RETURN_NODEVICE;
			}
			
			INFO_IP( ret, clientip, "Start Clear" );
			AdapterRequestsLog( Status, Requests, *RCount );

			if( AdapterRecorderGetStatus( &Status->GlobalDevice_t, *recorder ) )
			{
				DBGERR( "%s:%i: It wasn't possible to get status for cache\n", __func__, __LINE__ );
				return -1;
			}
		}
	}

	//���������� �������� ��������
	if( command & ADAPTER_COMMAND__RETURN_NETSAVE )
	{
		DBGLOG( "%s:%i: Command Save Adapter Settings\n", __func__, __LINE__ );
		ret = AdapterSettingsSaveFromRequest( Requests, RCount, Status, 0 );

		if( ret < 0 )
		{
			*answer_flag &= ~ADAPTER_COMMAND__RETURN;
			*answer_flag |= ADAPTER_COMMAND__RETURN_ERROR;
		}

		INFO_IP( ret, clientip, "Save Adapter Settings" );
		AdapterRequestsLog( Status, Requests, *RCount );
	}
	

	if( command & ADAPTER_COMMAND__RETURN_NETAPPLY )
	{
		DBGLOG( "%s:%i: Command Apply Adapter Settings\n", __func__, __LINE__ );
		ret = AdapterSettingsSaveFromRequest( Requests, RCount, Status, 1 );

		if( ret < 0 )
		{
			*answer_flag &= ~ADAPTER_COMMAND__RETURN;
			*answer_flag |= ADAPTER_COMMAND__RETURN_ERROR;
		}
		
		INFO_IP( ret, clientip, "Apply Adapter Settings" );
		AdapterRequestsLog( Status, Requests, *RCount );
	}

	if( command & ADAPTER_COMMAND__RETURN_REBOOT )
	{
		DBGLOG( "%s:%i: Command Reboot\n", __func__, __LINE__ );
		ret = AdapterCommandReboot( Status );

		if( ret < 0 )
		{
			*answer_flag &= ~ADAPTER_COMMAND__RETURN;
			*answer_flag |= ADAPTER_COMMAND__RETURN_ERROR;
		}
		
		INFO_IP( ret, clientip, "Command Reboot" );
		AdapterRequestsLog( Status, Requests, *RCount );
	}

	if( command & ADAPTER_COMMAND__RETURN_SLEEP )
	{
		DBGLOG( "%s:%i: Command Sleep\n", __func__, __LINE__ );
		ret = AdapterCommandSleep( Status );

		if( ret < 0 )
		{
			*answer_flag &= ~ADAPTER_COMMAND__RETURN;
			*answer_flag |= ADAPTER_COMMAND__RETURN_ERROR;
		}

		INFO_IP( ret, clientip, "Command Sleep" );
		AdapterRequestsLog( Status, Requests, *RCount );
	}

	return 0;
}

int AdapterCommandWorkRequest( char *b, size_t bsize,
                               axis_httpi_request *Requests,
                               size_t Count,
                               unsigned int CommandMask,
                               AdapterGlobalStatusStruct_t *Status, struct Recorder_s **recorder,  axis_ssl_wrapper_ex_sock *sock )
{
	if( !b || bsize <= 0 || !Requests || Count < 0 || !Status )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return 0;
	}

	int ret = 0;

	DBGLOG( "%s:%i A am hear\n", __func__, __LINE__ );
	int command_flag = 0;
	int cc = 0;
	int lcc = 0;
	size_t size = bsize;
	char *command;
	int answer_flag = 0;
	//�������� ������� �� ��������
	command_flag = AdapterInterfaceGetCommand( Requests, &Count, CommandMask, &command, Status );

	if( command_flag < 0 )
	{
		//������ ��������� ������� ������ ������ ���� - ������� ����, �� ��� ��������
		//� ������ ������ ���� ������� � ������, � ��� ��������� � ��������������
		//�� ����� ������ � 0 ����������� �� �� ���������� ������ �������!!!!
		answer_flag |= ADAPTER_COMMAND__RETURN_ERROR;
		DBGERR( "%s: It wasn't possible to get command from request\n", __func__ );
	}

	else
	{
		if( command_flag )
		{
			DBGLOG( "%s:%i: Try work on command flag\n", __func__, __LINE__ );
			ret = AdapterCommandWork( command_flag, Requests, &Count, Status, recorder, &answer_flag , sock );

			if( ret < 0 )
			{
				DBGERR( "%s: It wasn't possible to work request\n", __func__ );
				return 0;
			}
		}

		else
		{
			DBGLOG( "%s:%i: Try work on mask\n", __func__, __LINE__ );
			ret = AdapterCommandWorkOnMask( CommandMask, Status, recorder, &answer_flag );

			if( ret < 0 )
			{
				DBGERR( "%s:It wasn't possible to work mask\n", __func__ );
				return 0;
			}
		}
	}

	//��� �� ���� ������� ��� ���������������
	//���� ������ ���� �� �������������� � ����������� �� � �������
	int warning_flag = DispatcherLibCommandGetWarningFlagAndDebugPrintf( Requests, Count );

	//����������� ������ - ������, � �� ������ ����� �������� �������
	if( answer_flag & ADAPTER_COMMAND__RETURN_ERROR )
		DispatcherLibCommandErrorFlagDebugPrintf( Requests, Count );

	
	
	if( warning_flag )
	 answer_flag |= ADAPTER_COMMAND__RETURN_WARNING;

	
	lcc = DispatcherLibIniFileSprintfCommandRequestsResponse(

	              b + cc, size,
	              Requests,
	              Count,
	              answer_flag,
	              command );

	cc	+= lcc;
	size	-= lcc;

	DBGLOG( DISPATCHER_LIB_DEBUG__DONE );

	b[ bsize - 1 ] = 0x00;

	DBGLOG( "%s: Return %d\n", __func__, cc );

	//pthread_mutex_unlock(&Status->sets_mutex);

	return cc;

}



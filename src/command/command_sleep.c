//�������, ������� ������ ������� ���������� ���� �� �� ���� �������

//������: 2017.02.19 16:08
//�����: ��������� ����� ��� "����" ������
//�����: i.podkolzin@ross-jsc.ru

//���������� �������
#include <axis_error.h>

//������������ �������
#include <dispatcher_lib/dispatcher_lib_resource.h>

//��������� �������
#include <adapter/new_command.h>
#include <adapter/recorder.h>
#include <adapter/interface.h>
#include <adapter/dispatcher.h>
#include <adapter/power.h>

#include <adapter/sleep.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
//#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif



//������� ������� ���� ��������� �������
//����:
// Status - ��������� �� ���������� ��������� �������
//�������:
// ���� ���� ��� ������ ������� ������� ������� � ����������
int AdapterCommandSleep( AdapterGlobalStatusStruct_t *Status )
{
	int ret;

	if( !Status )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	//��� �� ���������� ������������� ����� - ������ � �������� ������ ��� �� �����
	//��������� �� ��� ���� �����������
	ret = AdapterRecorderSendCommandAllRecorders( Status, &Status->Recorder, ADAPTER_INTERFACE_COMMAND__STOP_UPLOAD );

	if( ret < 0 )
	{
		DBGERR( "%s: It wasn't possible to stop recorder uploading\n", __func__ );
		//��� �� ����������� ������
	}

	//��� �� ���� ������������ ����������, ��� �� �� ������ ���� ��� �� ���������
	ret = AdapterInterfaceSendCommand( Status, AXIS_INTERFACE_EVENT_STORAGE );

	if( ret < 0 )
	{
		DBGERR( "%s:%i: It wasn't possible to send event 'Storage' to interface\n", __func__, __LINE__ );
	}

	//� ��� ���� ��������� ���������� ���������� - �����������, ��� ���� �����������
	ret = AdapterRecorderSendCommandAllRecorders( Status, &Status->Recorder, ADAPTER_COMMAND__RETURN_STOP_MONITORING );

	if( ret < 0 )
	{
		DBGERR( "%s: It wasn't possible to stop recorder monitoring\n", __func__ );
		//��� �� ����������� ������
	}

	//�� ����� ���� �� ������ ���������� ���� ���� ��� ��� ��������� �����
	Status->AdapterNeedSleep = 1;

	return 0;
}


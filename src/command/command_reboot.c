//������� ������� ������ ������������� �������
//���� ��� ��������

//���������� �������
#include <axis_error.h>

//������������ �������
#include <dispatcher_lib/dispatcher_lib_resource.h>

//��������� �������
#include <adapter/new_command.h>
#include <adapter/dispatcher.h>
#include <adapter/power.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
//#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif



//������� ������� ���� ��������� �������
//����:
// Status - ��������� �� ���������� ��������� �������
//�������:
// ���� ���� ��� ������ ������� ������� ������� � ����������
int AdapterCommandReboot( AdapterGlobalStatusStruct_t *Status )
{

	if( !Status )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}
	
	//�� ����� ���� �� ������ ���������� ���� ����
	//��� ��� ���� �����������
	if ( Status->AdapterNeedReboot )
	{
		DBGLOG("%s: Try reboot device\n", __func__ );

		sync(); //�� ������ ������
		sleep( 5 );

		//������������ ���������� ������ ����� � ���� ����
		AdapterPowerGlobalReboot( Status );
	
		//���� ����� �� �������� - ���� ����������� ���
		DBGLOG("%s: Can't reboot system. Waite watchdog\n",__func__);
		while( 1 )
		{
			sleep( 10 );
		}
	}

	Status->AdapterNeedReboot = 1;

	return 0;

}

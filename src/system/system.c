//������� ������� ��������    ������!!!

#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>
#include <string.h>
#include <sys/ioctl.h>

#include <axis_error.h>
#include <termios.h>
#include <sys/select.h>
#include <unistd.h>
#include <axis_locks.h>
#include <axis_keyboard.h>

#include <dispatcher_lib/dispatcher_lib_resource.h>
#include <dispatcher_lib/getopt.h>
#include <dispatcher_lib/monitoring.h>
#include <dispatcher_lib/settings.h>
#include <dispatcher_lib/requests.h>

#include <dmxxx_lib/dmxxx_resource.h>
#include <dmxxx_lib/dmxxx_calls.h>

#include <adapter/system.h>
#include <adapter/global.h>
#include <adapter/debug.h>
#include <adapter/resource.h>
#include <adapter/interface.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
//#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif

static struct termios keyboard_old_termios;

static int adapter_keyboard_init( void )
{

	int ret = STDIN_FILENO; //������ ��� ��������� ���������� �����
	struct termios _new; //��� ������ ��������� ���������

	//� ��� ������� �� ����� ���� ������� - � ���� ����������� ������ ��!!!
	//� ������ ������� ��� ����� ���� � ������� - �������� �� �������� �������
	//� ���� ��� ������ ��� �� �� �� ������ ���������� ������
	//�� ����� � ������ ������� ������� /dev/null - ���� �����
	//� ��� ����� ������????

	//����� ���� �������� ��� ��������� ����������� ����� �� ���������
	// �������� fileno( stdin )
	// ����� ��������� �������� �� ������ ���� ����������
	// � � ���� ������ �� �������������� ��� ��������
	// � ���������� ����������

	if( ret < 0 )
    {
		DBGERR( "%s: No device STDIN handle\n", __func__ );
		//� ��� ��� ������� ��������� ���� - ������
		return -AXIS_ERROR_DEVICE_NOT_OPEN;
	}

	if( !isatty( ret ) )
	{
		//��� � ��� �� �������
		DBGERR( "%s: Device this handle %d is not tty\n", __func__, ret );
		return -AXIS_ERROR_CANT_SET_PARAMETR;
	}

	//������ ����� ����� ISIG ��� ������������ ��������� � ����������
	//����������� ��� ����

	//��������� ������ ��������� ��� ������ ����

	//������� �������
	if( tcgetattr( ret, &keyboard_old_termios ) < 0 )
	{
		DBGERR( "%s: Can't get current tty atribute\n", __func__ );
		return -AXIS_ERROR_CANT_GET_PARAMETR;
	}

	//�������� ����� �� ������
	_new = keyboard_old_termios; //�������� ������ ���������
	_new.c_lflag &= ~( ICANON | ECHO); //�������� ������������ ����� � ����� ���
	_new.c_cc[VMIN] = 1; //�� 1�� �������
	_new.c_cc[VTIME] = 0; //�� 0 ������
	_new.c_oflag |= ONLCR|XTABS;
	_new.c_iflag |= ICRNL;
	_new.c_iflag &= ~IXOFF;
	// termbuf.c_lflag &= ~ICANON;

	//� ��������� �����
	if( tcsetattr( ret, TCSANOW, &_new) < 0 )
	{
		DBGERR( "%s: Can't set new tty atribue '%s'\n", __func__, strerror( errno ) );
		return -AXIS_ERROR_CANT_SET_PARAMETR;
	}

	return ret;

}//����� ������� ������������� ����������

//������� �������� ������ ������ ����������
int adapter_keyboard_close( int console )
{

	DBGLOG("%s: %i Close Console %i \n",__func__,__LINE__, console);
    if( console < 0 )
    {
	//���� ������ ���������
	return AXIS_NO_ERROR;
    }

    if( tcsetattr( console, TCSANOW, &keyboard_old_termios ) < 0 )
    {
	
    	perror("tcsetattr");
        return -AXIS_ERROR_CANT_SET_PARAMETR;
    }

    return AXIS_NO_ERROR;
}//����� ������� �������� ������ ������ ����������

void AdapterSystemInitSignals( void )
{
	struct sigaction act;
	sigset_t   set;

	memset( &act, 0, sizeof( act ) );

	act.sa_handler = AdapterGlobalSignalHendler;

	sigemptyset( &set );

	sigaddset( &set, SIGINT );
	sigaddset( &set, SIGILL );
	sigaddset( &set, SIGABRT );
	sigaddset( &set, SIGTERM );
	sigaddset( &set, SIGSEGV );
	sigaddset( &set, SIGPIPE );
	sigaddset( &set, SIGUSR1 );
	sigaddset( &set, SIGUSR2 );
	sigaddset( &set, SIGBUS );

	act.sa_mask = set;

	sigaction( SIGINT,	&act, 0 );
	sigaction( SIGILL,	&act, 0 );
	sigaction( SIGABRT, &act, 0 );
	sigaction( SIGTERM, &act, 0 );
	sigaction( SIGSEGV, &act, 0 );
	sigaction( SIGPIPE, &act, 0 );
	sigaction( SIGUSR1, &act, 0 );
	sigaction( SIGUSR2, &act, 0 );
	sigaction( SIGBUS,	&act, 0 );
}

//������� ������� ������ ��������� ���������������� �������� �� ����������
//����� ���������
//����:
// aDev - ��������� �� ��������� ������������
// errfile - ��� ����� ������ (���� ������� ����� ������
//�������:
// 0 - ��� ��
// ����� 0 ������� ������
static void AdapterSystemWorkNow( dmdev_t* aDev, const char *errfile )
{
	int i;
	struct termios oldsettings;
	int	NoTTY = 1;

	//�������� ���� �������������� ������������
	//� ������ �� ��������� ���������� - ����� ������� ������
	DMXXXClose( aDev );

	//NOTE - �� ��� ��� �� ����� ������ ������������� ������...
	DMXXXDeinit( aDev );

	//� ��� �� ���� ������� ��� UnixSocket ������� ���� �������
	AdapterInterfaceDeInit( aDev );

	i = tcgetattr( fileno(stdin), &oldsettings );

	if( i < 0 )
	{
		//����������� ����� ����� �� ���� �������� - ������ � ����������� �� ���� �� �����
		//��� ��� ��������� �� ���
		NoTTY = 0;
	}

	//������� ��� �����, ������� ������� - �� �������� ��� ������
	for( i = getdtablesize() - 1; i >= 0; --i )
	{
		( void )close( i );
	}

	//���������� �� ������������ ��������� - ���� �����
	if( NoTTY )
	{
		i = open( "/dev/tty", O_RDWR );

		if( i >= 0 )
		{
			( void )ioctl( i, TIOCNOTTY, 0 );

			close( i );
		}
	}

	//������� �� ��������� �������
	if( chdir( "/tmp" ) < 0 )
	{

	}

	//������������ ����� �������
	( void )umask( 027 );

	//������� ����������� ���������� �����/�����
	i = open( errfile, O_RDWR | O_CREAT,
	          S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH );


	if( strcmp( errfile, ADAPTER_CONSOLE_FILE_NULL ) )
	{
		//�� ������� ���� � �� ���������� null
		//��������� � �����
		lseek( i, 0, SEEK_END );
	}

	//������� ��������� �����������, ��� ����� �������
	if( dup( i ) < 0 )
	{

	}

	if( dup( i ) < 0 )  //���������� ��������� �� �������
	{

	}
}



//������� ������� ��������� ������� ������� � ������ � �����������
//����:
// aDev - ��������� �� ��������� ����������
// lock_file - ��������� �� ��� ��� �����
// ConsoleFile - ��������� �� ��� ����� � �������� ����������������
//	����� ����� �� ���������
//	������ ��� ������ /dev/null
//�����:
// = 0 - ��� ������
// < 0 - ��� �����
//���������� �������� �� �� ��� ������ ����� ���� �������� ���� ������
//�� �����������
static int AdapterSystemDemonizeOneCopy( dmdev_t* aDev, const char *LockFile, const char *ConsoleFile )
{

	//������� ��� ��������� ��� ���� ��� �� �������� ���������
	//� �������� ������
	if( !LockFile || !ConsoleFile || !aDev )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}


	//����������� �� ���� � ��� � ��������� ������ �� �����
	AdapterSystemWorkNow( aDev, ConsoleFile );

	//��������� ����� ����� ������ ���������
	int i = fork();


	if( i < 0 )
	{
		return -AXIS_ERROR_CANT_CREATE_THREAD;
	}

	if( i )
	{
		DBGLOG( "%s: Adapter started at %d pid\n", __func__, i );
		exit( 0 ); //������������ ������� - �����
	}



	//��� �� ��� ��� ���� ����� - �� ���� ����� �������� ���������
	//���� ����� � ������� ��������� ��� �����
	//� ����� ����� ������ �� ������
	sleep( 1 );

	return axis_locks_only_one_copy( LockFile );

}

#define ADAPTER_SYSTEM_INTERACTIVE 			0x00001 //��� �� ���� ����� ���������������� - �������� � ��������
#define ADAPTER_SYSTEM_FORK_DEAMON			0x00002 //��� ���� ������� ���� � ��� ��������������
#define ADAPTER_SYSTEM_WORK					0x00000 //��� ���� ������ ������������� ������ ������ � ��� (���������� ������)

//������� ������� ������ ��������� �����
//��������� ��� ���� ���� �� ��������� ����� ���������
//� ������ ����
//����:
// aDev - ��������� �� �������� ����������
// iDeamonFlag - ���� ���� ��� ���� ��� ���������� �������
//	ADAPTER_SYSTEM_INTERACTIVE = 0x00001 - ��� �� ���� ����� ����������������
//	ADAPTER_SYSTEM_FORK_DEAMON = 0x00002 - ��� ���� ������� ���� � ��� ��������������
//	ADAPTER_SYSTEM_WORK	= 0x000000 - ��� ���� ������ ������������� ������ ������ � ��� (���������� ������)
// LockFile - ��������� �� ��� ����� ���������� ���������
// ConsoleFile - ��������� �� ��� ����� ����� ������������ ������� ��� �����������
//������:
// ����� 0 � ������ ������
// 0 ��� ��
static int AdapterSysytemGlobalDemonTry( dmdev_t* aDev, int iDeamonFlag,
                                  const char *LockFile, const char *ConsoleFile )
{



	int ret;

	if( !LockFile || !ConsoleFile || !aDev )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	ret = axis_locks_only_one_copy( LockFile );

	if( ret < 0 )
	{
		DBGERR( "%s: __ Can't lock only one copy of progremm\n", __func__ );
		return ret;
	}
	else if( ret > 0 )
	{
		DBGERR( "%s: __ Enather copy of programm execute now\n", __func__ );
		return -AXIS_ERROR_INVALID_ANSED;
	}



	if( iDeamonFlag == ADAPTER_SYSTEM_FORK_DEAMON )
	{
		//�� ������������ ���� ��������� ��� ���������

		DBGLOG( "%s: Try demonize Adapter\n", __func__ );

		ret = AdapterSystemDemonizeOneCopy( aDev, LockFile, ConsoleFile );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't lock only one copy of programm -%08x'%s'\n", __func__,
			        -ret, LockFile );
			return ret;
		}
		else if( ret > 0 )
		{
			DBGERR( "%s: Enater copy of programm execute now\n", __func__ );
			return -AXIS_ERROR_INVALID_ANSED;
		}

	}
	else if( iDeamonFlag == ADAPTER_SYSTEM_WORK	)
	{
		//�� ����������� �� ���� ���������� � ������ �������� ��������
		DBGLOG( "%s: Adapter try work without terminal out now\n", __func__ );
		DBGLOG( "%s: Console loge see at '%s' file\n", __func__, ConsoleFile );

		AdapterSystemWorkNow( aDev, ConsoleFile );
	}
	else if( iDeamonFlag == ADAPTER_SYSTEM_INTERACTIVE )
	{
		//�� �������� � �������������� ������
		DBGLOG( "%s: Adapter try work now\n", __func__ );
	}

	return ret;
}

/**
 * @brief �������, ������� ������� ��������� ��������� � ����� ������
 *
 * @param aDev - ��������� �� ��������� ����������
 * @param interractiveflag - ���� ������������� ������
 * @param iNoDaemon - ���� ������� �������� � ����� ������
 * @return int
 */
int AdapterSystemDaemonPrepare( dmdev_t* aDev, int interractiveflag, int iNoDaemon )
{
	int DemonFlag = ADAPTER_SYSTEM_WORK;
	int ret;

	//����������� ��� �� �����, �� ��������� ����
	if( interractiveflag )
	{
		DemonFlag = ADAPTER_SYSTEM_INTERACTIVE;
	}
	else if( !iNoDaemon )
	{
		DemonFlag = ADAPTER_SYSTEM_FORK_DEAMON;
	}


	//���� ������ ����� ���� ��������������
	//����� ����� ��������������
	// ���� � ��� ��� �������������� �����
	// ���� �� � ������������� ������ - �� ������������
	ret = AdapterSysytemGlobalDemonTry( aDev, DemonFlag, ADAPTER_LOCK_FILE, ADAPTER_CONSOLE_FILE );

	if( ret < 0 )
	{
		DBGERR( "%s:It wasn't possible to start daemon\n", __func__ );
		return -1;
	}

	return 0;
}


//������� ������� ��� ��������� ���� ������   ������
//����:
// Pid - ��� �������� �������� ����������� ������
// Value - ���������� ���������� ������
//������:
// 0 - ��� ��
// ����� 0 � ������ ������
//����������:
// ������� ����� ���� ������, �� �������� 2
// SIGUSER1 - ������������� ����� ������ �� ��������� ������ ���� ����� ���� �������� �����
// SIGUSER2 - ��������� �������� �� ��������� �� ��������
int AdapterSystemSendSignal( pid_t pid, int Value )
{

	int ret = AXIS_NO_ERROR;

	DBGLOG( DISPATCHER_LIB_DEBUG__START );



	DBGLOG( "%s: Send  PID=%d Siganl=%d\n", __func__, pid
	        , Value );

	//���� ��� ������
	if( pid > 1 )
	{
		DBGLOG( "%s: Signal him\n", __func__ );

		ret = kill( pid, Value );

		if( ret < 0 )
		{

			DBGERR( "%s: Can't send signal to adapter\n", __func__ );

			if( errno == ESRCH )
			{
				DBGERR( "%s: No proccess %d found for signal\n", __func__,
				        pid );
			}

			return -AXIS_ERROR_CANT_IOCTL_DEVICE;
		}
	}
	else
	{
		DBGERR( "%s: Invalid  process PID=%d\n", __func__, pid );
		return -AXIS_ERROR_INVALID_ANSED;
	}


	return AXIS_NO_ERROR;

}

static int adapter_system_init_dm_lib( AdapterGlobalStatusStruct_t *Status )
{
	if( !Status )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	Status->GlobalDevice_t.dmdevice_type = Status->AdapterDevice;

	Status->GlobalDevice_t.dmdevice_name = DispatcherLibStringAdapterNameFromDevice( Status->AdapterDevice );

	if( DMXXX_init_lib( &Status->GlobalDevice_t ) )
	{
		DBGERR( "%s:%i: It wasnt't possible to init libs\n", __func__, __LINE__ );
		return -1;

	}

	return 0;
}


int adapter_system_init_structures( AdapterGlobalStatusStruct_t *Status )
{
	if( !Status )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	//����� ���� ������ ����������������� �������� �����
	//�������� �� �� ��� ��������� ��� �������� � ������
	if( DispatcherLibSettingsCheckStructSize( ) )
	{
		DBGERR( "%s:Invalid structure size\n", __func__ );
		DispatcherLibStructPrintSize();
		return -1;
	}

	//�������������� ��������� �������
	DispatcherLibUploadInit( &Status->AdapterSSlUpload );

	Status->AdapterSSlUpload.AdapterType = Status->AdapterDevice;

	//�������������� ��������� �������
	DispatcherLibEventInit( &Status->ExchangeEvents.Events );

	DispatcherLibRequestCreateExchange( ( uint16_t * )&Status->ACCStatus, AXIS_DISPATCHER_CMD_GET_ACC_STATE, sizeof( Status->ACCStatus ) );

	Status->CopyStatus.PID = -1;

	return 0;
}

/**
 * @brief ������� ���������������� ������� ��� ���������� ������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @param adapterdevice - ��� ���������� ������������
 * @param iinteractiveflag - ���� ������ � ������������� ������
 * @param iresquemode - ���� ����, ��� �� ����� �������� � ������ �������������� ��������
 * @param iresetvalueflag - ���� ����, ��� ���� �������� ��������� � ����� �� ���������
 * @param ilogging - ���� ������� ������ �������� ������������
 * @param iHaveConsole - ���� ����, ��� ��������� �������� � ������� �� �������
 * @param iNoInterface - ���� ����, ��� ��������� �� ������ ��������� ���������
 * @param KernelDebugLevel - ������� ������� ����
 * @return int - 0 - ��, ����� ������
 */
int AdapterSystemConfigure( AdapterGlobalStatusStruct_t *Status, int adapterdevice, int iinteractiveflag, int iresquemode,
														int iresetvalueflag, int ilogging, int iHaveConsole, int iNoInterface,
														int KernelDebugLevel )
{
	int ret;

	if( !Status )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	ret = AdapterSetKernelDebugLevel( KernelDebugLevel );

	if( ret < 0 )
	{
		DBGERR( "%s: Can't set kernel debug level to %d\n", __func__, KernelDebugLevel );
	}

	Status->GlobalConsole = -1;

	Status->ACCStatus.DCIntACCRecord	= DISPATCHER_LIB_DC_INT_ACC_RECORD__CM;
	Status->ACCStatus.DCExtACCRecord	= DISPATCHER_LIB_DC_EXT_ACC_RECORD__CM;
	Status->ACCStatus.DCIntACCWork	= DISPATCHER_LIB_DC_INT_ACC_WORK__CM;
	Status->ACCStatus.DCExtACCWork	= DISPATCHER_LIB_DC_EXT_ACC_WORK__CM;

	Status->flag_interactive 	= iinteractiveflag;
	Status->flag_iconsole		= iHaveConsole;
	Status->flag_no_interface	= iNoInterface;

	Status->flag_resetsetstodefault = iresetvalueflag;

	Status->flag_resque = iresquemode;

	Status->AdapterDevice = adapterdevice;

	Status->icangosleep = 1; //�� ����� ����� ������, �� ���� �����, ����� ��� ���� ��������

	//����� ��������� ������
	Status->Build				= BUILD;

	Status->BuildTime			= BUILDTIME;

	Status->ilogging = ilogging; //�������� ������ ������������ �� �������� ������, ����� ���������� ��� ��������

	Status->InterfaceSleep = -1;

	if(adapter_system_init_dm_lib(Status))
	{
		DBGERR( "%s:It wasn't possible to init Lib\n", __func__ );
		return -1;
	}

	if( adapter_system_init_structures( Status ) )
	{
		DBGERR( "%s:It wasn't possible init hel structs\n", __func__ );
		return -1;
	}



	if( iinteractiveflag )
	{
		DBGLOG( "%s: Try work from interactive mode\n", __func__ );

		//� ������������� ������ �� ������ �������� � ������!
		DBGLOG( "%s: Try strat in interactive mode => PowerOn!\n", __func__ );
		//Status->PowerOn = 1;


		Status->GlobalConsole = adapter_keyboard_init( );

		if( Status->GlobalConsole < 0 )
		{
			DBGERR( "%s: Can't init console\n", __func__ );
			return -1;
		}
		else
		{
			DBGLOG( "%s: Console init at %d handel\n", __func__, Status->GlobalConsole );
		}
	}

	Status->AdapterStart = system_time_get();

	return 0;

}


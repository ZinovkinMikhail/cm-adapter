//������� ���������� ������

//������: 2011.10.24 17:11
//�����: ��������� ����� ��� "����"
//�����: i.podkolzin@ross-jsc.ru

#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <axis_error.h>
#include <axis_ssl_httpi_file_download.h>
#include <axis_httpi_tags.h>
#include <axis_ssl_httpi_server_error.h>

#include <adapter/resource.h>
#include <adapter/debug.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif

#define DISPATCHER_KERNEL_DEBUG_PROC_FILE		"/proc/sys/kernel/printk"

//������� ������� ����� �����������
//���� �� ����� � stderr � ���������������� � ����
//� ���� ��� ������ ��������� 2�� �� ������������� ��� ������� � 0
void AdapterDebugLogRotate( void )
{
	int dev = fileno( stderr );

	DBGLOG( "%s: Try log rotate\n", __func__ );

	//������ 2� ��������
	off_t err = lseek( dev, 0, SEEK_CUR );

	if( err < 0 )
	{
		DBGERR( "%s: Can't get current size\n", __func__ );
		return;
	}

	DBGLOG( "%s: Log size is %lld\n", __func__, err );

	if( err >= ADAPTER_DEBUG_MAX_LOG_SIZE )
	{

		DBGLOG( "%s: Do log rotate\n", __func__ );

		err = lseek( dev, 0, SEEK_SET );

		if( err < 0 )
		{
			DBGERR( "%s: Can't set log to begin\n", __func__ );
		}
	}

}


//������� ������ ����� ����������� ��� ����������
//���� ����� HTTP(S) ��������
//����
//	sock - ��������� �� ��������� ������ ����� ������� ��������� ����������
//	Requests - ��������� �� ��cc�� �������� �������
//	Count - ���������� ��������� � ������ �������
//  tCount - ��������� �� ������� ������ ������� ������ ������� ���� ������� ����������
//�������
//���������� ���� ���������� ������� ��� < 0 � ������ ������
int AdapterDebugTxtPrint(
        axis_ssl_wrapper_ex_sock *sock,
        axis_httpi_request *Requests,
        int Count,
        unsigned int *tCount )
{

	DBGLOG( "DBGLOG KERNEL START\n" );
	//������� ������ �����
	int Source;
	char buffer[65536]; //����� ��� ������
	int ret;
	int rcount = 0;

	DBGLOG( "%s: Try log apload\n", __func__ );

	Source = open( ADAPTER_CONSOLE_FILE, O_RDONLY );

	if( Source < 0 )
	{
		DBGERR( "%s: Can't open debug file '%s'\n", __func__, ADAPTER_CONSOLE_FILE );

		//���� �� ������ �������, �� ��� ���� �������???
		//��������� - ������� 404 ������
		ret = axis_ssl_httpi_server_send_error( sock,
												AXIS_SSL_HTTPI_ERROR__404_NOT_FOUND );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't print server error\n", __func__ );
		}

		return -AXIS_ERROR_CANT_OPEN_DEVICE;
	}

	//������� ����� �� ��� ����
	while( ( ret = read( Source, buffer, sizeof( buffer ) ) ) > 0 )
	{
		if( axis_ssl_wrapper_ex_write( sock, buffer, ret, 1 ) < 0 )
		{
			//����������� �������� � �����
			DBGERR( "%s: Can't write to socket\n", __func__ );
			rcount = -AXIS_ERROR_CANT_WRITE_SOCKET;
			goto end;
		}

		rcount += ret;
	}

	fd_set rdfdset;

	struct timeval tv;
	int MaxSel = 0;

	while( 1 )
	{

		( *tCount )++;
		FD_ZERO( &rdfdset );

		MaxSel = -1; //��� ����� �������� ��

		MaxSel = sock->sock;
		FD_SET( sock->sock,  &rdfdset ); //����������� ����� �������

		if( Source > MaxSel )
			MaxSel = Source;

		FD_SET( Source, &rdfdset );

		MaxSel++;

		//���������� ��������
		tv.tv_sec  = 1;

		tv.tv_usec = 0; //���� ������� = 1 sec

		ret = select( MaxSel, &rdfdset, NULL, NULL, &tv );

		if( ret < 0 )
		{
			//������ �������
			DBGERR( "%s: Select error errno=%d\n", __func__, errno );

			DBGLOG( "%s: EBADF=%d\n", __func__, EBADF );
			DBGLOG( "%s: EINTR=%d\n", __func__, EINTR );
			DBGLOG( "%s: EINVAL=%d\n", __func__, EINVAL );
			DBGLOG( "%s: ENOMEM=%d\n", __func__, ENOMEM );

			if( errno == EINTR )
			{
				//������!!! - �����??
				DBGERR( "%s: Not bloked signal!!!\n", __func__ );
			}

			rcount = -AXIS_ERROR_CANT_IOCTL_DEVICE;
			goto end;
		}
		else if( !ret )
		{

			continue;
		}

		if( FD_ISSET( sock->sock,  &rdfdset ) )
		{
			//���� ������� �� ����������� ������
			DBGERR( "%s: Source hendel event. Break\n", __func__ );
			return -AXIS_ERROR_INVALID_ANSED;
		}

		if( FD_ISSET( Source, &rdfdset ) )
		{
			rcount = read( Source, buffer, sizeof( buffer ) );

			if( rcount < 0 )
			{
				DBGERR( "%s:Can't read log\n", __func__ );
				rcount = -AXIS_ERROR_CANT_IOCTL_DEVICE;
				goto end;
			}

			if( rcount > 0 )
			{
				if( axis_ssl_wrapper_ex_write( sock, buffer, rcount, 1 ) < 0 )
				{
					//����������� �������� � �����
					DBGERR( "%s: Can't write to socket\n", __func__ );
					rcount = -AXIS_ERROR_CANT_WRITE_SOCKET;
					goto end;
				}
			}
		}

		//���� ������ �� ���������
//������� ��� ��� ���� ��� �� ������� Cicada-USB ��� ��������
	}

// 	ret = axis_ssl_file_download_send_header( sock, fsize,
// 					AXIS_HTTPI_CONTENT_TEXT_PLAN );
//     if( ret < 0 )
//     {
// 		DBGERR("%s: Can't send log header\n", __func__ );
// 		rcount = ret;
// 		goto end;
//     }

end:

	close( Source );

	return rcount;
}

//������� ������ ����� ����������� ��� ����������
//���� ����� HTTP(S) ��������
//����
//	sock - ��������� �� ��������� ������ ����� ������� ��������� ����������
//	Requests - ��������� �� ��cc�� �������� �������
//	Count - ���������� ��������� � ������ �������
//  tCount - ��������� �� ������� ������ ������� ������ ������� ���� ������� ����������
//�������
//���������� ���� ���������� ������� ��� < 0 � ������ ������
int AdapterKernelDebugTxtPrint(
        axis_ssl_wrapper_ex_sock *sock,
        axis_httpi_request *Requests,
        int Count,
        unsigned int *tCount )
{

	DBGLOG( "DBGLOG KERNEL START\n" );
	//������� ������ �����
	int Source;
	char buffer[65536]; //����� ��� ������
	int ret;
	int rcount = 0;

	DBGLOG( "%s: Try log apload\n", __func__ );

	Source = open( ADAPTER_KERNEL_FILE, O_RDONLY );

	if( Source < 0 )
	{
		DBGERR( "%s: Can't open debug file '%s'\n", __func__, ADAPTER_CONSOLE_FILE );
		return -AXIS_ERROR_CANT_OPEN_DEVICE;
	}

	//������� ����� �� ��� ����
	while( ( ret = read( Source, buffer, sizeof( buffer ) ) ) > 0 )
	{
		if( axis_ssl_wrapper_ex_write( sock, buffer, ret, 1 ) < 0 )
		{
			//����������� �������� � �����
			DBGERR( "%s: Can't write to socket\n", __func__ );
			rcount = -AXIS_ERROR_CANT_WRITE_SOCKET;
			goto end;
		}

		rcount += ret;
	}

	fd_set rdfdset;

	struct timeval tv;
	int MaxSel = 0;

	while( 1 )
	{

		( *tCount )++;
		FD_ZERO( &rdfdset );

		MaxSel = -1; //��� ����� �������� ��

		MaxSel = sock->sock;
		FD_SET( sock->sock,  &rdfdset ); //����������� ����� �������

		if( Source > MaxSel )
			MaxSel = Source;

		FD_SET( Source, &rdfdset );

		MaxSel++;

		//���������� ��������
		tv.tv_sec  = 5;

		tv.tv_usec = 0; //���� ������� = 1 sec

		ret = select( MaxSel, &rdfdset, NULL, NULL, &tv );

		if( ret < 0 )
		{
			//������ �������
			DBGERR( "%s: Select error errno=%d\n", __func__, errno );

			DBGLOG( "%s: EBADF=%d\n", __func__, EBADF );
			DBGLOG( "%s: EINTR=%d\n", __func__, EINTR );
			DBGLOG( "%s: EINVAL=%d\n", __func__, EINVAL );
			DBGLOG( "%s: ENOMEM=%d\n", __func__, ENOMEM );

			if( errno == EINTR )
			{
				//������!!! - �����??
				DBGERR( "%s: Not bloked signal!!!\n", __func__ );
			}

			rcount = -AXIS_ERROR_CANT_IOCTL_DEVICE;
			goto end;
		}
		else if( !ret )
		{

			continue;
		}

		if( FD_ISSET( sock->sock,  &rdfdset ) )
		{
			//���� ������� �� ����������� ������
			DBGERR( "%s: Source hendel event. Break\n", __func__ );
			return -AXIS_ERROR_INVALID_ANSED;
		}

		if( FD_ISSET( Source, &rdfdset ) )
		{
			rcount = read( Source, buffer, sizeof( buffer ) );

			if( rcount < 0 )
			{
				DBGERR( "%s:Can't read log\n", __func__ );
				rcount = -AXIS_ERROR_CANT_IOCTL_DEVICE;
				goto end;
			}

			if( axis_ssl_wrapper_ex_write( sock, buffer, rcount, 1 ) < 0 )
			{
				//����������� �������� � �����
				DBGERR( "%s: Can't write to socket\n", __func__ );
				rcount = -AXIS_ERROR_CANT_WRITE_SOCKET;
				goto end;
			}
		}

		//���� ������ �� ���������
//������� ��� ��� ���� ��� �� ������� Cicada-USB ��� ��������
	}

// 	ret = axis_ssl_file_download_send_header( sock, fsize,
// 					AXIS_HTTPI_CONTENT_TEXT_PLAN );
//     if( ret < 0 )
//     {
// 		DBGERR("%s: Can't send log header\n", __func__ );
// 		rcount = ret;
// 		goto end;
//     }

end:

	close( Source );

	return rcount;
}

//������� ��������� ������ ������� ��� ����
//�������:
// ������� �������
// ����� 0 � ������ ������
int AdapterGetKernelDebugLevel( void )
{

	int ret = AXIS_NO_ERROR;

	DBGLOG( "%s: Try get kernel debug level\n", __func__ );


	//���������� ��� ������ - ��������� ���� ���� � ������� ���� ��������
	FILE *fl = fopen( DISPATCHER_KERNEL_DEBUG_PROC_FILE, "r" );

	if( !fl )
	{
		DBGERR( "%s: Can't open proc file '%s'\n",
		        __func__,
		        DISPATCHER_KERNEL_DEBUG_PROC_FILE );
		return -AXIS_ERROR_CANT_OPEN_DEVICE;
	}

	char str[128];

	if( fgets( str, sizeof( str ), fl ) )
	{
		ret = atoi( str );
	}
	else
	{
		DBGERR( "%s: Can't read from proc file\n", __func__ );
		ret = -AXIS_ERROR_CANT_READ_DEVICE;
	}


	fclose( fl );

	return ret;

}

//������� ��������� ������ ������ ��� ����
//����:
// value - �������� ������� �������
//�������:
// 0 - ��� ������
// ����� 0 � ������ ������
int AdapterSetKernelDebugLevel( int value )
{

	DBGLOG( "%s: Try set kernel debug level to %d\n",
	        __func__, value );

	if( value < 0 )
	{
		DBGERR( "%s:Invalid argument\n", __func__ );
		return -1;

	}

	//���������� ��� ������ - ��������� ���� ���� � ������� ���� ��������
	FILE *fl = fopen( DISPATCHER_KERNEL_DEBUG_PROC_FILE, "w+" );

	if( !fl )
	{
		DBGERR( "%s: Can't open proc file '%s'\n",
		        __func__,
		        DISPATCHER_KERNEL_DEBUG_PROC_FILE );
		return -AXIS_ERROR_CANT_OPEN_DEVICE;
	}

	fprintf( fl, "%d\n", value );

	fclose( fl );

	return AXIS_NO_ERROR;

}


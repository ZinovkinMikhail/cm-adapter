//������� ������� �������� ������� �� ������ � � �������
//��������� �� ������� � �������

// ������: 2019.04.12 16:31
// �����: ��������� ����� �� "����" ������
// �����: i.podkolzin@ross-jsc.ru

#include <adapter/debug.h>
#include <adapter/network.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
//#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif

//������� ���������� ��� ������� �� ����
//NULL ���� �� �������
static const char *DispatcherLibEventGetEventNameRu( uint32_t EventType )
{

	const char *Type = NULL;

	//������ ��� �������
	switch( EventType )
	{
		case DISPATCHER_LIB_EVENT__EXT_POWER_BAD:
		{
			Type = "������ �������� ���";
			break;
		}
		case DISPATCHER_LIB_EVENT__POWER_UP:
		{
			Type = "���������";
			break;
		}
		case DISPATCHER_LIB_EVENT__WATCHDOG_UP:
		{
			Type = "���������� ������";
			break;
		}
		case DISPATCHER_LIB_EVENT__TIMER:
		{
			Type = "������";
			break;
		}
		case DISPATCHER_LIB_EVENT__WAKEUP:
		{
			Type = "����������� ��";
			break;
		}
		case DISPATCHER_LIB_EVENT__SENSOR:
		{
			Type = "������ ������";
			break;
		}
		case DISPATCHER_LIB_EVENT__BUTTON:
		{
			Type = "������";
			break;
		}
		case DISPATCHER_LIB_EVENT__EXT_ACC:
		{
			Type = "������� ���";
			break;
		}
		case DISPATCHER_LIB_EVENT__INT_ACC:
		{
			Type = "��������� ���";
			break;
		}
		case DISPATCHER_LIB_EVENT__REBOOT:
		{
			Type = "������������";
			break;
		}
		case DISPATCHER_LIB_EVENT__SLEEP:
		{
			Type = "���";
			break;
		}
		case DISPATCHER_LIB_EVENT__TEMPERATURE:
		{
			Type = "�����������";
			break;
		}
		case DISPATCHER_LIB_EVENT__CONNECT:
		{
			Type = "����������";
			break;
		}
		case DISPATCHER_LIB_EVENT__DSP_EVENT:
		{
			Type = "����������";
			break;
		}
		case DISPATCHER_LIB_EVENT__ADXL:
		{
			Type = "������������";
			break;
		}
		case DISPATCHER_LIB_EVENT__RECORDER_FULL_NAND:
		{
			Type = "������ �����";
			break;
		}
		case DISPATCHER_LIB_EVENT_WIFI:
		{
			Type = "WiFi �����";
			break;
		}
		case DISPATCHER_LIB_EVENT_VOLUME:
		{
			Type = "��������� ��������";
			break;
		}
		case DISPATCHER_LIB_EVENT_STORAGE:
		{
			Type = "���������";
			break;
		}
		default:
		{
		 	DBGERR( "%s: Invalid event type to convert '0x%08x'\n", __func__, EventType );
			Type = "�������������";
		}
	}
	
	return Type;
}


//������� ������� �������� � ����� ����� ��� ����� ��� ������ �������
//����
// b - ��������� �� ����� ���� ���������� ������
// size - ������ ������ ������
// Status - ��������� �� ��������� �� �������� �������
// N - ����� ������� ������� ��������
// Flag - �������� �������� ����� ��� ���
//�������:
// ���������� ����������� ����
static size_t DispatcherLibEventPrintRu( char *b, size_t bsize, DispatcherLibEventStatus_t *Status,
											unsigned int N, unsigned int Flag )
{

	int cc=0;
	int lcc = 0;
	size_t size = bsize;
	char DataBuffer[ 64 ];
	int Current = N;

	if( !Status || !b || !size || N >= Status->EventCount )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return 0;
	}
	
	if( !Status->Events[ N ].Type )
	{
		DBGERR("%s: Invalid event %d type %08x\n",__func__, N, Status->Events[ N ].Type );
		return 0;
	}

	N++;

//���������
	lcc = snprintf( b+cc, size, "\r\n������� �������� '%s' :\r\n",
					DispatcherLibEventGetEventNameRu(  Status->Events[ Current ].Type  ) );

	if( lcc < 0 || lcc >= size )
	{
		DBGERR( "%s:%i: -- %d\n", __func__, __LINE__, lcc );
		goto end;
	}

	cc	   += lcc;
	size   -= lcc;

//����� ������������ �������
	
	system_time_print_ini( 
			DataBuffer,sizeof( DataBuffer ),
			Status->Events[ Current ].Data );

	lcc = snprintf( b+cc, size, "    ���� �������: %s;\r\n", DataBuffer );

	if( lcc < 0 || lcc >= size )
	{
		DBGERR( "%s:%i: -- %d\n", __func__, __LINE__, lcc );
		goto end;
	}

	cc	   += lcc;
	size   -= lcc;

//�������� ������� - ���� ����������

	//��� ����������� ��������� ������������� � �����
	if( Status->Events[ Current ].Type == DISPATCHER_LIB_EVENT__CONNECT )
	{
		int16_t Level = (Status->Events[ Current ].Value2 & 0x0000FFFF);
		uint32_t Band = Status->Events[ Current ].Value2 >> 16;
		const char *BandStr = AdapterLTEModemBandToString( Band );

		//�� ������ � ������ � � IP �����
		axis_sprint_ip( DataBuffer, (unsigned int)Status->Events[ Current ].Value );
		lcc = snprintf( b+cc, size, "    IP �����: %s, ������� �������: %d dBm, Band: %s;\r\n",
										DataBuffer, Level, BandStr );
	}
	else if( Status->Events[ Current ].Type == DISPATCHER_LIB_EVENT__SENSOR )
	{
		lcc = snprintf( b+cc, size, "    ��������: %d;\r\n", Status->Events[ Current ].Value );
	}
	else if( Status->Events[ Current ].Type == DISPATCHER_LIB_EVENT__RECORDER_FULL_NAND )
	{
		lcc = snprintf( b+cc, size, "    ��������: %d;\r\n", Status->Events[ Current ].Value2 );
	}
	else
	{
		lcc = 0;
	}

	if( lcc < 0 || lcc >= size )
	{
		DBGERR( "%s:%i: -- %d\n", __func__, __LINE__, lcc );
		goto end;
	}

	cc	   += lcc;
	size   -= lcc;

//��������� ���	
	if( Status->Events[ Current ].IntACCVoltage )
	{
//���������� ���������� ���
		lcc = snprintf( b+cc, size, "    ��������� ���: ���������� %0.2f �, ����������� %d �;\r\n",
						(float)Status->Events[ Current ].IntACCVoltage/(float)1000,
						Status->Events[ Current ].IntTemperatura );

		if( lcc < 0 || lcc >= size )
		{
			DBGERR( "%s:%i: -- %d\n", __func__, __LINE__, lcc );
			goto end;
		}

		cc	   += lcc;
		size   -= lcc;
	}
	
//������� ���
	if( Status->Events[ Current ].ExtACCVoltage )
	{
//���������� �������� ���
		lcc = snprintf( b+cc, size, "    ������� ���: ���������� %0.2f �, ����������� %d �;\r\n",
						(float)Status->Events[ Current ].ExtACCVoltage/(float)1000,
						Status->Events[ Current ].ExtTemperatura );

		if( lcc < 0 || lcc >= size )
		{
			DBGERR( "%s:%i: -- %d\n", __func__, __LINE__, lcc );
			goto end;
		}

		cc	   += lcc;
		size   -= lcc;
	}

//������� ����������
	lcc = snprintf( b+cc, size, "    ������� ����������: %0.2f �;\r\n",
					(float)Status->Events[ Current ].InputVoltage/(float)1000 );

	if( lcc < 0 || lcc >= size )
	{
		DBGERR( "%s:%i: -- %d\n", __func__, __LINE__, lcc );
		goto end;
	}

	cc	   += lcc;
	size   -= lcc;

//���� ������� - ���� �����
	if( Flag )
	{
		lcc = snprintf( b+cc, size, "    ���� �������: %d.\r\n", Status->Events[ Current ].Flag );

		if( lcc < 0 || lcc >= size )
		{
			DBGERR( "%s:%i: -- %d\n", __func__, __LINE__, lcc );
			goto end;
		}

		cc	   += lcc;
		size   -= lcc;
	}

end:

	b[ cc ] = 0x00;
    b[ bsize - 1 ] = 0x00;

    return cc;
	
}

//������� ������� �������� � ����� ����� ��� ����� ��� ������ �������
//����
// b - ��������� �� ����� ���� ���������� ������
// size - ������ ������ ������
// Status - ��������� �� ��������� �� �������� �������
// N - ����� ������� ������� ��������
// Flag - �������� �������� ����� ��� ���
//�������:
// ���������� ����������� ����
size_t DispatcherLibEventPrintEx( char *b, size_t bsize, DispatcherLibEventStatus_t *Status,
											unsigned int N, unsigned int Flag )
{

	int cc=0;
	int lcc = 0;
	size_t size = bsize;
	char DataBuffer[ 64 ];
	int Current = N;

	if( !Status || !b || !size || N >= Status->EventCount )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return 0;
	}

	if( !Status->Events[ N ].Type )
	{
		DBGERR("%s: Invalid event %d type %08x\n",__func__, N, Status->Events[ N ].Type );
		return 0;
	}

	N++;

	//����� ������������ �������

	system_time_print_ini(
			DataBuffer,sizeof( DataBuffer ),
			Status->Events[ Current ].Data );

	lcc = axis_sprintf_shablon_t(
					b+cc, size,
					DISPATCHER_LIB_INTERFACE_INI__EVENTS_DATA_SHABLON,
					N,
				    DataBuffer );

	cc	   += lcc;
	size   -= lcc;
	if( size <= 0 ) goto end;

	//��� �������
	lcc = axis_sprintf_shablon_t(
					b+cc, size,
					DISPATCHER_LIB_INTERFACE_INI__EVENTS_TYPE_SHABLON,
					N,
				    DispatcherLibEventGetEventName(  Status->Events[ Current ].Type  ) );

	cc	   += lcc;
	size   -= lcc;
	if( size <= 0 ) goto end;

	//�������� �������

	//��� ������� ���������� NAND �� ������ ����� ��� � ������� ����������
	if( Status->Events[ Current ].Type == DISPATCHER_LIB_EVENT__RECORDER_FULL_NAND )
	{
		lcc = axis_sprintf_shablon_d( b+cc, size,
									  DISPATCHER_LIB_INTERFACE_INI__EVENTS_NAND_PERCENT_SHABLON,
									  N, Status->Events[ Current ].Value2 );

		cc	   += lcc;
		size   -= lcc;
		if( size <= 0 ) goto end;
	}

	//��� ����������� ��������� ������������� � �����
	if( Status->Events[ Current ].Type != DISPATCHER_LIB_EVENT__CONNECT )
	{

		lcc = axis_sprintf_shablon_d(
					b+cc, size,
					DISPATCHER_LIB_INTERFACE_INI__EVENTS_VALUE_SHABLON,
					N,
				    Status->Events[ Current ].Value );

		cc	   += lcc;
		size   -= lcc;
		if( size <= 0 ) goto end;
	}
	else
	{
		int16_t Level = (Status->Events[ Current ].Value2 & 0x0000FFFF);
		uint32_t Band = Status->Events[ Current ].Value2 >> 16;
		const char *BandStr = AdapterLTEModemBandToString( Band );

		//�� ������ � ������ � � IP �����
		axis_sprint_ip( DataBuffer, (unsigned int)Status->Events[ Current ].Value );
		lcc = axis_sprintf_shablon_t(
					b+cc, size,
					DISPATCHER_LIB_INTERFACE_INI__EVENTS_IP_SHABLON,
					N,
				    DataBuffer );

		cc	   += lcc;
		size   -= lcc;
		if( size <= 0 ) goto end;

		lcc = axis_sprintf_shablon_d(
					b+cc, size,
					DISPATCHER_LIB_INTERFACE_INI__EVENTS_SIGNAL_SHABLON,
					N, Level );

		cc	   += lcc;
		size   -= lcc;
		if( size <= 0 ) goto end;

		if( BandStr )
		{
			lcc = axis_sprintf_shablon_t( b+cc, size,
					DISPATCHER_LIB_INTERFACE_INI__EVENTS_BAND_SHABLON,
					N, 	BandStr );

			cc	   += lcc;
			size   -= lcc;
			if( size <= 0 ) goto end;
		}

	}

	//��������� ���
	if( Status->Events[ Current ].IntACCVoltage )
	{

		//���������� ���������� ���
		lcc = axis_sprintf_shablon_f(
					b+cc, size,
					DISPATCHER_LIB_INTERFACE_INI__EVENTS_INT_ACC_SHABLON,
				    N,
					(float)Status->Events[ Current ].IntACCVoltage/(float)1000 );

		cc	   += lcc;
		size   -= lcc;
		if( size <= 0 ) goto end;

		//����������� ���������� ���
		lcc = axis_sprintf_shablon_d(
					b+cc, size,
					DISPATCHER_LIB_INTERFACE_INI__EVENTS_INT_TEMP_SHABLON,
				    N,
					 Status->Events[ Current ].IntTemperatura );

		cc	   += lcc;
		size   -= lcc;
		if( size <= 0 ) goto end;

	}

	//������� ���
	if( Status->Events[ Current ].ExtACCVoltage )
	{
		//���������� �������� ���
		lcc = axis_sprintf_shablon_f(
					b+cc, size,
					DISPATCHER_LIB_INTERFACE_INI__EVENTS_EXT_ACC_SHABLON,
					N,
				    (float)Status->Events[ Current ].ExtACCVoltage/(float)1000 );

		cc	   += lcc;
		size   -= lcc;
		if( size <= 0 ) goto end;

		//����������� �������� ���
		lcc = axis_sprintf_shablon_d(
					b+cc, size,
					DISPATCHER_LIB_INTERFACE_INI__EVENTS_EXT_TEMP_SHABLON,
				    N,
					Status->Events[ Current ].ExtTemperatura );

		cc	   += lcc;
		size   -= lcc;
		if( size <= 0 ) goto end;
	}

	//������� ����������
	lcc = axis_sprintf_shablon_f(
				b+cc, size,
				DISPATCHER_LIB_INTERFACE_INI__EVENTS_INPUT_VOLTAGE_SHABLON,
				N,
				(float)Status->Events[ Current ].InputVoltage/(float)1000 );

	cc	   += lcc;
	size   -= lcc;
	if( size <= 0 ) goto end;

	//���� �������
	if( Flag )
	{
		lcc = axis_sprintf_shablon_d(
					b+cc, size,
					DISPATCHER_LIB_INTERFACE_INI__EVENTS_FLAG_SHABLON,
				    N,
					Status->Events[ Current ].Flag );

		cc	   += lcc;
		size   -= lcc;
		if( size <= 0 ) goto end;
	}

end:

    b[ bsize - 1 ] = 0x00;
    return cc;
}


/**
 * @brief ������� ��������� � ������ �������
 *
 * @param EStatus - ��������� �� ��������� �������
 * @param PrintFlag - ����� �� ���������� ����
 * @param Number - ����� ������� ��� ������
 * @return void
 */
void LOG_EVENT( DispatcherLibEventStatus_t *EStatus, uint32_t PrintFlag, uint32_t Number )
{
	char buff[65536];
	size_t count = 0;

	count = DispatcherLibEventPrintEx( buff, sizeof(buff), EStatus, Number, PrintFlag );
	buff[ count ] = 0x00;

	fprintf( stderr, "--------------------\n%s--------------------------\n", buff );

	memset( buff, 0, sizeof( buff ) );

	count = DispatcherLibEventPrintRu( buff, sizeof(buff), EStatus, Number, PrintFlag );
	
	PRINTLOG( buff, count );
}

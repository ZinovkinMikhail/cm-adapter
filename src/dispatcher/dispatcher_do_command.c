//������� ������� ����� �������� ���������� ������ ��������� �������

//������: 2010.09.08 17:13
//�����: ��������� ����� ��� "����"
//�����: i.podkolzin@ross-jsc.ru

//���������� �������
#include <string.h>

//������������ �������
#include <axis_error.h>

#include <dispatcher_lib/dispatcher_lib_resource.h>
#include <dispatcher_lib/requests.h>

//��������� �������
#include <adapter/global.h>
#include <adapter/dispatcher.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
//#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif


//������� ������� �������� ������� ����������
//����:
// Satstus - ��������� �� ���������� �������� �������
// Command - ���������� ������� � ��������
//������:
// 0 - ��� ��
// ����� 0 � ������ ������
int AdapterDispatcherDoCommand( AdapterGlobalStatusStruct_t *Status,
								uint16_t Command )
{

	int ret = AXIS_NO_ERROR;

	if( !Status || !Command )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}
	
	
	//���������� ���� ��������� � ������� ���� �� ����
#ifdef TMS320DM365
	if ( Status->GlobalUsbMajor > 0 )	
	{
		
		uint16_t Data[AXIS_DISPATCHER_EXCHANGE__MESSAGE_WORD_SIZE];
	
		memset( Data, 0, sizeof( Data ));
	
		ret = DispatcherLibRequestCreateExchange( Data, Command ,sizeof(Data));
		if( ret < 0 )
		{
			DBGERR("%s: Can't prepare data to exchange\n",__func__);
			return ret;
		}
		
		ret = DispatcherLibExchangeUSB( Status->GlobalUsbMajor, Data,
								 DISPATCHER_LIB_CICADA_CM_COMMAND_PIPE, 
								 DISPATCHER_LIB_CICADA_CM_STATUS_PIPE, &Status->GlobalUsbRecorderSemaphore );

									
		if( ret < 0 )
		{
			DBGERR("%s: Can't reciver adapter settings from device\n", __func__ );
			return ret;
		}
	
		return ret;

	}
	else
	{
		DBGERR("%s: Usb major not opened\n",__func__);
		return -1;
	}
#else

	DBGERR("NO COMMAND SUPPORT FOR THIS ARCHITECHTURE\n");
	
#endif

	if( Command == AXIS_DISPATCHER_CMD_POWER_OFF )
	{
			//�� ������� ��� ���� �� ����������!!!
		DBGLOG("%s: Command SLEEP = Exit this programm\n",__func__);
			
		ret = AdapterGlobalClose( Status );
		if( ret < 0 )
		{
			DBGERR("%s: Can't close all for command PowerOff\n",__func__);
			return ret;
		}
			
		exit( 0 );
			
	}
		
		
		
		return AXIS_NO_ERROR;
	

	//���� ���� ��� ������ �� ���� ��������� �� ��������� � �������� ���������

}

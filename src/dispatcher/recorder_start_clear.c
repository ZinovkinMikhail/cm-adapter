//������� ��������� ������ ������ ������ ������

//������: 2010.09.08 17:36
//�����: ��������� ����� ��� "����"
//�����: i.podkolzin@ross-jsc.ru

#include <adapter/dispatcher.h>


//������� ������� ��������� ��������� � ��� ��� ���� ������ �������
//(������� ���������)
//����:
// Status - ��������� �� ���������� ��������� ������� ���� � ���������
//�������:
// 0 - ��� ������
// ����� 0 � ������ �������
int AdapterDispatcherRecorderStartClear( AdapterGlobalStatusStruct_t *Status ){


	//��� ��� ������� ������ ���������� ������ �������� - ��� �� �� �� � ������� � ����

	return AdapterDispatcherDoCommand( Status, AXIS_DISPATCHER_CMD_START_CLEAR );

}

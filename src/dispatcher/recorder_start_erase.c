//������� ��������� ������ ����� ������ ������

//������: 2010.09.08 17:36
//�����: ��������� ����� ��� "����"
//�����: i.podkolzin@ross-jsc.ru

#include <adapter/dispatcher.h>


//������� ������� ��������� ��������� � ��� ��� ���� ������ �������
//(�������� 0)
//����:
// Status - ��������� �� ���������� ��������� ������� ���� � ���������
//�������:
// 0 - ��� ������
// ����� 0 � ������ �������
int AdapterDispatcherRecorderStartErase( AdapterGlobalStatusStruct_t *Status )
{


	//��� ��� ������� ������ ���������� ������ �������� - ��� �� �� �� � ������� � ����

	return AdapterDispatcherDoCommand( Status, AXIS_DISPATCHER_CMD_START_ERASE );

}

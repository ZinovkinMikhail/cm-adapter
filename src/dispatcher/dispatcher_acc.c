//������� ������� � ���������� ���������� ���

//������: 2010.09.08 16:58
//�����: ��������� ����� ��� "����"
//�����: i.podkolzin@ross-jsc.ru

//������������ �������
#include <axis_error.h>
#include <axis_find_from_file.h>
#include <dispatcher_lib/dispatcher_lib_resource.h>
#include <dispatcher_lib/requests.h>
#include <dispatcher_lib/printf.h>

//��������� �������
#include <adapter/dispatcher.h>
#include <adapter/hardware.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif

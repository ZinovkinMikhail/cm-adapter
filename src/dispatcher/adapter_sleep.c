
#include <time.h>
#include <signal.h>

#include <axis_dmlibs_resource.h>
#include <axis_error.h>

#include <dispatcher_lib/network.h>

#include <adapter/global.h>
#include <adapter/debug.h>

#ifdef TMS320DM355
#include <dm355_device.h>
#endif

#include <dmxxx_lib/dmxxx_calls.h>

#include <adapter/dmdevice.h>
#include <adapter/timer.h>
#include <adapter/leds.h>
#include <adapter/sleep.h>
#include <adapter/recorder.h>
#include <adapter/interface.h>
#include <adapter/power.h>
#include <adapter/sms.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif

/**
 * @brief �������, ������� �������� � ������� ����� ��� ���������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return void
 */
void AdapterPrintTimeToSleep( AdapterGlobalStatusStruct_t *Status )
{
	char buffer[ 128 ];
	char buffT1[ 128 ];
	char buffT2[ 128 ];
	int ret;

	system_time_print_ini( buffT1, sizeof( buffT1 ), system_time_translate( Status->LastInterfaceQuire ) );
	system_time_print_ini( buffT2, sizeof( buffT2 ), system_time_translate( time( NULL ) ) );

	if( Status->SleepTime > 0 && Status->icangosleep )
	{
		ret = snprintf( buffer, sizeof( buffer ), "%0.2f", (double)Status->SleepTime - difftime( time( NULL ), Status->LastInterfaceQuire ) );

		if( ret < 0 || ret >= sizeof( buffer ) )
		{
			buffer[0] = 'U';
			buffer[1] = 0x00;
		}
	}
	else
	{
		buffer[0] = 'X';
		buffer[1] = 0x00;
	}

	fprintf( stdout, "---------------------------------------------------------------------------------------------------\n");
	fprintf( stdout, "Sleep check:\tTime to sleep %s (NeadSl=%d SlTime=%d iCanSl=%d LasrQuery=%s Now=%s)\n",
			buffer,
			Status->AdapterNeedSleep, Status->SleepTime, Status->icangosleep,
			buffT1 + 11, buffT2 + 11 );

	//���� ���� ��������� - ������� ��� ���� ��� ������
	if( Status->GlobalDevice_t.dmdev_interface.dmctrl_ipresent )
	{
		time_t aTime = 0;

		ret = DMXXXModeButtonLastKeyPressTime( &Status->GlobalDevice_t, &aTime );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't get last key pressed time. Used last value\n", __func__ );
		}
		else if( aTime )
		{
			Status->LastKeyPressTest = aTime;
		}

		system_time_print_ini( buffT1, sizeof( buffT1 ), system_time_translate( Status->LastKeyPressTest ) );

		if( Status->InterfaceSleep > 0 &&
			Status->LastKeyPressTest   &&
			!(Status->GlobalDevice_t.dmdev_interface.dmctrl_status & DM_INTERFACE_STATUS_IN_SLEEP) )
		{
			ret = snprintf( buffer, sizeof( buffer ), "%0.2f", (double)Status->InterfaceSleep - difftime( time( NULL ), Status->LastKeyPressTest ) );

			if( ret < 0 || ret >= sizeof( buffer ) )
			{
				buffer[0] = 'U';
				buffer[1] = 0x00;
			}
		}
		else
		{
			buffer[0] = 'X';
			buffer[1] = 0x00;
		}

		fprintf( stdout, "Interface:\tTime to sleep %s (Status=%s SlTime=%d LastKeyPress=%s Now=%s)\n",
							buffer,
							Status->GlobalDevice_t.dmdev_interface.dmctrl_status & DM_INTERFACE_STATUS_IN_SLEEP ? "Sleep" : "Work",
							Status->InterfaceSleep, buffT1 + 11, buffT2 + 11 );
	}
	fprintf( stdout, "---------------------------------------------------------------------------------------------------\n");
}


/**
 * @brief �������, ������� ��������� ����� �� ����� �������� ��� ���
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return void
 **/
void AdapterSleepCheck( AdapterGlobalStatusStruct_t *Status )
{
	int ret;
	int NoTimers = 0;	//��� �������� � �������
	struct_data time_set;

	if ( !Status )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return ;
	}

	DBGLOG( "%s: Try sleep check. NeadSlep=%d SleepTime=%d iCangosleep=%d LastInterfaceQuire=%lu Now=%lu (%0.2f)\n", __func__,
			Status->AdapterNeedSleep, Status->SleepTime, Status->icangosleep,
			Status->LastInterfaceQuire, time( NULL ), difftime( time( NULL ), Status->LastInterfaceQuire ) );

	//���� ��� ����� �� ����� - �� � ������ ��� �� ����
	if( !Status->AdapterNeedSleep && Status->SleepTime < 0 && Status->InterfaceSleep < 0 )
	{
		return;
	}

	//��� ���, ������, ��� ������ ��� ��������� - �����!!!
	//�� �������� ��� ����� ������ ���� ����� ��� �����
	if( Status->AdapterNeedSleep )
	{
		//������������ ������ ���� - ���� �� ������ ����� ������ �����
		//�� ������ �� ��, ��� �� ����� ���������� ��� ����� ����������
		//������������ ��� ��� ��, ��� �� �� ����� �� ���������!!!
		//�� ���� � ��� ���� ������ � ������� ��� � ��� ���� ��, �� �������� ��� ��������
		//� ���� � ���� �� ��������� SMS ���������, �� ���� �� ������ ���������, �� �������
		//��� ��������
		//������ ��� �����-��1 � �����-��2 ���� ��� ��� ��� � ����������� �� ����
		//���� �� ������ ��� ��� - �� ��� ��� ���������� ���� ���������� ������������

		memset( &time_set,0,sizeof( struct_data ) );

		uint16_t check_alarm = AdapterTimerAlarmSet( Status, &time_set );

		if( !( check_alarm & 0xFF00 ) ) //TODO constant
		{
			DBGLOG( "%s: No timer in feature\n", __func__ );
			NoTimers = 1;
		}

		else
		{
			DBGLOG( "%s: I have timers in feature\n", __func__ );
		}

		//��� �������� � ���������� ���� ��� �������� �� �����
		if( NoTimers && Status->GlobalDevice_t.dmdev_timer.dmctrl_feature & DM_TIMER_NO_TIMER_NO_SLEEP )
		{
			DBGERR( "%s: No timer in feature - no sleep from command\n", __func__ );
			INFO_LOG( 1, "Command Sleep" ); //������� � ������, ��� �� ���� �� ���������
			Status->AdapterNeedSleep = 0;
			return;
		}

		//�������� ������ ��������� - ���� ����� ���������
		if( Status->GlobalDevice_t.dmdev_timer.dmctrl_feature & DM_TIMER_NO_PHONE_NO_SLEEP )
		{
			ret = AdapterPhoneCheck( Status );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't check phone for sleep\n", __func__ );
				return;
			}

			if( !ret )
			{
				DBGERR( "%s: No phone for wake up - no sleep from command\n", __func__ );
				INFO_LOG( 1, "Command Sleep" ); //������� � ������, ��� �� ���� �� ���������
				Status->AdapterNeedSleep = 0;
				return;
			}

			else
			{
				DBGLOG( "%s: I have phone to wake up\n", __func__ );
			}
		}

		//�������������� ���� ����� ��� ����� ����
		ret = AdapterSleepGo( Status, NoTimers ); //���� ��� ��������, �� ����� ��� 1

		if( ret < 0 )
		{
			DBGERR( "%s: Can't send sleep \n", __func__ );
		}
	}

	//NOTE - ���� ���� ��������� � �� �� ���� - �� ��� �� �� ������ �����
	if( Status->GlobalDevice_t.dmdev_interface.dmctrl_ipresent &&
		(Status->GlobalDevice_t.dmdev_interface.dmctrl_status & DM_INTERFACE_STATUS_IN_SLEEP) == 0 )
	{
		DBGLOG( "%s:%i: Interface is not sleep now can't sleep\n", __func__, __LINE__ );
		//��� �� ���� ����� �� ����� ����� ���������� �������� ������ � ������ ���������� �����
		Status->LastInterfaceQuire = time( NULL );
	}

	//������� �������� "���������" ���� ������������ ����, ��� �� ��� ���� �� �������
	if( AdapterRecorderCheckIfSomeOneUseNet( &Status->Recorder ) )
	{
		DBGLOG( "%s:%i: Recorder is monitoring or copy now can't sleep\n", __func__, __LINE__ );
		//��� �� ���� ����� �� ����� ����� ���������� �������� ������ � ������ ���������� �����
		Status->LastInterfaceQuire = time( NULL );
	}

	//��� ���, ������ ��� ����������� ������� ����� �����....
	if( Status->SleepTime > 0 &&  Status->icangosleep )
	{
		DBGLOG( "%s:%i: I can go sleep. Time left( last query ): %i sec (SleepTIme=%d)\n",
				__func__,
				__LINE__,
				( int )( Status->SleepTime - difftime( time( NULL ), Status->LastInterfaceQuire ) ),
				Status->SleepTime );
		//������ ���� ��������!!!
		//��������� ������ ����� �����

		//��� �������� ������
		//��� �����������
		if( ( time( NULL ) - Status->LastInterfaceQuire ) >= Status->SleepTime )
		{
			DBGLOG( "%s:%i: Las Querty GOOD\n",__func__,__LINE__ );

			if( AdapterRecorderCheckIfSomeOneUseNet( &Status->Recorder ) )
			{
				DBGLOG( "%s:%i: Recorder is monitoring or copy now can't sleep\n",__func__,__LINE__ );
				//��� �� ���� ����� �� ����� ����� ���������� �������� ������ � ������ ���������� �����
				Status->LastInterfaceQuire = time( NULL );
				return;
			}

			//� ���� ���� ���� - ��� ��������� �� �����, �� ��� ������?
			//��� ��� �����-��2, ���� ��� � ����� ��������� �� SMS, � ���� �� ������� ������
			//�� LTE ����� ����� ��������� ��� �� ��������, �� ���� �������� �� �����������
			//�� � ����������� �� �������� - ������ �� �����
			memset( &time_set,0,sizeof( struct_data ) );

			uint16_t check_alarm = AdapterTimerAlarmSet( Status, &time_set );

			if( !( check_alarm & 0xFF00 ) ) //TODO constant
			{
				DBGLOG( "%s: No timer in feature\n", __func__ );
				NoTimers = 1;
			}

			else
			{
				DBGLOG( "%s: I have timers in feature\n", __func__ );
			}

			if( Status->GlobalDevice_t.dmdev_timer.dmctrl_feature & DM_TIMER_NO_PHONE_NO_SLEEP )
			{
				//�������� ������ ��������� - ���� ����� ���������
				ret = AdapterPhoneCheck( Status );

				if( ret < 0 )
				{
					DBGERR( "%s: Can't check phone for sleep\n", __func__ );
					return;
				}

				if( !ret )
				{
					DBGERR( "%s: No phone for wake up - no sleep from last query\n", __func__ );
					Status->SleepTime = -1;
					return;
				}

				else
				{
					DBGLOG( "%s: I have phone to wake up\n", __func__ );
				}

			}

			int isleep = 1;

			//����� ���� ��� ���� ��������� ��� �� ���������� � ������ �� ����� ��������
			if( Status->GlobalDevice_t.dmdev_event.dmctrl_feature & DM_EVENT_CONTROL_STATE )
			{
				if( AdapterGlobalSensorIsActive( Status ) )
				{
					DBGLOG( "%s:%i: Can't go to sleep event setted \n", __func__, __LINE__ );
					isleep = 0;
				}
			}

			else
			{
				if( AdapterGlobalEventCheckOnSleep( Status ) )
				{
					DBGLOG( "%s:%i: Can't go to sleep \n", __func__, __LINE__ );
					isleep = 0;
				}
			}

			if( Status->GlobalDevice_t.dmdev_interface.dmctrl_ipresent )
			{
				time_t timekey = 0;

				if( AdapterDmDeviceGetLastKeyPress( &Status->GlobalDevice_t, &timekey ) )
				{
					DBGLOG( "%s:%i: It wasn't possible to get key press time ignore\n", __func__, __LINE__ );
				}

				else
				{
					if( ( time( NULL ) - timekey ) <= Status->SleepTime )
					{
						DBGLOG( "%s: User is Using interface no sleep now wait %i sec\n", __func__, Status->SleepTime );
						//���� �� ������� ���� ������ ������� �������� ������� ��� �� 10 �����
						Status->LastInterfaceQuire = ( ( time( NULL ) - Status->SleepTime ) + 10 );
						isleep = 0;
					}
				}
			}

			if( isleep )
			{
				DBGLOG( "%s: No quire at more time, try power off\n", __func__ );
				//����� ���� ���� ����� ������� ��� �������
				ret = AdapterGlobalSensorWork( Status );

				if( ret < 0 )
				{
					DBGERR( "%s: Can't init sensors before sleep\n", __func__ );
				}

				ret = AdapterSleepGo( Status, NoTimers ); //� �������� �� ���

				if( ret < 0 )
				{
					DBGERR( "%s: Can't send sleep \n", __func__ );
				}
			}

			else
			{
				DBGLOG( "%s: Cannot sleep wait, sensor work, try repeat timeout\n", __func__ );
				//Status->SleepTime = Status->AdapterSettings.Block0.AdapterSettings.Sleep;
				Status->LastInterfaceQuire = time( NULL );
			}
		}
	}

	if( Status->InterfaceSleep > 0 )
	{
		int isleep = 1;
		if( Status->GlobalDevice_t.dmdev_interface.dmctrl_ipresent &&
			!(Status->GlobalDevice_t.dmdev_interface.dmctrl_status &
			DM_INTERFACE_STATUS_IN_SLEEP) && 
			( time( NULL ) - Status->LastKeyPressTest ) >=
			Status->InterfaceSleep)
		{
			time_t timekey = 0;

			if( AdapterDmDeviceGetLastKeyPress( &Status->GlobalDevice_t, &timekey ) )
			{
				DBGERR( "%s:%i: It wasn't possible to get key press time ignore\n", __func__, __LINE__ );
			}
			else
			{
				if( ( time( NULL ) - timekey ) <=
						Status->InterfaceSleep )
				{
					DBGERR( "%s: User is Using interface no sleep now wait %i sec\n", __func__, Status->SleepTime );
					//���� �� �������� ���� ������ ������� �������� �������
					isleep = 0;
					Status->LastKeyPressTest = timekey;
					
				}
			}

			DBGLOG( "%s:%i: Check for sleep %i\n", __func__, __LINE__, isleep );

			if( isleep )
			{
				int iret = AdapterInterfaceSendCommand( Status, AXIS_INTERFACE_CMD_GO_SLEEP );

				if( iret < 0 )
				{
					DBGERR( "%s:%i: It wasn't possible to send event 'Go Sleep' to interface\n", __func__, __LINE__ );
				}

				Status->GlobalDevice_t.dmdev_interface.dmctrl_status |=
								DM_INTERFACE_STATUS_IN_SLEEP;
				AdapterInterfaceLcdOff(Status);
			}

			
		}
	}

}

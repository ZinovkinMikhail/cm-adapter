
#include <stdlib.h>
#include <stdint.h>
#include <sys/ioctl.h>
#include <pthread.h>
#include <signal.h>
#include <fcntl.h>
#include <errno.h>
//#include <bits/fcntl.h>
#include <sys/mman.h>
#include <axis_pid.h>
#include <axis_error.h>

#include <adapter/copy_data.h>
#include <adapter/interface.h>

#ifdef TMS320DM365
#include <mtdseq_ioctl.h>
#endif

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>

#else
#define DBGLOG( ... )
#endif


#define SELECT_TIMEOUT 1

static void AdapterCopyDataThreadCanceledCallback( void *value )
{

	SoftAdapterCopyDataStatus_t *Status =
	        ( SoftAdapterCopyDataStatus_t * )value;

	DBGLOG( "%s: Thread Canceled!!!!\n", __func__ );

	int fd;
	char* data = "3";
	int ret;
	sync();
	fd = open("/proc/sys/vm/drop_caches", O_WRONLY);
	if(fd < 0)
	{
		DBGERR("%s: It wasmn't possible to flush buffers\n", __func__ );
		return;
	}
	if( write(fd, data, sizeof(char)) < 0 )
	{
	 	DBGERR("%s: Can't write to proc\n", __func__ );
		close( fd );
		return;
	}

	ret = write( fd, data, sizeof( char ) );

	if( ret < 0 )
	{
		DBGERR( "%s: Can't write to proc\n", __func__ );
		close( fd );
		return;
	}
	
	
	close(fd);

	if(Status->fdfrom > 0)
	{
		close(Status->fdfrom);
		Status->fdfrom = 0;
	}

	if( Status->fdfrom > 0 )
	{
		close( Status->fdfrom );
	}

	sync();

	if( Status->fdto > 0 )
	{
		close( Status->fdto );
	}

	Status->status = AXIS_SOFTADAPTER_COPY_STATUS_STOPPED;
	
	ret = AdapterInterfaceSendCommand( AdapterGlobalGetStatus(), AXIS_DISPATCHER_CMD_STOP_COPY_TO_FILE );

	if( ret < 0 )
	{
		DBGERR( "%s:%i: It wasn't possible to send event 'Stop Copy' to interface\n", __func__, __LINE__ );
	}
	
	Status->PID = 0;

	DBGERR("%s:Exit from Callback\n",__func__);

//	pthread_exit( 0 );

	return;

}

int AdapterCopyDataCheck(SoftAdapterCopyDataStatus_t *Status)
{
	if( !Status )
	{
		DBGERR(DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	if( Status->PID < 0 )
	{
		//��� ������ ��� �������
		return -AXIS_NO_ERROR;
	}

	if( Status->PID == 0 )
	{
		DBGLOG("%s: Copy thread end of work\n",__func__);
		if( pthread_join( Status->thread, NULL ) < 0 )
		{
			DBGERR("%s: Can't join to thread\n",__func__);
			return -AXIS_ERROR_INVALID_ANSED;
		}
		DBGLOG("%s: Ended\n",__func__);
		Status->PID = -1;
		return -AXIS_NO_ERROR;
	}

	if( Status->PID > 0 )
	{
		DBGLOG("%s: Upload Thread started at pid %d TCount=%d (%d)\n",
				__func__, Status->PID, Status->TCount, Status->LastCount );

		if( Status->TCount == Status->LastCount )
		{

			DBGLOG("%s: Uploading thread is dead! Kill him\n",__func__);
			//����� ����� - ��������� ����

			//������ ����� ������ ���������� ��� �� ����, ��� ���
			//�� ���������� ������ �� ����� � ���� � �� ��� �� ��������� �� ����� ������
			//������ ���� ������� ������� ������� ��� ������!!!
			//������� ��� �� ������ �� ���� � ��� ��� �� ������ ������
			if( pthread_kill( Status->thread, SIGUSR1 ) )
			{
				DBGERR("%s: Can't send signal to canseled thread at pid %d\n",
					__func__,
					Status->PID );
				return -AXIS_ERROR_INVALID_ANSED;
			}

			//�������� ���� ��� ������������
			usleep( 2000 );

			//��� ���� ������ ����� ����� ��� ���
			//���� ����� - �� ���� - ���� ���, �� ������
			if( Status->PID != 0 )
			{
				DBGLOG("%s: Uploading thread not killed, try leter!\n", __func__ );
				return -AXIS_ERROR_INVALID_ANSED;
			}

			//����� ��������
			if( pthread_join( Status->thread, NULL ) < 0 )
			{
				DBGERR("%s: Can't join to thread\n",__func__);
				return -AXIS_ERROR_INVALID_ANSED;
			}
			Status->PID = -1;

		}
		else
		{
			Status->LastCount = Status->TCount;
		}

	}

	return AXIS_NO_ERROR;
}
#define BUFF_SIZE 1048576
static int AdapterCopyDataStartCopy(SoftAdapterCopyDataStatus_t *Status)
{
	if( !Status )
	{
		DBGERR("%s:%i: Invalid Argumentn\n",__func__,__LINE__);
		return -1;
	}



	fd_set rfds;





//	Status->seek;
#define PREVS
#ifdef PREVS
	FD_ZERO( &rfds );

	FD_SET( Status->fdfrom, &rfds );

// 	max_select =  Status->fdfrom;

	
// 	(*TCount)++;
#ifdef TMS320DM365
	int retval;
	int max_select = 0;
	struct timeval tv;
	struct mtdseq_kernel_upload kups;
#warning ������� ������� �� �����!!!
	kups.offset = 0;

	if(lseek64( Status->fdfrom, Status->seek, SEEK_SET) < 0)
	{
		DBGERR("%s:It wasn't possible to seek\n",__func__);
		return -1;
	}

	kups.size = /*104857600 * 2*/Status->sizetocopy;
	
	kups.socket = Status->fdto;

	kups.uptype = MTDSEQ_UPLOAD_TYPE_TO_FILE;

	kups.offset = Status->seek;

	uint64_t startseek = Status->seek;

	DBGLOG("%s:%i:Start to copy %llu offset %lld\n",__func__,__LINE__,kups.size, kups.offset );
	
	if ( ioctl( Status->fdfrom, MEMSEQUPLOADSTART, &kups ) < 0 )
	{
		DBGERR( "%s:It Wasn't possible to ioctl source device\n", __func__ );
		return -1;
	}

	time_t time_start = time(NULL);
	float speed;

	while ( 1 )
	{
		//pthread_testcancel();
		Status->TCount++;

		FD_ZERO( &rfds );

		FD_SET( Status->fdfrom, &rfds );

		max_select = Status->fdfrom;

		tv.tv_sec = SELECT_TIMEOUT;
		tv.tv_usec = 0;

		retval = select( max_select + 1, &rfds, NULL, NULL, &tv );

		if ( retval )
		{
			
			if ( ioctl( Status->fdfrom, MEMSEQFILEUPLOADSTATUS, &kups ) < 0 )
			{
				DBGERR( "%s:It Wasn't possible to ioctl status upload\n", __func__ );
				return -1;
			}
			 speed = ((float)(kups.size) / (float)(time(NULL)-time_start))/1024.0/1024.0;

			 DBGLOG("%s:%i:Mid Speed = %f \n",__func__,__LINE__,speed);

			Status->speed = speed * 1000.0;
			Status->seek = kups.offset - startseek;

			if (!kups.status)
			{
				return 0;
			}
			else	if(kups.status == 1)
			{

				continue;
			}
			
			DBGERR( "%s:Error on socket exit\n", __func__ );

			return -1;
		}
		else
		{
			if ( ioctl( Status->fdfrom, MEMSEQFILEUPLOADSTATUS, &kups ) < 0 )
			{
				DBGERR( "%s:It Wasn't possible to ioctl status upload\n", __func__ );
				return -1;
			}

			speed = ((float)( kups.offset - startseek) / (float)(time(NULL)-time_start))/1024.0/1024.0;

			Status->speed = speed * 1000.0;
			Status->seek = kups.offset - startseek;

			//���� ��� � ������ ���������� ����� selct ���� � ���������� ������
			//�� �������� �� ��� ��� �� ����� - ����� ����� � ���� ���������� �������������
			//������ �� ��� �������� ��� �������� ������ � ������ �� ������ ��� ������
			if( !kups.status )
			{
				DBGERR( "%s: Copy file thread unexpected success exit\n", __func__ );
				DBGLOG( "%s: Size=%llu Offset=%llu\n", __func__, kups.size, kups.offset );
				return 0;
			}
			else if( kups.status < 0 )
			{
				DBGERR( "%s: Copy file thread unexpected error exit\n", __func__ );
				return -1;
			}

			DBGLOG("%s:%i:Speed = %f \n",__func__,__LINE__,speed);
			//�������
			DBGLOG( "%s:%i:Retval Time out = %i\n", __func__, __LINE__, retval );
			DBGLOG( "%s: Size=%llu Offset=%llu\n", __func__, kups.size, kups.offset );
		}
	}

#else
	return -1;

#endif

#else
	
	int fd = Status->fdto;
	int prot = (PROT_WRITE);
	int flags = MAP_SHARED;
	
	uint8_t pattern_buf[BUFF_SIZE];
	int i;
	for ( i = 0; i < BUFF_SIZE;i++ )
	{
		pattern_buf[i] = ( i & 0xff );
	}

	

	if( lseek64( fd, Status->sizetocopy - 1, SEEK_SET ) == -1 )
	{
		int myerr = errno;
		printf( "ERROR: lseek failed (errno %d %s)\n", myerr, strerror( myerr ) );
		return EXIT_FAILURE;
	}
	
	float speed;
	void * addr = 0;
	addr = mmap64(NULL, Status->sizetocopy, prot, flags, fd, 0);
	if (addr == 0)
	{
		int myerr = errno;
		printf("ERROR (child): mmap failed (errno %d %s)\n", myerr, strerror(myerr));
		
	}
// 	 speed = ((float)( Status->sizetocopy) / (float)(time(NULL)-time_start))/1024.0/1024.0;
// 
// 	DBGLOG("%s:%i:Speed mmap = %f \n",__func__,__LINE__,speed);

write(fd, "", 1);
	uint64_t filll = Status->sizetocopy;
 int *baseaddr;
 baseaddr=addr;
   madvise((caddr_t)addr,Status->sizetocopy,MADV_SEQUENTIAL);
   time_t time_start = time(NULL);
   printf("%x\n",baseaddr);
	while(filll)
	{
// 		addr = mmap64(NULL,BUFF_SIZE , prot, flags, fd, Status->seek);
// 		if (addr == 0)
// 		{
// 		int myerr = errno;
// 		printf("ERROR (child): mmap failed (errno %d %s)\n", myerr, strerror(myerr));
// 
// 		}
//
// 		for(i = 0;i < BUFF_SIZE;i++)
// 		{
// 			*(int*)addr=i;
// 		}
// DBGLOG("%s:%i:BUS\n",__func__,__LINE__);
		memcpy((uint8_t*)addr,pattern_buf,sizeof(pattern_buf));
// 		DBGLOG("%s:%i:BUS\n",__func__,__LINE__);
		msync(addr,sizeof(pattern_buf),MS_ASYNC|MS_INVALIDATE);
		addr+=BUFF_SIZE;
		filll-=BUFF_SIZE;
		Status->seek+=BUFF_SIZE;

		if(!(filll % 10485760 ))
		{
			 speed = ((float)( Status->seek) / (float)(time(NULL)-time_start))/1024.0/1024.0;

			Status->speed = speed * 1000.0;

			DBGLOG("%s:%i:Speed = %f \n",__func__,__LINE__,speed);
		}
	}
	
	 speed = ((float)(Status->seek) / (float)(time(NULL)-time_start))/1024.0/1024.0;
	 
	DBGLOG("%s:%i:FINAL = %f \n",__func__,__LINE__,speed);
#endif
	
	return 0;

}

//���������� ������� ������� ����� ���������� � ��������� ������
static void *AdapterCopyDataThreadRoutine( void *Value )
{
	int ret;

	if( !Value )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		pthread_exit( NULL );
		return NULL;
	}

	SoftAdapterCopyDataStatus_t *Status =
	        ( SoftAdapterCopyDataStatus_t * )Value;

	if( Status->PID > 0 )
	{
		DBGERR( "%s: Thread already started at pid %d\n", __func__, Status->PID );
		pthread_exit( NULL );
		return NULL;
	}

	Status->PID = axis_getpid();

	//��� ��� ���� �� ������� �����
	//���� ����� ��� ������� �� ���� ���� �� ������ ��� �� ����� ���������


	DBGLOG("%s:%i:File to open = %s\n",__func__,__LINE__,Status->filepath);
	
// 	sync();

	int fdin = open64( /*"/tmp/mountflash/new"/*/Status->filepath,O_RDWR | O_CREAT | O_LARGEFILE | O_TRUNC, S_IRUSR | S_IWUSR );

	if(fdin < 0)
	{
		DBGERR("%s:It wasnt possible to open file to write\n",__func__);
		return NULL;
	}

	int fdout = open (RECORDER_DATA_BANJO_FILE_NAME,O_RDONLY);

	if(fdout < 0)
	{
		DBGERR("%s:It wasnt possible to open file to read\n",__func__);
		return NULL;
	}

	Status->fdfrom = fdout;

	Status->fdto = fdin;

	Status->status = AXIS_SOFTADAPTER_COPY_STATUS_COPY;
	pthread_cleanup_push( AdapterCopyDataThreadCanceledCallback, Status );
	if(AdapterCopyDataStartCopy(Status) < 0)
	{
		DBGERR("%s:It wasn't possible to start copy data to file\n",__func__);
		Status->status = AXIS_SOFTADAPTER_COPY_STATUS_ERROR;
	}
	else
	{
	
	Status->status = AXIS_SOFTADAPTER_COPY_STATUS_FINISHED;
	}
	
	
	//���������� �� ��� ��������� - �������� ��� ������
	pthread_cleanup_pop( 0 );

	//fsync(Status->fdto);
	int fd;
	char* data = "3";

	sync();
	fd = open("/proc/sys/vm/drop_caches", O_WRONLY);
	if(fd < 0)
	{
		DBGERR("%s: It wasn't possible to flush buffers\n", __func__ );
		return NULL;
	}
	if( write(fd, data, sizeof(char)) < 0 )
	{
	 	DBGERR("%s: Can't write data to proc\n", __func__ );
		close( fd );
		return NULL;
	}

	ret = write( fd, data, sizeof( char ) );

	if( ret < 0 )
	{
		DBGERR( "%s: Can't write data to proc\n", __func__ );
		close( fd );
		return NULL;
	}

	close(fd);

	if(Status->fdfrom > 0)
	{
		close(Status->fdfrom);
		Status->fdfrom = 0;
	}
	
	sync();
	
	if(Status->fdto > 0)
	{
		close(Status->fdto);
		Status->fdto = 0;
	}

	DBGERR( "%s: Exit from NORMAL\n", __func__ );

	ret = AdapterInterfaceSendCommand( AdapterGlobalGetStatus(), AXIS_DISPATCHER_CMD_STOP_COPY_TO_FILE );

	if( ret < 0 )
	{
		DBGERR( "%s:%i: It wasn't possible to send event 'Stop Copy' to interface\n", __func__, __LINE__ );
	}

	Status->PID  = 0;

	pthread_exit( NULL );

	return NULL;
}


int AdapterCopyDataStart( SoftAdapterCopyDataStatus_t *CopyStatus, AdapterGlobalStatusStruct_t *Status )
{
	if( !CopyStatus || !Status )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	if( Status->CopyStatus.PID == 0 )
	{
		DBGLOG("%s: Copy thread end of work\n",__func__);
		if( pthread_join( Status->CopyStatus.thread, NULL ) < 0 )
		{
			DBGERR("%s: Can't join to thread\n",__func__);
			return -AXIS_ERROR_INVALID_ANSED;
		}
		DBGLOG("%s: Ended\n",__func__);
		Status->CopyStatus.PID = -1;
	//	return -AXIS_NO_ERROR;
	}

	int wait = 5;
	

	while(wait--)
	{

		if( Status->CopyStatus.PID > 0 )
		{
			DBGERR( "%s:Copy thread already started\n", __func__ );
			sleep(1);
		}
		else
		{
			break;
		}

	}
	if( Status->CopyStatus.PID > 0 )
	{
		DBGERR( "%s:Copy thread already started\n", __func__ );
		return -1;
	}
	Status->CopyStatus.PID = 0;

	strcpy(Status->CopyStatus.filepath,CopyStatus->filepath);

	Status->CopyStatus.seek = CopyStatus->seek;

	Status->CopyStatus.sizetocopy = CopyStatus->sizetocopy;


	if( pthread_create( &Status->CopyStatus.thread, NULL,
	                    AdapterCopyDataThreadRoutine,
	                    ( void * )&Status->CopyStatus ) < 0 )
	{
		DBGERR( "%s: Can't start copy thread\n", __func__ );
		
		return -AXIS_ERROR_CANT_CREATE_THREAD;
	}


	return 0;

}

int AdapterCopyDataGetStatus(SoftAdapterCopyDataStatus_t *CopyStatus, AdapterGlobalStatusStruct_t *Status)
{
	if( !CopyStatus || !Status )
	{
		DBGERR("%s:%i: Invalid Argumentn\n",__func__,__LINE__);
		return -1;
	}

	static time_t lastreq = 0;

	if((time(NULL) - lastreq) > 8 )
	{
		DBGLOG("%s: %i Time to get data status\n",__func__,__LINE__);
		AdapterCopyDataCheck(&Status->CopyStatus);
		lastreq = time(NULL);
	}

	memcpy(CopyStatus,&Status->CopyStatus,sizeof(SoftAdapterCopyDataStatus_t));

	DBGLOG( "%s: Status=%d,Size=%llu,Offset=%llu\n", __func__, CopyStatus->status, CopyStatus->sizetocopy, CopyStatus->seek );

	return 0;
}

int AdapterCopyDataStop( AdapterGlobalStatusStruct_t *Status)
{
	if(  !Status )
	{
		DBGERR("%s:%i: Invalid Argumentn\n",__func__,__LINE__);
		return -1;
	}

	if( Status->CopyStatus.PID < 0 )
	{
		//��� ������ ��� �������
		return -AXIS_NO_ERROR;
	}

	if( Status->CopyStatus.PID == 0 )
	{
		DBGLOG("%s: copy thread end of work\n",__func__);
		if( pthread_join( Status->CopyStatus.thread, NULL ) < 0 )
		{
			DBGERR("%s: Can't join to thread\n",__func__);
			return -AXIS_ERROR_INVALID_ANSED;
		}
		Status->CopyStatus.PID = -1;
		return -AXIS_NO_ERROR;
	}

	if( Status->CopyStatus.PID > 0 )
	{
		DBGLOG("%s: copy Thread started at pid %d! Kill him\n",__func__,Status->CopyStatus.PID );

		//������ ����� ������ ���������� ��� �� ����, ��� ���
		//�� ���������� ������ �� ����� � ���� � �� ��� �� ��������� �� ����� ������
		//������ ���� ������� ������� ������� ��� ������!!!
		//������� ��� �� ������ �� ���� � ��� ��� �� ������ ������
		DBGLOG("KILLING copy Thread\n");
		if( pthread_cancel( Status->CopyStatus.thread ) )
		{
			DBGERR("%s: Can't send signal to canseled thread at pid %d\n",
				__func__,
				Status->CopyStatus.PID );
			return -AXIS_ERROR_INVALID_ANSED;
		}

		//����� ��������
		if( pthread_join( Status->CopyStatus.thread, NULL ) < 0 )
		{
			DBGERR("%s: Can't join to thread\n",__func__);
			return -AXIS_ERROR_INVALID_ANSED;
		}
		Status->CopyStatus.PID = -1;

	}

	//���� ��� ������ ������� ����� - �������
	if( Status->CopyStatus.fdfrom > 0 )
	{
		close(Status->CopyStatus.fdfrom);
	}

	if(Status->CopyStatus.fdto > 0)
	{
		close(Status->CopyStatus.fdto);
	}

	Status->CopyStatus.status = AXIS_SOFTADAPTER_COPY_STATUS_STOPPED;
	
	return -AXIS_NO_ERROR;
}

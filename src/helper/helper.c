//����� ����� ��� ������� ������� �������� �� ������ ������������

//���������� �������
#include <stdio.h>
#include <stdlib.h>

//������������ �������
#include <axis_uptime.h>
#include <axis_error.h>

#include <dispatcher_lib/dispatcher_lib_resource.h>
#include <dispatcher_lib/getopt.h>
#include <dispatcher_lib/network.h>
#include <dmxxx_lib/dmxxx_calls.h>

//��������� �������
#include <adapter/helper.h>
#include <adapter/resource.h>
#include <adapter/network.h>
#include <adapter/printf_ini_files.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
//#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif


//��� ������� �����
static const char *DELIMETR = "------------------------------------------------------------\n";

// ������� ������ �� ������ ��������� �� ������
void AdapterPrintUsage( void )
{
	fprintf( stderr, "Usage: adapter [comand]\n" );
	fprintf( stderr, "	-%c -%s - print this message\n",
			 ADAPTER_HELPER__PRINT_USAGE_ARG,
			 ADAPTER_HELPER__PRINT_USAGE_COMMAND );
	fprintf( stderr, "	-%c -%s - reset all settings to default value\n",
			 ADAPTER_HELPER__RESET_DEFAULT_ARG,
			 ADAPTER_HELPER__RESET_DEFAULT_COMMAND );
	fprintf( stderr, "\t-%c -%s - set rescue mode for network\n",
			 ADAPTER_HELPER__RESCUE_MODE_ARG,
			 ADAPTER_HELPER__RESCUE_MODE_COMMAND );
	fprintf( stderr, "\t-%c -%s - start in interactive mode\n",
			 ADAPTER_HELPER__INTERACTIVE_START_ARG,
			 ADAPTER_HELPER__INTERACTIVE_START_COMMAND );
	fprintf( stderr, "\t-%c -%s - start in console out mode\n",
			 ADAPTER_HELPER__CONSOLE_START_ARG,
			 ADAPTER_HELPER__CONSOLE_START_COMMAND );
	fprintf( stderr, "\t-%c -%s - no demonized at no interactive mode\n",
			 ADAPTER_HELPER__NO_DAEMON_ARG,
			 ADAPTER_HELPER__NO_DAEMON_COMMAND );
	fprintf( stderr, "\t-%c -%s - no automatical start graphical interface if device support it\n",
			 ADAPTER_HELPER__NO_INTERFACE_ARG,
			 ADAPTER_HELPER__NO_INTERFACE_COMMAND );
	fprintf( stderr, "\t-%c -%s - write log to log daemon\n",
			 ADAPTER_HELPER__ALONE_ARG,
			 ADAPTER_HELPER__ALONE_COMMAND );
	fprintf( stderr, "\t-%c -%s Device - Type of adapter we run\n",
			 ADAPTER_HELPER__ADAPTER_DEVICE_ARG,
			 ADAPTER_HELPER__ADAPTER_DEVICE_COMMAND );
	fprintf( stderr, "\tDevice = {%s}\n",
			 DISPATCHER_LIB_HELPER__ADAPTER_DEVICE__LIST );
	fprintf( stderr, "\t-%c -%s File - get command from file\n",
			 ADPATER_HELPER__ADAPTER_INPUT_FILE_ARG,
			 ADAPTER_HELPER__ADAPTER_INPUT_FILE_COMMAND );
	fprintf( stderr, "\tFile = {%s; %s; %s}\n",
			 ADPATER_HELPER__ADAPTER_INPUT_FILE_MODE,
			 ADPATER_HELPER__ADAPTER_INPUT_FILE_STATUS,
			 ADPATER_HELPER__ADAPTER_INPUT_FILE_NETWORK );
	fprintf( stderr, "\t-%c -%s level - set kernel debug level %u-%u. Default %u\n",
			 ADAPTER_HELPER__KERNEL_DEBUG_LEVEL_ARG,
			 ADAPTER_HELPER__KERNEL_DEBUG_LEVEL_COMMAND,
			 ADAPTER_DEFAULT_KERNEL_DEBUG_LEVEL,
			 ADAPTER_RESQUE_KERNEL_DEBUG_LEVEL,
			 ADAPTER_DEFAULT_KERNEL_DEBUG_LEVEL );
}

/**
 * @brief ������� ������ �� ������ ��������� �� ��������
 *
 * @param dev ��������� �� ��������� ���������
 * @return void
 **/
void AdapterPrintConsoleHelp( dmdev_t *dev )
{
	fprintf( stderr, "%s",DELIMETR );
	fprintf( stderr, " For manage adapter press a key:\n" );
	fprintf( stderr, "\t%c - system info screen\n",
			 ADAPTER_KEY__USER_SYSTEM_INFO );
	fprintf( stderr, "\t%c - system info in global wail\n",
			 ADAPTER_KEY__WAIL_SYSTEM_INFO );
	fprintf( stderr, "\t%c - sleep time status\n",
			 ADAPTER_KEY__SLEEP_TIME_PRINT );
	fprintf( stderr, "\t%c - sleep time status in global wail\n",
			 ADAPTER_KEY__SLEEP_TIME_PRINT_WAIL );
	fprintf( stderr, "\t%c - view mode screen\n",
			 ADAPTER_KEY__VIEW_MODE_SCREEN );
	fprintf( stderr, "\t%c - view network screen\n",
			 ADAPTER_KEY__VIEW_NETWORK_SCREEN );
	fprintf( stderr, "\t%c - view network info\n",
			 ADAPTER_KEY__VIEW_NETWORK_INFO );
	fprintf( stderr, "\t%c - recorder status info\n",
			 ADAPTER_KEY__USER_RECORDER_STATUS );
	fprintf( stderr, "\t%c - recorder status info in global wail\n",
			 ADAPTER_KEY__USER_RECORDER_STATUS_WAIL );
	fprintf( stderr, "\t%c - switch to rescue mode for adapter network\n",
			 ADAPTER_KEY__RESCUE_MODE );
	fprintf( stderr, "\t%c - view acc status info\n",
			 ADAPTER_KEY__VIEW_ACCSTAT );
	fprintf( stderr, "\t%c - acc status info in global wail\n",
			 ADAPTER_KEY__VIEW_ACCSTAT_WAIL );
	fprintf( stderr, "\t%c - view device status info\n",
			 ADAPTER_KEY__VIEW_DEVSTAT );
	fprintf( stderr, "\t%c - view device events\n",
			 ADAPTER_KEY__INTERFACE_EVENT_PRINT );
	fprintf( stderr, "\t%c - view open handles\n",
			 ADAPTER_KEY__VIEW_HENDELS );
	fprintf( stderr, "\t%c - view open handles in global wail\n",
			 ADAPTER_KEY__VIEW_HENDELS_WAIL );
	fprintf( stderr, "\t%c - view httpd clients status\n",
			 ADAPTER_KEY__INTERFACE_SERVER_PRINT );
	fprintf( stderr, "\t%c - view httpd clients status in global wail\n",
			 ADAPTER_KEY__INTERFACE_SERVER_PRINT_WAIL );
	fprintf( stderr, "\t%c - increase debug level\n",
			 ADAPTER_KEY__INC_KERN_DEBUG );
	fprintf( stderr, "\t%c - decrease debug level\n",
			 ADAPTER_KEY__DEC_KERN_DEBUG );
	fprintf( stderr, "\t%c - ping proxy host\n",
			 ADAPTER_KEY__INTERFACE_PING );
	fprintf( stderr, "\t[0...9] - reset USB port N (power off and on)\n" );

	//���� � ���������� ���� LTE �� ����� ��������� �������
	if( dev->dmdev_lte.dmctrl_icontrol )
	{
		fprintf( stderr, "\t%c - send network command set band auto\n",			ADAPTER_KEY__NET_MODE_AUTO	);
		fprintf( stderr, "\t%c - send network command set band 3G\n",			ADAPTER_KEY__NET_MODE_3G	);
		fprintf( stderr, "\t%c - send network command set band LTE\n",			ADAPTER_KEY__NET_MODE_LTE	);
		fprintf( stderr, "\t%c - send network command start PPP conection\n",	ADAPTER_KEY__NET_PPP_CONNECT);
	}

	fprintf( stderr, "\t%c - exit from adapter\n",
			 ADAPTER_KEY__USER_EXIT );

	if( dev->dmdev_wifi.dmctrl_icontrol && iDMXXXUsbGetWiFiPort( dev ) )
	{
		//���������� ������������ WiFi � �������� ������� ������������ ��� ����
		fprintf( stderr, "\t%c - turn on WiFi USB port power\n", ADAPTER_KEY__WIFI_ON );
		fprintf( stderr, "\t%c - turn off WiFi USB port power\n",ADAPTER_KEY__WIFI_OFF );
	}

	if( dev->dmdev_lte.dmctrl_icontrol && iDMXXXUsbGetLTEPort( dev ) )
	{
		//���������� ������������ LTE � �������� ������� ������������ ��� ����
		fprintf( stderr, "\t%c - turn on LTE USB port power\n", ADAPTER_KEY__LTE_ON );
		fprintf( stderr, "\t%c - turn off LTE USB port power\n",ADAPTER_KEY__LTE_OFF );
	}

	if( dev->dmdev_recorder.dmctrl_icontrol && iDMXXXUsbGetRecorderPort( dev ) )
	{
		//���������� ������������ Recorder � �������� ������� ������������ ��� ����
		fprintf( stderr, "\t%c - turn on Recorder USB port power\n", ADAPTER_KEY__RECORDER_ON );
		fprintf( stderr, "\t%c - turn off Recorder USB port power\n",ADAPTER_KEY__RECORDER_OFF );
	}

	fprintf( stderr,"%s",DELIMETR );
}

// ������� ������ �� ������ ���������
// ��� �� ��� ���� ������
//����:
// Satatus - ��������� �� ���������� ������ ��������
void AdapterPrintConsoleFast( AdapterGlobalStatusStruct_t *Status )
{
	fprintf( stderr, "%s",DELIMETR );

	AdapterPrintCurrentNetwork( Status, &Status->CurrentNetwork );

	//���� � ��� �� ����� ���� ���������� � ��� ���� �������
	if( Status->AdapterDevice == DM3x5_DEVICE_HARDWARE_TYPE__CM )
	{
		AdapterPrintCurrentNetwork( Status, &Status->CurrentNetworkInterface );
	}

	char IPBuffer[ 1024 ];
	int lcc = DispatcherLibNetworkSprintfDeviceIP(
				  IPBuffer,
				  sizeof( IPBuffer ),
				  NULL,
				  NULL ); //��� �� �� ������� �������
	IPBuffer[ lcc ] = 0x00;
	fprintf( stderr, "%s", IPBuffer );
	fprintf( stderr, " Press F1 for print help screen\n" );
	fprintf( stderr, "%s",DELIMETR );
}

//������� �������// 	fprintf(stderr, "", Status->Net.status.ip); �������� �������� ��� ����������
//����:
// falg - ���������� �������� ��� ���
void AdapterPrintSystemInfoWail( int flag )
{
	fprintf( stderr, "%s",DELIMETR );
	fprintf( stderr, " System info wial screen is %s\n",
			 flag ? "On" : "Off" );
	fprintf( stderr, "%s",DELIMETR );
}

//������� ������� �������� ��������� � ������� ������� ���������
//��������
//����:
// Satatus - ��������� �� ���������� ������ ��������
//�������:
// 0 - ��� ��
// ����� 0 � ������ ������
int AdapterPrintCurrentNetwork( AdapterGlobalStatusStruct_t *Status, CurrentNetworkSettingsStruct_t *CurrentNetwork )
{
	char BuildBuffer[ 64 ];
	char StartBuffer[ 64 ];
	const char *Device = NULL;


	if( !Status )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	fprintf( stderr, "%s",DELIMETR );

	Device = DispatcherLibStringAdapterNameFromDevice(
				 Status->AdapterDevice );


	int i = 0;

	for( i = 0 ; i < NET_NETWORK_INTERFACES_COUNT; i ++ )
	{
		libnetcontrol_t *Net = &Status->Net[i];

		if( !Net->netlib )
			continue;

		fprintf( stderr,"\tConnection type: %s\n", AdapterNetworkGetConnectionTypeName( Net->net_lib_type, 0 ) );

		// ���� �������� � ������������ �����, �� �������� ���������
		if( Net->net_lib_type == nettype_wifista ||
			Net->net_lib_type == nettype_wifiap )
		{
			fprintf( stderr,
					 "\tSSID: %s\n",
					 Net->config.ssid );

			fprintf( stderr,
					 "\tEncryptType: %s\n",
					 Net->config.encription_type );

			if( strncmp( Net->config.encription_type, ENC_TYPE_NONE, 1 ) )
			{

				fprintf( stderr,
						 "\tEncryptCode: %s\n",
						 Net->config.encription_code );
			}

			if( !Net->config.login[0] )
			{
				fprintf( stderr,
						 "\tLogin:\n" );
			}

			else
			{
				fprintf( stderr,
						 "\tLogin: %s\n",
						 Net->config.login );
			}

			if( !Net->config.password[0] )
			{
				fprintf( stderr,
						 "\tPasswd: \n" );
			}

			else
			{
				fprintf( stderr,
						 "\tPasswd: %s\n",
						 Net->config.password );
			}

		}
	}

#if 0

	//���� ����������� ������������ - �� ��������!!!
	if( Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature & DM_DM_ENABLE_DYNAMIC_NET )
	{
		Status->AdapterSettings.Block0.AdapterSettings.Connect &= ~AXIS_SOFTADAPTER_CONNECT__ADAPTER_TYPE;
		Status->AdapterSettings.Block0.AdapterSettings.Connect |= Status->PollingModem.connecttype;
	}

	// ���� �������� � ������������ �����, �� �������� ���������
	if( ( Status->AdapterSettings.Block0.AdapterSettings.Connect & AXIS_SOFTADAPTER_CONNECT__ADAPTER_TYPE ) ==
			AXIS_SOFTADAPTER_CONNECT__ADAPTER_TYPE_WIFI )
	{
		fprintf( stderr,"\tConnection type: Wi-Fi\n" );
		fprintf( stderr,
				 "\tSSID: %s\n",
				 Status->Net.config.ssid );

		fprintf( stderr,
				 "\tEncryptType: %s\n",
				 Status->Net.config.encription_type );

		if( strncmp( Status->Net.config.encription_type, ENC_TYPE_NONE, 1 ) )
		{

			fprintf( stderr,
					 "\tEncryptCode: %s\n",
					 Status->Net.config.encription_code );
		}

		if( !Status->Net.config.login )
		{
			fprintf( stderr,
					 "\tLogin:\n" );
		}

		else
		{
			fprintf( stderr,
					 "\tLogin: %s\n",
					 Status->Net.config.login );
		}

		if( !Status->Net.config.password )
		{
			fprintf( stderr,
					 "\tPasswd: \n" );
		}

		else
		{
			fprintf( stderr,
					 "\tPasswd: %s\n",
					 Status->Net.config.password );
		}

	}

	// ������� �������� �� ethernet ������ 0
	else if( ( Status->AdapterSettings.Block0.AdapterSettings.Connect & AXIS_SOFTADAPTER_CONNECT__ADAPTER_TYPE ) ==
			 AXIS_SOFTADAPTER_CONNECT__ADAPTER_TYPE_ETH )
	{
		fprintf( stderr,"\tConnection type: Ethernet\n" );
	}

	else if( ( Status->AdapterSettings.Block0.AdapterSettings.Connect & AXIS_SOFTADAPTER_CONNECT__ADAPTER_TYPE ) ==
			 AXIS_SOFTADAPTER_CONNECT__ADAPTER_TYPE_MODEM )
	{
		fprintf( stderr,"\tConnection type: CDMA\n" );
	}

	else if( ( Status->AdapterSettings.Block0.AdapterSettings.Connect & AXIS_SOFTADAPTER_CONNECT__ADAPTER_TYPE ) ==
			 AXIS_SOFTADAPTER_CONNECT__ADAPTER_TYPE_LTE )
	{
		fprintf( stderr,"\tConnection type: LTE\n" );
	}

	else
	{
		fprintf( stderr,"\tConnection type: Unknown (mask %i)\n",
				 Status->AdapterSettings.Block0.AdapterSettings.Connect & AXIS_SOFTADAPTER_CONNECT__ADAPTER_TYPE );
	}

#endif
	fprintf( stderr,
			 "\n\tControlPort    = %d\n",
			 CurrentNetwork->ControlPort );
	fprintf( stderr,
			 "\tUploadPort     = %d\n",
			 CurrentNetwork->UploadPort );
	fprintf( stderr,
			 "\tMonitoringPort = %d\n",
			 CurrentNetwork->MonitoringPort );

	fprintf( stderr,
			 "\n\tRescue mode=%s\n",
			 Status->RescueMode ? "on" : "off" );

	if( CurrentNetwork->ClientMode )
	{
		fprintf( stderr,
				 "\tClientMode=on Proxi=%s\n",
				 CurrentNetwork->ServerHost );
	}

	else
	{
		fprintf( stderr, "\tClientMode=off\n" );
	}

	system_time_print_ini( BuildBuffer,
						   sizeof( BuildBuffer ),
						   system_time_translate( Status->BuildTime ) );

	system_time_print_ini( StartBuffer,
						   sizeof( StartBuffer ),
						   Status->AdapterStart );

	fprintf( stderr, "\nDispatcher build %s at %s, Device=%s SN=%s\n",
			 Status->Build,
			 BuildBuffer,
			 Device,
			 Status->CurrentNetwork.SN );
	fprintf( stderr, "Start at %s, System UpTime=%lld\n",
			 StartBuffer,
			 axis_get_uptime() );

	fprintf( stderr, "%s",DELIMETR );

	return 0;
}

//������� ������� �������� ��������� � ������� ���������
//��������
//����:
// Satatus - ��������� �� ���������� ������ ��������
//�������:
// 0 - ��� ��
// ����� 0 � ������ ������
int AdapterPrintCurrentDevice( AdapterGlobalStatusStruct_t *Status )
{
	char buff[ 1024 ];
	int ret;
	ret = AdapterIniFileSprintfStatusDevInfo( buff, sizeof( buff ), Status->AdapterDevice, Status );

	if( ret <=0 )
	{
		DBGERR( "%s:Cannot print device info\n", __func__ );
		return -1;
	}

	fprintf( stderr, "%s", buff );
	return 0;

}

//������� ������� �������� ������ IP ������� ��������� �� ��������
void AdapterPrintDeviceIP( void )
{

	char buffer[1024];

	int lcc = DispatcherLibNetworkSprintfDeviceIP( buffer, sizeof( buffer ), NULL, NULL );
	buffer[ lcc ] = 0x00;

	fputs( DELIMETR, stderr );
	fputs( buffer, stderr );
	fputs( DELIMETR, stderr );
}

/**
 * @brief �������, ������� �������� ���������� �� �������� �������
 *
 * @param Client - ��������� �� ��������� �������
 * @return void
 **/
static void AdapterPrintStderrHTTPClientInfo( axis_ssl_httpi_server_client *Client )
{
	char TimeBuffer[ 64 ];
	char IpBuffer[64];
	
	axis_sprint_ip( IpBuffer, Client->csock.ip );

	//������ ������ ����� ����� "Client HTTP/HTTPS [Pid] client control_name start_time tcount keep_alive"

	fprintf( stderr, "Client %s [%d] %s '%s' %s %s TCount=%d KeepAlive=%d socket=%d\n",
			 Client->csock.essl ? "HTTPS" : "HTTP",
			 Client->pid,
		  	 Client->csock.ip ? axis_sprint_ip( IpBuffer, Client->csock.ip ) : "",
			 Client->client,
			 Client->control_name,
			 Client->start_time ? system_time_print_ini( TimeBuffer, sizeof( TimeBuffer ), system_time_translate( Client->start_time ) ) : "----.--.-- --:--:--",
			 Client->tCount,
			 Client->keep_alive,
			 Client->csock.sock
		   );

	
}



/**
 * @brief �������, ������� �������� ���������� �� �������� ��� ���������� �������
 *
 * @param Server - ��������� �� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
static void AdapterPrintStderrHTTPClientsInfoServer( axis_ssl_httpi_server_client *Server )
{
	int i;
	//������ ������ ����� ����� Server [pid] name (aliase) mode host. Clients = %d

	axis_ssl_httpi_server_client *Clients = ( axis_ssl_httpi_server_client * )Server->clients;

	fprintf( stderr, "Server [%d] %s (%s) Mode=%s Clients = %d TCount=%08x %s%s\n",
			 Server->pid,
			 Server->dev_name,
			 Server->dev_alias,
			 Server->mode == AXIS_SSL_HTTPI_SERVER_ACTIVE_MODE ? "Active" : "Passive",
			 Server->clients_count,
			 Server->tCount,
		  	 Server->mode == AXIS_SSL_HTTPI_SERVER_PASSIVE_MODE ? "Host=" : "",
			 Server->mode == AXIS_SSL_HTTPI_SERVER_ACTIVE_MODE ? "" : Server->host );

	for( i=0; i< Server->clients_count; i++ )
	{
		AdapterPrintStderrHTTPClientInfo( &Clients[ i ] );
	}
}


//�������, ������� �������� �� ������� stderr - ���������� �� �������� ������� ������� ����� ��������
/**
 * @brief �������� � ������� ���������� �� �������� ����� ��������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - ����� 0 � ������ ������
 **/
int AdapterPrintStderrHTTPClientsInfo( AdapterGlobalStatusStruct_t *Status )
{
	int print = 0;

	if( !Status )
	{
		DBGERR( "%s: Invalid argument\n", __func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	//� ����������� �� � ��� ����� ���� ���� ��� ��� ������� � ���� Qt ������

	if( Status->HTTPDMain.Server.pid > 0 )
	{
		//� ��� ���� ������ ������
		fprintf( stderr, "-------------------- Main ----------------------\n" );
		AdapterPrintStderrHTTPClientsInfoServer( &Status->HTTPDMain.Server );
		print++;
	}

	if( Status->HTTPDSec.Server.pid > 0 )
	{
		fprintf( stderr, "-------------------- Sec -----------------------\n" );
		AdapterPrintStderrHTTPClientsInfoServer( &Status->HTTPDSec.Server );
		print++;
	}


	if( Status->HTTPDQt.Server.pid > 0 )
	{
		fprintf( stderr, "--------------------- Qt -----------------------\n" );
		AdapterPrintStderrHTTPClientsInfoServer( &Status->HTTPDQt.Server );
		print++;
	}

	if( !print )
	{
		DBGERR( "%s: No HTTPd server in system\n", __func__ );
	}

	else
	{
		fputs( DELIMETR, stderr );
	}

	return 0;
}

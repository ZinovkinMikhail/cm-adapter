//������� ������� �������� ������ ��� ����� ����������� �� ������ ����

//������: 2010.09.15 14:16
//�����: ��������� ����� ��� "����"
//�����: i.podkolzin@ross-jsc.ru

#include <axis_sprintf.h>

#include <dispatcher_lib/dispatcher_lib_resource.h>
#include <dispatcher_lib/getopt.h>

#include <adapter/resource.h>
#include <adapter/printf_ini_files.h>
#include <adapter/recorder.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
//#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif


//������� ������� �������� ����� ��� ����� ����������� ��
//������ ��������
//����:
// b - ��������� �� ������ ���� �������� ��������� ������
// bsize - ������ ������� ������
// Status - ��������� �� ��������� �� �������� ��������
//�������:
// ���������� ���� ���������� � �����
int AdapterIniFileSprintfStatusDeviceList( char *b, size_t bsize,
                AdapterGlobalStatusStruct_t *Status )
{

	int cc = 0;
	int lcc = 0;
	size_t size = bsize;

	int devicenum = 0;

	if ( !b || bsize <= 0 || !Status )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return 0;
	}

	if( !(Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature & DM_DM_RECORDER_NOT_RECORDER) )
	{
		devicenum=1;;
	}

	DBGLOG("%s: %i Dev num = %i\n",__func__,__LINE__,devicenum);

	lcc = axis_sprintf_s( b + cc,  size, DISPATCHER_LIB_INTERFACE_INI_SECTION_DEVICE_LIST );

	cc	   += lcc;
	size   -= lcc;

	if ( size <= 0 ) goto end;
	if (  Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature & DM_DM_WORK_SEPARATE ||( Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature & DM_DM_RECORDER_NOT_RECORDER  ))
	{
		if(( (Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature & DM_DM_RECORDER_NOT_RECORDER) ) && !Status->Recorder.recorderscount )
		{
			devicenum++;
		}
		//���������� ���������
		lcc = axis_sprintf_d( b + cc, size,  DISPATCHER_LIB_INTERFACE_INI_NAME_DEVICE_LIST_COUNT, Status->Recorder.recorderscount + devicenum );//���� 1 �� �������� ��� ��� �������
		cc	   += lcc;
		size   -= lcc;

		if ( size <= 0 ) goto end;

		if( (Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature & DM_DM_RECORDER_NOT_RECORDER) && Status->Recorder.recorderscount > 0)
		{
			devicenum++; 
		}
		//�������� ����� ��������
		lcc = axis_sprintf_shablon_t( b + cc, size,
		                              DISPATCHER_LIB_INTERFACE_INI_NAME_001_DEVICE_LIST_ALIAS_MASK,
		                              devicenum,  Status->CurrentNetwork.Alias );

		cc	   += lcc;

		size   -= lcc;

		if ( size <= 0 ) goto end;
		//�������� �����
		lcc = axis_sprintf_shablon_t( b + cc, size,
		                              DISPATCHER_LIB_INTERFACE_INI_NAME_001_DEVICE_LIST_SN_MASK,
		                              devicenum,  Status->CurrentNetwork.SN );

		cc	   += lcc;

		size   -= lcc;

		if ( size <= 0 ) goto end;
		//�������� ����������
		lcc = axis_sprintf_shablon_t( b + cc, size,
		                              DISPATCHER_LIB_INTERFACE_INI_NAME_001_DEVICE_LIST_NAME_MASK,
		                              devicenum, DispatcherLibStringAdapterNameFromDevice( Status->GlobalDevice_t.dmdevice_type ) );

		cc	   += lcc;

		size   -= lcc;

		if ( size <= 0 ) goto end;

		devicenum++;
		
	}
		if(! (Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature & DM_DM_RECORDER_NOT_RECORDER) )
		{
			lcc = AdapterRecorderSprintDeviceLists( b + cc,   size,  &Status->Recorder, devicenum );
	
			cc	   += lcc;
			size   -= lcc;

			if ( size <= 0 ) goto end;
		}
	
end:

	b[ bsize - 1] = 0x00;

	return cc;

}

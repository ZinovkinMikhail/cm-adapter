//������� ������� �������� ��� ini ������  ���� c������� ������������


//�����: ��������� ����� ��� "����"
//�����: i.podkolzin@ross-jsc.ru
//������: 2010.09.16 13:26

#include <string.h>

#include <axis_sprintf.h>
#include <axis_pid.h>
#include <axis_time.h>

#include <dispatcher_lib/dispatcher_lib_resource.h>
#include <dispatcher_lib/network.h>
#include <dispatcher_lib/monitoring.h>
#include <dispatcher_lib/requests.h>
#include <dispatcher_lib/time_range.h>

#include <libcdma/axis_cdma.h>
#include <libcdma/usbmodem.h>
#include <libcdma/dmcmd.h>

#include <adapter/resource.h>
#include <adapter/httpd.h>
#include <adapter/network.h>
#include <adapter/printf_ini_files.h>


//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
//#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif
#include <axis_softrecorder.h>

int AdapterIniFileSprintfStatusGetCDMAStatus(DeviceCurrentStatus* dcs,  AdapterGlobalStatusStruct_t *Status  )
{
  	if(!dcs)
	{
	  	DBGERR("%s: Invalid argument\n",__func__);
		return -1;
	}
	
	int Present = 0;
	int InModem = 0;
	int NoDevice = 0;
	int fd;
	struct ROSS_INFO ri;
	struct struct_data sd;
	SoftRecorderExchangeConnectionTimeStruct ects;
	
	sd = system_time_get();
	;
	ross_check_cdma_device_entity( &Present, &InModem, &NoDevice );
		
	if( !Present )
	{
	  	dcs->DevStatus =  DEV_STATUS_APSENT;
		dcs->ConnectionLevel = 0;
		dcs->ConnectionQual = 0;
		return 0;
		
	}
	else if( Present && !InModem )
	{
	  	dcs->DevStatus = DEV_STATUS_DISCONNECTED;
		dcs->ConnectionLevel = 0;
		dcs->ConnectionQual =0;
		return 0;
	}
	else
	{
	  	dcs->DevStatus = DEV_STATUS_CONNECTED;
		
		fd = ross_cdma_dm_connect("/dev/ttyUSB2");
		
		if ( fd < 0 )
		{
			DBGERR("%s: Cannot open cdma device\n",__func__);
			return fd;
		}
						
		if ( ross_cdma_modem_info( fd, &ri ) < 0)
		{
			DBGERR("%s:Cannot get CDMA device status\n",__func__);
			ross_cdma_dm_disconnect(fd);
			return -1;
		}
		
		DispatcherLibRequestCreateExchange( (uint16_t *)&ects,AXIS_DISPATCHER_CMD_GET_CONNECTION_TIME ,sizeof(ects));
		
		 DispatcherLibExchange(  Status->GlobalDispatcher,
					&Status->GlobalUsbRecorderSemaphore,
					(uint16_t *)&ects,sizeof(ects));
		
		dcs->ConnectionLevel = ri.signal;
		dcs->ConnectionQual = ross_cdma_get_evdo_quality(fd);

		DispatcherLibTimeRange( &ects.Connecion_time, &sd, dcs->time, sizeof( dcs->time));
		
		ross_cdma_dm_disconnect(fd);	
		
	}
	return 0;
	
}


//������� ������� �������� ����� ��� ����� ����������� �� 
//������� ������ ����
//����:
// b - ��������� �� ������ ���� �������� ��������� ������
// bsize - ������ ������� ������
// adaptertype - ��� ��������
//�������:
// ���������� ���� ���������� � �����
int AdapterIniFileSprintfStatusDevInfo( char *b, size_t bsize, 
					int adaptertype, AdapterGlobalStatusStruct_t *Status )
{
  	if (!b  || bsize <= 0 || adaptertype < 0 )
	{        
	  	DBGERR("%s: Invalid arguments %p %i %x %p\n", __func__,b,(int)bsize,adaptertype,Status);
        return -1;
	}

	return AdapterNetworkPrintIniNetworkInfo(b,bsize,Status);

}

#include <axis_sprintf.h>
#include <axis_time.h>

#include <dispatcher_lib/dispatcher_lib_resource.h>
#include <dispatcher_lib/requests.h>

#include <adapter/resource.h>
#include <adapter/printf_ini_files.h>
#include <adapter/diagnostic.h>


//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
//#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif



int AdapterIniFileSprintfDiagnostic(  char *b, size_t bsize, AdapterGlobalStatusStruct_t* Status )
{
	int cc = 0;
	int lcc = 0;
	size_t size = bsize;

	if( !b || bsize <= 0 || !Status )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return 0;
	}

	// ��������� ������
	lcc = axis_sprintf_s(b+cc, size, DISPATCHER_LIB_INTERFACE_INI_SECTION_ADAPTER_DIAGNOSTIC );

	cc   += lcc;
	size   -= lcc;
	if( size <= 0 ) goto end;

	lcc = axis_sprintf_d64(
			b+cc,
		        size,
			DISPATCHER_LIB_INTERFACE_INI_NAME_ADAPTER_DIAGNOSTIC_START_GOOD,
			Status->AdapterDiagnostic.starts_good );

	cc   += lcc;
	size   -= lcc;
	if( size <= 0 ) goto end;

	lcc = axis_sprintf_d64(
			b+cc,
		        size,
			DISPATCHER_LIB_INTERFACE_INI_NAME_ADAPTER_DIAGNOSTIC_START_ERROR,
			Status->AdapterDiagnostic.starts_errored );

	cc   += lcc;
	size   -= lcc;
	if( size <= 0 ) goto end;

// 	lcc = axis_sprintf_d64(
// 			b+cc,
// 		        size,
// 			DISPATCHER_LIB_INTERFACE_INI_NAME_ADAPTER_DIAGNOSTIC_START_ERROR,
// 			Status->AdapterDiagnostic.starts_errored );
// 
// 	cc   += lcc;
// 	size   -= lcc;
// 	if( size <= 0 ) goto end;

	lcc = axis_sprintf_d64(
			b+cc,
		        size,
			DISPATCHER_LIB_INTERFACE_INI_NAME_ADAPTER_DIAGNOSTIC_SIGNAL_CATCH,
			Status->AdapterDiagnostic.signals_catched );

	cc   += lcc;
	size   -= lcc;
	if( size <= 0 ) goto end;

	lcc = axis_sprintf_d64(
			b+cc,
		        size,
			DISPATCHER_LIB_INTERFACE_INI_NAME_ADAPTER_DIAGNOSTIC_MEMERRORS,
			Status->AdapterDiagnostic.memerrors );

	cc   += lcc;
	size   -= lcc;
	if( size <= 0 ) goto end;

	lcc = axis_sprintf_d64(
			b+cc,
		        size,
			DISPATCHER_LIB_INTERFACE_INI_NAME_ADAPTER_DIAGNOSTIC_START_TOTAL,
			Status->AdapterDiagnostic.starts_total );

	cc   += lcc;
	size   -= lcc;
	if( size <= 0 ) goto end;

	lcc = axis_sprintf_d64(
			b+cc,
		        size,
			DISPATCHER_LIB_INTERFACE_INI_NAME_ADAPTER_DIAGNOSTIC_INTERFACE_CRASHED,
			Status->AdapterDiagnostic.interface_crashed );

	cc   += lcc;
	size   -= lcc;
	if( size <= 0 ) goto end;


	lcc = axis_sprintf_s(b+cc, size, DISPATCHER_LIB_INTERFACE_INI_SECTION_RECORDER_DIAGNOSTIC );

	cc   += lcc;
	size   -= lcc;
	if( size <= 0 ) goto end;

	lcc = axis_sprintf_d64(
			b+cc,
		        size,
			DISPATCHER_LIB_INTERFACE_INI_NAME_RECORDER_DIAGNOSTIC_REQUESTS_GOOD,
			Status->RecorderDiagnostic.requests_good );

	cc   += lcc;
	size   -= lcc;
	if( size <= 0 ) goto end;

	lcc = axis_sprintf_d64(
			b+cc,
		        size,
			DISPATCHER_LIB_INTERFACE_INI_NAME_RECORDER_DIAGNOSTIC_REQUESTS_ERROR,
			Status->RecorderDiagnostic.requests_errored );

	cc   += lcc;
	size   -= lcc;
	if( size <= 0 ) goto end;

	
end:
	b[ bsize - 1 ] = 0x00;
	return cc;	
}

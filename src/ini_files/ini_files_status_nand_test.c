#include <axis_sprintf.h>
#include <axis_time.h>

#include <dispatcher_lib/dispatcher_lib_resource.h>
#include <dispatcher_lib/requests.h>

#include <adapter/resource.h>
#include <adapter/printf_ini_files.h>


//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
//#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif


/**
 * @brief ������� ������� �������� ����� ��� ����� ����������� �� ������ ����� �����
 * 
 * @param b ��������� �� ������ ���� �������� ��������� ������
 * @param bsize ������ ������� ������
 * @param TestNandStatus ��������� �� ���� ��������� �� ������� ������� ������
 * @return ���������� ���� ���������� � �����
 */
int AdapterIniFileSprintfNandTestGetLog(  char *b, size_t bsize, DspRecorderExchangeFlashTestStatusStruct_t* TestNandStatus )
{
	int cc = 0;
	int lcc = 0;
	size_t size = bsize;
	int i;
	
	if( !b || bsize <= 0 || !TestNandStatus )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return 0;
	}
	
	// ��������� ������
	lcc = axis_sprintf_s(b+cc, size, DISPATCHER_LIB_INTERFACE_INI_SECTION_NAND );
	
	cc   += lcc;
	size   -= lcc;
	if( size <= 0 ) goto end;
	
	uint16_t ChipCount = TestNandStatus->ChipNum;
	
	// ���������� �����
	lcc = axis_sprintf_d( 
			b+cc,
		        size,
			DISPATCHER_LIB_INTERFACE_INI_NAME_NAND_COUNT,
			ChipCount );
	
	cc   += lcc;
	size   -= lcc;
	if( size <= 0 ) goto end;
	
	// TestResult
	switch( TestNandStatus->TestResult )
	{
		case NAND_TEST_ERROR:
		{
			lcc = axis_sprintf_t(
				b+cc,
				size,
				DISPATCHER_LIB_INTERFACE_INI_NAME_NAND_TEST_RESULT,
				DISPATCHER_LIB_INTERFACE_INI_NAME_NAND_TEST_RESULT_ERROR );
			
			cc   += lcc;
			size   -= lcc;
			if( size <= 0 ) goto end;
			
			break;
		}
		case NAND_TEST_OK:
		{
			lcc = axis_sprintf_t(
				b+cc,
				size,
				DISPATCHER_LIB_INTERFACE_INI_NAME_NAND_TEST_RESULT,
				DISPATCHER_LIB_INTERFACE_INI_NAME_NAND_TEST_RESULT_OK );
			
			cc   += lcc;
			size   -= lcc;
			if( size <= 0 ) goto end;
			
			break;
		}
		case NAND_TEST_WARNING:
		{
			lcc = axis_sprintf_t(
				b+cc,
				size,
				DISPATCHER_LIB_INTERFACE_INI_NAME_NAND_TEST_RESULT,
				DISPATCHER_LIB_INTERFACE_INI_NAME_NAND_TEST_RESULT_WARNING );
			
			cc   += lcc;
			size   -= lcc;
			if( size <= 0 ) goto end;
			
			break;
		}
		case NAND_TEST_ERASE:
		{
			lcc = axis_sprintf_t(
				b+cc,
				size,
				DISPATCHER_LIB_INTERFACE_INI_NAME_NAND_TEST_RESULT,
				DISPATCHER_LIB_INTERFACE_INI_NAME_NAND_TEST_RESULT_ERASE );
			
			cc   += lcc;
			size   -= lcc;
			if( size <= 0 ) goto end;
			
			break;
		}
		case NAND_TEST_WRITING:
		{
			lcc = axis_sprintf_t(
				b+cc,
				size,
				DISPATCHER_LIB_INTERFACE_INI_NAME_NAND_TEST_RESULT,
				DISPATCHER_LIB_INTERFACE_INI_NAME_NAND_TEST_RESULT_WRITING );
			
			cc   += lcc;
			size   -= lcc;
			if( size <= 0 ) goto end;
			
			break;
		}
		case NAND_TEST_WRITTEN:
		{
			lcc = axis_sprintf_t(
				b+cc,
				size,
				DISPATCHER_LIB_INTERFACE_INI_NAME_NAND_TEST_RESULT,
				DISPATCHER_LIB_INTERFACE_INI_NAME_NAND_TEST_RESULT_WRITTEN );
			
			cc   += lcc;
			size   -= lcc;
			if( size <= 0 ) goto end;
			
			break;
		}
		case NAND_TEST_READING:
		{
			lcc = axis_sprintf_t(
				b+cc,
				size,
				DISPATCHER_LIB_INTERFACE_INI_NAME_NAND_TEST_RESULT,
				DISPATCHER_LIB_INTERFACE_INI_NAME_NAND_TEST_RESULT_READING );
			
			cc   += lcc;
			size   -= lcc;
			if( size <= 0 ) goto end;
			
			break;
		}
		case NAND_TEST_ABORTED:
		{
			lcc = axis_sprintf_t(
				b+cc,
				size,
				DISPATCHER_LIB_INTERFACE_INI_NAME_NAND_TEST_RESULT,
				DISPATCHER_LIB_INTERFACE_INI_NAME_NAND_TEST_RESULT_ABORTED );
			
			cc   += lcc;
			size   -= lcc;
			if( size <= 0 ) goto end;
			
			break;
		}
		case NAND_TEST_READY:
		{
			lcc = axis_sprintf_t(
				b+cc,
				size,
				DISPATCHER_LIB_INTERFACE_INI_NAME_NAND_TEST_RESULT,
				DISPATCHER_LIB_INTERFACE_INI_NAME_NAND_TEST_RESULT_READY );
			
			cc   += lcc;
			size   -= lcc;
			if( size <= 0 ) goto end;
			
			break;
		}
		// ���� ���-�� ������, ������� ����� Hz
		default:
		{
			lcc = axis_sprintf_t(
				b+cc,
				size,
				DISPATCHER_LIB_INTERFACE_INI_NAME_NAND_TEST_RESULT,
				DISPATCHER_LIB_INTERFACE_INI_NAME_NAND_TEST_RESULT_ERROR );
			
			cc   += lcc;
			size   -= lcc;
			if( size <= 0 ) goto end;
			
			break;
		}
			
	}
	
	// UncorrectableErrors
	lcc = axis_sprintf_d(
			b+cc,
			size,
			DISPATCHER_LIB_INTERFACE_INI_NAME_NAND_TEST_UNCORRECTABLE_ERRORS,
			TestNandStatus->UncorrectableErrors );
	
	cc   += lcc;
	size   -= lcc;
	if( size <= 0 ) goto end;
	
	// ChipID
	lcc = axis_sprintf_d(
			b+cc,
			size,
			DISPATCHER_LIB_INTERFACE_INI_NAME_NAND_TEST_CHIP_ID,
			TestNandStatus->ChipID );
	
	cc   += lcc;
	size   -= lcc;
	if( size <= 0 ) goto end;
	
	// �������� ���� ������ ���������� ��� ���� �����
	for (i = 0; i < ChipCount; i++ )
	{
		// CorrEcc
		lcc = axis_sprintf_shablon_d(
				b+cc,
				size,
				DISPATCHER_LIB_INTERFACE_INI_NAME_NAND_CORR_ECC,
				i+1,
				TestNandStatus->NandStat[i].CorrEcc );
		
		cc   += lcc;
		size   -= lcc;
		if( size <= 0 ) goto end;
		
		// ErrorEraseHigh
		lcc = axis_sprintf_shablon_d(
				b+cc,
				size,
				DISPATCHER_LIB_INTERFACE_INI_NAME_NAND_ERROR_ERASE_HIGH,
				i+1,
				TestNandStatus->NandStat[i].ErrorEraseHigh );
		
		cc   += lcc;
		size   -= lcc;
		if( size <= 0 ) goto end;
		
		// ErrorEraseLow
		lcc = axis_sprintf_shablon_d(
				b+cc,
				size,
				DISPATCHER_LIB_INTERFACE_INI_NAME_NAND_ERROR_ERASE_LOW	,
				i+1,
				TestNandStatus->NandStat[i].ErrorEraseLow );
		
		cc   += lcc;
		size   -= lcc;
		if( size <= 0 ) goto end;
		
		// ErrorFactoryHigh
		lcc = axis_sprintf_shablon_d(
				b+cc,
				size,
				DISPATCHER_LIB_INTERFACE_INI_NAME_NAND_ERROR_FACTORY_HIGH,
				i+1,
				TestNandStatus->NandStat[i].ErrorFactoryHigh );
		
		cc   += lcc;
		size   -= lcc;
		if( size <= 0 ) goto end;
		
		// ErrorFactoryLow
		lcc = axis_sprintf_shablon_d(
				b+cc,
				size,
				DISPATCHER_LIB_INTERFACE_INI_NAME_NAND_ERROR_FACTORY_LOW,
				i+1,
				TestNandStatus->NandStat[i].ErrorFactoryLow );
		
		cc   += lcc;
		size   -= lcc;
		if( size <= 0 ) goto end;
		
		// HiByteRead
		lcc = axis_sprintf_shablon_d(
				b+cc,
				size,
				DISPATCHER_LIB_INTERFACE_INI_NAME_NAND_HI_BYTE_READ,
				i+1,
				TestNandStatus->NandStat[i].HighByteRead );
		
		cc   += lcc;
		size   -= lcc;
		if( size <= 0 ) goto end;
		
		// LowByteRead
		lcc = axis_sprintf_shablon_d(
				b+cc,
				size,
				DISPATCHER_LIB_INTERFACE_INI_NAME_NAND_LOW_BYTE_READ,
				i+1,
				TestNandStatus->NandStat[i].LowByteRead );
		
		cc   += lcc;
		size   -= lcc;
		if( size <= 0 ) goto end;
		
		// UncorErrors
		lcc = axis_sprintf_shablon_d(
				b+cc,
				size,
				DISPATCHER_LIB_INTERFACE_INI_NAME_NAND_UNCOR_ERRORS,
				i+1,
				TestNandStatus->NandStat[i].UncorrectErrors );
		
		cc   += lcc;
		size   -= lcc;
		if( size <= 0 ) goto end;
		
		// WriteError
		lcc = axis_sprintf_shablon_d(
				b+cc,
				size,
				DISPATCHER_LIB_INTERFACE_INI_NAME_NAND_WRITE_ERROR,
				i+1,
				TestNandStatus->NandStat[i].WriteError );
		
		cc   += lcc;
		size   -= lcc;
		if( size <= 0 ) goto end;
	}
	

end:
	b[ bsize - 1 ] = 0x00;
	return cc;	
}

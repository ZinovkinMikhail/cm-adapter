//������� ������� �������� ������ ���������� �� Alarms

//������: 2010.09.15 14:16
//�����: ��������� ����� ��� "����"
//�����: i.podkolzin@ross-jsc.ru

#include <axis_sprintf.h>
#include <axis_time.h>

#include <dispatcher_lib/dispatcher_lib_resource.h>
#include <dispatcher_lib/requests.h>

#include <adapter/resource.h>
#include <adapter/recorder.h>
#include <adapter/printf_ini_files.h>

#include <dmxxx_lib/dmxxx_calls.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif


//������� ������� �������� ����� ��� ����� ����������� �� 
//������ ������������ �������
//����:
// b - ��������� �� ������ ���� �������� ��������� ������
// bsize - ������ ������� ������
// Status - ��������� �� ��������� �� �������� ��������
// AdapterDevice - ��� ���������� �� ������� ��� ��� ���������
//�������:
// ���������� ���� ���������� � �����
int AdapterIniFileSprintfStatusAlarm( char *b, size_t bsize, 
					AdapterGlobalStatusStruct_t *Status,
					int AdapterDevice,struct Recorder_s *recorder )
{

    int cc = 0;
    int lcc = 0;
    size_t size = bsize;
	int i;
	int j;
	DBGLOG("%s: %i PRINT\n",__func__,__LINE__);

	if( !b || bsize <= 0 || !Status )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return 0;
	}
	DBGLOG("%s: %i PRINT %x\n",__func__,__LINE__,AdapterDevice);
	
	//���������� ��������� �������� �������

	//������� �� � ��� �� ���� - ���������� �������
	int AlarmCount = 0;

//TODO - �� ����� ���� ���� ������� DMXXXSensorCount �������
//���������� ��� ������� �� ��������
	switch( AdapterDevice )
	{
		case OMAP3530_DEVICE_HARDWARE_TYPE__ARK:
		case XXXXXXXX_DEVICE_HARDWARE_TYPE__GMK:
		case XXXXXXXX_DEVICE_HARDWARE_TYPE__DUMMY:
		case IMX8M_DEVICE_HARDWARE_TYPE__Y4C:
			AlarmCount = 0;
			break;
		case DM355_DEVICE_HARDWARE_TYPE__CDMA:
		case DM355_DEVICE_HARDWARE_TYPE__R2:
		{
			AlarmCount = 2;
			break;
		}
		case DM365_DEVICE_HARDWARE_TYPE__BANJO:
		{
			AlarmCount = 3;
			break;
		}
		case DM365_DEVICE_HARDWARE_TYPE__BANJO2:
		case DM365_DEVICE_HARDWARE_TYPE__BRT:
		case OMAP4430_DEVICE_HARDWARE_TYPE__BR3:
		case OMAP4430_DEVICE_HARDWARE_TYPE__YWG1:
		case OMAP3530_DEVICE_HARDWARE_TYPE__YWG2:
		case OMAP4430_DEVICE_HARDWARE_TYPE__BR4M:
		case OMAP3530_DEVICE_HARDWARE_TYPE__BR4W:
		{
			AlarmCount = 3;
			break;
		}		
		case DM355_DEVICE_HARDWARE_TYPE__UD2:
		case DM355_DEVICE_HARDWARE_TYPE__220:
		case DM3x5_DEVICE_HARDWARE_TYPE__CM:
		{
			AlarmCount = 4;
			break;
		}
		default:
		{
			DBGERR("%s: Invalid AdapterDevice=%d\n",__func__, AdapterDevice );
			return 0;
		}
	}


	if(!AlarmCount)
	{
		return 0;
	}

	    lcc = axis_sprintf_s(
							b+cc,
							size,
							DISPATCHER_LIB_INTERFACE_INI_SECTION_ALARM );
	cc	   += lcc;
	size   -= lcc;
	if( size <= 0 ) goto end;
	DBGLOG("%s: %i PRINT\n",__func__,__LINE__);


	//���������� ���������
	lcc = axis_sprintf_d(
			  b+cc,
			  size,
			  DISPATCHER_LIB_INTERFACE_INI__ALARM_COUNT,
			  AlarmCount ); //���� �� ������ �� �����
	cc	   += lcc;
	size   -= lcc;

	if( size <= 0 ) goto end;

	DBGLOG( "%s: %i PRINT\n",__func__,__LINE__ );

	//�� ����� �� ���� ������ �� ����� �� ������� ���������
	//� ����� ��������� 
	// 00X_Alarm = {Disable, on, off}
	//	Disable - �������� � ����������
	//	on - ���� ������������ 
	//  off - �� ���� ������������
	// 00X_Alarm.Time - ����� ����� ��������� ������������

	//��� ������� � �������
	uint16_t IOAlarm = Status->AdapterSettings.Block0.AdapterSettings.IOAlarmUSB;

	//���������� ������ ��� 
		DBGLOG("%s: %i PRINT\n",__func__,__LINE__);

	//����� ��� ������� �������� �������
	uint16_t 	AlarmMask 	= 0x0003; //���� �� 2 ����
	//�������������� ����� �� �������� ������� 
	int 		AlarmShift 	= 4; //������� 4 ���� �������� IO

	struct_data *AlarmTime = NULL;	//����� ����� ��������� ������� ������� �������
	DBGLOG("%s: %i PRINT\n",__func__,__LINE__);


	char *Value = NULL; //����� �������� ���������� � ������ ������
	struct  struct_data dates[10];
	struct  struct_data maxdate;
	int alarmevents = 0;
	int alarmindex[10] = {0};
	int maxindex = 0;
	int ree;

	for( i=0; i< AlarmCount; i++ )
	{
		DBGLOG("%s: %i PRINT\n",__func__,__LINE__);

		AlarmTime = NULL;
	
		if( !((IOAlarm >> AlarmShift) & AlarmMask) )
		{	DBGLOG("%s: %i PRINT\n",__func__,__LINE__);

			//���������
			Value = DISPATCHER_LIB_INTERFACE_INI_VALUE_DISABLE;

			//��������� �� ���� �������� �� �������������, � ������� ����� �� 2, ��� �� �� ����������
			//������ ������
			if( i == 2 ) //2 - ��� ������ ������, �� ���� ������������
			{
				for( j=0; j < Status->ExchangeEvents.Events.EventCount; j++ )
				{
					if( Status->ExchangeEvents.Events.Events[j].Type == DISPATCHER_LIB_EVENT__ADXL &&
						Status->ExchangeEvents.Events.Events[j].Flag == 1 ) //TODO - ��� ������ - ���� ���� �������, ��� �� ��������� ����� SMS
																			//� ��� ���������, ������� �� ����� ����� ����� �� � �������
					{
						Status->ExchangeEvents.Events.Events[j].Flag = 2; //��� �� � ��������� ��� �� ���������� �������
					}
				}
			}
		}
		else
		{
			DBGLOG("%s: %i PRINT\n",__func__,__LINE__);

			Value = DISPATCHER_LIB_INTERFACE_INI_VALUE_OFF;
		
			//�������� - ������ ���� ����� �� �������� ����� ���� ������������
			for( j=0; j < Status->ExchangeEvents.Events.EventCount; j++ )
			{
				
				if( ( Status->ExchangeEvents.Events.Events[j].Type ==
						DISPATCHER_LIB_EVENT__SENSOR &&
						Status->ExchangeEvents.Events.Events[j].Value == i+1 &&
						!Status->ExchangeEvents.Events.Events[j].Value2 ) ||
					( i == 2 && //2 - ��� ������ ������, ������� ������������
					  Status->ExchangeEvents.Events.Events[j].Type == DISPATCHER_LIB_EVENT__ADXL &&
					  Status->ExchangeEvents.Events.Events[j].Flag < 2 ) ) //TODO - ���������, ���� ��� ��� ���� �� ��������
				{
					DBGLOG( "Register Alarm event index = %i\n",j );
					alarmindex[alarmevents] = j;
					dates[alarmevents] = Status->ExchangeEvents.Events.Events[j].Data;
					alarmevents++;
					//���� ������������
				}
			
			}
			if( alarmevents > 1)
			{	
				maxdate = dates[0];
				  
				for(j = 0; j < alarmevents; j++)
				{
				    ree = system_times_compare_no_sec(maxdate, dates[j]);
				    if ( ree <= 0)
				    {
						maxdate = dates[j];
						maxindex = alarmindex[j];
				    }  
				}

				Value = DISPATCHER_LIB_INTERFACE_INI_VALUE_ON;
				AlarmTime = &Status->ExchangeEvents.Events.Events[maxindex].Data;	/*&maxdate;		*/	
				
			}
			else if ( alarmevents > 0)
			{
				DBGLOG("ALARM INDEX = %i   %i\n",alarmindex[alarmevents], alarmevents);
				Value = DISPATCHER_LIB_INTERFACE_INI_VALUE_ON;
				AlarmTime = &Status->ExchangeEvents.Events.Events[ alarmindex[0] ].Data;
			}
			else
			{
				Value = DISPATCHER_LIB_INTERFACE_INI_VALUE_OFF;
				//AlarmTime = &Status->ExchangeEvents.Events.Events[0].Data;			
			}
			
		}

		DBGLOG( "%s: %i PRINT\n",__func__,__LINE__ );

		lcc = axis_sprintf_shablon_t(
				b+cc,
				size,
				ADAPTER_INTERFACE_INI__ALARM_SHABLON_STATUS,
				i+1,
				Value );

		cc	   += lcc;
		size   -= lcc;
		if( size <= 0 ) goto end;
		
		DBGLOG( "%s: %i PRINT\n",__func__,__LINE__ );

		if( AlarmTime )
		{
			char TimeBuffer[64];
			

			lcc = axis_sprintf_shablon_t(
					b+cc,
					size,
					ADAPTER_INTERFACE_INI__ALARM_SHABLON_STATUS_TIME,
					i+1,
					system_time_print_ini( TimeBuffer, sizeof( TimeBuffer ), *AlarmTime ) );
			
			cc	   += lcc;
			size   -= lcc;
			if( size <= 0 ) goto end;
			
		}

		DBGLOG( "%s: %i PRINT\n",__func__,__LINE__ );

/*
		//������ ���������� ��� �� ����� �� �����
		if( Status->ExchangeEvents.Alarm & AlarmValusMask )
		{
			Value = DISPATCHER_LIB_INTERFACE_INI_VALUE_HIGH;
		}
		else
		{
			Value = DISPATCHER_LIB_INTERFACE_INI_VALUE_LOW;
		}

		lcc = axis_sprintf_shablon_t(
				b+cc,
				size,
				ADAPTER_INTERFACE_INI__ALARM_SHABLON_STATUS_VALUE,
				i+1,
				Value );
				
		cc	   += lcc;
		size   -= lcc;
		if( size <= 0 ) goto end;
 */		
		AlarmShift += 2; //�������� �� 2 ���
		alarmevents =0;
//		AlarmValusMask = AlarmValusMask << 1;

	}
	DBGLOG("%s: %i PRINT\n",__func__,__LINE__);

	if(recorder)
	{	DBGLOG("%s: %i PRINT\n",__func__,__LINE__);

		lcc = AdapterRecorderPrintAlarmIni(recorder,b+cc, size);
		cc	   += lcc;
		size   -= lcc;
		if( size <= 0 ) goto end;
	}
	DBGLOG("%s: %i PRINT\n",__func__,__LINE__);

	//���� �������� IO => IO.Input = {on off}
	
	//���� �� ������ �������� ����� � ��������� ���������
	//��� ��� ��� ��������� ��������� - 3���� ��� ������ �����������
	//� ��� ���������, �������� �� ������� �� ����� ���, ��� ��� 1 � 2 ���������, � ������ - ������ �����������
	switch( AdapterDevice )
	{
		case DM365_DEVICE_HARDWARE_TYPE__BANJO:
		case DM365_DEVICE_HARDWARE_TYPE__BANJO2:
		case DM365_DEVICE_HARDWARE_TYPE__BRT:
		case OMAP4430_DEVICE_HARDWARE_TYPE__BR3:
		{
			AlarmCount--; //��� ��� ��������� ������ ��� ������ ��������� ��� ������ �����������
			break;
		}		
		case OMAP4430_DEVICE_HARDWARE_TYPE__YWG1:
		case OMAP3530_DEVICE_HARDWARE_TYPE__YWG2:
		{
			AlarmCount = 0;	//��� ��� 2 ������� ���������, ��� �� �� �������� ������, � ������ - �����������
			break;
		}		
	}
	

	if( !AlarmCount ) //��� �� ���� ��������
	{
		b[ bsize - 1 ] = 0x00;
		return cc; 
	}
	
	//���������� ��������� �������� ������
    lcc = axis_sprintf_s( 
							b+cc,
							size,
							DISPATCHER_LIB_INTERFACE_INI_SECTION_IO );
    cc	   += lcc;
    size   -= lcc;
    if( size <= 0 ) goto end;

    //���������� ���������
    lcc = axis_sprintf_d(
							b+cc,
							size,
							DISPATCHER_LIB_INTERFACE_INI__IO_INPUT_COUNT, 
						    AlarmCount ); //���� �� ������ �� �����
    cc	   += lcc;
    size   -= lcc;
    if( size <= 0 ) goto end;

	DBGLOG("%s: %i PRINT\n",__func__,__LINE__);

	//Status->ExchangeEvents.Alarm = ret;
	// DM355_DEVICE_SENSOR__4_VALUE - �������� ����� ������� 4
	// DM355_DEVICE_SENSOR__3_VALUE - �������� ����� ������� 3
	// DM355_DEVICE_SENSOR__2_VALUE	- �������� ����� ������� 2
	// DM355_DEVICE_SENSOR__1_VALUE	- �������� ����� ������� 1

	// ����� ��� �� ��� - �� ������ �������� � ����������
	// ������� �������� ������ ��� ������� �������
	// ��� AlarmCount - ����� ���������� �������� ������� � ��� ����
	if( iDMXXXSensorGetState( &Status->GlobalDevice_t ) )
	{
		//������� ��������� �������� ��������� �������� ����������
		//������ �� �� � ����������
		int SensorState = 0;
		for( i=0; i< AlarmCount; i++ )
		{
			DBGLOG("%s: %i PRINT\n",__func__,__LINE__);
			SensorState = DMXXXSensorGetState(  &Status->GlobalDevice_t, i+1 );
			if( SensorState < 0 )
			{
				DBGERR( "%s: Can't get state for %d sensor from library\n", __func__, i );
				continue;
			}

			lcc = axis_sprintf_shablon_b(
							b+cc,
							size,
							DISPATCHER_LIB_INTERFACE_INI__IO_INPUT_SHABLON,
							i+1,
							SensorState );

			cc	   += lcc;
			size   -= lcc;
			if( size <= 0 ) goto end;
		}
	}
	else
	{
		//������� ��������� �������� ��������� �������� ��� � ��������
		//������ ��������� �������� ��������!
#ifdef TMS320DM365
	//����� �� �������� ������� ������ (�� ����)
	uint16_t 	AlarmValusMask = DM355_DEVICE_SENSOR__1_VALUE;
	DBGLOG("%s: %i PRINT\n",__func__,__LINE__);

	for( i=0; i< AlarmCount; i++ )
	{	DBGLOG("%s: %i PRINT\n",__func__,__LINE__);

		lcc = axis_sprintf_shablon_b(
				b+cc,
				size,
				DISPATCHER_LIB_INTERFACE_INI__IO_INPUT_SHABLON,
				i+1,
				Status->ExchangeEvents.Alarm & AlarmValusMask );
		
		cc	   += lcc;
		size   -= lcc;
		if( size <= 0 ) goto end;
			DBGLOG("%s: %i PRINT\n",__func__,__LINE__);

		AlarmValusMask = AlarmValusMask << 1;
	}
	DBGLOG("%s: %i PRINT\n",__func__,__LINE__);

#endif
	}

end:
	DBGLOG("%s: %i PRINT\n",__func__,__LINE__);

    b[ bsize - 1 ] = 0x00;
    return cc;

}

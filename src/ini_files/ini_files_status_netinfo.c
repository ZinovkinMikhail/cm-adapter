//������� ������� �������� ��� ini ������� �������� ����


//�����: ��������� ����� ��� "����"
//�����: i.podkolzin@ross-jsc.ru
//������: 2010.09.16 13:26

#include <axis_sprintf.h>
#include <axis_pid.h>

#include <dispatcher_lib/dispatcher_lib_resource.h>
#include <dispatcher_lib/network.h>

#include <adapter/resource.h>
#include <adapter/httpd.h>
#include <adapter/network.h>
#include <adapter/printf_ini_files.h>
#include <adapter/recorder.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
//#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif


/**
 * @brief ������� ������� �������� ����� ��� ����� ����������� �� ������� ������
 *
 * @param b - ��������� �� ����� ���� �������� ���������
 * @param bsize - ������ ������ ������
 * @param Status - ��������� �� ��������� ������� ��������
 * @param ClientID - ��������� �� ������ ��� ��� �������
 * @param Recorder - ��������� �� ����������, ���� ���������� ��� NULL
 * @return int - ���������� ���������� ������ � �����
 */
int AdapterIniFileSprintfStatusNetInfo( char *b, size_t bsize, 
					AdapterGlobalStatusStruct_t *Status,
					const char *ClientID, struct Recorder_s *Recorder )
{
	int cc = 0;
	int lcc = 0;
	size_t size = bsize;
	
	if( !b || !bsize || bsize >= SIZE_MAX || !Status )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return 0;
	}

	//������
	lcc = axis_sprintf_s( 
							b+cc, 
							size,
							DISPATCHER_LIB_INTERFACE_INI_SECTION_NET_INFO  );
	cc	   += lcc;
	size   -= lcc;
	if( size <= 0 ) goto end;

	//������ ���� ����������
	int client_count = 1;

	//����������� ���� �������� �������
	lcc = axis_ssl_httpi_server_sprintf_server_status( 
							b+cc,
							size, 
							&Status->HTTPDMain.Server,
							&client_count,
							0xFFFFFFFF );
	cc	   += lcc;
	size   -= lcc;
	if( size <= 0 ) goto end;
	
	//���� �� �������� � ������ ������� - �� ��������� ����� � ��� ���� ������ ������
	//� ���� �� �������� - �� �� ���������� � ��� ������
	if( Status->CurrentNetwork.ClientMode && Status->HTTPDSec.Server.pid > 0 )
	{
		//����������� ���� �������� �������
		lcc = axis_ssl_httpi_server_sprintf_server_status(
				  b+cc,
				  size,
				  &Status->HTTPDSec.Server,
				  &client_count,
				  0xFFFFFFFF );
		cc	   += lcc;
		size   -= lcc;

		if( size <= 0 ) goto end;

	}

	//���� ���� ����������, �� ��� ���� �� �������� ������ �����������
	if( Recorder )
	{
		lcc = AdapterRecorderPrintMonitoringUDPNetInfoStatusIni( Recorder, b+cc, size, &client_count );

		cc	   += lcc;
		size   -= lcc;
		if( size <= 0 ) goto end;
	}

	//����������� IP �����
	lcc = DispatcherLibNetworkSprintfDeviceIP(
							b+cc,
							size,
							DISPATCHER_LIB_INTERFACE_INI_NAME_SHABLON_NET_INFO_DEVICE_IP,
							DISPATCHER_LIB_INTERFACE_INI_NAME_NET_INFO_DEVICE_IP_COUNT );
	cc	   += lcc;
	size   -= lcc;
	if( size <= 0 ) goto end;
	
	//� ���������� �� ����������			
	lcc = axis_sprintf_d( 
							b+cc,
							size,  
							DISPATCHER_LIB_INTERFACE_INI_NAME_NET_INFO_SESSION_COUNT,
							client_count-1 ); //����� ���� ��� ��� �������� � 0
	cc	   += lcc;
	size   -= lcc;
	if( size <= 0 ) goto end;

	//� ���������� �� ������������ ����������
	lcc = axis_sprintf_d( 
							b+cc,
							size,
							DISPATCHER_LIB_INTERFACE_INI_NAME_NET_INFO_MAX_SESSION_COUNT,
							AdapterHTTPdGetMaxSessionCount( Status ) + 1 ); 
									//���� ���� ��� ��� �� UDP ���������� ���� ������
	cc	   += lcc;
	size   -= lcc;
	if( size <= 0 ) goto end;
	//����������� ���� �� ����� ��������������
	lcc = axis_sprintf_b( 
							b+cc,
							size,
							DISPATCHER_LIB_INTERFACE_INI_NAME_NET_INFO_RECOVERY, 
							Status->RescueMode );

    cc	   += lcc;
    size   -= lcc;
    if( size <= 0 ) goto end;

    //���������� ��������� �� ���� ������
    //�������� ����� �� ��� � ��������� ���� � ������ ��� � ������

    unsigned int client_ip = AdapterHttpdGetClientIP( axis_getpid(),
														Status, NULL );
    
    if( !client_ip || 
		!DispatcherLibNetworkAllowLocalNetwork( client_ip ) || (Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature & DM_DM_ENABLE_RESCUE_MODE &&  !Status->RescueMode ))
	{
		lcc = axis_sprintf_b( 
							b+cc, 
							size,
							DISPATCHER_LIB_INTERFACE_INI_NAME_NET_INFO_CHANGE_ENABLE, 
							0 );
	}
	else
	{
		lcc = axis_sprintf_b( 
							b+cc,
							size,
							DISPATCHER_LIB_INTERFACE_INI_NAME_NET_INFO_CHANGE_ENABLE, 
							1 );
	}
	
	cc	   += lcc;
	size   -= lcc;
	if( size <= 0 ) goto end;


    //��������� � �� � ������ �� �� ���������
    //���� ���������� ��������� ����� � ��� ������ ��������������
    if( ((!Status->RescueMode) && ( Status->AdapterSettings.Block0.AdapterSettings.Connect & AXIS_SOFTADAPTER_CONNECT__CLIENT_MODE ))

		|| ( !client_ip	|| !DispatcherLibNetworkAllowLocalNetwork( client_ip )) )
	{
		lcc = axis_sprintf_b( 
							b+cc,
							size,
							DISPATCHER_LIB_INTERFACE_INI_NAME_NET_INFO_CLIENT_MODE,
							1 );
	}
	else
	{
		lcc = axis_sprintf_b( 
							b+cc,
							size,
							DISPATCHER_LIB_INTERFACE_INI_NAME_NET_INFO_CLIENT_MODE,
							0 );
	}
	cc	   += lcc;
	size   -= lcc;
	if( size <= 0 ) goto end;

	//������ �� ������� ClientID
	lcc = axis_sprintf_t( 
							b+cc,
							size,
							DISPATCHER_LIB_INTREFACE_INI_NAME_NET_INFO_CLIENT_ID,
							ClientID );
	cc	   += lcc;
	size   -= lcc;
	if( size <= 0 ) goto end;

end:

	b[ bsize - 1 ] = 0x00;
	return cc;
}

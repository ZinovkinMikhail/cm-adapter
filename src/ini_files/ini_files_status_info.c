//������� ���������� �� ������ ������� ���������� �� �������

//������: 2010.09.16 14:55
//�����: ��������� ����� ��� "����"
//�����: i.podkolzin@ross-jsc.ru

#include <axis_sprintf.h>
#include <axis_pid.h>
#include <axis_uptime.h>

#include <dispatcher_lib/dispatcher_lib_resource.h>
#include <dispatcher_lib/network.h>
#include <dispatcher_lib/getopt.h>

#include <adapter/resource.h>
#include <adapter/printf_ini_files.h>
#include <adapter/recorder.h>


//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
//#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif


//������� ������� �������� ����� ��� ����� ����������� ��
//������� ���������� ��� ��������
//����:
// b - ��������� �� ����� ���� �������� ��������� ������
// bsize - ������ ������� ������
// Status - ��������� �� ��������� �� �������� ��������
//�������:
// ���������� ���� ���������� � �����
int AdapterIniFileSprintfStatusInfo( char *b, size_t bsize,
				AdapterGlobalStatusStruct_t *Status )
{

	int cc = 0;
	int lcc = 0;
	size_t size = bsize;
	char time_str[24];

	if ( !b || bsize <= 0 || !Status )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return 0;
	}

	//�������������� ������� �����
	struct_data ct = system_time_get();

	system_time_print_ini( time_str, sizeof( time_str ), ct );

	//��� �� ��� �� �������� ��� ������������ ���������� - ��� �������� ��� �������� � �����

	//��� ������
	lcc = axis_sprintf_s(
			b + cc,
			size,
			DISPATCHER_LIB_INTERFACE_INI_SECTION_INFO );

	cc	   += lcc;
	size   -= lcc;
	if ( size <= 0 ) goto end;

	//��� ����������
	lcc = axis_sprintf_t(
			b + cc,
			size,
			DISPATCHER_LIB_INTERFACE_INI_NAME_INFO_DEVICE_NAME,
			DispatcherLibStringAdapterNameFromDevice(
				Status->GlobalDevice_t.dmdevice_type ) );
	
	cc	   += lcc;
	size   -= lcc;
	if ( size <= 0 ) goto end;

	//��� �������� �����
	lcc = axis_sprintf_t(
			b + cc,
			size,
			DISPATCHER_LIB_INTERFACE_INI_NAME_INFO_DEVICE_SN,
			Status->CurrentNetwork.SN );

	cc	   += lcc;
	size   -= lcc;
	if ( size <= 0 ) goto end;

	//���������
	lcc = axis_sprintf_t(
			b + cc,
			size,
			DISPATCHER_LIB_INTERFACE_INI_NAME_INFO_DEVICE_ALIAS,
			Status->CurrentNetwork.Alias );

	cc	   += lcc;
	size   -= lcc;
	if ( size <= 0 ) goto end;

	//��������� ����� ��������
	lcc = axis_sprintf_t(
			b + cc,
			size,
			DISPATCHER_LIB_INTERFACE_INI_NAME_INFO_DEVICE_TIME,
			time_str );

	cc	   += lcc;
	size   -= lcc;
	if ( size <= 0 ) goto end;


	//��������� ����������� ������ ��������
	lcc = axis_sprintf_d64(
			b + cc,
			size,
			DISPATCHER_LIB_INTERFACE_INI_NAME_INFO_DEVICE_UP_TIME,
			axis_get_uptime() );

	cc		+= lcc;
	size	-= lcc;
	if ( size <= 0 ) goto end;

	//������
	lcc = axis_sprintf_t(
			b + cc,
			size,
			DISPATCHER_LIB_INTERFACE_INI_NAME_INFO_DEVICE_VERSION,
			Status->Build );

	cc	   += lcc;
	size   -= lcc;
	if ( size <= 0 ) goto end;

	//����� ������
	ct = system_time_translate( Status->BuildTime );

	system_time_print_ini( time_str, sizeof( time_str ), ct );

	lcc = axis_sprintf_t(
			b + cc,
			size,
			DISPATCHER_LIB_INTERFACE_INI_NAME_INFO_DEVICE_VERSION_DATE,
			time_str );

	cc	   += lcc;
	size   -= lcc;
	if ( size <= 0 ) goto end;

end:
	b[ bsize - 1 ] = 0x00;

	return cc;


}

//������� ���������� �� ������ ������ ��� ����� ������� ����

//������: 2010.09.15 15:33
//�����: ��������� ����� ��� "����"
//�����: i.podkolzin@ross-jsc.ru

#include <axis_sprintf.h>

#include <dispatcher_lib/dispatcher_lib_resource.h>

#include <adapter/resource.h>
#include <adapter/printf_ini_files.h>


//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
//#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif


//������� ������� �������� ����� ��� ����� ����������� �� 
//������� ������ ����
//����:
// b - ��������� �� ������ ���� �������� ��������� ������
// bsize - ������ ������� ������
// NotSave - ���� �� ����������� ������ ��� ������� ��������
//�������:
// ���������� ���� ���������� � �����
int AdapterIniFileSprintfStatusNet( char *b, size_t bsize, 
										int NotSave )
{

    int cc = 0;
    int lcc = 0;
    size_t size = bsize;
    
	if( !b || bsize <= 0  )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return 0;
	}

    lcc = axis_sprintf_s( 
							b+cc,
							size,
							DISPATCHER_LIB_INTERFACE_INI_SECTION_STATUS  );
    cc	   += lcc;
    size   -= lcc;
    if( size <= 0 ) goto end;

    //� ������� �� ������� ��� ����� ��������� ��������� ��� ��� ������
    if( NotSave < 0 )
	{
		lcc = axis_sprintf_t( 
							b+cc,
							size,
							DISPATCHER_LIB_INTERFACE_INI_NAME_STATUS_DATA, 
							DISPATCHER_LIB_INTERFACE_INI_VALUE_ERRORED );
    }
    else if( NotSave )
	{
		lcc = axis_sprintf_t( 
							b+cc,
							size,
							DISPATCHER_LIB_INTERFACE_INI_NAME_STATUS_DATA, 
							DISPATCHER_LIB_INTERFACE_INI_VALUE_ACCEPTED );
    }
    else
	{
		lcc = axis_sprintf_t( 
							b+cc,
							size,
							DISPATCHER_LIB_INTERFACE_INI_NAME_STATUS_DATA, 
							DISPATCHER_LIB_INTERFACE_INI_VALUE_SAVED );
    }

    cc	   += lcc;
    size   -= lcc;
    if( size <= 0 ) goto end;

end:
    b[ bsize - 1 ] = 0x00;
    return cc;

}


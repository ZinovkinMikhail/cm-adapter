//���������� �������
 #include <stdio.h>
 #include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <axis_error.h>
#include <axis_softrecorder.h>
#include <axis_dmlibs_resource.h>

#include <dmxxx_lib/dmxxx_calls.h>

#include <adapter/dmdevice.h>
#include <adapter/debug.h>
#include <adapter/global.h>
#include <adapter/leds.h>
#include <adapter/recorder.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif

/**
 * @brief ������� ���������� ��������� �����������
 * 
 * @param dev - ��������� ����������
 * @return int: 0 -OK, <0 - fail
 */
int AdapterLedsOn(dmdev_t *dev)
{
	if( !dev )
	{
		DBGERR("%s:%i: Invalid Argumentn\n",__func__,__LINE__);
		return -1;
	}

	if(!dev->dmdev_led.dmctrl_ipresent)
	{
		return 0;
	}

	//����� ���� ��� �������, ��� ���������� ���������� ��������������
	//����� ���������� - ������ �� ��� ���������
	if( dev->dmdev_led.dmctrl_feature & DM_LED_POWER_IN_RECORDER )
		return 0;

	DBGLOG( "%s: Try led mode on\n", __func__ );
 	return DMXXXLedModeOn(dev);
}

/**
 * @brief ������� ���������� ��������� �����������
 * 
 * @param dev - ��������� ����������
 * @return int: 0 -OK, <0 - fail
 */
int AdapterLedsOff(dmdev_t *dev)
{
	if( !dev )
	{
		DBGERR("%s:%i: Invalid Argumentn\n",__func__,__LINE__);
		return -1;
	}

	if(!dev->dmdev_led.dmctrl_ipresent)
	{
		return 0;
	}

	//����� ���� ��� �������, ��� ���������� ���������� ��������������
	//����� ���������� - ������ �� ��� ���������
	if( dev->dmdev_led.dmctrl_feature & DM_LED_POWER_IN_RECORDER )
		return 0;

	DBGLOG( "%s: Try led mode off\n", __func__ );
 	return DMXXXLedModeOff(dev);
}

//������� ��� ��������� �������� � ����������
//����:
// Status - ��������� �� ��������� ����������� ������� ����������
//�������:
// 0 �� ����� �����
int AdapterLedControlSettings( AdapterGlobalStatusStruct_t *Status )
{
	if ( !Status )
	{
		DBGERR( "%s: Invalid argument\n", __func__ );
		return -1;
	}
	if(!Status->GlobalDevice_t.dmdev_led.dmctrl_ipresent)
	{
		return 0;
	}

	//����� ���� ��� �������, ��� ���������� ���������� ��������������
	//����� ���������� - ������ �� ��� ���������
	if( Status->GlobalDevice_t.dmdev_led.dmctrl_feature & DM_LED_POWER_IN_RECORDER )
		return 0;

	DBGLOG("%s:%i: Led Control Settings\n", __func__, __LINE__ );
	
	switch ( Status->AdapterSettings.Block0.AdapterSettings.Power & AXIS_SOFTRECORDER_SETTINGS__POWER_LED_MODE )
	{
		
		case AXIS_SOFTRECORDER_SETTINGS__POWER_LED_MODE_ON:
		{
			DBGLOG("%s:%i: Led mode on\n", __func__, __LINE__ );
 			return DMXXXLedModeOn( &Status->GlobalDevice_t );
		}

		case AXIS_SOFTRECORDER_SETTINGS__POWER_LED_MODE_AUTO:
		{
			DBGLOG("%s:%i: Led mode auto\n", __func__, __LINE__ );
 			return DMXXXLedModeAuto(&Status->GlobalDevice_t, ADAPTER_LED_AUTO_TIME );
		}

		case AXIS_SOFTRECORDER_SETTINGS__POWER_LED_MODE_OFF:
		{
			DBGLOG("%s:%i: Led mode off\n", __func__, __LINE__ );
 			return DMXXXLedModeOff( &Status->GlobalDevice_t );
		}
		default:
		{
			DBGERR( "%s: Invalid led power settings 0x%04x\n", __func__, 
				Status->AdapterSettings.Block0.AdapterSettings.Power & AXIS_SOFTRECORDER_SETTINGS__POWER_LED_MODE );
			return DMXXXLedModeAuto(&Status->GlobalDevice_t, ADAPTER_LED_AUTO_TIME );
		}
	}

	return 0;
}

/**
 * @brief ������� �������� ������� �����������
 * 
 * @param Status - ��������� �� ���������� ��������� ����������
 * @param lednum - ����� ����������
 * @param color - ���� ����������
 * @param func - ��� ���������� ������� ��� �������
 * @return int: 0 -OK, <0 - fail
 */
int AdapterLedBlinkFast( AdapterGlobalStatusStruct_t *Status, int lednum, const char *color, const char *func )
{
	if( !Status || !color )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n",__func__,__LINE__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	DBGLOG( "%s: Try blink fast '%s'\n", func, color );

	// ���������� ���������� ������������ � ��� �� � ����������
	if( Status->GlobalDevice_t.dmdev_led.dmctrl_ipresent &&
			!( Status->GlobalDevice_t.dmdev_led.dmctrl_feature & DM_LED_IN_RECORDER ) )
	{
		return DMXXXLedFastBlink( &Status->GlobalDevice_t, color, lednum );
	}

	// ���������� ���������� � ���������� - ���� ��� � ������
	else if( Status->Recorder.next && Status->GlobalDevice_t.dmdev_led.dmctrl_feature & DM_LED_IN_RECORDER )
	{
		return AdapterRecorderLedFastBlink( Status->Recorder.next, color );
	}

	// ��� ��������� � � ������
	else
	{
		return 0;
	}
}

/**
 * @brief ������� ������� �����������
 *
 * @param Status - ��������� �� ���������� ��������� ����������
 * @param lednum - ����� ����������
 * @param color - ���� ����������
 * @param func - ��� ���������� ������� ��� �������
 * @return int: 0 -OK, <0 - fail
 */
int AdapterLedBlink( AdapterGlobalStatusStruct_t *Status, int lednum, const char *color, const char *func )
{
	if( !Status || !color )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n",__func__,__LINE__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	DBGLOG( "%s: Try blink '%s'\n", func, color );

	// ���������� ���������� ������������ � ��� �� � ����������
	if( Status->GlobalDevice_t.dmdev_led.dmctrl_ipresent &&
			!( Status->GlobalDevice_t.dmdev_led.dmctrl_feature & DM_LED_IN_RECORDER ) )
	{
		return DMXXXLedBlink( &Status->GlobalDevice_t, color, lednum );
	}

	// ���������� ���������� � ���������� - ���� ��� � ������
	else if( Status->Recorder.next && Status->GlobalDevice_t.dmdev_led.dmctrl_feature & DM_LED_IN_RECORDER )
	{
		return AdapterRecorderLedBlink( Status->Recorder.next, color );
	}

	// ���������� ���������� �����������
	else
	{
		return 0;
	}
}

/**
 * @brief ������� ��������� ����������
 * 
 * @param Status - ��������� �� ���������� ��������� ����������
 * @param lednum - ����� ����������
 * @param color - ���� ����������
 * @param func - ��� ���������� ������� ��� �������
 * @return int: 0 -OK, <0 - fail
 */
int AdapterLedOn( AdapterGlobalStatusStruct_t *Status, int lednum, const char *color, const char *func )
{
	if( !Status || !color )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n",__func__,__LINE__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	DBGLOG( "%s: Try on '%s'\n", func, color );

	// ���������� ���������� ������������ � ��� �� � ����������
	if( Status->GlobalDevice_t.dmdev_led.dmctrl_ipresent &&
			!( Status->GlobalDevice_t.dmdev_led.dmctrl_feature & DM_LED_IN_RECORDER ) )
	{
		return DMXXXLedOn( &Status->GlobalDevice_t, color, lednum );
	}

	// ���������� ���������� � ���������� - ���� ��� � ������
	else if( Status->Recorder.next && Status->GlobalDevice_t.dmdev_led.dmctrl_feature & DM_LED_IN_RECORDER )
	{
		return AdapterRecorderLedOn( Status->Recorder.next, color );
	}

	// ���������� ���������� �����������
	else
	{
		return 0;
	}
}

/**
 * @brief ������� ���������� ����������
 *
 * @param Status - ��������� �� ���������� ��������� ����������
 * @param lednum - ����� ����������
 * @param color - ���� ����������
 * @param func - ��� ���������� ������� ��� �������
 * @return int: 0 -OK, <0 - fail
 */
int AdapterLedOff( AdapterGlobalStatusStruct_t *Status, int lednum, const char *color, const char *func )
{
	if( !Status || !color )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n",__func__,__LINE__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	DBGLOG( "%s: Try off '%s'\n", func, color );

	// ���������� ���������� ������������ � ��� �� � ����������
	if( Status->GlobalDevice_t.dmdev_led.dmctrl_ipresent &&
			!( Status->GlobalDevice_t.dmdev_led.dmctrl_feature & DM_LED_IN_RECORDER ) )
	{
		return DMXXXLedOff( &Status->GlobalDevice_t, color, lednum );
	}

	// ���������� ���������� � ���������� - ���� ��� � ������
	else if( Status->Recorder.next && Status->GlobalDevice_t.dmdev_led.dmctrl_feature & DM_LED_IN_RECORDER )
	{
		return AdapterRecorderLedOff( Status->Recorder.next, color );
	}

	// ���������� ���������� �����������
	else
	{
		return 0;
	}
}

/**
 * @brief �������, ������� �������� �������������� ���������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @param LedNum - ����� ����������, � ������ ���������� �� ���� �� ����������
 * @param Colour - ���� ����������� ���������
 * @param FunctionName - ��� ���������� ������� ��� �������
 * @return int
 **/
static int AdapterLedFunctionalOn( AdapterGlobalStatusStruct_t *Status, int LedNum, 
								   const char *Colour, const char *FunctionName )
{
	if( !Status )
	{
		DBGERR( "%s: Invalid argument\n", FunctionName );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	//���� ���� ����� ��� ������������� - �� ��������
	if( Colour )
	{
		return AdapterLedOn( Status, LedNum, Colour, FunctionName );
	}

	else
	{
		DBGLOG( "%s: No led colour configured\n", FunctionName );
	}

	return 0;
}

/**
 * @brief �������, ������� ��������� �������������� ���������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @param LedNum - ����� ��������������� ����������
 * @param Colour - ���� ��������������� ����������, � ������ ���������� �� ���� �� �����������
 * @param FunctionName - ��� ���������� ������� ��� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
static int AdapterLedFunctionalOff( AdapterGlobalStatusStruct_t *Status, int LedNum, 
								   const char *Colour, const char *FunctionName )
{
	if( !Status )
	{
		DBGERR( "%s: Invalid argument\n", FunctionName );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	//���� ���� ����� ��� ������������� - �� ��������
	if( Colour )
	{
		return AdapterLedOff( Status, LedNum, Colour, FunctionName );
	}

	else
	{
		DBGLOG( "%s: No led colour configured\n", FunctionName );
	}

	return 0;
}

/**
 * @brief �������, ������� ������������ ������� �������� ��������������� ����������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @param LedNum - ����� ��������������� ����������
 * @param Colour - ���� ��������������� ����������, � ������ ���������� �� ���� �� �����������
 * @param FunctionName - ��� ���������� ������� ��� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
static int AdapterLedFunctionalBlinkFast( AdapterGlobalStatusStruct_t *Status, int LedNum, 
								   const char *Colour, const char *FunctionName )
{
	if( !Status )
	{
		DBGERR( "%s: Invalid argument\n", FunctionName );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	//���� ���� ����� ��� ������������� - �� ��������
	if( Colour )
	{
		return AdapterLedBlinkFast( Status, LedNum, Colour, FunctionName );
	}

	else
	{
		DBGLOG( "%s: No led colour configured\n", FunctionName );
	}

	return 0;
}

/**
 * @brief �������, ������� ������������ �������� ��������������� ����������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @param LedNum - ����� ��������������� ����������
 * @param Colour - ���� ��������������� ����������, � ������ ���������� �� ���� �� �����������
 * @param FunctionName - ��� ���������� ������� ��� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
static int AdapterLedFunctionalBlink( AdapterGlobalStatusStruct_t *Status, int LedNum, 
								   const char *Colour, const char *FunctionName )
{
	if( !Status )
	{
		DBGERR( "%s: Invalid argument\n", FunctionName );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	//���� ���� ����� ��� ������������� - �� ��������
	if( Colour )
	{
		return AdapterLedBlink( Status, LedNum, Colour, FunctionName );
	}

	else
	{
		DBGLOG( "%s: No led colour configured\n", FunctionName );
	}

	return 0;
}

/**
 * @brief �������, ������� �������� ��������� ��� �������� ������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
static int AdapterLedUploadDataOn( AdapterGlobalStatusStruct_t *Status )
{
	return AdapterLedFunctionalOn( Status, Status->LedUploadNum, Status->LedUploadColour, __func__ );
}

/**
 * @brief �������, ������� ��������� ��������� ��� �������� ������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
static int AdapterLedUploadDataOff( AdapterGlobalStatusStruct_t *Status )
{
	return AdapterLedFunctionalOff( Status, Status->LedUploadNum, Status->LedUploadColour, __func__ );
}

/**
 * @brief ������� ������������ � ���������/���������� �������� ������
 * 
 * @param command - �������:
 * 			1 - ��� ������������
 * 			0 - ���� ������������
 *
 * @return int: 0 -OK, <0 - fail
 */
int AdapterLedUpMonControl( uint16_t command)
{
	int ret;
	AdapterGlobalStatusStruct_t *Status = AdapterGlobalGetStatus();

	DBGLOG( "%s: Try control %04x\n", __func__, command );
	if ( command == 1 )
	{
		ret = AdapterLedUploadDataOn( Status );
		
		if( ret < 0 )
		{
			DBGERR( "%s: It wasn't possible to on led\n", __func__ );
			return ret;
		}
	}
	else if ( command == 0 )
	{
		ret = AdapterLedUploadDataOff( Status );
		
		if( ret < 0 )
		{
			DBGERR( "%s: Ity wasn't possibloe to on led\n", __func__ );
			return ret;
		}
	}

	return 0;
}

/**
 * @brief �������, ������� �������������� ��� ��������� �������� ������ �����
 *
 * @param Status - ��������� �� ���������� ��������� ������� ��������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterLedInit( AdapterGlobalStatusStruct_t *Status )
{
	// ����� ���� ������� ����� - �� ������ ���������� ����� ��������������� ������� ���������� ����������
	// ��� ��������� ������ � �������� �� � ���������� ���������
	// ���� ����� ��� - �� ��������� ������ ���� ��������� NULL

	dmdev_t *Dev = NULL;
	int ret;

	if( !Status )
	{
		DBGERR( "%s: Invalid argument\n", __func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	Dev = &Status->GlobalDevice_t;

	//��� ����� ������ ������� �� ���������� ����������
	if( iDMXXXLedGetFirmwareColour( Dev ) )
	{
		ret = DMXXXLedGetFirmwareColour( Dev, &Status->LedFirmwareNum, &Status->LedFirmwareColour );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't get firmware colour from device library\n", __func__ );
			Status->LedFirmwareColour = NULL;
		}

		else if( !Status->LedFirmwareColour )
		{
			DBGERR( "%s: Device not support firmware colour\n", __func__ );
		}
	}

	else
	{
		DBGERR( "%s: Device not support firmware colour\n", __func__ );
		Status->LedFirmwareColour = NULL;
	}

	if( iDMXXXLedGetHardwareColour( Dev ) )
	{
		ret = DMXXXLedGetHardwareColour( Dev, &Status->LedHardwareNum, &Status->LedHardwareColour );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't get hardware colour from device library\n", __func__ );
			Status->LedHardwareColour = NULL;
		}

		else if( !Status->LedHardwareColour )
		{
			DBGERR( "%s: Device not support hardware colour\n", __func__ );
		}

	}

	else
	{
		DBGERR( "%s: Device not support hardware colour\n", __func__ );
		Status->LedHardwareColour = NULL;
	}

	if( iDMXXXLedGetHeartBeatColour( Dev ) )
	{
		ret = DMXXXLedGetHeartBeatColour( Dev, &Status->LedHeartBeatNum, &Status->LedHeartBeatColour );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't get heart beat colour from device library\n", __func__ );
			Status->LedHeartBeatColour = NULL;
		}

		else if( !Status->LedHeartBeatColour )
		{
			DBGERR( "%s: Device not support heart beat connect colour\n", __func__ );
		}
	}

	else
	{
		DBGERR( "%s: Device not support heart beat connect colour\n", __func__ );
		Status->LedHeartBeatColour = NULL;
	}

	if( iDMXXXLedGetNetworkConnectColour( Dev ) )
	{
		ret = DMXXXLedGetNetworkConnectColour( Dev, &Status->LedNetworkConnectNum, &Status->LedNetworkConnectColour );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't get network connect colour from device library\n", __func__ );
			Status->LedNetworkConnectColour = NULL;
		}

		else if( !Status->LedNetworkConnectColour )
		{
			DBGERR( "%s: Device not support network connect colour\n", __func__ );
		}
	}

	else
	{
		DBGERR( "%s: Device not support network connect colour\n", __func__ );
		Status->LedNetworkConnectColour = NULL;
	}

	if( iDMXXXLedGetNetworkNoConnectionsColour( Dev ) )
	{
		ret = DMXXXLedGetNetworkNoConnectionsColour( Dev, &Status->LedNetworkNoConnectionNum, &Status->LedNetworkNoConnectionColour );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't get NoConnection colour from device library\n", __func__ );
			Status->LedNetworkNoConnectionColour = NULL;
		}

		else if( !Status->LedNetworkNoConnectionColour )
		{
			DBGERR( "%s: Device not support NoConnection colour\n", __func__ );
		}
	}

	else
	{
		DBGERR( "%s: Device not support NoConnection colour\n", __func__ );
		Status->LedNetworkNoConnectionColour = NULL;
	}

	if( iDMXXXLedGetRescueColour( Dev ) )
	{
		ret = DMXXXLedGetRescueColour( Dev, &Status->LedRescueNum, &Status->LedRescueColour );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't get rescue colour from device library\n", __func__ );
			Status->LedRescueColour = NULL;
		}

		else if( !Status->LedRescueColour )
		{
			DBGERR( "%s: Device not support rescue colour\n", __func__ );
		}
	}

	else
	{
		DBGERR( "%s: Device not support rescue colour\n", __func__ );
		Status->LedRescueColour = NULL;
	}

	if( iDMXXXLedGetUSBNoModemsColour( Dev ) )
	{
		ret = DMXXXLedGetUSBNoModemsColour( Dev, &Status->LedUSBNoModemsNum, &Status->LedUSBNoModemsColour );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't get USB no modem colour from device library\n", __func__ );
			Status->LedUSBNoModemsColour = NULL;
		}

		else if( !Status->LedUSBNoModemsColour )
		{
			DBGERR( "%s: Device not support USB no modem colour\n", __func__ );
		}
	}

	else
	{
		DBGERR( "%s: Device not support USB no modem colour\n", __func__ );
		Status->LedUSBNoModemsColour = NULL;
	}

	if( iDMXXXLedGetUploadColour( Dev ) )
	{
		ret = DMXXXLedGetUploadColour( Dev, &Status->LedUploadNum, &Status->LedUploadColour );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't get USB no modem colour from device library\n", __func__ );
			Status->LedUploadColour = NULL;
		}

		else if( !Status->LedUploadColour )
		{
			DBGERR( "%s: Device not support upload colour\n", __func__ );
		}
	}

	else
	{
		DBGERR( "%s: Device not support upload colour\n", __func__ );
		Status->LedUploadColour = NULL;
	}

	DBGLOG( "%s: LedFirmwareColour = '%s'\n", __func__, Status->LedFirmwareColour );
	DBGLOG( "%s: LedHardwareColou = '%s'\n", __func__, Status->LedHardwareColour );
	DBGLOG( "%s: LedHeartBeatColour = '%s'\n", __func__, Status->LedHeartBeatColour );
	DBGLOG( "%s: LedNetworkConnectColour = '%s'\n", __func__, Status->LedNetworkConnectColour );
	DBGLOG( "%s: LedNetworkNoConnectionColour = '%s'\n", __func__, Status->LedNetworkNoConnectionColour );
	DBGLOG( "%s: LedRescueColour = '%s'\n", __func__, Status->LedRescueColour );
	DBGLOG( "%s: LedUploadColour = '%s'\n", __func__, Status->LedUploadColour );
	DBGLOG( "%s: LedUSBNoModemsColour = '%s'\n", __func__, Status->LedUSBNoModemsColour );

	//���� �� ���������� �� ��������� ������� - ������ ��������
	//�� �� ��� �� �������� ��������� - ���� ���, �� ��� ����� �������� ��� ��������� � ���� �����
	if( Status->GlobalDevice_t.dmdevice_powerupstatus == DISPATCHER_LIB_EVENT__POWER_UP )
		AdapterLedsOn(&Status->GlobalDevice_t);

	return 0;
}

/**
 * @brief �������, ������� �������� ��������� ������������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterLedHeartBeatOn( AdapterGlobalStatusStruct_t *Status )
{
	return AdapterLedFunctionalOn( Status, Status->LedHeartBeatNum, Status->LedHeartBeatColour, __func__ );
}

/**
 * @brief �������, ������� ��������� ��������� ������������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterLedHeartBeatOff( AdapterGlobalStatusStruct_t *Status )
{
	return AdapterLedFunctionalOff( Status, Status->LedHeartBeatNum, Status->LedHeartBeatColour, __func__ );
}

/**
 * @brief �������, ������� ������������ �������� ����������� ������������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterLedHeartBeatBlink( AdapterGlobalStatusStruct_t *Status )
{
	return AdapterLedFunctionalBlinkFast( Status, Status->LedHeartBeatNum, Status->LedHeartBeatColour, __func__ );
}

/**
 * @brief �������, ������� �������� ��������� ��� ���������� ��������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterLedFirmwareOn( AdapterGlobalStatusStruct_t *Status )
{
	return AdapterLedFunctionalOn( Status, Status->LedFirmwareNum, Status->LedFirmwareColour, __func__ );
}

/**
 * @brief �������, ������� ��������� ��������� ��� ���������� ��������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterLedFirmwareOff( AdapterGlobalStatusStruct_t *Status )
{
	return AdapterLedFunctionalOff( Status, Status->LedFirmwareNum, Status->LedFirmwareColour, __func__ );
}

/**
 * @brief �������, ������� ������������ �������� ��� ���������� ��������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterLedFirmwareBlink( AdapterGlobalStatusStruct_t *Status )
{
	return AdapterLedFunctionalBlinkFast( Status, Status->LedFirmwareNum, Status->LedFirmwareColour, __func__ );
}

/**
 * @brief �������, ������� �������� ��������� ��� ������ � �������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterLedHardwareOn( AdapterGlobalStatusStruct_t *Status )
{
	return AdapterLedFunctionalOn( Status, Status->LedHardwareNum, Status->LedHardwareColour, __func__ );
}

/**
 * @brief �������, ������� ��������� ��������� ��� ����� � �������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterLedHardwareOff( AdapterGlobalStatusStruct_t *Status )
{
	return AdapterLedFunctionalOff( Status, Status->LedHardwareNum, Status->LedHardwareColour, __func__ );
}

/**
 * @brief �������, ������� ������������ �������� ��� ������ � �������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterLedHardwareBlink( AdapterGlobalStatusStruct_t *Status )
{
	return AdapterLedFunctionalBlinkFast( Status, Status->LedHardwareNum, Status->LedHardwareColour, __func__ );
}

/**
 * @brief �������, ������� �������� ��������� ��� ����������� � ����
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterLedNetworkConnectOn( AdapterGlobalStatusStruct_t *Status )
{
	return AdapterLedFunctionalOn( Status, Status->LedNetworkConnectNum, Status->LedNetworkConnectColour, __func__ );
}

/**
 * @brief �������, ������� ��������� ��������� ��� ���������� �� ����
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterLedNetworkConnecOff( AdapterGlobalStatusStruct_t *Status )
{
	return AdapterLedFunctionalOff( Status, Status->LedNetworkConnectNum, Status->LedNetworkConnectColour, __func__ );
}

/**
 * @brief �������, ������� ������������ �������� ���������� ��� ����������� � ����
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterLedNetworkConnectBlink( AdapterGlobalStatusStruct_t *Status )
{
	return AdapterLedFunctionalBlinkFast( Status, Status->LedNetworkConnectNum, Status->LedNetworkConnectColour, __func__ );
}

/**
 * @brief �������, ������� �������� ��������� ��� ���������� �������� �����������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterLedNetworkNoConnectionOn( AdapterGlobalStatusStruct_t *Status )
{
	return AdapterLedFunctionalOn( Status, Status->LedNetworkNoConnectionNum, Status->LedNetworkNoConnectionColour, __func__ );
}

/**
 * @brief �������, ������� ��������� ��������� ��� ���������� �������� ����������� (������� ������� ��� �������) 
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterLedNetworkNoConnectionOff( AdapterGlobalStatusStruct_t *Status )
{
	return AdapterLedFunctionalOff( Status, Status->LedNetworkNoConnectionNum, Status->LedNetworkNoConnectionColour, __func__ );
}

/**
 * @brief �������, ������� ������������ �������� ����������� ��� ���������� ���� 
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterLedNetworkNoConnectionBlink( AdapterGlobalStatusStruct_t *Status )
{
	return AdapterLedFunctionalBlink( Status, Status->LedNetworkNoConnectionNum, Status->LedNetworkNoConnectionColour, __func__ );
}

/**
 * @brief �������, ������� �������� ��������� ���� ��� �� ������ USB ������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterLedUSBNoModemsOn( AdapterGlobalStatusStruct_t *Status )
{
	return AdapterLedFunctionalOn( Status, Status->LedUSBNoModemsNum, Status->LedUSBNoModemsColour, __func__ );
}

/**
 * @brief �������, ������� ��������� ��������� ���� ��� �� ������ USB ������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterLedUSBNoModemsOff( AdapterGlobalStatusStruct_t *Status )
{
	return AdapterLedFunctionalOff( Status, Status->LedUSBNoModemsNum, Status->LedUSBNoModemsColour, __func__ );
}

/**
 * @brief �������, ������� ������������ �������� ���������� � ������ ���������� USB ������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterLedUSBNoModemsBlink( AdapterGlobalStatusStruct_t *Status )
{
	return AdapterLedFunctionalBlinkFast( Status, Status->LedUSBNoModemsNum, Status->LedUSBNoModemsColour, __func__ );
}

/**
 * @brief �������, ������� �������� ��������� ��� �������� � ����� ��������������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterLedRescueModeOn( AdapterGlobalStatusStruct_t *Status )
{
	return AdapterLedFunctionalOn( Status, Status->LedRescueNum, Status->LedRescueColour, __func__ );
}

/**
 * @brief �������, ������� ��������� ��������� ��� �������� � ����� ��������������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterLedRescueModeOff( AdapterGlobalStatusStruct_t *Status )
{
	return AdapterLedFunctionalOff( Status, Status->LedRescueNum, Status->LedRescueColour, __func__ );
}

/**
 * @brief �������, ������� ������������ �������� ����������� � ������ ��������������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterLedRescueModeBlink( AdapterGlobalStatusStruct_t *Status )
{
	return AdapterLedFunctionalBlinkFast( Status, Status->LedRescueNum, Status->LedRescueColour, __func__ );
}

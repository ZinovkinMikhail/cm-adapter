//������� ���������� LTE �������, ���� ������� �� ����

// ������: 2019.03.04 14:26
// �����: ��������� ����� �� "����" ������
// �����: i.podkolzin@ross-jsc.ru

#include <axis_error.h>

#include <dmxxx_lib/dmxxx_calls.h>

#include <adapter/global.h>
#include <adapter/network.h>
#include <adapter/lte.h>
#include <adapter/power.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
//#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif

/**
 * @brief �������, ������� ���������� ����������� � USB LTE ���������
 *
 * @param Dev - ��������� �� ��������� ����������
 * @param power - ���� ���������: 1 - ��������, 0 - ���������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterLTEPower( dmdev_t *Dev, int Power )
{
	int ret;
	int LTEPort;

	if( !Dev )
	{
		DBGERR( "%s: Invalid argument\n", __func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	if( !Dev->dmdev_usb.dmctrl_icontrol )
	{
		DBGERR( "%s: USB device not controled\n", __func__ );
		return -AXIS_ERROR_NOT_INIT;
	}

	if( !Dev->dmdev_lte.dmctrl_icontrol )
	{
		DBGERR( "%s: LTE device not controled\n", __func__ );
		return -AXIS_ERROR_NOT_INIT;
	}

	//��� �� ����� ��������/��������� WiFi - ��� ���� ����� ����� �����!!!
	//���� �� �������� �����������, �� ��� ���� ����������� �� �������
	//�� ���� �������� �����������, �� ��� ������ ���� ����� ����
	//����� ��� ����� ������� ���������� ���������� - ��� ��� ����� �� ����� ����� ��� ��� �� �����������

	if( !iDMXXXUsbGetLTEPort( Dev ) )
	{
		DBGERR( "%s: Functions for get LTE USB port not implementtion\n", __func__ );
		return -AXIS_ERROR_NOT_INIT;
	}

	LTEPort = DMXXXUsbGetLTEPort( Dev );

	if( LTEPort < 0 )
	{
		DBGERR( "%s: Can't get USB LTE port number from device library\n", __func__ );
		return LTEPort;
	}

	if( !LTEPort )
	{
		DBGERR( "%s: Device library not have USB LTE port\n", __func__ );
		return -AXIS_ERROR_INVALID_VALUE;
	}

	DBGLOG( "%s: Try power %s LTE modem on port %d\n", __func__, Power ? "on" : "off", LTEPort );

	ret = AdapterPowerUSB( Dev, LTEPort, Power );

	if( ret < 0 )
	{
		DBGERR( "%s: Can't power %s LTE USB adapter at port %d\n", __func__, Power ? "on" : "off", LTEPort );
	}

	return 0;
}

/**
 * @brief �������, ������� ���������� ������� ��� LTE ������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @param Command - ������� ������� ����� ���������� ������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 */
int AdapterLTESendCommand( AdapterGlobalStatusStruct_t *Status, uint32_t Command )
{
	libnetcontrol_t *LTENetwork;
	int ret;

	if( !Status || !Command )
	{
		DBGERR( "%s: Invalid argument\n", __func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	if( Status->GlobalDevice_t.dmdev_lte.dmctrl_icontrol != DM_CONTROL_ENABLE )
	{
		DBGERR( "%s: This device not supported LTE library\n", __func__ );
		return -AXIS_ERROR_NOT_INIT;
	}

	LTENetwork = AdapterNetworkGetLibControlByConnectionType( Status, nettype_lte );

	if( !LTENetwork )
	{
		DBGERR( "%s: No LTE network found\n", __func__ );
		return -AXIS_ERROR_INVALID_ANSED;
	}

	if( !LTENetwork->net_do_command )
	{
		DBGERR( "%s: LTE network not supported command\n", __func__ );
		return -AXIS_ERROR_INVALID_ANSED;
	}

	ret = LTENetwork->net_do_command( Command, NULL );

	if( ret < 0 )
	{
		DBGERR( "%s: Can't execute network command\n", __func__ );
	}

	return ret;
}

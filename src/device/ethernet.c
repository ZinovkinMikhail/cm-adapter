//���������� �������
 #include <stdio.h>
#include <unistd.h>
 #include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <net/if.h>
#include <net/if_arp.h>		// For ARPHRD_ETHER
#include <sys/ioctl.h>

#include <axis_error.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
//#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif


#ifdef JUST_NOT_USED
static int adapter_ethernet_downethinterface( char* ifname )
{
	if ( !ifname )
	{
		DBGERR( "%s:Invalid Argument\n", __func__ );
		return -1;
	}

	int fd;

	struct ifreq ifr;


	if (( fd = socket( AF_INET, SOCK_DGRAM, 0 ) ) < 0 )
	{
		return -AXIS_ERROR_CANT_CREATE_SOCKET;
	}

	memset(( char * )&ifr, 0, sizeof( struct ifreq	) );

	strncpy( ifr.ifr_name, ifname, sizeof( ifr.ifr_name ) );
	ifr.ifr_name[ sizeof( ifr.ifr_name ) - 1] = 0x00;

	if ( ioctl( fd, SIOCGIFFLAGS, &ifr ) < 0 )
	{
		close( fd );
		return -AXIS_ERROR_CANT_IOCTL_DEVICE;
	}

	ifr.ifr_flags &= ~( IFF_UP | IFF_RUNNING );

	//��������� �������

	if ( ioctl( fd, SIOCSIFFLAGS, &ifr ) < 0 )
	{
		close( fd );
		return -AXIS_ERROR_CANT_IOCTL_DEVICE;
	}

	close( fd );

	return 0;
}

static int adapter_ethernet_upethinterface( char* ifname )
{
	if ( !ifname )
	{
		DBGERR( "%s:Invalid Argument\n", __func__ );
		return -1;
	}

	int fd;

	struct ifreq ifr;


	if (( fd = socket( AF_INET, SOCK_DGRAM, 0 ) ) < 0 )
	{
		return -AXIS_ERROR_CANT_CREATE_SOCKET;
	}

	memset(( char * )&ifr, 0, sizeof( struct ifreq	) );

	strncpy( ifr.ifr_name, ifname, sizeof( ifr.ifr_name ) );
	ifr.ifr_name[ sizeof( ifr.ifr_name ) - 1] = 0x00;

	if ( ioctl( fd, SIOCGIFFLAGS, &ifr ) < 0 )
	{
		close( fd );
		return -AXIS_ERROR_CANT_IOCTL_DEVICE;
	}

	if (( ifr.ifr_flags & IFF_UP ) == IFF_UP )
	{
		close( fd );
		return 0;
	}

	ifr.ifr_flags |= ( IFF_UP | IFF_RUNNING );

	//��������� �������

	if ( ioctl( fd, SIOCSIFFLAGS, &ifr ) < 0 )
	{
		close( fd );
		return -AXIS_ERROR_CANT_IOCTL_DEVICE;
	}


	//��� ��� ��������� �� ����
	close( fd );

	return 0;
}


#endif

#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>
#include <errno.h>
#include <limits.h>
#include <string.h>

#if defined(OMAP3530) || defined(YOCTO)
 #include <linux/usb/ch9.h>
#else
 #include <usb.h>
#endif
#include <linux/usbdevice_fs.h>

#include <axis_find_from_file.h>
#include <axis_modules.h>
#include <axis_error.h>
#include <axis_time.h>
#include <axis_usb.h>

#include <dmxxx_lib/dmxxx_resource.h>
#include <dmxxx_lib/dmxxx_calls.h>

#include <dispatcher_lib/getopt.h>

#include <adapter/hardware.h>
#include <adapter/usb.h>
#include <adapter/network.h>
#include <adapter/power.h>
#include <adapter/module.h>
#include <adapter/timer.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif

#define MOUNT_TEST_FILE				"/tmp/mountflash/.testme001"
#define FS_MOUNT_COUNT				3
#define MOUNT_TEST_FILE_NAME		"testme001"

/**
 * @brief ���������� ����� USB ����� �� ������� ��������� �������
 *
 * @param drvname - ��������� �� ������ � ������ �����
 * @return int - ����� ����� �� ������� ������ �������, ����� 0 � ������ ������
 **/
int AdapterUsbPortNumber( const char *drvname )
{
	int USBBusses;
	uint16_t bus_number;
	int ret;

	if( !drvname )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	//��� ������ ���� ����� ���������� ��� USB �� ����������
	USBBusses = axis_usb_get_bus_count( );

	if( USBBusses < 0 )
	{
		DBGERR( "%s: Can't find USB bus number\n", __func__ );
		return USBBusses;
	}

	if( !USBBusses )
	{
		DBGERR( "%s: Np USB bus fund on host\n", __func__ );
		return -AXIS_ERROR_NO_DEVICE;
	}

	for( bus_number = 0; bus_number < USBBusses; bus_number++ )
	{
		ret = axis_usb_get_por_number_for_device_at_bus( bus_number, drvname );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't find device '%s' on USB bus %u\n", __func__, drvname, bus_number );
			return ret;
		}

		if( !ret )
		{
			continue;
		}

		DBGLOG( "%s: USB device '%s' was found at bus %u at port %d\n", __func__, drvname, bus_number, ret );
		break;
	}

	if( !ret)
	{
		DBGLOG( "%s: USB device '%s' not found on host (USB busses %d)\n", __func__, drvname, USBBusses );
	}

	return ret;
}

int AdapterUsbSwithInternal( dmdev_t *dev )
{
	if( !dev )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	if( dev->dmdev_usb.dmctrl_icontrol )
	{
		return DMXXXUsbSwitchInternal( dev );
	}

	return 0;
}

int AdapterUsbSwithExternal( dmdev_t *dev )
{
	if( !dev )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	if( dev->dmdev_usb.dmctrl_icontrol )
	{
		return DMXXXUsbSwitchExternal( dev );
	}

	return 0;
}


int AdapterUsbDeinitPolling( AxisUsbDesc_t *dev )
{
	char devpath[ PATH_MAX ];

	if( !dev )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	sprintf( devpath, USB_POLL_PATH_PATTERN, dev->port );
	
	if(dev->fdpoll > 0)
		close( dev->fdpoll );
	

	memset( dev, 0, sizeof( AxisUsbDesc_t ) );
	dev->fdpoll = -1;
	
	remove( devpath );

	return 0;
}

int AdapterUsbInitPolling( AxisUsbDesc_t *dev )
{
	char devpath[ PATH_MAX ];
	int fd;

	if( !dev )
	{
		DBGERR( "%s: Invalid Argument \n", __func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	if( dev->fdpoll > 0 )
	{
		DBGERR( "%s: Handel already opening (old handel = %d)\n", __func__, dev->fdpoll );
		return -1;
	}
	
	sprintf( devpath, USB_POLL_PATH_PATTERN, dev->port );
	remove( devpath );

	if( mknod( devpath, S_IFCHR | 0644, dev->dev )  < 0 )
	{
		DBGLOG( "%s: Can't create device for\n", __func__ );
		perror( "mknod" );
		return -1;
	}

	fd =  open( devpath, O_RDWR );

	if( fd < 0 )
	{
		DBGERR( "%s:It wasn't possible to open device for poll\n", __func__ );
		perror( "open" );
		remove( devpath );
		return -1;
	}

	dev->fdpoll = fd;

	DBGLOG( "%s:%i: FD Poll %i \n", __func__, __LINE__ ,dev->fdpoll);

	return 0;

}

int AdapterUsbResetPoll( libnetcontrol_t *Net )
{
	if( !Net )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	if( !Net->EnablePollingModem )
	{
		DBGERR( "%s:i: Pooling disabled for this network '%s'\n", __func__, AdapterNetworkGetConnectionTypeName( Net->net_lib_type, 0 ) );
		return -AXIS_ERROR_INVALID_VALUE;
	}

	struct usb_device_descriptor devdesc;

	if( read( Net->PollingModem.fdpoll, &devdesc, sizeof( struct usb_device_descriptor ) ) < 0 )
	{
		DBGERR( "%s: It wasn't possible to read dev desc (%s)\n", __func__, strerror( errno ) );

		close( Net->PollingModem.fdpoll );

		Net->PollingModem.fdpoll = -1;

		return -1;
	}

	return 0;
}


int AdapterUsbInit( dmdev_t *dev )
{
	if( !dev )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	if( dev->dmdev_usb.dmctrl_icontrol )
	{
		return AdapterUsbSwithInternal( dev );
	}

	return 0;
}

/**
 * @brief �������, ������� ������������� USB �������������
 *
 * @param dev - ��������� �� ��������� ���������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterUsbReSwitch( dmdev_t *dev )
{
	int ret;

	if( !dev )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n",__func__,__LINE__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	if( dev->dmdev_usb.dmctrl_icontrol )
	{
		ret = AdapterUsbSwithExternal( dev );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't switch USB to external\n", __func__ );
			return ret;
		}

		sleep( 2 );
		ret = AdapterUsbSwithInternal( dev );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't switch USB to internal\n", __func__ );
			return ret;
		}
	}

	return AXIS_NO_ERROR;
}

/**
 * @brief ������� ������������ �������, ����� �� ������ USB ������ �� ���� ������� � ������� ������������� �������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @param Dev  - ��������� �� ��������� �������� ���������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterUSBNoModemsAtMoreTime( AdapterGlobalStatusStruct_t *Status )
{
	int ret = 0;

	if( !Status )
	{
		DBGERR( "%s: Invalid Argument\n", __func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	//��������� ��� ���������� �� ����� �������, ������ � �.�.
	//� ���������� ����������
	if( iDMXXXUsbNoModemFound( &Status->GlobalDevice_t ) )
	{
		ret = DMXXXUsbNoModemFound( &Status->GlobalDevice_t, 0 );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't work no USB modems from device library function\n", __func__ );
		}
	}
	else
	{
		DBGERR( "%s: No one USB modems found on device, but no function for work it\n", __func__ );
	}

	return ret;
}

/**
 * @brief ������� ������������ �������, ����� �� ������ USB ������ �� ���� ������� - � ��� ����������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @param Dev  - ��������� �� ��������� �������� ���������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterUSBNoModemsFatal( AdapterGlobalStatusStruct_t *Status )
{
	int ret = 0;

	if( !Status )
	{
		DBGERR( "%s: Invalid Argument\n", __func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	//��������� ��� ���������� �� ����� �������, ������ � �.�.
	//� ���������� ����������
	if( iDMXXXUsbNoModemFound( &Status->GlobalDevice_t ) )
	{
		ret = DMXXXUsbNoModemFound( &Status->GlobalDevice_t, 1 ); //1 - ��� ��������� ������

		if( ret < 0 )
		{
			DBGERR( "%s: Can't work no USB modems from device library function\n", __func__ );
		}
	}
	else
	{
		DBGERR( "%s: No one USB modems found on device fatal, but no function for work it\n", __func__ );
	}

	return ret;
}

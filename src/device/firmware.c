//���������� �������
#include <stdio.h>
#include <stdlib.h>
#include <sys/vfs.h>
#include <sys/mount.h>
#include <sys/stat.h>
#include <errno.h>

#include <axis_error.h>
#include <axis_dmlibs_resource.h>
#include <axis_find_from_file.h>

#include <dispatcher_lib/getopt.h>
#include <dispatcher_lib/dispatcher_lib_resource.h>

#include <dmxxx_lib/dmxxx_calls.h>

#include <adapter/usb.h>
#include <adapter/module.h>
#include <adapter/leds.h>
#include <adapter/firmware.h>
#include <adapter/debug.h>
#include <adapter/dmdevice.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
//#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#include <unistd.h>
#else
#define DBGLOG( ... )
#endif


#define UIMAGE_PATH_PATTERN	"/tmp/mount/DM_%s/uImage"
#define VERSION_PATH_PATTERN	"/tmp/mount/DM_%s/version.ini"
#define RAMDISK_PATH_PATTERN	"/tmp/mount/DM_%s/rootdisk.%s"

#define UIMAGE_PATH_PATTERN_WIFI	"/tmp/uImage"
#define VERSION_PATH_PATTERN_WIFI	"/tmp/version.ini"
#define ROOTDISK_PATH_PATTERN_WIFI	"/tmp/rootdisk.%s"


static inline int adapter_firmware_check( void )
{

	int ret = axis_find_pattern_from_file( "/proc/scsi/usb-storage/0", "usb-storage" );

	if( ret < 0 )
	{
		DBGERR( "%s: It wosnt found USB storage\n", __func__ );
		return ret;
	}

	if( ret == 1 )
	{
		DBGLOG( "%s:%i: Found usb storage, try to start rewrite firmware\n",
																	__func__, __LINE__ );
		return 1;
	}

	DBGLOG( "%s: USB storage not found\n", __func__ );
	return 0;
}

/**
 * @brief ������� ��������� ����� ����������� ������, ���������� �/� Wi-Fi
 * 
 * @param dev - ��������� ����������
 * @param versionpath - ��������� �� ����� ���� ������� ���� � ����� version.ini
 * @param vsize - ������ ������� ������
 * @param uimagepath - ��������� �� ����� ���� ������� ���� � ����� uImage
 * @param usize  - ������ ������� ������
 * @param versionpath - ��������� �� ����� ���� ������� ���� � ����� ramdisk
 * @param rsize - ������ ������� ������
 * @return int
 */
static int AdapterFirmwareGetWifiPaths( dmdev_t *dev, char *versionpath, size_t vsize,
		char *uimagepath, size_t usize, char *ramdiskpath, size_t rsize )
{
	int ret;

	if( !dev || !versionpath || !vsize || !uimagepath || !usize || !ramdiskpath || !rsize )
	{
		DBGERR( "%s: Invalid argument\n", __func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	const char *ramdisknamearg = 
				DispatcherLibStringRootDiskAdapterNameFromDevice( dev->dmdevice_type );

	if( !ramdisknamearg )
	{
		DBGERR( "%s:%i: It wasn't possible to get devicename for ram disk\n",
																		__func__,__LINE__);
		return -AXIS_ERROR_INVALID_VALUE;
	}

	ret = snprintf( uimagepath, usize, UIMAGE_PATH_PATTERN_WIFI );

	if( ret < 0 || ret >= usize )
	{
		DBGERR( "%s: Can't create path to kernal file\n", __func__ );
		return -AXIS_ERROR_INVALID_VALUE;
	}

	ret = snprintf( ramdiskpath, rsize, ROOTDISK_PATH_PATTERN_WIFI, ramdisknamearg);

	if( ret < 0 || ret >= rsize )
	{
		DBGERR( "%s: Can't create path to ramdisk file\n", __func__ );
		return -AXIS_ERROR_INVALID_VALUE;
	}

	ret = snprintf( versionpath, vsize, VERSION_PATH_PATTERN_WIFI );

	if( ret < 0 || ret > vsize )
	{
		DBGERR( "%s: Can't create path to version file\n", __func__ );
		return -AXIS_ERROR_INVALID_VALUE;
	}

	return 0;
}

static int AdapterFirmwareGetUimagePath( dmdev_t *dev, char *path, size_t size )
{
	int ret;

	if( !dev || size < 256 )
	{
		DBGERR( "%s:%i: Invalid argument\n", __func__, __LINE__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
		
	}

	const char *devicename =
						DispatcherLibStringAdapterNameFromDevice( dev->dmdevice_type );

	if( !devicename )
	{
		DBGERR("%s:%i: It wasn't possible to get devicename\n",__func__,__LINE__);
		return -AXIS_ERROR_INVALID_VALUE;
	}

	ret = snprintf( path, size, UIMAGE_PATH_PATTERN, devicename );

	if( ret < 0 || ret >= size )
	{
		DBGERR( "%s: Can't create path to kernal file", __func__ );
		return -AXIS_ERROR_INVALID_VALUE;
	}

	return 0;
}

static int AdapterFirmwareGetRamdiskPath( dmdev_t *dev, char *path, size_t size )
{
	int ret;

	if(!dev || size < 256)
	{
		DBGERR( "%s:%i: Invalid argument\n", __func__, __LINE__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	const char *devicename = DispatcherLibStringAdapterNameFromDevice( dev->dmdevice_type );
	const char *ramdisknamearg = DispatcherLibStringRootDiskAdapterNameFromDevice( dev->dmdevice_type );
	
	if( !ramdisknamearg || !devicename )
	{
		DBGERR("%s:%i: It wasn't possible to get devicename\n", __func__, __LINE__ );
		return -AXIS_ERROR_INVALID_VALUE;
	}

	ret = snprintf( path, size, RAMDISK_PATH_PATTERN, devicename, ramdisknamearg );
	
	if( ret < 0 || ret >= size )
	{
		DBGERR( "%s: Can't create path to ramdisk file\n", __func__ );
		return -AXIS_ERROR_INVALID_VALUE;
	}

	return 0;
}

static int AdapterFirmwareVersionPath( dmdev_t *dev, char *path, size_t size )
{
	int ret;

	if( !dev || size < 256 )
	{
		DBGERR( "%s:%i: Invalid argument\n", __func__, __LINE__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	const char *devicename = DispatcherLibStringAdapterNameFromDevice( dev->dmdevice_type );

	if( !devicename )
	{
		DBGERR( "%s:%i: It wasn't possible to get devicename\n", __func__, __LINE__ );
		return -AXIS_ERROR_INVALID_VALUE;
	}

	ret = snprintf( path, size, VERSION_PATH_PATTERN, devicename );

	if( ret < 0 || ret >= size )
	{
		DBGERR( "%s: Can't create path to version file\n", __func__ );
		return -AXIS_ERROR_INVALID_VALUE;
	}

	return 0;
}

// �������, ������� ��������� USB-������, ������ �����
// ������������ ������ ��������
//
// @param dev - ��������� �� ��������� ����������
//
// @return int: 0 - OK, <0 - fail
static int AdapterFirmwareMount( dmdev_t *dev )
{
	int ret = 0;

	if( !dev )
	{
		DBGERR( "%s:%i: Invalid argument\n", __func__, __LINE__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	if( mkdir( "/tmp/mount", S_IRWXU |S_IRWXG | S_IROTH | S_IXOTH ) < 0 )
	{
	    DBGERR( "%s : Error cannot make dir '%s'\n", __func__, strerror( errno ) );
	    return -AXIS_ERROR_CANT_WRITE_DEVICE;
	}

	//������� �������� ������� ���������� ����� ��� ������� ������������
	sleep(3);

	int mountcount = 2;

	while( mountcount-- )
	{
		DBGLOG( "%s:%i: Try Mount\n", __func__, __LINE__ );

		ret = mount( "/dev/sda1", "/tmp/mount", "vfat", MS_RDONLY, NULL );

		if( errno == EBUSY )
		{
			ret = 0;
		}

		if( ret < 0 )
		{
			DBGERR( "%s:%i: It wasn't possible to mount device '%s' %s'\n", __func__, __LINE__,
					strerror( errno ), mountcount ? ". Try again ..." : "." );
			ret = -AXIS_ERROR_CANT_IOCTL_DEVICE;
			sleep(3);
			continue;
		}

		DBGLOG( "%s:%i: Mount Ok\n", __func__, __LINE__ );
		break;
	}

	return ret;
}

// ������� ���������� � ��������
// @detailed ��������� ����� ������ �������� �� ����� � ���������� �������
// ����������
//
// @param dev - ��������� �� ��������� ����������
// @param mode - ����� ��������:
//			0 - �/� usb
//			1 - �/� wifi
//
// @return int: 0 - OK, <0 - fail
int AdapterFirmwarePrepareGo( dmdev_t *dev, int mode )
{
	char uimagepath[256];
	char ramdiskpath[256];
	char versionpath[256];
	int ret;

	if( !dev )
	{
		DBGERR( "%s:%i: Invalid argument}\n", __func__, __LINE__ );
		return -1;
		
	}

	
	if( mode )
	{
		ret = AdapterFirmwareGetWifiPaths( dev, versionpath, sizeof( versionpath ),
											uimagepath, sizeof( uimagepath ),
											ramdiskpath, sizeof( ramdiskpath) );
		if( ret < 0 )
		{
			DBGERR( "%s: Can't get path for WiFi upgrade\n", __func__ );
			return ret;
		}
	}
	else
	{
		ret = AdapterFirmwareVersionPath( dev, versionpath, sizeof( versionpath ) );
		if( ret < 0 )
		{
			DBGERR( "%s:%i: It wasn't possible to get version path\n", __func__, __LINE__ );
			return ret;
		}

		ret = AdapterFirmwareGetUimagePath( dev, uimagepath, sizeof( uimagepath ) );
		if( ret < 0 )
		{
			DBGERR( "%s:%i: It wasn't possible to get uimage path\n", __func__, __LINE__ );
			return ret;
		}

		ret = AdapterFirmwareGetRamdiskPath( dev, ramdiskpath, sizeof( ramdiskpath ) );
		if( ret < 0 )
		{
			DBGERR( "%s:%i: It wasn't possible to get ramdisk path\n", __func__, __LINE__ );
			return ret;
		}
	}

	ret = AdapterFirmwareStart( dev, uimagepath, ramdiskpath, versionpath,
											dev->devfirmpath, DM_FIRM_FLAGS_CHECK_DATE );
	if( ret < 0 )
	{
		DBGERR( "%s:%i: It wasn't possible to upgrate firmware\n", __func__, __LINE__ );
		return ret;
	}

	return 0;
}

/**
 * @brief ������� ��������� ������ ��� ������������ ���������� ��������
 *
 * @param PrivateData - ��������� �� ���������� ��������� �������
 * @return void
 */
static void AdapterFirmwareHeartBeat( void *PrivateData )
{
	AdapterGlobalStatusStruct_t *Status = (AdapterGlobalStatusStruct_t *)PrivateData;

	if( !PrivateData )
	{
		DBGERR( "%s: Invalid argument\n", __func__ );
		return;
	}

	//��� ������ � ��������� ���� �� ����!
	if( AdapterLedHeartBeatBlink( Status ) )
	{
		DBGERR( "%s: Failed heart beat blink\n", __func__ );
	}

	//��� ���� ������� ����� ���������� ��������, ��� �� ��� ��������� �� �������
	if( AdapterDmDeviceWatchDogTick( &Status->GlobalDevice_t ) )
	{
		DBGERR( "%s:Failed reset watchdog\n", __func__ );
	}
}

/**
 * @brief ������� �������� � ���������� ������������ ����������� ��������, ������� ��������� � ������ ����������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @param Recorder - ��������� �� ��������� ������ ������ ����������
 * @return int - ����� 0 � ������ ������, 0 ���������� �� �� ���������, ������� �� ������ ���������� � ������ ��������� ���������� ��������
 */
int AdapterFirmwareCheckInitInRecorder( AdapterGlobalStatusStruct_t *Status, struct Recorder_s *RecorderHead )
{
	int ret = 0;
	struct Recorder_s *lRecorder;

	if( !Status || !RecorderHead )
	{
		DBGERR( "%s: Invalid argument\n", __func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	if( !RecorderHead->recorderscount || !RecorderHead->next )
	{
		DBGERR( "%s: No initing recorder on...\n", __func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	lRecorder = RecorderHead->next;

	if( Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature & DM_DM_ENABLE_RECORDER_FIRMWARE_UPDATE &&
		lRecorder->recorder_clear_firmware_status_flag && lRecorder->recorder_get_firmware_status_flag &&
		lRecorder->recorder_get_adapter_firmware )
	{
		//1 - ���������� ���� � ����������
		ret = lRecorder->recorder_get_firmware_status_flag( lRecorder->hen, &lRecorder->semaphore, &lRecorder->recorder_data);

		if( ret < 0 )
		{
			DBGERR( "%s: Can't get recorder firmware flag\n", __func__ );
			return ret;
		}

		if( !( ret & DM_RECORDER_FIRMWARE_OMAP ) )
		{
			DBGLOG( "%s: Recorder no have firmware in memory\n", __func__ );
			return ret;
		}

		//2 - ���������� �������� ������ ��� ������� ����!
		DBGLOG( "%s: Recorder have a new adapter firmware. Try update it...\n", __func__ );

		//��� ��� �� ��������� �������� - �� ������ ������� ���������
		AdapterLedsOn( &Status->GlobalDevice_t );

		//� ������� ��������������� ���������
		AdapterLedFirmwareOn( Status );


		//�������� �������� � ���������� � ��������� �� ��� ��������� USB �����
		ret = lRecorder->recorder_get_adapter_firmware( lRecorder->hen,
				&lRecorder->semaphore, &lRecorder->recorder_data, &lRecorder->lock,
				AdapterFirmwareHeartBeat, Status );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't get new firmware from recorder\n", __func__ );
			AdapterLedFirmwareOff( Status );
			return ret;
		}

		//�� ��!!! - � ��� �� ����� �� ��������� ���������??
		ret = AdapterFirmwarePrepareGo( &Status->GlobalDevice_t, 0 ); //0 - ��� ���� ���������� USB
		if( ret < 0 )
		{
			DBGERR("%s:%i: It wasn't possible to write new firmware\n",__func__,__LINE__);
			AdapterLedFirmwareOff( Status );
			return ret;
		}

		//�� ��������� ��������� - ��������� ���������
		AdapterLedFirmwareOff( Status );

		INFO_LOG( 0, "New firmware writed" );

		//���� �������� ���� ���������� � ������� ������ (�� ���� ��������)
		ret = lRecorder->recorder_clear_firmware_status_flag( lRecorder->hen, &lRecorder->semaphore, &lRecorder->recorder_data );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't clear recorder status flag\n", __func__ );
		}

		INFO_LOG( ret, "Recorder ready for work" );

		while( 1 )
		{
			//������������� �� ���, ��� �������� ������� ���������
			AdapterLedFirmwareBlink( Status );
			sleep( 1);
		}

	}
	else
	{
		DBGLOG( "%s: This hardware or recorder not support adapter firmware\n", __func__ );
	}

	return ret;
}

/**
 * @brief ������� �������� � ���������� ������������ ����������� ��������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - ����� 0 � ������ ������, 0 - ���������� ������������ ����������� �� ���������, ������� �� ������ ���������� � ������ ��������� ���������� ��������
 **/
int AdapterFirmwareCheckInit( AdapterGlobalStatusStruct_t *Status )
{
	int ret = 0;

	if( !Status )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	if( Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature & DM_DM_ENABLE_USB_FIRMWARE_UPDATE &&
		Status->GlobalDevice_t.dmdev_firm.dmctrl_icontrol &&
		!( Status->GlobalDevice_t.dmdev_firm.dmctrl_icontrol && DM_DM_ENABLE_RECORDER_FIRMWARE_UPDATE ) //���� ���������� �� ����� ���������� - �� ��� ��� ������ �� ����
	  )
	{

		ret = AdapterInsertModule( &Status->GlobalDevice_t.dmdev_firm );
		if( ret < 0 )
		{
			DBGERR( "%s: It wasn't posssible insert module for check new firmware\n", __func__ );
			goto exit;
		}

		ret = AdapterUsbSwithExternal( &Status->GlobalDevice_t );
		if( ret < 0 )
		{
			DBGERR( "%s: It wasn't possible to switch USB\n", __func__ );
			goto exit;
		}

		//��� �� ����� ���
		sleep(1);

		ret = adapter_firmware_check();
		if( ret < 0 )
		{
			DBGERR( "%s: It wasn't possible to check firmware\n", __func__ );
			goto exit;
		}

		if( !ret )
		{
			DBGERR( "%s: No flash device in\n", __func__ );
			goto exit;
		}

		//��� ��� �� ��������� �������� - �� ������ ������� ���������
		AdapterLedsOn( &Status->GlobalDevice_t );

		//� ������� ��������������� ���������
		AdapterLedFirmwareOn( Status );

		ret = AdapterFirmwareMount( &Status->GlobalDevice_t );
		if( ret < 0 )
		{
			DBGERR( "%s:%i: It wasn't possible to mount flash device\n", __func__, __LINE__ );
			goto exit;
		}

		ret = AdapterFirmwarePrepareGo( &Status->GlobalDevice_t, 0 );
		if( ret < 0 )
		{
			DBGERR( "%s:%i: It wasn't possible to write new firmware\n", __func__, __LINE__ );
			goto exit;
		}

		//�� ��������� ��������� - ��������� ���������
		AdapterLedFirmwareOff( Status );

		INFO_LOG( 0, "New firmware writed" );

		while(1)
		{
			//������������� �� ���, ��� �������� ������� ���������
			AdapterLedFirmwareBlink( Status );
			sleep( 1);
		}

		return ret;
	}

exit:
	AdapterLedsOff( &Status->GlobalDevice_t );

	//���� ��� �� �� ��������� - ��������� ���������
	AdapterLedFirmwareOff( Status );

	//������ �� ����� ���� ���� ��������� - ��� �� �� ������ ���������� ����
	if( AdapterRemoveModule( &Status->GlobalDevice_t.dmdev_firm ) < 0 )
	{
		DBGERR( "%s: Can't remove firmware modules\n", __func__ );
	}

	return ret;
}

//������� ��������� ��������
//
// @param dev - ��������� �� ��������� ����������
// @param uimagepath - ���� � ������ ����
// @param ramdiskpath - ���� � ������ ��������� �����
// @param versioninipath - ���� � ini ����� �������������
// @param flags - �������������� �����
//
// @return int: 0 - OK, <0 -fail
int AdapterFirmwareStart( dmdev_t *dev, const char *uimagepath, const char *ramdiskpath,
		  const char* versioninipath, const char *envrimentspath, int flags)
{
	if( !dev || !uimagepath ||!ramdiskpath ||!versioninipath )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	if( dev->dmdev_firm.dmctrl_icontrol != DM_CONTROL_ENABLE )
	{
		DBGERR( "%s: Firmware control is disabled from device library\n", __func__ );
		return -AXIS_ERROR_INVALID_VALUE;
	}

	DBGLOG( "%s: uImage path = %s \n", __func__, uimagepath );
	DBGLOG( "%s: ramdisk path = %s \n", __func__, ramdiskpath );
	DBGLOG( "%s: version ini path = %s \n", __func__, versioninipath );
	DBGLOG( "%s: envirement path = %s \n", __func__, envrimentspath );

	return DMXXXFirmWriteStart(dev, uimagepath, ramdiskpath, versioninipath,
				   envrimentspath, flags );
}


int AdapterFirmwareStop( dmdev_t *dev )
{
	if( !dev )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	return DMXXXFirmWriteStop( dev );
}

int AdapterFirmwareGetStatus( dmdev_t *dev )
{
	if( !dev )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	return DMXXXFirmGetStatus( dev );
}

int AdapterFirmwareGetProgress( dmdev_t *dev )
{
	if( !dev )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	return DMXXXFirmGetProgress( dev );
}

int AdapterFirmwareGetImageTimes( dmdev_t *dev, char *versionpath, char *envirpath, time_t *uimagetotime,
			   time_t* ramdisktotime, time_t* uimageintime, time_t* ramdiskintime )
{
	if( !dev || !versionpath || !uimageintime || !ramdiskintime || !uimageintime || !ramdiskintime )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	return DMXXXFirmGetImageTimes( dev, versionpath, envirpath, uimagetotime,
				      ramdisktotime, uimageintime, ramdiskintime );
}

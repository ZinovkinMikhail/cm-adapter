#include <unistd.h>
#include <stdio.h>


//������������ �������
#include <axis_error.h>
#include <axis_modules.h>
#include <axis_dmlibs_resource.h>

#include <dmxxx_lib/dmxxx_resource.h>
#include <dmxxx_lib/dmxxx_calls.h>

#include <adapter/module.h>
#include <adapter/debug.h>


//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>

#else
#define DBGLOG( ... )
#endif

/**
 * @brief ������������� ������� ��� ����������� �-��
 *
 * @param dev ��������� �� ��������� ����������� �-��
 * @return 0 - �� ����� ������
 **/
int AdapterInsertModule( dmdev_ctrl_hw_t *dev )
{
	if ( !dev )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}
	
	// ������������ �������� ������ �������� � ���������� ����������
	if ( dev->dmctrl_imodulescount <= 0 )
	{
		DBGLOG( "%s:%i:No modules need for %s \n", __func__, __LINE__, dev->dmdev_device_name );
		return 0;
	}

	int i = 0;
	int ret;
	
	// ���� �� ���� �������, � ������� ��������� ����������
	// ��������� �� ��� ������������� � ���� ���� ���������
	for ( i = 0; i < dev->dmctrl_imodulescount; i++ )
	{
		if(!dev->dmctrl_modules[i].noinsmod)
		{
			ret = axis_check_driver_ex( dev->dmctrl_modules[i].dmmodule_name, dev->dmctrl_modules->dmmodule_params );
			INFO_LOG( ret, "Init module %s", dev->dmctrl_modules[i].dmmodule_name );

			if ( ret )
			{
				DBGERR( "%s:It wasn't possible to init module\n", __func__ );
				return -1;
			}
		}

	}

	return 0;
}

/**
 * @brief �������� ������� ��� ����������� �-��
 *
 * @param dev ��������� �� ��������� ����������� �-��
 * @return 0 - �� ����� ������
 **/
int AdapterRemoveModule( dmdev_ctrl_hw_t *dev )
{
	if ( !dev )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	if ( dev->dmctrl_imodulescount <= 0 )
	{
		DBGLOG( "%s:%i:No modules need for %s \n", __func__, __LINE__, dev->dmdev_device_name );
		return 0;
	}

	int i = 0;

	int ret = 0;
	int iret;

	for ( i = dev->dmctrl_imodulescount - 1 ; i >=0; i-- )
	{
		if ( dev->dmctrl_modules[i].dmmodule_ineed_remove )
		{
			iret = axis_modules_rmmod( dev->dmctrl_modules[i].dmmodule_name );
			INFO_LOG( iret, "Remove module %s", dev->dmctrl_modules[i].dmmodule_name );

			if ( iret )
			{
				DBGERR( "%s: It wasn't possible to remove module %s\n", __func__, dev->dmctrl_modules[i].dmmodule_name );
				ret = -1;
			}
		}

	}

	return ret;
}

/**
 * @brief ��������������� ���� ������� �-��, ������ �������������
 * �������� � ����������  ���������� ���������� ����������� dm_dev_xxx
 * ������������ ������� ��������� ��� ������, ���� ���� �� ����� �� �����
 * ��������� ������.
 *
 * @param dev ��������� �� ��������� �-��
 * @return 0 - ��, ����� ������
 **/
int AdapterRemoveDevicesModules( dmdev_t *dev )
{
	int ret = 0;
	if ( !dev )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	if ( AdapterRemoveModule( &dev->dmdev_dm3xx ) )
	{
		DBGERR( "%s: It wasn't posssible remove module MainDevice\n", __func__ );
		ret = -1;
	}

	if ( AdapterRemoveModule( &dev->dmdev_acc ) )
	{
		DBGERR( "%s: It wasn't posssible remove module ACC\n", __func__ );
		ret = -1;
	}

	if ( AdapterRemoveModule( &dev->dmdev_eth ) )
	{
		DBGERR( "%s: It wasn't posssible remove module Eth\n", __func__ );
		ret = -1;
	}

	if ( AdapterRemoveModule( &dev->dmdev_event ) )
	{
		DBGERR( "%s: It wasn't posssible remove module Event\n", __func__ );
		ret = -1;
	}

	if ( AdapterRemoveModule( &dev->dmdev_fs ) )
	{
		DBGERR( "%s: It wasn't posssible remove module FS\n", __func__ );
		ret = -1;
	}

	if ( AdapterRemoveModule( &dev->dmdev_led ) )
	{
		DBGERR( "%s: It wasn't posssible remove module Led\n", __func__ );
		ret = -1;
	}

	if ( AdapterRemoveModule( &dev->dmdev_lpc ) )
	{
		DBGERR( "%s: It wasn't posssible remove module Lpc\n", __func__ );
		ret = -1;
	}

	if ( AdapterRemoveModule( &dev->dmdev_nand ) )
	{
		DBGERR( "%s: It wasn't posssible remove module Nand\n", __func__ );
		ret = -1;
	}

	if ( AdapterRemoveModule( &dev->dmdev_usb ) )
	{
		DBGERR( "%s: It wasn't posssible remove module Usb\n", __func__ );
		ret = -1;
	}

	if ( AdapterRemoveModule( &dev->dmdev_recorder ) )
	{
		DBGERR( "%s: It wasn't posssible remove module Recorder\n", __func__ );
		ret = -1;
	}

	if ( AdapterRemoveModule( &dev->dmdev_timer ) )
	{
		DBGERR( "%s: It wasn't posssible remove module Timer\n", __func__ );
		ret = -1;
	}

	if ( AdapterRemoveModule( &dev->dmdev_wd ) )
	{
		DBGERR( "%s: It wasn't posssible remove module Wd\n", __func__ );
		ret = -1;
	}
	if ( AdapterRemoveModule( &dev->dmdev_wifi ) )
	{
		DBGERR( "%s: It wasn't posssible remove module WiFi\n", __func__ );
		ret = -1;
	}
	if ( AdapterRemoveModule( &dev->dmdev_cdma ) )
	{
		DBGERR( "%s:It wasn't posssible remove module CDMA\n", __func__ );
		ret = -1;
	}
	if ( AdapterRemoveModule( &dev->dmdev_lte ) )
	{
		DBGERR( "%s:It wasn't posssible remove module LTE\n", __func__ );
		ret = -1;
	}

	if ( AdapterRemoveModule( &dev->dmdev_audiodev ) )
	{
		DBGERR( "%s:It wasn't posssible remove module Audio\n", __func__ );
		ret = -1;
	}
	INFO_LOG( ret, "Remove Device Modules" );

	return ret;

}

/**
 * @brief ������������� ���� ������� �-��, ������ ��������������� ��������
 * � ���������� ���������� ���������� ����������� dm_dev_xxx
 *
 * 
 * @param dev ��������� �� ��������� �-��
 * @return 0 - �� ����� ������
 **/
int AdapterInitDevicesModules( dmdev_t *dev )
{
	if ( !dev )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	if ( AdapterInsertModule( &dev->dmdev_dm3xx ) )
	{
		DBGERR( "%s:It wasn't posssible insert module DM365\n", __func__ );
		return -1;
	}

	if ( AdapterInsertModule( &dev->dmdev_acc ) )
	{
		DBGERR( "%s:It wasn't posssible insert module ACC\n", __func__ );
		return -1;
	}

	if ( AdapterInsertModule( &dev->dmdev_eth ) )
	{
		DBGERR( "%s:It wasn't posssible insert module Eth\n", __func__ );
		return -1;
	}

	if ( AdapterInsertModule( &dev->dmdev_event ) )
	{
		DBGERR( "%s:It wasn't posssible insert module Event\n", __func__ );
		return -1;
	}

	if ( AdapterInsertModule( &dev->dmdev_fs ) )
	{
		DBGERR( "%s:It wasn't posssible insert module FS\n", __func__ );
		return -1;
	}

	if ( AdapterInsertModule( &dev->dmdev_led ) )
	{
		DBGERR( "%s:It wasn't posssible insert module Led\n", __func__ );
		return -1;
	}

	if ( AdapterInsertModule( &dev->dmdev_lpc ) )
	{
		DBGERR( "%s:It wasn't posssible insert module Lpc\n", __func__ );
		return -1;
	}

	if ( AdapterInsertModule( &dev->dmdev_nand ) )
	{
		DBGERR( "%s:It wasn't posssible insert module Nand\n", __func__ );
		return -1;
	}

	if ( AdapterInsertModule( &dev->dmdev_usb ) )
	{
		DBGERR( "%s:It wasn't posssible insert module Usb\n", __func__ );
		return -1;
	}

	if ( AdapterInsertModule( &dev->dmdev_recorder ) )
	{
		DBGERR( "%s:It wasn't posssible insert module Recorder\n", __func__ );
		return -1;
	}

	if ( AdapterInsertModule( &dev->dmdev_timer ) )
	{
		DBGERR( "%s:It wasn't posssible insert module Timer\n", __func__ );
		return -1;
	}

	if ( AdapterInsertModule( &dev->dmdev_wd ) )
	{
		DBGERR( "%s:It wasn't posssible insert module WD\n", __func__ );
		return -1;
	}
	
	if ( AdapterInsertModule( &dev->dmdev_wifi ) )
	{
		DBGERR( "%s:It wasn't posssible insert module WiFi\n", __func__ );
		return -1;
	}

	if ( AdapterInsertModule( &dev->dmdev_cdma ) )
	{
		DBGERR( "%s:It wasn't posssible insert module CDMA\n", __func__ );
		return -1;
	}
	if ( AdapterInsertModule( &dev->dmdev_lte ) )
	{
		DBGERR( "%s:It wasn't posssible insert module LTE\n", __func__ );
		return -1;
	}

	if ( AdapterInsertModule( &dev->dmdev_audiodev ) )
	{
		DBGERR( "%s:It wasn't posssible insert module Audio\n", __func__ );
		return -1;
	}

	return 0;

}


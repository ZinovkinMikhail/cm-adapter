//������� ��� ������ � WiFi ���������

//������: 2017.02.24 12:35
//�����: ��������� ����� ��� "����" ������
//�����: i.podkolzin@ross-jsc.ru

#include <axis_error.h>

#include <dmxxx_lib/dmxxx_calls.h>

#include <adapter/power.h>
#include <adapter/wifi.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
//#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif


/**
 * @brief �������, ������� ���������� ����������� � USB WiFi ���������
 *
 * @param Dev - ��������� �� ��������� ����������
 * @param power - ���� ���������: 1 - ��������, 0 - ���������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterWiFiPower( dmdev_t *Dev, int Power )
{
	int ret;
	int WiFiPort;

	if( !Dev )
	{
		DBGERR( "%s: Invalid argument\n", __func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	if( !Dev->dmdev_usb.dmctrl_icontrol )
	{
		DBGERR( "%s: USB device not controled\n", __func__ );
		return -AXIS_ERROR_NOT_INIT;
	}

	if( !Dev->dmdev_wifi.dmctrl_icontrol )
	{
		DBGERR( "%s: WiFi device not controled\n", __func__ );
		return -AXIS_ERROR_NOT_INIT;
	}

	//��� �� ����� ��������/��������� WiFi - ��� ���� ����� ����� �����!!!
	//���� �� �������� �����������, �� ��� ���� ����������� �� �������
	//�� ���� �������� �����������, �� ��� ������ ���� ����� ����
	//����� ��� ����� ������� ���������� ���������� - ��� ��� ����� �� ����� ����� ��� ��� �� �����������

	if( !iDMXXXUsbGetWiFiPort( Dev ) )
	{
		DBGERR( "%s: Functions for get WiFi USB port not implementtion\n", __func__ );
		return -AXIS_ERROR_NOT_INIT;
	}

	WiFiPort = DMXXXUsbGetWiFiPort( Dev );

	if( WiFiPort < 0 )
	{
		DBGERR( "%s: Can't get USB WiFi port number from device library\n", __func__ );
		return WiFiPort;
	}

	if( !WiFiPort )
	{
		DBGERR( "%s: Device library not have USB WiFi port\n", __func__ );
		return -AXIS_ERROR_INVALID_VALUE;
	}

	DBGLOG( "%s: Try power %s WiFi modem on port %d\n", __func__, Power ? "on" : "off", WiFiPort );

	ret = AdapterPowerUSB( Dev, WiFiPort, Power );

	if( ret < 0 )
	{
		DBGERR( "%s: Can't power %s WiFi USB adapter at port %d\n", __func__, Power ? "on" : "off", WiFiPort );
	}

	return 0;
}

//���������� �������
 #include <stdio.h>
 #include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <axis_error.h>
#include <axis_dmlibs_resource.h>
#include <axis_string.h>

#include <dmxxx_lib/dmxxx_calls.h>

#include <adapter/dmdevice.h>
#include <adapter/debug.h>
#include <adapter/global.h>
//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif

/**
 * @brief ������� ������������� ���������� ��� ������
 * 
 * @param dev - ��������� �� ��������� ����������
 * @return int: 0 - OK, <0 - fail
 */
int AdapterDmDeviceInit(dmdev_t *dev)
{
	int ret;

	if( !dev )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	// �������������� ���������� � ������ ������
	ret = DMXXXInit( dev );
	if( ret < 0 )
	{
		DBGERR( "%s: It wasn't possible to init dm device\n", __func__ );
		return ret;
	}

	// ��������� ����������
	ret = DMXXXOpen( dev );
	if( ret < 0 )
	{
		DBGERR( "%s: It wasn't possible to open device\n", __func__ );
		return ret;
	}

	// ����� �������� watchdog ��� �������
	ret = AdapterDmDeviceWatchDogEnable( dev );
	if( ret )
	{
		DBGERR( "%s: It wasn't possible to enable watchdog\n", __func__ );
		return ret;
	}

	//�������� ������� �� ������ ��������� ����������
	ret = DMXXXModeButtonEnable( dev );
	if( ret < 0 )
	{
		DBGERR( "%s: It wasn't possible to enable button\n", __func__ );
		return ret;
	}

	//������ ������� ������ ��� ������ �� ���
	ret = DMXXXACCStartModems( dev);
	if( ret < 0 )
	{
		DBGERR( "%s: It wasn't possible to start modem from acc\n", __func__ );
		return ret;
	}

	return 0;
}

/**
 * @brief ������� ��������������� ���������� ��� ���������� ������
 * 
 * @param dev - ��������� �� ��������� ����������
 * @return int: 0 - OK, <0 - fail
 */
int AdapterDmDeviceDeInit(dmdev_t *dev)
{
	if( !dev )
	{
		DBGERR("%s:%i: Invalid Argumentn\n",__func__,__LINE__);
		return -1;
	}
	
	//��� ���� ������, � �� ������ �� ��� ������� �� ��� �� �������!
	if( !dev->libp )
	{
		DBGLOG( "%s: No device library initing - no close\n", __func__ );
		return 0;
	}
	
	if(dev->dmdev_timer.dmctrl_ipresent && dev->dmdev_timer.dmctrl_feature & DM_TIMER_STATUS_FLAGS_ENABLE)
	{
		if(DMXXXTimerPutStatusFlags(dev))
		{
			DBGERR("%s:%i:It wasn't possible to save timer flags\n",\
									__func__,\
									__LINE__ );
			return -1;
		}
	}
	if(DMXXXClose(dev))
	{
		DBGERR("%s:It wasn't possible to close device\n",__func__);
		return -1;
	}

	if(DMXXXDeinit(dev))
	{
		DBGERR("%s:It wasn't possible to deinit dm device\n",__func__);
		return -1;
	}

	return 0;

}

/**
 * @brief ������� �������� ���� ��� ������ ���������� ����������
 * 
 * @param dev - ��������� ���������� ����������
 * @return int: 0 - OK, <0 - fail
 */
static int adapter_dm_device_mknod(dmdev_ctrl_hw_t *dev)
{
	if( !dev )
	{
		DBGERR("%s:%i: Invalid Argumentn\n",__func__,__LINE__);
		return -1;
	}

	if( dev->dmctrl_imknod != DM_CONTROL_NEED )
	{
		// ������ �� ����� ���������� ���� (��� ���������)
		return 0;
	}
	
	//��������� �������, ���� ���, �� �������
	char path[64];
	struct stat mstat;
	snprintf(path,sizeof(path),DEVICE_PATH_MASK,dev->dmdev_device_name);
	
	if ( stat( path, &mstat ) < 0 )
	{
		//���� ��� ����� ��� ����������
		//�������� ���
		if ( mknod( path, S_IFCHR | 0644, dev->dmctrl_dev )  < 0 )
		{
			DBGERR( "%s: Can't create device for %s\n",\
							__func__, path );
			return -AXIS_ERROR_CANT_OPEN_DEVICE;
		}
	}

	INFO_LOG(0,"mknod for %s",path);
	
	return 0;
	
}


/**
 * @brief ������� �������� ��������������� ���� ��� ������� ��
 * �������������� � �������� ���������
 * 
 * @param dev - ��������� �� ��������� ����������
 * @return int: 0 - OK, <0 - fail
 */
int AdapterDmDeviceMknod( dmdev_t *dev)
{
	if( !dev )
	{
		DBGERR("%s:%i: Invalid Argumentn\n",__func__,__LINE__);
		return -1;
	}

	if(adapter_dm_device_mknod(&dev->dmdev_dm3xx))
	{
		DBGERR("%s:It wasn't posssible mknod DM365\n",__func__);
		return -1;
	}
	if(adapter_dm_device_mknod(&dev->dmdev_acc))
	{
		DBGERR("%s:It wasn't posssible mknod ACC\n",__func__);
		return -1;
	}
	if(adapter_dm_device_mknod(&dev->dmdev_eth))
	{
		DBGERR("%s:It wasn't posssible mknod Eth\n",__func__);
		return -1;
	}
	if(adapter_dm_device_mknod(&dev->dmdev_event))
	{
		DBGERR("%s:It wasn't posssible mknod Event\n",__func__);
		return -1;
	}
	if(adapter_dm_device_mknod(&dev->dmdev_fs))
	{
		DBGERR("%s:It wasn't posssible mknod FS\n",__func__);
		return -1;
	}
	if(adapter_dm_device_mknod(&dev->dmdev_led))
	{
		DBGERR("%s:It wasn't posssible mknod Led\n",__func__);
		return -1;
	}
	if(adapter_dm_device_mknod(&dev->dmdev_lpc))
	{
		DBGERR("%s:It wasn't posssible mknod Lpc\n",__func__);
		return -1;
	}
	if(adapter_dm_device_mknod(&dev->dmdev_nand))
	{
		DBGERR("%s:It wasn't posssible mknod Nand\n",__func__);
		return -1;
	}
	if(adapter_dm_device_mknod(&dev->dmdev_usb))
	{
		DBGERR("%s:It wasn't posssible mknodUsb\n",__func__);
		return -1;
	}
	if(adapter_dm_device_mknod(&dev->dmdev_recorder))
	{
		DBGERR("%s:It wasn't posssible mknod Recorder\n",__func__);
		return -1;
	}
	if(adapter_dm_device_mknod(&dev->dmdev_timer))
	{
		DBGERR("%s:It wasn't posssible mknod Timer\n",__func__);
		return -1;
	}
	if(adapter_dm_device_mknod(&dev->dmdev_wd))
	{
		DBGERR("%s:It wasn't posssible mknod Wd\n",__func__);
		return -1;
	}

	return 0;
}

/**
 * @brief ������� �������� ������� ��� watchdog
 * 
 * @detailed ������� ����������� ������ watchdog, ����� �� �����
 * ������ ������
 * 
 * @param dev - ��������� �� ��������� ����������
 * @return int: 0 - OK, <0 - fail
 */
int AdapterDmDeviceWatchDogTick( dmdev_t *dev)
{
	if( !dev )
	{
		DBGERR("%s:%i: Invalid Argumentn\n",__func__,__LINE__);
		return -1;
	}
	if(dev->dmdev_wd.dmctrl_ipresent)
	{
		DBGLOG("%s: %i WD TIC blea\n",__func__,__LINE__);

		if(DMXXXWdTick(dev))
		{
			DBGERR("%s:%i:It wasn't possible to tick watchdog\n",\
									__func__,\
									__LINE__);
			return -1;
		}
	}

	return 0;
}

/**
 * @brief ������� ���������� ������������ watchdog
 * 
 * @param dev - ��������� �� ��������� ����������
 * @return int: 0 - OK, <0 - fail
 */
int AdapterDmDeviceWatchDogEnable( dmdev_t *dev)
{
	if( !dev )
	{
		DBGERR("%s:%i: Invalid Argumentn\n",__func__,__LINE__);
		return -1;
	}

	if(dev->dmdev_wd.dmctrl_ipresent && dev->dmdev_wd.dmctrl_icontrol)
	{
		if(DMXXXWdEnable(dev))
		{
			DBGERR("%s:%i:It wasn't possible to enable watchdog\n",\
									__func__,\
									__LINE__);
			return -1;
		}
	}

	return 0;
}

/**
 * @brief ������� ���������� ������������ watchdog
 * 
 * @param dev - ��������� �� ��������� ����������
 * @return int: 0 - OK, <0 - fail
 */
int AdapterDmDeviceWatchDogDisable(dmdev_t *dev)
{
	if( !dev )
	{
		DBGERR("%s:%i: Invalid Argumentn\n",__func__,__LINE__);
		return -1;
	}

	if(dev->dmdev_wd.dmctrl_ipresent && dev->dmdev_wd.dmctrl_icontrol)
	{
		if(DMXXXWdDisable(dev))
		{
			DBGERR("%s:%i:It wasn't possible to disable watchdog\n",\
									__func__,\
									__LINE__);
			return -1;
		}
	}

	return 0;
}

int AdapterDmDeviceGetLastKeyPress(dmdev_t* dev, time_t *time)
{
	if( !dev || !time )
	{
		DBGERR("%s:%i: Invalid Argumentn\n",__func__,__LINE__);
		return -1;
	}

	return DMXXXModeButtonLastKeyPressTime(dev,time);
	
}

/**
 * @brief ������� ������������ ���������� � ����� ����������� �����������������
 * 
 * @param dev - ��������� �� ��������� ����������
 * @return int: 0 - OK, <0 - fail
 */
int AdapterDmDeviceSleepGo(dmdev_t *dev)
{
	if( !dev )
	{
		DBGERR("%s:%i: Invalid Argumentn\n",__func__,__LINE__);
		return -1;
	}

	return DMXXXLpcControlGo(dev);
}

int AdapterDmDeviceAdxlInit( dmdev_t *dev )
{
	return DMXXXAdxlInit( dev );
}

int AdapterDmDeviceLockKeyboard(dmdev_t *dev)
{
	return DMXXXModeKeysDisable(dev);
}

int AdapterDmDeviceUnlockKeyboard(dmdev_t *dev)
{
	return DMXXXModeKeysEnable(dev);
}

/**
 * @brief ������� ��������� ��������� ������ ����������
 * 
 * @param Status - ���������� ��������� ���������� ��������
 * @return int: 0 - OK, <0 - fail
 */
int AdapterDmDeviceGetSerialNumber(AdapterGlobalStatusStruct_t *Status)
{
	if( !Status )
	{
		DBGERR("%s:%i: Invalid Argumentn\n",__func__,__LINE__);
		return -1;
	}
	unsigned char sn[8];
	int ret;
	ret = DMXXXInitGetSN(&Status->GlobalDevice_t,sn, sizeof(sn));
	if( ret <0 )
	{
		DBGERR("%s:It wasn't possible to get SN\n",__func__);
		return -1;
	}
	axis_sprint_buff( Status->CurrentNetwork.SN,
						sizeof(Status->CurrentNetwork.SN ),
						sn,
						sizeof( sn ) );

	Status->CurrentNetwork.SN[ sizeof( Status->CurrentNetwork.SN ) - 1 ] = 0x00;

	return 0;
	
}

//���������� �������
#include <stdio.h>
#include <stdlib.h>
#include <sys/mount.h>

#include <axis_error.h>
#include <axis_dmlibs_resource.h>
#include <axis_find_from_file.h>

#include <dispatcher_lib/dispatcher_lib_resource.h>

#include <dmxxx_lib/dmxxx_calls.h>

#include <adapter/global.h>
#include <adapter/hardware.h>
#include <adapter/timer.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#include <unistd.h>
#else
#define DBGLOG( ... )
#endif

/**
 * @brief ������� �������� ������� �����
 * 
 * @param dev ��������� ����������
 * @return int: 0 -OK, <0 -fail
 */
static int adapter_timer_check_if_clock_go(dmdev_t *dev)
{
	if( !dev )
	{
		DBGERR("%s:%i: Invalid Argumentn\n",__func__,__LINE__);
		return -1;
	}

	struct_data sd_date;

	struct_data sd_date_copy;

	memset( &sd_date, 0, sizeof( sd_date ) );

	memset( &sd_date_copy, 0, sizeof( sd_date_copy ) );

	if(DMXXXTimerGetTime(dev,&sd_date) < 0)
	{
		DBGERR("%s: It wasn't possible to get time from hardware\n",__func__);
		return -1;
	}

	sleep(1);

	if(DMXXXTimerGetTime(dev,&sd_date_copy) < 0)
	{
		DBGERR("%s: It wasn't possible to get time twice from hardware\n",__func__);
		return -1;
	}

	if ( sd_date.sec == sd_date_copy.sec )
	{
		DBGERR("%s:Timer Is Not ticking\n",__func__);
		return -1;
	}

	return 0;
}


/**
 * @brief ������� ������������� ����� � ����������� ���������� �������
 * 
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int: 0 - OK, <0 -fail
 */
int AdapterTimerClockInit( AdapterGlobalStatusStruct_t *Status )
{
	if( !Status )
	{
		DBGERR("%s:%i: Invalid Argument\n",__func__,__LINE__);
		return -1;
	}

	if( !Status->GlobalDevice_t.dmdev_timer.dmctrl_ipresent )
	{
		return 0;
	}

	if( DMXXXTimerInit( &Status->GlobalDevice_t ) )
	{
		DBGERR("%s:It wasn't possible to init time\n",__func__);
		return -1;
	}

	if( adapter_timer_check_if_clock_go(  &Status->GlobalDevice_t ) )
	{
		DBGERR("%s:Clock stoped\n",__func__);
		return -1;
	}

	//�� ������ ��������������� ����� ������ ������ �� �������� ���������
	//���������� �������, ��� �� ���� ��� �����
	int32_t UpTime = time( NULL ) - system_time_convert( Status->AdapterStart ); //����� ������ �� �������������
	time_t Now = time( NULL ); //������� �����

	if( UpTime < 0 )
	{
		//� ��� ��� ������ �������
		UpTime = 0;
	}

	if( AdapterTimerSyncTime(  &Status->GlobalDevice_t ) < 0 )
	{
		DBGERR("%s:It wasn't possible to sync time\n",__func__);
		return -1;
	}

	//������������ ����� ������ � ������ ���������� �������
	if( UpTime && Now < time( NULL ) )
	{
		Status->AdapterStart = system_time_translate( time( NULL ) - UpTime );
	}
	return 0;
}

/**
 * @brief ������� ��������� ������� � ���� � ����
 * 
 * @param dev - �������� ����������
 * @param date - ��������� � �����
 * @return int: 0 - OK, <0 - fail
 */
int AdapterTimerSetTime(dmdev_t *dev,struct_data *date)
{
	if( !dev || !date )
	{
		DBGERR("%s:%i: Invalid Argumentn\n",__func__,__LINE__);
		return -1;
	}
	
	if(!dev->dmdev_timer.dmctrl_ipresent)
	{
		return 0;
	}
	
	return DMXXXTimerSetTime( dev, date);
}

int AdapterTimerGetTime(dmdev_t *dev,struct_data *date)
{
	int ret;

	if( !dev || !date )
	{
		DBGERR("%s:%i: Invalid Argumentn\n",__func__,__LINE__);
		return -1;
	}

	if(!dev->dmdev_timer.dmctrl_ipresent)
	{
		return 0;
	}

	ret = DMXXXTimerGetTime(dev,date);

	if( ret < 0 )
	{
		DBGERR( "%s: Can't get system time from hardwre\n", __func__ );
	}

	return ret;
}


int AdapterTimerGetAlarm(dmdev_t *dev,struct_data *date)
{
	if( !dev || !date )
	{
		DBGERR("%s:%i: Invalid Argumentn\n",__func__,__LINE__);
		return -1;
	}

	if(!dev->dmdev_timer.dmctrl_ipresent)
	{
		return 0;
	}

	return DMXXXTimerGetAlarm(dev,date);
}


int AdapterTimerSetAlarm(dmdev_t *dev,struct_data *date)
{
	if( !dev || !date )
	{
		DBGERR("%s:%i: Invalid Argumentn\n",__func__,__LINE__);
		return -1;
	}

#ifdef CONFIG_ENCODER_DEBUG_MSG
	char timer_str[ 256 ];
	system_time_print_ini( timer_str, sizeof( timer_str ), *date );
	DBGLOG( "%s: --- Try set alarm to '%s'\n", __func__, timer_str );
#endif

	if(!dev->dmdev_timer.dmctrl_ipresent)
	{
		return 0;
	}

	return DMXXXTimerSetAlarm(dev,date);
}

/**
 * @brief ������� ��������� ��������� ������ ������� �� ����������������� ������
 *
 * @param Status ...
 * @return int
 */
int AdapterTimerGetStatusFlags( AdapterGlobalStatusStruct_t *Status )
{
	if( !Status)
	{
		DBGERR("%s:%i: Invalid Argumentn\n",__func__,__LINE__);
		return -1;
	}

	if( Status->GlobalDevice_t.dmdev_timer.dmctrl_feature & DM_TIMER_STATUS_FLAGS_ENABLE )
	{

		if( Status->GlobalDevice_t.dmdev_timer.dmctrl_feature & DM_TIMER_STATUS_FLAGS_SAVE_RECORDER &&
			Status->Recorder.recorderscount > 0 &&
			Status->Recorder.next &&
			Status->Recorder.next->recorder_get_adapter_timers_status_flag )
		{
			int ret = Status->Recorder.next->recorder_get_adapter_timers_status_flag( Status->Recorder.next->hen,
				&Status->Recorder.next->semaphore, &Status->GlobalDevice_t.dmdev_timer.dmctrl_status );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't get timers status flag from recorder\n", __func__ );
				return ret;
			}
		}

		else if( DMXXXTimerGetStatusFlags( &Status->GlobalDevice_t ) )
		{
			DBGERR("%s:It wasn't possible to get status timer flags\n",__func__);
			return -1;
		}
	}

	return 0;
}


/**
 * @brief �������, ������� ���������� ��������� ���� ������� � ����������������� ������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 */
int AdapterTimerPutStatusFlags( AdapterGlobalStatusStruct_t *Status )
{
	if( !Status )
	{
		DBGERR("%s:%i: Invalid Argumentn\n",__func__,__LINE__);
		return -1;
	}

	if( Status->GlobalDevice_t.dmdev_timer.dmctrl_feature & DM_TIMER_STATUS_FLAGS_ENABLE)
	{

		if( Status->GlobalDevice_t.dmdev_timer.dmctrl_feature & DM_TIMER_STATUS_FLAGS_SAVE_RECORDER &&
			Status->Recorder.recorderscount > 0 &&
			Status->Recorder.next &&
			Status->Recorder.next->recorder_put_adapter_timers_status_flag )
		{
			int ret = Status->Recorder.next->recorder_put_adapter_timers_status_flag( Status->Recorder.next->hen,
				&Status->Recorder.next->semaphore, Status->GlobalDevice_t.dmdev_timer.dmctrl_status );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't put status flag to recorder\n", __func__ );
				return ret;
			}
		}

		else if( DMXXXTimerPutStatusFlags( &Status->GlobalDevice_t ) )
		{
			DBGERR("%s:It wasn't possible to get status timer flags\n",__func__);
			return -1;
		}
	}

	return 0;
}


//������� ������� ��� alarm_set
//����:
// Status - ��������� �� ���������� ��������� �������
// time_set - ��������� �� ��������� ���� ���������� ��������� ������������ �������
//�������:
// ��������� �������� � ��������
uint16_t AdapterTimerAlarmSet( AdapterGlobalStatusStruct_t *Status, struct_data *time_set )
{
	uint64_t *timerFlags[1];
	uint64_t LocalFlag = 0;

	if(!Status->GlobalDevice_t.dmdev_timer.dmctrl_ipresent)
	{
		return 0;
	}

	//���� ������� ���������� ����� �������� - �� ����� �� ���������� ����������
	//����� ����������� ��������� ������, ������� �� �� ��� �� ������ - ��� �������������
	if( Status->GlobalDevice_t.dmdev_timer.dmctrl_feature & DM_TIMER_STATUS_FLAGS_ENABLE )
	{
		timerFlags[0] = &Status->GlobalDevice_t.dmdev_timer.dmctrl_status;
	}
	else
	{
		timerFlags[0] = &LocalFlag;
	}

	if( Status->GlobalDevice_t.dmdev_timer.dmctrl_feature & DM_TIMER_TYPE_DSP )
	{
		if( Status->GlobalDevice_t.dmdev_timer.dmctrl_feature & DM_TIMER_TYPE_ADAPTER )
		{
			return alarm_set_dsp( &Status->TimersSettingsNet, time_set, timerFlags );
		}

		DBGLOG( "%s:%i: Not supported timer work for recorder now\n",__func__,__LINE__);

		return 0;
	}
	else
	{
		if( Status->GlobalDevice_t.dmdev_timer.dmctrl_feature & DM_TIMER_TYPE_ADAPTER )
		{
			return alarm_set( &Status->TimersSettingsNetAxis, time_set, timerFlags, 2 );
		}

		DBGLOG( "%s:%i: Not supported timer work for recorder now\n",__func__,__LINE__);

		return 0;
	}

	return 0;
}

/**
 * @brief �������, ������� ������������� ��������� ������, ��� ��� � ������� �� ��� �� �����������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
static int AdapterTimerDeinit( AdapterGlobalStatusStruct_t *Status )
{
	if( !Status )
	{
		DBGERR( "%s: Invalid argument\n",__func__ );
		return -1;
	}

	if( DMXXXTimerDisableAlarm( &Status->GlobalDevice_t ) < 0 )
	{
		DBGERR( "%s: It wasn't possible to disable alarm\n",__func__ );
		return -1;
	}

	return 0;
}

//������� ������� ���������� ���������� ��������
int AdapterTimerWork( AdapterGlobalStatusStruct_t *Status )
{

	 if( !Status )
   	 {
		DBGERR(DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return -AXIS_ERROR_INVALID_ARGUMENT;
   	 }

	 if(!Status->GlobalDevice_t.dmdev_timer.dmctrl_ipresent)
	 {
		return 0;
	 }

	//������� ���������� ������ ������, ��� �� ����� ���������� ������
	//������� ��� �� ����� ���������� ������ ���, �� ��� �� ������ ��� ���������
	//� ��� �� ����� ���������� ����� ������ ������, � ����� ��������� �������� �� �������
	if( AdapterTimerGetStatusFlags( Status ) < 0 )
	{
		DBGERR("%s: It wasn't possible to load timer flags\n",__func__);
		return -1;
	}

	 struct_data time_set;

	 memset(&time_set,0,sizeof(struct_data));

	uint16_t check_alarm = AdapterTimerAlarmSet(Status,&time_set);

	if( AdapterTimerPutStatusFlags( Status ) < 0 )
	{
		DBGERR("%s:It wasn't possible to save timer flags\n",__func__);
		return -1;
	}

	if( check_alarm & 0x00001 )
	{
		//������� ����� �� ������������ ����� ��������
		//������� ������ ������!!!
		//��� ��� �������� �� �������
		Status->SleepTime = -1;
		DBGLOG("%s: Adapter must work! Setting standart sleep time: %i sec\n", __func__,
											Status->SleepTime );
	}

	if( check_alarm & 0xFF00 )
	{
		DBGLOG("%s: Must SET TIMER\n", __func__ );
		if(!(check_alarm & 0x00001))
		{
			DBGLOG("%s:%i: Setting standart sleep time: %i secs\n",__func__, __LINE__,
				   							Status->AdapterSettings.Block0.AdapterSettings.Sleep);
			//���� �� �� ����� �� ������� ������ �������� ������ ���� ����
			//Status->LastInterfaceQuire = time(NULL);
			Status->SleepTime = Status->AdapterSettings.Block0.AdapterSettings.Sleep;
		}
		char time[126];
		system_time_print_ini( time, sizeof( time ), time_set);
		DBGLOG("%s:%i: TIMER = %s\n",__func__,__LINE__,time);
		if(AdapterTimerSetAlarm(&Status->GlobalDevice_t, &time_set) < 0)
		{
			DBGERR("%s:%i: It wasn' t possible to set alarm\n",__func__, __LINE__ );
			return -1;
		}
	}
	else if( !check_alarm )
	{

		//�������� � ������� ���, � ������ �� �� �������� �� �������
		//���� ��������� ������, ��� �� �� �� �������� � �������� �����
		DBGLOG( "%s: DEINIT ALARM!!!!\n", __func__ );

		if( AdapterTimerDeinit( Status ) < 0 )
		{
			DBGERR( "%s: It wasn't possible to deinit alarm\n",__func__ );
		}

		if( Status->GlobalDevice_t.dmdev_timer.dmctrl_feature & DM_TIMER_NO_TIMER_NO_SLEEP )
		{
			//��� ������� - ��� ������� � ������ ����� (��� ��� ��������� � ������� ��� ��)
			//������ �� ������ ���������, � �� �������� �� � ��� ��, ��� ��� ���������
			//������� ����� �������� �� ������ �� ��������, ��� ��������, ����-�3 � �������
			//�� ��������� �� ��� �� ������ ��� � ������� �������
			//��� ��������� ��������� ������� ����������� (�� �� ������������) �������
			//�� ��� �� ������ ������ �� ���
			//TODO - ���� ��� �� ���� �� ���������� - ���� ����� ��� ���������� ��� ����-�3
			if( Status->AdapterDevice == OMAP4430_DEVICE_HARDWARE_TYPE__BR3 &&
					( Status->AdapterSettings.Block0.AdapterSettings.IOAlarmUSB & ( 1 << 2 ) ) ) //TODO - ��� ���� ���������!!!
			{
				Status->SleepTime = Status->AdapterSettings.Block0.AdapterSettings.Sleep; //��� �� ������ ����, ���� �� ����
				DBGERR( "%s: Device 'Base-R3' have a enabled RC device - Setting standart sleep tume: %i sec\n", __func__,
						Status->AdapterSettings.Block0.AdapterSettings.Sleep	);
			}

			else
			{
				//��� ������ ��������� - ��� ������� �� �����
				//� �� ��������, ������� ����� �� ������ ��������� ��������� ���� DM_TIMER_NO_TIMER_NO_SLEEP
				//� �������� ��������
				Status->SleepTime = -1;
				DBGLOG( "%s: No timer work in future, adapter must work!!! Setting standart sleep tume: %i sec\n",__func__, 
						Status->SleepTime );
			}
		}

		else
		{
			//���� ��� ��������� ����� - ������ ����� ��������� ���������� �������, ���� ������� ����� �� �����
			DBGLOG( "%s:%i: Setting standart sleep time: %i secs\n",__func__, __LINE__,
											Status->AdapterSettings.Block0.AdapterSettings.Sleep );
			Status->SleepTime = Status->AdapterSettings.Block0.AdapterSettings.Sleep;
		}

	}

	Status->LastInterfaceQuire = time(NULL);

	return check_alarm;
}

int AdapterTimerSyncTime(dmdev_t *dev)
{
	if( !dev )
	{
		DBGERR("%s:%i: Invalid Argumentn\n",__func__,__LINE__);
		return -1;
	}

	if(dev->dmdev_timer.dmctrl_ipresent && dev->dmdev_timer.dmctrl_icontrol)
	{
		return DMXXXTimerSyncTime( dev);
	}

	return 0;
}

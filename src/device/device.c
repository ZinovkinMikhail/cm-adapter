//������� ������������� ��������� ��� ������ � �����

#ifdef IN_TRASH

//���������� �������
 #include <stdio.h>
 #include <stdlib.h>
//������������ �������
#include <axis_error.h>
#include <axis_modules.h>
#include <axis_string.h>
#include <adapter/global.h>
#include <axis_modules.h>
#include <adapter/device.h>
#include <wifi/Axis/libAxisWiFi.h>

//�������� USB ������ ��� 355 �����������
#ifdef TMS320DM355
#include <dm355_ioctl.h>
#include <dm355_device.h>
#endif

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
//#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif




int copydat()
{
	int f1,f2;
	if(!(f1 = open(FROM_COPY, O_RDONLY)))
	{
		DBGERR("%s:Cannot copy open %s\n",__func__, FROM_COPY);
		return -1;
	}

	if(!(f2 = open(TO_COPY, O_WRONLY | O_CREAT, 0600)))
	{
		DBGERR("%s:Cannot copy open %s\n",__func__, TO_COPY);
		return -1;
	}	

	char buf[1024];
	memset(buf, 0, 1024);
	int size;

	size =read(f1, buf, 1024);
	while(size != 0)
	{
		write(f2, buf, size);
		size = read(f1, buf, 1024);
	}

	close(f1);
	close(f2);
	return 0;
}

//������� ������� �������� ��� ������ � �������� 
// �� ����� ���
void adapter_device_deinit( int hen, int adaptertype)
{
#ifdef TMS320DM355	
  	//������� ����������
	if( hen < 0 )
	{
	  	DBGERR("%s:Bad device hendel \n",__func__);
		return;
	}
		//�������� ��
	//�������� ��� ������
	adapter_device_remove_modules( adaptertype);
	
	 if( adaptertype == DM355_DEVICE_HARDWARE_TYPE__R2)
	 {
		 dm355_device_vusb1_off( hen );
		
	 }
	 else if ( adaptertype == DM355_DEVICE_HARDWARE_TYPE__220)
	 {
#warning ������� �� ����� �� �����
	   	//dm355_device_ethernet_off(hen);
	   	//�� ����� 220 ��������� �� ��� �� ����� ��� � ����
	   	dm355_device_cdma_off( hen );
	}
	else 
	{
	  	DBGERR("%s: Not supported adapter type\n", __func__);
	}
	sleep(5);
	

	
#endif	
}

int adapter_close_device_hendel( int hen)
{
#ifdef TMS320DM355	
  	if (hen <= 0)
	{
	  	DBGERR("%s:Invalid argument\n",__func__);
		return -1;
	}
	
	dm355_device_close( hen );
#endif	
	return 0;
}

int adapter_open_device_hendel()
{

	int hen = -AXIS_ERROR_CANT_OPEN_DEVICE;
#ifdef TMS320DM355
	int err;
	//�������� �������
	err = axis_check_driver( DISPATCHER_DM355_DEVICE_MODULES, NULL );
	
	if ( err < 0) 
	{
		DBGERR("%s: Can't insert device driver\n", __func__ );

		return err;
		
	}
	//������� ����������
	hen = dm355_device_open();

	DBGLOG("%s: DM355Device open at %d handel\n",__func__, hen);
	
	if( hen < 0 )
	{
		DBGERR("%s: Can't open device driver\n", __func__ );
		return hen;
	}
#endif  /*TMS320DM355*/	
	return hen;

}

static int check_iface(const char *name)
{
	char path[PATH_MAX];

	snprintf(path, sizeof(path), "/sys/class/net/%s", name);
	return access(path, F_OK);
}


//������� �������� ���� �� Wireless ���������� 
int isdevice_in(char* interface, int adaptertype)
{
	 char dev_name[32];	
	 
	 if( adaptertype == DM355_DEVICE_HARDWARE_TYPE__R2)
	 {
	
		 int skfd;
	   
		  if((skfd = iw_sockets_open()) < 0)
		  {
			  DBGERR("%s: Can't open socket\n",__func__);
			  return -1;
		  }
	  
		  memset(dev_name,'\0',IFNAMSIZ+1);
	  
		  get_device_name(skfd, dev_name);	
			
		close( skfd );
	  
		  if( !dev_name[0] )
		  {
			DBGERR("%s: No Wireless Devices found\n",__func__);
			return -1;
		  }
		  
		    strncpy(interface, dev_name, MAX_INTERFACE_NAME);
	 }
	else if( adaptertype ==DM355_DEVICE_HARDWARE_TYPE__220 )
	{
	   	//���� ����� ��� ����� ��� �������� �� ���������� 
#warning ��� �� ��������� ���� �� �� �������
		
		if (check_iface("eth0") < 0)
		{
			return -1;
		}
		
		 strncpy(interface, "eth0", MAX_INTERFACE_NAME);
	}
	else 
	{
	  	DBGERR("%s: Not supported adapter type\n", __func__);
		return -2;
	}
		  
	  return 0;
}

//���������� ��� ����������� ������ ��� ������ � wifi
//�������:
// 0 - ��� ��
// �����  - ������
int adapter_device_insert_modules( int adaptertype )
{
	int ret			= 0;
 	if( adaptertype == DM355_DEVICE_HARDWARE_TYPE__R2)
 	{
		  //�� ��������� �� ���� ������ ���
		
	  
		  ret =axis_check_driver_ex( R8187L, NULL );
	  
		 if ( ret < 0  )
		  {
		   	DBGERR("%s: Cannot load R8187L module\n", __func__);
			return ret;
		  }	  
	  
		  ret =axis_check_driver_ex( RT3070STA, NULL );
	  
		  if ( ret < 0  )
		  {
		    	DBGERR("%s: Cannot load RT3070STA module\n", __func__);
			return ret;
		  }		  
		    ret =  axis_check_driver_ex( G_ZERO, NULL );
	  
		  if ( ret < 0  )
		  {
		    	DBGERR("%s: Cannot load G_ZERO module\n",__func__);
			return ret;
		  }
	}
	else  if( adaptertype == DM355_DEVICE_HARDWARE_TYPE__220)
	{
		  ret =axis_check_driver_ex( DM9000, NULL );
	  
		  if ( ret < 0  )
		  {
		    	DBGERR("%s: Cannot load dm9000 module\n", __func__);
			return ret;
		  }		    
	}
	else 
	{
	  	DBGERR("%s: Not supported adapter type\n", __func__);
		return -1;
	}
	
	return ret;
}

//��������� ��� ����������� ������ ��� ������ � wifi 
//�������:
// 0 - ��� ��
// �����  - ������
int adapter_device_remove_modules( int adaptertype )
{
	int ret			= 0;

	if( adaptertype == DM355_DEVICE_HARDWARE_TYPE__R2)
 	{	
		//������� ��������...
		if( axis_modules_rmmod( G_ZERO ) != 0 )
		{
			DBGERR("It wasn't possible to remove g_zero module\n");
			ret = -1;
		}
	
		if( axis_modules_rmmod( RT3070STA ) != 0 )
		{
			DBGERR("It wasn't possible to remove  RT3070STA module\n");
			ret = -1;
		}	
	
		if( axis_modules_rmmod( R8187L ) != 0 )
		{
			DBGERR("It wasn't possible to remove r8187l module\n");
			ret = -1;
		}
	}
	else if ( adaptertype == DM355_DEVICE_HARDWARE_TYPE__220)
	{
		if( axis_modules_rmmod( DM9000 ) != 0 )
		{
			DBGERR("It wasn't possible to remove dm9000 module\n");
			ret = -1;
		}	  	
	}
	else 
	{
	  	DBGERR("%s: Not supported adapter type\n", __func__);
		return -1;
	}
	
	return ret;

	

}

#endif
 

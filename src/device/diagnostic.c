#include <axis_find_from_file.h>
#include <axis_modules.h>
#include <axis_error.h>

#include <dmxxx_lib/dmxxx_resource.h>
#include <dmxxx_lib/dmxxx_calls.h>

#include <dispatcher_lib/getopt.h>

#include <adapter/hardware.h>
#include <adapter/global.h>
#include <adapter/diagnostic.h>



//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
//#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif


int AdapterLoadDiagnostic(dmdev_t *dev,AdapterGlobalStatusStruct_t* Status)
{
	if( !dev || !Status)
	{
		DBGERR("%s:%i: Invalid Argumentn\n",__func__,__LINE__);
		return -1;
	}


	return DMXXXLoadDiagnosticData( dev, &Status->RecorderDiagnostic, &Status->AdapterDiagnostic );

}
int AdapterSaveDiagnostic(dmdev_t *dev,AdapterGlobalStatusStruct_t* Status)
{
	if( !dev || !Status)
	{
		DBGERR("%s:%i: Invalid Argumentn\n",__func__,__LINE__);
		return -1;
	}


	return DMXXXSaveDiagnosticData( dev, &Status->RecorderDiagnostic, &Status->AdapterDiagnostic );

}


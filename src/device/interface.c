#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/file.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <inttypes.h>
#include <errno.h>

#include <axis_process.h>
#include <axis_error.h>
#include <axis_dmlibs_resource.h>
#include <axis_softrecorder.h>

#include <dispatcher_lib/network.h>
#include <dispatcher_lib/requests.h>
#include <dispatcher_lib/command.h>

#include <dmxxx_lib/dmxxx_resource.h>
#include <dmxxx_lib/dmxxx_calls.h>

#include <adapter/dmdevice.h>

#include <adapter/debug.h>
#include <adapter/interface.h>
#include <adapter/recorder.h>
#include <adapter/usb.h>
#include <adapter/copy_data.h>
#include <adapter/settings.h>
#include <adapter/module.h>
#include <adapter/power.h>
#include <adapter/firmware.h>
#include <adapter/diagnostic.h>
#include <adapter/httpd.h>
#include <adapter/sleep.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif

/**
 * @brief �������, ������� ���������, � �� �������� �� ��������� �����
 *
 * @param InterfacePriv - ��������� �� ��������� ������ ����������
 * @param Statu - ��������� �� ���������� ������ ����������
 *
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 */
static int AdapterInterfaceCheckZombi( dmdev_user_interface_t *InterfacePriv, AdapterGlobalStatusStruct_t *Status )
{
	if( !InterfacePriv || !Status )
	{
		DBGERR( "%s: Invalid argument\n", __func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	DBGLOG( "%s: Interface application worked from pid %d\n", __func__, InterfacePriv->pid );

	//��� ��� ��������� ���������� �� �������� - ��������, ��� � ������� �����,
	//� ������ ��� ����� ���������� ��� ������ (���� �� �� ���� ��� ���������!!)
	if( InterfacePriv->pid )
	{
		//���� ��� ������ �� ������� - �������� PID ������ � ���������� ��� (����� ��� ���� ������ ��� ����� �������)
		char ProcessName[ 1024 ];

		int iret = axis_get_process_name( ProcessName, sizeof( ProcessName ), InterfacePriv->pid );

		if( iret < 0 )
		{
			DBGERR( "%s: Can't get nanme for process at pid %d\n", __func__, InterfacePriv->pid );
			return iret;
		}
		else
		{
			//������� ��� �������� � ���������
			ProcessName[ sizeof( ProcessName ) - 1 ] = 0x00;
			DBGLOG( "%s: Interface application '%s' (%s) worked from pid %d\n", __func__, ProcessName,
								Status->GlobalDevice_t.dmdev_interface.dmdev_device_name, InterfacePriv->pid );

			if( strcmp( ProcessName, Status->GlobalDevice_t.dmdev_interface.dmdev_device_name ) )
			{
				DBGERR( "%s: Application at pid %d have a invalid name '%s'(%s)\n", __func__, InterfacePriv->pid,
												ProcessName, Status->GlobalDevice_t.dmdev_interface.dmdev_device_name );
				//��� �� ���� ������ - ������ �� ����� ���� ������ ����� ��������
				InterfacePriv->pid = 0;
				return 0;
			}
			else
			{
				//������� ������ ������� ��������
				int pState = axis_get_process_state(  InterfacePriv->pid, ProcessName );

				if( pState < 0 )
				{
					DBGERR( "%s: Can't get process state for process name '%s'\n", __func__, ProcessName );
					return pState;
				}
				else
				{
					DBGLOG( "%s: Interface application '%s' worked from pid %d have a state '%c'\n", __func__,
								ProcessName, InterfacePriv->pid, pState );

					//������ �� ������ ��������� ��� ������ � ��� ���� �����
					if( ( pState == 'r' || pState == 's' || pState == 'S' || pState == 'R' ) )
					{
						DBGLOG( "%s: Interface application '%s' at PID %d have a normal state '%c'\n", __func__,
										Status->GlobalDevice_t.dmdev_interface.dmdev_device_name, InterfacePriv->pid, pState );
						//���������� �������� ��������� �������� ��� �� � ��� �� ������
						return 0;
					}
					else if( pState == 'z' || pState == 'Z' )
					{
						int status;

						//������� ����� � ���� ��������
						iret = waitpid( InterfacePriv->pid, &status, 0 );

						if( iret < 0 )
						{
							DBGERR( "%s: Waitpid for %d return %d errno=%d (%s)\n", __func__, InterfacePriv->pid, iret, errno, strerror( errno ) );
						}
						else
						{
							InterfacePriv->pid = 0; //��� ������ �� ������ ������
						}

					}
					else
					{
						DBGERR( "%s: Interface application '%s' worked from pid %d have a invalid state '%c' (Z)\n", __func__,
																							ProcessName, InterfacePriv->pid, pState );
						return -AXIS_ERROR_INVALID_ANSED;
					}
				}
			}
		}
	}

	return 0;
}

pid_t AdapterInterfaceStart( dmdev_t *dev, int noIntterface )
{

	if( !dev )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	if( noIntterface )
	{
		dev->dmdev_interface.dmctrl_status |=  DM_INTERFACE_STATUS_IN_SLEEP;
		DMXXXInterfaceDisplayOff( dev );
		return 1;
	}

	//���� ������� ��������� �� ������� � ���������� ��������������� ���� � �����������
	//�� �� �� ��������� ��������� - ��� �� ��� ��������� ���� ������ �� ��������
	if( dev->dmdev_interface.dmctrl_feature & DM_INTERFACE_DONT_START_ON_TIMER &&
		dev->dmdevice_powerupstatus == DISPATCHER_LIB_EVENT__TIMER )
	{
		dev->dmdev_interface.dmctrl_status |=  DM_INTERFACE_STATUS_IN_SLEEP;
		dev->dmdev_interface.dmctrl_feature &= ~DM_INTERFACE_DONT_START_ON_TIMER;

		//��� ��� �� ���� � �� ����������� - ��������� ������� (�� ������ ������)
		DMXXXInterfaceDisplayOff( dev );
		return 1;
	}

	pid_t ret = fork();

	if( ret < 0 )
	{
		DBGERR( "%s: Can't fork for new thread\n", __func__ );
		return -AXIS_ERROR_CANT_CREATE_THREAD;
	}

	if( ret )
	{
		//��� ��������

		DBGLOG( "%s: Fork return %d pid\n", __func__, ret );
		//��� ��� execv ����� ���������� �������
		//�� ��� �� ����� ����� �� ������ - ������ �����

		//������������� ���� �������� ������ ������

		//����� ���� ������ ����������
		sleep( 1 );

		//������� ������ ������
		int sret = axis_get_process_state( ret,
					dev->dmdev_interface.dmdev_device_name );

		DBGLOG( "%s: Process %d have a %d (%c) state\n",
		        __func__, ret, sret, sret );

		if( sret < 0 )
		{
			DBGERR( "%s: Can't get status for process\n", __func__ );
			return -AXIS_ERROR_CANT_CREATE_THREAD;
		}

		if( !sret )
		{
			int status;

			//0 - ������ ��� ������ ������
			DBGERR( "%s: Process not started\n", __func__ );

			//�� ��� ���� � ��� ����� �������� ����� ����������
			//������ ������� ��� ���� ����
			waitpid( ret, &status, WNOHANG );


			return -AXIS_ERROR_CANT_CREATE_THREAD;
		}

		if( sret == 'z' || sret == 'Z' )
		{
			int status;
			DBGERR( "%s: Process started at zombie error\n", __func__ );

			waitpid( ret, &status, WNOHANG );
			return -AXIS_ERROR_CANT_CREATE_THREAD;
		}


		if( !( sret == 'r' || sret == 's' || sret == 'S' || sret == 'R' ) )
		{
			//������ �� �������� � �� ����
			DBGERR( "%s: Invald running state for process '%c'\n",
			        __func__, sret );
// 			return -AXIS_ERROR_CANT_CREATE_THREAD;
		}

		//������� ��� ����� ������� �������
		DBGLOG( "%s: '%s' started at %d PID\n",
		        __func__, dev->dmdev_interface.dmdev_device_name, ret );

		return ret;
	}


	char argv_0[1024];

	char argv_1[1024];

	char argv_2[1024];

	char argv_3[1024];

	//------------------------------------------------------------------------
	//���� ������� ���� ����������� �������� �����������
	//��� �� �� ������ �� ���� ����� �������
	//��� ����������� �� axis_lock.c ��� ��� �������� �� ����������

	//�������� ���� �������������� ������������
	int i;

	for( i = getdtablesize() - 1; i >= 0; --i )
		( void )close( i );

	//���������� �� ������������ ���������
	i = open( "/dev/tty", O_RDWR );

	( void )ioctl( i, TIOCNOTTY, 0 );

	( void )close( i );

	//������������ ����� �������
	( void )umask( 027 );

	//������� ����������� ���������� ����
	i = open( "/dev/null", O_RDWR );

	ret = dup( i );

	ret = dup( i );//���������� ��������� �� �������

	//------------------------------------------------------------------------
	char path[32];

	snprintf( path, sizeof( path ), "/usr/bin/%s",
					dev->dmdev_interface.dmdev_device_name );

	//��������� ���� ��� ������
	strncpy( argv_0, path, sizeof( argv_0 ) );

	argv_1[ sizeof( argv_1 ) - 1 ] = 0x00;

	//������ �������� ������ ���� ��� �������� �� ������� ��� ��������
	strcpy( argv_1, "-qws" );

	strncpy( argv_2,
		( ( dmdev_user_interface_t* )dev->dmdev_interface.privatedata )->socketinname, sizeof( argv_2 ) );

	argv_2[ sizeof( argv_2 ) - 1 ] = 0x00;

	strncpy( argv_3,
		 ( ( dmdev_user_interface_t* )dev->dmdev_interface.privatedata )->socketoutname, sizeof( argv_3 ) );

	argv_3[ sizeof( argv_3 ) - 1 ] = 0x00;

	char *argv[5];

	argv[0] = argv_0;

	argv[1] = argv_1;

	argv[2] = argv_2;

	argv[3] = argv_3;

	argv[4] = NULL;

	execv( path, argv );

	exit( 0 );
}

//������� ���������� ������ �����������
//����:
// ssock - ��������� ����� �� ������� ���� ������
//�������:
// 0 - ��� ��
// ����� 0 � ������ ������
int AdaptrerInterfaceFlushSecondConnect( int ssock )
{

	int csock;

	if( ssock < 0 )
	{
		DBGERR( "%s: Server socket not init\n", __func__ );
		return -AXIS_ERROR_DEVICE_NOT_OPEN;
	}

	csock = DispatcherLibNetworkUnixServiceAccept( ssock );

	if( csock < 0 )
	{
		DBGERR( "%s: Can't accept new client\n", __func__ );
		return csock;
	}

	DispatcherLibNetworkUnixServiceClose( csock );

	return AXIS_NO_ERROR;
}


int AdapterInterfaceInitSocket( dmdev_t *dev )
{
	if( !dev || !dev->dmdev_interface.privatedata )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	if( ( ( dmdev_user_interface_t* )dev->dmdev_interface.privatedata )->communication_type &
						DM_INTERFACE_CONNECTION_UNIX )
	{

		( ( dmdev_user_interface_t* )dev->dmdev_interface.privatedata )->socketin =
			DispatcherLibNetworkUnixServiceInit( ( ( dmdev_user_interface_t* )dev->dmdev_interface.privatedata )->socketinname );

		( ( dmdev_user_interface_t* )dev->dmdev_interface.privatedata )->socketout =
			DispatcherLibNetworkUnixServiceInit( ( ( dmdev_user_interface_t* )dev->dmdev_interface.privatedata )->socketoutname );

		sem_init( &( ( dmdev_user_interface_t* )dev->dmdev_interface.privatedata )->semaphoreout, 0 , 1 );

	}

	return 0;
}

int AdapterInterfaceSetLcdBrightness(dmdev_t* dev,int value)
{
	if( !dev )
	{
		DBGERR("%s:%i: Invalid Argumentn\n",__func__,__LINE__);
		return -1;
	}

	return DMXXXInterfaceDisplayBright(dev,value);
}

/**
 * @brief ��������������� ����������������� ����������
 *
 * @param dev ��������� �� ���������� ��������� ����������
 * @return 0 - �� ����� - ������
 **/
int AdapterInterfaceDeInit( dmdev_t *dev )
{
	dmdev_user_interface_t *interface;

	if( !dev )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	if( !dev->dmdev_interface.dmctrl_ipresent )
	{
		DBGLOG( "%s: Interface not present in system\n", __func__ );
		return 0;
	}

	if( !dev->dmdev_interface.privatedata )
	{
		DBGERR( "%s: No private data for interface \n", __func__ );
		return -AXIS_ERROR_INVALID_VALUE;
	}

	interface = ( dmdev_user_interface_t* )dev->dmdev_interface.privatedata;

	if( interface->communication_type & DM_INTERFACE_CONNECTION_UNIX )
	{
		if( interface->socketin > 0 )
		{
			DispatcherLibNetworkUnixServiceClose( interface->socketin );
			interface->socketin = -1;
		}

		if( interface->socketout > 0  )
		{
			DispatcherLibNetworkUnixServiceClose( interface->socketout );
			interface->socketout = -1;
		}

		if( interface->sockethenin > 0 )
		{
			close( interface->sockethenin );
			interface->sockethenin = -1;
		}

		if( interface->sockethenout > 0 )
		{
			close( interface->sockethenout );
			interface->sockethenout = -1;
		}
	}

	//�� ����� ������� ���������, ������ ���� �� ����
	if( interface->pid > 0 )
	{
		kill( interface->pid, SIGTERM );
		interface->pid = -1;
	}

	return 0;

}

/**
 * @brief ������������� ����������������� ����������
 *
 * @param dev ��������� �� ���������� ��������� ����������
 * @param noIntterface - �� ��������� ���������, ���� ���� ����� �������
 * @return 0 - �� ����� - ������
 **/
int AdapterInterfaceInit( dmdev_t *dev, int noIntterface )
{
	dmdev_user_interface_t *interface;
	int ret = 0;


	if( !dev )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	if( !dev->dmdev_interface.dmctrl_ipresent )
	{
		DBGLOG( "%s: Interface not present in system\n", __func__ );
		return 0;
	}

	if( !dev->dmdev_interface.privatedata )
	{
		DBGERR( "%s: No private data for interface \n", __func__ );
		return -AXIS_ERROR_INVALID_VALUE;
	}

	interface = ( dmdev_user_interface_t* )dev->dmdev_interface.privatedata;

	//� ��� ���� ��� ��� ���������� ��� �������� - �� ��� �������� �����
	interface->pid = 0;

	ret = DMXXXInterfaceInit( dev );

	if( ret < 0 )
	{
		DBGERR( "%s:It wan't possible to init interface lib\n", __func__ );
		return -1;
	}

	ret = AdapterInterfaceInitSocket( dev );

	if( ret < 0 )
	{
		DBGERR( "%s:It wasn't possible to init socket\n", __func__ );
		return -1;
	}

	interface->pid = AdapterInterfaceStart( dev, noIntterface );

	if( interface->pid <= 0 )
	{
		DBGERR( "%s:It wasn't possible to start interface process %i\n", __func__, interface->pid );
		return -1;
	}

	return 0;
}

/**
 * @brief �������, ������ ����� � ����� ������
 *
 * @param fileout - ��������� �� ����� ������
 * @param TCount - ��������� �� ������� ������ ��� �� �� ��������
 * @param FileName - ��������� �� ��� ����� ��� ������
 * @param Message - ��������� �� ��������� ��������
 * @return int - ���������� ���������� ����, ����� 0 � ������ ������
 */
int AdapterInterfacePrintFile( FILE* Fileout, uint32_t *TCount, const char *FileName, const char *Message )
{
	int dev;
	int fd;
	int ret = 0;
	int rcount = 0;
	char buffer[ 32000 ]; //����� ��� ������

	if( !Fileout || !TCount || !FileName || !Message )
	{
		DBGERR( "%s: Invalid argument\n", __func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	dev = fileno( Fileout );

	fd = open( FileName, O_RDONLY );

	if( fd < 0 )
	{
		DBGERR( "%s: It wasn't possible to open file '%s'\n", __func__, FileName );
		return -AXIS_ERROR_CANT_OPEN_DEVICE;
	}

	fprintf( Fileout, "------- %s -------\n", Message );

	while( ( ret = read( fd, buffer, sizeof( buffer ) ) ) > 0 )
	{
		if( write( dev, buffer, ret ) < 0 )
		{
			//����������� �������� � �����
			DBGERR( "%s: Can't write to out\n", __func__ );

			goto end;
		}

		rcount += ret;
	}

	if( ret < 0 )
	{
		DBGERR( "%s: Can't read file '%s'\n", __func__, FileName );
		ret = -AXIS_ERROR_CANT_READ_DEVICE;
	}

	fprintf( Fileout, "-----------END --------\n" );

end:

	close( fd );

	if( ret < 0 )
	{
		return ret;
	}

	return rcount;
}

/**
 * @brief �������, ������� ���������� ������� � ��������� ������ �� ���������
 *
 * @param Status - ��������� �� ������ ���������
 * @param cmd - ������� ��� �������� � ���������
 * @param value - �������� ��� ������ �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 */
int AdapterInterfaceSendCommandAndValue( AdapterGlobalStatusStruct_t *Status, uint16_t Cmd, int Value )
{
	int ret = 0;

	if( !Status )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	if( !Status->GlobalDevice_t.dmdev_interface.dmctrl_ipresent )
	{
		//��� ���������� - ��� �������
		return 0;
	}

	DBGLOG( "%s:%i: SEND COMMAND %x to interface. Value=%d\n", __func__, __LINE__ , Cmd, Value );

	if( ( ( dmdev_user_interface_t* )Status->GlobalDevice_t.dmdev_interface.privatedata )->communication_type & DM_INTERFACE_CONNECTION_UNIX &&
		( ( dmdev_user_interface_t* )Status->GlobalDevice_t.dmdev_interface.privatedata )->sockethenout > 0 )
	{
		SoftRecordeExchangeStruct_t Data;
		int32_t *dValue = (int32_t *)Data.Data; //�������� ������ � �������� int � ������

		memset( &Data, 0, sizeof( Data ) );

		*dValue = Value;

		ret = DispatcherLibRequestCreateExchange( ( uint16_t * )&Data, Cmd, sizeof( SoftRecordeExchangeStruct_t ) );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't create echange for send command to interface\n", __func__ );
			return ret;
		}

		ret = DispatcherLibExchange( ( ( dmdev_user_interface_t* )Status->GlobalDevice_t.dmdev_interface.privatedata )->sockethenout,
									 &( ( dmdev_user_interface_t* )Status->GlobalDevice_t.dmdev_interface.privatedata )->semaphoreout,
									 ( uint16_t* )&Data, sizeof( SoftRecordeExchangeStruct_t ) );

		if( ret < 0 )
		{
			DBGERR( "%s: It wasn't possible to send command to interface 0x%04x ret=%d\n", __func__, Cmd, ret );

			//TODO - ����� ��� ���� ��� �� �������?? ��������, ������� ��� ������ � �������� ��������� ���������??
			close( (( dmdev_user_interface_t* )Status->GlobalDevice_t.dmdev_interface.privatedata )->sockethenout );
			close( (( dmdev_user_interface_t* )Status->GlobalDevice_t.dmdev_interface.privatedata )->sockethenin  );
			(( dmdev_user_interface_t* )Status->GlobalDevice_t.dmdev_interface.privatedata )->sockethenout = -1;
			(( dmdev_user_interface_t* )Status->GlobalDevice_t.dmdev_interface.privatedata )->sockethenin  = -1;
			//TODO - ���� �� ��� �� ���������� � ������ ����������!!!

			return ret;
		}
	}

	return ret;
}

int AdapterInterfaceLcdOff( AdapterGlobalStatusStruct_t *Status )
{
	if( !Status )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	return DMXXXInterfaceDisplayOff(&Status->GlobalDevice_t);
}

int AdapterInterfaceLcdOn( AdapterGlobalStatusStruct_t *Status, int iNoInterface )
{
	int ret;

	if( !Status )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	Status->LastKeyPressTest  = time( NULL ); //����� �������, ��� ������ ������ ������ ���

	ret = DMXXXInterfaceDisplayOn( &Status->GlobalDevice_t );

	if( ret < 0 )
	{
		DBGERR( "%s: Can't turn on device dispaly\n", __func__ );
		return ret;
	}

	if( !ret )
	{
		pid_t ipid = 0;

		//����� ��� ��������, ��� �� ���������� ���������� ���������� ��� �������
		//� ��� ����� ������ �������� ��������� �� ����� - ����������� ��� ���� ��� ���

		ret = DMXXXInterfaceGetPid( &Status->GlobalDevice_t, &ipid );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't get interface application pid\n", __func__ );
		}
		else if( !ipid && !iNoInterface )
		{
			dmdev_user_interface_t *interface;

			interface = ( dmdev_user_interface_t* )Status->GlobalDevice_t.dmdev_interface.privatedata;

			DBGERR( "%s: Interface application crashed without display. Try restart it\n", __func__ );

			interface->pid = AdapterInterfaceStart( &Status->GlobalDevice_t, 0 ); //0 - ��� ��� ��������� ��� ���������� �����

			if( interface->pid <= 0 )
			{
				DBGERR( "%s: It wasn't possible to start interface process %i\n", __func__, interface->pid );
			}

			INFO_LOG( interface->pid < 0 ? 1 : 0, "interface restart" );

			if( interface->pid > 0 )
			{
				Status->GlobalDevice_t.dmdev_interface.dmctrl_status &= ~DM_INTERFACE_STATUS_IN_SLEEP;
				Status->LastKeyPressTest = time( NULL ); //���� ��������� ��������� - ������ ������������ ����� �� ������!!!
			}
		}
		else
		{
			DBGLOG( "%s: Intrafce application worked at pid %d\n", __func__, ipid );
		}
	}

	return AXIS_NO_ERROR;
}

/**
 * @brief - ������� ���������� ���������� �������� �������� �� HTTP ����������
 *
 * @param HTTPD - ��������� �� ��������� ������� �������
 * @return int
 **/
static int AdapterInterfaceGetHTTPClientCount( AdapterHTTPDStatusStruct_t *HTTPD )
{
	int i;
	int count = 0;

	for( i = 0; i < HTTPD->Server.clients_count; i++ )
	{
		axis_ssl_httpi_server_client *clients = ( axis_ssl_httpi_server_client * )HTTPD->Server.clients;
		axis_ssl_httpi_server_client *client = &clients[i];

		if( client->pid && !client->return_code && client->control_name[0] != 0x00 )
		{
			count++;
		}
	}

	return count;
}

static size_t AdapterInterfaceGetHTTPClientsPids( AdapterHTTPDStatusStruct_t *HTTPD, pid_t *pidsArray, size_t pidsArraySize )
{
	int i;
	size_t count = 0;

	for( i = 0; i < HTTPD->Server.clients_count; i++ )
	{
		axis_ssl_httpi_server_client *clients = ( axis_ssl_httpi_server_client * )HTTPD->Server.clients;
		axis_ssl_httpi_server_client *client = &clients[i];

		if( client->pid && !client->return_code && client->control_name[0] != 0x00 )
		{
			pidsArray[ count ] = client->pid;

			count++;

			if( count >= pidsArraySize )
			{
				break;
			}
		}
	}

	return count;
}


/**
 * @brief ������� ��������� ������ �������� �� ����������
 *
 * @param hen - ������ ��������� ���������� ������ � �����������
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 ��� ��
 */
static int AdapterInterfaceDoCommandFrom( int hen, AdapterGlobalStatusStruct_t *Status )
{
	uint16_t cmd  = 0x0000; //������� ������� � ��� ������
	int ret = 0;
	int value;
	SoftRecorderExchangeControlStruct_t Data[ DISPATCHER_LIB_EXCHANGE_MAX_BLOCK_COUNT ];

	if( !Status )
	{
		DBGERR( "%s:Invalid argument\n", __func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	if( hen <= 0 )
	{
		DBGERR( "%s: Interface socket not opened\n", __func__ );
		return -AXIS_ERROR_DEVICE_NOT_OPEN;
	}

	DBGLOG( "%s:%i: Command from interface\n", __func__, __LINE__ );

	ret = DispatcherLibReciveFromClent(
	              hen,
	              0, //����� ������
	              DISPATCHER_LIB_EXCHANGE_TIMOUT,
	              ( uint16_t * )Data,
	              sizeof( Data ) / sizeof( SoftRecordeExchangeStruct_t ) ); //������� ������ �����������

	if( ret < 0 )
	{
		//�������� ������ ��� ������ ������

		DBGERR( "%s: Can't recive request from partner\n", __func__ );
		//�� ����� ���� ��� ����� ���� ������ � ���, ��� ������
		//������ �����������

		if( ret == -AXIS_ERROR_CANT_READ_SOCKET )
		{
			DBGERR( "%s: Closed partner socket\n", __func__ );
		}

		return ret;
	}

	if( AdapterDmDeviceWatchDogTick( &Status->GlobalDevice_t ) )
	{
		DBGERR( "%s:It wasn't possible to tick eatch dog\n", __func__ );
	}

	//���� ������� ���� ����� - �� �������� �� ������� ���������� ��� �� ������������
	if( Status->AdapterNeedSleep )
	{
		goto norecorder;
	}

	switch( Data[0].ID )
	{

		case AXIS_DISPATCHER_EXCHANGE_BLOKS__ID:
		{
			//� ����� ������� ������ ��� ������� ����� � ������ �����
			//� ��� ��� �� � ���������� ��������� ������ �� ���� - ������� ����������
			//������� ������
			SoftRecordeExchangeExtBlockStruct_t *nData = ( SoftRecordeExchangeExtBlockStruct_t * )Data;
			cmd = nData[0].Command;
			break;
		}

		case AXIS_SOFTRECORDER_EXCHANGE__ID:
		{
			cmd = Data[0].Command;
			break;
		}

		default:
		{
			DBGERR( "%s; Invalid id for data buffer %04x\n", __func__,
			        Data[ 0 ].ID );
			return -AXIS_ERROR_INVALID_VALUE;
		}
	}

	DBGLOG( "%s:%i:Comman from interface 0x%04x\n", __func__, __LINE__, cmd );

	switch( cmd )
	{
		case AXIS_DISPATCHER_CMD_GET_ADAPTER_STATUS:
		{
			DBGLOG( "%s: Command query adapter status\n", __func__ );

			SoftAdapterExchangeConnectionStatusStruct_t connstatus;
			memset( &connstatus, 0, sizeof( connstatus ) );
            int count = 0;

			count += AdapterInterfaceGetHTTPClientCount( &Status->HTTPDMain );
			count += AdapterInterfaceGetHTTPClientCount( &Status->HTTPDSec );

			connstatus.Status.userscount = count;

			if( count > 0 )
			{
				size_t i;

				pid_t mainClientsPIDs[ 10 ];
				pid_t secClientsPIDs[  10 ];

				size_t mCount = AdapterInterfaceGetHTTPClientsPids( &Status->HTTPDMain, mainClientsPIDs, sizeof( mainClientsPIDs ) / sizeof( mainClientsPIDs[0] ) );
				size_t sCount = AdapterInterfaceGetHTTPClientsPids( &Status->HTTPDSec,  secClientsPIDs,  sizeof( secClientsPIDs  ) / sizeof( secClientsPIDs[0]  ) );

				size_t maxIpStatusSize = sizeof( connstatus.Status.clientIp ) / sizeof( connstatus.Status.clientIp[0] );

				for( i = 0; i < mCount; i++ )
				{
					if( i < maxIpStatusSize )
					{
						connstatus.Status.clientIp[i] = AdapterHttpdGetClientIP( mainClientsPIDs[i], Status, NULL );
					}
				}

				for( i = 0; i < sCount; i++ )
				{
					if( i + mCount < maxIpStatusSize )
					{
						connstatus.Status.clientIp[i + mCount] = AdapterHttpdGetClientIP( secClientsPIDs[i], Status, NULL );
					}
				}
			}

			DBGLOG( "%s:%i:Clients coun = %i    %i\n", __func__, __LINE__, connstatus.Status.userscount, count );
			connstatus.Status.usbdirection = ( Status->GlobalDevice_t.dmdev_usb.dmctrl_status & DM_USB_SWITCH_INTERNAL );
			//Take by default zero interface
			connstatus.Status.ConnecionTime =  system_time_translate( Status->Net[0].status.connection_time );
			connstatus.Status.ConnectionLevel = Status->Net[0].status.level;
			connstatus.Status.ConnectionQual = Status->Net[0].status.quality;

			strncpy( connstatus.Status.version, Status->Build, sizeof( connstatus.Status.version ) );

			connstatus.Status.datebuild = Status->BuildTime;

			if( Status->Net[0].status.status == DM_NET_CONNECTED )
			{
				connstatus.Status.iconnected = 1;
				connstatus.Status.ip = Status->Net[0].status.ip;
			}
			else
			{
				connstatus.Status.iconnected = 0;
			}

			//���� ����������� �� DHCP - �� ������ ������ ������ � ���
			memcpy( &connstatus.Status.DHCP, &Status->DHCPNetParam, sizeof( struct struct_net_param ) );

			connstatus.Status.recorderscount = Status->Recorder.recorderscount ;

			if( Status->Recorder.recorderscount > 0 )
			{
				connstatus.Status.sn = Status->Recorder.next->sn;

				connstatus.Status.status =  AdapterRecorderGetStatusThreads( Status->Recorder.next );
				DBGLOG( "%s: STATUS = %016"PRIX64"\n", __func__, connstatus.Status.sn );
			}

			ret = DispatcherLibRequestCreateExchange(
								( uint16_t * )&connstatus,
								AXIS_DISPATCHER_CMD_GET_ADAPTER_STATUS,
								sizeof( SoftAdapterExchangeConnectionStatusStruct_t ) );

			if( ret < 0 )
			{
				DBGERR( "%s: Cant cretate exchange for send adapter status\n", __func__ );
				return ret;
			}

			ret = DispatcherLibSendToClent(
								hen,
								10,
								( uint16_t * )&connstatus,
								sizeof( SoftAdapterExchangeConnectionStatusStruct_t ) );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't send adapter status\n", __func__ );
				return ret;
			}

			break;
		}
		case AXIS_DISPATCHER_CMD_GET_STATUS:
		{
			DBGLOG( "%s:%i: Command query recorder status\n", __func__, __LINE__ );

			if( Status->Recorder.recorderscount >= 1 )
			{
				ret = AdapterRecorderGetStatus( &Status->GlobalDevice_t, Status->Recorder.next );
				if( ret < 0 )
				{
					DBGERR( "%s: It wasn' possible to get recorder status\n", __func__ );
					goto error;
				}

				ret = DispatcherLibRequestCreateExchange(
								( uint16_t * )Status->Recorder.next->recorder_data.recorderstatus_ex,
								AXIS_DISPATCHER_CMD_GET_STATUS,
								sizeof( SoftRecordeExchangeStruct_t ) );

				if( ret < 0 )
				{
					DBGERR( "%s: Cant cretate exchange for send recorder status\n", __func__ );
					return ret;
				}

				ret = DispatcherLibSendToClent(
								hen,
								TIMEOUT_EXCHANGE_INTERFACE,
								( uint16_t * ) Status->Recorder.next->recorder_data.recorderstatus_ex,
								sizeof( SoftRecordeExchangeStruct_t ) );

				if( ret < 0 )
				{
					DBGERR( "%s: Can't send recorder status to interface\n", __func__ );
					return ret;
				}

			}

			else
			{
				goto norecorder;
			}

			break;
		}
		case AXIS_DISPATCHER_CMD_GET_PARAM:
		{
			DBGLOG( "%s:%i: Command query recorder settings\n", __func__, __LINE__ );

			if( Status->Recorder.recorderscount >= 1 )
			{
				ret = AdapterRecorderChacheRecorderSettings( &Status->GlobalDevice_t, Status->Recorder.next );

				if( ret < 0 )
				{
					DBGERR( "%s: It wasn' possible to get recorder settings\n", __func__ );
					goto error;
				}

				ret = DispatcherLibRequestCreateExchange(
								( uint16_t * )Status->Recorder.next->recorder_data.recordersettings_ex,
								AXIS_DISPATCHER_CMD_GET_PARAM,
								sizeof( SoftRecordeExchangeStruct_t ) );

				if( ret < 0 )
				{
					DBGERR( "%s: Cant cretate exchange for get recorder settings\n", __func__ );
					break;
				}

				ret = DispatcherLibSendToClent(
								hen,
								TIMEOUT_EXCHANGE_INTERFACE,
								( uint16_t * ) Status->Recorder.next->recorder_data.recordersettings_ex,
								sizeof( SoftRecordeExchangeStruct_t ) );

				if( ret < 0 )
				{
					DBGERR( "%s: Can't send recorder settings\n", __func__ );
					return ret;
				}
			}

			else
			{
				goto norecorder;
			}

			break;
		}

		case DSP_RECORDER_CMD_GET_TIMERS:
		{
			DBGLOG( "%s:%i: Command query recorder timers\n", __func__, __LINE__ );

			if( Status->Recorder.recorderscount >= 1 )
			{
				ret = AdapterRecorderChacheRecorderSettings( &Status->GlobalDevice_t, Status->Recorder.next );

				if( ret < 0 )
				{
					DBGERR( "%s: It wasn' possible to get recorder timers\n", __func__ );
					goto error;
				}

				ret = DispatcherLibRequestCreateExchange(
								( uint16_t * )Status->Recorder.next->recorder_data.recorderstimers_ex,
								DSP_RECORDER_CMD_GET_TIMERS,
								sizeof( SoftRecordeExchangeStruct_t ) );

				if( ret < 0 )
				{
					DBGERR( "%s: Cant cretate exchange for query recorder timers\n", __func__ );
					break;
				}

				ret = DispatcherLibSendToClent(
								hen,
								TIMEOUT_EXCHANGE_INTERFACE,
								( uint16_t * ) Status->Recorder.next->recorder_data.recorderstimers_ex,
								sizeof( SoftRecordeExchangeStruct_t ) );

				if( ret < 0 )
				{
					DBGERR( "%s: Can't send recorder timers to interface\n", __func__ );
					return ret;
				}

			}
			else
			{
				goto norecorder;
			}

			break;
		}
		case AXIS_DISPATCHER_CMD_SET_AUDIO_CFG:
			//NOTE - ���������� ���������� - ��� ��������� ���������� ������, ��� ������
			//���������� �������� � ����� - �� �� ���� ������ ��������� ���������� ����������
		case AXIS_DISPATCHER_CMD_SET_PARAM:
		{
#if defined( CONFIG_ENCODER_DEBUG_MSG ) || defined( CONFIG_ENCODER_DEBUG_ERR )
			const char *debug_string = cmd == AXIS_DISPATCHER_CMD_SET_PARAM ? "save" : "applay";
#endif

			DBGLOG( "%s:%i: Command query for %s recorder settings\n", __func__, __LINE__, debug_string );

			if( Status->Recorder.recorderscount >= 1 )
			{
				memcpy( Status->Recorder.next->recorder_data.recordersettings_ex, &Data[0], sizeof( SoftRecordeExchangeStruct_t ) );

				ret = AdapterRecorderPutSettings( &Status->GlobalDevice_t, Status->Recorder.next );

				if( ret < 0 )
				{
					DBGERR( "%s: Can't %s settings to recorder\n", __func__, debug_string );
					Data[0].Command = 0;
				}

				//� ����� �������� ��������� ���������� ������� - ��� ��� �� �� �� � ��������
				Status->InterfaceSleep = AdapterRecorderGetInterfaceSleepTimeFistRecorder( &Status->Recorder );

				ret = DispatcherLibSendToClent(
								hen,
								TIMEOUT_EXCHANGE_INTERFACE,
								( uint16_t * ) &Data[0],
								sizeof( SoftRecordeExchangeStruct_t ) );

				if( ret < 0 )
				{
					DBGERR( "%s: Can't send answer for %s recorder settings\n", __func__, debug_string );
					return ret;
				}

			}
			else
			{
				goto norecorder;
			}

			break;
		}
		case DSP_RECORDER_CMD_PUT_TIMERS:
		{
			DBGLOG( "%s:%i: Command query for save recorder timers\n", __func__, __LINE__ );

			if( Status->Recorder.recorderscount >= 1 )
			{
				memcpy( Status->Recorder.next->recorder_data.recorderstimers_ex, &Data[0], sizeof( SoftRecordeExchangeStruct_t ) );

				ret = AdapterRecorderPutTimers( &Status->GlobalDevice_t, Status->Recorder.next );

				if( ret < 0 )
				{
					DBGERR( "%s: Can't save recorder timers\n", __func__ );
					goto error;
				}

				ret = DispatcherLibSendToClent(
								hen,
								TIMEOUT_EXCHANGE_INTERFACE,
								( uint16_t * ) &Data[0],
								sizeof( SoftRecordeExchangeStruct_t ) );

				if( ret < 0 )
				{
					DBGERR( "%s: Can't send answer for save recorder settings\n", __func__ );
					return ret;
				}

			}
			else
			{
				goto norecorder;
			}

			break;
		}
		case AXIS_DISPATCHER_CMD_GET_ACC_STATE:
		{
			if( Status->Recorder.recorderscount >= 1 )
			{
				DBGLOG( "%s:%i: Command query device ACC status\n", __func__, __LINE__ );

				ret = AdapterRecorderGetAccStatus___( &Status->GlobalDevice_t, Status->Recorder.next );

				if( ret < 0 )
				{
					DBGERR( "%s: It wasn' possible to get ACC status\n", __func__ );
					goto error;
				}

				ret = DispatcherLibRequestCreateExchange(
								( uint16_t * )Status->Recorder.next->recorder_data.recorderaccstatus_ex,
								AXIS_DISPATCHER_CMD_GET_ACC_STATE,
								sizeof( SoftRecordeExchangeStruct_t ) );

				if( ret < 0 )
				{
					DBGERR( "%s: Cant cretate exchange for get ACC status\n", __func__ );
					return ret;
				}

				ret = DispatcherLibSendToClent(
								hen,
								TIMEOUT_EXCHANGE_INTERFACE,
								( uint16_t * ) Status->Recorder.next->recorder_data.recorderaccstatus_ex,
								sizeof( SoftRecordeExchangeStruct_t ) );

				if( ret < 0 )
				{
					DBGERR( "%s: Can't send ACC status to interface\n", __func__ );
				}

			}
			else
			{
				goto norecorder;
			}

			break;
		}

		case AXIS_DISPATCHER_CMD_LISTEN_CONTROL:
		case AXIS_DISPATCHER_CMD_RECORD_CONTROL:
		{
			DBGLOG( "%s:%i: Command record or listen contorl 0x%04x\n", __func__, __LINE__, cmd );

			if( Status->Recorder.recorderscount >= 1 )
			{
				//�� ����������� ������� �������� � ����������
				//����������� ����� � ������ �� ���������
				ret = AdapterRecorderSendControl( Status, Status->Recorder.next,
												  (uint8_t *)Data, sizeof( SoftRecorderExchangeControlStruct_t ) );

				if( ret < 0 )
				{
					DBGERR( "%s: Can't send control command to recorder\n", __func__ );
				}
			}

			else
			{
				goto norecorder;
			}

			break;
		}

		case  AXIS_INTERFACE_EVENT_START_PLAYER:
		{

			Status->iplay = 1;

			DBGERR( "%s:%i: Command query set play status flag\n", __func__, __LINE__ );

			if( Status->Recorder.recorderscount >= 1 )
			{
				ret = AdapterRecorderSendCommand( Status, Status->Recorder.next,
												  ADAPTER_INTERFACE_COMMAND__STOP_UPLOAD, NULL, 0 );

				if( ret < 0 )
				{
					DBGERR( "%s: It wasn' possible to stop recorder uploading\n", __func__ );
					//��� �� ����������� ������
				}

				ret = DispatcherLibSendToClent(
									hen,
									TIMEOUT_EXCHANGE_INTERFACE,
									( uint16_t * ) &Data[0],
									sizeof( SoftRecordeExchangeStruct_t ) );

				if( ret < 0 )
				{
					DBGERR( "%s: Can't send answer for set play status flag command\n", __func__ );
					return ret;
				}
			}

			else
			{
				goto norecorder;
			}

			break;
		}
		case  AXIS_INTERFACE_EVENT_STOP_PLAYER:
		{

			Status->iplay = 0;

			DBGLOG( "%s:%i: Command query clear play status flag\n", __func__, __LINE__ );

			ret = DispatcherLibSendToClent(
									hen,
									TIMEOUT_EXCHANGE_INTERFACE,
									( uint16_t * ) &Data[0],
									sizeof( SoftRecordeExchangeStruct_t ) );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't send answer for clear play status flag\n", __func__ );
				return ret;
			}

			break;
		}
		case AXIS_INTERFACE_CMD_GO_USB_DIR:
		{

			DBGLOG( "%s:%i: Command query recorder count\n", __func__, __LINE__ );

			SoftRecordeExchangeStruct_t Send;

			memset( &Send, 0, sizeof( SoftRecordeExchangeStruct_t ) );

			Send.Data[0] = Status->Recorder.recorderscount;

			DBGERR( "%s: Recorder count is %i \n", __func__, Send.Data[0] );

			ret = DispatcherLibRequestCreateExchange(
								( uint16_t * )&Send,
								AXIS_INTERFACE_CMD_GO_USB_DIR,
								sizeof( SoftRecordeExchangeStruct_t ) );

			if( ret < 0 )
			{
				DBGERR( "%s: Cant cretate exchange for get recorder count command\n", __func__ );
				return ret;;
			}

			ret = DispatcherLibSendToClent(
								hen,
								TIMEOUT_EXCHANGE_INTERFACE,
								( uint16_t * )&Send,
								sizeof( SoftRecordeExchangeStruct_t ) );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't send answer for get recorder count\n", __func__ );
				return ret;
			}

			break;
		}

		case AXIS_DISPATCHER_CMD_SWITCH_USB_OUT:
		{

			DBGLOG( "%s:%i: Command query switch USB to out\n", __func__, __LINE__ );

			Status->usbswitches++;

			DBGLOG( "%s: USB switch count is %i \n", __func__, Status->usbswitches );

			ret = AdapterUsbSwithExternal( &Status->GlobalDevice_t );

			if( ret < 0 )
			{
				DBGERR( "%s: Cant switch usb to external\n", __func__  );
				goto error;
			}

			ret = DispatcherLibSendToClent(
								hen,
								TIMEOUT_EXCHANGE_INTERFACE,
								( uint16_t * ) &Data[0],
								sizeof( SoftRecordeExchangeStruct_t ) );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't send answer for switch USB out command\n", __func__ );
				return ret;
			}

			break;
		}

		case AXIS_DISPATCHER_CMD_SWITCH_USB_IN:
		{

			DBGLOG( "%s:%i: Command query swicth USB to in\n", __func__, __LINE__ );

			if( Status->usbswitches )
			{
				Status->usbswitches--;
			}

			DBGLOG( "%s: USB switch count is %i \n", __func__, Status->usbswitches );

			if( !Status->usbswitches )
			{

				ret = AdapterUsbSwithInternal( &Status->GlobalDevice_t );

				if( ret < 0 )
				{
					DBGERR( "%s: Cant switch usb to internal\n", __func__ );
					goto error;
				}
			}

			ret = DispatcherLibSendToClent(
								hen,
								TIMEOUT_EXCHANGE_INTERFACE,
								( uint16_t * ) &Data[0],
								sizeof( SoftRecordeExchangeStruct_t ) );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't send answer for switch USB in command\n", __func__ );
				return ret;
			}

			break;
		}

		case AXIS_DISPATCHER_CMD_START_COPY_TO_FILE:
		{
			DBGLOG( "%s:%i: Command query fro start copy data to file\n", __func__, __LINE__ );

			SoftAdapterCopyDataStatus_t *CopyStatus;

			CopyStatus = ( SoftAdapterCopyDataStatus_t * )&Data[0].Control;

			ret = AdapterCopyDataStart( CopyStatus, Status );

			if( ret < 0 )
			{
				DBGERR( "%s: It wasn't possible to start copy\n", __func__ );
				goto error;
			}

			ret = DispatcherLibSendToClent(
								hen,
								TIMEOUT_EXCHANGE_INTERFACE,
								( uint16_t * ) &Data[0],
								sizeof( SoftRecordeExchangeStruct_t ) );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't send answer for start copy command\n", __func__ );
				return ret;
			}

			break;
		}

		case AXIS_DISPATCHER_CMD_COPY_TO_FILE_STATUS:
		{
			DBGLOG( "%s:%i: Command query copy data status\n", __func__, __LINE__ );

			SoftAdapterCopyDataStatus_t *CopyStatus;

			CopyStatus = ( SoftAdapterCopyDataStatus_t * )&Data[0].Control;

			ret = AdapterCopyDataGetStatus( CopyStatus, Status );

			if( ret < 0 )
			{
				DBGERR( "%s: It wasn't possible to get copy status\n", __func__ );
				goto error;
			}

			ret = DispatcherLibSendToClent(
								hen,
								TIMEOUT_EXCHANGE_INTERFACE,
								( uint16_t * ) &Data[0],
								sizeof( SoftRecordeExchangeStruct_t ) );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't send answer for get copy status\n", __func__ );
				return ret;
			}

			break;
		}

		case AXIS_DISPATCHER_CMD_STOP_COPY_TO_FILE:
		{

			DBGLOG( "%s:%i: Command query for stop copy data\n", __func__, __LINE__ );

			ret = AdapterCopyDataStop( Status );

			if( ret < 0 )
			{
				DBGERR( "%s: It wasn't possible to stop copy\n", __func__ );
				goto error;
			}

			ret = DispatcherLibSendToClent(
								hen,
								TIMEOUT_EXCHANGE_INTERFACE,
								( uint16_t * ) &Data[0],
								sizeof( SoftRecordeExchangeStruct_t ) );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't send answer for stop copy data command\n", __func__ );
				return ret;
			}

			break;
		}

		case DSP_RECORDER_CMD_GET_NET_PARAM:
		{
			DBGLOG( "%s:%i: Command query fpr getting Net Settings\n", __func__, __LINE__ );

			ret = AdapterSettingsLoadAdapterSettings( Status );

			if( ret == -AXIS_ERROR_BUSY_DEVICE )
			{
				goto norecorder;
			}

			if( ret < 0 )
			{
				DBGERR( "%s: It wasn't possible to get network settings\n", __func__ );
				goto error;
			}

			ret = DispatcherLibRequestCreateExchange(
								( uint16_t * )&Status->AdapterSettings.Block0,
								DSP_RECORDER_CMD_GET_NET_PARAM,
								sizeof( SoftRecordeExchangeStruct_t ) );

			if( ret < 0 )
			{
				DBGERR( "%s: Cant cretate exchange for Get network settings\n", __func__ );
				return ret;
			}

			ret = DispatcherLibSendToClent(
								hen,
								TIMEOUT_EXCHANGE_INTERFACE,
								( uint16_t * ) &Status->AdapterSettings.Block0,
								sizeof( SoftRecordeExchangeStruct_t ) );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't send network settings for interface\n", __func__ );
				return ret;
			}

			break;
		}

		case DSP_RECORDER_CMD_SET_NET_PARAM:
		{
			DBGLOG( "%s:%i: Command query for saving network settings\n", __func__, __LINE__ );

			//������ ��� ��������� ����� ������� ��������� �� ���� ���������!!!!
			//------------------------------------------------------------------
			SoftAdapterExchangeSettingsStruct_t SettingsForCheck;

			memset( &SettingsForCheck, 0, sizeof( SettingsForCheck ) );
			memcpy( &SettingsForCheck.Block0, &Data[0], sizeof( Status->AdapterSettings.Block0 ) );

			ret = AdapterSettingsCheckForNetSetting( &Status->GlobalDevice_t, &SettingsForCheck );

			if( ret < 0 )
			{
				DBGERR( "%s: Receve a invalid network settings for device\n", __func__ );
				goto error;
			}
			//------------------------------------------------------------------

			memcpy( &Status->AdapterSettings.Block0, &Data[0], sizeof( Status->AdapterSettings.Block0 ) );

			ret = AdapterSettingsSaveAdapterSettings( Status );

			if( ret == -AXIS_ERROR_BUSY_DEVICE )
			{
				goto norecorder;
			}

			if( ret < 0 )
			{
				DBGERR( "%s: It wasn't possible to save network settings\n", __func__ );
				goto error;
			}

			ret = adapter_settings_work_new_sets( Status, 0, 0, NULL );

			if( ret < 0 )
			{
				DBGERR( "%s: It wasn't possible to work new network setsings\n", __func__ );
				goto error;
			}

			ret = DispatcherLibSendToClent(
								hen,
								TIMEOUT_EXCHANGE_INTERFACE,
								( uint16_t * ) &Data[0],
								sizeof( SoftRecordeExchangeStruct_t ) );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't send answer for save network settings\n", __func__ );
				return ret;
			}

			break;
		}

		case DSP_RECORDER_CMD_PUT_NET_TIMERS:
		{
			DBGLOG( "%s:%i: Command query for saving network timers\n", __func__, __LINE__ );

			memcpy( &Status->TimersSettingsNet, &Data[0], sizeof( Status->TimersSettingsNet ) );

			ret = AdapterSettingsSaveNetTimersSettings( Status );

			if( ret == -AXIS_ERROR_BUSY_DEVICE )
			{
				goto norecorder;
			}

			if( ret < 0 )
			{
				DBGERR( "%s: It wasn' possible to save network timers\n", __func__ );
				goto error;
			}

			ret = adapter_settings_work_new_sets( Status, 0, 0, NULL );

			if( ret < 0 )
			{
				DBGERR( "%s: It wasn't possible to work new network setings\n", __func__ );
				goto error;
			}

			ret = DispatcherLibSendToClent(
								hen,
								TIMEOUT_EXCHANGE_INTERFACE,
								( uint16_t * ) &Data[0],
								sizeof( SoftRecordeExchangeStruct_t ) );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't send answer for save network timers\n", __func__ );
				return ret;
			}

			break;
		}

		case DSP_RECORDER_CMD_GET_NET_TIMERS:
		{
			DBGLOG( "%s:%i: Command query for reading network timers\n", __func__, __LINE__ );

			ret = AdapterSettingsLoadAdapterTimers( Status );

			if( ret == -AXIS_ERROR_BUSY_DEVICE )
			{
				goto norecorder;
			}

			if( ret < 0 )
			{
				DBGERR( "%s: It wasn' possible to get network timers\n", __func__ );
				goto error;
			}

			ret = DispatcherLibRequestCreateExchange(
								( uint16_t * )&Status->TimersSettingsNet,
								DSP_RECORDER_CMD_GET_NET_TIMERS,
								sizeof( SoftRecordeExchangeStruct_t ) );

			if( ret < 0 )
			{
				DBGERR( "%s: Cant cretate exchange for Get network timers", __func__ );
				return ret;
			}

			ret = DispatcherLibSendToClent(
								hen,
								TIMEOUT_EXCHANGE_INTERFACE,
								( uint16_t * ) &Status->TimersSettingsNet,
								sizeof( SoftRecordeExchangeStruct_t ) );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't send network timers to interface\n", __func__ );
				return ret;
			}

			break;
		}

		case DSP_RECORDER_CMD_APPLY_NET_PARAM:
		{
			DBGLOG( "%s:%i: Command query for applay network parametrs\n", __func__, __LINE__ );

			memcpy( &Status->AdapterSettings.Block0, &Data[0], sizeof( Status->AdapterSettings.Block0 ) );

			ret =  AdapterSettingsSaveAdapterSettings( Status ) ;

			if( ret == -AXIS_ERROR_BUSY_DEVICE )
			{
				goto norecorder;
			}

			if( ret < 0 )
			{
				DBGERR( "%s: It wasn't to possible to save adapter settings\n", __func__ );
				goto error;
			}

			ret = adapter_settings_work_new_sets( Status, 1, 0, NULL );

			if( ret < 0 )
			{
				DBGERR( "%s: It wasn't possible to work new network setings for applay\n", __func__ );
				goto error;
			}

			ret = DispatcherLibSendToClent(
								hen,
								TIMEOUT_EXCHANGE_INTERFACE,
								( uint16_t * ) &Data[0],
								sizeof( SoftRecordeExchangeStruct_t ) );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't send answer for applay network settings\n", __func__ );
				return ret;
			}

			break;
		}

		case AXIS_DISPATCHER_CMD_START_ERASE:
		{
			DBGLOG( "%s:%i: Command query for start erase\n",__func__,__LINE__);

			if( Status->Recorder.recorderscount >= 1 )
			{
				ret = AdapterRecorderStartErase( NULL, NULL, Status, Status->Recorder.next );

				if( ret < 0 )
				{
					DBGERR( "%s: Cant start erase from recorder\n", __func__ );
					goto error;
				}

				ret = DispatcherLibSendToClent(
								hen,
								TIMEOUT_EXCHANGE_INTERFACE,
								( uint16_t * ) &Data[0],
								sizeof( SoftRecordeExchangeStruct_t ) );

				if( ret < 0 )
				{
					DBGERR( "%s: Can't send answer for start earse command\n", __func__ );
					return ret;
				}
			}
			else
			{
				goto norecorder;
			}

			break;
		}
		case AXIS_DISPATCHER_CMD_START_CLEAR:
		{
			DBGLOG( "%s:%i: Command query fot start clear\n", __func__, __LINE__);

			if( Status->Recorder.recorderscount >= 1 )
			{
				ret = AdapterRecorderStartClear( NULL, NULL, Status, Status->Recorder.next );

				if( ret < 0 )
				{
					DBGERR( "%s: Cant start erase from recorder\n", __func__ );
					goto error;
				}

				ret = DispatcherLibSendToClent(
								hen,
								TIMEOUT_EXCHANGE_INTERFACE,
								( uint16_t * ) &Data[0],
								sizeof( SoftRecordeExchangeStruct_t ) );

				if( ret < 0 )
				{
					DBGERR( "%s: Can't send answer for start clear command\n", __func__ );
					return ret;
				}
			}
			else
			{
				goto norecorder;
			}

			break;
		}
		case AXIS_ADAPTER_CMD_REBOOT:
		{
			DBGLOG( "%s:%i: Command query for reboot device\n", __func__, __LINE__ );

			ret = DMXXXPowerReboot( &Status->GlobalDevice_t );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't work reboot method from device library\n", __func__ );
				goto error;
			}

			ret = DispatcherLibSendToClent(
								hen,
								TIMEOUT_EXCHANGE_INTERFACE,
								( uint16_t * ) &Data[0],
								sizeof( SoftRecordeExchangeStruct_t ) );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't send answer for command reboot\n", __func__ );
				return ret;
			}

			break;
		}

		case AXIS_DISPATCHER_CMD_SWITCH_NAND_DATA:
		{
			DBGLOG( "%s:%i: Command query for switch NAND data\n", __func__, __LINE__ );

			ret = AdapterInsertModule( &Status->GlobalDevice_t.dmdev_nand );

			if( ret < 0 )
			{
				DBGERR( "%s: Cant insert modules for NAND\n", __func__ );
				goto error;
			}

			ret = DispatcherLibSendToClent(
								hen,
								TIMEOUT_EXCHANGE_INTERFACE,
								( uint16_t * ) &Data[0],
								sizeof( SoftRecordeExchangeStruct_t ) );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't send answer for command switch NAND\n", __func__ );
				return ret;
			}

			break;
		}

		case AXIS_DISPATCHER_CMD_SWITCH_NAND_BOOT:
		{
			DBGLOG( "%s:%i: Command query for switch NAND boot\n", __func__, __LINE__ );

			ret = AdapterRemoveModule( &Status->GlobalDevice_t.dmdev_nand );

			if( ret < 0 )
			{
				DBGERR( "%s: Cant remove modules for NAND\n", __func__ );
				goto error;
			}

			ret = DispatcherLibSendToClent(
								hen,
								TIMEOUT_EXCHANGE_INTERFACE,
								( uint16_t * ) &Data[0],
								sizeof( SoftRecordeExchangeStruct_t ) );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't send answer for switch NAND boot\n", __func__ );
				return ret;
			}

			break;
		}

		case AXIS_ADAPTER_CMD_POWER_OFF:
		{
			DBGLOG( "%s:%i: Command query for poweroff device\n", __func__, __LINE__ );

			ret = AdapterCommandSleep( Status );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't call power off command from device library\n", __func__ );
				goto error;
			}

			ret = DispatcherLibSendToClent(
								hen,
								TIMEOUT_EXCHANGE_INTERFACE,
								( uint16_t * ) &Data[0],
								sizeof( SoftRecordeExchangeStruct_t ) );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't send answer for command power off\n", __func__ );
				return ret;
			}

			break;
		}

		case AXIS_INTERFACE_CMD_SET_BRIGHT:
		{
			DBGLOG( "%s:%i: Command query for set LCD brightness\n", __func__, __LINE__ );

			value = *(int*)&Data[0].Control;

			ret = AdapterInterfaceSetLcdBrightness( &Status->GlobalDevice_t, value );

			if( ret < 0 )
			{
				DBGERR( "%s: It wasn't possible to set lcd Brightness\n", __func__ );
				goto error;
			}

			ret = DispatcherLibSendToClent(
								hen,
								TIMEOUT_EXCHANGE_INTERFACE,
								( uint16_t * ) &Data[0],
								sizeof( SoftRecordeExchangeStruct_t ) );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't send answer for command LCD brightness\n", __func__ );
				return ret;
			}

			break;

		}

		case AXIS_INTERFACE_CMD_GO_SLEEP:
		{
			DBGLOG( "%s:%i: Command query for set go sleep\n", __func__, __LINE__ );

			value = *((int*)(( SoftRecordeExchangeStruct_t* )&Data[0])->Data);

			DBGLOG( "%s:%i: Go sleep Value = %i\n", __func__, __LINE__, value);

			Status->GlobalDevice_t.dmdev_interface.dmctrl_status &= ~DM_INTERFACE_STATUS_IN_SLEEP;

			Status->InterfaceSleep = value;
			Status->LastKeyPressTest  = 0;

			ret = AdapterInterfaceLcdOn( Status, 0 ); //��� ������� �� ���������� - � ������ ����� ����� ������� 0

			if( ret < 0 )
			{
				DBGERR( "%s: It wasn't posible to set LCD on\n", __func__ );
				goto error;
			}

			ret = DispatcherLibSendToClent(
								hen,
								TIMEOUT_EXCHANGE_INTERFACE,
								( uint16_t * ) &Data[0],
								sizeof( SoftRecordeExchangeStruct_t ) );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't send interface answer for go sleep\n", __func__ );
				return ret;
			}

			break;
		}

		case AXIS_DISPATCHER_CMD_START_NAND_TEST_WRITE:
		{

			DBGLOG( "%s:%i: Command query for start NAND test write\n", __func__, __LINE__ );

			ret = AdapterRecorderSendCommand( Status, Status->Recorder.next, ADAPTER_INTERFACE_COMMAND__NAND_TEST_WRITE, NULL, 0 );

			if( ret < 0)
			{
				DBGERR("%s:It wasn't possible to send command NAND test write to recorder\n",__func__);
				goto error;
			}

			ret = DispatcherLibSendToClent(
								hen,
								TIMEOUT_EXCHANGE_INTERFACE,
								( uint16_t * ) &Data[0],
								sizeof( SoftRecordeExchangeStruct_t ) );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't send interface answer for NAND test write\n", __func__ );
				return ret;
			}

			break;
		}

		case AXIS_DISPATCHER_CMD_START_NAND_TEST_READ:
		{
			DBGLOG( "%s:%i: Command query for NAND test read\n", __func__, __LINE__ );

			ret = AdapterRecorderSendCommand( Status, Status->Recorder.next, ADAPTER_INTERFACE_COMMAND__NAND_TEST_READ, NULL, 0 );

			if( ret < 0)
			{
				DBGERR( "%s: It wasn't possible to send command NAND test read to recorder\n",__func__);
				goto error;
			}

			ret = DispatcherLibSendToClent(
								hen,
								TIMEOUT_EXCHANGE_INTERFACE,
								( uint16_t * ) &Data[0],
								sizeof( SoftRecordeExchangeStruct_t ) );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't send interface answer for NAND test read\n", __func__ );
				return ret;
			}

			break;
		}

		case AXIS_DISPATCHER_CMD_COMMIT_STATUS:
		{
			DBGLOG( "%s:%i: Command query for commit status\n", __func__, __LINE__ );

			ret = AdapterRecorderSendCommand( Status, Status->Recorder.next, ADAPTER_INTERFACE_COMMAND__FLUSH_RECORDS, NULL, 0);

			if( ret < 0 )
			{
				DBGERR( "%s: It wasn't possible to send command commit status to recorder\n",__func__);
				goto error;
			}

			ret = DispatcherLibSendToClent(
								hen,
								TIMEOUT_EXCHANGE_INTERFACE,
								( uint16_t * ) &Data[0],
								sizeof( SoftRecordeExchangeStruct_t ) );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't send interface answer for commit status\n", __func__ );
				return ret;
			}

			break;
		}

		case AXIS_INTERFACE_CMD_UNLOCK_KEYBOARD:
		{
			DBGLOG( "%s:%i: Command query for unlock keyboard\n", __func__, __LINE__ );

			ret = AdapterDmDeviceUnlockKeyboard( &Status->GlobalDevice_t );

			if( ret < 0)
			{
				DBGERR( "%s: It wasn't possible to unlock keyboard\n", __func__ );
				goto error;
			}

			ret = DispatcherLibSendToClent(
								hen,
								TIMEOUT_EXCHANGE_INTERFACE,
								( uint16_t * ) &Data[0],
								sizeof( SoftRecordeExchangeStruct_t ) );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't send interface answer for unlock keyboard\n", __func__ );
				return ret;
			}

			break;
		}

		case AXIS_INTERFACE_CMD_LOCK_KEYBOARD:
		{
			DBGLOG( "%s:%i: Command query fo lock keyboard\n", __func__, __LINE__ );

			ret = AdapterDmDeviceLockKeyboard( &Status->GlobalDevice_t );

			if( ret < 0)
			{
				DBGERR( "%s: It wasn't possible to lock keyboard\n",__func__);
				goto error;
			}

			ret = DispatcherLibSendToClent(
								hen,
								TIMEOUT_EXCHANGE_INTERFACE,
								( uint16_t * ) &Data[0],
								sizeof( SoftRecordeExchangeStruct_t ) );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't send interface answer for lock keyboard\n", __func__ );
				return ret;
			}

			break;
		}

		case AXIS_DISPATCHER_CMD_START_NAND_TEST_STATUS:
		{
			DspRecorderExchangeFlashTestStatusStruct_t *TestNandStatus = (DspRecorderExchangeFlashTestStatusStruct_t*)&Data[0];

			DBGLOG( "%s:%i: Command query for get NAND test status\n", __func__, __LINE__ );

			ret = AdapterRecorderSendCommand( Status, Status->Recorder.next,
											  ADAPTER_INTERFACE_COMMAND__NAND_TEST_GET_LOG,
											  (uint8_t *)TestNandStatus,
											  sizeof( DspRecorderExchangeFlashTestStatusStruct_t ) );

			if( ret < 0)
			{
				DBGERR( "%s: It wasn't possible to send command NAND test status to recorder\n", __func__ );
				goto error;
			}

			ret = DispatcherLibSendToClent(
								hen,
								TIMEOUT_EXCHANGE_INTERFACE,
								( uint16_t * ) &Data[0],
								sizeof( SoftRecordeExchangeStruct_t ) );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't send interface answer for NAND test status\n", __func__ );
				return ret;
			}

			break;
		}

		case AXIS_DISPATHCER_CMD_GET_IMAGE_TIMES:
		{
			SoftAdapterFirmWriteControl_t *Control;
			Control = (SoftAdapterFirmWriteControl_t*)(( SoftRecordeExchangeStruct_t* )&Data[0])->Data; //TODO - ������ ����������, ����� ������� ��� ���� ���������??

			DBGLOG( "%s:%i: Command query firmware image times\n", __func__, __LINE__ );

			ret = AdapterFirmwareGetImageTimes( &Status->GlobalDevice_t,
										Control->versionpath,
										Control->envrimentpath,
										&Control->Uimagetime,
										&Control->Ramdisktime,
										&Control->Uimageintime,
										&Control->Ramdiskintime );

			if( ret < 0)
			{
				DBGERR( "%s: It wasn't possible to get firmware images time\n", __func__ );
				goto error;
			}

			ret = DispatcherLibRequestCreateExchange( ( uint16_t * )&Data[0],
														AXIS_DISPATHCER_CMD_GET_IMAGE_TIMES,
														sizeof( SoftRecordeExchangeStruct_t ) );

			if( ret < 0 )
			{
				DBGERR( "%s: Cant cretate exchange for Get firmware image time cmd\n", __func__ );
				return ret;
			}

			ret = DispatcherLibSendToClent( hen, TIMEOUT_EXCHANGE_INTERFACE, ( uint16_t * ) &Data[0],
														sizeof( SoftRecordeExchangeStruct_t ) );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't send interface answer from get firmware image time\n", __func__ );
				return ret;
			}

			break;
		}

		case AXIS_DISPATHCER_CMD_START_FIRM_WRITE:
		{
			SoftAdapterFirmWriteControl_t *Control;
			Control = (SoftAdapterFirmWriteControl_t*)(( SoftRecordeExchangeStruct_t* )&Data[0])->Data; //TODO - ������ ����������, �� ������ 2054

			DBGLOG( "%s:%i: Command query start firmware write\n", __func__, __LINE__ );

			ret = AdapterFirmwareStart( &Status->GlobalDevice_t, Control->uimagepath,
										Control->ramdiskpath,Control->versionpath,
										Control->envrimentpath,DM_FIRM_FLAGS_SEPARATE_THREAD );

			if( ret < 0)
			{
				DBGERR( "%s: It wasn't possible to start firmwrite write\n", __func__ );
				goto error;
			}

			ret = DispatcherLibSendToClent( hen, TIMEOUT_EXCHANGE_INTERFACE, ( uint16_t * ) &Data[0],
																	sizeof( SoftRecordeExchangeStruct_t ) );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't send interface answer for start firmware write\n", __func__ );
				return ret;
			}

			break;
		}

		case AXIS_DISPATHCER_CMD_STOP_FIRM_WRITE:
		{
			DBGLOG( "%s:%i: Command query stop firmware write\n", __func__, __LINE__ );

			ret = AdapterFirmwareStop( &Status->GlobalDevice_t );

			if( ret < 0)
			{
				DBGERR( "%s: It wasn't possible to stop firmwrite write\n", __func__ );
				goto error;
			}

			ret = DispatcherLibSendToClent( hen, TIMEOUT_EXCHANGE_INTERFACE, ( uint16_t * ) &Data[0],
														sizeof( SoftRecordeExchangeStruct_t ) );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't send interface answer for stop firmware write\n", __func__ );
				return ret;
			}

			break;
		}

		case AXIS_DISPATHCER_CMD_GET_FIRM_STATUS:
		{
			SoftAdapterFirmWriteControl_t *Control;
			Control = (SoftAdapterFirmWriteControl_t*)(( SoftRecordeExchangeStruct_t* )&Data[0])->Data; //TODO - ������ ����������, �� 2054

			DBGLOG( "%s:%i: Command query firmware status\n", __func__, __LINE__ );

			Control->status =  AdapterFirmwareGetStatus( &Status->GlobalDevice_t );

			ret = DispatcherLibRequestCreateExchange( ( uint16_t * )&Data[0],
														AXIS_DISPATHCER_CMD_GET_FIRM_STATUS,
														sizeof( SoftRecordeExchangeStruct_t ) );

			if( ret < 0 )
			{
				DBGERR( "%s: Cant cretate exchange for Get firmware status cmd\n", __func__ );
				return ret;
			}

			ret = DispatcherLibSendToClent( hen, TIMEOUT_EXCHANGE_INTERFACE, ( uint16_t * ) &Data[0],
																	sizeof( SoftRecordeExchangeStruct_t ) );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't send interface answer for get firmware status\n", __func__ );
				return ret;
			}

			break;
		}

		case AXIS_DISPATHCER_CMD_GET_FIRM_PROGRESS:
		{
			SoftAdapterFirmWriteControl_t *Control;
			Control = (SoftAdapterFirmWriteControl_t*)(( SoftRecordeExchangeStruct_t* )&Data[0])->Data; //TODO - ������ ���������� - �� ������ 2054

			DBGLOG( "%s:%i: Command query firmware progress\n", __func__, __LINE__ );

			Control->progress =  AdapterFirmwareGetProgress( &Status->GlobalDevice_t );

			ret = DispatcherLibRequestCreateExchange( ( uint16_t * )&Data[0],
														AXIS_DISPATHCER_CMD_GET_FIRM_STATUS,
														sizeof( SoftRecordeExchangeStruct_t ) );

			if( ret < 0 )
			{
				DBGERR( "%s: Cant cretate exchange for Get firmware status cmd\n", __func__ );
				return ret;
			}

			ret = DispatcherLibSendToClent( hen, TIMEOUT_EXCHANGE_INTERFACE, ( uint16_t * ) &Data[0],
																	sizeof( SoftRecordeExchangeStruct_t ) );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't send interface answer for get firmware progress\n", __func__ );
				return ret;
			}

			break;
		}

		case AXIS_DISPATCHER_CMD_NAND_ABORT_TEST:
		{
			DBGLOG( "%s:%i: Command query for abort NAND test\n", __func__, __LINE__ );

			ret = AdapterRecorderSendCommand( Status, Status->Recorder.next, ADAPTER_INTERFACE_COMMAND__NAND_ABORT_TEST, NULL, 0 );

			if( ret < 0 )
			{
				DBGERR( "%s: It wasn't possible to send command abort NAND test to recorder\n",__func__);
				goto error;
			}

			ret = DispatcherLibSendToClent(
								hen,
								TIMEOUT_EXCHANGE_INTERFACE,
								( uint16_t * ) &Data[0],
								sizeof( SoftRecordeExchangeStruct_t ) );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't send interface answer for abort NAND test\n", __func__ );
				return ret;
			}

			break;
		}
	    case AXIS_DISPATHCER_CMD_SD_REQUEST_GET:
        {
            if( DMXXXSwitchSD( &Status->GlobalDevice_t, 1)  < 0 )
            {
                DBGERR( "%s: Can't switch SD card to SOM\n", __func__ );
            }

            ret = DispatcherLibSendToClent(
                    hen,
                    TIMEOUT_EXCHANGE_INTERFACE,
                    ( uint16_t * ) &Data[0],
                    sizeof( SoftRecordeExchangeStruct_t ) );

            if( ret < 0 )
            {
                DBGERR( "%s: Can't send interface answer for switch SD\n", __func__ );
                return ret;
            }

            break;
        }
        case AXIS_DISPATHCER_CMD_SD_REQUEST_RET:
        {
            if( DMXXXSwitchSD( &Status->GlobalDevice_t, 0)  < 0 )
            {
                DBGERR( "%s: Can't send interface answer for switch SD\n", __func__ );
                goto error;
            }

            ret = DispatcherLibSendToClent(
                    hen,
                    TIMEOUT_EXCHANGE_INTERFACE,
                    ( uint16_t * ) &Data[0],
                    sizeof( SoftRecordeExchangeStruct_t ) );

            if( ret < 0 )
            {
                DBGERR( "%s: Can't send interface answer for abort NAND test\n", __func__ );
                return ret;
            }
            break;
        }
	    case AXIS_DISPATCHER_CMD_PLAY_CONTROL:
        {
            if( Status->Recorder.recorderscount >= 1 )
            {
                //�� ����������� ������� �������� � ����������

                ret = AdapterRecorderSendControl( Status, Status->Recorder.next,
                                                  (uint8_t *)&Data[0], sizeof( SoftRecorderExchangeControlStruct_t ) );

                if( ret < 0 )
                {
                    DBGERR( "%s: Can't send control command to recorder\n", __func__ );
                    goto error;
                }

                ret = DispatcherLibSendToClent(
                        hen,
                        TIMEOUT_EXCHANGE_INTERFACE,
                        ( uint16_t * ) &Data[0],
                        sizeof( SoftRecordeExchangeStruct_t ) );

                if( ret < 0 )
                {
                    DBGERR( "%s: Can't send interface answer for switch SD\n", __func__ );
                    return ret;
                }
            }
            else
            {
                goto norecorder;
            }
            break;
        }
		default:
		{
			DBGERR( "%s: Invalid Command 0x%04x \n", __func__, cmd );
			ret = -AXIS_ERROR_INVALID_VALUE;
		}
	}

	return ret;

norecorder:

	DBGERR( "%s: No recorders for command 0x%04x\n", __func__, cmd );

	ret = AdapterInterfaceSendCommand( Status, AXIS_INTERFACE_EVENT_RECORDER_OUT );

	if( ret < 0 )
	{
		DBGERR( "%s:%i: It wasn't possible to send event 'Recorder Out' to interface\n", __func__, __LINE__ );
	}

error:

	ret = DispatcherLibRequestCreateExchange(
								( uint16_t * ) &Data[0],
								AXIS_INTERFACE_CMD_ERROR,
								sizeof( SoftRecordeExchangeStruct_t ) );

	if( ret < 0 )
	{
		DBGERR( "%s: Cant cretate error exchange for intarface\n", __func__ );
		return ret;
	}

	ret = DispatcherLibSendToClent(
								hen,
								TIMEOUT_EXCHANGE_INTERFACE,
								( uint16_t * ) &Data[0],
								sizeof( SoftRecordeExchangeStruct_t ) );

	if( ret < 0 )
	{
		DBGERR( "%s: Cant send error exchange to interface\n", __func__ );
		return ret;
	}

	return 0;
}

/**
 * @brief ������� ��������� ������� �� ������� ����������
 *
 * @param InterfacePriv - ��������� �� ��������� ������ ����������
 * @param rfds - ��������� �� ��������� ������� ��� ���������
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 */
int AdapterInterfaceWorkUnixSockets( dmdev_user_interface_t *InterfacePriv, fd_set *rfds, AdapterGlobalStatusStruct_t *Status )
{
	if( !InterfacePriv || !rfds )
	{
		DBGERR( "%s: Invalid argument\n", __func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	//����������� ����������� ���������� � ������� - ���������� �����
	if( InterfacePriv->socketout > 0 && FD_ISSET( InterfacePriv->socketout, rfds ) )
	{
		if( InterfacePriv->sockethenout > 0 )
		{
			DBGERR ( "%s: Second adapter process try connected. Break it.\n", __func__ );
			AdaptrerInterfaceFlushSecondConnect( InterfacePriv->socketout );
		}
		else
		{
			InterfacePriv->sockethenout = DispatcherLibNetworkUnixServiceAccept( InterfacePriv->socketout );
			if( InterfacePriv->sockethenout < 0 )
			{
				DBGERR ( "%s: Can't accept interface event connection\n", __func__ );
				INFO_LOG( 1, "Interface service event accepted" );
				return InterfacePriv->sockethenout;
			}
			else
			{
				INFO_LOG( 0, "Interface service event accepted" );
			}
		}
	}

	//����������� ����������� ���������� � ������� - ��������� �����
	if( InterfacePriv->socketin > 0 && FD_ISSET( InterfacePriv->socketin, rfds ) )
	{
		if( InterfacePriv->sockethenin > 0 )
		{
			DBGERR ( "%s: Second adapter process try connected. Break it.\n", __func__ );
			AdaptrerInterfaceFlushSecondConnect( InterfacePriv->socketin );
		}
		else
		{
			InterfacePriv->sockethenin = DispatcherLibNetworkUnixServiceAccept( InterfacePriv->socketin );
			if( InterfacePriv->sockethenin < 0 )
			{
				DBGERR ( "%s: Can't accept interface command connection\n", __func__ );
				INFO_LOG( 1, "Interface service command accepted" );
				return InterfacePriv->sockethenin;
			}
			else
			{
				INFO_LOG( 0, "Interface service command accepted" );
			}
		}
	}

	//������� �� ������ �������� �� ����������
	if( InterfacePriv->sockethenin > 0 && FD_ISSET( InterfacePriv->sockethenin, rfds ) )
	{
		int retval;

		DBGLOG ( "%s:%i: Do command from interface %i \n", __func__, __LINE__, InterfacePriv->sockethenin );

		retval = AdapterInterfaceDoCommandFrom( InterfacePriv->sockethenin, Status );

		if( retval  == -AXIS_ERROR_CANT_READ_SOCKET )
		{
			close( InterfacePriv->sockethenin );
			InterfacePriv->sockethenin = -1;
			close( InterfacePriv->sockethenout );
			InterfacePriv->sockethenout = -1;

			Status->usbswitches = 0;
			AdapterUsbSwithInternal ( &Status->GlobalDevice_t );

			if( Status->GlobalDevice_t.dmdev_interface.dmctrl_status & DM_INTERFACE_STATUS_IN_SLEEP )
			{
				DBGLOG( "%s:%i: Interface went to sleep\n", __func__, __LINE__ );

				//�������� ���������� �� ��������� �����
				int ret = AdapterInterfaceCheckZombi( InterfacePriv, Status );

				if( ret < 0 )
				{
					DBGERR( "%s: Can't check interface zobie state\n", __func__ );
				}

				INFO_LOG( ret, "interface sleep" );
			}
			else
			{
				DBGLOG ( "%s:%i: Interface crashed!!! \n", __func__, __LINE__ );

				if( Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature & DM_DM_ENABLE_DIAGNOSTIC )
				{
					Status->AdapterDiagnostic.interface_crashed++;

					if( AdapterSaveDiagnostic( &Status->GlobalDevice_t, Status ) < 0 )
					{
						DBGERR ( "%s: It wasn't possible to save diagnostic info\n", __func__ );
					}
				}

				if( (Status->GlobalDevice_t.dmdev_interface.dmctrl_feature & DM_INTERFACE_RESTART) &&
					!Status->flag_no_interface )
				{
					if( InterfacePriv->pid > 0 )
					{
						kill( InterfacePriv->pid, SIGKILL );
					}
					Status->iplay = 0;
					InterfacePriv->pid = AdapterInterfaceStart ( &Status->GlobalDevice_t, Status->flag_no_interface );
				}
			}
		}
		else if( retval < 0 )
		{
			DBGERR ( "%s: It wasn't possible to do command from interface %i\n", __func__, InterfacePriv->sockethenin );
			return retval;
		}
	}

	return 0;
}

/**
 * @brief �������, ������� ����������� ������ ���������� ��� ������������ �������
 *
 * @param InterfacePriv - ��������� �� ��������� ������ ����������
 * @param rfds - ��������� �� ������ ���������� ��� �������
 * @param maxsel - ������������ ��������� � �������
 * @return int - ����� ������������ ��������� � �������
 */
int AdapterInterfaceSetHendelUnixSocket( dmdev_user_interface_t *InterfacePriv, fd_set *rfds, int maxsel )
{
	if( !InterfacePriv || !rfds )
	{
		DBGERR( "%s: Invalid argument\n", __func__ );
		return maxsel;
	}

	if( InterfacePriv->socketin > 0 )
	{
		maxsel = AdapterGlobalWileSetHendel( InterfacePriv->socketin, rfds, maxsel );
	}

	if( InterfacePriv->socketout > 0 )
	{
		maxsel = AdapterGlobalWileSetHendel( InterfacePriv->socketout, rfds, maxsel );
	}

	if( InterfacePriv->sockethenin > 0 )
	{
		maxsel = AdapterGlobalWileSetHendel( InterfacePriv->sockethenin, rfds, maxsel );
	}

	return maxsel;
}

#include <unistd.h>

#include <axis_error.h>

#include <dmxxx_lib/dmxxx_resource.h>
#include <dmxxx_lib/dmxxx_calls.h>

#include <adapter/debug.h>
#include <adapter/power.h>
#include <adapter/global.h>
#include <adapter/timer.h>
#include <adapter/leds.h>
#include <adapter/dmdevice.h>
#include <adapter/diagnostic.h>
#include <adapter/network.h>
#include <adapter/interface.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif

/**
 * @brief ������� ���������� ���������� Ethernet
 *
 * @param dev - ��������� �� ��������� ����������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
static int adapter_power_off_ethernet( dmdev_t *dev )
{
	int ret;

	if( !dev )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	if( !dev->dmdev_eth.dmctrl_ipresent || !dev->dmdev_eth.dmctrl_icontrol )
	{
		DBGLOG( "%s:%i: Ethernet not present or controlled. Exit\n", __func__, __LINE__ );
		return 0;
	}

	ret = DMXXXPowerEthOff( dev );

	if( ret < 0 )
	{
		DBGERR( "%s: It wasn't possible to poweroff eth\n", __func__ );
		return ret;
	}

	return 0;
}

/**
 * @brief ������� ���������� ����� USB
 *
 * @param dev - ��������� �� ��������� ����������
 * @param usbnum - ����� ����� ��� ����������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
static int adapter_power_off_usb_n( dmdev_t *dev, uint16_t usbnum )
{
	if( !dev )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	return DMXXXPowerUSBOff( dev, usbnum );
}

/**
 * @brief �������, ������� ��������� ��� ����� USB ������ �� ����, ��� �������� ���������� ������
 *
 * @param Dev - ��������� �� ��������� ����������
 * @param Number - ���������� USB ������ �� ����������
 * @return int
 **/
static inline int adapter_power_off_usb_from_number( dmdev_t *Dev, uint8_t Number )
{
	int i;
	int ret;

	for( i = 1; i <= Number; i++ )
	{
		ret = adapter_power_off_usb_n( Dev, i );

		INFO_LOG( ret, "Power Off usb num %i", i );

		if( ret )
		{
			DBGERR( "%s: It wasn't possible to power off usb at port %i\n", __func__, i );
			return ret;
		}
	}

	return ret;
}

/**
 * @brief �������, ������� ��������� ��� ����� USB ������ �� ����, ��� �������� ������� ����� ������
 *
 * @param Dev - ��������� �� ��������� ����������
 * @param Mask - ������� ����� ������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
static inline int adapter_power_off_usb_from_mask( dmdev_t *Dev, uint8_t Mask )
{
	int i;
	int ret = AXIS_NO_ERROR;

	for( i = 0; i < sizeof( uint8_t )*8; i++ )
	{
		if( Mask & ( 1 << i ) )
		{
			ret = adapter_power_off_usb_n( Dev, i );

			INFO_LOG( ret, "Power Off usb num %i", i );

			if( ret )
			{
				DBGERR( "%s: It wasn't possible to power off usb at port %i\n", __func__, i );
				return ret;
			}
		}
	}

	return ret;
}

/**
 * @brief �������, ������� ��������� ��� ����� USB �� ����������
 *
 * @param dev - ��������� �� ��������� ����������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
static int adapter_power_off_all_usb( dmdev_t *dev )
{
	int ret;
	int USBPorts;

	if( !dev )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	if( !dev->dmdev_usb.dmctrl_ipresent )
	{
		DBGLOG( "%s:%i: No usb device present\n", __func__, __LINE__ );
		return 0;
	}

	USBPorts = dev->dmdev_usb.dmctrl_feature & DM_USB_NUM_MASK;

	if( !USBPorts )
	{
		DBGERR( "%s: No ports detected from device\n", __func__ );
		return -AXIS_ERROR_INVALID_VALUE;
	}

	if( dev->dmdev_usb.dmctrl_feature & DM_USB_BITMASK_CTRL )
	{
		DBGLOG( "%s: Try power off USB from bit mask 0x%02x\n", __func__, USBPorts );
		ret = adapter_power_off_usb_from_mask( dev, USBPorts );
	}

	else
	{
		DBGLOG( "%s: Try power off USB from port count %d\n", __func__, USBPorts );
		ret = adapter_power_off_usb_from_number( dev, USBPorts );
	}

	if( ret < 0 )
	{
		DBGERR( "%s: Can't power off all USB ports\n", __func__ );
		return ret;
	}

	return 0;
}

/**
 * @brief ������� ��������� ���������� Ethernet
 *
 * @param dev - ��������� �� ��������� ����������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
static int adapter_power_on_ethernet( dmdev_t *dev )
{
	int ret;

	if( !dev )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	if( !dev->dmdev_eth.dmctrl_ipresent || !dev->dmdev_eth.dmctrl_icontrol )
	{
		DBGLOG( "%s:%i: Ethernet not present exit\n", __func__, __LINE__ );
		return 0;
	}

	ret = DMXXXPowerEthOff( dev );

	if( ret < 0 )
	{
		DBGERR( "%s: It wasn't possible to poweroff eth before on\n", __func__ );
		return ret;
	}

	ret = DMXXXPowerEthReset( dev );

	if( ret < 0 )
	{
		DBGERR( "%s: It wasn't possible to reset ethernet\n", __func__ );
		return ret;
	}

	ret = DMXXXPowerEthOn( dev );

	if( ret < 0 )
	{
		DBGERR( "%s: It wasn't possible to power on ethernet\n", __func__ );
		return ret;
	}

	return 0;
}

/**
 * @brief �������, ������� �������� ���� USB
 *
 * @param dev - ��������� �� ��������� ����������
 * @param usbnum - ����� USB ����� ��� ���������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
static int adapter_power_on_usb_n( dmdev_t *dev, uint8_t usbnum )
{
	if( !dev || !usbnum )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	return DMXXXPowerUSBOn( dev, usbnum );
}


/**
 * @brief �������, ������� �������� ��� ����� USB ������ �� ����, ��� �������� ���������� ������
 *
 * @param Dev - ��������� �� ��������� ����������
 * @param Number - ���������� USB ������ �� ����������
 * @return int
 **/
static inline int adapter_power_on_usb_from_number( dmdev_t *Dev, uint8_t Number )
{
	int i;
	int ret;

	for( i = 1; i <= Number; i++ )
	{
		ret = adapter_power_on_usb_n( Dev, i );

		INFO_LOG( ret, "Power on usb num %i", i );

		if( ret )
		{
			DBGERR( "%s: It wasn't possible to power on usb at port %i\n", __func__, i );
			return ret;
		}
	}

	return ret;
}

/**
 * @brief �������, ������� �������� ��� ����� USB ������ �� ����, ��� �������� ������� ����� ������
 *
 * @param Dev - ��������� �� ��������� ����������
 * @param Mask - ������� ����� ������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
static inline int adapter_power_on_usb_from_mask( dmdev_t *Dev, uint8_t Mask )
{
	int i;
	int ret = AXIS_NO_ERROR;

	for( i = 0; i < sizeof( uint8_t )*8; i++ )
	{
		if( Mask & ( 1 << i ) )
		{
			DBGLOG( "%s: Try power on USB port %i\n", __func__, i );

			ret = adapter_power_on_usb_n( Dev, i );

			INFO_LOG( ret, "Power on usb num %i", i );

			if( ret )
			{
				DBGERR( "%s: It wasn't possible to power on usb at port %i\n", __func__, i );
				return ret;
			}
		}
	}

	return ret;
}

/**
 * @brief �������, ������� �������� ��� ����� USB �� ����������
 *
 * @param dev - ��������� �� ��������� ����������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
static int adapter_power_on_all_usb( dmdev_t *dev )
{
	int ret;
	int USBPorts;

	if( !dev )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	if( !dev->dmdev_usb.dmctrl_ipresent )
	{
		DBGLOG( "%s:%i: No usb device present\n", __func__, __LINE__ );
		return 0;
	}

	USBPorts = dev->dmdev_usb.dmctrl_feature & DM_USB_NUM_MASK;

	if( !USBPorts )
	{
		DBGERR( "%s: No ports detected from device\n", __func__ );
		return -AXIS_ERROR_INVALID_VALUE;
	}

	if( dev->dmdev_usb.dmctrl_feature & DM_USB_BITMASK_CTRL )
	{
		DBGLOG( "%s: Try power on USB from bit mask 0x%02x\n", __func__, USBPorts );
		ret = adapter_power_on_usb_from_mask( dev, USBPorts );
	}

	else
	{
		DBGLOG( "%s: Try power on USB from port count %d\n", __func__, USBPorts );
		ret = adapter_power_on_usb_from_number( dev, USBPorts );
	}

	if( ret < 0 )
	{
		DBGERR( "%s: Can't power off all USB ports\n", __func__ );
		return ret;
	}

	return 0;
}

/**
 * @brief ������� ���������� �������� USB ����� (���/����)
 *
 * @param dev - ��������� �� ��������� ����������
 * @param usbnum - ����� ����� USB ��� ������
 * @param on - ���� ���������: 1 - ��������, 0 - ���������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterPowerUSB( dmdev_t *dev, uint8_t usbnum , int on )
{
	if( on )
	{
		return adapter_power_on_usb_n( dev,usbnum );
	}

	else
	{
		return adapter_power_off_usb_n( dev,usbnum );
	}
}

/**
 * @brief ������� ���������� �������� USB ����� (��������� � ����� ��������)
 *
 * @param dev - ��������� �� ��������� ���������
 * @param usbnum - ����� ����� USB ��� ������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterPowerUSBReset( dmdev_t *dev, uint8_t usbnum )
{
	int ret;
    int  old_cancel_state; 
    pthread_setcancelstate  (PTHREAD_CANCEL_DISABLE, &old_cancel_state); 
	ret = AdapterPowerUSB( dev, usbnum, 0 );

	if( ret < 0 )
	{
		DBGERR( "%s: -- Can't power off usb port %d\n", __func__, usbnum );
        pthread_setcancelstate (old_cancel_state,  NULL); 
		return ret;
	}

	sleep( 3 ); //TODO - ���������

	ret = AdapterPowerUSB( dev, usbnum, 1 );

	if( ret < 0 )
	{
		DBGERR( "%s: -- Can't power off usb port %d\n", __func__, usbnum );
        pthread_setcancelstate (old_cancel_state,  NULL); 
		return ret;
	}
    pthread_setcancelstate (old_cancel_state,  NULL); 
	return 0;
}

/**
 * @brief ������� ������������� ������� ����������
 *
 * @param dev - ��������� �� ��������� ����������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterPowerInit( dmdev_t *dev )
{
	int ret;

	if( !dev )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	ret = adapter_power_on_ethernet( dev );
	INFO_LOG( ret, "Power On ethrnet" );

	if( ret < 0 )
	{
		DBGERR( "%s: It wasn't possible to power on ethernet\n", __func__ );
		return ret;
	}

	ret = adapter_power_on_all_usb( dev );
	INFO_LOG( ret, "Power On all USB ports" );

	if( ret < 0 )
	{
		DBGERR( "%s: It wasn't possible to power on usb\n", __func__ );
		return ret;
	}

	ret = DMXXXModeButtonEnable( dev );
	INFO_LOG( ret, "Button enable" );

	if( ret < 0 )
	{
		DBGERR( "%s: Can't enable button\n", __func__ );
		return ret;
	}

	return 0;
}

/**
 * @brief ������� ���������� ������� ����������
 *
 * @param dev - ��������� �� ��������� ����������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterPowerDeInit( dmdev_t *dev )
{
	int ret;

	if( !dev )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	ret = adapter_power_off_ethernet( dev );
	INFO_LOG( ret, "Power Off ethrnet" );

	if( ret < 0 )
	{
		DBGERR( "%s: It wasn't possible to power off ethernet\n", __func__ );
		return ret;
	}

	ret = adapter_power_off_all_usb( dev );
	INFO_LOG( 0, "Power Off usb's" );

	if( ret < 0 )
	{
		DBGERR( "%s: It wasn't possible to power off usb\n", __func__ );
		return ret;
	}

	return 0;
}

/**
 * @brief ������� ������������ �������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterPowerGlobalReboot( AdapterGlobalStatusStruct_t *Status )
{
	int ret;

	if( !Status )
	{
		DBGERR( "%s:%i : Invalid Argument\n", __func__, __LINE__ );
		return -1;
	}

	ret = iDMXXXPowerReboot( &Status->GlobalDevice_t );

	if( ret < 0 )
	{
		DBGERR( "%s: Device not support 'reboot'\n", __func__ );
		return -AXIS_ERROR_INVALID_VALUE;
	}

	//���� �������� ��������� ������� � Reboot ����� ������ SMS
	//������ ����� ��������� �� ����
	if( Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature | DM_DM_ENABLE_INCOMMING_SMS_REBOOT )
	{
		//���������� ���� ��������, � �� ����� ������� �������, ��� ���
		//�� � ���� ��� �� ������� - ������ ������ ����� �������
		int ret = AdapterGlobalEventAddAndLog( Status, "Adapter Reboot",
											   DISPATCHER_LIB_EVENT__REBOOT,
											   0, 0 );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't add new connect event\n", __func__ );
		}

		else
		{
			//� ��� �� �������� ��� ���������
			ret = AdapterGlobalSMS( Status, 0, 0 ); //��������� �� ����� - ������ 0

			if( ret < 0 )
			{
				DBGERR( "%s: Can't send new event\n", __func__ );
			}

			//���� ���� ��������� ����� ��� �� �����������!
			sleep( 10 );
		}
	}

	AdapterTimerPutStatusFlags( Status );

	//������� �����������, ��� �� ����� ������������
	AdapterLedHardwareOn( Status );

	Status->GlobalDevice_t.dmdev_lpc.dmctrl_status &= ~DM_LPC_LEAVE_ACC;

	//�� ������� �� �� �� ������ ��� ��� - �������� �����������
	if( Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature & DM_DM_ENABLE_DIAGNOSTIC )
	{
		Status->GlobalDevice_t.dmdevice_powerupvalue = 1; //TODO - ��������� ���� ��� �� ������������� ���������
		if( AdapterSaveDiagnostic( &Status->GlobalDevice_t, Status ) < 0 )
		{
			DBGERR( "%s: It wasn't possible to save diagnostic info\n", __func__ );
		}
	}

	//���������� ���� ������������
	ret = DMXXXPowerReboot( &Status->GlobalDevice_t );

	if( ret < 0 )
	{
		DBGERR( "%s: Can't reboot device\n", __func__ );
		return ret;
	}

	AdapterGlobalClose( Status );

	exit( 0 );

	return 0;
}

/**
 * @brief �������, ������� ��������� ���������� � ��� � ����������� �� �����
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @param DevicePower - ���� ��������� �� ����������� ���������� ���������� (�������� ����������) /1 = ���������/
 * @param SleepValue - ���� Value � ������� Sleep
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterPowerGlobalLPC( AdapterGlobalStatusStruct_t *Status, int DevicePower, int SleepValue )
{
	if( !Status )
	{
		DBGERR( "%s: %i : Invalid Argument\n", __func__, __LINE__ );
		return -1;
	}

	//���� �������� ��������� ������� � SLEEP ����� ������ SMS
	//������ ����� ��������� �� ����
	if( Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature & DM_DM_ENABLE_INCOMMING_SMS_SLEEP )
	{
		//���������� ���� ��������, � �� ����� ������� �������, ��� ���
		//�� � ���� ��� �� ������� - ������ ������ ����� �������
		int ret = AdapterGlobalEventAddAndLog( Status, "Adapter Sleep",
											   DISPATCHER_LIB_EVENT__SLEEP,
											   SleepValue, 0 );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't add new connect event\n", __func__ );
		}

		else
		{
			//� ��� �� �������� ��� ���������
			ret = AdapterGlobalSMS( Status, 0, 0 ); //��������� �� ����� - ������ 0

			if( ret < 0 )
			{
				DBGERR( "%s: Can't send new event\n", __func__ );
			}

			//���� ���� ��������� ����� ��� �� �����������!
			//TODO - � ��� ����� ���� �� ��� ����� ��� ���������� ���������
			sleep( 10 );
		}
	}

	else
	{
		LOG( "Adapter Sleep\r\n" );
	}

	//NOTE - ���� � ��� ���� ��������� � �� �� ���� - �� ������ ������� ���
	//��� �� ���� ���� ����� � ����� �� ������ ��� ��� ��� ��� ���������
	if( Status->GlobalDevice_t.dmdev_interface.dmctrl_ipresent &&
		(Status->GlobalDevice_t.dmdev_interface.dmctrl_status & DM_INTERFACE_STATUS_IN_SLEEP) == 0 )
	{
		DBGLOG( "%s: Have normal interface work - try command for sleep it\n", __func__ );

		int iret = AdapterInterfaceSendCommand( Status, AXIS_INTERFACE_CMD_GO_SLEEP );

		if( iret < 0 )
		{
			DBGERR( "%s:%i: It wasn't possible to send event 'Go Sleep' to interface\n", __func__, __LINE__ );
		}
	}


	//TODO - ��� �� ���� ����� - � ���� �� ��������� WiFi ����� � USB HUB ��� �����-��2

	AdapterTimerPutStatusFlags( Status );

	//�� ������� �� �� �� ������ ��� ��� - �������� �����������
	if( Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature & DM_DM_ENABLE_DIAGNOSTIC )
	{
		Status->GlobalDevice_t.dmdevice_powerupvalue = 2; //TODO - ��������� ���� ��� �� ��������� � ��� ���������
		if( AdapterSaveDiagnostic( &Status->GlobalDevice_t, Status ) < 0 )
		{
			DBGERR( "%s: It wasn't possible to save diagnostic info\n", __func__ );
		}
	}

	//�� ������ ����� ���������� ��������� ��� ������� �����������
	//��� ����� ���� ����� �� ����� ������� ����� �� ��������� ��������
	//����� ����������, �� �����������, ��� ��� ����� �����������
	//� ��������������� ������ ����������
	DBGLOG( "%s: Try stop all networks!!!\n", __func__ );
	if(AdapterAllNetworksStop( Status) < 0)
	{
		DBGERR("%s: It wasn't possible to stop network\n",__func__);
	}
	DBGLOG( "%s: All networks stoped!!!\n", __func__ );

	//������� ����������� ��� �� ����� ����������
	AdapterLedHardwareOn( Status );

	if( DevicePower )
	{
		Status->GlobalDevice_t.dmdev_lpc.dmctrl_status |= DM_LPC_LEAVE_ACC;
	}

	else
	{
		Status->GlobalDevice_t.dmdev_lpc.dmctrl_status &= ~DM_LPC_LEAVE_ACC;
	}

	if( AdapterDmDeviceSleepGo( &Status->GlobalDevice_t ) )
	{
		DBGERR( "%s: Some errors while going to sleep\n", __func__ );
		return -1;
	}

	AdapterGlobalClose( Status );

	exit( 0 );

	return 0;
}

/**
 * @brief ������� ��������� ���������� ��
 *
 * @param dev - ��������� �� ��������� ����������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterPowerRcOn( dmdev_t *dev )
{
	if( !dev )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	return DMXXXPowerRcOn( dev );
}

/**
 * @brief ������� ������� ��������� ���������� ��
 *
 * @param dev - ��������� �� ��������� ����������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterPowerRcOff( dmdev_t *dev )
{
	if( !dev )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	return DMXXXPowerRcOff( dev );
}

/**
 * @brief �������, ������� ������������� ��������� USB ������ �� ������� ����������
 *
 * @param dev - ��������� �� ��������� ����������
 * @param Settings - ��������� �� ��������� ���������� ��������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterPowerControlUSB( dmdev_t *dev, SoftAdapterExchangeSettingsStruct_t *Settings )
{
 	int ret;
	uint8_t USBPorts;
	int i;
	
	if( !dev || !Settings )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	if( !( dev->dmdev_usb.dmctrl_feature & DM_USB_POWER_CTRL ) )
	{
		DBGLOG( "%s: Device not nead power control USB\n", __func__ );
		return 0;
	}

	if( !dev->dmdev_usb.dmctrl_icontrol || !dev->dmdev_usb.dmctrl_ipresent )
	{
		DBGERR( "%s: USB device not present or not controlled\n", __func__ );
		return -AXIS_ERROR_INVALID_VALUE;
	}

	if( dev->dmdev_usb.dmctrl_feature & DM_USB_BITMASK_CTRL )
	{
	 	DBGERR( "%s: Not supported bitmask port control\n", __func__ );
		//TODO - ���������� ������� ����� ����� ����� USB �������� ������
		return -AXIS_ERROR_NOT_INIT;
	}
	
	USBPorts = dev->dmdev_usb.dmctrl_feature & DM_USB_NUM_MASK;
	
	if( !USBPorts )
	{
	 	DBGERR( "%s: Device not have a USB ports\n", __func__ );
		return -AXIS_ERROR_INVALID_VALUE;
	}

	if( USBPorts > 4 ) //TODO Constants
	{
	 	DBGERR( "%s: Adapter settings support only 4 ports (%d)\n", __func__, USBPorts );
	}
	
	//�� ���� ������ � ����������
	for( i = 0; i < USBPorts; i++ )
	{
		uint16_t SettingsPortMask = ( AXIS_SOFTADAPTER_USB__ENABLE_01 << i );

		if( ( Settings->Block0.AdapterSettings.IOAlarmUSB & AXIS_SOFTADAPTER_USB__MASK ) & SettingsPortMask )
		{
			DBGLOG( "%s: Try power on USB port %i\n", __func__, i+1 );

			ret = adapter_power_on_usb_n( dev, i+1 );

			if( ret < 0 )
			{
				DBGERR( "%s: It wasn't possible to power on USB port %i\n", __func__, i+1 );
				return ret;
			}
		}
		else
		{
			DBGLOG( "%s: Try power off USB port %i\n", __func__, i+1 );

			ret = adapter_power_off_usb_n( dev, i+1 );

			if( ret < 0 )
			{
				DBGERR( "%s: It wasn't possible to power off USB port %i\n", __func__, i+1 );
				return ret;
			}
		}

	}

	return 0;
}

/**
 * @brief �������, ������� ������������� ��������� Ethernet �������� �� ������� ����������
 *
 * @param dev - ��������� �� ��������� ����������
 * @param Settings - ��������� �� ��������� �������� �������� ��������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterPowerControlEthernet( dmdev_t *dev, SoftAdapterExchangeSettingsStruct_t *Settings )
{
	int ret;

	if( !dev || !Settings )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	if( ( Settings->Block0.AdapterSettings.Connect & AXIS_SOFTADAPTER_CONNECT__ADAPTER_TYPE ) == AXIS_SOFTADAPTER_CONNECT__ADAPTER_TYPE_ETH )
	{
		ret = adapter_power_on_ethernet( dev );

		if( ret < 0 )
		{
			DBGERR( "%s: It wasn't possible to power on erthernet\n", __func__ );
			return ret;
		}
	}

	else
	{
		ret = adapter_power_off_ethernet( dev );

		if( ret < 0 )
		{
			DBGERR( "%s: It wasn't possible to power off ethernet\n", __func__ );
			return ret;
		}
	}

	return 0;
}

/**
 * @brief ������� ������� ������������ �������� �� ������������ Ethernet � USB
 *
 * @param dev - ��������� �� ��������� ���������
 * @param Settings - ��������� �� ��������� ���������� �������� ��������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterPowerControl( dmdev_t *dev, SoftAdapterExchangeSettingsStruct_t *Settings )
{
	int ret;

	if( !dev || !Settings )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	ret = AdapterPowerControlUSB( dev, Settings );

	if( ret < 0 )
	{
		DBGERR( "%s: It wasn't possible to control usb power\n", __func__ );
		return ret;
	}

	ret = AdapterPowerControlEthernet( dev, Settings );

	if( ret < 0 )
	{
		DBGERR( "%s: It wasn't possible to control power ethernet\n", __func__ );
		return ret;
	}

	return 0;
}

/**
 * @brief �������, ������� ������������ ��������� ��������� �� �����
 *
 * @param Settings - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterPowerMask( AdapterGlobalStatusStruct_t *Statuss )
{
	int USBPort	= 0;
	int ret		= 0;

	if( !Statuss )
	{
		DBGERR( "%s: Invalid Argument\n", __func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	if( !Statuss->PowerDeviceMask )
	{
		DBGERR( "%s: Device power mask is empty\n", __func__ );
		return -AXIS_ERROR_INVALID_VALUE;
	}

	if( Statuss->PowerDeviceMask & DM_RECORDER_RC_MODEM_WIFI )
	{
		USBPort = DMXXXUsbGetWiFiPort( &Statuss->GlobalDevice_t );

		if( !USBPort )
		{
			DBGERR( "%s: Device not support get USB WiFi port\n", __func__ );
			ret = -AXIS_ERROR_NOT_INIT;
		}

		else
		{
			ret = adapter_power_on_usb_n( &Statuss->GlobalDevice_t, USBPort );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't power on USB WiFi on port %d\n", __func__, USBPort );
			}
		}

		//������� �����, ����� ���� �� �������
		Statuss->PowerDeviceMask &= ~DM_RECORDER_RC_MODEM_WIFI;

		if( ret < 0 )
		{
			return ret;
		}
	}

	if( Statuss->PowerDeviceMask & DM_RECORDER_RC_MODEM_LTE )
	{
		USBPort = DMXXXUsbGetLTEPort( &Statuss->GlobalDevice_t );

		if( !USBPort )
		{
			DBGERR( "%s: Device not support get USB LTE port\n", __func__ );
			ret = -AXIS_ERROR_NOT_INIT;
		}

		else
		{
			ret = adapter_power_on_usb_n( &Statuss->GlobalDevice_t, USBPort );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't power on USB LTE on port %d\n", __func__, USBPort );
			}
		}

		//������� �����, ����� ���� �� �������
		Statuss->PowerDeviceMask &= ~DM_RECORDER_RC_MODEM_LTE;

		if( ret < 0 )
		{
			return ret;
		}
	}

	return ret;

}

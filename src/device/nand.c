//#ifdef TMS320DM365
//���������� �������
#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <dmxxx_lib/dmxxx_calls.h>

//���������
#include <adapter/global.h>
#include <adapter/nand.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
//#undef	CONFIG_ENCODER_DEBUG_MSG

#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif

int AdapterNandInit( dmdev_t *dev, Nand_t *nand_s)
{
	if( !dev || !nand_s )
	{
		DBGERR("%s:%i: Invalid Argumentn\n",__func__,__LINE__);
		return -1;
	}
	if(dev->dmdev_nand.dmctrl_ipresent)
	{
		if ( DMXXXNandGetPeakStart( dev, &nand_s->flashpeakstart, &nand_s->fullsize ) )
		{
			DBGERR( "%s:It wasn't possible to get nand offsets\n", __func__ );
			return -1;
		}
		if ( DMXXXNandGetPeakEnd( dev, &nand_s->flashpeakend) < 0 )
		{
			DBGERR( "%s:It wasn't possible to get nand peaksize\n", __func__ );
			return -1;
		}
	}
	return 0;
}

int  AdapterNandGetPeakEnd(dmdev_t *dev,uint64_t *peaksize)
{
	if( !dev || !peaksize )
	{
		DBGERR("%s:%i: Invalid Argumentn\n",__func__,__LINE__);
		return -1;
	}
	
	if(dev->dmdev_nand.dmctrl_ipresent)
	{
		if ( DMXXXNandGetPeakEnd( dev, peaksize) < 0 )
		{
			DBGERR( "%s:It wasn't possible to get nand peaksize\n", __func__ );
			return -1;
		}
	}

	
	return 0;
}


int AdapterNandDeInit()
{

	return 0;
}


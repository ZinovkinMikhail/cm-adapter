//���������� �������
#include <stdio.h>
#include <stdlib.h>
#include <sys/mount.h>
#include <dlfcn.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <errno.h>
#include <string.h>
#include <sched.h>
#include <inttypes.h>
#include <sys/sysmacros.h>

#if 1
#include <sys/select.h>

#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#endif

#include <axis_error.h>
#include <axis_dmlibs_resource.h>
#include <axis_find_from_file.h>
#include <axis_httpi_tags.h>
#include <axis_sprintf.h>
#include <axis_ssl_httpi_download.h>
#include <axis_process.h>
#include <axis_time.h>

#include <cicada_usb_ioctl.h>

#include <dispatcher_lib/printf.h>
#include <dispatcher_lib/dispatcher_lib_resource.h>
#include <dispatcher_lib/getopt.h>
#include <dispatcher_lib/monitoring.h>
#include <dispatcher_lib/monitoring_common.h>

#include <dmxxx_lib/dmxxx_calls.h>

#include <adapter/global.h>
#include <adapter/hardware.h>
#include <adapter/recorder.h>
#include <adapter/httpd.h>
#include <adapter/timer.h>
#include <adapter/dmdevice.h>
#include <adapter/leds.h>
#include <adapter/interface.h>
#include <adapter/settings.h>
#include <adapter/network.h>
#include <adapter/power.h>
#include <adapter/debug.h>
#include <adapter/usb.h>

#include <dmxxx_lib_recorder/dmxxx_lib_recorder_cals.h>

#define MAX_LOCKS	82


//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#include <unistd.h>
#else
#define DBGLOG( ... )
#define DBGINFO( ... )
#define DBGINFOE( ... )
#endif

static pid_t ParentPid = 0;
static pthread_mutex_t mutex_go;

typedef struct LockControl_s
{
	pid_t PID;
	const char *func;
	uint32_t line;
	pthread_mutex_t *mutex;
	
}LockControl_t;



LockControl_t Lock[MAX_LOCKS];

#define TESTPIDS_COUNT		26

static void LockControlInit(void)
{
	ParentPid = axis_getpid();
	
	memset(Lock,0,sizeof(Lock));
}

static void TestForPresentProcess(LockControl_t* pLock)
{
	pid_t pids[TESTPIDS_COUNT] = {0};

	pthread_mutex_lock(&mutex_go);
	
	int children_count =  axis_get_process_pid_childs( ParentPid , pids,TESTPIDS_COUNT);

	if(children_count < 0)
	{
		DBGERR("%s:It wasn't possible to get children\n",__func__);
		pthread_mutex_unlock(&mutex_go);
		return ;
	}
	int i = 0;

	for(i=0;i<children_count;i++)
	{
		DBGINFO("PIDS = %i\n",pids[i]);
		if(pLock->PID == pids[i])
		{DBGINFO("PIDS out\n");
			pthread_mutex_unlock(&mutex_go);
			return;
		}
	}

	DBGINFO("NO CHILD FREE LOCK %i %p\n",pLock->PID,pLock->mutex);

	//� ����� �����
	if(!pLock->PID && pLock->mutex == NULL)
	{
		DBGINFO("KILLED BEFORE %i \n",pLock->PID);
		pthread_mutex_unlock(&mutex_go);
		return;
	}
	if(pLock->mutex)
	{
		pthread_mutex_unlock(pLock->mutex);
	}
	pLock->mutex = NULL;
	pLock->PID  = 0;
pthread_mutex_unlock(&mutex_go);
}

static LockControl_t* FindLock(LockControl_t* Locks,int lockscount,pthread_mutex_t *mutex)
{
	int i = 0;
	pthread_mutex_lock(&mutex_go);
	for(i = 0;i < lockscount;i++)
	{
		if(Locks[i].mutex !=NULL)
		{
			DBGINFO("Locks = %i\n",Locks[i].PID);
			if(Locks[i].mutex == mutex)
			{
				DBGINFO("Found Lock locked by %i\n",Locks[i].PID);
		pthread_mutex_unlock(&mutex_go);
				return &Locks[i];
			}
		}
	}
		pthread_mutex_unlock(&mutex_go);
	return NULL;
}

static LockControl_t* GetFreeLock(LockControl_t* Locks,int lockscount)
{
	int i = 0;
	pthread_mutex_lock(&mutex_go);
	for(i = 0;i < lockscount;i++)
	{
		if(Locks[i].mutex == NULL)
		{pthread_mutex_unlock(&mutex_go);
			return &Locks[i];
		
		}
	}
		DBGINFOE("No free lock!!!i\n");
		pthread_mutex_unlock(&mutex_go);
	return NULL;
}


pid_t PID = 0;



#define pthread_mutex_hard( a)  \
{\
	struct sched_param params,prevparams;\
	int policy = 0;\
	 pthread_t this_thread = pthread_self();\
	params.sched_priority = sched_get_priority_max(SCHED_FIFO);\
	pthread_getschedparam(this_thread, &policy, &prevparams);\
	pthread_setschedparam(this_thread, SCHED_FIFO, &params);\
	\
	DBGINFO("%s: %i Lock Hard\n",__func__,__LINE__);\
	LockControl_t* Test = GetFreeLock(Lock,MAX_LOCKS);if(!Test)return -1;\
	LockControl_t* pLock = FindLock(Lock,MAX_LOCKS,a);\
if(pLock){DBGINFO("%s: %i Locked Hard by %i\n",__func__,__LINE__,pLock->PID);TestForPresentProcess(pLock);}\
\
pthread_mutex_lock( a);\
\
if(pLock){pLock->PID = axis_getpid();pLock->mutex = a;}\
else{ pLock = GetFreeLock(Lock,MAX_LOCKS); if(pLock){pLock->PID = axis_getpid();pLock->func = __func__;pLock->line = __LINE__; pLock->mutex = a;}} \
\
DBGINFO("%s: %i Locked Hard\n",__func__,__LINE__);\
pthread_setschedparam(this_thread, policy, &prevparams);\
\
}

#define pthread_mutex_hard_pointer( a)  \
{\
	struct sched_param params,prevparams;\
	int policy = 0;\
	 pthread_t this_thread = pthread_self();\
	params.sched_priority = sched_get_priority_max(SCHED_FIFO);\
	pthread_getschedparam(this_thread, &policy, &prevparams);\
	pthread_setschedparam(this_thread, SCHED_FIFO, &params);\
	\
	DBGINFO("%s: %i Lock Hard\n",__func__,__LINE__);\
	LockControl_t* Test = GetFreeLock(Lock,MAX_LOCKS);if(!Test) return NULL;\
	LockControl_t* pLock = FindLock(Lock,MAX_LOCKS,a);\
if(pLock){DBGINFO("%s: %i Locked Hard by %i\n",__func__,__LINE__,pLock->PID);TestForPresentProcess(pLock);}\
\
pthread_mutex_lock( a);\
\
if(pLock){pLock->PID = axis_getpid();pLock->mutex = a;}\
else{ pLock = GetFreeLock(Lock,MAX_LOCKS); if(pLock){pLock->PID = axis_getpid();pLock->func = __func__;pLock->line = __LINE__;pLock->mutex = a;}} \
\
DBGINFO("%s: %i Locked Hard\n",__func__,__LINE__);\
pthread_setschedparam(this_thread, policy, &prevparams);\
\
}



#define pthread_mutex_lock_my( a)  \
{\
	struct sched_param params,prevparams;\
	int policy = 0;\
	 pthread_t this_thread = pthread_self();\
	params.sched_priority = sched_get_priority_max(SCHED_FIFO);\
	pthread_getschedparam(this_thread, &policy, &prevparams);\
	pthread_setschedparam(this_thread, SCHED_FIFO, &params);\
	\
DBGINFO("%s: %i Lock My %p \n",__func__,__LINE__,a);\
LockControl_t* Test = GetFreeLock(Lock,MAX_LOCKS);if(!Test)return -1;\
LockControl_t* pLock = FindLock(Lock,MAX_LOCKS,a);\
if(pLock){DBGINFO("%s: %i Locked Timed by %i\n",__func__,__LINE__,pLock->PID);TestForPresentProcess(pLock);}\
\
 struct timespec ts;\
  ts.tv_sec = time(NULL)+3;\
  ts.tv_nsec = 0;\
 	if( pthread_mutex_timedlock(a,&ts))\
	{\
		DBGINFOE("%s: %i No lock my My %p\n",__func__,__LINE__,a);\
		DBGERR("%s: Can't lock recorder at 3 sec... locked by pid=%i %s:%u\n", __func__, pLock ? pLock->PID : 0, pLock ? pLock->func : "", pLock ? pLock->line : 0 );\
		pthread_setschedparam(this_thread, policy, &prevparams);\
		return -1;\
		}\
\
if(pLock){pLock->PID = axis_getpid();pLock->mutex = a;}\
else{ pLock = GetFreeLock(Lock,MAX_LOCKS); if(pLock){pLock->PID = axis_getpid();pLock->func = __func__;pLock->line = __LINE__;pLock->mutex = a;}} \
\
DBGINFO("%s: %i Locked My %i %p \n",__func__,__LINE__,pLock->PID,pLock->mutex);\
pthread_setschedparam(this_thread, policy, &prevparams);\
}

#define pthread_mutex_unlock_my( a)  \
{\
	struct sched_param params2,prevparams2;\
	int policy2 = 0;\
	 pthread_t this_thread2 = pthread_self();\
	params2.sched_priority = sched_get_priority_max(SCHED_FIFO);\
	pthread_getschedparam(this_thread2, &policy2, &prevparams2);\
	pthread_setschedparam(this_thread2, SCHED_FIFO, &params2);\
	\
DBGINFO("%s: %i Unlock\n",__func__,__LINE__);\
LockControl_t* pLock = FindLock(Lock,MAX_LOCKS,a);\
if(pLock)DBGINFO("%s: %i Locked Timed by %i UnlockFrom %i \n",__func__,__LINE__,pLock->PID,axis_getpid());\
	pthread_mutex_unlock(a);\
\
if(pLock){pLock->PID = 0;pLock->mutex=NULL;}\
	pthread_setschedparam(this_thread2, policy2, &prevparams2);\
	DBGINFO("OUT UNLOCK\n");\
}

#define pthread_mutex_lock_my_pointer( a)  \
{\
	struct sched_param params,prevparams;\
	int policy = 0;\
	 pthread_t this_thread = pthread_self();\
	params.sched_priority = sched_get_priority_max(SCHED_FIFO);\
	pthread_getschedparam(this_thread, &policy, &prevparams);\
	pthread_setschedparam(this_thread, SCHED_FIFO, &params);\
	\
DBGINFO("%s: %i Lock My \n",__func__,__LINE__);\
\
LockControl_t* pLock = FindLock(Lock,MAX_LOCKS,a);\
if(pLock){DBGINFO("%s: %i Locked Timed by %i\n",__func__,__LINE__,pLock->PID);TestForPresentProcess(pLock);}\
\
 struct timespec ts;\
  ts.tv_sec = time(NULL)+3;\
  ts.tv_nsec = 0;\
 	if( pthread_mutex_timedlock(a,&ts))\
	{\
		DBGINFOE("%s: %i No lock my My\n",__func__,__LINE__);\
		pthread_setschedparam(this_thread, policy, &prevparams);\
		return NULL;\
		}\
\
if(pLock){pLock->PID = axis_getpid();pLock->mutex = a;}\
else{ pLock = GetFreeLock(Lock,MAX_LOCKS);pLock->PID = axis_getpid();pLock->func = __func__;pLock->line = __LINE__;pLock->mutex = a;} \
\
DBGINFO("%s: %i Locked My\n",__func__,__LINE__);\
pthread_setschedparam(this_thread, policy, &prevparams);\
}




/*
#define pthread_mutex_unlock_my( a)  \
{\\\
	struct sched_param params,prevparams;\
	int policy = 0;\
	 pthread_t this_thread = pthread_self();\
	struct sched_param params;\
	params.sched_priority = sched_get_priority_max(SCHED_FIFO);\
	pthread_getschedparam(this_thread, &policy, &prevparams);\
	pthread_setschedparam(this_thread, SCHED_FIFO, &params);\
	\
DBGINFO("%s: %i Unlock\n",__func__,__LINE__);\
LockControl_t* pLock = FindLock(Lock,MAX_LOCKS,a);\
if(pLock)DBGINFO("%s: %i Locked Timed by %i UnlockFrom %i \n",__func__,__LINE__,pLock->PID,axis_getpid());\
	pthread_mutex_unlock(a);\
\
if(pLock){pLock->PID = 0;pLock->mutex=NULL;}\
}*/


#define RECORDER_HEAD 	 "Head"


int recorder_lock_time(pthread_mutex_t *lock)
{
		pthread_mutex_lock_my( lock );
		return 0;
}

int recorder_unlock(pthread_mutex_t *lock)
{
		pthread_mutex_unlock_my( lock );
		return 0;
}
#ifdef NOT_NOW
//������������ �����
static	char cicada_usb_major_name[] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', };





static int adapter_recorder_mount_usbfs( )
{
	int ret;
	ret = axis_find_pattern_from_file( MOUNTS_PROC_FILE, USBFS_MOUNT_TYPE );

	if ( ret < 0 )
	{
		DBGERR( "%s: Can't found mount usbfs\n", __func__ );
		return ret;
	}
	else if ( ret == 1 )
	{
		DBGLOG( "%s: usbfs already mount\n", __func__ );
	}
	else
	{
		//���������� ���������
		ret = mount( USBFS_MOUNT_TYPE, USBFS_MOUNT_TARGET,
		             USBFS_MOUNT_TYPE, 0, NULL );

		if ( ret < 0 )
		{
			DBGERR( "%s: Can't mount usbfs\n", __func__ );
			return ret;
		}
	}

	return 0;

}


static int adapter_recorder_init_usb_recorder( dmdev_t *dev, struct Recorder_s *Recorder )
{
	if ( !dev || !Recorder )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	int ret;

	int i = 0;
	char strbuffer[1024];

	char *dalee = NULL;
	char *end = NULL;

	char *DeviceFindName = NULL;

	int waitrecordertimeout = 0;

	if ( dev->dmdev_recorder.dmctrl_feature & DM_RECORDER_ALWAYS_NEED )
	{
		waitrecordertimeout = ADPATER_USB_DEVICE_WAITE_TIMEOUT;
	}
	else
	{
		waitrecordertimeout = 1;
	}

	for ( i = 0; i < waitrecordertimeout; i++ )
	{

		DeviceFindName = DM365_BANJO_USBDEV_NAME1;
		//��������� - ������� ���� � ����� ����, � �� ��������
		sleep( 1 );

		ret = axis_find_pattern_from_file_ex( strbuffer,
						      sizeof( strbuffer ),
		                                      CICADA_USB_DEVICE_FILE,
		                                      dev->dmdev_recorder.dmdev_device_name );

		if ( ret < 0 )
		{
			DBGERR( "%s: Error find device in driver file\n", __func__ );
			return ret;
		}

		if ( ret == 1 )
		{

			//����� �� ������ ����������� SN ����� ������� � ����������� �����
			//������ ������ ���������
			//��������� ������� ���� �������� �����
			dalee = strpbrk( strbuffer,
					 dev->dmdev_recorder.dmdev_device_name );

			if ( dalee )
			{
				end = strchr( dalee, '\t' );

				if ( !end )
				{
					end = strchr( dalee, ' ' );
				}

				if ( !end )
				{
					DBGERR( "%s:It wasn't possible to get name from usb recorder proc string\n", __func__ );
					return -1;
				}

				*end = 0x00;

				Recorder->type = DispatcherLibAdapterDeviceFromString( dalee );

				if ( !Recorder->type )
				{
					DBGERR( "%s: Wrong recorder name, may be recorder not supported\n", __func__ );
					return -1;
				}

				break;
			}

		}
	}

	int major = atoi( strbuffer );

	if( major < DM365_USB_MINMAL_MAJOR_NUMBER ||
		major >= DM365_USB_MAXIMAL_MAJOR_NUMBER )
	{
		DBGERR( "%s: Invalid Major number for device %d\n", __func__, major );
		return -AXIS_ERROR_INVALID_ANSED;
	}

	end++;

	Recorder->hen = major;

	return 0;


}
#endif

static void* adapter_recorder_alloc()
{
	return  malloc( sizeof( struct Recorder_s ) );
}

int AdapterRecorderResetErrors(AdapterGlobalStatusStruct_t *Status)
{
	if(!Status)
	{
		DBGERR("%s:Invalid argument\n",__func__);
		return -1;
	}

	Status->currentRecerrors = Status->RecorderDiagnostic.requests_errored;

	return 0;
}

int AdapterRecorderRecover(AdapterGlobalStatusStruct_t *Status)
{
	if(!Status)
	{
		DBGERR("%s:Invalid Argument\n",__func__);
		return -1;
	}
	
	DMXXXResetUsbDSP(&Status->GlobalDevice_t);

	return 0;
	
}

int AdapterRecorderCheckAndRecover( AdapterGlobalStatusStruct_t *Status )
{
	if( !Status )
	{
		DBGERR( "%s: Invalid argument\n", __func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	if( Status->RecorderDiagnostic.requests_errored > Status->currentRecerrors )
	{
		DBGERR( "%s:%i: Recorder ERROR count changed %"PRIu64"->%"PRIu64" try reinit\n", __func__, __LINE__,
											Status->currentRecerrors, Status->RecorderDiagnostic.requests_errored );
		AdapterRecorderRecover( Status );
	}

	AdapterRecorderResetErrors( Status );

	return 0;
}

struct Recorder_s* adapter_recorder_find_last_recorder( struct Recorder_s* head )
{
	if ( !head )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return NULL;
	}

	struct Recorder_s *last;

	if ( head->recorderscount > 0 )
	{
		last = head->next;

		while ( last )
		{
			if ( last->next )
			{
				last = last->next;
			}
			else
			{
				return last;
			}
		}
	}

	return head;
}

static void*  adapter_recorder_alloc_node( struct Recorder_s *head )
{
	if ( !head )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return NULL;
	}

	struct Recorder_s* last;

	last = adapter_recorder_find_last_recorder( head );

	if ( !last || last->next )
	{
		DBGERR( "%s:It wasn't possible to find last recorder\n", __func__ );
		return NULL;
	}

	last->next = adapter_recorder_alloc();

	if ( !last->next )
	{
		DBGERR( "%s:It wasn't possible to allocate noode\n", __func__ );
		return NULL;
	}

	memset( last->next, 0, sizeof( struct Recorder_s ) );

	last->next->prev = last;
	last->next->head = head;
	return last->next;

}

static int adapter_recorder_free_node( struct Recorder_s *recorder )
{

	if ( recorder )
	{

		if ( !recorder->prev )
		{
			DBGERR( "%s:%i:Attempt to free head node\n", __func__, __LINE__ );
			return -1;
		}

		//������� �����
		if ( recorder->next != NULL )
		{
			if ( recorder->prev )
			{
				recorder->prev->next = recorder->next;
				recorder->next->prev = recorder->prev;
			}
		}
		else
		{
			recorder->prev->next = NULL;
		}
		free(( void* )recorder );

		recorder = NULL;
	}

	return 0;

}

static int adapter_recorder_init_head( struct Recorder_s *Recorder )
{
	if ( !Recorder )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	Recorder->next = NULL;

	Recorder->prev = NULL;
	Recorder->recordernum = 0;
	Recorder->recorderscount = 0;
	Recorder->type = RECORDER_HEADER_TYPE_ID;

	if ( sem_init( &Recorder->semaphore, 0, 1 ) < 0 )
	{
		DBGERR( "%s: Can't init semaphore\n", __func__ );
		return -1;
	}

	if ( pthread_mutex_init( &Recorder->lock, NULL ) )
	{
		DBGERR( "%s:It wasn't possible to init mutex\n", __func__ );
		return -1;
	}

	return 0;
}

static int adapter_recorder_lib_configure( struct Recorder_s *recorder,char *recorder_name )
{
	if ( !recorder )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	char path[256];

	if ( !recorder_name )
	{
		DBGERR( "%s:Recorder not supported\n", __func__ );
		return -1;
	}

	snprintf( path, sizeof( path ), RECORDER_LIB_MASK, recorder_name );

	recorder->recorderlib = dlopen( path, RTLD_NOW );

	if ( !recorder->recorderlib )
	{
		DBGERR( "%s:It wasn't possible to open lib recorder not supported %s %s\n", __func__, path, dlerror() );
		return -1;
	}


	recorder->recorder_print_ini_alarm 		= dlsym( recorder->recorderlib, DMXXX_RECORDER__LIB_PRINT_ALARM_INI );
	recorder->recorder_get_diagnostic 			= dlsym( recorder->recorderlib, DMXXX_RECORDER__LIB_GET_DIAGNOSTIC_DATA );
	recorder->recorder_set_diagnostic 			= dlsym( recorder->recorderlib, DMXXX_RECORDER__LIB_SET_DIAGNOSTIC_DATA );
	recorder->recorder_init 					= dlsym( recorder->recorderlib, DMXXX_RECORDER__LIB_INIT );
	recorder->recorder_deinit 					= dlsym( recorder->recorderlib, DMXXX_RECORDER__LIB_DEINIT );
	recorder->recorder_get_settings 			= dlsym( recorder->recorderlib, DMXXX_RECORDER__LIB_SETTINGS_GET );
	recorder->recorder_put_settings 			= dlsym( recorder->recorderlib, DMXXX_RECORDER__LIB_SETTINGS_PUT );
	recorder->recorder_get_timers 				= dlsym( recorder->recorderlib, DMXXX_RECORDER__LIB_TIMERS_GET );
	recorder->recorder_put_timers 				= dlsym( recorder->recorderlib, DMXXX_RECORDER__LIB_TIMERS_PUT );
	recorder->recorder_get_status 				= dlsym( recorder->recorderlib, DMXXX_RECORDER__LIB_STATUS_GET );
	recorder->recorder_get_nand_status 		= dlsym( recorder->recorderlib, DMXXX_RECORDER__LIB_STATUS_NAND_GET );
	recorder->recorder_print_ini_status 		= dlsym( recorder->recorderlib, DMXXX_RECORDER__LIB_PRINTF_INI_STATUS );
	recorder->recorder_print_ini_info_status 	= dlsym( recorder->recorderlib, DMXXX_RECORDER__LIB_PRINTF_INI_INF_STATUS );
	recorder->recorder_print_in_file_status 	= dlsym( recorder->recorderlib, DMXXX_RECORDER__LIB_PRINT_RECORDER_INFO );
	recorder->recorder_print_ini_monitoring_net_info_status = dlsym( recorder->recorderlib, DMXXX_RECORDER__LIB_PRINT_MONITORING_NET_INFO );
	recorder->recorder_get_recorder_alias 		= dlsym( recorder->recorderlib, DMXXX_RECORDER__LIB_GET_RECORDER_ALIAS );
	recorder->recorder_get_version_date 		= dlsym( recorder->recorderlib, DMXXX_RECORDER__LIB_STATUS_GET_DEV_VERSION );
	recorder->recorder_print_ini_settings 		= dlsym( recorder->recorderlib, DMXXX_RECORDER__LIB_PRINTF_INI_SETTINGS );
	recorder->recorder_do_command 				= dlsym( recorder->recorderlib, DMXXX_RECORDER__LIB_COMMAND_DO );
	recorder->recorder_upload 					= dlsym( recorder->recorderlib, DMXXX_RECORDER__LIB_UPLOAD );
	recorder->recorder_http_monitor 			= dlsym( recorder->recorderlib, DMXXX_RECORDER__LIB_HTTP_MONITORING );
	recorder->recorder_get_status_threads 		= dlsym( recorder->recorderlib, DMXXX_RECORDER__LIB_GET_STATUS_THREADS );
	recorder->recorder_check_while 			= dlsym( recorder->recorderlib, DMXXX_RECORDER__LIB_CHECK_WHILE );
	recorder->recorder_sync_time				= dlsym( recorder->recorderlib, DMXXX_RECORDER__LIB_SYNC_TIME );
	recorder->recorder_get_time_from_struct	= dlsym( recorder->recorderlib, DMXXX_RECORDER__LIB_GET_TIME_FROM_STRUCT );
	recorder->recorder_get_acc_status			= dlsym( recorder->recorderlib, DMXXX_RECORDER__LIB_STATUS_ACC_GET );
	recorder->recorder_print_ini_acc_status	= dlsym( recorder->recorderlib, DMXXX_RECORDER__LIB_PRINTF_INI_ACC_STATUS );
	recorder->recorder_get_sn					= dlsym( recorder->recorderlib, DMXXX_RECORDER__LIB_GET_SN );
	recorder->recorder_print_more_info			= dlsym( recorder->recorderlib, DMXXX_RECORDER__LIB_PRINT_MORE_INFO );
	recorder->recorder_get_voltage_temp		= dlsym( recorder->recorderlib, DMXXX_RECORDER__LIB_GET_VOLTAGE_TEMP );
	recorder->recorder_convert_acc_to_standart	= dlsym( recorder->recorderlib, DMXXX_RECORDER__LIB_CONVERT_ACC_TO_STANDART_STRUCT );
	recorder->recorder_print_logs				= dlsym( recorder->recorderlib, DMXXX_RECORDER__LIB_LOGS );
	recorder->recorder_get_adapter_timers		= dlsym( recorder->recorderlib, DMXXX_RECORDER__LIB_GET_ADAPTER_TIMERS );
	recorder->recorder_put_adapter_timers		= dlsym( recorder->recorderlib, DMXXX_RECORDER__LIB_PUT_ADAPTER_TIMERS );
	recorder->recorder_get_adapter_settings	= dlsym( recorder->recorderlib, DMXXX_RECORDER__LIB_GET_ADAPTER_SETTINGS );
	recorder->recorder_put_adapter_settings	= dlsym( recorder->recorderlib, DMXXX_RECORDER__LIB_PUT_ADAPTER_SETTINGS );
	recorder->recorder_convert_status_to_standart_struc = dlsym( recorder->recorderlib, DMXXX_RECORDER__LIB_CONVERT_STATUS_TO_STANDART_STRUCT );
	recorder->recorder_led_command				= dlsym( recorder->recorderlib, DMXXX_RECORDER__LIB_WORK_LED );
	recorder->recorder_get_rcr_status			= dlsym( recorder->recorderlib, DMXXX_RECORDER__LIB_GET_RCR_STATUS );
	recorder->recorder_get_firmware_status_flag	= dlsym( recorder->recorderlib, DMXXX_RECORDER__LIB_GET_FIRMWARE_STATUS );
	recorder->recorder_clear_firmware_status_flag = dlsym( recorder->recorderlib, DMXXX_RECORDER__LIB_CLEAR_FIRMWARE_STATUS );
	recorder->recorder_get_adapter_firmware		= dlsym( recorder->recorderlib, DMXXX_RECORDER__LIB_GET_ADAPTER_FIRMWARE );
	recorder->recorder_get_adapter_timers_status_flag = dlsym( recorder->recorderlib, DMXXX_RECORDER__LIB_GET_ADAPTER_TIMERS_STATUS_FLAG );
	recorder->recorder_put_adapter_timers_status_flag = dlsym( recorder->recorderlib, DMXXX_RECORDER__LIB_PUT_ADAPTER_TIMERS_STATUS_FLAG );
	recorder->recorder_monitoring_send_alarm	 = dlsym( recorder->recorderlib, DMXXX_RECORDER__LIB_MONITORING_SEND_ALARM_EVENT );
	recorder->recorder_get_interface_sleep_time	 = dlsym( recorder->recorderlib, DMXXX_RECORDER__LIB_SETTINGS_GET_INTERFACE_SLEEP_TIME );

	return 0;
}

static int adapter_recorder_lib_deconfigure( struct Recorder_s *recorder )
{
	if ( !recorder )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	if ( !recorder->recorderlib )
	{
		DBGLOG( "%s:%i:Lib already closed\n", __func__, __LINE__ );
		return 0;
	}

	if(recorder->recorder_deinit)
	{	
		recorder->recorder_deinit(&recorder->recorder_data);
	}

	dlclose( recorder->recorderlib );

	return 0;
}

static int adapter_recorder_lib_init( struct Recorder_s *recorder )
{
	if ( !recorder )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	DBGLOG("INIT RECOREDER ONCE\n");

	if ( recorder->recorder_init )
	{
		recorder->recorder_data.private_data = (void*)&recorder->hen;
		
		if ( recorder->recorder_init( &recorder->recorder_data ) )
		{
			DBGERR( "%s: It wasn't possible to init recorder libs\n", __func__ );
			return -1;
		}
	}

	//���� ���������� �������� ������� �������������, �� ��� ��������� ������ ���� ����������������
	//�������� ���! ����� ����� ������� ������ � ����������� �����
	if( !recorder->recorder_data.recorderaccstatus_ex )
	{
		DBGERR( "%s: Recorder not init ACC status struct\n", __func__ );
		goto error;
	}

	if( !recorder->recorder_data.recordernandstatus_ex )
	{
		DBGERR( "%s: Recorder not init NAND status struct\n", __func__ );
		goto error;
	}

	if( !recorder->recorder_data.recordersettings_ex )
	{
		DBGERR( "%s: Recorder not init Settings struct\n", __func__ );
		goto error;
	}

	if( !recorder->recorder_data.recorderstatus_ex )
	{
		DBGERR( "%s: Recorder not init Status struct\n", __func__ );
		goto error;
	}

	if( !recorder->recorder_data.recorderstimers_ex )
	{
		DBGERR( "%s: Recorder not init Timer struct\n", __func__ );
		goto error;
	}

	if( !recorder->recorder_data.recordertempdata )
	{
		DBGERR( "%s: Recorder not init Temp data struct\n", __func__ );
		goto error;
	}

	return 0;

error:
	//��� �� �� ��� �� ���������� ����������

	if( recorder->recorder_deinit )
	{
		recorder->recorder_deinit( &recorder->recorder_data );
	}

	return -AXIS_ERROR_NOT_INIT;

}

#ifdef NOT_NOW
static int adapter_recorder_check_if_recorder_in( dmdev_t *dev, struct Recorder_s *head )
{
	if ( !head || !dev )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	struct Recorder_s find_recorder;

	if ( dev->dmdev_recorder.dmctrl_feature & DM_RECORDER_HW && dev->dmdev_recorder.dmctrl_feature & DM_RECORDER_HW_USB )
	{
		if ( adapter_recorder_init_usb_recorder( dev, &find_recorder ) )
		{
			DBGERR( "%s:It wasn't possible to init recorder", __func__ );
			return -1;
		}

		if ( find_recorder.hen > 0 )
		{

			struct Recorder_s *newrecorder;
			newrecorder = adapter_recorder_alloc_node( head );

			if ( !newrecorder )
			{
				DBGERR( "%s:It wasn't possible to allocate new recorder\n", __func__ );
				return -1;
			}

			head->recorderscount++;

			newrecorder->hen = find_recorder.hen;
			newrecorder->recordernum = head->recorderscount;
			newrecorder->recorderscount = head->recorderscount;
			newrecorder->type = find_recorder.type;

			if ( adapter_recorder_lib_configure( newrecorder,"NE ISPOLIZUY ETU FUNCTSIYU" ) )
			{
				//������ �� �������������� �� �� ����
				DBGERR( "%s:It wasn't possible to init lib\n", __func__ );
				adapter_recorder_free_node( newrecorder );
				head->recorderscount--;
				return 0;
			}

			if ( adapter_recorder_lib_init( newrecorder ) )
			{
				DBGERR( "%s:It wasn't possible to init lib work\n", __func__ );
				return -1;
			}

			if ( sem_init( &newrecorder->semaphore, 0, 1 ) < 0 )
			{
				DBGERR( "%s: Can't init semaphore\n", __func__ );
				return -1;
			}

			if ( pthread_mutex_init( &newrecorder->lock, NULL ) )
			{
				DBGERR( "%s:It wasn't possible to init mutex\n", __func__ );
				return -1;
			}
		}
	}

	//�������������� ������� ��� ������� � ����������
	return 0;
}
#endif

static int adapter_deinit_recorder( struct Recorder_s *recorder )
{
	uint32_t wait_count = 0;

	if ( !recorder )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	struct Recorder_s *head = recorder->head;

recheckpids:
	wait_count++;

	if(recorder->usecount > 0)
	{
		pid_t pids[TESTPIDS_COUNT] = {0};

		int children_count =  axis_get_process_pid_childs(ParentPid, pids,TESTPIDS_COUNT);

		if( children_count < 0 )
		{
			DBGERR( "%s: It wasn't possible to get children\n", __func__ );
			return -children_count;
		}
		
		int i = 0;
		int userin = 0;

		for( i=0; i < children_count; i++ )
		{

			DBGINFO( "PIDS = %i %i\n", pids[i], recorder->UserPID );

			if( recorder->UserPID == pids[i] )
			{
				userin = 1;
				break;
			}
		}

		if( !userin )
		{
			recorder->usecount--;
		}
	}

	while( recorder->usecount > 0 )
	{
		//������ �������� ������������ ���������� - ��� ������
		if( wait_count > 5 )
		{
			DBGERR( "%s:%i: Wait while free recorder at PID=%d count=%d\n", __func__, __LINE__, recorder->UserPID, recorder->usecount );
		}
		else
		{
			DBGLOG( "%s:%i: Wait while free recorder at PID=%d count=%d\n", __func__, __LINE__, recorder->UserPID, recorder->usecount );
		}

		sleep(1);

		goto recheckpids;
	}

	pthread_mutex_hard( &head->lock );

	DBGLOG( "%s: Head locked\n", __func__ );

	//TODO - ���� ���� ������ ����������� � �������, �� ������� �� ��� ������ ���� �������� ������������!!!
	pthread_mutex_hard( &recorder->lock );

	DBGLOG( "%s: Rec locked\n", __func__ );

	recorder->hen = -1;
	if ( adapter_recorder_lib_deconfigure( recorder ) )
	{
		DBGERR( "%s: It wasn;t possible to deconfigure recorder \n", __func__ );
		pthread_mutex_unlock_my( &recorder->lock );
		pthread_mutex_unlock_my( &head->lock );
		return -1;
	}
	if ( adapter_recorder_free_node( recorder ) )
	{
		DBGERR( "%s: It wasn't possible to delete recorder\n", __func__ );
		pthread_mutex_unlock_my( &recorder->lock );
		pthread_mutex_unlock_my( &head->lock );
		return -1;
	}
	head->recorderscount--;
	pthread_mutex_unlock_my( &recorder->lock );

	pthread_mutex_unlock_my( &head->lock );
	return 0;
}

static int adapter_delete_all_recs( struct Recorder_s *head )
{
	if ( !head )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	if ( !head->recorderscount )
	{
		DBGLOG( "%s:%i:No recorders inited\n", __func__, __LINE__ );
		return 0;
	}

	while ( head->next && head->recorderscount )
	{
		DBGLOG( "%s:%i:Deiniting recorder %x\n",
							__func__,
							__LINE__,
							head->next->type );

		if ( adapter_deinit_recorder( head->next ) )
		{
			DBGERR( "%s:It wasn't possible to deinit recorder\n", __func__ );
			return -1;
		}

		head->recorderscount--;
	}

	return 0;
}

static dev_t adapter_recorder_char_info_get_dev( const char* name )
{
	if ( !name )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	//�������������� � ��2
	int major = 0;

	char buf[128];

	FILE *devices;

	devices = fopen( "/proc/devices", "r" );

	if ( devices == NULL )
	{
		DBGERR( "%s:It wasn't possible to open /proc/devices\n", __func__ );
		return 0;
	}

	//TODO - ��� �� ���� ��� ������ � ����� /sys/devices/virtual/misc/cicada_usb_info/dev
	
	// ������ ������ "<maj> <name>"
	while ( fgets( buf, sizeof( buf ), devices ) != NULL )
	{
		char *sp = strchr( buf, ' ' );

		if ( !sp ) continue;

		if ( buf[strlen( buf ) - 1] == '\n' )
			buf[strlen( buf ) - 1] = '\0';

		// ������� ������������
		if ( !strcmp( sp + 1, name ) )
		{
			*sp = 0;
			major = atoi( buf );

			if ( major < 0 || major > 65536 )
			{
				DBGERR( "%s:Invalid Major number\n", __func__ );
				goto err;
			}

			break;
		}
	}

	if ( ferror( devices ) )
	{

		DBGERR( "%s: Error reading from /proc/devices", __func__ );
		goto err;
	}

	if ( feof( devices ) )
	{

		DBGERR( "%s:%i:Device wasn't found in /proc/devices\n",
								__func__,
								__LINE__ );
		goto err;
	}

	if ( fclose( devices ) == EOF )

		DBGERR( "%s: Error closing/proc/devices", __func__ );

// 	DBGLOG( "%s:%i:MAJOR = %i\n", __func__, __LINE__, major );

	return makedev( major, 0 );

err:
	fclose( devices );

	return 0;


}

static int adapter_recorder_char_info_close( int fd )
{
	close( fd );
	return 0;
}

static int adapter_recorder_char_info_open( const char* chardevname )
{
	if ( !chardevname )
	{
		DBGERR( "%s:Invalid argument\n", __func__ );
		return -1;
	}

	char path[64];

	int ret = 0;

	DBGLOG( "%s: Try open '%s' recorder device\n", __func__, chardevname );
	snprintf( path, sizeof( path ), RECORDER_USB_INFO_NODE_MASK, chardevname );
	DBGLOG( "%s: Try open '%s' recorder device\n", __func__, path );

	ret = access( path, F_OK );

	if ( ret < 0 )
	{
		dev_t dev = adapter_recorder_char_info_get_dev( chardevname );

		if ( !dev )
		{

			DBGERR( "%s:It wasn't possible to find major number\n", __func__ );
			return -1;
		}

		ret = mknod( path,  S_IFCHR | S_IRUSR, dev );

		if ( ret < 0 )
		{
			DBGERR( "%s:It wasnt possible to mknod!!!\n", __func__ );
			return -1;

		}
	}

	ret = open( path, O_RDWR );

	if ( ret < 0 )
	{

		DBGERR( "%s:It wasn't possible to open info device '%s'\n", __func__, path );
		return -1;

	}

	return ret;

}

/**
 * @brief �������, ������� ������������ ��������, ��� �� ���� ������� �� ������ ���������� � ������� ����� �������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
static inline int RecorderUSBNoRecorderFatal( AdapterGlobalStatusStruct_t *Status )
{
	//�� ����� ����������� ��� �������, ��� ����� ��� ���������� ��� ������ �� ����
	//TODO - ���� ������ ��� �� ��2, ������� ����� �������� � ��� ����������
	//���� �� ���� ��� ��� ����� ������� - ��� ������ ���� ��������������� ��������
	return AdapterPowerGlobalReboot( Status );
}

/**
 * @brief �������, ������� ������������ ��������, ��� �� ���� ������� �� ������ ����������
 *
 * @param Status - ��������� �� ���������� ��������� ������� ����������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
static int RecorderUSBNoRecordersAtMoreTime( AdapterGlobalStatusStruct_t *Status )
{
	int ret;
	int RecorderUSBPort = 0;

	if( !Status )
	{
		DBGERR( "%s: Invalid Argument\n", __func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	RecorderUSBPort = DMXXXUsbGetRecorderPort( &Status->GlobalDevice_t );

	if( RecorderUSBPort < 0 )
	{
		DBGERR( "%s: Can't get recorder USB port from device library\n", __func__ );
		return RecorderUSBPort;
	}

	if( !RecorderUSBPort )
	{
		DBGLOG( "%s: No recorder USB port detected. Try reswitch USB....\n", __func__ );

		//��� �����, ��� ��� �����, �� � ��������� ������� ���� ������� ������������ �����
		ret = AdapterUsbReSwitch( &Status->GlobalDevice_t );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't reswitch USB\n", __func__ );
			return ret;
		}

		return 0; //�� ���� ������� ��� ����� �� ��������� - ��� �����
	}

	DBGLOG( "%s: -- Try off USB port %d\n", __func__, RecorderUSBPort );

	ret = AdapterPowerUSB( &Status->GlobalDevice_t, RecorderUSBPort, 0 );

	if( ret < 0 )
	{
		DBGERR( "%s: -- Can't power off USB recorder port\n", __func__ );
		return ret;
	}

	DBGLOG( "%s: -- Try sleep one sec\n", __func__ );

	sleep( 3 );

	DBGLOG( "%s: Try on USB port %d\n", __func__, RecorderUSBPort );

	ret = AdapterPowerUSB( &Status->GlobalDevice_t, RecorderUSBPort, 1 );

	if( ret < 0 )
	{
		DBGERR( "%s: -- Can't power on USB recorder port\n", __func__ );
		return ret;
	}

	return 0;
}

/**
 * @brief ������� �������� ��������� ���������� �� ���� USB
 *
 * @param Status - ��������� �� ���������� ��������� ������� ��������
 * @param dev - ��������� �� ��������� ���������
 * @param Recorder - ��������� �� ��������� ����������
 * @return int - 0 ��� �� - ���������� �����������, ����� 0 � ������ ������
 **/
static int adapter_recorder_wait_recorder( AdapterGlobalStatusStruct_t *Status, dmdev_t *dev, struct Recorder_s *Recorder)
{
	if ( !dev || !Recorder || Recorder->hen <= 0)
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	fd_set rfds;	//��������� ��� �������

	int ret;

	struct timeval tv;	//��������� ��������
	int retval;		//�������
	int max_sel	= 0;	//������������ ������ ��� �������
	unsigned int NoFoundCount = 0; //���������� ������� �������� ����������

	while ( 1 )
	{
		FD_ZERO( &rfds );
		FD_SET( Recorder->hen, &rfds );
		max_sel = Recorder->hen;
		max_sel++;
		tv.tv_sec = 1;
		tv.tv_usec = 0;
		retval = select( max_sel, &rfds, NULL, NULL, &tv );

		if ( retval < 0 )
		{
			//������ �������
			if ( errno == EINTR )
			{
				//��� �� ������ - �� ������� ������!!!
				DBGLOG( "%s: Select signal interrupt\n", __func__ );
				continue;
			}

			//������ ������� - ����� �� �����
			DBGERR( "%s: Select Error\n", __func__ );

			ret = -AXIS_ERROR_CANT_IOCTL_DEVICE;

			break;
		}

		if ( !retval )
		{
			DBGLOG( "%s:%i: Wait recorder timeout\n", __func__, __LINE__ );
			AdapterDmDeviceWatchDogTick(dev);		
			NoFoundCount++;
			if( NoFoundCount % 8 == 0) //TODO - Constants
			{
				DBGERR( "%s: No USB recorders at more times 0x%08x\n", __func__, NoFoundCount );
				INFO_LOG( 1, "No USB recorders at more times" );
				
				if( NoFoundCount % 32 == 0 )	//TODO - Constants
				{
					DBGERR( "%s: No USB recorders at more fatal times 0x%08x\n", __func__, NoFoundCount );
					INFO_LOG( 1, "No USB recorders at more fatal times" );

					//�� �� ������� �� ������ USB ���������� �� ����� ������ �����
					//��� � ����� ��������� ������ - � ����� �� ��������� ������� ��������
					//��� ����� ������ ��� � �������������� �����
					ret = RecorderUSBNoRecorderFatal( Status );

					if( ret < 0 )
					{
						DBGERR( "%s: Can't work no USB recorder found fatal\n", __func__ );
					}
				}

				else
				{
					ret = RecorderUSBNoRecordersAtMoreTime( Status );

					if( ret < 0 )
					{
						DBGERR( "%s: Can't work no USB recorder found\n", __func__ );
					}

				}
				
			}
			continue;
		}

		if(FD_ISSET(Recorder->hen, &rfds ))
		{
			
			if(AdapterRecorderProcessEvent(dev,Recorder))
			{
				DBGERR("%s:It wasn't possible to process event\n",__func__);
				return -1;
			}
			if(Recorder->recorderscount > 0)
			{
				DBGLOG("%s:%i:Recorder found can continue\n",__func__,__LINE__);
				return 0;
			}

			continue;
		}
	}
	return ret;
}

/**
 * @brief ������� ������������� ����������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @param dev - ��������� �� ��������� ���������
 * @param Recorder - ��������� �� ��������� ����������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterRecorderInit( AdapterGlobalStatusStruct_t *Status, dmdev_t *dev, struct Recorder_s *Recorder )
{
	if ( !dev || !Recorder )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	if ( !dev->dmdev_recorder.dmctrl_ipresent )
	{
		return 0;
	}

	
	if ( pthread_mutex_init( &mutex_go, NULL ) )
	{
		DBGERR( "%s:It wasn't possible to init mutex\n", __func__ );
		return -1;
	}
	LockControlInit();

	

	if ( adapter_recorder_init_head( Recorder ) )
	{
		DBGERR( "%s:It wasn't possible to init head\n", __func__ );
		return -1;
	}

	if ( AdapterRecorderInitControlRecorders( dev, Recorder ) )
	{
		DBGERR( "%s:It wasn't possible to init recorder char info\n", __func__ );
		return -1;
	}
	
 	if ( dev->dmdev_recorder.dmctrl_feature & DM_RECORDER_HW_USB )
	{
//����� ������� ��� �� �����
		DBGLOG("%s: Try find recorder event\n", __func__ );
		if ( AdapterRecorderProcessEvent( dev, Recorder ) )
		{
			DBGERR( "%s:It wasn't possible to check if recorder in\n", __func__ );
			return -1;
		}

		if ( dev->dmdev_dm3xx.dmctrl_feature &
				DM_DM_ENABLE_USB_FIRMWARE_SETTING_IN_RECORDER )
		{
		 
		 	DBGERR("%s: Try USB Firmware Setting. Recorder count %d\n",
										__func__,
										Recorder->recorderscount );
			if ( Recorder->recorderscount > 0 )
			{
				return 0;
			}
			else
			{
			 	DBGERR("%s: Try wait recorder\n", __func__ );
				if ( adapter_recorder_wait_recorder( Status, dev, Recorder ) )
				{
					DBGERR( "%s:It wasn'r possible to wait recorder\n", __func__ );
					return -1;
				}
			}
		}
	}

	return 0;
}

int AdapterRecorderDeInit( dmdev_t *dev, struct Recorder_s *Recorder )
{
	if ( !dev || !Recorder )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	if ( !dev->dmdev_recorder.dmctrl_ipresent )
	{
		return 0;
	}

	if ( AdapterRecorderDeinitControlRecorders( dev, Recorder ) )
	{
		DBGERR( "%s:It wasnt possible to deinit control recorder\n", __func__ );
		return 0;
	}

	return adapter_delete_all_recs( Recorder );
}

static int adapter_recorder_char_info_read_structure( struct Recorder_s *Recorder )
{
	if ( !Recorder )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	if ( Recorder->hen <= 0 )
	{
		DBGERR( "%s:Char info not opened\n", __func__ );
		return -1;
	}

	if ( ioctl( Recorder->hen, CHAR_INFO_IOCTL_USE_STRUCT ) < 0 )
	{
		DBGERR( "%s:It wasn't possible to ioctl\n", __func__ );
		perror("ioctl");
		return -1;
	}

	if( ioctl( Recorder->hen, CHAR_INFO_IOCTL_USE_RESET ) < 0 )
	{
		DBGERR( "%s:It wasn't possible to Resey\n", __func__ );
		perror( "ioctl" );
		return -1;
	}

	return 0;
}

int AdapterRecorderInitControlRecorders( dmdev_t* dev, struct Recorder_s *Recorder )
{
	if ( !dev || !Recorder )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	if ( Recorder->type != RECORDER_HEADER_TYPE_ID )
	{
		DBGERR( "%s:Not recorder header!!!! %i \n", __func__, Recorder->type );
		return -1;
	}

	if ( !( dev->dmdev_recorder.dmctrl_feature & DM_RECORDER_USB_INFO ) )
	{
		return 0;
	}

	if ( !dev->dmdev_recorder.dmdev_device_name )
	{
		DBGERR( "%s:No char info device name\n", __func__ );
		return -1;
	}

	Recorder->hen = adapter_recorder_char_info_open( dev->dmdev_recorder.privatedata );

	if ( Recorder->hen <= 0 )
	{
		DBGERR( "%s:It wasnt possible to open recorder char info device\n", __func__ );
		return -1;
	}


	if ( adapter_recorder_char_info_read_structure( Recorder ) )
	{
		DBGERR( "%s:It wasn't possible to control read info\n", __func__ );
		return -1;
	}

	return 0;

}

int AdapterRecorderDeinitControlRecorders( dmdev_t* dev, struct Recorder_s *Recorder )
{
	if ( !dev || !Recorder )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	if ( Recorder->type != RECORDER_HEADER_TYPE_ID )
	{
		DBGERR( "%s:Not recorder header!!!! %i \n", __func__, Recorder->type );
		return -1;
	}

	if ( !( dev->dmdev_recorder.dmctrl_feature & DM_RECORDER_USB_INFO ) )
	{
		return 0;
	}

	adapter_recorder_char_info_close( Recorder->hen );

	return 0;

}

static struct Recorder_s* adapter_recorder_find_recorder( struct Recorder_s *head, uint64_t sn )
{
	if ( !head )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return NULL;
	}

	if ( head->type != RECORDER_HEADER_TYPE_ID )
	{
		DBGERR( "%s:Not head structs\n", __func__ );
		return NULL;
	}

	if ( head->recorderscount <= 0 )
	{
		return NULL;
	}

	
	pthread_mutex_hard_pointer( &head->lock );

	struct Recorder_s *recorderfound;

	recorderfound = head->next;

	while ( recorderfound )

	{

		DBGLOG( "%s:%i:SerialRec  = %llx Serial In = %llx Address = %p\n", __func__, __LINE__, recorderfound->sn, sn ,recorderfound);

		if ( recorderfound->sn == sn )
		{
			pthread_mutex_unlock_my( &head->lock );
			return recorderfound;
		}
		else
		{
			recorderfound = recorderfound->next;
		}
	}

	pthread_mutex_unlock_my( &head->lock );

	return NULL;

}

int AdapterRecorderRelease( struct Recorder_s *recorder)
{
	if( !recorder )
	{
		DBGERR("%s:%i: Invalid Argumentn\n",__func__,__LINE__);
		return -1;
	}

	if(recorder->usecount > 0)
	{
		recorder->usecount--;
	}
	
	return 0;
}

static int adapter_recorder_move_recorder_up( struct Recorder_s *head, struct Recorder_s *recorder )
{
	if ( !head || !recorder )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	if ( head->type != RECORDER_HEADER_TYPE_ID )
	{
		DBGERR( "%s:Not head structs\n", __func__ );
		return -1;
	}

	if ( head->recorderscount < 0 )
	{
		DBGLOG( "%s:%i:No recorders in head \n", __func__, __LINE__ );
		return 0;
	}

	if ( recorder->prev )
	{
		if ( recorder->next )
		{
			recorder->prev->next = recorder->next;
			recorder->next->prev = recorder->prev;
		}
		else
		{
			recorder->prev->next = NULL;
		}
	}

	if ( head->next )
	{
		recorder->next = head->next;
		recorder->next->prev = recorder;
	}

	head->next = recorder;

	recorder->prev = head;

	return 0;
}

struct Recorder_s** AdapterRecorderFindAndUpRecorder( struct Recorder_s *head, uint64_t sn )
{
	if ( !head )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return NULL;
	}

	struct Recorder_s *recorder;

	recorder = adapter_recorder_find_recorder( head, sn );

	if ( !recorder )
	{
		DBGLOG( "%s:%i:No recorder with such serial\n", __func__, __LINE__ );
		return NULL;
	}

	if ( adapter_recorder_move_recorder_up( head, recorder ) )
	{
		DBGERR( "%s:It wasn't possible to up recorder\n", __func__ );
		return NULL;
	}

	recorder->UserPID = axis_getpid();
	
	recorder->usecount++;

	return &head->next;
}

int AdapterRecorderSetupControls(struct Recorder_s *recorder)
{

	//recorder->recorder_data.Controls.
	//����� ���������� �� ����
	recorder->recorder_data.Controls.get_led_num = NULL;;
	recorder->recorder_data.Controls.led_blink = &AdapterNetworkLedBlink;
	recorder->recorder_data.Controls.led_on = &AdapterNetworkLedOn;
	recorder->recorder_data.Controls.led_off = &AdapterNetworkLedOff;
	recorder->recorder_data.Controls.led_fastblink = &AdapterNetworkLedFastBlink;
	recorder->recorder_data.Controls.event_callback = NULL;
	recorder->recorder_data.Controls.port =  recorder->port+1;

	return 0;


}

static int adapter_recorder_add_new( dmdev_t*dev, struct Recorder_s *head, struct cicada_usb_event_info *info )

{
	if ( !dev || !head || !info )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	if ( head->type != RECORDER_HEADER_TYPE_ID )
	{
		DBGERR( "%s:Invalid Recorder type not head\n", __func__ );
		return -1;
	}

	if(head->recorderscount > 0 &&
			!( dev->dmdev_dm3xx.dmctrl_feature & DM_DM_WORK_SEPARATE))
	{
		DBGLOG("%s:%i:Available to register only one recorder\n",__func__,__LINE__);
		return 0;
	}

	pthread_mutex_hard( &head->lock );

	if ( dev->dmdev_recorder.dmctrl_feature & DM_RECORDER_HW &&
			dev->dmdev_recorder.dmctrl_feature & DM_RECORDER_HW_USB )
	{

		struct Recorder_s *newrecorder;
		newrecorder = adapter_recorder_alloc_node( head );

		if ( !newrecorder )
		{
			DBGERR( "%s:It wasn't possible to allocate new recorder\n", __func__ );
			pthread_mutex_unlock_my( &head->lock );
			return -1;
		}

		head->recorderscount++;

		newrecorder->hen = info->major;
		newrecorder->recordernum = head->recorderscount;
		newrecorder->recorderscount = head->recorderscount;
		newrecorder->sn = info->serial;
		newrecorder->type =  DispatcherLibAdapterDeviceFromString( info->name );
		newrecorder->port = info->port;

		if( !newrecorder->type )
		{
			DBGERR( "%s: Recorder '%s' is not supported\n", __func__, info->name );
			adapter_recorder_free_node( newrecorder );
			head->recorderscount--;
			pthread_mutex_unlock_my( &head->lock );
			return -AXIS_ERROR_NOT_INIT;
		}

		if ( adapter_recorder_lib_configure( newrecorder ,info->name) )
		{
			//������ �� �������������� �� �� ����
			DBGERR( "%s:It wasn't possible to init lib\n", __func__ );
			adapter_recorder_free_node( newrecorder );
			head->recorderscount--;
			pthread_mutex_unlock_my( &head->lock );
			return -1;
		}

		if ( adapter_recorder_lib_init( newrecorder ) )
		{
			DBGERR( "%s:It wasn't possible to init lib work\n", __func__ );
			adapter_recorder_free_node( newrecorder );
			head->recorderscount--;
			pthread_mutex_unlock_my( &head->lock );
			return -1;
		}

		if ( sem_init( &newrecorder->semaphore, 0, 1 ) < 0 )
		{
			DBGERR( "%s: Can't init semaphore\n", __func__ );
			adapter_recorder_free_node( newrecorder );
			head->recorderscount--;
			pthread_mutex_unlock_my( &head->lock );
			return -1;
		}

		if ( pthread_mutex_init( &newrecorder->lock, NULL ) )
		{
			DBGERR( "%s:It wasn't possible to init mutex\n", __func__ );
			adapter_recorder_free_node( newrecorder );
			head->recorderscount--;
			pthread_mutex_unlock_my( &head->lock );
			return -1;
		}

		if ( AdapterRecorderSetupControls(newrecorder) < 0)
		{
			DBGERR( "%s:It wasn't possible to init cintrols\n", __func__ );
			adapter_recorder_free_node( newrecorder );
			head->recorderscount--;
			pthread_mutex_unlock_my( &head->lock );
			return -1;
		}

		if(AdapterRecorderSnGet(newrecorder) < 0)
		{
			DBGERR("%s:It wasn't possible to get sn\n",__func__);
			adapter_recorder_free_node( newrecorder );
			head->recorderscount--;
			pthread_mutex_unlock_my( &head->lock );
			return -1;
		}

		//� ����� � ���, ��� ��� ���������� ���������� �� �� ����� 
		//������� ��� ������� ��� ��� ��� �� � ���������� 
		//������� ���������, � ����� ��� ������
		if (  AdapterRecorderChacheRecorderSettings( dev, newrecorder ) )
		{
			if(AdapterGlobalGetStatus()->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature & DM_DM_RECORDER_NOT_RECORDER)
			{
				DBGERR( "%s: It wasn't possible to cache rec acc, but recorder is not recorder - no error\n", __func__ );
			}
			else
			{
				DBGERR( "%s:It wasn't possible to cache rec acc\n", __func__ );
				pthread_mutex_unlock_my( &head->lock );
				return -1;
			}
		}
		if (  AdapterRecorderChacheRecorderStatus( dev, newrecorder ) )
		{
			DBGERR( "%s:It wasn't possible to cache rec acc\n", __func__ );
			pthread_mutex_unlock_my( &head->lock );
			return -1;
		}
		
		//��� ��. - ������������ ����� �����������
		newrecorder->connecttime = time( NULL );

		//TODO  - ����� ��� ���???
		sleep(5);
	}



	pthread_mutex_unlock_my( &head->lock );

	return 0;

	//�������������� ���
}

int AdapterRecorderProcessEvent( dmdev_t *dev, struct Recorder_s *head )
{
	struct cicada_usb_event_info info;
	struct Recorder_s *recorderforwork;
	int ret;

	if ( !dev || !head )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	if ( head->type != RECORDER_HEADER_TYPE_ID )
	{
		DBGERR( "%s: Invalid Recorder type not head\n", __func__ );
		return -AXIS_ERROR_INVALID_VALUE;
	}

	ret = read( head->hen, &info, sizeof( struct cicada_usb_event_info ) );

	if ( ret < 0 )
	{
		DBGERR( "%s: It wasn't possible to read recorder event\n", __func__ );
		return -AXIS_ERROR_CANT_READ_DEVICE;
	}
	else if( !ret)
	{
		DBGLOG( "%s:%i: Read emty event\n", __func__, __LINE__ );
		
		//������ ��������� - ��� ��� �� ������, ��� � ��� ��� ����������
		//��� ����� ���� ��������, ��� ��� �������� ������, ��� ��� �� �������
		//��� ��������� �� ��� - ������ ��������� ������� ��� ������ �����
		//������� ������ �������� �� �������
		
		//�� ������ ������� ���� /proc/driver/cicada-usb/devices
		//� � ��� ��������� ������ ����� ��� ��� ��� �����
		//� ����� ����� ��� ����������, ������� �������� � ��������� ���������� �
		//���������������� ��� � ���������� ����������

		ret = axis_find_pattern_from_file( "/proc/driver/cicada-usb/devices",
						   dev->dmdev_recorder.dmdev_device_name );
		if( ret < 0 )
		{
			DBGERR( "%s:%i: Error find recorder %s in driver\n", __func__, __LINE__, dev->dmdev_recorder.dmdev_device_name );
			return ret; //��� ����� � ����
		}
		else if( ret )
		{
			DBGERR( "%s:%i: Recorder %s found in driver\n", __func__, __LINE__, dev->dmdev_recorder.dmdev_device_name );
			info.major = UD2_CICADA_USB_CHAR_MAJOR; //TODO - ���� == 192 ��� �� ����� �� ��� �� ����
			strcpy( info.name, dev->dmdev_recorder.dmdev_device_name );
			info.present = 1;
			info.opened = 0;
			info.port = 1;
		}
		else
		{
			DBGERR( "%s:%i: Recorder %s not found in driver\n", __func__, __LINE__, dev->dmdev_recorder.dmdev_device_name );
			return 0;
		}
	}

	if ( !info.present )
	{
		if(AdapterGlobalGetStatus()->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature & DM_DM_RECORDER_NOT_RECORDER)
		{

			DBGLOG( "%s: %i Recorder init %s %s\n",__func__,__LINE__,info.name, dev->dmdev_recorder.dmdev_device_name );

			//���� ��������� ��� ���� �� ����� ���, ����� ����
			if( !strcmp( info.name, dev->dmdev_recorder.dmdev_device_name ) )
			{
				//� ��� ���� �������� � ���� ��� ����� ��������
				recorderforwork = adapter_recorder_find_recorder( head,  strtoull( AdapterGlobalGetStatus()->CurrentNetwork.SN, NULL, 16 ) );
			}
			else
			{
				return 0;
			}
		}
		else
		{
			recorderforwork = adapter_recorder_find_recorder( head, info.serial );
		}

		if( !recorderforwork )
		{
			DBGERR( "%s: No recorder with such serial '%016llx'\n", __func__, info.serial );
			return 0;
		}

		DBGLOG( "%s:%i: Try to deinit recorder\n", __func__, __LINE__ );

		if( adapter_deinit_recorder( recorderforwork ) < 0 )
		{
			DBGERR( "%s: It wasn't possible to deinit recorder\n", __func__ );
			return -AXIS_ERROR_CANT_IOCTL_DEVICE;
		}

		//������� ������� ���� ��������
		DMXXXHWEvent( dev, DM_HW_EVENT_RECORDER_DISCONNECT, NULL );

		int iret = AdapterInterfaceSendCommand( AdapterGlobalGetStatus(), AXIS_INTERFACE_EVENT_RECORDER_OUT );

		if( iret < 0 )
		{
			DBGERR( "%s:%i: It wasn't possible to send event 'Recorder Out' to interface\n", __func__, __LINE__ );
		}
	}
	else
	{
		recorderforwork = adapter_recorder_find_recorder( head, info.serial );

		if( recorderforwork )
		{
			DBGLOG( "%s: Recorder already inited\n", __func__ );
			return 0;
		}

		if( adapter_recorder_add_new( dev, head, &info ) )
		{
			DBGERR( "%s: It wasn't possible to add new recorder\n", __func__ );
			DBGLOG( "%s:%i: Try to retry\n", __func__, __LINE__ );
			return 0;
		}

		//������� ������� ���� ��������
		DMXXXHWEvent( dev, DM_HW_EVENT_RECORDER_CONNECT, NULL );

		int iret = AdapterInterfaceSendCommand( AdapterGlobalGetStatus(), AXIS_INTERFACE_EVENT_RECORDER_IN );

		if( iret < 0 )
		{
			DBGERR( "%s:%i: It wasn't possible to send event 'Recorder In' to interface\n", __func__, __LINE__ );
		}
	}

	return 0;
}

/**
 * @brief ������� ��������� ������� ���������� ����������
 *
 * @param dev - ��������� �� ��������� ����������
 * @param recorder - ��������� �� ��������� ����������
 * @return int - 0 ��� ��, ����� 0 � ������ ������ (AXIS_ERROR_TIMEOUT_EVENT - 409 ������, AXIS_ERROR_BUSY_DEVICE - NoDevice)
 */
int AdapterRecorderGetStatus( dmdev_t *dev, struct Recorder_s *recorder )
{
	int ret = 0;

	if ( !dev || !recorder )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	pthread_mutex_lock_my( &recorder->lock );

	if ( recorder->hen <= 0 )
	{
		DBGERR( "%s:Recorder hendel not opened cannot work\n", __func__ );
		pthread_mutex_unlock_my( &recorder->lock );
		return -AXIS_ERROR_BUSY_DEVICE; //�� � ����� ������ ������ - ���������� ���������� USB
	}

	if ( recorder->recorder_get_status )
	{
		ret = recorder->recorder_get_status( recorder->hen,
						     recorder->recorder_data.recorderstatus_ex,
						     &recorder->semaphore ) ;

		if ( ret )
		{
			AdapterGlobalGetStatus()->RecorderDiagnostic.requests_errored++;

			if( ret == -AXIS_ERROR_CANT_READ_DEVICE ||
				ret == -AXIS_ERROR_BUSY_DEVICE )
			{
				int iret = AdapterInterfaceSendCommand( AdapterGlobalGetStatus(), AXIS_INTERFACE_EVENT_READ_RECORDER_ERROR );

				if( iret < 0 )
				{
					DBGERR( "%s:%i: It wasn't possible to send event 'Recorder Error' to interface\n", __func__, __LINE__ );
				}
			}

			DBGERR( "%s: It wasn't possible to get recorder status\n", __func__ );
			pthread_mutex_unlock_my( &recorder->lock );
			return ret; //������� ������
		}

		recorder->laststatusquery = time( NULL );
		
		AdapterGlobalGetStatus()->RecorderDiagnostic.requests_good++;
	}

	//���� ����������, � ���������� ���������� ����� ����, ��� ���������� ���������
	//������� �� ��������� ��, �� ��������� ��������� ���������, � ������ ����������
	//����� ��������������� ������� - �� �� �� ������� � �� ����������� ������� ��������������� ����
	if( ( dev->dmdev_recorder.dmctrl_feature & DM_RECORDER_RC_MODEM_MASK ) &&
			recorder->recorder_get_rcr_status )
	{
		ret = recorder->recorder_get_rcr_status( recorder->hen, &recorder->semaphore, &recorder->recorder_data );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't get recorder RCR status\n", __func__ );
			//��� ������ - �� ����������� ������ - �� ������ �� � �����
			ret = 0;
		}

		else if( ret & DM_RECORDER_RC_MODEM_MASK )
		{
			//��� ���� ������ ����� �������� �� ����������, ��� ������ ��������!!!
			AdapterGlobalStatusStruct_t	 *GstaTus = AdapterGlobalGetStatus();

			if( !GstaTus )
			{
				DBGERR( "%s: Can't get global status structure pointer\n", __func__ );
			}

			else
			{
				//������� ����, ��� �� ����� �� ������, ��� ������ ��������
				GstaTus->PowerDeviceMask |= ( ret & DM_RECORDER_RC_MODEM_MASK );

				//����������� ������� ��������� WiFi �� ������� �� ����������
				if( (ret & DM_RECORDER_RC_MODEM_MASK) == DM_RECORDER_RC_MODEM_WIFI )
				{
					INFO_LOG( 0, "RemoteControl command WiFi on" );
				}
			}
		}
	}

	if ( recorder->recorder_get_nand_status )
	{
		ret = recorder->recorder_get_nand_status( recorder->hen,
							  recorder->recorder_data.recordernandstatus_ex,
							  &recorder->semaphore ) ;

		if ( ret )
		{
			AdapterGlobalGetStatus()->RecorderDiagnostic.requests_errored++;

			if( ret == -AXIS_ERROR_CANT_READ_DEVICE ||
				ret == -AXIS_ERROR_BUSY_DEVICE )
			{
				int iret = AdapterInterfaceSendCommand( AdapterGlobalGetStatus(), AXIS_INTERFACE_EVENT_READ_RECORDER_ERROR );

				if( iret < 0 )
				{
					DBGERR( "%s:%i: It wasn't possible to send event 'Recorder Error' to interface\n", __func__, __LINE__ );
				}
			}
			DBGERR( "%s: It wasn't possible to get nand status\n", __func__ );
			pthread_mutex_unlock_my( &recorder->lock );
			return ret; //������� ������
		}
		else
		{
			AdapterGlobalGetStatus()->RecorderDiagnostic.requests_good++;
		}
	}


	pthread_mutex_unlock_my( &recorder->lock );

	return ret;

}

uint32_t adapter_recorder_get_timelast( time_t querytime )
{
	return ( time( NULL ) - querytime );
}

/**
 * @brief �������, ������� � ������ ������� ���� ���������� � ���������� �� �������� ���
 *
 * @param dev - ��������� �� ��������� ���������
 * @param recorder - ��������� �� ��������� ����������
 * @return int
 **/
static int AdapterRecorderGetAccStatusCashed( dmdev_t *dev, struct Recorder_s *recorder )
{
	int ret = 0;

	if( !dev || !recorder )
	{
		DBGERR( "%s: Invalid argument\n", __func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	if( adapter_recorder_get_timelast( recorder->laststatusquery ) >
			recorder->recorder_data.recorderstatuscache_timeout )
	{
		//�������� ������ �������� ������ ���
		ret = AdapterRecorderGetAccStatus___( dev, recorder );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't get ACC status from recorder in real\n", __func__ );
		}
	}

	//��� ��� ���� �������� ��������� ��� ��������� � ����
	return ret;
}

/**
 * @brief �������, ��������� ������������� ������� ��� ��� ������� ����������
 *
 * @param dev - ��������� �� ��������� ����������
 * @param head - ��������� �� ������ ������ �����������
 * @return int - 0 ��� �� ����� 0 � ������ ������
 */
int AdapterRecorderGetFistRecorderAccStatusCashed( dmdev_t *dev, struct Recorder_s *head )
{
	struct Recorder_s *recorder;

	if( !dev || !head )
	{
		DBGERR( "%s: Invalid argument\n", __func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	if ( head->type != RECORDER_HEADER_TYPE_ID )
	{
		DBGERR( "%s:Invalid Recorder type not head\n", __func__ );
		return -1;
	}

	recorder = head->next;

	if ( head->recorderscount <= 0 || !recorder  )
	{
		DBGLOG( "%s:%i: Fist recorder noy inited\n", __func__, __LINE__ );
		//��� ���������� � ����� - ��� �� ������
		return 0;
	}

	return AdapterRecorderGetAccStatusCashed( dev, recorder );
}



int AdapterRecorderGetAccStatus___( dmdev_t *dev, struct Recorder_s *recorder ) //TODO - � ����� ������� ������ ���� �����������
{
	if ( !dev || !recorder )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	DBGLOG("%s:%i:ADAAAAAAAA = %p\n",__func__,__LINE__,recorder);

	int ret = 0;

	pthread_mutex_lock_my( &recorder->lock );

	if ( recorder->hen <= 0 )
	{
		DBGERR( "%s:Recorder hendel not opened cannot work\n", __func__ );
		pthread_mutex_unlock_my( &recorder->lock );
		return -1;
	}

	if ( recorder->recorder_get_acc_status )
	{
		ret = recorder->recorder_get_acc_status( recorder->hen,
							 recorder->recorder_data.recorderaccstatus_ex,
							 &recorder->semaphore ) ;

		if( ret )
		{
			if( ret == -AXIS_ERROR_CANT_READ_DEVICE )
			{
				AdapterGlobalGetStatus()->RecorderDiagnostic.requests_errored++;

				int iret = AdapterInterfaceSendCommand( AdapterGlobalGetStatus(), AXIS_INTERFACE_EVENT_READ_RECORDER_ERROR );

				if( iret < 0 )
				{
					DBGERR( "%s:%i: It wasn't possible to send event 'Recorder Error' to interface\n", __func__, __LINE__ );
				}
			}

			DBGERR( "%s:%i: It wasn't possible to get Acc status\n", __func__, __LINE__ );
			pthread_mutex_unlock_my( &recorder->lock );
			return -1;
		}

		AdapterGlobalStatusStruct_t *Status = AdapterGlobalGetStatus();

		if( Status->GlobalDevice_t.dmdev_acc.dmctrl_feature & DM_ACC_FEATURE_RECORDER )
		{
			DBGLOG( "%s:%i: Convert ACC\n",__func__,__LINE__ );
			ret = AdapterRecorderConvertAcctoStandartStructUnlocked( recorder, &Status->ACCStatus );

			if( ret < 0 )
			{
				DBGERR( "%s:%i: It wasn't possible to convert ACC Struct\n",__func__,__LINE__ );
			}
		}

		AdapterGlobalGetStatus()->RecorderDiagnostic.requests_good++;
		//recorder->laststatusquery = time( NULL );
	}

	pthread_mutex_unlock_my( &recorder->lock );

	return ret;

}

static int adapter_recorder_sync_time_from_rec( dmdev_t *dev, struct Recorder_s *recorder, 
															AdapterGlobalStatusStruct_t *Status)
{
	if( !dev || !recorder )
	{
		DBGERR("%s:%i: Invalid Argumentn\n",__func__,__LINE__);
		return -1;
	}
	int ret = 0;
	if(dev->dmdev_dm3xx.dmctrl_feature & DM_DM_RECORDER_SYNC_TIME)
	{
		struct_data dateinrec;
		struct_data dateinad;

		DBGLOG( "%s:%i: TEST\n",__func__,__LINE__);

		if(AdapterRecorderGetTimeFromStructNonBlock(recorder,&dateinrec))
		{
			DBGERR("%s:It wasnt possible to get time from recorder\n",__func__);
			return -1;
		}
		if(AdapterTimerGetTime(dev,&dateinad) < 0)
		{
			DBGERR("%s:It wasn;t possible to get time from adapter\n",__func__);
			return -1;
		}
		if(dateinrec.day == 0x00 && dateinrec.mon == 0x00)
		{
			DBGLOG("%s: %i No sync time allowed\n",__func__,__LINE__);
			return 0;
		}

		if(system_times_compare_no_sec(dateinrec,dateinad) )
		{
			if(AdapterTimerSetTime(dev, &dateinrec) < 0)
			{
				DBGERR("%s: It wasn't possible to set sys time\n",__func__ );
				return -1;
			}
			ret = AdapterTimerSyncTime(dev);
			if(ret  < 0)
			{
				DBGERR("%s:It wasn't possible to sync time\n",__func__);
				return -1;
			}

			//����� ����� ��������� ������ ���� �������� �����
			if( ret == 1 )
			{
				DBGLOG( "%s:%i: Nulling Query time\n", __func__, __LINE__ );

				Status->LastInterfaceQuire = time( NULL );

				//�� ����� ���� ��� ��� ���� ����������� �������
				//��� �� �� ���������, ��� ������ ������� � �������
				//��� ����� � �������
				ret = AdapterTimerWork( Status );

				if( ret < 0 )
				{
					DBGERR( "%s: Can't reinit alarm\n", __func__ );
					return ret;
				}
			}
		}
	}

	return 0;
}

/**
 * @brief ������� ������ ���������� ����������
 *
 * @param dev - ��������� �� ��������� ���������, �� ������� �������� ����������
 * @param recorder - ��������� �� ��������� ����������, ��� �������� ���������� ������ ����������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 *
 * @note �����������, ������ � ���������� ���������� �������� ��������, ����������� ������� AdapterRecorderChacheRecorderSettings
 */
static int AdapterRecorderGetSettings( dmdev_t *dev, struct Recorder_s *recorder )
{
	int ret = 0;
	AdapterGlobalStatusStruct_t *GlobalStatus = AdapterGlobalGetStatus();

	if ( !dev || !recorder || !GlobalStatus )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	pthread_mutex_lock_my( &recorder->lock );

	if ( recorder->hen <= 0 )
	{
		DBGERR( "%s: Recorder hendel not opened cannot work\n", __func__ );
		pthread_mutex_unlock_my( &recorder->lock );
		return -AXIS_ERROR_DEVICE_NOT_OPEN;
	}

	if ( recorder->recorder_get_settings )
	{
		ret = recorder->recorder_get_settings( recorder->hen,
						       recorder->recorder_data.recordersettings_ex,
						       &recorder->semaphore ) ;
		if ( ret < 0 )
		{
			GlobalStatus->RecorderDiagnostic.requests_errored++;
			
			if( ret == -AXIS_ERROR_CANT_READ_DEVICE )
			{
				int iret = AdapterInterfaceSendCommand( GlobalStatus, AXIS_INTERFACE_EVENT_READ_RECORDER_ERROR );

				if( iret < 0 )
				{
					DBGERR( "%s:%i: It wasn't possible to send event 'Recorder Error' to interface\n", __func__, __LINE__ );
				}
			}

			DBGERR( "%s: It wasn't possible to get recorder settings\n", __func__ );
			pthread_mutex_unlock_my( &recorder->lock );
			return ret;
		}

		//NOTE - � ���������� ���������� �������� ��� ����� - �� ������ (������???) ����������������
		//���� ��������� ����� � ���
		if( adapter_recorder_sync_time_from_rec( dev, recorder, GlobalStatus ) )
		{
			DBGERR( "%s: It wasn't possible to sync recorder time with adapter\n", __func__ );
			//NOTE - ��� ������� ������ ���������� ��� �� ����������� ������
		}

		recorder->lastsettingsquery = time( NULL );

		GlobalStatus->RecorderDiagnostic.requests_good++;
	}

	pthread_mutex_unlock_my( &recorder->lock );

	return ret;

}

int AdapterRecorderPutSettings( dmdev_t *dev, struct Recorder_s *recorder )
{
	if ( !dev || !recorder )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	int ret = 0;

	pthread_mutex_lock_my( &recorder->lock );

	if ( recorder->hen <= 0 )
	{
		DBGERR( "%s:Recorder hendel not opened cannot work\n", __func__ );
		pthread_mutex_unlock_my( &recorder->lock );
		return -1;
	}

	if ( recorder->recorder_put_settings )
	{
		ret = recorder->recorder_put_settings( recorder->hen,
						       recorder->recorder_data.recordersettings_ex,
						       &recorder->semaphore, 0 ) ;

		if ( ret )
		{
			AdapterGlobalGetStatus()->RecorderDiagnostic.requests_errored++;
			
			if(ret == -AXIS_ERROR_CANT_READ_DEVICE)
			{
				int iret = AdapterInterfaceSendCommand( AdapterGlobalGetStatus(), AXIS_INTERFACE_EVENT_READ_RECORDER_ERROR );

				if( iret < 0 )
				{
					DBGERR( "%s:%i: It wasn't possible to send event 'Recorder Error' to interface\n", __func__, __LINE__ );
				}
			}
			DBGERR( "%s:It wasn't possible to put settings\n", __func__ );
			pthread_mutex_unlock_my( &recorder->lock );
			return -1;
		}

		recorder->lastsettingsquery = 0 /*time( NULL )*/;

		AdapterGlobalGetStatus()->RecorderDiagnostic.requests_good++;
	}

	pthread_mutex_unlock_my( &recorder->lock );

	return ret;

}

int AdapterRecorderPutSettingsDefault( dmdev_t *dev, struct Recorder_s *recorder )
{
	if ( !dev || !recorder )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	int ret = 0;

	pthread_mutex_lock_my( &recorder->lock );

	if ( recorder->hen <= 0 )
	{
		DBGERR( "%s:Recorder hendel not opened cannot work\n", __func__ );
		pthread_mutex_unlock_my( &recorder->lock );
		return -1;
	}

	if ( recorder->recorder_put_settings )
	{
		ret = recorder->recorder_put_settings( recorder->hen,
						       recorder->recorder_data.recordersettings_ex,
						       &recorder->semaphore,
						       1 ) ;

		if ( ret )
		{
			AdapterGlobalGetStatus()->RecorderDiagnostic.requests_errored++;
			if(ret == -AXIS_ERROR_CANT_READ_DEVICE)
			{
				int iret = AdapterInterfaceSendCommand( AdapterGlobalGetStatus(), AXIS_INTERFACE_EVENT_READ_RECORDER_ERROR );

				if( iret < 0 )
				{
					DBGERR( "%s:%i: It wasn't possible to send event 'Recorder Error' to interface\n", __func__, __LINE__ );
				}
			}
			DBGERR( "%s:It wasn't possible to put settings\n", __func__ );
			pthread_mutex_unlock_my( &recorder->lock );
			return -1;
		}

		AdapterGlobalGetStatus()->RecorderDiagnostic.requests_good++;

		recorder->lastsettingsquery = time( NULL );
	}

	pthread_mutex_unlock_my( &recorder->lock );

	return ret;

}


static int AdapterRecorderGetTimers( dmdev_t *dev, struct Recorder_s *recorder )
{
	if ( !dev || !recorder )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	int ret = 0;

	pthread_mutex_lock_my( &recorder->lock );

	if ( recorder->hen <= 0 )
	{
		DBGERR( "%s:Recorder hendel not opened cannot work\n", __func__ );
		pthread_mutex_unlock_my( &recorder->lock );
		return -1;
	}

	if ( recorder->recorder_get_timers )
	{
		ret = recorder->recorder_get_timers( recorder->hen,
						     recorder->recorder_data.recorderstimers_ex,
						     &recorder->semaphore ) ;
		
		if ( ret )
		{
			AdapterGlobalGetStatus()->RecorderDiagnostic.requests_errored++;
			
			if(ret == -AXIS_ERROR_CANT_READ_DEVICE)
			{
				int iret = AdapterInterfaceSendCommand( AdapterGlobalGetStatus(), AXIS_INTERFACE_EVENT_READ_RECORDER_ERROR );

				if( iret < 0 )
				{
					DBGERR( "%s:%i: It wasn't possible to send event 'Recorder Error' to interface\n", __func__, __LINE__ );
				}

			}
			DBGERR( "%s:It wasn't possible to get timers\n", __func__ );
			pthread_mutex_unlock_my( &recorder->lock );
			return -1;
		}

		AdapterGlobalGetStatus()->RecorderDiagnostic.requests_good++;

		recorder->lastsettingsquery = time( NULL );
	}

	pthread_mutex_unlock_my( &recorder->lock );

	return ret;

}


int AdapterRecorderPutTimers( dmdev_t *dev, struct Recorder_s *recorder )
{
	if ( !dev || !recorder )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	int ret = 0;

	pthread_mutex_lock_my( &recorder->lock );

	if ( recorder->hen <= 0 )
	{
		DBGERR( "%s:Recorder hendel not opened cannot work\n", __func__ );
		pthread_mutex_unlock_my( &recorder->lock );
		return -1;
	}

	if ( recorder->recorder_put_timers )
	{
		ret = recorder->recorder_put_timers( recorder->hen,
						     recorder->recorder_data.recorderstimers_ex,
						     &recorder->semaphore, 0 ) ;

		if ( ret )
		{
			AdapterGlobalGetStatus()->RecorderDiagnostic.requests_errored++;
			if(ret == -AXIS_ERROR_CANT_READ_DEVICE)
			{
				int iret = AdapterInterfaceSendCommand( AdapterGlobalGetStatus(), AXIS_INTERFACE_EVENT_READ_RECORDER_ERROR );

				if( iret < 0 )
				{
					DBGERR( "%s:%i: It wasn't possible to send event 'Recorder Error' to interface\n", __func__, __LINE__ );
				}

			}
			DBGERR( "%s:It wasn't possible to get settings\n", __func__ );
			pthread_mutex_unlock_my( &recorder->lock );
			return -1;
		}

		AdapterGlobalGetStatus()->RecorderDiagnostic.requests_good++;

		recorder->lastsettingsquery = time( NULL );
	}

	pthread_mutex_unlock_my( &recorder->lock );

	return ret;

}

int AdapterRecorderPutTimersDefault( dmdev_t *dev, struct Recorder_s *recorder )
{
	if ( !dev || !recorder )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	int ret = 0;

	pthread_mutex_lock_my( &recorder->lock );

	if ( recorder->hen <= 0 )
	{
		DBGERR( "%s:Recorder hendel not opened cannot work\n", __func__ );
		pthread_mutex_unlock_my( &recorder->lock );
		return -1;
	}

	if ( recorder->recorder_put_timers )
	{
		ret = recorder->recorder_put_timers( recorder->hen,
						     recorder->recorder_data.recorderstimers_ex,
						     &recorder->semaphore, 1 ) ;

		if ( ret )
		{
			AdapterGlobalGetStatus()->RecorderDiagnostic.requests_errored++;
			
			if(ret == -AXIS_ERROR_CANT_READ_DEVICE)
			{
				int iret = AdapterInterfaceSendCommand( AdapterGlobalGetStatus(), AXIS_INTERFACE_EVENT_READ_RECORDER_ERROR );

				if( iret < 0 )
				{
					DBGERR( "%s:%i: It wasn't possible to send event 'Recorder Error' to interface\n", __func__, __LINE__ );
				}

			}
			DBGERR( "%s:It wasn't possible to get settings\n", __func__ );
			pthread_mutex_unlock_my( &recorder->lock );
			return -1;
		}
		AdapterGlobalGetStatus()->RecorderDiagnostic.requests_good++;

		recorder->lastsettingsquery = time( NULL );
	}

	pthread_mutex_unlock_my( &recorder->lock );

	return ret;

}

int AdapterRecorderChacheRecorderSettings( dmdev_t *dev, struct Recorder_s *recorder )
{
	if ( !dev || !recorder )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	if ( adapter_recorder_get_timelast( recorder->lastsettingsquery ) >
	     recorder->recorder_data.recordersettingscache_timeout )
	{
		if ( AdapterRecorderGetSettings( dev, recorder ) )
		{
			if(AdapterGlobalGetStatus()->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature & DM_DM_RECORDER_NOT_RECORDER)
			{
				return -AXIS_ERROR_BUSY_DEVICE;
			}
			DBGERR( "%s:It wasn't possible to get settings for cache\n", __func__ );
			return -1;
		}

		if ( AdapterRecorderGetTimers( dev, recorder ) )
		{
			if(AdapterGlobalGetStatus()->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature & DM_DM_RECORDER_NOT_RECORDER)
			{
				return -AXIS_ERROR_BUSY_DEVICE;
			}
			DBGERR( "%s:It wasn't possible to get timers for cache\n", __func__ );
			return -1;
		}
	}

	return 0;

}

/**
 * @brief - ������� ������ ������� ����������, � ������������ ����������
 *
 * @param dev - ��������� �� ��������� ����������
 * @param recorder - ��������� �� ����������, � �������� ���������� ������
 * @return int - 0 ��� ��, ����� 0 � ������ ������ (AXIS_ERROR_TIMEOUT_EVENT - 409 ������, AXIS_ERROR_BUSY_DEVICE - NoDevice )
 */
int AdapterRecorderChacheRecorderStatus( dmdev_t *dev, struct Recorder_s *recorder )
{
	int ret;

	if ( !dev || !recorder )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	DBGLOG( "%s:%i: Get cache status was called %i sec ago (%u)\n", __func__, __LINE__,
			adapter_recorder_get_timelast( recorder->laststatusquery ),recorder->recorder_data.recorderstatuscache_timeout );

	if ( adapter_recorder_get_timelast( recorder->laststatusquery ) >
	     recorder->recorder_data.recorderstatuscache_timeout )
	{
		DBGLOG( "%s:%i: Get new status from recorder\n", __func__, __LINE__ );

		ret = AdapterRecorderGetStatus( dev, recorder );
		if( ret < 0 )
		{
			DBGERR( "%s: It wasn't possible to get status for cache\n", __func__ );
			return ret;
		}

		//ATTENTION - ����� ������� ��� ������, ��� ��� ������������ ���������
		ret = AdapterRecorderGetAccStatus___( dev, recorder );
		if( ret < 0 )
		{
			DBGERR( "%s: It wasn't possible to cache rec acc\n", __func__ );
			return ret;
		}
	}

	return 0;
}

int AdapterRecorderCacheAllRecordersSettings( dmdev_t *dev, struct Recorder_s *head )
{
	if ( !dev || !head )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	if ( head->type != RECORDER_HEADER_TYPE_ID )
	{
		DBGERR( "%s:Invalid Recorder type not head\n", __func__ );
		return -1;
	}

	struct Recorder_s *recorder;

	if ( head->recorderscount <= 0 )
	{
		DBGLOG( "%s:%i:No recorders inited\n", __func__, __LINE__ );
		return 0;
	}

	recorder = head->next;

	while ( recorder )
	{
		if ( AdapterRecorderChacheRecorderSettings( dev, recorder ) )
		{
			DBGERR( "%s:It wasn't possible to cache rec Settings\n", __func__ );
			//����� � ��������� ��� ����
			//return -1;
		}

		recorder = recorder->next;
	}

	return 0;
}

/**
 * @brief �������, ������� �������� ������ ��� ���� ����������� � �������
 *
 * @param dev - ��������� �� ��������� ���������
 * @param head - ��������� �� ������ ������ �����������
 * @return int - 0 ��� ��, ���� 0 � ������ ������
 */
int AdapterRecorderGetAllRecordersStatus( dmdev_t *dev, struct Recorder_s *head )
{
	if ( !dev || !head )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	if ( head->type != RECORDER_HEADER_TYPE_ID )
	{
		DBGERR( "%s: Invalid Recorder type not head\n", __func__ );
		return -AXIS_ERROR_INVALID_VALUE;
	}

	struct Recorder_s *recorder;

	if ( head->recorderscount <= 0 )
	{
		DBGLOG( "%s:%i: No recorders inited\n", __func__, __LINE__ );
		return 0;
	}

	recorder = head->next;

	while ( recorder )
	{
		if ( AdapterRecorderGetStatus( dev, recorder ) )
		{
			DBGERR( "%s: It wasn't possible to get rec status\n", __func__ );
			return -1;
		}

		recorder = recorder->next;
	}

	return 0;
}

int AdapterRecorderCacheAllRecordersStatus( dmdev_t *dev, struct Recorder_s *head )
{
	if ( !dev || !head )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	if ( head->type != RECORDER_HEADER_TYPE_ID )
	{
		DBGERR( "%s:Invalid Recorder type not head\n", __func__ );
		return -1;
	}

	struct Recorder_s *recorder;

	if ( head->recorderscount <= 0 )
	{
		DBGLOG( "%s:%i:No recorders inited\n", __func__, __LINE__ );
		return 0;
	}

	recorder = head->next;

	while ( recorder )
	{
		if ( AdapterRecorderChacheRecorderStatus( dev, recorder ) )
		{
			DBGERR( "%s:It wasn't possible to cache rec status\n", __func__ );
			return -1;
		}

		recorder = recorder->next;
	}

	return 0;
}

int AdapterRecorderPrintStatusInFile( struct Recorder_s *recorder, int file )
{
	if ( !recorder )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	pthread_mutex_lock_my( &recorder->lock );

	if ( recorder->hen <= 0 )
	{
		DBGERR( "%s:Recorder Not inited\n", __func__ );
		pthread_mutex_unlock_my( &recorder->lock );
		return -1;
	}

	if ( recorder->recorder_print_in_file_status )
	{
		if ( recorder->recorder_print_in_file_status( file,
							      &recorder->recorder_data,
							      recorder->datanotsave ) )
		{
			DBGERR( "%s:It wasn't possible to print recorder status\n", __func__ );
			pthread_mutex_unlock_my( &recorder->lock );
			return -1;
		}
	}

	pthread_mutex_unlock_my( &recorder->lock );

	return 0;
}

/**
 * @brief �������, ������� �������� � ����� ������� ������ ����������� �� UDP
 *
 * @param recorder - ��������� �� ��������� ����������
 * @param b - ��������� �� �����
 * @param bsize - ������ ���������� �����
 * @param client_number - ����� ������ ��� ������
 * @return int - ���������� ������������ ����
 */
int AdapterRecorderPrintMonitoringUDPNetInfoStatusIni( struct Recorder_s *recorder,
													char *b, size_t bsize, int *client_number )
{
	int ret = 0;

	if( !recorder || !b || !bsize || !client_number )
	{
		DBGERR( "%s: Invalid argument\n", __func__ );
		return 0;
	}

	pthread_mutex_lock_my( &recorder->lock );

	if( recorder->recorder_print_ini_monitoring_net_info_status )
	{
		ret = recorder->recorder_print_ini_monitoring_net_info_status( b, bsize, client_number );
	}
	else
	{
		DBGERR( "%s: This recorder not support print this\n", __func__ );
	}

	pthread_mutex_unlock_my( &recorder->lock );

	return ret;

}

int AdapterRecorderPrintSettingsIni(struct Recorder_s *recorder,char *buff,size_t size)
{
	if( !recorder || !buff || size <= 0 )
	{
		DBGERR("%s: %i : Invalid Argument\n",__func__,__LINE__);
		return -1;
	}
	
	int rsize = 0;

	pthread_mutex_lock_my( &recorder->lock );

	if ( recorder->hen <= 0 )
	{
		DBGERR( "%s:Recorder Not inited\n", __func__ );
		pthread_mutex_unlock_my( &recorder->lock );
		return -1;
	}

	if ( recorder->recorder_print_ini_settings )
	{
		rsize = recorder->recorder_print_ini_settings( buff,
							       size,
							       &recorder->recorder_data);

		if ( rsize < 0 )
		{
			DBGERR( "%s:It wasn't possible to print recorder status\n", __func__ );
			pthread_mutex_unlock_my( &recorder->lock );
			return -1;
		}
	}

	pthread_mutex_unlock_my( &recorder->lock );

	return rsize;
	
}

int AdapterRecorderPrintAlarmIni(struct Recorder_s *recorder,char *buff,size_t size)
{
	if( !recorder || !buff || size <= 0 )
	{
		DBGERR("%s: %i : Invalid Argument\n",__func__,__LINE__);
		return 0;
	}

	int rsize = 0;

	pthread_mutex_lock_my( &recorder->lock );

	if ( recorder->hen <= 0 )
	{
		DBGERR( "%s:Recorder Not inited\n", __func__ );
		pthread_mutex_unlock_my( &recorder->lock );
		return 0;
	}

	if ( recorder->recorder_print_ini_alarm )
	{
		rsize = recorder->recorder_print_ini_alarm( buff,
							    size,
							    &recorder->recorder_data);

		if ( rsize < 0 )
		{
			DBGERR( "%s:It wasn't possible to print recorder status\n", __func__ );
			pthread_mutex_unlock_my( &recorder->lock );
			return 0;
		}
	}

	pthread_mutex_unlock_my( &recorder->lock );

	return rsize;

}

int AdapterRecorderPrintStatusIni( struct Recorder_s *recorder, char *buff, size_t size )
{
	if ( !recorder || !recorder->head|| !buff || size <= 0 )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	int rsize = 0;

	pthread_mutex_lock_my( &recorder->lock );

	if ( recorder->hen <= 0 )
	{
		DBGERR( "%s:Recorder Not inited\n", __func__ );
		pthread_mutex_unlock_my( &recorder->lock );
		return 0;
	}

	if ( recorder->recorder_print_ini_status )
	{
		rsize = recorder->recorder_print_ini_status( buff,
							     size,
							     &recorder->recorder_data,
							     recorder->datanotsave );

		if ( rsize < 0 )
		{
			DBGERR( "%s:It wasn't possible to print recorder status\n", __func__ );
			pthread_mutex_unlock_my( &recorder->lock );
			return 0;
		}
	}

	pthread_mutex_unlock_my( &recorder->lock );

	return rsize;
}

int AdapterRecorderPrintAccStatusIni( struct Recorder_s *recorder, char *buff, size_t size )
{
	if (  !recorder || !recorder->head|| !buff || size <= 0 )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	int rsize = 0;

	pthread_mutex_lock_my( &recorder->lock );

	if ( recorder->hen <= 0 )
	{
		DBGERR( "%s:Recorder Not inited\n", __func__ );
		pthread_mutex_unlock_my( &recorder->lock );
		return 0;
	}

	if ( recorder->recorder_print_ini_acc_status )
	{
		rsize = recorder->recorder_print_ini_acc_status( buff,
								 size,
								 &recorder->recorder_data );

		if ( rsize < 0 )
		{
			DBGERR( "%s:It wasn't possible to print recorder status\n", __func__ );
			pthread_mutex_unlock_my( &recorder->lock );
			return 0;
		}
	}

	pthread_mutex_unlock_my( &recorder->lock );

	return rsize;
}

int AdapterRecorderPrintAllRecordersStatusInFile( struct Recorder_s *head, int file )
{
	if ( !head )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	if ( head->recorderscount <= 0 )
	{
		DBGLOG( "%s:%i:No recorders nothing to print\n", __func__, __LINE__ );
		return 0;
	}

	struct Recorder_s *recorder;

	recorder = head->next;

	while ( recorder )
	{
		if ( AdapterRecorderPrintStatusInFile( recorder, file ) )
		{
			DBGERR( "%s:It wasn't possible to print current recorder in file\n", __func__ );
		}

		recorder = recorder->next;
	}

	return 0;


}

static const char* AdapterRecorderGetAlias( struct Recorder_s *recorder )
{
	const char *alias = NULL;

	if ( !recorder )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return NULL;
	}

	pthread_mutex_lock_my_pointer( &recorder->lock );

	if ( recorder->hen <= 0 )
	{
		DBGERR( "%s:Recorder Not inited\n", __func__ );
		pthread_mutex_unlock_my( &recorder->lock );
		return NULL;
	}

	if ( recorder->recorder_get_recorder_alias )
	{
		alias = recorder->recorder_get_recorder_alias( &recorder->recorder_data );

		if ( !alias)
		{
			//���� ��� ������ � �������� ������� �� �������� ����� ����������� ����
			if(AdapterGlobalGetStatus()->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature & DM_DM_RECORDER_NOT_RECORDER)
			{
				pthread_mutex_unlock_my( &recorder->lock );
				return AdapterGlobalGetStatus()->AdapterSettings.Block0.AdapterSettings.Alias;
			}
			DBGERR( "%s:It wasn't possible to print recorder status\n", __func__ );
			pthread_mutex_unlock_my( &recorder->lock );
			return NULL;
		}
	}

	pthread_mutex_unlock_my( &recorder->lock );

	return alias;
}

static const char *AdapterRecorderGetVersionDate( struct Recorder_s *recorder )
{
	const char *versdate = NULL;

	if ( !recorder )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return NULL;
	}

	pthread_mutex_lock_my_pointer( &recorder->lock );

	if ( recorder->hen <= 0 )
	{
		DBGERR( "%s:Recorder Not inited\n", __func__ );
		pthread_mutex_unlock_my( &recorder->lock );
		return NULL;
	}

	if ( recorder->recorder_get_version_date)
	{
		versdate = recorder->recorder_get_version_date( &recorder->recorder_data );

		if ( versdate < 0 )
		{
			DBGERR( "%s:It wasn't possible to print recorder status\n", __func__ );
			pthread_mutex_unlock_my( &recorder->lock );
			return NULL;
		}
	}

	pthread_mutex_unlock_my( &recorder->lock );

	return versdate;
}

int AdapterRecorderSprintDeviceLists( char* b, size_t bsize, struct Recorder_s *head, int count )
{
	if ( !b || !head || bsize <= 0 )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return 0;
	}

	if ( !head->recorderscount )
	{
		DBGLOG( "%s: No recorders to print\n", __func__ );
		return 0;
	}

	int cc = 0;

	int lcc = 0;

	const char *name = NULL;

	size_t size = bsize;

	struct Recorder_s *recorder;

	recorder = head->next;

	int i = count;


	if(!(AdapterGlobalGetStatus()->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature & DM_DM_WORK_SEPARATE))
	{
	
		lcc = axis_sprintf_d( b + cc, size,  DISPATCHER_LIB_INTERFACE_INI_NAME_DEVICE_LIST_COUNT, head->recorderscount );//���� 1 �� �������� ��� ��� �������
		cc	   += lcc;
		size   -= lcc;

		if ( size <= 0 ) goto end;
	}

	while ( recorder )
	{
		
		lcc = axis_sprintf_shablon_016x64( b + cc,
						size,
		                                DISPATCHER_LIB_INTERFACE_INI_NAME_001_DEVICE_LIST_SN_MASK,
		                                i,
						recorder->sn );

		cc	   += lcc;

		size   -= lcc;

		if ( size <= 0 ) goto end;

		name = AdapterRecorderGetAlias( recorder );

// 		if(!name)
// 		{
// 			return
// 		}


		lcc = axis_sprintf_shablon_t( b + cc,
					      size,
		                              DISPATCHER_LIB_INTERFACE_INI_NAME_001_DEVICE_LIST_ALIAS_MASK,

		                              i, name);
		cc	   += lcc;

		size   -= lcc;

		if ( size <= 0 ) goto end;


		lcc = axis_sprintf_shablon_t( b + cc,
					      size,
		                              DISPATCHER_LIB_INTERFACE_INI_NAME_001_DEVICE_LIST_NAME_MASK,
		                              i,
					      DispatcherLibStringAdapterNameFromDevice( recorder->type ) );

		cc	   += lcc;

		size   -= lcc;

		if ( size <= 0 ) goto end;
		i++;

		recorder = recorder->next;
	}

end:

	b[ bsize - 1] = 0x00;
	return cc;
}

//�������, ������� �������� ������� ������ ������ info �� ���������� ����������
//����:
// b - ��������� �� ����� ���� ��������
// bsize - ������ ���������� ����� � ������
// recorder - ��������� �� ������� ����������
//�������:
// ���������� ���������� � ����� ����
// 0 - ���� ���������� �� ������������ ������ ����������
// ����� 0 � ������ ������
static int AdapterRecorderPrintInfoIni(char* b, size_t bsize, struct Recorder_s *recorder)
{

	if( !b || !recorder || bsize <=0 )
	{

		DBGERR("%s:%i: Invalid Argumentn\n",__func__,__LINE__);
		return -1;
	}

	int rsize = 0; //���� �� ������������ ������ 0

	pthread_mutex_lock_my( &recorder->lock );

	if ( recorder->hen <= 0 )
	{
		DBGERR( "%s:Recorder Not inited\n", __func__ );
		pthread_mutex_unlock_my( &recorder->lock );
		return -1;
	}

	if ( recorder->recorder_print_ini_info_status )
	{
		DBGLOG("%s: Try get [Info] from recorder\n", __func__ );
		rsize = recorder->recorder_print_ini_info_status( b,
								  bsize,
								  recorder->recorder_data.recorderstatus_ex );

		if ( rsize < 0 )
		{
			DBGERR( "%s: It wasn't possible to print recorder status\n", __func__ );
		}
	}

	pthread_mutex_unlock_my( &recorder->lock );

	return rsize;	

	
}

int AdapterRecorderSnGet( struct Recorder_s *recorder)
{
	if( !recorder )
	{
		DBGERR("%s:%i: Invalid Argumentn\n",__func__,__LINE__);
		return -1;
	}

	int ret = 0;

	pthread_mutex_lock_my( &recorder->lock );

	if ( recorder->hen <= 0 )
	{
		DBGERR( "%s:Recorder hendel not opened cannot work\n", __func__ );
		pthread_mutex_unlock_my( &recorder->lock );
		return -1;
	}

	if(AdapterGlobalGetStatus()->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature & DM_DM_RECORDER_NOT_RECORDER)
	{
		recorder->sn = strtoull(AdapterGlobalGetStatus()->CurrentNetwork.SN,NULL,16);
		DBGERR("SERIAl = %s  %"PRIx64"\n",AdapterGlobalGetStatus()->CurrentNetwork.SN,recorder->sn);
		pthread_mutex_unlock_my( &recorder->lock );

		return 0;
		
	}

	if ( recorder->recorder_get_sn )
	{
		ret = recorder->recorder_get_sn( recorder->hen,
						 &recorder->semaphore,
						 &recorder->sn,
						 &recorder->recorder_data ) ;

		if ( ret )
		{

			DBGERR( "%s:It wasn't possible to get status\n", __func__ );
			pthread_mutex_unlock_my( &recorder->lock );
			return -1;
		}

	}

	pthread_mutex_unlock_my( &recorder->lock );

	return ret;

}

//�������, ������� �������� ������ info ��� ���������� ����������
//����:
// b - ��������� �� ����� ���� ��������
// bsize - ������ ������� ������
// recorder - ��������� �� ��������� �������� ����������
// Build - ������ ������ ��������
// BuildTime - ����� ������ ��������
//�������:
// ���������� ������������ ����
// ����� 0 � � ����� ������
int AdapterRecorderSprintRecorderInfo( char* b, size_t bsize, struct Recorder_s *recorder,
									   const char *Build, time_t BuildTime )
{
	if ( !b || !recorder || bsize <= 0 )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	int cc = 0;
	int lcc = 0;
	size_t size = bsize;

	//������ �����, ���� � ��� ���������� ������������ ������ [info] �� ��� ����� ��������
	//���� ��� ��� ������
	lcc = AdapterRecorderPrintInfoIni( b, bsize, recorder );
	
	if( lcc < 0 )
	{
		DBGERR("%s: It wasn't possible to print status\n",__func__);
		return lcc;
	}

	else if( !lcc )
	{
		DBGLOG("%s: Recoder_lib not supported [Info]\n", __func__ );
		
		cc	   += lcc;
		size   -= lcc;
		if ( size <= 0 ) goto end;
		
		lcc = axis_sprintf_s( b + cc,
					size,
					DISPATCHER_LIB_INTERFACE_INI_SECTION_INFO );
	
		cc	   += lcc;
		size   -= lcc;

		if ( size <= 0 ) goto end;
		
		lcc = axis_sprintf_t( b + cc,
					size,
					DISPATCHER_LIB_INTERFACE_INI_NAME_INFO_DEVICE_NAME,
					DispatcherLibStringAdapterNameFromDevice( recorder->type ) );
	
		cc	   += lcc;
		size   -= lcc;
		if ( size <= 0 ) goto end;
							  
				//��� �������� �����
		lcc = axis_sprintf_x64(	b + cc,
					size,
					DISPATCHER_LIB_INTERFACE_INI_NAME_INFO_DEVICE_SN,
					recorder->sn );

		cc	   += lcc;
		size   -= lcc;
		if ( size <= 0 ) goto end;

		lcc = axis_sprintf_t( b + cc,
				      size,
				      DISPATCHER_LIB_INTERFACE_INI_NAME_INFO_DEVICE_ALIAS,
				      AdapterRecorderGetAlias( recorder ) );

		cc	   += lcc;
		size   -= lcc;
		if ( size <= 0 ) goto end;

		//��� ������ ���� ����� ���������� - �� ��� ������ ��������� � ��������� �� �� �����
		struct struct_data sd;
		char time_str[64];
		AdapterRecorderGetTimeFromStructNonBlock( recorder, &sd );
		system_time_print_ini( time_str, sizeof( time_str ), sd );
		lcc = axis_sprintf_t( b + cc,
						size,
						DISPATCHER_LIB_INTERFACE_INI_NAME_INFO_DEVICE_TIME,
						time_str );

		cc	   += lcc;
		size   -= lcc;
		if ( size <= 0 ) goto end;

		//��� ������
		lcc = axis_sprintf_d( b + cc,
				      size,
				      DISPATCHER_LIB_INTERFACE_INI_NAME_INFO_DSP_VERSION,
				      /* Status->HardwareManufactureVersion */ 1 );

		cc	   += lcc;
		size   -= lcc;
		if ( size <= 0 ) goto end;

		lcc = axis_sprintf_t( b + cc,
				      size,
				      DISPATCHER_LIB_INTERFACE_INI_NAME_INFO_DSP_VERSION_DATE,
				      AdapterRecorderGetVersionDate(recorder));
		
		DBGLOG("%s:%i:Status info printed \n",__func__,__LINE__);
	}

	cc	   += lcc;
	size   -= lcc;
	if ( size <= 0 ) goto end;

	//������ ����� ����������� ����������
	lcc = axis_sprintf_d64(
			b + cc,
			size,
			DISPATCHER_LIB_INTERFACE_INI_NAME_INFO_DEVICE_UP_TIME,
			time(NULL) - recorder->connecttime );

	cc	   += lcc;
	size   -= lcc;
	if ( size <= 0 ) goto end;

	//������
	lcc = axis_sprintf_t(
			b + cc,
			size,
			DISPATCHER_LIB_INTERFACE_INI_NAME_INFO_DEVICE_VERSION,
			Build );

	cc	   += lcc;
	size   -= lcc;
	if ( size <= 0 ) goto end;

	//����� ������
	struct_data ct = system_time_translate( BuildTime );
	char time_str[24];

	system_time_print_ini( time_str, sizeof( time_str ), ct );

	lcc = axis_sprintf_t(
			b + cc,
			size,
			DISPATCHER_LIB_INTERFACE_INI_NAME_INFO_DEVICE_VERSION_DATE,
			time_str );

	cc	   += lcc;
	size   -= lcc;
	if ( size <= 0 ) goto end;

end:

	b[ bsize - 1] = 0x00;
	return cc;
}

//TODO - ��� ������� � ������� ������ �� ��� �� ������������
int AdapterRecorderSprintFirstRecorderInfo( char* b, size_t bsize, struct Recorder_s *head, const char *Build, time_t BuildTime )
{
	if ( !b || !head || bsize <= 0 )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	if ( !head->recorderscount && !head->next)
	{
		DBGERR( "%s:No recorders to print\n", __func__ );
		return 0;
	}

	struct Recorder_s *recorder;

	//����� ��������� ����� ��� ��� � �� ����� ��������
	if(head->type == RECORDER_HEADER_TYPE_ID)
	{
		recorder = head->next;
	}
	else
	{
		recorder = head;
	}

	return AdapterRecorderSprintRecorderInfo( b, bsize, recorder, Build, BuildTime );
}


/**
 * @brief ������� ��������� �����������
 *
 * @param Requests - ��������� �� ������ ��������
 * @param RCount - ��������� �� ���������� �������� � �������
 * @param Status - ��������� �� ���������� ��������� ������� ��������
 * @param recorder - ��������� �� ����������, ������� ������ ���������� ����������
 * @param ClientIP - IP ����� �������, ������� ������������� ����������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 */
int AdapterRecorderStopMonitoring( axis_httpi_request *Requests, size_t *RCount,
								   AdapterGlobalStatusStruct_t *Status,
								   struct Recorder_s *recorder, uint32_t ClientIP )
{
	int ret = 0;
	uint32_t ClientID = 0;

	if( !recorder || !Requests || !Status || !RCount ||*RCount <=0 )
	{
		DBGERR("%s: %i : Invalid Argument\n",__func__,__LINE__);
		return -1;
	}
	pthread_mutex_lock_my( &recorder->lock );
	
	if ( recorder->hen <= 0 )
	{
		DBGERR( "%s:Recorder Not inited\n", __func__ );
		pthread_mutex_unlock_my( &recorder->lock );
		return -1;
	}

	//������� �� ������� ClientID
	char *ClientIDc = axis_httpi_get_caserequest_value( Requests,
							    *RCount,
							    AXIS_HTTPI_CONTENT_CLIENT_ID );
	
	
	if( ClientIDc )
	{
		ClientID = (unsigned int)atoll( ClientIDc );
	}
	else
	{
		DBGERR("%s: Can't found ClientID from requests\n", __func__ );
	}
	
	DispatcherLibMonitoringStatusStruct_t MonitoringStatus;
	
	memset(&MonitoringStatus , 0 ,sizeof(DispatcherLibMonitoringStatusStruct_t));
	
	MonitoringStatus.ClientID = ClientID;
	MonitoringStatus.ClientIP = ClientIP;
	
	recorder->recorder_data.private_data = &MonitoringStatus;
	
	if(recorder->recorder_do_command)
	{
		ret = recorder->recorder_do_command(ADAPTER_COMMAND__RETURN_STOP_MONITORING,
						    Requests,
						    RCount,
						    &recorder->recorder_data,
						    NULL,
						    recorder->hen,
						    &recorder->semaphore);
		if( ret < 0)
		{
			AdapterGlobalGetStatus()->RecorderDiagnostic.requests_errored++;
			if(ret == -AXIS_ERROR_CANT_READ_DEVICE)
			{
				int iret = AdapterInterfaceSendCommand( AdapterGlobalGetStatus(), AXIS_INTERFACE_EVENT_READ_RECORDER_ERROR );

				if( iret < 0 )
				{
					DBGERR( "%s:%i: It wasn't possible to send event 'Recorder Error' to interface\n", __func__, __LINE__ );
				}

			}
			DBGERR("%s:%i: It wasn't possible to stop monitoring'}\n",__func__,__LINE__);
			
		}
		else
		{
			AdapterGlobalGetStatus()->RecorderDiagnostic.requests_good++;
		}
	}
	
	pthread_mutex_unlock_my( &recorder->lock );
	
	return ret;
	
}

int AdapterRecorderStartMonitoring(axis_httpi_request *Requests,
						size_t *RCount,
						AdapterGlobalStatusStruct_t *Status,
						struct Recorder_s *recorder)
{
	if( !recorder || !Requests || !Status || !RCount ||*RCount <=0 )
	{
		DBGERR("%s: %i : Invalid Argument\n",__func__,__LINE__);
		return -1;
	}
	
	unsigned int ClientIP;
	unsigned short ClientPort;
	unsigned int ClientID = 0x00000000;
	int ret = 0;
	DispatcherLibMonitoringStatusStruct_t MonitoringStatus;
	
	pthread_mutex_lock_my( &recorder->lock );

	if ( recorder->hen <= 0 )
	{
		DBGERR( "%s:Recorder Not inited\n", __func__ );
		pthread_mutex_unlock_my( &recorder->lock );
		return -1;
	}

	memset(&MonitoringStatus , 0 ,sizeof(DispatcherLibMonitoringStatusStruct_t));

	CurrentNetworkSettingsStruct_t* NetSettings = NULL;
	
	ClientIP = AdapterHttpdGetClientIP( axis_getpid(), Status,&NetSettings );
	
	if ( !NetSettings )
	{
		DBGERR( "%s: Net settings not inited\n", __func__ );
		pthread_mutex_unlock_my( &recorder->lock );
		return -1;
	}


	//������ ��� ����� ���� �����������
	ClientPort = Status->AdapterSettings.Block0.AdapterSettings.MonitoringPort;

	//�� ���� �� � ������ �������������� ��������
	if ( Status->flag_resque )
	{
		ClientPort = AXIS_SOFTADAPTER_MONITORING_PORT__DEFAULT;
	}
	
	//������� �� ������� ClientID
	char *ClientIDc = axis_httpi_get_caserequest_value(
	                          Requests,
	                          *RCount,
	                          AXIS_HTTPI_CONTENT_CLIENT_ID );


	if ( ClientIDc )
	{
		ClientID = ( unsigned int )atoll( ClientIDc );
	}
	else
	{
		DBGERR( "%s: Can't found ClientID from requests\n", __func__ );
	}

	char *AgentName = axis_httpi_get_caserequest_value(
	                          Requests,
	                         *RCount,
	                          AXIS_HTTPI_CONTENT_USER_TYPE );
	
	MonitoringStatus.ClientIP	= ClientIP;
	MonitoringStatus.ClientPort	= ClientPort;
	MonitoringStatus.ClientID	= ClientID;
	MonitoringStatus.ClientIDStop	= ClientID;
	MonitoringStatus.ClientMode	= NetSettings->ClientMode;
	MonitoringStatus.ledcontrol	= &AdapterLedUpMonControl;
	
	if( !MonitoringStatus.ClientMode )
		MonitoringStatus.ServerPort = NetSettings->MonitoringPort;
	else
		MonitoringStatus.ServerPort	= 0;
	
	MonitoringStatus.SN = recorder->sn;
	
	if( AgentName )
	{
		strncpy( MonitoringStatus.AgentName,
					AgentName,
					sizeof( MonitoringStatus.AgentName ));

		MonitoringStatus.AgentName[ sizeof( MonitoringStatus.AgentName ) - 1 ] = 0x00;
	}
	else
	{
		MonitoringStatus.AgentName[0] = 0x00;
	}

	recorder->recorder_data.private_data = &MonitoringStatus;

	DBGLOG( "%s: HEYIP=%x ClienMode=%i\n", __func__, MonitoringStatus.ClientIP, NetSettings->ClientMode );

	if(recorder->recorder_do_command)
	{
		ret = recorder->recorder_do_command(ADAPTER_COMMAND__RETURN_START_MONITORING,
						    Requests,
						    RCount,
						    &recorder->recorder_data,
						    NULL,
						    recorder->hen,
						    &recorder->semaphore);
		
		if( ret < 0)
		{
			AdapterGlobalGetStatus()->RecorderDiagnostic.requests_errored++;

			if(ret == -AXIS_ERROR_CANT_READ_DEVICE)
			{
				int iret = AdapterInterfaceSendCommand( AdapterGlobalGetStatus(), AXIS_INTERFACE_EVENT_READ_RECORDER_ERROR );

				if( iret < 0 )
				{
					DBGERR( "%s:%i: It wasn't possible to send event 'Recorder Error' to interface\n", __func__, __LINE__ );
				}

			}
			DBGERR("%s:%i: It wasn't possible to start monitoring'\n",__func__,__LINE__);
			
		}
		else
		{
			AdapterGlobalGetStatus()->RecorderDiagnostic.requests_good++;
		}
	}
	
	pthread_mutex_unlock_my( &recorder->lock );
	
	return ret;

}

/**
 * @brief �������, ������� ���������� � ���������� ��������� ������� ����� ����� ������� RecorderLibCommandDo �� ���������� ����������
 *
 * @param Requests - ��������� �� ������ ������� ��������
 * @param RCount - ��������� �� ���������� � ����������� ��������
 * @param Status - ��������� �� ���������� ������ ��������
 * @param recorder - ��������� �� ��������� ����������
 * @param Data - ��������� �� ������ ����������
 * @param Command - ��� ������� ��� �������� � �������
 * @param sCommand - ��������� �� ������ � ��������� ������� ��� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 */
int AdapterRecorderDoSameCommand( axis_httpi_request *Requests, size_t *RCount,
									AdapterGlobalStatusStruct_t *Status, struct Recorder_s *recorder,
									recorder_data_t *Data, uint32_t Command, const char *sCommand )
{
	if( !recorder )
	{
		DBGERR("%s: %i : Invalid Argument\n", __func__, __LINE__);
		return -1;
	}

	int ret = 0;

	pthread_mutex_lock_my( &recorder->lock );

	if( recorder->hen <= 0 )
	{
		DBGERR( "%s:Recorder Not inited\n", __func__ );
		pthread_mutex_unlock_my( &recorder->lock );
		return -1;
	}

	if( recorder->recorder_do_command )
	{
		ret = recorder->recorder_do_command( Command, Requests, RCount,
											Data, NULL,
											recorder->hen, &recorder->semaphore );
		if( ret < 0)
		{
			AdapterGlobalGetStatus()->RecorderDiagnostic.requests_errored++;

			if( ret == -AXIS_ERROR_CANT_READ_DEVICE )
			{
				int iret = AdapterInterfaceSendCommand( AdapterGlobalGetStatus(), AXIS_INTERFACE_EVENT_READ_RECORDER_ERROR );

				if( iret < 0 )
				{
					DBGERR( "%s:%i: It wasn't possible to send event 'Recorder Error' to interface\n", __func__, __LINE__ );
				}

			}

			DBGERR("%s:%i: It wasn't possible to %s\n",__func__,__LINE__, sCommand );
		}
		else
		{
			AdapterGlobalGetStatus()->RecorderDiagnostic.requests_good++;
		}
	}

	pthread_mutex_unlock_my( &recorder->lock );

	return ret;
}

/**
 * @brief ������� ������� ���������� ������� ����������
 *
 * @param Status - ��������� �� ���������� ��������� ������� ��������
 * @param recorder - ��������� �� ��������� ����������� ����������
 * @param cmd - ��� �������, ������� �������� ����������
 * @param Data - ��������� �� ����� ��� ������ ���������� �� ���������� � recorder->recorder_data.recordertempdata, ����� ���� NULL
 * @param size - ������ ������ ��� ������
 * @return int
 **/
int AdapterRecorderSendCommand( AdapterGlobalStatusStruct_t *Status,
								struct Recorder_s *recorder,
								uint32_t cmd,
								uint8_t *Data,
								size_t size )
{
	int ret = AdapterRecorderDoSameCommand( NULL, NULL, Status, recorder, &recorder->recorder_data, cmd, "send command" );

	if( ret < 0 )
	{
		return ret;
	}
	else
	{
		if( Data && size > 0 )
		{
			memcpy( Data, recorder->recorder_data.recordertempdata, size );
		}
	}
	return ret;
}

/**
 * @brief �������, ������� �������� ����� ��������� ���������� ��� ����������� ����������
 *
 * @param Recorder - ��������� �� ��������� ���������� � �������� ����������� ������
 * @return int - ����� ��������� ����������, ����� 0 ���� ������ ��� ������� ���
 */
static int AdapterRecorderGetInterfaceSleepTime( struct Recorder_s *Recorder )
{
	if( !Recorder )
	{
		DBGERR( "%s: Invalid argument\n", __func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	if( !Recorder->recorder_data.recordersettings_ex )
	{
		DBGERR( "%s: This recorder not have a settings\n", __func__ );
		return -AXIS_ERROR_INVALID_VALUE;
	}

	if( Recorder->recorder_get_interface_sleep_time )
	{
		return Recorder->recorder_get_interface_sleep_time( &Recorder->recorder_data );
	}

	DBGERR( "%s: Recorder not have function for this\n", __func__ );

	//�� ���������� ������, �� ��� ����
	return -AXIS_ERROR_NOT_INIT;
}

int AdapterRecorderSaveFromRequest(axis_httpi_request *Requests,size_t *RCount,
				   AdapterGlobalStatusStruct_t *Status,struct Recorder_s *recorder)
{
	int ret = AdapterRecorderDoSameCommand( Requests, RCount, Status, recorder, &recorder->recorder_data, ADAPTER_COMMAND__RETURN_SAVE, "save from request" );

	if( ret < 0 )
	{
		return ret;
	}

	//� ����� � ������� ���������� ������ ��� ������� �������� ����� �����
	//�� ������ ���������������� ����� ���������� ����������, � �� ����� � ���� ������� ��������� ���������
	//����� �� ����� ��������� ���, � ��� ��������� ����������� � ����� ������� �� ������� �����, ��� ���
	//�� ������ ������� ��� ��������� ������ ��� ��, ��������, ���������.
	if( adapter_recorder_sync_time_from_rec( &AdapterGlobalGetStatus()->GlobalDevice_t, recorder, AdapterGlobalGetStatus() ) < 0 )
	{
		DBGERR( "%s: It wasn't possible to sync recorder time with adapter\n", __func__ );
	}

	//� ����� ������� � �������� � � ������� �������� ����� ���������� ������� - ���� �� ��� ��������
	if( Status->GlobalDevice_t.dmdev_interface.dmctrl_ipresent == DM_DEVICE_PRESENT )
	{
		Status->InterfaceSleep = AdapterRecorderGetInterfaceSleepTime( recorder );
	}

	return ret;
}

int AdapterRecorderSetDiagnosticData(AdapterGlobalStatusStruct_t *Status,
						struct Recorder_s *recorder)
{
	if( !recorder ||   !Status  )
	{
		DBGERR("%s: %i : Invalid Argument\n",__func__,__LINE__);
		return -1;
	}

	int ret = 0;

	pthread_mutex_lock_my( &recorder->lock );

	if ( recorder->hen <= 0 )
	{
		DBGERR( "%s:Recorder Not inited\n", __func__ );
		pthread_mutex_unlock_my( &recorder->lock );
		return -1;
	}

	if(recorder->recorder_set_diagnostic)
	{
		ret = recorder->recorder_set_diagnostic((void*)&Status->RecorderDiagnostic);
		if( ret < 0)
		{
			DBGERR("%s:%i: It wasn't possible to set diagnostic\n",__func__,__LINE__);
		}

	}

	pthread_mutex_unlock_my( &recorder->lock );

	return ret;
}

int AdapterRecorderGetDiagnosticData(AdapterGlobalStatusStruct_t *Status,
						struct Recorder_s *recorder)
{
	if( !recorder ||   !Status  )
	{
		DBGERR("%s: %i : Invalid Argument\n",__func__,__LINE__);
		return -1;
	}

	int ret = 0;

	pthread_mutex_lock_my( &recorder->lock );

	if ( recorder->hen <= 0 )
	{
		DBGERR( "%s:Recorder Not inited\n", __func__ );
		pthread_mutex_unlock_my( &recorder->lock );
		return -1;
	}

	if(recorder->recorder_get_diagnostic)
	{
		ret = recorder->recorder_get_diagnostic((void*)&Status->RecorderDiagnostic);
		if( ret < 0)
		{
			DBGERR("%s:%i: It wasn't possible to set diagnostic\n",__func__,__LINE__);
		}

	}

	pthread_mutex_unlock_my( &recorder->lock );

	return ret;
}

int AdapterRecorderPrintLogs(axis_ssl_wrapper_ex_sock *sock,
			     struct Recorder_s *recorder,   unsigned int *tCount)
{
	int ret = 0;

	pthread_mutex_lock_my( &recorder->lock );

	if ( recorder->hen <= 0 )
	{
		DBGERR( "%s:Recorder Not inited\n", __func__ );
		pthread_mutex_unlock_my( &recorder->lock );
		return -1;
	}

	//TODO - ����� ����� ���� �������, � �������� ���������� �� ���������!!!

	if(recorder->recorder_print_logs)
	{
		ret = recorder->recorder_print_logs(sock, &recorder->recorder_data, recorder->hen,&recorder->semaphore,tCount);
	}

	else
	{
		DBGERR( "%s: Recorder not supported print logs at function\n", __func__ );
		ret = -1;
	}

	pthread_mutex_unlock_my( &recorder->lock );

	return ret;
}
int AdapterRecorderUploadStart(axis_ssl_wrapper_ex_sock *sock,
			       axis_ssl_download_request *UploadRequest,
			       struct Recorder_s *recorder,
			       unsigned int *tCount)
{
	int ret;

	if( !sock || !UploadRequest || !recorder )
	{
		DBGERR("%s:%i: Invalid Argumentn\n",__func__,__LINE__);
		return -1;
	}

#if 1
    printf("============ %s: sock=%p UploadRequest=%p recorder=%p \n", __func__, sock, UploadRequest, recorder);


#endif

	//������� �������� ������ ��� ��� ��� �� ���������
	if(recorder->recorder_upload)
	{
		ret = recorder->recorder_upload(sock,
						 &recorder->recorder_data,
						 recorder->hen,
						 &recorder->semaphore,
						 UploadRequest,
						 &recorder->lock,
						 tCount,
						 &AdapterLedUpMonControl);
		if( ret < 0 )
		{
			AdapterGlobalGetStatus()->RecorderDiagnostic.requests_errored++;
		}
		else
		{
			AdapterGlobalGetStatus()->RecorderDiagnostic.requests_good++;
		}
		return ret;
	}

	return 0;
}
int AdapterRecorderHttpMonitoringStart(axis_ssl_wrapper_ex_sock *sock,
				       axis_ssl_download_request *UploadRequest,
				       struct Recorder_s *recorder,
				       unsigned int *tCount,
				       AdapterGlobalStatusStruct_t *Status,
				       axis_httpi_request *Requests,
				       int Count)
{
	int ret;
	if( !sock || !UploadRequest || !recorder )
	{
		DBGERR("%s:%i: Invalid Argumentn\n",__func__,__LINE__);
		return -1;
	}
	unsigned int ClientIP;
	unsigned short ClientPort;
	unsigned int ClientID = 0x00000000;
// 	int ret = 0;
	DispatcherLibMonitoringStatusStruct_t MonitoringStatus;

	if ( recorder->hen <= 0 )
	{
		DBGERR( "%s:Recorder Not inited\n", __func__ );
		return -1;
	}

	memset(&MonitoringStatus , 0 ,
	       sizeof(DispatcherLibMonitoringStatusStruct_t));

	ClientIP = AdapterHttpdGetClientIP( axis_getpid(), Status, NULL );


	//������ ��� ����� ���� �����������
	ClientPort = Status->AdapterSettings.Block0.AdapterSettings.MonitoringPort;

	//�� ���� �� � ������ �������������� ��������
	if ( Status->flag_resque )
	{
		ClientPort = AXIS_SOFTADAPTER_MONITORING_PORT__DEFAULT;
	}

	//������� �� ������� ClientID
	char *ClientIDc = axis_httpi_get_caserequest_value( Requests,
							    Count,
							    AXIS_HTTPI_CONTENT_CLIENT_ID );


	if ( ClientIDc )
	{
		ClientID = ( unsigned int )atoll( ClientIDc );
	}
	else
	{
		DBGERR( "%s: Can't found ClientID from requests\n", __func__ );
	}


	MonitoringStatus.ClientIP	= ClientIP;
	MonitoringStatus.ClientPort	= ClientPort;
	MonitoringStatus.ClientID	= ClientID;
	MonitoringStatus.ClientIDStop	= ClientID;
	MonitoringStatus.ClientMode	= Status->CurrentNetwork.ClientMode;
	MonitoringStatus.ledcontrol	= &AdapterLedUpMonControl;

	if( !MonitoringStatus.ClientMode )
		MonitoringStatus.ServerPort	= Status->CurrentNetwork.MonitoringPort;
	else
		MonitoringStatus.ServerPort	= 0;

	MonitoringStatus.SN = recorder->sn;

	recorder->recorder_data.private_data = &MonitoringStatus;
	
	//������� �������� ������ ��� ��� ��� �� ���������
	if(recorder->recorder_http_monitor)
	{
		ret = recorder->recorder_http_monitor( sock,
						       &recorder->recorder_data,
						       recorder->hen,
						       &recorder->semaphore,
						       UploadRequest,
						       &recorder->lock,
						       tCount,
						       &AdapterLedUpMonControl);
		if( ret < 0 )
		{
			Status->RecorderDiagnostic.requests_errored++;
		}
		else
		{
			Status->RecorderDiagnostic.requests_good++;
		}
		return ret;
	}

	return 0;
}
int AdapterRecorderGetStatusThreads(struct Recorder_s *recorder)
{
	if( !recorder )
	{
		DBGERR("%s:%i: Invalid Argumentn\n",__func__,__LINE__);
		return -1;
	}

	int ret = 0;

	pthread_mutex_lock_my( &recorder->lock );

	if ( recorder->hen <= 0 )
	{
		DBGERR( "%s:Recorder Not inited\n", __func__ );
		pthread_mutex_unlock_my( &recorder->lock );
		return -1;
	}

	if(recorder->recorder_get_status_threads)
	{
		ret = recorder->recorder_get_status_threads();
		if( ret < 0)
		{
			
			if(ret == -AXIS_ERROR_CANT_READ_DEVICE)
			{
				int iret = AdapterInterfaceSendCommand( AdapterGlobalGetStatus(), AXIS_INTERFACE_EVENT_READ_RECORDER_ERROR );

				if( iret < 0 )
				{
					DBGERR( "%s:%i: It wasn't possible to send event 'Recorder Error' to interface\n", __func__, __LINE__ );
				}

			}
			DBGERR("%s:%i: It wasn't possible to get status threads\n",__func__,__LINE__);
		}
	}

	pthread_mutex_unlock_my( &recorder->lock );

	return ret;

	
}

int AdapterRecorderCheckWhile(struct Recorder_s *recorder)
{
	if( !recorder )
	{
		DBGERR("%s:%i: Invalid Argumentn\n",__func__,__LINE__);
		return -1;
	}

	int ret = 0;

	pthread_mutex_lock_my( &recorder->lock );

	if ( recorder->hen <= 0 )
	{
		DBGERR( "%s:Recorder Not inited\n", __func__ );
		pthread_mutex_unlock_my( &recorder->lock );
		return -1;
	}

	if(recorder->recorder_check_while)
	{
		ret = recorder->recorder_check_while(&recorder->recorder_data);
		if( ret < 0)
		{
			DBGERR("%s:%i: It wasn't possible to check while\n",__func__,__LINE__);
		}
	}

	pthread_mutex_unlock_my( &recorder->lock );

	return ret;
}

int AdapterRecorderCheckAllRecordersWhile( struct Recorder_s *head)
{
	if( !head )
	{
		DBGERR("%s:%i: Invalid Argumentn\n",__func__,__LINE__);
		return -1;
	}

	if ( head->type != RECORDER_HEADER_TYPE_ID )
	{
		DBGERR( "%s:Not head structs\n", __func__ );
		return -1;
	}

	if ( head->recorderscount <= 0 )
	{
		DBGLOG( "%s:%i:No recorders registered\n", __func__, __LINE__ );
		return 0;
	}

	pthread_mutex_lock_my( &head->lock );

	struct Recorder_s *recorder;

	recorder = head->next;

	while ( recorder )
	{

		if(AdapterRecorderCheckWhile(recorder))
		{
			DBGERR("%s:It wasn't possible to check while recorder\n",__func__);
			pthread_mutex_unlock_my( &head->lock );
			return -1;
		}

		recorder = recorder->next;

	}

	pthread_mutex_unlock_my( &head->lock );

	return 0;
}

int AdapterRecorderSyncTime(struct Recorder_s *recorder)
{
	if( !recorder )
	{
		DBGERR("%s:%i: Invalid Argumentn\n",__func__,__LINE__);
		return -1;
	}

	int ret = 0;

	pthread_mutex_lock_my( &recorder->lock );

	if ( recorder->hen <= 0 )
	{
		DBGERR( "%s:Recorder Not inited\n", __func__ );
		pthread_mutex_unlock_my( &recorder->lock );
		return -1;
	}

	if(recorder->recorder_sync_time)
	{
		
		ret = recorder->recorder_sync_time(recorder->hen, &recorder->semaphore);
		if( ret < 0)
		{
			DBGERR("%s:%i: It wasn't possible to get status threads\n",__func__,__LINE__);
			AdapterGlobalGetStatus()->RecorderDiagnostic.requests_errored++;
		}
		else
		{
			AdapterGlobalGetStatus()->RecorderDiagnostic.requests_good++;
		}
	}

	pthread_mutex_unlock_my( &recorder->lock );

	return ret;
}

int AdapterRecorderSyncAllRecordersTime(struct Recorder_s *head)
{
	if( !head )
	{
		DBGERR("%s:%i: Invalid Argumentn\n",__func__,__LINE__);
		return -1;
	}
	
	if ( head->type != RECORDER_HEADER_TYPE_ID )
	{
		DBGERR( "%s:Not head structs\n", __func__ );
		return -1;
	}

	if ( head->recorderscount <= 0 )
	{
		DBGLOG( "%s:%i:No recorders registered\n", __func__, __LINE__ );
		return 0;
	}

	pthread_mutex_lock_my( &head->lock );

	struct Recorder_s *recorder;

	recorder = head->next;

	while ( recorder )
	{

		if(AdapterRecorderSyncTime(recorder))
		{
			DBGERR("%s:It wasn't possible to sync time\n",__func__);
			pthread_mutex_unlock_my( &head->lock );
			return -1;
		}
		
		recorder = recorder->next;
		
	}

	pthread_mutex_unlock_my( &head->lock );

	return 0;
	
}


int AdapterRecorderGetTimeFromStructNonBlock( struct Recorder_s *recorder,struct_data *date)
{
	if( !recorder || !date )
	{
		DBGERR("%s:%i: Invalid Argumentn\n",__func__,__LINE__);
		return -1;
	}

	if(recorder->recorder_get_time_from_struct)
	{
		return recorder->recorder_get_time_from_struct(recorder->recorder_data.recordersettings_ex,
									date);
	}

	return 0;
}

int AdapterRecorderCheckIfSomeOneUseNet(struct Recorder_s *head)
{
	if( !head )
	{
		DBGERR("%s:%i: Invalid Argumentn\n",__func__,__LINE__);
		return -1;
	}

		if ( head->type != RECORDER_HEADER_TYPE_ID )
	{
		DBGERR( "%s:Not head structs\n", __func__ );
		return -1;
	}

	if ( head->recorderscount <= 0 )
	{
		DBGLOG( "%s:%i:No recorders registered\n", __func__, __LINE__ );
		return 0;
	}

	pthread_mutex_lock_my( &head->lock );

	struct Recorder_s *recorder;

	recorder = head->next;

	int status = 0;
	int ret = 0;

	while ( recorder )
	{
		status = AdapterRecorderGetStatusThreads(recorder);
		if(status < 0)
		{
			DBGERR("%s:It wasn't possible to get status thread\n",__func__);
			pthread_mutex_unlock_my( &head->lock );
			return -1;
		}
		if( status & DM_RECORDER_MONITORING_NOW ||
				status & DM_RECORDER_UPLOADING_NOW )
		{
			ret++;
		}

		recorder = recorder->next;

	}

	pthread_mutex_unlock_my( &head->lock );

	return ret;
	
}

int AdapterRecorderPrintMoreInfo(char* b, size_t bsize, struct Recorder_s *recorder )
{
	if( !b || bsize <=0 || !recorder  )
	{
		DBGERR("%s:%i: Invalid Argumentn\n",__func__,__LINE__);
		return -1;
	}

	int ret = 0;

	pthread_mutex_lock_my( &recorder->lock );

	if ( recorder->hen <= 0 )
	{
		DBGERR( "%s:Recorder Not inited\n", __func__ );
		pthread_mutex_unlock_my( &recorder->lock );
		return -1;
	}

	recorder->recorder_data.private_data = &recorder->semaphore;

	if(recorder->recorder_print_more_info)
	{
		ret = recorder->recorder_print_more_info( b,
							  bsize,
							  &recorder->recorder_data,
							  recorder->hen);
		if( ret < 0)
		{
			DBGERR("%s:%i: It wasn't possible to print more info\n",__func__,__LINE__);
		}
	}

	pthread_mutex_unlock_my( &recorder->lock );

	return ret;
	
}

int AdapterRecorderGetVoltageAndTempInfo(uint16_t *voltage,uint16_t *temp,short* ac,struct Recorder_s *recorder)
{
	int ret = 0;

	pthread_mutex_lock_my( &recorder->lock );

	if ( recorder->hen <= 0 )
	{
		DBGERR( "%s:Recorder Not inited\n", __func__ );
		pthread_mutex_unlock_my( &recorder->lock );
		return -1;
	}

	

	recorder->recorder_data.private_data = &recorder->semaphore;

	if(recorder->recorder_get_voltage_temp)
	{
		ret = recorder->recorder_get_voltage_temp(voltage,temp,ac,&recorder->recorder_data,recorder->hen);
		if( ret < 0)
		{
			DBGERR("%s:%i: It wasn't possible to print more info\n",__func__,__LINE__);
			goto end;
		}

	}
	else
	{
		ret = -1;
	}
end:
	pthread_mutex_unlock_my( &recorder->lock );

	return ret;
}

int AdapterRecorderConvertAcctoStandartStructUnlocked(struct Recorder_s *recorder, SoftRecorderGetAccStateStruct_t* acc )
{
	if( !recorder || !acc )
	{
		DBGERR("%s:%i: Invalid Argumentn\n",__func__,__LINE__);
		return -1;
	}

	int ret = 0;


	if ( recorder->hen <= 0 )
	{
		DBGERR( "%s:Recorder Not inited\n", __func__ );
		
		return -1;
	}



	recorder->recorder_data.private_data = &recorder->semaphore;

	if(recorder->recorder_convert_acc_to_standart)
	{
		ret = recorder->recorder_convert_acc_to_standart(acc,&recorder->recorder_data,recorder->hen);
		if( ret < 0)
		{
			DBGERR("%s:%i: It wasn't possible convert acc to standart struct\n",__func__,__LINE__);
			goto end;
		}

	}
end:
	

	return ret;

}

int AdapterRecorderConvertAcctoStandartStruct(struct Recorder_s *recorder, SoftRecorderGetAccStateStruct_t* acc )
{
	if( !recorder || !acc )
	{
		DBGERR("%s:%i: Invalid Argumentn\n",__func__,__LINE__);
		return -1;
	}

	int ret = 0;

	pthread_mutex_lock_my( &recorder->lock );

	if ( recorder->hen <= 0 )
	{
		DBGERR( "%s:Recorder Not inited\n", __func__ );
		pthread_mutex_unlock_my( &recorder->lock );
		return -1;
	}



	recorder->recorder_data.private_data = &recorder->semaphore;

	if(recorder->recorder_convert_acc_to_standart)
	{
		ret = recorder->recorder_convert_acc_to_standart(acc,&recorder->recorder_data,recorder->hen);
		if( ret < 0)
		{
			DBGERR("%s:%i: It wasn't possible to print more info\n",__func__,__LINE__);
			goto end;
		}

	}
end:
	pthread_mutex_unlock_my( &recorder->lock );

	return ret;
	
}

/**
 * @brief �������, ������� �������� ������� � ���������� ��� ���������� ������������, ���� ��������
 *
 * @param Recorder - ��������� �� ��������� ����������
 * @param Color - ��������� �� ������ � ������
 * @param Command - ������� ��� ����������
 * @return int -  0 ��� �� (���� ���� ������� �� ��������), ����� 0 � ������ ������
 **/
int AdapterRecorderLedWork( struct Recorder_s *Recorder, const char *Color, RecorderLibLedCommand_t Command )
{
	int ret = 0;

	if( !Recorder || !Color )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n",__func__,__LINE__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	DBGLOG( "%s: Recorder led command %u '%s'\n", __func__, Command, Color );

	pthread_mutex_lock_my( &Recorder->lock );

	if( Recorder->hen <= 0 )
	{
		DBGERR( "%s: Recorder Not inited\n", __func__ );
		pthread_mutex_unlock_my( &Recorder->lock );
		return -AXIS_ERROR_NOT_INIT;
	}

	Recorder->recorder_data.private_data = &Recorder->semaphore;

	if( Recorder->recorder_led_command )
	{
		ret = Recorder->recorder_led_command( Recorder->hen, &Recorder->semaphore, Color, Command );

		if( ret < 0 )
		{
			DBGERR( "%s:%i: It wasn't possible to Recorder led %s command %u\n",__func__,__LINE__, Color, Command );
			AdapterGlobalGetStatus()->RecorderDiagnostic.requests_errored++;
		}
		else
		{
			AdapterGlobalGetStatus()->RecorderDiagnostic.requests_good++;
		}

	}

	pthread_mutex_unlock_my( &Recorder->lock );

	return ret;
}

/**
 * @brief �������, ��������� ������� ��������� ������� ������������������� ����������
 *
 * @param head - ��������� �� ������� �����������
 * @return int - ����� ����������, ����� 0 � ������ ������ ��� ���� ���������� ���������
 */
int AdapterRecorderGetInterfaceSleepTimeFistRecorder( struct Recorder_s *head )
{
	struct Recorder_s *Recorder;

	if( !head )
	{
		DBGERR( "%s: Invalid argument\n", __func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	if( head->type != RECORDER_HEADER_TYPE_ID )
	{
		DBGERR( "%s:%i: Invalid Recorder type not head\n", __func__, __LINE__ );
		return -AXIS_ERROR_INVALID_VALUE;
	}


	if( head->recorderscount <= 0 )
	{
		DBGLOG( "%s:%i: No recorders inited\n", __func__, __LINE__ );
		return -AXIS_ERROR_NOT_INIT;
	}

	Recorder = head->next;

	return AdapterRecorderGetInterfaceSleepTime( Recorder );
}

/**
 * @brief ������� ������� ���������, � �� ��������� �� ������ ���������� � ������ ���� �� ������� ����,
 * @brief ��� ��� ������ ���������, � ������� ������� �� ������ ����
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @param dev - ��������� �� ��������� ������ ����������
 * @param Recorder - ��������� �� ��������� ����������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
static int AdapterRecorderCheckFullNand( AdapterGlobalStatusStruct_t *Status, dmdev_t *dev, struct Recorder_s *Recorder )
{

	//��� ������ ������:
	//1 - � �������� �������� ������� ������ ���� ����� ���������� � �� ����������
	//2 - ������ ��� ������ ������� ���������� �� �����, ���� ������� �� �� ����� ������ ������

	//��� ������ ������ - ��� �� �������� ������ � ����������
	int ret = 0;
	SoftRecorderStatusStruct_t StandartStatus;
	DispatcherLibEvent_t *Event = NULL;
	double percent = 0;
    
    
    printf("============== %s: Status=%p dev=%p Recorder=%p \n", __func__, Status, dev, Recorder );

	if( !Status || !dev || !Recorder )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	memset( &StandartStatus, 0, sizeof( SoftRecorderStatusStruct_t ) );

	//������������ ����� ����������� ������ ���������� � ����������� ���������
	pthread_mutex_lock_my( &Recorder->lock );

	if( Recorder->hen <= 0 )
	{
		DBGERR( "%s:Recorder hendel not opened cannot work\n", __func__ );
		pthread_mutex_unlock_my( &Recorder->lock );
		return -AXIS_ERROR_INVALID_VALUE;
	}

	if( Recorder->recorder_convert_status_to_standart_struc )
	{
		ret = Recorder->recorder_convert_status_to_standart_struc( &StandartStatus,
				Recorder->recorder_data.recorderstatus_ex );

		if( ret < 0 )
		{
			DBGERR( "%s:%i: Can't convert recorder status to standart struct\n", __func__, __LINE__ );
			pthread_mutex_unlock_my( &Recorder->lock );
			return ret;
		}
	}

	else
	{
		//������� �� ���� ���������� � ����������
		//��� ��������� ������ - ��� ��� ��� ���������� ������ ����, ��� ���������� ������������
		//������ �����������
		DBGERR( "%s:%i: Function 'RecorderLibConvertStatusToStandartStruct' not supported this recorder\n",
				__func__, __LINE__ );
		pthread_mutex_unlock_my( &Recorder->lock );
		return -AXIS_ERROR_INVALID_ANSED;
	}

	pthread_mutex_unlock_my( &Recorder->lock );

	if( StandartStatus.FlashStatus == fsFlashErasing || 
		StandartStatus.FlashStatus == fsFlashClearing )
	{
		DBGLOG( "%s: NAND is erasing now\n", __func__ );
		return 0; //���� ������� - ������ ����� �� �����
	} 

	//���������, ��� � ��� ��������� ����� ���������� �������� - ������ ��� ��������
	if( ( StandartStatus.FlashStatus != fsFlashReady 
			&& StandartStatus.FlashStatus != fsFlashUploading ) || !StandartStatus.FlashCapacity )
	{
		//���� � ������� ��� �� �� ��� ��� ��� �� ���� �� ������
		DBGERR( "%s:%i: Invalid status flash status=%x or no flash size=%"PRIu64"\n", __func__, __LINE__, StandartStatus.FlashStatus,
				StandartStatus.FlashCapacity );
		return 0;
	}

	//TODO - � ��� �������� ����� �������� - �� ������ ���������� SMS ����� 
	//������ ����� ��������� ����� ��� �� ��������� � ���������� ��������
	//� �� ������ ����� ��� � ��� ������!!!!! � ������ ��� ���� �� �������
	percent = StandartStatus.FlashUsed/StandartStatus.FlashCapacity * 100;

	//�� ������ ��������� �� ������ ����� �������������� ������� �� ����������, � ���� ��� �� ����������
	//�� �� ������� � ��� ������� ���������� ��� �� ���� �����
	Event = AdapterGlobalGetFirstPowerUpEvent( &Status->ExchangeEvents.Events );

	if( Event )
	{
		Event->Value2 = percent;
	}

	//���� � ���������� ���� ������� �� �������� ���������� SMS - �� �������� � ���
	if( StandartStatus.FlashProgress && 
		percent >= StandartStatus.FlashProgress )
	{
		//�� �������� ���������!!!
		DBGLOG( "%s: Try send event for NAND progress %0.2f (%u)\n", __func__,
											percent, StandartStatus.FlashProgress );
	}
	else
	{
		//� ������ �� ������� ��������� - ��������� ������ �� ����������

		//�������� � ��� ���� ������ ����������� == ������� ������ �����
		if( StandartStatus.Chanel1Status & AXIS_SOFTRECORDER_STATUS__RECORD_SOURCE ||
			StandartStatus.Chanel2Status & AXIS_SOFTRECORDER_STATUS__RECORD_SOURCE ||
			StandartStatus.Chanel3Status & AXIS_SOFTRECORDER_STATUS__RECORD_SOURCE ||
			StandartStatus.Chanel4Status & AXIS_SOFTRECORDER_STATUS__RECORD_SOURCE ||
			StandartStatus.Chanel5Status & AXIS_SOFTRECORDER_STATUS__RECORD_SOURCE ||
			StandartStatus.Chanel6Status & AXIS_SOFTRECORDER_STATUS__RECORD_SOURCE ||
			StandartStatus.Chanel7Status & AXIS_SOFTRECORDER_STATUS__RECORD_SOURCE ||
			StandartStatus.Chanel8Status & AXIS_SOFTRECORDER_STATUS__RECORD_SOURCE ||
			( StandartStatus.FlashCapacity != StandartStatus.FlashUsed ) )
		{
			DBGLOG( "%s:%i: Rcord now or flash not full\n", __func__, __LINE__ );
			return 0; //������ ��� �� ��� ������
		}
	}

	//����� ������� ����� - ���� ���������� SMS
	//���������, �� �������� - ���� �� ����� ������� ��� � ������
	//���� ���� - �� ������ �� ������
	//� ���� ��� - �� ������� ����� ������� - � ��� ��� ����� ���������� ���������
	//����������� ����������

	Event = AdapterGlobalGetFirstEventbyType( &Status->ExchangeEvents.Events,
										  DISPATCHER_LIB_EVENT__RECORDER_FULL_NAND,
										  0 ); //�� �� ��������� �������� value //TODO - ���������

	if( Event )
	{
		char time_buffer[ 64 ];
		system_time_print_ini( time_buffer, sizeof( time_buffer ), Event->Data );
		DBGLOG( "%s:%i: Full NAND event was sending at %s\n", __func__, __LINE__, time_buffer );
		//��� ���� ���� � ���� ������� �� ��������
		return 0;
	}

	ret = AdapterGlobalEventAddAndLog( Status, "Recorder haw a full NAND",
									   DISPATCHER_LIB_EVENT__RECORDER_FULL_NAND,
									   0, percent ); //�� ������ �������� ������� ������

	if( ret < 0 )
	{
		DBGERR( "%s:%i: Can't add new event to buffer\n", __func__, __LINE__ );
		return ret;
	}

	return 0;

}

/**
 * @brief ������� ���������, � �� ��������� �� ����� ������ �� ����������� � ������ ���� �� ������� ����,
 * @brief ��� ��� ������ ���� ���������
 *
 * @param Status - ��������� �� ���������� ��������� ��������
 * @param dev - ��������� �� ��������� ����������� ���� ����������
 * @param head - ��������� �� ������ ����������� ��������� � ������ ������
 * @return int - 0 ��� �� (���� �� ���� ������� �� ������ ����������)
 **/
int AdapterRecorderCheckFullAllRecorders( AdapterGlobalStatusStruct_t *Status, dmdev_t *dev, struct Recorder_s *head )
{

	struct Recorder_s *Recorder;
	int ret;

	if( !dev || !head || !Status )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	if( head->type != RECORDER_HEADER_TYPE_ID )
	{
		DBGERR( "%s:%i: Invalid Recorder type not head\n", __func__, __LINE__ );
		return -AXIS_ERROR_INVALID_VALUE;
	}


	if( head->recorderscount <= 0 )
	{
		DBGLOG( "%s:%i: No recorders inited\n", __func__, __LINE__ );
		return 0;
	}

	Recorder = head->next;

	//�� ���� ����������� ��� �� ����� ���� ��� �������� ������
	while( Recorder )
	{
	 	ret = AdapterRecorderCheckFullNand( Status, dev, Recorder );
		if( ret < 0 )
		{
			DBGERR( "%s: It wasn't possible to check recorder full nand\n", __func__ );
			return ret;
		}

		Recorder = Recorder->next;
	}

	return 0;
}

/**
 * @brief �������, ������� ���������� ������� ���� ������������������ � ������� �����������
 *
 * @param Status - ��������� �� ��������� ����������� ������� ����������
 * @param head - ��������� �� ������ ������ �����������
 * @param cmd - ����� ������� � ����������
 * @return int - 0 ��� ��, ����� 0 � ������ ������ (���� ��� ������ � ����� �� �����������)
 * @note - ������ �� ���������� �� ��� �� ����, ��� ��� �� �����.
 * @note - ������ ������ ���������� �� �������� � ������ �������� ������� ��� ������ �����������
 */
int AdapterRecorderSendCommandAllRecorders( AdapterGlobalStatusStruct_t *Status, struct Recorder_s *head, uint32_t cmd )
{
	struct Recorder_s *Recorder;
	int ret = 0;

	if( !head || !Status )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	if( head->type != RECORDER_HEADER_TYPE_ID )
	{
		DBGERR( "%s:%i: Invalid Recorder type not head\n", __func__, __LINE__ );
		return -AXIS_ERROR_INVALID_VALUE;
	}


	if( head->recorderscount <= 0 )
	{
		DBGLOG( "%s:%i: No recorders inited\n", __func__, __LINE__ );
		return 0;
	}

	Recorder = head->next;

	//�� ���� ����������� ��� �� ����� ���� ��� �������� ������
	while( Recorder )
	{
		int iret = AdapterRecorderSendCommand( Status, Recorder, cmd, NULL, 0 );

		if( iret < 0 )
		{
			DBGERR( "%s: It wasn't possible to send command to recorder\n", __func__ );
			ret = iret;
		}

		Recorder = Recorder->next;
	}

	return ret;
}

/**
 * @brief ������� ���������� ������� ������� �� ������ ����������� ��� ���� �����������, ������� ��� ������������
 * ��� ����, ������ ����� �������� ������� ���������� ������, ���� ��� ��������� � ��
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int
 */
int AdapterRecorderMonitoringSendAlarmAllRecorders( AdapterGlobalStatusStruct_t *Status )
{
	int ret;
	struct Recorder_s *Recorder;

	if( !Status )
	{
		DBGERR( "%s: Invalid argument\n", __func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	if( Status->Recorder.type != RECORDER_HEADER_TYPE_ID )
	{
		DBGERR( "%s:%i: Invalid Recorder type not head\n", __func__, __LINE__ );
		return -AXIS_ERROR_INVALID_VALUE;
	}


	if( Status->Recorder.recorderscount <= 0 )
	{
		DBGLOG( "%s:%i: No recorders inited\n", __func__, __LINE__ );
		return 0;
	}

	Recorder = Status->Recorder.next;

	//�� ���� ����������� ��� �� ����� ���� ��� �������� ������
	while( Recorder )
	{
		//���� ���������� ������������ ��, ��� �� ����� - �� ������ ����� ����
		if( Recorder->recorder_monitoring_send_alarm )
		{
			ret = Recorder->recorder_monitoring_send_alarm( );

			if( ret < 0 )
			{
				DBGERR( "%s: It wasn't possible to send alarm event from this recorder\n", __func__ );
			}
		}

		Recorder = Recorder->next;
	}

	return 0;
}

/**
 * @brief �������, ������� ���������� ����������� � USB ������ ����������
 *
 * @param Dev - ��������� �� ��������� ����������
 * @param power - ���� ���������: 1 - ��������, 0 - ���������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterRecorderPower( dmdev_t *Dev, int Power )
{
	int ret;
	int RecorderPort;

	if( !Dev )
	{
		DBGERR( "%s: Invalid argument\n", __func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	if( !Dev->dmdev_usb.dmctrl_icontrol )
	{
		DBGERR( "%s: USB device not controled\n", __func__ );
		return -AXIS_ERROR_NOT_INIT;
	}

	if( !Dev->dmdev_recorder.dmctrl_icontrol )
	{
		DBGERR( "%s: USB recorder device not controled\n", __func__ );
		return -AXIS_ERROR_NOT_INIT;
	}

	//��� �� ����� ��������/��������� ���������� - ��� ���� ����� ����� �����!!!
	//���� �� �������� �����������, �� ��� ���� ����������� �� �������
	//�� ���� �������� �����������, �� ��� ������ ���� ����� ����
	//����� ��� ����� ������� ���������� ���������� - ��� ��� ����� �� ����� ����� ��� ��� �� �����������

	if( !iDMXXXUsbGetRecorderPort( Dev ) )
	{
		DBGERR( "%s: Functions for get Recorder USB port not implementtion\n", __func__ );
		return -AXIS_ERROR_NOT_INIT;
	}

	RecorderPort = DMXXXUsbGetRecorderPort( Dev );

	if( RecorderPort < 0 )
	{
		DBGERR( "%s: Can't get USB Recorder port number from device library\n", __func__ );
		return RecorderPort;
	}

	if( !RecorderPort )
	{
		DBGERR( "%s: Device library not have USB Recorder port\n", __func__ );
		return -AXIS_ERROR_INVALID_VALUE;
	}

	DBGLOG( "%s: Try power %s Recorder on USB port %d\n", __func__, Power ? "on" : "off", RecorderPort );

	ret = AdapterPowerUSB( Dev, RecorderPort, Power );

	if( ret < 0 )
	{
		DBGERR( "%s: Can't power %s USB recorder at port %d\n", __func__, Power ? "on" : "off", RecorderPort );
	}

	return 0;
}

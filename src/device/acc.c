#include <stdio.h>
#include <stdlib.h>

#include <inttypes.h>

#include <axis_error.h>
#include <axis_dmlibs_resource.h>
#include <axis_acc.h>

#include <dmxxx_lib/dmxxx_calls.h>

#include <adapter/global.h>
#include <adapter/power.h>
#include <adapter/recorder.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif


/**
 * @brief �������, ������� ��������� ��������� ��� � ������� ����������� �������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
static int AdapterControlAccStatus( AdapterGlobalStatusStruct_t *Status )
{
	if( !Status )
	{
		DBGERR("%s:%i: Invalid Argument\n",__func__,__LINE__);
		return -1;
	}

	//��� � ����������� �� ����, ��� ��� �����
	
	//���� �� �� ������������ ���, �� � ������ ��� ������ - ������
	if( !Status->GlobalDevice_t.dmdev_acc.dmctrl_icontrol )
	{
	 	DBGERR( "%s: ACC not controled\n", __func__ );
		return -AXIS_ERROR_INVALID_VALUE;
	}
	
	//��������� ��� � ��� ���� �� � ���������� - ������ �� ������
	if ( !Status->GlobalDevice_t.dmdev_acc.dmctrl_ipresent &&
						(Status->GlobalDevice_t.dmdev_acc.dmctrl_feature &
						DM_ACC_RECORDER_CNTRL))
	{
		DBGLOG("%s:%i: Go out recorder ctrl\n",__func__,__LINE__);
		return 0;
	}

	DBGLOG("%s:%i: Check Acc Status\n",__func__,__LINE__);
	uint64_t laststatus = Status->GlobalDevice_t.dmdev_acc.dmctrl_status;
	int err;

	if( DMXXXACCControlStatus(&Status->GlobalDevice_t, &Status->ACCStatus))
	{
		DBGERR("%s:%i: It wasn't possible to get status control \n",__func__,__LINE__);
		return -1;
	}
	
	if( (Status->GlobalDevice_t.dmdev_acc.dmctrl_status & DM_ACC_STATUS_LOW_TEMP) && !(laststatus & DM_ACC_STATUS_LOW_TEMP ) )
	{
		
		err= AdapterGlobalEventAddAndLog( Status, "Low ACC Temperature detected",
						DISPATCHER_LIB_EVENT__TEMPERATURE,
						Status->ACCStatus.AccStatus.int_acc.acc_temperature,0 );
		if(err < 0)
		{
			DBGERR("%s:%i: It wasn't possible to add event'\n",__func__,__LINE__);
		}
	}
	if( (Status->GlobalDevice_t.dmdev_acc.dmctrl_status & DM_ACC_STATUS_HIGH_TEMP) && !(laststatus & DM_ACC_STATUS_HIGH_TEMP ) )
	{
		err= AdapterGlobalEventAddAndLog( Status, "High ACC Temperature detected",
						DISPATCHER_LIB_EVENT__TEMPERATURE,
						Status->ACCStatus.AccStatus.int_acc.acc_temperature,0 );
		if(err < 0)
		{
			DBGERR("%s:%i: It wasn't possible to add event'\n",__func__,__LINE__);
		}
	}
	if( (Status->GlobalDevice_t.dmdev_acc.dmctrl_status & DM_ACC_STATUS_INT_PERCENT_70) && !(laststatus & DM_ACC_STATUS_INT_PERCENT_70 ) )
	{
		err= AdapterGlobalEventAddAndLog( Status, "ACC Capasity 70\%",
						DISPATCHER_LIB_EVENT__INT_ACC,
						DISPATCHER_LIB_80_PERCENT_CAPACITY,0 );
		if(err < 0)
		{
			DBGERR("%s:%i: It wasn't possible to add event'\n",__func__,__LINE__);
		}
	}
	if( (Status->GlobalDevice_t.dmdev_acc.dmctrl_status & DM_ACC_STATUS_INT_PERCENT_20) && !(laststatus & DM_ACC_STATUS_INT_PERCENT_20 ) )
	{
		err= AdapterGlobalEventAddAndLog( Status, "ACC Capacity 20\%",
						DISPATCHER_LIB_EVENT__INT_ACC,
						DISPATCHER_LIB_20_PERCENT_CAPACITY,0 );
		if(err < 0)
		{
			DBGERR("%s:%i: It wasn't possible to add event'\n",__func__,__LINE__);
		}
	}
	if( (Status->GlobalDevice_t.dmdev_acc.dmctrl_status & DM_ACC_STATUS_EXT_PERCENT_20) && !(laststatus & DM_ACC_STATUS_EXT_PERCENT_20 ) )
	{
		err= AdapterGlobalEventAddAndLog( Status, "ACC Capacity 20\%",
						DISPATCHER_LIB_EVENT__EXT_ACC,
						DISPATCHER_LIB_20_PERCENT_CAPACITY,0 );
		if(err < 0)
		{
			DBGERR("%s:%i: It wasn't possible to add event'\n",__func__,__LINE__);
		}
	}
	if( (Status->GlobalDevice_t.dmdev_acc.dmctrl_status & DM_ACC_STATUS_EXT_PERCENT_70) && !(laststatus & DM_ACC_STATUS_EXT_PERCENT_70 ) )
	{
		err= AdapterGlobalEventAddAndLog( Status, "ACC Capacity 70\%",
						DISPATCHER_LIB_EVENT__EXT_ACC,
						DISPATCHER_LIB_80_PERCENT_CAPACITY,0 );
		if(err < 0)
		{
			DBGERR("%s:%i: It wasn't possible to add event'\n",__func__,__LINE__);
		}
	}

	if( ( Status->GlobalDevice_t.dmdev_acc.dmctrl_status & DM_ACC_STATUS_INT_LOW ) && !( laststatus & DM_ACC_STATUS_INT_LOW ) )
	{
		err= AdapterGlobalEventAddAndLog( Status, "Int ACC critical level",
										  DISPATCHER_LIB_EVENT__INT_ACC, 0, 0 );

		if( err < 0 )
		{
			DBGERR( "%s:%i: It wasn't possible to add event'\n",__func__,__LINE__ );
		}

		err = AdapterPowerGlobalOff( Status );

		if( err < 0 )
		{
			DBGERR( "%s:%i: It wasn't possible to power off'\n",__func__,__LINE__ );
		}
	}

	DBGLOG( "%s:%i: ACC Control Last Status\t0x%016"PRIxLEAST64"\n",__func__,__LINE__,laststatus );
	DBGLOG("%s:%i: ACC Control Status\t\t0x%016"PRIxLEAST64"\n",__func__,__LINE__,Status->GlobalDevice_t.dmdev_acc.dmctrl_status);

	
	return 0;
	
}

int AdapterAccControlModems( AdapterGlobalStatusStruct_t *Status )
{
	if( !Status )
	{
		DBGERR("%s:%i: Invalid Argumentn\n",__func__,__LINE__);
		return -1;
	}

	if( Status->AdapterSettings.Block0.AdapterSettings.Power & AXIS_SOFTRECORDER_SETTINGS__POWER_CHECK_POWER_ENABLE )
	{
		DBGLOG( "%s:%i: Check power enable. Try check ACC\n", __func__, __LINE__);

		if( Status->ACCStatus.AccStatus.int_acc.acc_present && 									//� ������� ������������ ���������� ���
			(Status->GlobalDevice_t.dmdev_acc.dmctrl_feature & DM_DM_ACC_NO_CHECK_POWER) == 0 )	//� � ���������� ������� �� ��������� ������������ ������ ���������
		{
			
			double int_dc = axis_acc_get_dc(&Status->ACCStatus.AccStatus.int_acc);
			if( int_dc < -0.01)
			{
				if(!Status->modemsInSleep)
				{
					if(DMXXXACCStopModems(&Status->GlobalDevice_t)< 0)
					{
						DBGERR("%s: It wasn't possible to stop modems\n",__func__);
						return -1;
					}
					Status->modemsInSleep =1;
				}
				
			}
			else if(Status->modemsInSleep)
			{
				if(DMXXXACCStartModems(&Status->GlobalDevice_t)< 0)
					{
						DBGERR("%s: It wasn't possible to stop modems\n",__func__);
						return -1;
					}
					Status->modemsInSleep =0;
			}
		}

	}

	return 0;
}

/**
 * @brief �������, ������� �������� ��������� ��� ��� �� �� �� ���������
 * @brief � �������� ���������� ��������� � ��������� Status->AccStatus
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterAccGetStatus( AdapterGlobalStatusStruct_t *Status )
{
	if( !Status )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	int ret = AXIS_NO_ERROR;

	//� ��� ���������� ����, ��� �� ������������ ���, ���� ��� - �� ������ �������
	if( Status->GlobalDevice_t.dmdev_acc.dmctrl_icontrol )
	{
		DBGLOG( "%s: %i Get status\n",__func__,__LINE__ );

		//� ��� ��� ����� ���� ��� � ����������, �� ������ ������ ��������
		if( Status->GlobalDevice_t.dmdev_acc.dmctrl_feature & DM_ACC_RECORDER_CNTRL )
		{
			DBGLOG( "%s: Acc status in recorder (old verion)\n", __func__ );
			if( Status->Recorder.recorderscount > 0 && Status->Recorder.next )
			{
				if( recorder_lock_time( &Status->Recorder.next->lock ) < 0 )
				{
					DBGERR( "%s: Can't lock recorder\n", __func__ );
					return -1;
				}

				Status->GlobalDevice_t.dmdev_acc.privatedata = &Status->Recorder.next->semaphore;
				Status->GlobalDevice_t.dmdev_acc.dmctrl_sets_hen = Status->Recorder.next->hen;

				//�� ������ �� ��, ��� �������� ������� ��������, �� ��� � ����
				//����� �������� ��������� � ����������
				ret = DMXXAccGetStatus( &Status->GlobalDevice_t, &Status->ACCStatus );

				recorder_unlock( &Status->Recorder.next->lock );

				if( ret < 0 )
				{
					DBGERR( "%s: It wasn't possible to get acc status from device\n", __func__ );
					return ret;
				}
			}

			else
			{
				DBGLOG( "%s: No Recorders for getting acc status\n", __func__ );
				return 0;
			}

		}

		//��� �� ��� ����� ���� �� ����� ������
		else if( Status->GlobalDevice_t.dmdev_acc.dmctrl_feature & DM_DM_ACC_STATUS_IN_RECORDER )
		{
		 	DBGLOG( "%s: Acc status in recorder (new version)\n", __func__ );
			//���� � ��� ������ ��� � ���������� - �� �� � ��� ������������ � ������
			ret = AdapterRecorderGetFistRecorderAccStatusCashed( &Status->GlobalDevice_t, &Status->Recorder );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't get ACC status from fist recorder\n", __func__ );
				return ret;
			}
		}

		//��� �� �� ����� ���� � ��������
		else if( Status->GlobalDevice_t.dmdev_acc.dmctrl_ipresent )
		{
		 	DBGLOG( "%s: Acc status in adapter\n", __func__ );
			ret = DMXXAccGetStatus( &Status->GlobalDevice_t, &Status->ACCStatus );

			if( ret < 0 )
			{
				DBGERR( "%s: It wasn't possible to get acc status from device\n", __func__ );
				return ret;
			}
		}

		else
		{
			//����� �� ������������ ���, �� ��� ���, ��� �� ���������
			DBGERR( "%s: Cnotrol not pressent ACC\n", __func__ );
			return -AXIS_ERROR_INVALID_VALUE;
		}

		//���� �� ������������ ��� - ������� �� ��� ������������
		ret = AdapterControlAccStatus( Status );

		if( ret < 0 )
		{
			DBGERR( "%s:It wasn't possible to cotrol acc status\n", __func__ );
		}

	}

	return ret;
}

//������� ��������� �������� network.txt
//��� ������������ ������ - ���������
//������ ����������

//���������� �������
#include <axis_pid.h>

#include <dmxxx_lib/dmxxx_calls.h>

//������������ �������
#include <dispatcher_lib/dispatcher_lib_resource.h>
#include <dispatcher_lib/sprintf_ini_files.h>
#include <dispatcher_lib/command.h>

//��������� �������
#include <adapter/new_command.h>
#include <adapter/txt_files.h>
#include <adapter/httpd.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif

//������� ��������� �������� ���������� ��������
//������ � ���������� ������� ��������
//����:
// b - ��������� �� ����� � ������� ���������� ���������
// bsize - ������ ������� ������
// requests - ��������� �� ��cc�� �������� �������
// count - ���������� ��������� � ������ �������
// Status - ��������� �� ���������� ��������� ������� ��������
// WorkCommand - ���� ����, ��� �� ������������ ������� ��� ��� (������ ������)
//�������:
// ���������� ���� ���������� � �����
int AdapterTxtFileWorkNetworkTxt( char *b, size_t bsize,
								  axis_httpi_request *requests, int count,
								  AdapterGlobalStatusStruct_t *Status, int WorkCommand )
{

	if( !b || bsize <= 0 || !requests || count < 0 || !Status )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return 0;
	}

	DBGLOG( DISPATCHER_LIB_DEBUG__START );

#ifdef CONFIG_ENCODER_DEBUG_MSG
	axis_httpi_print_request( requests, count );
#endif

	int cc = 0;
	int lcc = 0;
	size_t size = bsize;
	unsigned int CommandMask = 0;
	void *NetTimers;

	//������� ������� �������� � ���� ��������� � � ���� ������ ����������
	if( Status->GlobalDevice_t.dmdev_timer.dmctrl_feature & DM_TIMER_TYPE_DSP )
	{
		NetTimers = &Status->TimersSettingsNet;
	}
	else
	{
		NetTimers = &Status->TimersSettingsNetAxis;
	}

	//������ ���� ��������� �� ��� ��� ������ � �������, ���� �� ������� ������ �������
	if( WorkCommand )
	{
		CommandMask = ADAPTER_INTERFACE_COMMAND__NETSAVE;
	}

	DBGLOG("%s: %i field\n",__func__,__LINE__);

	//������ ���� ��������� �� ��� ��� ������ � �������
	lcc = AdapterCommandWorkRequest( b + cc, size, requests, count, CommandMask, Status, NULL, NULL );

	cc	   += lcc;
	size   -= lcc;

	if( size <= 0 ) goto end;

	if( iDMXXXAdapterSettingsIniFileSprintf( &Status->GlobalDevice_t ) )
	{
		//NOTE ����� AIP � ���������� ��������� ���������
		lcc = DMXXXAdapterSettingsIniFileSprintf( &Status->GlobalDevice_t, b + cc, size,
					&Status->AdapterSettings, NetTimers, &Status->DHCPNetParam,
					AdapterHttpdGetClientIP( axis_getpid(), Status, NULL ) );
	}
	else
	{
		//NOTE - ��� ��� ���������, ������� ��� �� ���������� ����������
		lcc = DispatcherLibIniFileSprintfAdapterSettings( b + cc, size,
					&Status->AdapterSettings, NetTimers, &Status->DHCPNetParam,
					AdapterHttpdGetClientIP( axis_getpid(), Status,NULL ),
					Status->GlobalDevice_t.dmdevice_type );
	}

	cc	   += lcc;
	size   -= lcc;

	if( size <= 0 ) goto end;


end:

	DBGLOG( "NetWork.txt: Stop\n" );

	b[ bsize - 1 ] = 0x00;

	return cc;
}

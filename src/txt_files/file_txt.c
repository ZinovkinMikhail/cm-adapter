//��� ����� ������� ������� ������������ ������� �������� - ��������� ��������
//� ������������ � ����� ����������

//�����: ��������� ����� ��� "����"
//������: 2010.09.14 12:13
//�����: i.podkolzin@ross-jsc.ru

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>

#include <axis_error.h>

#include <dispatcher_lib/dispatcher_lib_resource.h>
#include <dispatcher_lib/ini_to_requests.h>

#include <adapter/resource.h>
#include <adapter/txt_files.h>


//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif

//���������� ������� ������� ������������ ������ �� ������� ��������
//��� ���� �� �������, ��� ��� ����������� ����������� �����������
//� ��� ����������� ������ ����������
//� � ����� ������ �� ��� ���� �������� ��� ���������� ��� �����
//�� ���������� �������� � ������� ���������, � ������ ���� ��� �����
//����: 
// cInputFileName - ��������� �� ������ � ������ �������� �����
// OutputFile - ������ ��������� ��������� �� ������� ���� ������ ���������
// Status - ��������� �� ���������� ��������� ������� ����������
//������:
// 0 - ��� ������� �����������
// ����� 0 � ������ ������
int AdapterFileTxtDoIt( const char *cInputFileName, int OutputFile,
				AdapterGlobalStatusStruct_t *Status )
{

	char InputBuffer[ DISPATCHER_LIB_BUFFFER_SIZE ]; 
												//����� ��� ����
	char OutputBuffer[ DISPATCHER_LIB_BUFFFER_SIZE ];
												//����� ��� �����
	int ret = AXIS_NO_ERROR;					//��� ��������
	int iFile = -1;								//������ �������� �����
	int count = -1;								//���������� ���������� ��������
	axis_httpi_request requests[AXIS_HTTPI_MAX_REQUEST_COUNT];
												//���������� ������ ��������
	char *dalee = NULL;							//��� ������ ����� ����������
	int	shift = 0;								//����� �� ������ ������ �� �����
												//��� �� �� �������� ��������� const

	if( !cInputFileName || !Status )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}
	
	if( OutputFile < 0 )
	{
		DBGERR( "%s: Output file not open\n", __func__ );
		return -AXIS_ERROR_DEVICE_NOT_OPEN;
	}

	struct Recorder_s **recorder = NULL;
	DBGLOG( DISPATCHER_LIB_DEBUG__START );


	
	//������� ����
	iFile = open( cInputFileName, O_RDONLY );
	if( iFile < 0 )
	{
		DBGERR("%s: Can't open input file '%s'\n", __func__, cInputFileName);
		return -AXIS_ERROR_CANT_OPEN_DEVICE;
	}
	
		
	//���������� ���� ������������� ������� ���� � �������
	count = ini_to_requests_ex( iFile, InputBuffer, sizeof( InputBuffer ),
								requests, AXIS_HTTPI_MAX_REQUEST_COUNT );
	if( count < 0 )
	{
		DBGERR("%s: Can't convert file to requests\n",__func__);
	}
	
	close( iFile );
	if( count < 0 )
	{
		return count;
	}

#ifdef CONFIG_ENCODER_DEBUG_MSG
    axis_httpi_print_request(requests, count);
#endif


	
	//���������� ����
	dalee = strrchr( cInputFileName, '/' );
	if( !dalee )
	{
		shift = 0;
	}
	else
	{
		shift = (int)(dalee - cInputFileName);
	}
	
	//������ ��� ������� �� ���� ��� ������
	if( strcmp( cInputFileName + shift,
						ADPATER_HELPER__ADAPTER_INPUT_FILE_MODE ) == 0 )
	{
		ret = AdapterTxtFileWorkModeTxt( OutputBuffer, sizeof( OutputBuffer ),
									requests, count,
									Status,recorder );
	}
	else if( strcmp( cInputFileName + shift,
						ADPATER_HELPER__ADAPTER_INPUT_FILE_STATUS ) == 0 )
	{
		DBGERR("%s: Not function for '%s'\n", __func__, dalee);
		return -AXIS_ERROR_INVALID_ANSED;
	}
	else if( strcmp( cInputFileName + shift,
						ADPATER_HELPER__ADAPTER_INPUT_FILE_NETWORK ) == 0 )
	{
		ret = AdapterTxtFileWorkNetworkTxt( OutputBuffer, sizeof( OutputBuffer ),
									requests, count,
									Status, 1 ); //�� ����� ������ ������
	}
	else
	{
		DBGERR("%s: Invalid file name for work '%s'\n",__func__, dalee );
		return -AXIS_ERROR_INVALID_ANSED;
	}
	
	if( ret < 0 )
	{
		DBGERR("%s: Invalid ansed from txt function %d\n",__func__, ret );
		return -AXIS_ERROR_INVALID_ANSED;
	}
	
	OutputBuffer[ ret ] = 0x00;
	
	if( write( OutputFile, OutputBuffer, ret ) < 0 )
	{
		DBGERR("%s: Can't write to output file\n", __func__);
		return -AXIS_ERROR_CANT_WRITE_DEVICE;
	}

	DBGLOG( DISPATCHER_LIB_DEBUG__DONE );
	
	return AXIS_NO_ERROR;
	
}

//������� ��������� �������� mode.txt
//��� ������������ ������ - ���������
//������ ����������

//���������� �������

//������������ �������
#include <axis_error.h>

#include <dispatcher_lib/dispatcher_lib_resource.h>
#include <dispatcher_lib/sprintf_ini_files.h>
#include <dispatcher_lib/command.h>

//��������� �������
#include <adapter/new_command.h>
#include <adapter/txt_files.h>
#include <adapter/recorder.h>


//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif

//������� ��������� �������� ���������� ����������
//������ � ���������� ������� ��������
//����:
// b - ��������� �� ����� � ������� ���������� ���������
// bsize - ������ ������� ������
// requests - ��������� �� ��cc�� �������� �������
// count - ���������� ��������� � ������ �������
// Status - ��������� �� ���������� ��������� ������� ��������
//�������:
// ���������� ���� ���������� � �����
int AdapterTxtFileWorkModeTxt( char *b, size_t bsize,
                               axis_httpi_request *requests,
                               int count,
                               AdapterGlobalStatusStruct_t *Status, struct Recorder_s **recorder )
{

	if( !b || bsize <= 0 || !requests || count < 0 || !Status || !( *recorder ) )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return 0;
	}

	DBGLOG( DISPATCHER_LIB_DEBUG__START );

#ifdef CONFIG_ENCODER_DEBUG_MSG
	axis_httpi_print_request( requests, count );
#endif

	int cc = 0;
	int lcc = 0;
	size_t size = bsize;


	//����� ������� �� ������������
	unsigned int CommandMask = Status->GlobalDevice_t.dmdev_recorder.dmctrl_feature >> DM_RECORDER_COMMAND_MASK_SHIFT;

	if( !CommandMask )
	{
		DBGERR( "%s: Device not support any recorder command. Command mask is empty\n", __func__ );
		//�� ����� ���� ��� �� ������ ������ ��� ��������� ������ - ������ ������ � ���
	}
	
	//�������� �� ������ ������ ����������� �� ���������� ����������
	//�� ������� �������� � mode.txt - � ������ ������ � ���������� ��������
	CommandMask &= (ADAPTER_INTERFACE_COMMAND__APPLY | ADAPTER_INTERFACE_COMMAND__SAVE );

	DBGLOG("%s: %i field\n",__func__,__LINE__);
	//������ ���� ��������� �� ��� ��� ������ � �������
	lcc = AdapterCommandWorkRequest(
	              b + cc, size,
	              requests,
	              count,
	              CommandMask,
	              Status, recorder, NULL );

	cc	+= lcc;
	size	-= lcc;

	if( size <= 0 ) goto end;

	lcc = AdapterRecorderPrintSettingsIni( *recorder,
	                                       b + cc, size );

	cc	+= lcc;
	size	-= lcc;

	if( size <= 0 ) goto end;

end:

	DBGLOG( DISPATCHER_LIB_DEBUG__DONE );

	b[ bsize - 1 ] = 0x00;

	return cc;

}


//����� ����� ��� ������� ������� ���������� � �������� ������ ��� HTTPD �������


//�����: ��������� ����� ��� "����"
//�����: i.podkolzin@ross-jsc.ru
//������: 2010.09.15 17:29

#include <string.h>

#include <axis_error.h>

#include <dispatcher_lib/dispatcher_lib_resource.h>
#include <dispatcher_lib/network.h>

#include <adapter/resource.h>
#include <adapter/global.h>
#include <adapter/httpd.h>
#include <adapter/network.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif

//��������������� ������� ��������� ������,��� ������� Qt
//������� �������� � ������ ��������� ���������
//����:
// User - ��������� �� ����� ���� ��������� ��� ������������
// UserSize - ������ ������� ������
// Passwd - ��������� �� ����� ���� ������� ������
// passwdSize - ������ ������� ������
//������:
// ����� 0 � ������ ������
// 0 - ��� ��
int AdapterHTTPDGetPasswordCallbackQt( char *User, int UserSize, char *Passwd, int PasswdSize, void *data )
{

	if( !User || UserSize <= 0 || !Passwd || PasswdSize <= 0 )
	{
        DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT  );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	AdapterGlobalStatusStruct_t *Status = AdapterGlobalGetStatus();

	if( !Status )
	{
        DBGERR( "%s: Invalid global status\n", __func__  );
		return -AXIS_ERROR_INVALID_ANSED;
	}

	memcpy( User,
			Status->CurrentNetworkInterface.Login,
			UserSize );
	User[ UserSize - 1 ] = 0x00;

	memcpy(	Passwd,
			Status->CurrentNetworkInterface.Passwd,
			PasswdSize );

	Passwd[ PasswdSize - 1 ] = 0x00;

	return 0;
}

//��������������� ������� ��������� ������,
//������� �������� � ������ ��������� ���������
//����:
// User - ��������� �� ����� ���� ��������� ��� ������������
// UserSize - ������ ������� ������
// Passwd - ��������� �� ����� ���� ������� ������
// passwdSize - ������ ������� ������
//������:
// ����� 0 � ������ ������
// 0 - ��� ��
int AdapterHTTPDGetPasswordCallback( char *User, int UserSize, char *Passwd, int PasswdSize, void *data )
{

	if( !User || UserSize <= 0 || !Passwd || PasswdSize <= 0 )
	{
        DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT  );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}
	
	AdapterGlobalStatusStruct_t *Status = AdapterGlobalGetStatus();
	
	if( !Status )
	{
        DBGERR( "%s: Invalid global status\n", __func__  );
		return -AXIS_ERROR_INVALID_ANSED;
	}

	memcpy( User,
			Status->CurrentNetwork.Login,
			UserSize );
	User[ UserSize - 1 ] = 0x00;
	
	memcpy(	Passwd,
			Status->CurrentNetwork.Passwd,
			PasswdSize );
			
	Passwd[ PasswdSize - 1 ] = 0x00;
	
	return 0;
}

//��������������� ������� ��������� ������,
//������� �������� � ������ ��������� ���������
//����:
// User - ��������� �� ����� ���� ��������� ��� ������������
// UserSize - ������ ������� ������
// Passwd - ��������� �� ����� ���� ������� ������
// passwdSize - ������ ������� ������
//������:
// ����� 0 � ������ ������
// 0 - ��� ��
int AdapterHTTPDGetSPasswordCallback( char *User, int UserSize, char *Passwd, int PasswdSize, void *data )
{
	
	memcpy( User,
			ADAPTER_HTTPD_INTERFACE_SUPPER_USER,
			UserSize );
	User[ UserSize - 1 ] = 0x00;
	
	memcpy(	Passwd,
			ADAPTER_HTTPD_INTERFACE_SUPPER_PASSWD,
			PasswdSize );
			
	Passwd[ PasswdSize - 1 ] = 0x00;
	
	return 0;

}

//��������������� ������� ��������� ������, ������� ���������
//IP ����� ������������� �� ����������
//����:
// ip - ���������� IP ����� � �������� ������� ����
//������:
// 0 - IP ������� - ����� ����������
// ����� 0 - ���� �� ���
int AdapterHTTPDCheckIPCallback( uint32_t ip )
{

    char bu[64];
	int ret;

//��� ��������� ��� ���������
    DBGLOG("%s: Accepting client %s\n", __func__, axis_sprint_ip( bu, ip ));

	//���� �� � ������ �������������� - �� ��������� ���������� ������
	//�� ��������� ����
	AdapterGlobalStatusStruct_t *Status = AdapterGlobalGetStatus();
	if( !Status )
	{
		DBGERR("%s: Can't get global status for adapter\n",__func__);
		return -AXIS_ERROR_INVALID_ANSED;
	}
	
	if( Status->RescueMode )
	{
		//��������� �� ��������� � ��������� ����
		ret = DispatcherLibNetworkAllowLocalNetwork( ip );
		if( !ret )
		{
			DBGERR("%s: Device in rescue mode. IP '%s' is not local network\n",
					__func__, axis_sprint_ip( bu, ip ) );
			return -AXIS_ERROR_INVALID_ANSED;
		}
	}

	ret = AdapterNetworkChaeckIPForAllowNetwork(
			ip,
			Status->AdapterSettings.Block0.AdapterSettings.AllowNet,
			Status->AdapterSettings.Block0.AdapterSettings.AllowMask,
			AXIS_SOFTADAPTER_ALLOW_NET_COUNT  );
			
	if( !ret )
	{
	    DBGLOG("%s: '%s' is no grand ip - close\n", __func__, axis_sprint_ip( bu, ip ));
	    return -1;
	}
	
	return 0;
}



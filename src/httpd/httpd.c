//��� ��� ��������� � HTTP ��������

//���������� �������

//������������ �������
#include <axis_error.h>
#include <axis_pid.h>

#include <dispatcher_lib/dispatcher_lib_resource.h>

#include <dmxxx_lib/dmxxx_calls.h>

//��������� �������
#include <adapter/httpd.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif


//������� ������� ���������� IP ����� �������
//� ��������� �������� �� �������
//����:
// pid - ������������� ����������� ������
//	��� ��������� ������� �������������� ����������� �������
//	axis_getpid()
// Status - ��������� �� ���������� ��������� ������� ��������
// NetSettings - ���������� ��������� �� ��������� �������� ��� ������� ��������
//�������:
// ���������� ����� ������� ������� �� ������
//	��� ������� ���� �������� � ��������� ������
// 0 - �� ����� ������
unsigned int AdapterHttpdGetClientIP( pid_t pid, AdapterGlobalStatusStruct_t *Status,CurrentNetworkSettingsStruct_t** NetSettings )
{
	unsigned int ret;

	if( pid <= 0 || !Status )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return 0;
	}

	//��������� �������� � ������� �������
	ret = axis_ssl_httpi_server_get_client_ip( &Status->HTTPDMain.Server,  pid );

	if( ret )
	{
		//����� ...
		if(NetSettings)
			*NetSettings = &Status->CurrentNetwork;
		return ret;
	}

	//���� �� �������� � ��������� ������ - �� ��������� ����� � ��� ���� ������ ������
	if( Status->CurrentNetwork.ClientMode )
	{
		//��������� �������� � ������� �������
		ret = axis_ssl_httpi_server_get_client_ip( &Status->HTTPDSec.Server,  pid );

		if( ret )
		{
			//����� ...
			if(NetSettings)
				*NetSettings = &Status->HTTPSecondNetwork;

		//	NetSettings = &Status->HTTPSecondNetwork;
			return ret;
		}
		
		//��� �� ����� - ������ ��� ���� ���� ���������� ������ 
	//	NetSettings = &Status->CurrentNetwork;
		if(NetSettings)
			*NetSettings = &Status->CurrentNetwork;


		return  axis_create_ip(Status->CurrentNetwork.ServerHost);
		
	}

	//��� �� ����� �� ������ - ��� �� ������
	DBGERR( "%s: Can't find client IP from server\n", __func__ );

	return 0;
}

//������� ������� ��������� ������������ ���������� �����
//������� �������������� ����� ��������
//����:
// Status - ���������� ��������� �������
//������:
// ������������ ���������� ������
// 0 - � ������ ������
int AdapterHTTPdGetMaxSessionCount( AdapterGlobalStatusStruct_t *Status )
{
	int ret = 0;
	if ( !Status )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return 0;
	}

	ret += axis_ssl_httpi_server_get_clients_count( &Status->HTTPDMain.Server );
	ret += axis_ssl_httpi_server_get_clients_count( &Status->HTTPDSec.Server );

	return ret;
}
/**
 * @brief ����� ����� �������, � ����������� �� ������������ �������� ���� ��� ��� ��� ����������
 *
 * @param Status ��������� �� ���������� ���������
 * @return 0 - �� ����� - ������
 **/
int AdapterHttpdInit( AdapterGlobalStatusStruct_t *Status )
{
	if ( !Status )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	if ( Status->RescueMode )
	{
		AdapterGlobalSetRescueMode();
	}
	else
	{
		Status->CurrentNetwork.ClientMode = Status->AdapterSettings.Block0.AdapterSettings.Connect &
		                                    AXIS_SOFTADAPTER_CONNECT__CLIENT_MODE;

		axis_sprint_ip( Status->CurrentNetwork.ServerHost,  Status->AdapterSettings.Block0.AdapterSettings.ServerIPAddress );

		Status->CurrentNetwork.ControlPort = Status->AdapterSettings.Block0.AdapterSettings.ControlPort;

		Status->CurrentNetwork.UploadPort = Status->AdapterSettings.Block0.AdapterSettings.UploadPort;

		Status->CurrentNetwork.MonitoringPort = Status->AdapterSettings.Block0.AdapterSettings.MonitoringPort;

		memcpy( Status->CurrentNetwork.Login, Status->AdapterSettings.Block0.AdapterSettings.AdapterUser,
		        sizeof( Status->CurrentNetwork.Login ) );

		memcpy(	Status->CurrentNetwork.Passwd, Status->AdapterSettings.Block0.AdapterSettings.AdapterPasswd,
		        sizeof( Status->CurrentNetwork.Passwd ) );

		memcpy( Status->CurrentNetwork.Alias,  Status->AdapterSettings.Block0.AdapterSettings.Alias,
		        sizeof( Status->CurrentNetwork.Alias ) );

		if ( AdapterHTTPdStart( &Status->HTTPDMain, &Status->CurrentNetwork, Status->AdapterDevice, &Status->LastInterfaceQuire, 0 ) )
		{
			DBGERR( "%s:It wasn't possible to start fist httpd server\n", __func__ );
			return -1;

		}
		
		//���� ���� ��������� ��� ������� ������� �������, � ���� �� � ������ �������, �� �������� ���
		if( Status->CurrentNetwork.ClientMode && Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature & DM_DM_ENABLE_TWO_HTTP_SERVER )
		{
			Status->HTTPSecondNetwork.ClientMode = 0; //�� � ������ ������� ����������
			Status->HTTPSecondNetwork.ControlPort = Status->AdapterSettings.Block0.AdapterSettings.ControlPort;

			Status->HTTPSecondNetwork.UploadPort = Status->AdapterSettings.Block0.AdapterSettings.UploadPort;

			Status->HTTPSecondNetwork.MonitoringPort = Status->AdapterSettings.Block0.AdapterSettings.MonitoringPort;

			memcpy( Status->HTTPSecondNetwork.Login, Status->AdapterSettings.Block0.AdapterSettings.AdapterUser,
					sizeof( Status->HTTPSecondNetwork.Login ) );

			memcpy(	Status->HTTPSecondNetwork.Passwd, Status->AdapterSettings.Block0.AdapterSettings.AdapterPasswd,
					sizeof( Status->HTTPSecondNetwork.Passwd ) );

			memcpy( Status->HTTPSecondNetwork.Alias,  Status->AdapterSettings.Block0.AdapterSettings.Alias,
					sizeof( Status->HTTPSecondNetwork.Alias ) );

			if( AdapterHTTPdStart( &Status->HTTPDSec, &Status->HTTPSecondNetwork, Status->AdapterDevice, &Status->LastInterfaceQuire, 0 ) )
			{
				DBGERR( "%s:It wasn't possible to start second httpd server\n", __func__ );
				return -1;

			}

		}
		
	}

	if ( Status->GlobalDevice_t.dmdev_interface.dmctrl_ipresent )
	{
		if ( !Status->GlobalDevice_t.dmdev_interface.privatedata )
		{
			DBGERR( "%s:No private data for interface\n", __func__ );
			return -1;
		}

		dmdev_user_interface_t *interface = ( dmdev_user_interface_t* )Status->GlobalDevice_t.dmdev_interface.privatedata;

		if ( !( interface->communication_type & DM_INTERFACE_CONNECTION_HTTPD ) )
		{
			return 0;
		}

		Status->CurrentNetworkInterface.ClientMode = AXIS_SOFTADAPTER_CONNECT__CLIENT_MODE;

		axis_sprint_ip(Status->CurrentNetworkInterface.ServerHost, interface->intrface_server_configuration.ip);

		Status->CurrentNetworkInterface.ControlPort = interface->intrface_server_configuration.portssl;
		Status->CurrentNetworkInterface.UploadPort = interface->intrface_server_configuration.porthttp;
		Status->CurrentNetworkInterface.MonitoringPort = interface->intrface_server_configuration.portudp;
		strncpy( Status->CurrentNetworkInterface.Login, interface->intrface_server_configuration.login,
		         sizeof( Status->CurrentNetwork.Login ) );
		strncpy(	Status->CurrentNetworkInterface.Passwd, interface->intrface_server_configuration.pass, sizeof( Status->CurrentNetwork.Passwd ) );
		memcpy( Status->CurrentNetworkInterface.Alias, interface->intrface_server_configuration.alias,
		        sizeof( Status->CurrentNetwork.Alias ) );

		if(AdapterHTTPdStart( &Status->HTTPDQt, &Status->CurrentNetworkInterface, Status->AdapterDevice, &Status->LastInterfaceQuireQt, 1 ))
		{
			DBGERR("%s:It wasn't possible to start httpd for interface\n",__func__);
			return -1;
		}
	}

	return 0;

}

/**
 * @brief ���� ����� �������, � ����������� �� ������������ �������� ���� ��� ��� ��� ����������
 *
 * @param Status ��������� �� ���������� ���������
 * @return 0 - �� ����� - ������
 **/
int AdapterHttpdDeinit( AdapterGlobalStatusStruct_t *Status )
{
	if( !Status )
	{
		DBGERR("%s:%i: Invalid Argumentn\n",__func__,__LINE__);
		return -1;
	}

	if(AdapterHTTPdStop( &Status->HTTPDMain ))
	{
		DBGERR( "%s: Can't stop main HTTPd thread\n", __func__ );
	}

	if( Status->CurrentNetwork.ClientMode )
	{
		//�� � ������ ������� - ��������� ����� � ��� ���� ������ ������
		if( AdapterHTTPdStop( &Status->HTTPDMain ) )
		{
			DBGERR( "%s: Can't stop second HTTPd thread\n", __func__ );
		}
	}
	
	if(Status->GlobalDevice_t.dmdev_interface.dmctrl_ipresent )
	{
			dmdev_user_interface_t *interface = ( dmdev_user_interface_t* )Status->GlobalDevice_t.dmdev_interface.privatedata;

		if ( !( interface->communication_type & DM_INTERFACE_CONNECTION_HTTPD ) )
		{
			return 0;
		}

		if(AdapterHTTPdStop(&Status->HTTPDQt))
		{
			DBGERR("%s:It wasn't possible to stop httpd qt\n",__func__);
			return -1;
		}
	}

	return 0;

	
}

static int AdapterHttpdCheckServer( const char *Name, 
									AdapterHTTPDStatusStruct_t *Server, CurrentNetworkSettingsStruct_t *Network,
									int AdapterDevice, time_t *LastInterfaceQuire, int iQt )
{
	int ret = 0; 

	//����� ������� - ���� �� ��� �������
	DBGLOG( "%s: HTTPD %s thread Count=%d LastCount=%d\n", __func__, Name,
			Server->Server.tCount,
			Server->LastTCount );

	if( Server->Server.tCount == Server->LastTCount )
	{
		DBGERR( "%s: HTTPD %s thread is dead!!! Restart it\n", __func__, Name );

		ret = AdapterHTTPdStart( Server, Network, AdapterDevice, LastInterfaceQuire, iQt );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't restart dead HTTPD %s thread\n", __func__, Name );
		}
	}

	Server->LastTCount = Server->Server.tCount;
	
	return ret;
}


int AdapterHttpdCheck( AdapterGlobalStatusStruct_t *Status )
{
	if ( !Status )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	int ret = 0;

	//������� �������� ������
	if ( Status->HTTPDMain.Server.pid )
	{
		ret = AdapterHttpdCheckServer( "Main", &Status->HTTPDMain, &Status->CurrentNetwork,
									   Status->AdapterDevice, &Status->LastInterfaceQuire, 0 );
		
		if( ret < 0 )
		{
		 	DBGERR( "%s: Can't check main HTTPd server\n", __func__ );
		}
	}
	
	//����� ��������������
	if( Status->HTTPDSec.Server.pid )
	{
		ret = AdapterHttpdCheckServer( "Second", &Status->HTTPDSec, &Status->HTTPSecondNetwork,
									   Status->AdapterDevice, &Status->LastInterfaceQuire, 0 );
		
		if( ret < 0 )
		{
		 	DBGERR( "%s: Can't check second HTTPd server\n", __func__ );
		}
	}
	
	//����� Qt
	if( Status->HTTPDQt.Server.pid )
	{
		ret = AdapterHttpdCheckServer( "Qt", &Status->HTTPDQt, &Status->CurrentNetworkInterface,
									   Status->AdapterDevice, &Status->LastInterfaceQuire, 1 );
		
		if( ret < 0 )
		{
		 	DBGERR( "%s: Can't check Qt HTTPd server\n", __func__ );
		}
	}

	return 0;

}


//������� ������� �������������� ��������� - ������ ������� ������� �� ������������
// ��� ������ �������

//�����: ��������� ����� ��� "����"
//������: 2010.09.14 18:01
//�����: i.podkolzin@ross-jsc.ru

#include <axis_error.h>
#include <axis_httpi_tags.h>

#include <axis_ssl_httpi_download.h>
#include <axis_ssl_monitoring.h>

#include <dispatcher_lib/dispatcher_lib_resource.h>

//��������� �������
#include <adapter/httpd.h>
#include <adapter/interface.h>
#include <adapter/resource.h>
#include <adapter/debug.h>
#include <adapter/mtdupltest.h>
//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif



//���������� ������� ������������� ���������� ����������
//�� ���� �� ��� ���� ����� SSL
//����:
// Control - ��������� �� ��������� �������� ��� ������� �����������
// Pages - ��������� �� ������ �������� � ������� ����� �������� ��������
// Count - ���������� ��������� ������� ������� - ������������ ����� �������
// ssl - ���� ��� ������ ������ �� ������������� ��������
//�������:
// 0 - ��� ��
// ����� 0 � ������ ������
int AdapterHTTPdInterfaceInitControls( axis_ssl_httpi_control *Control,
                                       axis_ssl_httpi_page_control *Pages, size_t Count,
                                       int ssl )
{

	if ( !Control || !Pages || Count <= 0 )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	DBGLOG( DISPATCHER_LIB_DEBUG__START );

	axis_ssl_httpi_control_init( Control, Pages, Count ); //�� ����� ��� � C3E ���������

	// 1 1
	//������� ������� ��� ������ ������ ���� �������� ����������
	//� ������ ������ - ��� �� �� ���������
	axis_ssl_httpi_control_add( Control, AXIS_HTTPI_MAIN_PAGE,
	                            AdapterInterfaceDefaultTxtPrint );


	if ( 1 /*ssl*/ ) //��� SSL ���� �����
	{
		//�������� ������� 2
		axis_ssl_httpi_control_add( Control, ADAPTER_INTERFACE_STATUS_TXT_PAGE,
		                            AdapterInterfaceStatusTxtPrint );

		//������ ���� ��� 3
		axis_ssl_httpi_control_add( Control, ADAPTER_INTERFACE_MODE_TXT_PAGE,
		                            AdapterInterfaceModeTxtPrint );

		//����� ������� ��� 4
		axis_ssl_httpi_control_add( Control, ADAPTER_INTERFACE_NETWORK_TXT_PAGE,
		                            AdapterInterfaceNetworkTxtPrint );

		//�������� ���������� ���� 5
		axis_ssl_httpi_control_add( Control, ADAPTER_INTERFACE_LOG_TXT_PAGE,
		                            AdapterInterfaceLogTxtPrint );
		//�������� ���������� ���� �������� 6
		axis_ssl_httpi_control_add( Control, ADAPTER_INTERFACE_DEBUG_TXT_PAGE,
		                            AdapterDebugTxtPrint );
		//�������� ���� ���� 7
		axis_ssl_httpi_control_add( Control, ADAPTER_INTERFACE_KERNEL_TXT_PAGE,
		                            AdapterKernelDebugTxtPrint );
		
		// �������� ������� ����� ����� txt 8
		axis_ssl_httpi_control_add( Control, ADAPTER_HELPER__ADAPTER_INPUT_FILE_NAND_TEST_LOG, 
					    AdapterInterfaceNandTestLogTxtPrint );
		// �������� ����������� NAND 9
		axis_ssl_httpi_control_add( Control, ADAPTER_HELPER__ADAPTER_INPUT_FILE_NAND_DIAGNOSTIC_LOG,
					    AdapterInterfaceDiagnosticTxtPrint );		
		//�������� �������� ���� ���������� 10
		axis_ssl_httpi_control_add( Control, ADAPTER_INTERFACE_DSP_LOG_TXT_PAGE,
		                            AdapterInterfaceLogDspTxtPrint );

		//TODO - ��� ���� ���� � ����!!! + ���� ����������� �� ������� ����� HTTPS
		//�������� �������� ���������� ������ 11
		axis_ssl_httpi_control_add( Control,    AXIS_SSL_DOWNLOAD_PAGE,
	                            AdapterInterfaceUploadPrint );

		//�������� �������� ���� ������ 12
		axis_ssl_httpi_control_add( Control,    AXIS_SSL_DOWNLOAD_FULL_PAGE,
					AdapterInterfaceUploadPrint );

		//�������� �������� ����������� 13
		axis_ssl_httpi_control_add( Control,    AXIS_SSL_DOWNLOAD_NVELOP_PAGE,
					AdapterInterfaceUploadPrint );
		
		//�������� �������� GPS ���� 14
		axis_ssl_httpi_control_add( Control, AXIS_SSL_DOWNLOAD_GPS_DATA_PAGE,
		 			AdapterInterfaceUploadPrint );

		//�������� ���������� �� ����� ����
		axis_ssl_httpi_control_add( Control, ADAPTER_HELPER__ADAPTER_INPUT_FILE_FIRMWARE,
	                            AdapterInterfaceFirmware );

		//�������� �������� ��������� ���� ���������� 10 - ����� �������� ������
		axis_ssl_httpi_control_add( Control, AXIS_SSL_DOWNLOAD_LOG_DATA_PAGE,
		                            AdapterInterfaceUploadPrint );
	}

	//��� ��������� �������� 10 2
	
	
	//��������� � ����� ���������� �� SSL �� ���� �����

	//�������� �������� ���������� ������ 11 3
	axis_ssl_httpi_control_add( Control,    AXIS_SSL_DOWNLOAD_PAGE,
	                            AdapterInterfaceUploadPrint );

	//�������� �������� ���� ������ 12 4
	axis_ssl_httpi_control_add( Control,    AXIS_SSL_DOWNLOAD_FULL_PAGE,
	                            AdapterInterfaceUploadPrint );

	//�������� �������� ����������� 13 5
	axis_ssl_httpi_control_add( Control,    AXIS_SSL_DOWNLOAD_NVELOP_PAGE,
	                            AdapterInterfaceUploadPrint );

	//�������� �������� ���������� ������ 14 6
	axis_ssl_httpi_control_add( Control,    AXIS_SSL_DOWNLOAD_PAGE_USB,
	                            AdapterInterfaceUploadPrint );

	//�������� �������� ���� ������ 15 7
	axis_ssl_httpi_control_add( Control,    AXIS_SSL_DOWNLOAD_FULL_PAGE_USB,
	                            AdapterInterfaceUploadPrint );

	//�������� �������� ����������� 16 8
	axis_ssl_httpi_control_add( Control,    AXIS_SSL_DOWNLOAD_NVELOP_PAGE_USB,
	                            AdapterInterfaceUploadPrint );

	//���� ������ ��������� ����� �������� ����������� ����� http
	// 17 9
	axis_ssl_httpi_control_add( Control,    AXIS_SSL_MONITORING_PAGE,
	                            AdapterInterfaceMonitoringPrint );
	// 18 10
	axis_ssl_httpi_control_add( Control,    AXIS_SSL_DOWNLOAD_TEST_PAGE,
	                            AdapterInterfaceTestPrint );
	// 19 11
	axis_ssl_httpi_control_add( Control,    ADAPTER_INTERFACE_MTD_TEST_TXT_PAGE,
	                            AdapterInterfaceUploadMtdTest );

	DBGLOG( DISPATCHER_LIB_DEBUG__DONE );

	return AXIS_NO_ERROR;
}


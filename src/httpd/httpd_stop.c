//������� ��������� ������� HTTPD

//�����: ��������� ����� ��� "����"
//�����: i.podkolzin@ross-jsc.ru
//������: 2010.09.15 19:15
#include <axis_error.h>

#include <dispatcher_lib/dispatcher_lib_resource.h>
#include <dispatcher_lib/getopt.h>

//��������� �������
#include <adapter/httpd.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
//#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif

//������� ��������� ������ HTTP
//����:
// Status - ��������� �� ��������� ������� �������
//������:
// 0 - ������� ����������
// ����� 0 � ������ ������
int AdapterHTTPdStop( AdapterHTTPDStatusStruct_t *Status )
{

	if( !Status )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT  );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	DBGLOG( DISPATCHER_LIB_DEBUG__START );

    int time_count = 15;
    //������ ��������� ������� ���������� ������
    while( axis_ssl_httpi_server_status( &Status->Server,
								AXIS_SSL_HTTPI_SERVER_STOP_MODE ) == 0 && 
			time_count > 0 )
	{
		DBGLOG("%s: Can't stop http server at tcount=%d\n",__func__, time_count );
		sleep( 1 );
		time_count--;
    }

    if( time_count <= 0 )
	{
		DBGERR("%s: Can't stop http server\n", __func__ );
		return -AXIS_ERROR_INVALID_ANSED;
    }

	DBGLOG( DISPATCHER_LIB_DEBUG__DONE );

	return AXIS_NO_ERROR;
}

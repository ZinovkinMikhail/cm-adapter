//������� ������ http ������� ��� ������ ��������

//�����: ��������� ����� ��� "����"
//�����: i.podkolzin@ross-jsc.ru
//������: 2010.09.14 17:31

#include <axis_error.h>

#include <dispatcher_lib/dispatcher_lib_resource.h>
#include <dispatcher_lib/getopt.h>

//��������� �������
#include <adapter/resource.h>
#include <adapter/httpd.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif

static axis_ssl_wrapper_ex_sock gssl_sock; //NOTE - ���������� ���������� ��� ����������� ����������� ������������� SSL

//���������� ������� ������ �������
//����:
// Status - ����������� ���������� ��������� ������� 
//�������:
// 0 - ��� ������� ����������
// ����� 0 � ������ ������
int AdapterHTTPdStart( AdapterHTTPDStatusStruct_t *HTTPD,
				   CurrentNetworkSettingsStruct_t *CurrentNetwork,
				   int AdapterDevice, time_t *LastInterfaceQuire, int iQt )
{

	int ret = AXIS_NO_ERROR;
    int time_count = 15;

	if( !HTTPD )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	DBGLOG( DISPATCHER_LIB_DEBUG__START );

	//��� �������� ��������� - ���� ���������� �� ������� �� � ��� ���
	//����� ������� - ��� �� ����� ��� ������������� � ������
	//���� ������������ �������� �������� ����������� ��������� �������
	//��������, ssl ����� ��� ��� ������ ���

	//������ ��������� ������� ���������� ������
	while( axis_ssl_httpi_server_status( &HTTPD->Server,
								AXIS_SSL_HTTPI_SERVER_STOP_MODE ) == 0 &&
			time_count > 0 )
	{
		DBGLOG( "%s: Can't stop http server at tcount=%d\n", __func__, time_count );
		sleep( 1 );
		time_count--;
	}

	if( time_count <= 0 )
	{
		DBGERR( "%s: Can't stop http server\n", __func__ );
		return -AXIS_ERROR_INVALID_ANSED;
	}

	//�������� ��������� ���������� ����������
	HTTPD->Server.port_ssl			= CurrentNetwork->ControlPort;
																//���� ����������
	HTTPD->Server.port				= CurrentNetwork->UploadPort;		
																//���� ��������

	//�������� ������������� � ������������� SSL �� ���������� ����������
	//������������ ������ ������������ ��������� � ������ �������
	if( !gssl_sock.ctx )
	{
		DBGLOG( "%s: Try init global SSL\n", __func__ );

		ret = axis_ssl_wrapper_ex_init_ssl( 
				AXIS_INTERFACE_SSL_KEYFILE,
				AXIS_INTERFACE_SSL_KEYFILE_PASSWORD,
				AXIS_INTERFACE_SSL_CA_LIST,
				AXIS_INTERFACE_SSL_DHFILE,
				&gssl_sock );
		if( ret < 0 )
		{
			DBGERR( "%s: Can't init SSL\n", __func__ );
			return ret;
		}

		//��� ���� ���������������� �������������� ��� SSL
		ret = axis_ssl_wrapper_ex_threads_init();

		if( ret < 0 )
		{
			DBGERR( "%s: Can't init SSL for multithread work\n", __func__ );
			axis_ssl_wrapper_ex_close_ssl( &gssl_sock );
			return ret;
		}
	}

	//���� �� ���������������� SSL
	if( !HTTPD->Server.csock.ctx )
	{
		DBGLOG( "%s: Try init server SSL\n", __func__ );
		//�������������� ��� ������� ����� � ��� �� SSL
		memcpy( &HTTPD->Server.csock, &gssl_sock, sizeof( axis_ssl_wrapper_ex_sock ) );
	}

	//���������� �����
	if( CurrentNetwork->ClientMode )
	{
		HTTPD->Server.mode 		= AXIS_SSL_HTTPI_SERVER_PASSIVE_MODE;
		HTTPD->Server.host 		= CurrentNetwork->ServerHost;
	}
	else
	{
		HTTPD->Server.mode		= AXIS_SSL_HTTPI_SERVER_ACTIVE_MODE;
	}

	//������� �������� IP �� ����������
	HTTPD->Server.check_ip 		= AdapterHTTPDCheckIPCallback;

	//���������������� ���������
	HTTPD->Server.control_ssl 	= &HTTPD->HTTPSControl;
	HTTPD->Server.control 		= &HTTPD->HTTPControl;

	//���� ������ ��� qt ���������� ����� ����� ������������ ���� ��������
	if( iQt )
	{
		//��� ����
		HTTPD->Server.keep_alive		= AXIS_SSL_HTTPI_SERVER_KEEP_ALIVE_NONE;
		HTTPD->Server.getpasswd			= AdapterHTTPDGetPasswordCallbackQt;
		HTTPD->Server.getsupperpasswd	= AdapterHTTPDGetSPasswordCallback;
		HTTPD->Server.clients_count = 2;
	
		ret = AdapterHTTPdInterfaceInitControlsQt( &HTTPD->HTTPSControl,
										HTTPD->HTTPSPages,
										ADAPTER_HTTP__HTTPS_PAGE_COUNT,
										1 ); //��� SSL
		if( ret < 0 )
		{
			DBGERR( "%s: Can't init HTTPS control for Qt\n", __func__ );
			return ret;
		}

		ret = AdapterHTTPdInterfaceInitControlsQt( &HTTPD->HTTPControl,
										HTTPD->HTTPPages,
										ADAPTER_HTTP__HTTP_PAGE_COUNT,
										0 ); //��� �� SSL
		if( ret < 0 )
		{
			DBGERR( "%s: Can't init HTTP control for Qt\n", __func__ );
			return ret;
		}
	}

	else
	{
		//��� ����
		HTTPD->Server.keep_alive		= AXIS_SSL_HTTPI_SERVER_KEEP_ALIVE;
		HTTPD->Server.getpasswd			= AdapterHTTPDGetPasswordCallback;
		HTTPD->Server.getsupperpasswd	= AdapterHTTPDGetSPasswordCallback;
		HTTPD->Server.clients_count = 10;

		ret = AdapterHTTPdInterfaceInitControls( &HTTPD->HTTPSControl,
										HTTPD->HTTPSPages,
										ADAPTER_HTTP__HTTPS_PAGE_COUNT,
										1 ); //��� SSL
		if( ret < 0 )
		{
			DBGERR( "%s: Can't init HTTPS control\n",__func__ );
			return ret;
		}

		ret = AdapterHTTPdInterfaceInitControls( &HTTPD->HTTPControl,
										HTTPD->HTTPPages,
										ADAPTER_HTTP__HTTP_PAGE_COUNT,
										0 ); //��� �� SSL
		if( ret < 0 )
		{
			DBGERR("%s: Can't init HTTP control\n",__func__);
			return ret;
		}
	}

	//������� �����
	HTTPD->Server.tCount		= 0;
	HTTPD->LastTCount			= -1;

	//�������� ����� � ������
	axis_ssl_httpi_server_passive_init( &HTTPD->Server,
				DispatcherLibStringAdapterNameFromDevice( AdapterDevice),
				CurrentNetwork->SN,
				CurrentNetwork->Alias );

	//���������� �������� ��� ������
	ret = axis_ssl_httpi_server_start( &HTTPD->Server );
	if( ret < 0 )
	{
		DBGERR("%s: Can't start http server -%08x\n",__func__,-ret);
		return ret;
	}

	//��� ����� ��������� ���� ����� ��������� - ��� �� ������ ����� ����������
	sleep( 1 );

	//���������� �������� ��� ��� �����������
	ret = axis_ssl_httpi_server_status( &HTTPD->Server, 
									AXIS_SSL_HTTPI_SERVER_CONTINUE_MODE );

	if( ret < 0 )
	{
		DBGERR("%s: Server not started with code %d\n",__func__, ret );
		return ret;
	}
	
	LastInterfaceQuire = (time_t*)time( NULL );

	DBGLOG( DISPATCHER_LIB_DEBUG__DONE );

	return AXIS_NO_ERROR;
}

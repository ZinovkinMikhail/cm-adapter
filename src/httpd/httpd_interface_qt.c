//������� ������� �������������� ��������� - ������ ������� ������� �� ������������
// ��� ������ �������

//�����: ��������� ����� ��� "����"
//������: 2010.09.14 18:01
//�����: i.podkolzin@ross-jsc.ru

#include <axis_error.h>
#include <axis_httpi_tags.h>

#include <axis_ssl_httpi_download.h>
#include <axis_ssl_monitoring.h>

#include <dispatcher_lib/dispatcher_lib_resource.h>

//��������� �������
#include <adapter/httpd.h>
#include <adapter/interface.h>
#include <adapter/resource.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
//#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif



//���������� ������� ������������� ���������� ����������
//������ �� ��� ���� ����� SSL
//����:
// Control - ��������� �� ��������� �������� ��� ������� �����������
// Pages - ��������� �� ������ �������� � ������� ����� �������� ��������
// Count - ���������� ��������� ������� ������� - ����������� ����� �������
// ssl - ���� ��� ������ ������ �� ���������� ��������
//�������:
// 0 - ��� ��
// ����� 0 � ������ ������
int AdapterHTTPdInterfaceInitControlsQt( axis_ssl_httpi_control *Control, 
										axis_ssl_httpi_page_control *Pages, size_t Count,
										int ssl )
{

	if( !Control || !Pages || Count <= 0  )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT  );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	DBGLOG( DISPATCHER_LIB_DEBUG__START );
	
	axis_ssl_httpi_control_init( Control, Pages, Count ); //�� ����� ��� � C3E ���������


	//������� ������� ��� ������ ����� ���� �������� ����������� 
	//� ������ ������ - ��� �� �������
 	 axis_ssl_httpi_control_add( Control, AXIS_HTTPI_MAIN_PAGE,
											AdapterInterfaceDefaultTxtPrint );
	
	
	if( 1 /*ssl*/ ) //��� SSL ���� �����
	{
		//�������� �������
		axis_ssl_httpi_control_add( Control, ADPATER_HELPER__ADAPTER_INPUT_FILE_STATUS,
											AdapterInterfaceStatusTxtPrint );
	
		//������ ���� ���
		axis_ssl_httpi_control_add( Control, ADPATER_HELPER__ADAPTER_INPUT_FILE_MODE,
											AdapterInterfaceModeTxtPrint );
	
		//����� ������� ���
		axis_ssl_httpi_control_add( Control, ADPATER_HELPER__ADAPTER_INPUT_FILE_NETWORK,
											AdapterInterfaceNetworkTxtPrint );
											
		//�������� ���������� ����
		axis_ssl_httpi_control_add( Control, ADAPTER_INTERFACE_LOG_TXT_PAGE,
											AdapterInterfaceLogTxtPrint );
		//C������� �������
// 		axis_ssl_httpi_control_add( Control, ADPATER_HELPER__ADAPTER_INPUT_FILE_QCMD,
// 											 AdapterInterfaceQCmdTxtPrint );
		
	}
	
	//��� ��������� ��������
	
	//�������� � ���������� �� SSL �� ���� �����
 
    //�������� �������� ���������� ������
    axis_ssl_httpi_control_add( Control,    AXIS_SSL_DOWNLOAD_PAGE,
					    					AdapterInterfaceUploadPrint );

    //�������� �������� ���� ������
    axis_ssl_httpi_control_add( Control,    AXIS_SSL_DOWNLOAD_FULL_PAGE,
										    AdapterInterfaceUploadPrint );

    //�������� �������� �����������
    axis_ssl_httpi_control_add( Control,    AXIS_SSL_DOWNLOAD_NVELOP_PAGE,
					    					AdapterInterfaceUploadPrint );

    //���� ������ ��������� ����� �������� ����������� ����� http

    axis_ssl_httpi_control_add( Control,    AXIS_SSL_MONITORING_PAGE,
					    					AdapterInterfaceMonitoringPrint );
       
     axis_ssl_httpi_control_add( Control,    AXIS_SSL_DOWNLOAD_TEST_PAGE,
					    					AdapterInterfaceTestPrint );
	

	DBGLOG( DISPATCHER_LIB_DEBUG__DONE );

	return AXIS_NO_ERROR;
}


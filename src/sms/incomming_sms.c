//������� ��� ������ � ��������� SMS �� ������������

//�����: ��������� ����� ��� "����" ������
//�����: i.podkolzin@ross-jsc.ru
//������: 2016.09.14 16:31
//
//��� ����������� �� ���������� ������ � ������� SMS �� �������� ����� ������� ����� ��� "����" ������

#include <axis_error.h>

#include <adapter/sms.h>
#include <adapter/power.h>
#include <adapter/debug.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
//#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif

//----------------------------------------------------------------------------------------------------
//������� ���������� SMS - ����� ������� �����, �������� ��������� �����

static int net_lib_init_sms_head( struct smscontrol_s *head )
{
	if( !head )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	head->nextsms = NULL;

	head->prevsms = NULL;
	head->smscount = 0;
	head->smsnum = 0;

	if( pthread_mutex_init( &head->lock, NULL ) )
	{
		DBGERR( "%s:It wasn't possible to init mutex\n", __func__ );
		return -1;
	}

	return 0;
}

struct smscontrol_s *net_lib_find_last_recorder( struct smscontrol_s *head )
{
	if( !head )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return NULL;
	}

	struct smscontrol_s *last;

	if( head->smscount > 0 )
	{
		last = head->nextsms;

		while( last )
		{
			if( last->nextsms )
			{
				last = last->nextsms;
			}

			else
			{
				return last;
			}
		}
	}

	return head;
}

static void *net_lib_sms_alloc()
{
	return  malloc( sizeof( struct smscontrol_s ) );
}

static void  *net_lib_alloc_sms_node( struct smscontrol_s *head )
{
	if( !head )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return NULL;
	}

	struct smscontrol_s *last;


	last = net_lib_find_last_recorder( head );

	if( !last || last->nextsms )
	{
		DBGERR( "%s:It wasn't possible to find last recorder\n", __func__ );
		return NULL;
	}

	last->nextsms = net_lib_sms_alloc();

	if( !last->nextsms )
	{
		DBGERR( "%s:It wasn't possible to allocate noode\n", __func__ );
		return NULL;
	}

	memset( last->nextsms, 0, sizeof( struct smscontrol_s ) );

	last->nextsms->prevsms = last;
	last->nextsms->head = head;
	return last->nextsms;

}

//------------------------------------------------------------------------------------------------------
//��������������� �������

/**
 * @brief ������� ��������� ���������� �� ����� � ������ ����� ������� �� ��������
 *
 * @param StatusAdapter - ��������� �� ���������� ��������� �������
 * @param Phone - ��������� �� ����� � �������
 * @return int - 1 - ����� ���������� � ����, 0 - ����� �� ���������� � ����, ����� 0 � ������ ������
 **/
static int AdapterCheckSMSPhoneFromList( AdapterGlobalStatusStruct_t *StatusAdapter, const char *Phone )
{

	int i;

	if( !StatusAdapter || !Phone || !Phone[0] )
	{
		DBGERR( "%s: Invalid argument or empty phone\n", __func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	for( i=0; i<4; i++ )
	{
		//�� ���� ������� � �������, ���� �� ��������, �� ���� �����
		//+7����... == 8����
		//� Phone ����� ��������� �� �������� +7, � ��� � ������� ����� ���� � ��� � ���
		if( StatusAdapter->AdapterSettings.Block1.CDMASettings.Phone[ i ][0] )
		{
			//���� �����
			if( StatusAdapter->AdapterSettings.Block1.CDMASettings.Phone[ i ][0] == '+' &&
					Phone[0] == '+' )
			{
				//�� ���������� ��� ������ - ��� � �������
				if( !strcmp( StatusAdapter->AdapterSettings.Block1.CDMASettings.Phone[ i ], Phone ) )
				{
					//���� �����
					return 1;
				}
			}

			else if( StatusAdapter->AdapterSettings.Block1.CDMASettings.Phone[ i ][0] == '8' &&
					 Phone[0] == '+' && Phone[1] == '7' )
			{
				//���� ������� � ������� +7���� ������ 8���� - ���������� � ������ ������
				if( !strcmp( StatusAdapter->AdapterSettings.Block1.CDMASettings.Phone[ i ]+1, Phone+2 ) )
				{
					//���� �����
					return 1;
				}
			}

			else
			{
				//�� �� ������ ������� ���� �����, �� ����������� �������� �������
				DBGERR( "%s: Can't compare '%s' to '%s'\n", __func__,
						StatusAdapter->AdapterSettings.Block1.CDMASettings.Phone[ i ],
						Phone	);
			}
		}

	}

	return 0; //�� ���� �� �����

}


/**
 * @brief ������� ������������� SMS � �������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @param Text - ��������� �� ������ � ������� SMS
 * @param Phone - ��������� �� ������ � ������� ����������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
static int AdapterInitSMS( AdapterGlobalStatusStruct_t *Status, const char *Text, const char *Phone )
{
	if( !Status || !Text || !Phone )
	{
		DBGERR( "%s: Invalid argument\n", __func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	DBGLOG( "%s:%i: INIT SMS %i\n", __func__, __LINE__, Status->SMSControl.smscount );

	pthread_mutex_lock( &Status->SMSControl.lock );

	struct smscontrol_s *sms = ( struct smscontrol_s * )net_lib_alloc_sms_node( &Status->SMSControl );

	if( !sms )
	{
		DBGERR( "%s:It wasn't possible to allocate sms work\n",__func__ );
		pthread_mutex_unlock( &Status->SMSControl.lock );
		return -1;
	}

	if( pthread_mutex_init( &sms->lock, NULL ) )
	{
		DBGERR( "%s:It wasn't possible to init mutex\n", __func__ );
		pthread_mutex_unlock( &Status->SMSControl.lock );
		return -1;
	}

	Status->SMSControl.smscount++;

	sms->smsnum = Status->SMSControl.smscount;

	memcpy( sms->text, Text, sizeof( sms->text ) );
	memcpy( sms->number, Phone, sizeof( sms->number ) );

	sms->queued = time( NULL );

	pthread_mutex_unlock( &Status->SMSControl.lock );

	return 0;

}

//----------------------------------------------------------------------------------------------------------
//�������� �������

/**
 * @brief ������� ������� �������� �������� SMS � ������� �� ���������
 *
 * @param StatusAdapter - ��������� �� ���������� ��������� ������� ��������
 * @param SMS - ��������� �� ��������� SMS ���������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterAddNewSMSToReciveQueue( AdapterGlobalStatusStruct_t *StatusAdapter, sms_t *SMS )
{
	int ret;

	if( !StatusAdapter || !SMS )
	{
		DBGERR( "%s: Invalid argument\n", __func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	DBGLOG( "%s: Incomming SMS from '%s'\n", __func__, SMS->number );

	//�� ��������� SMS ������ �� ������� ������������������ � �������� ��������
	ret = AdapterCheckSMSPhoneFromList( StatusAdapter, SMS->number );

	if( ret < 0 )
	{
		DBGERR( "%s: Can't chaeck incomming SMS number from phone list\n", __func__ );
		return ret;
	}

	if( !ret )
	{
		DBGLOG( "%s: Incomming SMS Phone not conent in phone list '%s'\n", __func__, SMS->number );
		//�� ��� �� ����� ���� SMS ������� ��� ��������� - � ���� ���, �� ���� ���������� ���� �����
		if( StatusAdapter->GlobalDevice_t.dmdevice_powerupstatus != DISPATCHER_LIB_EVENT__WAKEUP )
		{
		 	DBGERR( "%s: WakeUp but recive wrong SMS\n", __func__ );
		}
		return 0; //��������� � ��������
	}

	//��������� SMS � �������
	ret = AdapterInitSMS( StatusAdapter, SMS->text, SMS->number );

	if( ret < 0 )
	{
		DBGERR( "%s: Can't init SMS\n", __func__ );
	}

	return ret;
}

/**
 * @brief ������� �������������� SMS ���������
 *
 * @param head - ��������� �� ��������� ������ SMS
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterInitSmsInterface( struct smscontrol_s *head )
{
	if( !head )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	return net_lib_init_sms_head( head );
}

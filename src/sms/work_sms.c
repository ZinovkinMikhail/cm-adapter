//������� ��������� �������� SMS � ���������� �����

//������: 2016.09.14 19:13
//�����: ��������� ����� ��� "����" ������
//�����: i.podkolzin@ross-jsc.ru

#include <axis_error.h>
#include <axis_string.h>

#include <dispatcher_lib/network.h>

#include <adapter/sms.h>
#include <adapter/power.h>
#include <adapter/network.h>
#include <adapter/debug.h>
#include <adapter/wifi.h>

#include <dmxxx_lib/dmxxx_calls.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif

/**
 * @brief ������� ������ �� �������� SMS Connect
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
static inline int AdapterWorkSMSConnect( AdapterGlobalStatusStruct_t *Status )
{
 	int ret = 0;
		int i;

	if( !Status )
	{
		DBGERR( "%s: Invalid argumnet\n", __func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	//��� �����-��1 � �����-��2 - ����������� ��������� ����� ����� ����� �������
	//��������, ���� �� ������ ���� - ������ ������� ������� � ����� � ������� �����������
	//�� ������������ ��� ����������� ������ ������� - ��� �������� ���������
	if( Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature && DM_DM_ENABLE_INCOMMING_SMS_CONNECT )
	{
		//1 - ����� ����� ������� � ����� ������� � ������ � ���������� � ��� ����, ��� ��� �����
		//� ������������� ��� ����� ���������� ��������
		//2 - ����� ������������ ������ ����������� �� ���� � ������������ ������ - �������������
		//����� ����� �������
		//3 - ������� � ������� ������� IP ������ � ������� ����������� � ������� ����� �������
		//------
		//������ ������� �����!!!

		LOG( "Incoming SMS command Connect\r\n" );

		for( i = 0; i < NET_NETWORK_INTERFACES_COUNT; i++ )
		{

			if( !Status->Net[i].netlib )
				continue;

			//�� ����� ��� ���� ����������, ������� ����� ������� �� ����������� �� ������� SMS
			if( Status->Net[i].net_lib_type == nettype_modem ||
				Status->Net[i].net_lib_type == nettype_lte			)
			{

				ret = AdapterNetworkGetConnectionStatus( &Status->Net[i] );

				if( ret < 0 )
				{
					DBGERR( "%s: Can't get status for network [%i]\n", __func__, i );
					continue;
				}


				if( Status->Net[i].status.status == DM_NET_CONNECTED )
				{
					//�� ���� ���������� - ������� ����� �������

					//�������� ���� ��� ����� ������� - � ��� �� ����������
					DispatcherLibEvent_t *oldevent = AdapterGlobalGetFirstEventbyType(
														 &Status->ExchangeEvents.Events,
														 DISPATCHER_LIB_EVENT__CONNECT,
														 0 );

					if( oldevent && !oldevent->Flag )
					{
						DBGLOG( "%s: Old 'Connect' event not sending to client. No Event add\n", __func__ );
					}

					else
					{

						ret = AdapterGlobalEventAddAndLogConnect( Status, "Incoming SMS command Connect",
														   Status->Net[i].status.ip,
														   Status->Net[i].status.level,
														   Status->Net[i].status.band );

						if( ret < 0 )
						{
							DBGERR( "%s: Can't add new connect event\n", __func__ );
						}
					}
				}

				else
				{
					//�� �� ���������� - ������ ����� ���������� ������� � ��� ����� ���������������� SMS
					//��� ��� ����� ��������� �������� ������������
					DBGLOG( "%s: Current not connected. Connected letar....\n", __func__ );
					DBGLOG( "%s: %i type=%d status=%d\n", __func__, i, Status->Net[i].net_lib_type,  Status->Net[i].status.status );

					//����������� ������� �� ����������� � ����
					Status->AdapterNeedLTECommand = DM_NET_COMMAND_CONNECT;
				}
			}
		}

	}

	//��� ������ ����� ��������� � ������� ����� ���� ��� �� ���
	else
	{
		DBGERR( "%s: Device not support 'Connect' SMS command\n", __func__ );
		//�� �� ������������ � �� ���� - � ����� ��
	}
	
	return ret;
}

/**
 * @brief ������� ��������� �������� ������� SMS Reboot
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
static inline int AdapterWorkSMSReboot( AdapterGlobalStatusStruct_t *Status )
{
	int ret = 0;

	if( !Status )
	{
		DBGERR( "%s: Invalid argument\n", __func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	if( Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature | DM_DM_ENABLE_INCOMMING_SMS_REBOOT )
	{

		INFO_LOG( 0, "Incoming SMS command Reboot" );

		//������ �������� ������������
		Status->AdapterNeedReboot = 1;
	}

	//��� ������ ����� ��������� � ������� ����� ���� ��� �� ���
	else
	{
		DBGERR( "%s: Device not support 'Reboot' SMS command\n", __func__ );
		//�� �� ������������ � �� ���� - � ����� ��
	}

	return ret;
}

/**
 * @brief ������� ��������� ������� SMS Sleep
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
static inline int AdapterWorkSMSSleep( AdapterGlobalStatusStruct_t *Status )
{
	int ret = 0;

	if( !Status )
	{
		DBGERR( "%s: Invalid argument\n", __func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	//��� �����-��1 � �����-��2 �� ����� ������� ����� � ����� ������
	//��� ����������� �� ��������� ������� �������� - ��� ����� ��������� ����������� SMS
	if( Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature | DM_DM_ENABLE_INCOMMING_SMS_SLEEP )
	{

		INFO_LOG( 0, "Incoming SMS command Sleep" );

		//������ �������� �����������
		Status->AdapterNeedSleep = 1;
	}

	//��� ������ ����� ��������� � ������� ����� ���� ��� �� ���
	else
	{
		DBGERR( "%s: Device not support 'Sleep' SMS command\n", __func__ );
		//�� �� ������������ � �� ���� - � ����� ��
	}

	return ret;
}

/**
 * @brief �������, ������� ������������ �������� SMS ���� Band=XXX
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @param Text - ��������� �� ����� � ��������� SMS
 * @return int
 */
static int AdapterWorkSMSBand( AdapterGlobalStatusStruct_t *Status, const char *Text )
{
	int ret = 0;

	if( !Status || !Text )
	{
		DBGERR( "%s: Invalid argument\n", __func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	if( Text[4] != '=' )
	{
		DBGERR( "%s: Invalid text for Band '%s'\n", __func__, Text );
		return -AXIS_ERROR_INVALID_VALUE;
	}

	Text += 5;

	if( Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature | DM_DM_ENABLE_INCOMMING_SMS_BAND )
	{
		if( !strncasecmp( Text, DEV_STATUS_STATUS_VALUE_BAND_AUTO, strlen( DEV_STATUS_STATUS_VALUE_BAND_AUTO ) ) )
		{
			Status->AdapterNeedLTECommand = DM_NET_COMMAND_SET_BAND_AUTO;
			INFO_LOG( 0, "SMS command LTE band 'Auto'" );
		}

		else if( !strncasecmp( Text, DEV_STATUS_STATUS_VALUE_BAND_LTE, strlen( DEV_STATUS_STATUS_VALUE_BAND_LTE ) ) )
		{
			Status->AdapterNeedLTECommand = DM_NET_COMMAND_SET_BAND_LTE;
			INFO_LOG( 0, "SMS command LTE band 'LTE'" );
		}

		else if( !strncasecmp( Text, DEV_STATUS_STATUS_VALUE_BAND_3G, strlen( DEV_STATUS_STATUS_VALUE_BAND_3G ) ) )
		{
			Status->AdapterNeedLTECommand = DM_NET_COMMAND_SET_BAND_3G;
			INFO_LOG( 0, "SMS command LTE band '3G'" );
		}

		else
		{
			DBGERR( "%s: Invalid SMS band command value '%s'\n", __func__, Text );
			//���� �� ���� - ����� ��� ����� � �����
		}
	}

	else
	{
		DBGERR( "%s: Device not support 'Band' SMS command.\n", __func__ );
		//�� �� ������������ � �� ���� - � ����� ��
	}

	return ret;
}

/**
 * @brief ������� ��������� �������� SMS WiFiOn & WiFiOff
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @param flag - ���� �������� SMS - �������� ��� ���������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
static inline int AdapterWorkSMSWiFi( AdapterGlobalStatusStruct_t *Status, int flag )
{
	int ret = 0;

	if( !Status )
	{
		DBGERR( "%s: Invalid argument\n", __func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	if( Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature | DM_DM_ENABLE_INCOMMING_SMS_WIFI )
	{

		ret = AdapterWiFiPower( &Status->GlobalDevice_t, flag );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't power %s WiFi USB adapter\n", __func__, flag ? "on" : "off" );
			INFO_LOG( ret, "Incoming SMS command WiFi %s", flag ? "on" : "off" );
		}

		else
		{
			//������� ���������
			char buffer[256];
			snprintf( buffer, sizeof( buffer ), "Incoming SMS command WiFi %s", flag ? "on" : "off" );
			buffer[ sizeof( buffer ) - 1 ] = 0x00;
			
			ret = AdapterGlobalEventAddAndLog( Status, buffer,
											   DISPATCHER_LIB_EVENT_WIFI,
											   flag, 0 );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't add new connect event\n", __func__ );
			}
		}
		
	}

	else
	{
		DBGERR( "%s: Device not support 'WiFi' SMS command. Flag=%d\n", __func__, flag );
		//�� �� ������������ � �� ���� - � ����� ��
	}

	return ret;;
}

/**
 * @brief ������� ������ �� �������� SMS
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @param Phone - ��������� �� ������ � ������� ��������
 * @param Text - ��������� �� ������ � ������� ���������
 * @return int
 **/
static int AdapterWorkSMS( AdapterGlobalStatusStruct_t *Status, const char *Phone, const char *Text )
{
	int ret = 0;

	//��� �� ������ �������� SMS � ������ ������, �� ��� � ��� �������
	if( !Status || !Phone || !Text )
	{
		DBGERR( "%s: Invalid argument\n", __func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	DBGLOG( "%s: Work SMS '%s' from phone '%s'\n", __func__, Text, Phone );

	//������ ������� �������
	while( Text[0] )
	{
		if( Text[0] == ' ' )
		{
			Text++;
		}

		else
		{
			break;
		}
	}

	if( !Text[0] )
	{
		DBGERR( "%s: Recive empty text SMS\n", __func__ );
		return -AXIS_ERROR_INVALID_VALUE;
	}

	if( !strncasecmp( Text, CM_ADAPTER_SMS_COMMAND__CONNECT, strlen( CM_ADAPTER_SMS_COMMAND__CONNECT ) ) )
	{
		DBGLOG( "%s: Try work SMS command connect\n", __func__ );
		ret = AdapterWorkSMSConnect( Status );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't work from SMS 'Connect'\n", __func__ );
		}

		else
		{
			//���� �� ���������� �� SMS ������ ������ ������, ��� �� ����� ���� �������� 
			//��� ��������� ���������
			if( Status->GlobalDevice_t.dmdevice_powerupstatus == DISPATCHER_LIB_EVENT__WAKEUP )
			{
				Status->GlobalDevice_t.dmdevice_powerupstatus = DISPATCHER_LIB_EVENT__NONE; //��� �� ������ ����� SMS
			}
		}
	}

	else if( Status->GlobalDevice_t.dmdevice_powerupstatus != DISPATCHER_LIB_EVENT__WAKEUP )
	{

		//�� ���������� �� �� SMS - ����� ������������ ����� �������
		if( !strncasecmp( Text, CM_ADAPTER_SMS_COMMAND__REBOOT, strlen( CM_ADAPTER_SMS_COMMAND__REBOOT ) ) )
		{
			DBGLOG( "%s: Try work SMS command reboot\n", __func__ );
			ret = AdapterWorkSMSReboot( Status );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't work from SMS 'Reboot'\n", __func__ );
			}
		}

		else if( !strncasecmp( Text, CM_ADAPTER_SMS_COMMAND__SLEEP, strlen( CM_ADAPTER_SMS_COMMAND__SLEEP ) ) )
		{
			DBGLOG( "%s: Try work SMS command sleep\n", __func__ );
			ret = AdapterWorkSMSSleep( Status );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't work from SMS 'Sleep'\n", __func__ );
			}
		}

		else if( !strncasecmp( Text, CM_ADAPTER_SMS_COMMAND__WIFI_ON, strlen( CM_ADAPTER_SMS_COMMAND__WIFI_ON ) ) )
		{
			DBGLOG( "%s: Try work SMS command WiFi On\n", __func__ );
			ret = AdapterWorkSMSWiFi( Status, 1 );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't work from SMS 'WiFi On'\n", __func__ );
			}
		}

		else if( !strncasecmp( Text, CM_ADAPTER_SMS_COMMAND__WIFI_OFF, strlen( CM_ADAPTER_SMS_COMMAND__WIFI_OFF ) ) )
		{
			DBGLOG( "%s: Try work SMS command WiFi Off\n", __func__ );
			ret = AdapterWorkSMSWiFi( Status, 0 );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't work from SMS 'WiFi Off'\n", __func__ );
			}
		}

		else if( !strncasecmp( Text, CM_ADAPTER_SMS_COMMAND__BAND, strlen( CM_ADAPTER_SMS_COMMAND__BAND ) ) )
		{
			DBGLOG( "%s: Try work SMS command Band\n", __func__ );
			ret = AdapterWorkSMSBand( Status, Text );

			if( ret < 0 )
			{
				DBGERR( "%s: Can't work from SMS '%s'\n", __func__, Text );
			}
		}

		else
		{
			DBGERR( "%s: SMS command '%s' not supported\n", __func__, Text );
			return 0; //�� �������������� � �� ���� - � ����� ��
		}
	}

	else
	{
		DBGERR( "%s: WakeUp from SMS only 'Connect' command supported (invalid '%s')\n", __func__, Text );
		return 0; //�� �������������� � �� ���� - � ����� ��
	}

	Status->LastInterfaceQuire = time( NULL );
	
	return ret;
}

/**
 * @brief �������, ������� ��������� ������ �� ������� SMS
 *
 * @param sms - ��������� �� ������� SMS
 * @return int
 **/
static int net_lib_free_node( struct smscontrol_s *sms )
{

	if( sms )
	{
		if( !sms->prevsms )
		{
			DBGERR( "%s:%i: Attempt to free head node\n", __func__, __LINE__ );
			return -1;
		}

		//������� �����
		if( sms->nextsms != NULL )
		{
			if( sms->prevsms )
			{
				sms->prevsms->nextsms = sms->nextsms;
				sms->nextsms->prevsms = sms->prevsms;
			}

		}

		else
		{
			sms->prevsms->nextsms = NULL;
		}

		free( ( void * )sms );
		sms = NULL;
	}

	return 0;

}

/**
 * @brief �������, ������� ��������� ���� �� ������������� �������� ��� �����������
 *
 * @param Status - ��������� �� ���������� ��������� ������� ��������
 * @return int - 0 ��� ���������, 1 - ���� ���� �� ���� �������, ����� 0 � ������ ������
 **/
int AdapterPhoneCheck( AdapterGlobalStatusStruct_t *Status )
{
	int i;

	if( !Status )
	{
		DBGERR( "%s: Invalid Argument\n", __func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	for( i=0; i< AXIS_SOFTADAPTER_CDMA_PHONE_COUNT; i++ )
	{
		if( Status->AdapterSettings.Block1.CDMASettings.Phone[ i ][ 0 ] )
		{
			//��� �� ���� - �� ������ ������
			if( DispatcherLibPhoneCheck( Status->AdapterSettings.Block1.CDMASettings.Phone[ i ] ) )
			{
				//����� ���������
				DBGLOG( "%s: Have a correct phone '%s' at %d\n", __func__,
						Status->AdapterSettings.Block1.CDMASettings.Phone[ i ], i );
				return 1; //���� ���� �� ���� ���������� �����
			}

			else
			{
				DBGERR( "%s: Have a wrong phone '%s' at %d\n", __func__,
						Status->AdapterSettings.Block1.CDMASettings.Phone[ i ], i );
			}
		}
	}

	return 0; //��� �� ������ ����������� ��������
}


/**
 * @brief ������� ��������� �������� SMS
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @param SMSHead - ��������� �� ������ SMS
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterWorkNextSMS( AdapterGlobalStatusStruct_t *Status, smscontrol_t *SMSHead )
{

	int ret;

	if( !Status || !SMSHead )
	{
		DBGERR( "%s: Invalid argument\n", __func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	pthread_mutex_lock( &SMSHead->lock );

	if( SMSHead->nextsms == NULL )
	{
		//��� �������� - ��� �� ����� SMS ��������
		DBGLOG( "%s: No new SMS in buffer\n", __func__ );
		pthread_mutex_unlock( &SMSHead->lock );
		return 0;
	}

	if( SMSHead->nextsms->status < 0 )
	{
		//��� �������� SMS � ������� ��� ���� ������ - ��������� ����, ����� ��� �� �����������
		DBGLOG( "%s: Previus work SMS have a problem... Continue leter\n", __func__ );
		SMSHead->nextsms->status = 0;
		pthread_mutex_unlock( &SMSHead->lock );
		return 0;
	}

	ret = AdapterWorkSMS( Status, SMSHead->nextsms->number, SMSHead->nextsms->text );

	if( ret < 0 )
	{
		DBGERR( "%s: Can't work SMS from phone '%s'\n", __func__, SMSHead->nextsms->number );
		SMSHead->nextsms->status = -1;
		pthread_mutex_unlock( &SMSHead->lock );
		return ret;
	}

	DBGLOG( "%s: SMS from phone '%s' done\n", __func__, SMSHead->nextsms->number );

	ret = net_lib_free_node( SMSHead->nextsms );

	if( ret < 0 )
	{
		DBGERR( "%s: Can't free SMS\n", __func__ );
		pthread_mutex_unlock( &SMSHead->lock );
		return ret;
	}

	SMSHead->smscount--;

	pthread_mutex_unlock( &SMSHead->lock );

	return ret;
}

/**
 * @brief ������� ������ SMS ��� �������� - ����� ������
 *
 * @param b - ��������� �� ����� ��� ������
 * @param bsize - ������ ������� ������
 * @param Event - �������, ������� �� ��������
 * @param Sn - �������� ����� ��������
 * @param SnSize - ������ ��������� ������
 * @return size_t - ���������� ���� ���������� � �����
 */
size_t AdapterEventPrintSMS( char *b, size_t bsize, DispatcherLibEvent_t *Event,
					uint8_t *Sn, size_t SnSize )
{
	int cc=0;
	int lcc = 0;
	size_t size = bsize;
	char Buffer[ 64 ];

	if( !Event || !b || !size || !Sn || !SnSize )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return 0;
	}

	if( !Event->Type )
	{
		DBGERR("%s: Invalid event type %08x\n",__func__, Event->Type );
		return 0;
	}

	if( bsize <= SnSize * 2 + 2 )
	{
		return 0;
	}

	//������ ��������� �������� �����
	axis_sprint_buff( Buffer, sizeof( Buffer ), Sn, SnSize );

	lcc = axis_sprintf_t(
					b+cc, size,
					DISPATCHER_LIB_SMS_NAME_EVENTS_SN,
					Buffer );


	cc		+= lcc;
	size	-= lcc;
	if( size <= 0 ) goto end;

	//������ ����!!!!
	system_time_print_ini( Buffer, sizeof( Buffer ), Event->Data );

	lcc = axis_sprintf_t(
					b+cc, size,
					DISPATCHER_LIB_SMS_NAME_EVENTS_DATA,
					Buffer );

	cc		+= lcc;
	size	-= lcc;
	if( size <= 0 ) goto end;


	//������ ��� �������
	lcc = axis_sprintf_t(
					b+cc, size,
					DISPATCHER_LIB_SMS_NAME_EVENTS_TYPE,
				    DispatcherLibEventGetEventName(  Event->Type ) );

	cc	   += lcc;
	size   -= lcc;
	if( size <= 0 ) goto end;

	//�������� �������

	//��� ����������� ��������� ������������� � �����
	if( Event->Type != DISPATCHER_LIB_EVENT__CONNECT )
	{
		lcc = axis_sprintf_d(
					b+cc, size,
					DISPATCHER_LIB_SMS_NAME_EVENTS_VALUE,
				    Event->Value );

		cc	   += lcc;
		size   -= lcc;
		if( size <= 0 ) goto end;

		//ATTENTION -- �������� �������
		//�� ����� ������ �� ����� ���������� � SMS ��������� �������
		//���������� ������, �� ������ ��� ���������
		if( Event->Type == DISPATCHER_LIB_EVENT__WAKEUP ||
			Event->Type == DISPATCHER_LIB_EVENT__POWER_UP ||
			Event->Type == DISPATCHER_LIB_EVENT__TIMER ||
			Event->Type == DISPATCHER_LIB_EVENT__REBOOT ||
			Event->Type == DISPATCHER_LIB_EVENT__RECORDER_FULL_NAND )
		{
			lcc = axis_sprintf_d( b+cc, size,
					DISPATCHER_LIB_SMS_NAME_EVENTS_NAND,
					Event->Value2 ); //TODO - � ����� 2???

			cc	   += lcc;
			size   -= lcc;
			if( size <= 0 ) goto end;
		}

	}
	else
	{
		int16_t Level = (Event->Value2 & 0x0000FFFF);
		uint32_t Band = Event->Value2 >> 16;
		const char *BandStr = AdapterLTEModemBandToString( Band );

		//�� ������ � ������ � � IP �����
		axis_sprint_ip( Buffer, (unsigned int)Event->Value );
		lcc = axis_sprintf_t(
					b+cc, size,
					DISPATCHER_LIB_SMS_NAME_EVENTS_VALUE,
				    Buffer );
		cc	   += lcc;
		size   -= lcc;
		if( size <= 0 ) goto end;

		lcc = axis_sprintf_d(
					b+cc, size,
					DISPATCHER_LIB_SMS_NAME_EVENTS_SIGNAL,
				    Level );
		cc	   += lcc;
		size   -= lcc;
		if( size <= 0 ) goto end;

		if( BandStr )
		{
			lcc = axis_sprintf_t( b+cc, size,
									DISPATCHER_LIB_SMS_NAME_EVENTS_BAND,
									BandStr );
			cc	   += lcc;
			size   -= lcc;
			if( size <= 0 ) goto end;

		}
	}

	//��������� ���
	if( Event->IntACCVoltage )
	{
		//���������� ���������� ���
		lcc = axis_sprintf_f(
					b+cc, size,
					DISPATCHER_LIB_SMS_NAME_EVENTS_INT_ACC,
					(float)Event->IntACCVoltage/(float)1000 );

		cc	   += lcc;
		size   -= lcc;
		if( size <= 0 ) goto end;

		//����������� ���������� ���
		lcc = axis_sprintf_d(
					b+cc, size,
					DISPATCHER_LIB_SMS_NAME_EVENTS_INT_TEMP,
					Event->IntTemperatura );

		cc	   += lcc;
		size   -= lcc;
		if( size <= 0 ) goto end;

	}

	//������� ���
	if( Event->ExtACCVoltage )
	{
		//���������� �������� ���
		lcc = axis_sprintf_f(
					b+cc, size,
					DISPATCHER_LIB_SMS_NAME_EVENTS_EXT_ACC,
				    (float)Event->ExtACCVoltage/(float)1000 );

		cc	   += lcc;
		size   -= lcc;
		if( size <= 0 ) goto end;

		//����������� �������� ���
		lcc = axis_sprintf_d(
					b+cc, size,
					DISPATCHER_LIB_SMS_NAME_EVENTS_EXT_TEMP,
					Event->ExtTemperatura );

		cc	   += lcc;
		size   -= lcc;
		if( size <= 0 ) goto end;
	}

	//������� ����������
	lcc = axis_sprintf_f(
				b+cc, size,
				DISPATCHER_LIB_SMS_NAME_EVENTS_INPUT_VOLTAGE,
				(float)Event->InputVoltage/(float)1000 );

	cc	   += lcc;
	size   -= lcc;
	if( size <= 0 ) goto end;

end:

    b[ cc ] = 0x00;
    return cc;
}

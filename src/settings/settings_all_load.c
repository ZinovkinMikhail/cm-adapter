//���������� ����� ��������


//���������� �������

//������������ �������
#include <axis_error.h>
#include <axis_ifconfig.h>
#include <axis_time.h>
#include <dispatcher_lib/dispatcher_lib_resource.h>
#include <dispatcher_lib/settings.h>
#include <dispatcher_lib/requests.h>
#include <dispatcher_lib/printf.h>
#include <dmxxx_lib/dmxxx_calls.h>

//��������� �������
#include <adapter/settings.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>

#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif

/**
 * @brief ������� �������� ������� ��������, ��������� ������, ���������� ���� ���������� ���������� �� ������������
 * @brief ������ ����������
 *
 * @param NetSettings - ��������� �� ��������� ������� ��������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
static int AdapterSettingsCheckForNetSettingLocal( SoftAdapterExchangeSettingsStruct_t *NetSettings )
{
	if( !NetSettings )
	{
		DBGERR( "%s:Invalid argument\n",__func__ );
		return -1;
	}

	if( ( NetSettings->Block0.AdapterSettings.IPAddress == 0xFFFFFFFF || !NetSettings->Block0.AdapterSettings.IPAddress ) &&
		( NetSettings->Block0.AdapterSettings.Connect & AXIS_SOFTADAPTER_CONNECT__IP_STATIC ) )
	{
		DBGERR( "%s: Invalid Ip Address %08x at static IP \n",__func__, NetSettings->Block0.AdapterSettings.IPAddress );
		return -1;
	}

	if( !NetSettings->Block0.AdapterSettings.ControlPort
			|| NetSettings->Block0.AdapterSettings.ControlPort == 0xffff
			|| NetSettings->Block0.AdapterSettings.UploadPort == 0xffff
			||!NetSettings->Block0.AdapterSettings.UploadPort
			|| NetSettings->Block0.AdapterSettings.MonitoringPort == 0xffff
			||!NetSettings->Block0.AdapterSettings.MonitoringPort
	  )
	{
		DBGERR( "%s: Invalid ports \n",__func__ );
		return -1;
	}

	if( NetSettings->Block0.AdapterSettings.Connect & AXIS_SOFTADAPTER_CONNECT__CLIENT_MODE )
	{
		if( NetSettings->Block0.AdapterSettings.ServerIPAddress == 0xFFFFFFFF ||
				!NetSettings->Block0.AdapterSettings.ServerIPAddress )
		{
			DBGERR( "%s:Invalid server ip address \n",__func__ );
			return -1;
		}
	}

	if( axis_is_empty( NetSettings->Block0.AdapterSettings.HWAddress ) )
	{
		DBGERR( "%s:Invalid server mac address \n",__func__ );
		return -1;
	}

	return 0;
}

/**
 * @brief ������� �������� ������� ��������
 *
 * @param aDev - ��������� �� ��������� ����������
 * @param NetSettings - ��������� �� ��������� ������� ��������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterSettingsCheckForNetSetting( dmdev_t *aDev, SoftAdapterExchangeSettingsStruct_t *NetSettings )
{
	if( !aDev || !NetSettings )
	{
		DBGERR( "%s: Invalid argument\n",__func__ );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	//���� ���������� ���������� ������������ �������� ������� �������� - �������� ��
	//� ��������� ������ ��������� ������ �������
	if( iDMXXXInitCheckSettings( aDev ) )
	{
		return DMXXXInitCheckSettings( aDev, NetSettings );
	}

	else
	{
		return AdapterSettingsCheckForNetSettingLocal( NetSettings );
	}
}

//������� �������� ��������� RTC �� ������������ - ������ �����
//� � ������ ������ ������������� ���������� ����
//����:
// pstDataTime - ��������� �� ��������� ����
//�������:
// 0 - ������ ���������
// ���� 0 � ������ ������
static int system_time_verify_dow_time( struct_data *pstDataTime )
{
	int nResult = 0;

	//��������� ����
	if( system_time_hexd_to_d( pstDataTime->hour ) > 23 )
	{
		DBGERR( "%s: Invalid hour in date %02x (%d). Set default\n", __func__, pstDataTime->hour, system_time_hexd_to_d( pstDataTime->hour ) );
		pstDataTime->hour = 0x00;
		nResult = -1;
	}

	//��������� ������
	if( system_time_hexd_to_d( pstDataTime->min ) > 59 )
	{
		DBGERR( "%s: Invalid min in date %02x (%d). Set default\n", __func__, pstDataTime->min, system_time_hexd_to_d( pstDataTime->min ) );
		pstDataTime->min = 0x00;
		nResult = -1;
	}

	//��������� �������
	if( system_time_hexd_to_d( pstDataTime->sec ) > 59 )
	{
		DBGERR( "%s: Invalid sec in date %02x (%d). Set default\n", __func__, pstDataTime->sec, system_time_hexd_to_d( pstDataTime->sec ) );
		pstDataTime->sec = 0x00;
		nResult = -1;
	}

	return nResult;
}


static int AdapterSettingsCheckForNetTimers(AdapterGlobalStatusStruct_t *Status)
{
	if(!Status)
	{
		DBGERR("%s:Invalid Argument\n",__func__);
		return -1;
	}

	if(Status->GlobalDevice_t.dmdev_timer.dmctrl_feature & DM_TIMER_TYPE_ADAPTER)
	{
		int blocks = MEMORY_BUFFER_SIZE /
			sizeof( SoftRecordeExchangeExtBlockStruct_t );
		int timerscount =  (AXIS_DISPATCHER_EXCHANGE__MESSAGE_SIZE /
															sizeof(axis_timer_t)) -
							((AXIS_DISPATCHER_EXCHANGE_EXT__DATA_WORD_OFFSET << 1)/
															sizeof(axis_timer_t)) -
							1;
		int i =0;
		int j =0;
		for(j =0;j<blocks;j++)
		{
			for(i =0;i<timerscount;i++)
			{
				switch(Status->TimersSettingsNetAxis.Blocks[j].TimersData.Timers[i].stop.reserv_0)
				{
					case 0x00:
					{
						DBGLOG("%s:%i: Timer %i not configured pass\n",__func__,__LINE__, i );
						continue;
					}
					case TIMER_FLAG_DISABLE:
					{
						DBGLOG("%s:%i: Timer %i disabled Ok\n",__func__,__LINE__, i );
						continue;
					}
					case TIMER_FLAG_EXPIRED:
					{
						DBGLOG("%s:%i: Timer %i expired ok Ok\n",__func__,__LINE__, i );
						continue;
					}
					case TIMER_FLAG_DONE:
					{
						DBGLOG("%s:%i: Timer %i done ok Ok\n",__func__,__LINE__, i );
						continue;
					}
					case TIMER_FLAG_WORKING:
					{
						DBGLOG("%s:%i: Timer %i working Ok \n",__func__,__LINE__, i );
						continue;
					}
					case TIMER_FLAG_READY:
					{
							//��������� � ������� ������� ���� ��������� ��������
							if( Status->TimersSettingsNetAxis.Blocks[j].TimersData.Timers[i].start.dow )
							{
								//��������� ����������� ������� - �������� ���������� dow
								if( Status->TimersSettingsNetAxis.Blocks[j].TimersData.Timers[i].start.dow !=
										Status->TimersSettingsNetAxis.Blocks[j].TimersData.Timers[i].stop.dow )
								{
									DBGERR( "%s:%i: Day of weak timer %i not equal start stop dow\n", __func__, __LINE__, i );
									return -1;
								}

								//��������� ����������� ������� - �������� ������������ �������
								if( !system_time_verify_dow_time( &Status->TimersSettingsNetAxis.Blocks[j].TimersData.Timers[i].start ) &&
										!system_time_verify_dow_time( &Status->TimersSettingsNetAxis.Blocks[j].TimersData.Timers[i].stop ) )
								{
									DBGLOG( "%s:%i: Day of weak timer %i ready Ok\n", __func__, __LINE__, i );
									continue;
								}

								DBGERR( "%s:%i: Day of weak timer %i invalid times on ready flag\n",__func__, __LINE__, i );
								return -1;
							}

							else
							{
								//��������� ����������� ������� - ���� � ����� ������ ���� ���������
								if( !system_time_verify_current_time( &Status->TimersSettingsNetAxis.Blocks[j].TimersData.Timers[i].start )
										&& !system_time_verify_current_time( &Status->TimersSettingsNetAxis.Blocks[j].TimersData.Timers[i].stop ) )
								{
									DBGLOG( "%s:%i: Normal Timer %i ready Ok \n",__func__,__LINE__, i );
									continue;
								}

								DBGERR( "%s:%i: Timer %i Invalid date or times on ready flag\n",__func__, __LINE__, i );
								return -1;
							}
					}
					default:
					{
						DBGERR("%s:Invalid flag\n",__func__);
						return -1;
					}

				}

				
			}
		}

	}
	else if(Status->GlobalDevice_t.dmdev_timer.dmctrl_feature & DM_TIMER_TYPE_DSP)
	{

		DBGLOG("%s: %i DSP timer check not supported yet\n",__func__,__LINE__);
		
	}

	return 0;

	
}

//������� �������� ���� ��������� � ��������
//����:
// iResetFlag - ���� ���� ��� �� ���� ��������� � ���������
// Status - ���������� ��������� ������� ��������
//�������:
// 0 - ��� ������
// ����� 0 � ������ ������
int AdapterSettingsAllLoad( int iResetFlag, AdapterGlobalStatusStruct_t *Status )
{

	int ret = AXIS_NO_ERROR;

	if ( !Status )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	DBGLOG( "%s: Start, iResetFlag = %d\n", __func__, iResetFlag );
	DBGLOG("%s: %i field\n",__func__,__LINE__);
	//������� ���� ��� �� ���� ��������� �� ������ ���������� ������
	//��� �� �� ��� ������ ������� �������� �������� �� ��
resave:
	DBGLOG("%s: %i field\n",__func__,__LINE__);

	if ( iResetFlag )
	{
		ret = AdapterSettingsSaveAdapterSettingsDefault( Status );
		if ( ret < 0 )
		{
			
			DBGERR( "%s: Can't save adapter settings default\n", __func__ );
			return ret;
		}
	}
		DBGLOG("%s: %i field\n",__func__,__LINE__);

		ret = AdapterSettingsLoadAdapterTimers( Status ) ;

		if ( ret  < 0 )
		{
			DBGERR( "%s: It wasn't possible to load timers\n", __func__ );
			return ret;
		}
		DBGLOG("%s: %i field\n",__func__,__LINE__);

		ret = AdapterSettingsLoadAdapterSettings( Status );

		if ( ret < 0  )
		{	DBGLOG("%s: %i field\n",__func__,__LINE__);
	DBGLOG("%s: %i field\n",__func__,__LINE__);

			if (ret == -AXIS_ERROR_INVALID_VALUE)
			{
				DBGERR("%s:Invalid data, settings may be corrupted restart\n",__func__);
				iResetFlag = 1;
				goto resave;
			}
			DBGERR( "%s: Can't load adapter settings\n", __func__ );
			if(ret == -AXIS_ERROR_INVALID_VALUE && !iResetFlag && Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature & DM_DM_ENABLE_USB_FIRMWARE_SETTING_IN_RECORDER)
			{
				//������ ���� ������ ������� � ����� ���� ��� ��� � ���������� ������ ������ � �����������
				DBGERR("%s:Settings may be nulled first start set default\n",__func__);
				iResetFlag = 1;
				goto resave;
				
			}
			return ret;
		}

		DBGLOG("%s: %i field\n",__func__,__LINE__);
	
		
		//��� �� ��� ������� - ������� ���� ������ �� ������ ������� ��� �����
	//	Status->SleepTime = Status->AdapterSettings.Block0.AdapterSettings.Sleep;

	if( AdapterSettingsCheckForNetSetting( &Status->GlobalDevice_t, &Status->AdapterSettings ) )
	{	DBGLOG("%s: %i field\n",__func__,__LINE__);

		DBGERR("%s:Invalid adapter net settings\n",__func__);
		if(!iResetFlag)
		{
			iResetFlag = 1;
			goto resave;
		}
		return  -AXIS_ERROR_INVALID_VALUE;
	}
	DBGLOG("%s: %i field\n",__func__,__LINE__);

	if(AdapterSettingsCheckForNetTimers(Status))
	{	DBGLOG("%s: %i field\n",__func__,__LINE__);

		DBGERR("%s:Invalid adapter net timers\n",__func__);
		if(Status->GlobalDevice_t.dmdev_timer.dmctrl_feature & DM_TIMER_TYPE_ADAPTER)
		{
			memset(&Status->TimersSettingsNetAxis,0,sizeof(Status->TimersSettingsNetAxis));
			if(AdapterSettingsSaveNetTimersSettings(Status ) < 0)
			{
				return  -AXIS_ERROR_INVALID_VALUE;
			}
		}	DBGLOG("%s: %i field\n",__func__,__LINE__);

		if(Status->GlobalDevice_t.dmdev_timer.dmctrl_feature & DM_TIMER_TYPE_DSP)
		{
			memset(&Status->TimersSettingsNet,0,sizeof(Status->TimersSettingsNet));
			if(AdapterSettingsSaveNetTimersSettings(Status ) < 0)
			{
				return  -AXIS_ERROR_INVALID_VALUE;
			}
		}
			DBGLOG("%s: %i field\n",__func__,__LINE__);

	}
	
	DBGLOG("%s: %i field\n",__func__,__LINE__);

	DBGLOG( DISPATCHER_LIB_DEBUG__DONE );

	return ret;

}


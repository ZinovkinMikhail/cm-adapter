//������� �������� ���������� ���������


//�����: ��������� ����� ��� "����"
//�����: i.podkolzin@ross-jsc.ru
//������: 2010.09.13 15:11


//���������� �������

//������������ �������
#include <axis_error.h>

#include <dispatcher_lib/dispatcher_lib_resource.h>
#include <dispatcher_lib/settings.h>
#include <dispatcher_lib/requests.h>
#include <dispatcher_lib/printf.h>

//��������� �������
#include <adapter/settings.h>
#include <adapter/recorder.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
//#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif


//������� ��������� �������� ���������
//����:
// Status - ���������� ��������� ������� ��������
//�������:
// 0 - ��� ������ - ������ ������� �������
// ����� 0 � ������ ������

int AdapterSettingsSaveRecorderTimers(
        AdapterGlobalStatusStruct_t *Status )
{

	int ret = AXIS_NO_ERROR;

	if ( !Status )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	if ( Status->Recorder.recorderscount > 0 )
	{
		if ( Status->Recorder.next )
		{
			if(recorder_lock_time( &Status->Recorder.next->lock ) < 0)
			{
				return -1;
			}
		}
	}

	else
	{
		DBGERR( "%s:No Recorders for getting timers\n", __func__ );
		return -1;
	}

	if ( Status->Recorder.next->recorder_put_timers )
	{

// 		if(!Status->Recorder.next->recorder_data.recorderstimers_ex)
// 		{
// 			DBGLOG("%s:%i:No timers data init\n",__func__,__LINE__);
// 		}
		ret = Status->Recorder.next->recorder_put_timers( Status->Recorder.next->hen , Status->Recorder.next->recorder_data.recorderstimers_ex, &Status->Recorder.next->semaphore,0 ) ;
		if ( ret)
		{
			DBGERR( "%s:It wasn't possible to get timers\n", __func__ );
		}
	}



	if ( Status->Recorder.recorderscount > 0 )
	{
		if ( Status->Recorder.next )
		{
			recorder_unlock( &Status->Recorder.next->lock );
		}

	}


	DBGLOG( DISPATCHER_LIB_DEBUG__DONE );

	return ret;

}
//������� ��������� �������� ���������
//����:
// Status - ���������� ��������� ������� ��������
//�������:
// 0 - ��� ������ - ������ ������� �������
// ����� 0 � ������ ������

int AdapterSettingsSaveRecorderTimersDefault(
        AdapterGlobalStatusStruct_t *Status )
{

	int ret = AXIS_NO_ERROR;

	if ( !Status )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	if ( Status->Recorder.recorderscount > 0 )
	{
		if ( Status->Recorder.next )
		{
			if(recorder_lock_time( &Status->Recorder.next->lock ) < 0)
			{
				return -1;
			}
		}
	}

	else
	{
		DBGERR( "%s:No Recorders for getting timers\n", __func__ );
		return -1;
	}

	if ( Status->Recorder.next->recorder_put_timers )
	{

// 		if(!Status->Recorder.next->recorder_data.recorderstimers_ex)
// 		{
// 			DBGLOG("%s:%i:No timers data init\n",__func__,__LINE__);
// 		}
		ret = Status->Recorder.next->recorder_put_timers( Status->Recorder.next->hen , Status->Recorder.next->recorder_data.recorderstimers_ex, &Status->Recorder.next->semaphore,1 ) ;
		if ( ret)
		{
			DBGERR( "%s:It wasn't possible to get timers\n", __func__ );
		}
	}



	if ( Status->Recorder.recorderscount > 0 )
	{
		if ( Status->Recorder.next )
		{
			recorder_unlock( &Status->Recorder.next->lock );
		}

	}


	DBGLOG( DISPATCHER_LIB_DEBUG__DONE );

	return ret;

}
//������� ��������� ������ ���������
//����:
// Status - ���������� ��������� ������� ��������
//�������:
// 0 - ��� ������ - ������ ������� �������
// ����� 0 � ������ ������

int AdapterSettingsSaveRecorderSettings(
        AdapterGlobalStatusStruct_t *Status )
{
	int ret = AXIS_NO_ERROR;

	if ( !Status )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}
	if ( Status->Recorder.recorderscount > 0 )
	{
		if ( Status->Recorder.next )
		{
			if(recorder_lock_time( &Status->Recorder.next->lock ) < 0)
			{
				return -1;
			}
		}
	}

	else
	{
		DBGERR( "%s:No Recorders for getting settings\n", __func__ );
		return -1;
	}

	if ( Status->Recorder.next->recorder_put_settings )
	{
		ret = Status->Recorder.next->recorder_put_settings( Status->Recorder.next->hen , Status->Recorder.next->recorder_data.recordersettings_ex, &Status->Recorder.next->semaphore,0 ) ;
		if ( ret )
		{
			DBGERR( "%s:It wasn't possible to get timers\n", __func__ );
			return -1;
		}
	}

	if ( Status->Recorder.recorderscount > 0 )
	{
		if ( Status->Recorder.next )
		{
			recorder_unlock( &Status->Recorder.next->lock );
		}

	}
	return ret;
}
//������� ��������� ������ ���������
//����:
// Status - ���������� ��������� ������� ��������
//�������:
// 0 - ��� ������ - ������ ������� �������
// ����� 0 � ������ ������

int AdapterSettingsSaveRecorderSettingsDefault(
        AdapterGlobalStatusStruct_t *Status )
{
	int ret = AXIS_NO_ERROR;

	if ( !Status )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}
	if ( Status->Recorder.recorderscount > 0 )
	{
		if ( Status->Recorder.next )
		{
			if(recorder_lock_time( &Status->Recorder.next->lock ) < 0)
			{
				return -1;
			}
		}
	}

	else
	{
		DBGERR( "%s:No Recorders for getting settings\n", __func__ );
		return -1;
	}

	if ( Status->Recorder.next->recorder_put_settings )
	{
		ret = Status->Recorder.next->recorder_put_settings( Status->Recorder.next->hen , Status->Recorder.next->recorder_data.recordersettings_ex, &Status->Recorder.next->semaphore,1 ) ;
		if ( ret )
		{
			DBGERR( "%s:It wasn't possible to get timers\n", __func__ );
			return -1;
		}
	}

	if ( Status->Recorder.recorderscount > 0 )
	{
		if ( Status->Recorder.next )
		{
			recorder_unlock( &Status->Recorder.next->lock );
		}

	}
	return ret;
}

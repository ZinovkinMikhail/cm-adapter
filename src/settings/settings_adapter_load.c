//���������� ����� ��������


//���������� �������

//������������ �������
#include <axis_error.h>

#include <dmxxx_lib/dmxxx_calls.h>
#include <dispatcher_lib/printf.h>
//��������� �������
#include <adapter/settings.h>
#include <adapter/hardware.h>
#include <adapter/timer.h>
#include <adapter/recorder.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif


//������� ��������� ������ ��������
//����:
// Status - ���������� ��������� ������� ��������
//�������:
// 0 - ��� ������ - ������ ������� �������
// ����� 0 � ������ ������
int AdapterSettingsLoadAdapterSettings(
        AdapterGlobalStatusStruct_t *Status )
{


	int ret = AXIS_NO_ERROR;

	if( !Status )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}
	DBGLOG("%s: %i field\n",__func__,__LINE__);
	if( Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature & DM_DM_ENABLE_USB_FIRMWARE_SETTING_IN_RECORDER )
	{

		if( Status->Recorder.recorderscount > 0 )
		{
			if( Status->Recorder.next )
			{
				if(recorder_lock_time( &Status->Recorder.next->lock ) < 0)
				{
					return -1;
				}
				Status->GlobalDevice_t.dmdev_dm3xx.privatedata = &Status->Recorder.next->semaphore;
				Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_sets_hen = Status->Recorder.next->hen;
			}

		}
		else
		{
			DBGERR( "%s:No Recorders for getting settings\n", __func__ );
			return -AXIS_ERROR_BUSY_DEVICE;
		}
	}
	DBGLOG("%s: %i field\n",__func__,__LINE__);

	//���� ���������� ������������ ����������� ������ ��������� �� ����
	if( Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature & DM_DM_ENABLE_USB_FIRMWARE_SETTING_IN_RECORDER &&
			Status->Recorder.next->recorder_get_adapter_settings )
	{
		ret = Status->Recorder.next->recorder_get_adapter_settings( Status->Recorder.next->hen,
				&Status->Recorder.next->semaphore,
				&Status->AdapterSettings );
	}

	else
	{
		ret = DMXXXInitGetSettings( &Status->GlobalDevice_t, &Status->AdapterSettings );
	}

	DBGLOG("%s: %i field\n",__func__,__LINE__);

	if( ret < 0 )
	{
		DBGERR( "%s: It wasn't possible to get settings\n", __func__ );
	}
	
	//���� ���������� ����, ��� ������� ��������� ��������� � ����������
	if( Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature & DM_DM_ENABLE_USB_FIRMWARE_SETTING_IN_RECORDER &&
			Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature & DM_DM_ENABLE_USE_RECORDER_ALIASE &&
			Status->Recorder.next->recorder_get_recorder_alias )
	{
		const char *Alias = Status->Recorder.next->recorder_get_recorder_alias( &Status->Recorder.next->recorder_data );

		if( !Alias )
		{
			DBGERR( "%s: Can't get alias from recorder\n", __func__ );
		}

		else
		{
			//�������� ��������� ��������� ����
			strncpy( Status->AdapterSettings.Block0.AdapterSettings.Alias,
					 Alias,
					 sizeof( Status->AdapterSettings.Block0.AdapterSettings.Alias ) );
		}
	}
	
	DBGLOG("%s: %i field\n",__func__,__LINE__);

	if( Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature & DM_DM_ENABLE_USB_FIRMWARE_SETTING_IN_RECORDER )
	{
		if( Status->Recorder.recorderscount > 0 )
		{
			if( Status->Recorder.next )
			{
				recorder_unlock( &Status->Recorder.next->lock );
			}

		}
	}

	DBGLOG("%s: %i field\n",__func__,__LINE__);

	if(ret < 0)
	{
		return ret;
	}

	DBGLOG("%s: %i field\n",__func__,__LINE__);

	//��� �� ��� ������� - ������� ���� ������ �� ������ ������� ��� �����
	Status->SleepTime = Status->AdapterSettings.Block0.AdapterSettings.Sleep;

//������� ��� ���� ������ ���� � ������� � �� ��� ����� ������ ���������� ��� ������������
#if 0
	if( Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature & DM_DM_ENABLE_DYNAMIC_NET )
	{
		Status->AdapterSettings.Block0.AdapterSettings.Connect &= ~AXIS_SOFTADAPTER_CONNECT__ADAPTER_TYPE;
		Status->AdapterSettings.Block0.AdapterSettings.Connect |= Status->PollingModem.connecttype;
	}
#endif
	DBGLOG("%s: %i field\n",__func__,__LINE__);
	if( adapter_settings_work_new_sets( Status, 0, 0, NULL ) < 0 )
	{
		DBGERR( "%s:It wasn't possible to work new sets\n", __func__ );
	}
	return ret;

}


int AdapterSettingsLoadDefault( dmdev_t *dev, SoftAdapterExchangeSettingsStruct_t* Settings )
{
	if( !dev || !Settings )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	if( DMXXXInitLoadDefaultSettings( dev, ( void* )Settings ) < 0 )
	{
		DBGERR( "%s:It wasn't possible to load default settings\n", __func__ );
		return -1;
	}

	return 0;


}

//������� ��������� �������� ���������
//����:
// Status - ���������� ��������� ������� ��������
//�������:
// 0 - ��� ������ - ������ ������� �������
// ����� 0 � ������ ������

int AdapterSettingsLoadAdapterTimers(
        AdapterGlobalStatusStruct_t *Status )
{
	int ret = AXIS_NO_ERROR;
	DBGLOG( DISPATCHER_LIB_DEBUG__START );

	if( !Status )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	if( Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature & DM_DM_ENABLE_USB_FIRMWARE_SETTING_IN_RECORDER )
	{
		if( Status->Recorder.recorderscount > 0 )
		{
			if( Status->Recorder.next )
			{
				if(recorder_lock_time( &Status->Recorder.next->lock ) < 0)
				{
					return -1;
				}
				Status->GlobalDevice_t.dmdev_dm3xx.privatedata = &Status->Recorder.next->semaphore;
				Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_sets_hen = Status->Recorder.next->hen;
			}

		}
		else
		{
			DBGERR( "%s:No Recorders for getting settings\n", __func__ );
			return -AXIS_ERROR_BUSY_DEVICE;
		}
	}
	
	//���� ��������� �������� ��������� � ����������
	//� ���������� ���������� ������������ ���� ���������� - �������� ������� �� ����������
	//���������� ����� ��������� �� ��������

	if( Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature & DM_DM_ENABLE_USB_FIRMWARE_SETTING_IN_RECORDER &&
			Status->Recorder.next->recorder_get_adapter_timers )
	{
		if( Status->GlobalDevice_t.dmdev_timer.dmctrl_feature & DM_TIMER_TYPE_DSP )
		{
			ret = Status->Recorder.next->recorder_get_adapter_timers(
					  Status->Recorder.next->hen, &Status->Recorder.next->semaphore, &Status->TimersSettingsNet );
		}

		else
		{
			ret = Status->Recorder.next->recorder_get_adapter_timers(
					  Status->Recorder.next->hen, &Status->Recorder.next->semaphore, &Status->TimersSettingsNetAxis );
		}

		if( ret < 0 )
		{
			DBGERR( "%s: It wasn't possible to get adapter timers from recorder lib\n", __func__ );
			goto exit;
		}

	}

	else
	{

		if( Status->GlobalDevice_t.dmdev_timer.dmctrl_feature & DM_TIMER_TYPE_DSP )
		{
			if( DMXXXInitGetTimers( &Status->GlobalDevice_t, &Status->TimersSettingsNet ) )
			{

				DBGERR( "%s:It wasn't possible to get adapter timers\n",__func__ );
				ret= -1;
				goto exit;
			}
		}

		else
		{
			if( DMXXXInitGetTimers( &Status->GlobalDevice_t, &Status->TimersSettingsNetAxis ) )
			{

				DBGERR( "%s:It wasn't possible to get adapter timers\n",__func__ );
				ret= -1;
				goto exit;
			}
		}
	}

exit:
	if(Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature & DM_DM_ENABLE_USB_FIRMWARE_SETTING_IN_RECORDER)
	{
		if( Status->Recorder.recorderscount > 0 )
		{
			if( Status->Recorder.next )
			{
				recorder_unlock( &Status->Recorder.next->lock );
			}

		}
	}


	//��� ���� ����������� �� �����������
	ret = AdapterTimerWork( Status );

	if(ret < 0)
	{
		DBGERR("%s:It wasn't Possible to work timer\n",__func__);
		return -1;
	}


	return ret;

}

//���������� ����� ��������


//���������� �������

//������������ �������
#include <axis_error.h>

#include <dispatcher_lib/dispatcher_lib_resource.h>
#include <dispatcher_lib/settings.h>
#include <dispatcher_lib/requests.h>
#include <dispatcher_lib/printf.h>
#include <dispatcher_lib/command.h>
#include <dispatcher_lib/req_to_timers.h>
#include <dispatcher_lib/network.h>

#include <dmxxx_lib/dmxxx_calls.h>
//��������� �������
#include <adapter/settings.h>
#include <adapter/timer.h>
#include <adapter/leds.h>
#include <adapter/network.h>
#include <adapter/recorder.h>
#include <adapter/httpd.h>
#include <adapter/power.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif


//������� ������ ������ ��������
//����:
// Status - ��������� �� ���������� ��������� �������
//�������:
// 0 - ��� ������ - ������ ������� �������
// ����� 0 � ������ ������
int AdapterSettingsSaveAdapterSettings( AdapterGlobalStatusStruct_t *Status )
{


	int ret = AXIS_NO_ERROR;

	if ( !Status )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	if ( Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature &
				DM_DM_ENABLE_USB_FIRMWARE_SETTING_IN_RECORDER )
	{

		if ( Status->Recorder.recorderscount > 0 )
		{
			if ( Status->Recorder.next )
			{
				if(recorder_lock_time( &Status->Recorder.next->lock ) < 0)
				{
					return -1;
				}

				Status->GlobalDevice_t.dmdev_dm3xx.privatedata =
							&Status->Recorder.next->semaphore;
				Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_sets_hen =
							Status->Recorder.next->hen;
			}

		}
		else
		{
			DBGERR( "%s:No Recorders for saving settings\n", __func__ );
			return -AXIS_ERROR_BUSY_DEVICE;
		}
	}

	//���� ���������� ������������ ����������� ������ ��������� �� ����
	if( Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature & DM_DM_ENABLE_USB_FIRMWARE_SETTING_IN_RECORDER &&
			Status->Recorder.next->recorder_put_adapter_settings )
	{
		ret = Status->Recorder.next->recorder_put_adapter_settings( Status->Recorder.next->hen,
				&Status->Recorder.next->semaphore,
				&Status->AdapterSettings );
	}

	else
	{
	 	ret = DMXXXInitPutSettings( &Status->GlobalDevice_t, &Status->AdapterSettings );
	}
	
	if ( ret < 0 )
	{
		DBGERR( "%s: It wasn't possible to save settings\n", __func__ );
	}

	if ( Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature &
				DM_DM_ENABLE_USB_FIRMWARE_SETTING_IN_RECORDER )
	{
		if ( Status->Recorder.recorderscount > 0 )
		{
			if ( Status->Recorder.next )
			{
				recorder_unlock( &Status->Recorder.next->lock );
			}

		}
	}

	//��� �� ��� ������� - ������� ���� ������ �� ������ ������� ��� �����
	Status->SleepTime = Status->AdapterSettings.Block0.AdapterSettings.Sleep;


	DBGLOG( DISPATCHER_LIB_DEBUG__DONE );

	return ret;

}

//������� ������ ������ ��������
//����:
// Status - ��������� �� ���������� ��������� �������
//�������:
// 0 - ��� ������ - ������ ������� �������
// ����� 0 � ������ ������
int AdapterSettingsSaveAdapterSettingsDefault( AdapterGlobalStatusStruct_t *Status )
{


	int ret = AXIS_NO_ERROR;

	if( !Status )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}

	ret = AdapterSettingsLoadDefault( &Status->GlobalDevice_t, &Status->AdapterSettings );

	if( ret < 0 )
	{
		DBGERR( "%s: Can't load default settings in global\n", __func__ );
		return ret;
	}

	ret = AdapterSettingsSaveAdapterSettings( Status );

	if( ret < 0 )
	{
		DBGERR( "%s: Can't save new settings\n", __func__ );
		return ret;
	}

	//��� �� ��� ������� - ������� ���� ������ �� ������ ������� ��� �����
	Status->SleepTime = Status->AdapterSettings.Block0.AdapterSettings.Sleep;

	//TODO - ������� DMXXXInitSaveDefaultSettings �� ���������� ���������� ������ �� ������������

	DBGLOG( DISPATCHER_LIB_DEBUG__DONE );

	return ret;

}

//������� ������ ������ ��������
//����:
// Status - ��������� �� ���������� ��������� �������
//�������:
// 0 - ��� ������ - ������ ������� �������
// ����� 0 � ������ ������
int AdapterSettingsSaveNetTimersSettings( AdapterGlobalStatusStruct_t *Status )
{

	int ret = AXIS_NO_ERROR;

	if ( !Status )
	{
		DBGERR( DISPATCHER_LIB_DEBUG__INVALID_ARGUMENT );
		return -AXIS_ERROR_INVALID_ARGUMENT;
	}
	
	if ( Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature &
				DM_DM_ENABLE_USB_FIRMWARE_SETTING_IN_RECORDER )
	{
		if ( Status->Recorder.recorderscount > 0 )
		{
			if ( Status->Recorder.next )
			{
				if(recorder_lock_time( &Status->Recorder.next->lock ) < 0)
				{
					return -1;
				}
				Status->GlobalDevice_t.dmdev_dm3xx.privatedata = &Status->Recorder.next->semaphore;
				Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_sets_hen = Status->Recorder.next->hen;
			}

		}
		else
		{
			DBGERR( "%s:No Recorders for getting settings\n", __func__ );
			return -AXIS_ERROR_BUSY_DEVICE;
		}
	}
	
	if( Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature & DM_DM_ENABLE_USB_FIRMWARE_SETTING_IN_RECORDER &&
			Status->Recorder.next->recorder_put_adapter_timers )
	{
		if( Status->GlobalDevice_t.dmdev_timer.dmctrl_feature & DM_TIMER_TYPE_DSP )
		{
			ret = Status->Recorder.next->recorder_put_adapter_timers(
					  Status->Recorder.next->hen, &Status->Recorder.next->semaphore, &Status->TimersSettingsNet );
		}

		else
		{
			ret = Status->Recorder.next->recorder_put_adapter_timers(
					  Status->Recorder.next->hen, &Status->Recorder.next->semaphore, &Status->TimersSettingsNetAxis );
		}

		if( ret < 0 )
		{
			DBGERR( "%s: It wasn't possible to put adapter timers from recorder lib\n", __func__ );
		}

	}
	else
	{
	
		if( Status->GlobalDevice_t.dmdev_timer.dmctrl_feature & DM_TIMER_TYPE_DSP )
		{
			ret =  DMXXXInitPutTimers( &Status->GlobalDevice_t, &Status->TimersSettingsNet );
			if ( ret)
			{
				DBGERR( "%s:It wasn't possible to put adapter timers\n", __func__ );
			}
		}
		else
		{
			ret =  DMXXXInitPutTimers( &Status->GlobalDevice_t,
						&Status->TimersSettingsNetAxis );
			if ( ret)
			{
				DBGERR( "%s:It wasn't possible to put adapter timers\n", __func__ );
			}
		}
	}

	if ( Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature &
				DM_DM_ENABLE_USB_FIRMWARE_SETTING_IN_RECORDER )
	{
		if ( Status->Recorder.recorderscount > 0 )
		{
			if ( Status->Recorder.next )
			{
				recorder_unlock( &Status->Recorder.next->lock );
			}

		}
	}

	DBGLOG( DISPATCHER_LIB_DEBUG__DONE );

	return ret;

}

int adapter_settings_work_new_sets(AdapterGlobalStatusStruct_t *Status,
				   int iapplynow,int inewtime,struct_data *newdate)
{
	if( !Status ||( inewtime && !newdate))
	{
		DBGERR("%s:%i: Invalid Argumentn\n",__func__,__LINE__);
		return -1;
	}
	int ret = 0;

	ret = AdapterTimerWork(Status);
	if(ret < 0)
	{
		DBGERR("%s:It wasn't Possible to work timer\n",__func__);
		return -1;
	}

	ret = AdapterLedControlSettings(Status);
	if(ret < 0)
	{
		DBGERR("%s:It wasn't possible work leds\n",__func__);
		return -1;
	}

	ret = AdapterGlobalSensorWork(Status);
	if(ret < 0)
	{
		DBGERR("%s:It wasn't possible to work sensor\n",__func__);
		return -1;
	}

	ret = AdapterGlobalOutputSensorPowerWork(Status);
	if(ret < 0)
	{
		DBGERR("%s:It wasn't possible to work io\n",__func__);
		return -1;
	}

	DBGLOG("%s: %i field\n",__func__,__LINE__);

	ret = AdapterPowerControlUSB(&Status->GlobalDevice_t,
						&Status->AdapterSettings);
	if(ret < 0)
	{
		DBGERR("%s:It wasn't possible to control usb power\n",__func__);
		return -1;
	}
	DBGLOG("%s: %i field\n",__func__,__LINE__);

	if(iapplynow)
	{
		ret = AdapterNetworkConnectionRestart(Status);
		if(ret < 0)
		{
			DBGERR("%s:It wasn't possible apply network sets now\n",__func__);
			return -1;
		}
	}
	DBGLOG("%s: %i field\n",__func__,__LINE__);

	if(inewtime)
	{	DBGLOG("%s: %i field\n",__func__,__LINE__);

		//�� ����� ������ ����� - ������ ���� ��������� ��� �� ��� �����, ��� �� ����� ����������� ��������
		int iCanSleep = Status->icangosleep;
		Status->icangosleep = 0;

		ret = AdapterTimerSetTime(&Status->GlobalDevice_t,newdate);
		if( ret < 0)
		{
			DBGERR("%s:It wasn't possible to set time\n",__func__);
			return -1;
		}	DBGLOG("%s: %i field\n",__func__,__LINE__);

		ret = AdapterTimerSyncTime(&Status->GlobalDevice_t);
		if( ret < 0)
		{
			DBGERR("%s:It wasn't possible to sync time\n",__func__);
			return -1;
		}	DBGLOG("%s: %i field\n",__func__,__LINE__);

		if(Status->GlobalDevice_t.dmdev_dm3xx.dmctrl_feature &
						DM_DM_RECORDER_SYNC_TIME)
		{	DBGLOG("%s: %i field\n",__func__,__LINE__);

			ret = AdapterRecorderSyncAllRecordersTime(&Status->Recorder);
			if(ret < 0)
			{
				DBGERR("%s:It wasn't possible to sync recorders time\n",__func__);
				return -1;
			}
		}
		DBGLOG("%s: %i field\n",__func__,__LINE__);

		//����� ���������� - ������� ��������� ���������
		Status->LastInterfaceQuire = time( NULL );

	 	//�� ��������� ������ ����� - ���������� ��������� ��� �� �����
		Status->icangosleep = iCanSleep;
	}
	DBGLOG("%s: %i field\n",__func__,__LINE__);

	return 0;
}

int AdapterSettingsSaveFromRequest( axis_httpi_request *Requests, size_t *RCount,
									AdapterGlobalStatusStruct_t *Status,int iapplynow )
{
	if( !Requests || ! RCount || !Status || *RCount <= 0 )
	{
		DBGERR( "%s:%i: Invalid Argumentn\n", __func__, __LINE__ );
		return -1;
	}

	int ret;

	struct_data NewDate;
	int error_flag=0;
	int return_flag = 0;
	unsigned int ClientIP = AdapterHttpdGetClientIP( axis_getpid(), Status ,NULL );
	void *NetTimers;

	//������� ������� �������� � ���� ��������� � � ���� ������ ����������
	if( Status->GlobalDevice_t.dmdev_timer.dmctrl_feature & DM_TIMER_TYPE_DSP )
	{
		NetTimers = &Status->TimersSettingsNet;
	}
	else
	{
		NetTimers = &Status->TimersSettingsNetAxis;
	}


	memset( &NewDate,0,sizeof( struct_data ) );

	if( iDMXXXAdapterSettingsSaveFromRequest( &Status->GlobalDevice_t ) )
	{
		//NOTE - ���� ����� API � ���������� ���������� - �� ����� ������������ ���
		ret = DMXXXAdapterSettingsSaveFromRequest( &Status->GlobalDevice_t, Requests, RCount,
												   &Status->AdapterSettings, NetTimers, &NewDate, ClientIP );

		if( ret < 0 )
		{
			DBGERR( "%s: Can't convert Adapter Settings from request\n", __func__ );
			error_flag = 1;
		}
		else if ( ret > 0 )
		{
			DBGERR( "%s: Convert Adapter Settings return - Ok\n", __func__ );
			return_flag = 1;
		}
		else
		{
			DBGERR( "%s: Convert Adapter Settings return - None\n", __func__ );
		}
	}
	else
	{
		//NOTE - ��� ������ ���������, � ������� ��� ��������������� �������

		if( ClientIP && DispatcherLibNetworkAllowLocalNetwork( ClientIP ) == 1 )
		{
			//NOTE - �������� �� ���� Status->RescueMode � ������ ������� �����������
			// ��� ��� �� ��� �������� ��� ������������ � ������ ������ (�������� �����-4�)
			// �� ��� ���� � �������� ������ ������ ������� ��������� �� USB ����������
			// � �� ����� ��� ����� �������� �������� �� ����������� � ��������� ����
			ret =  DispatcherLibCommandSetNetConnectionsFromRequests(
										Requests, *RCount,
										&Status->AdapterSettings.Block0.AdapterSettings,
										ClientIP,
										Status->GlobalDevice_t.dmdevice_type, 1 );
		}
		else
		{
			//� ������� ������ ��� �������� ������ ����� ����� ��
			ret = DispatcherLibCommandSetNetConnectionsFromRequestsRC(
										Requests, *RCount,
										&Status->AdapterSettings.Block0.AdapterSettings,
										ClientIP,
										Status->GlobalDevice_t.dmdevice_type, 1 );
		}

		if( ret < 0 )
		{
			//�������� ������ ��������������
			*RCount = -ret;
			error_flag |= 1;
			DBGERR( "%s: Net connections return - Error\n", __func__ );
		}

		else if( ret )
		{
			return_flag |= 1;
			DBGLOG( "%s: Net connections return - Ok\n", __func__ );
		}

		else
		{
			//���� �� ���� �������� ����� ��� ��������� ������� �� ������
			DBGLOG( "%s: Net connections return - None\n", __func__ );
		}

		ret =  DispatcherLibCommandSetNetNetFromRequests(
				Requests, *RCount,
				&Status->AdapterSettings.Block0.AdapterSettings,
				ClientIP,
				Status->GlobalDevice_t.dmdevice_type,
				1 );

		if( ret < 0 )
		{
			//�������� ������ ��������������
			*RCount = -ret;
			error_flag |= 1;
			DBGERR( "%s: Net Net return - Error\n", __func__ );
		}

		else if( ret )
		{
			return_flag |= 1;
			DBGLOG( "%s: Net Net return - Ok\n", __func__ );
		}

		else
		{
			//���� �� ���� �������� ����� ��� ��������� ������� �� ������
			DBGLOG( "%s: Net Net return - None\n", __func__ );
		}

		ret = DispatcherLibCommandSetNetTimersFromRequests(
				Requests,
				*RCount,
				&Status->AdapterSettings.Block0.AdapterSettings,
				&NewDate,
				Status->GlobalDevice_t.dmdevice_type,
				1 );//POWER ON??????????

		if( ret < 0 )
		{
			//�������� ������ ��������������
			*RCount = -ret;
			error_flag |= 1;
			DBGERR( "%s: Net Timers settings return - Error\n", __func__ );
		}

		else if( ret )
		{
			return_flag |= 1;
			DBGLOG( "%s: Net Timers settings return - Ok\n", __func__ );
		}

		else
		{
			//���� �� ���� �������� ����� ��� ��������� ������� �� ������
			DBGLOG( "%s: Net Timers settings return - None\n", __func__ );
		}

		// ������� �������� � ���� ������ �����
		if( Status->GlobalDevice_t.dmdev_timer.dmctrl_feature & DM_TIMER_TYPE_DSP )
		{
			DspRecorderExchangeTimerSettingsStruct_t *Timers = ( DspRecorderExchangeTimerSettingsStruct_t *)NetTimers;

			ret = action1_req_to_timers_cm( Requests, *RCount, Timers );
		}

		else
		{
			DispatcherLibAlarmExchangeBuffer_t *Timers = ( DispatcherLibAlarmExchangeBuffer_t * )NetTimers;

			ret = action1_req_to_timers( Requests, *RCount, Timers );
		}

		if( ret < 0 )
		{
			//�������� ������ ��������������
			*RCount = -ret;
			error_flag |= 1;
			DBGERR( "%s: Net Timers actions return - Error\n", __func__ );
		}

		else if( ret )
		{
			return_flag |= 1;
			DBGLOG( "%s: Net Timers actions return - Ok\n", __func__ );
		}

		else
		{
			//���� �� ���� �������� ����� ��� ��������� ������� �� ������
			DBGLOG( "%s: Net Timers actions return - None\n", __func__ );
		}

		ret = DispatcherLibCommandSetNetPowerFromRequests( Requests, *RCount,
				&Status->AdapterSettings.Block0.AdapterSettings,
				Status->GlobalDevice_t.dmdevice_type );

		if( ret < 0 )
		{
			//��������� ������ ��������������
			*RCount = -ret;
			error_flag = 1;
			DBGERR( "%s: Net Power return - Error\n", __func__ );
		}

		else if( ret )
		{
			return_flag |= 1;
			DBGLOG( "%s: Net Power return - Ok\n", __func__ );
		}

		else
		{
			DBGLOG( "%s: Net Power return - None\n", __func__ );
		}

		ret = DispatcherLibCommandSetNetIOFromRequests( Requests, *RCount,
				&Status->AdapterSettings.Block0.AdapterSettings.IOAlarmUSB,
				Status->GlobalDevice_t.dmdevice_type );

		if( ret < 0 )
		{
			//��������� ������ ��������������
			*RCount = -ret;
			error_flag = 1;
			DBGERR( "%s: Net IO return - Error\n", __func__ );
		}

		else if( ret )
		{
			return_flag |= 1;
			DBGLOG( "%s: Net IO return - Ok\n", __func__ );
		}

		else
		{
			DBGLOG( "%s: Net IO return - None\n", __func__ );
		}

		//������ alarm
		ret = DispatcherLibCommandSetNetAlarmFromRequests( Requests, *RCount,
				&Status->AdapterSettings.Block0.AdapterSettings.IOAlarmUSB,
				Status->GlobalDevice_t.dmdevice_type );

		if( ret < 0 )
		{
			//��������� ������ ��������������
			*RCount = -ret;
			error_flag = 1;
			DBGERR( "%s: Net Alrarm return - Error\n", __func__ );
		}

		else if( ret )
		{
			return_flag |= 1;
			DBGLOG( "%s: Net Alarm return - Ok\n", __func__ );
		}

		else
		{
			DBGLOG( "%s: Net Alarm return - None\n", __func__ );
		}

		DBGLOG( "%s: Try CDMA!!!!\n", __func__ );

		ret = DispatcherLibCommandSetNetCDMAFromRequests(
				Requests,
				*RCount,
				& Status->AdapterSettings.Block1.CDMASettings );


		if( ret < 0 )
		{
			//��������� ������ ��������������
			*RCount = -ret;
			error_flag = 1;
			DBGERR( "%s: Net CDMA return - Error\n", __func__ );
		}

		else if( ret )
		{
			return_flag |= 1;
			DBGLOG( "%s: Net CDMA return - Ok\n", __func__ );
		}

		else
		{
			DBGLOG( "%s: Net CDMA return - None\n", __func__ );
		}


		ret = DispatcherLibCommandSetNetWiFiFromRequests(
				Requests,
				*RCount,
				&Status->AdapterSettings.Block1.WiFiSettings,
				ClientIP,
				Status->GlobalDevice_t.dmdevice_type,
				1 ); //�� �������� � ������???

		if( ret < 0 )
		{
			//��������� ������ ��������������
			*RCount = -ret;
			error_flag = 1;
			DBGERR( "%s: Net Wifi return - Error\n", __func__ );
		}

		else if( ret )
		{
			return_flag |= 1;
			DBGLOG( "%s: Net Wifi return - Ok\n", __func__ );
		}

		else
		{
			DBGLOG( "%s: Net Wifi return - None\n", __func__ );
		}

	}
//-------------------------------------------------------------------------------
//TODO - ��� ���� ������� ��������� �������� � ������������, �������
//������� ��� �������� - ���� ������� � ��������� �������
//� ����� ������ df576b79ee53f6f52d35d6c907f3637dba073f89
//�������� �� ��������������!!!
	//������� �� � ��� �� ���� - ���������� �������
	int AlarmCount = 0;
	int i;
	int j;
	//��� ������� � �������
	uint16_t IOAlarm = Status->AdapterSettings.Block0.AdapterSettings.IOAlarmUSB;
	//����� ��� ������� �������� �������
	uint16_t 	AlarmMask 	= 0x0003; //���� �� 2 ����
	//�������������� ����� �� �������� �������
	int 		AlarmShift 	= 4; //������� 4 ���� �������� IO

	switch( Status->GlobalDevice_t.dmdevice_type )
	{
		case OMAP3530_DEVICE_HARDWARE_TYPE__ARK:
		case XXXXXXXX_DEVICE_HARDWARE_TYPE__GMK:
		case XXXXXXXX_DEVICE_HARDWARE_TYPE__DUMMY:
			AlarmCount = 0;
			break;
		case DM355_DEVICE_HARDWARE_TYPE__CDMA:
		case DM355_DEVICE_HARDWARE_TYPE__R2:
		{
			AlarmCount = 2;
			break;
		}
		case DM365_DEVICE_HARDWARE_TYPE__BANJO:
		{
			AlarmCount = 3;
			break;
		}
		case DM365_DEVICE_HARDWARE_TYPE__BANJO2:
		case DM365_DEVICE_HARDWARE_TYPE__BRT:
		case OMAP4430_DEVICE_HARDWARE_TYPE__BR3:
		case OMAP4430_DEVICE_HARDWARE_TYPE__YWG1:
		case OMAP3530_DEVICE_HARDWARE_TYPE__YWG2:
		case OMAP4430_DEVICE_HARDWARE_TYPE__BR4M:
		case OMAP3530_DEVICE_HARDWARE_TYPE__BR4W:
		{
			AlarmCount = 3;
			break;
		}
		case DM355_DEVICE_HARDWARE_TYPE__UD2:
		case DM355_DEVICE_HARDWARE_TYPE__220:
		case DM3x5_DEVICE_HARDWARE_TYPE__CM:
		{
			AlarmCount = 4;
			break;
		}
		default:
		{
			DBGERR("%s: Invalid AdapterDevice=%d\n",__func__, Status->GlobalDevice_t.dmdevice_type );
		}
	}

	for( i=0; i< AlarmCount; i++ )
	{
		if( !((IOAlarm >> AlarmShift) & AlarmMask) )
		{
			//��������� - ��������� �� ���� �������� �� �������������, � ������� ����� �� 2, ��� �� �� ����������
			//������ ������
			if( i == 2 ) //2 - ��� ������ ������, �� ���� ������������
			{
				for( j=0; j < Status->ExchangeEvents.Events.EventCount; j++ )
				{
					if( Status->ExchangeEvents.Events.Events[j].Type == DISPATCHER_LIB_EVENT__ADXL )
					{
						Status->ExchangeEvents.Events.Events[j].Flag = 2; //��� �� � ��������� ��� �� ���������� �������
					}
				}
			}
		}
	}
//----------------------------------------------------------------------------------

	DBGLOG( "%s: Return %d\n", __func__, return_flag );

	//����� �� �������

	if( error_flag )
	{
		DBGERR( "%s: Error detected!\n", __func__ );
		return -*RCount;
	}

	if( return_flag )
	{
		DBGLOG( "%s: Converted Ok\n", __func__ );
		ret = AdapterSettingsSaveAdapterSettings( Status );

		if( ret < 0 )
		{
			DBGERR( "%s:It wasn't possible to save net sets\n",__func__ );
			return -1;
		}

		ret = AdapterSettingsSaveNetTimersSettings( Status );

		if( ret < 0 )
		{
			DBGERR( "%s:It wasn't possible to save net timers\n",__func__ );
			return -1;
		}

		DBGLOG( "%s:%i:Neew date = %i\n",__func__,__LINE__,NewDate.reserv_0 );

		ret = adapter_settings_work_new_sets( Status,
											  iapplynow,
											  NewDate.reserv_0,
											  &NewDate );

		if( ret < 0 )
		{
			DBGERR( "%s: It wasn't possible to work new settings\n",__func__ );
			return -1; //TODO - ��� ������ ������ ����� � �������, ��� ������ - ��� ��� � ����
			//������ ������� ���� ������, ��� ���� �������������� ������� � ����������
			//Error �� ��, � ������������ ����� �����������, ������ �� �������� �����
			//� ����� �� �������� ��� ��������!!!!
			//� �������� 631 ������ �� ��� ����� � �������
		}

		//����� ��� �� ��������� ������ ���� ��������� ���������� ����� ���������

	}

	else
	{
		DBGLOG( "%s: Converted None\n", __func__ );
	}

	return ret;
}

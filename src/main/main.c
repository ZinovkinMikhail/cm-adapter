//------------------------------------------------------//
// ������ �������� ��� �������� "������-��" 			//
// 														//
// ������ ������ ����� ���������� �� ���������� Texas	//
// TMS320DM365											//
//														//
// ��� ������ ����� �������� �� �������������� ��������	//
// � ��� �� ����			//
//														//
// ����� ��������� ����� ����� ������� ��� "����" podkolzin@ross-jsc.ru e.ilyin@ross-jsc.ru
// 2009'11'28											//
//														//
//------------------------------------------------------//

//���������� �������
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#ifndef YOCTO	//TODO - ��� �����-4� ��� Yocto - � ��� ��� �������� �������
#include <execinfo.h>
#endif

#include <axis_net.h>

//������������ �������
#include <dispatcher_lib/dispatcher_lib_resource.h>
#include <dispatcher_lib/getopt.h>

//��������� �������
#include <adapter/resource.h>
#include <adapter/helper.h>
#include <adapter/global.h>

//��� ������� - ����� ��������� ���������
#define CONFIG_ENCODER_DEBUG_ERR
//#undef	CONFIG_ENCODER_DEBUG_ERR

//��� ������� - ����� ���������������
#define CONFIG_ENCODER_DEBUG_MSG
#undef	CONFIG_ENCODER_DEBUG_MSG


#ifdef	CONFIG_ENCODER_DEBUG_ERR
#include <axis_debug.h>
#else
#define DBGERR( ... )
#endif

#ifdef	CONFIG_ENCODER_DEBUG_MSG
#include <axis_debug.h>
#else
#define DBGLOG( ... )
#endif

#ifdef YOCTO
static inline void BacktracePrint( void )
{
	DBGERR( "%s: No backtrace symbols for Yocto project\n", __func__ );
}
#else

static inline void BacktracePrint( void )
{
	int j, nptrs;
	int size = 20; // ������� backtrace, 10-�� ������, � �����

	void *buffer[size];
	char **strings;
	int btfd = open( "/tmp/logdev/crash.log", O_WRONLY |
					 O_TRUNC |
					 O_CREAT,
					 S_IRUSR |
					 S_IWUSR |
					 S_IRGRP |
					 S_IWGRP );

	// ������� ������� ��������
	nptrs = backtrace( buffer, size );
	// ��� ��� ����������
	strings = backtrace_symbols( buffer, nptrs );

	if( strings == NULL )
	{
		perror( "backtrace_symbols" );
	}

	fprintf( stderr, "\n" );
	fprintf( stderr, "----------backtrace start----------\n" );

	// ������� � ���� backtrace
	if( btfd > 0 )
	{
		backtrace_symbols_fd( buffer, nptrs, btfd );
	}

	// �� � �������� ���� ������� � �������� �������,
	// ��� ������� ���� ������� ����������� �����
	// �� �������� ��� ���������,
	// �. �. ��� �������� ( ���� � ���������� ������ libc )
	for( j = ( nptrs - 1 ); j > 1 ; j-- )
	{
		fprintf( stderr, "%s\n", strings[j] );
	}

	free( strings );

	if( btfd > 0 )
	{
		close( btfd );
	}

	fprintf( stderr, "----------backtrace end----------\n" );
	fprintf( stderr, "\n" );

}

#endif

//��������� ���������� ���������� ������
//�� ���������� ����� ������ ����� ������� ��� �������
//� ���� ����� ��� ��� ���� ��������� ���������

//������� ��������� ��������
//����:
// sig - ���������� ����� ��������������� �������
static void MainGlobalSignalHendler ( int sig )
{
	DBGLOG ( "%s: %d\n", __func__, sig );

	//��� ����� ������� ��������� ���������� ���
	//��� �������

	switch ( sig )
	{
		case SIGINT:
		{
			fprintf( stderr, "User terminated\n" );
			break;
		}

		case SIGILL:
		{
			fprintf( stderr, "Incorrect CPU instruction\n" );
			BacktracePrint();
			break;
		}

		case SIGABRT:
		{
			fprintf( stderr, "Abort\n" );
			break;
		}

		case SIGSEGV:
		{
			fprintf( stderr, "Memory Error\n" );
			BacktracePrint();
			break;
		}

		case SIGTERM:
		{
			fprintf( stderr, "Terminated\n" );
			break;
		}

		case SIGPIPE:
		{
			fprintf( stderr, "Broken PIPE\n" );
			return;
		}

		case SIGBUS:
		{
			fprintf( stderr, "Bus Error\n" );
			return;
		}

		case SIGUSR1:
		{
			return;
		}

		case SIGUSR2:
		{
			return;
		}

		default:
		{
			fprintf( stderr,  "Unknown signal = %d\n", sig );
			break;
		}
	}
}

static void MainSystemInitSignals( void )
{
	struct sigaction act;
	sigset_t   set;

	memset( &act, 0, sizeof( act ) );

	act.sa_handler = MainGlobalSignalHendler;

	sigemptyset( &set );

	sigaddset( &set, SIGINT );
	sigaddset( &set, SIGILL );
	sigaddset( &set, SIGABRT );
	sigaddset( &set, SIGTERM );
	sigaddset( &set, SIGSEGV );
	sigaddset( &set, SIGPIPE );
	sigaddset( &set, SIGUSR1 );
	sigaddset( &set, SIGUSR2 );
	sigaddset( &set, SIGBUS );

	act.sa_mask = set;

	sigaction( SIGINT,	&act, 0 );
	sigaction( SIGILL,	&act, 0 );
	sigaction( SIGABRT, &act, 0 );
	sigaction( SIGTERM, &act, 0 );
	sigaction( SIGSEGV, &act, 0 );
	sigaction( SIGPIPE, &act, 0 );
	sigaction( SIGUSR1, &act, 0 );
	sigaction( SIGUSR2, &act, 0 );
	sigaction( SIGBUS,	&act, 0 );
}


//������ �������� ���������� ������� �� ���������
static Options_t options[] =
{
	{
		ADAPTER_HELPER__PRINT_USAGE_COMMAND,
		NoArgument, //RequiredArgument, OptionalArgument
		ADAPTER_HELPER__PRINT_USAGE_ARG,
	},
	{
		ADAPTER_HELPER__INTERACTIVE_START_COMMAND,
		NoArgument,
		ADAPTER_HELPER__INTERACTIVE_START_ARG,
	},
	{
		ADAPTER_HELPER__RESET_DEFAULT_COMMAND,
		NoArgument,
		ADAPTER_HELPER__RESET_DEFAULT_ARG,
	},
	{
		ADAPTER_HELPER__ALONE_COMMAND,
		NoArgument,
		ADAPTER_HELPER__ALONE_ARG,
	},
	{
		ADAPTER_HELPER__ADAPTER_DEVICE_COMMAND,
		RequiredArgument,
		ADAPTER_HELPER__ADAPTER_DEVICE_ARG,
	},
	{
		ADAPTER_HELPER__ADAPTER_INPUT_FILE_COMMAND,
		RequiredArgument,
		ADPATER_HELPER__ADAPTER_INPUT_FILE_ARG,
	},
	{
		ADAPTER_HELPER__RESCUE_MODE_COMMAND,
		NoArgument,
		ADAPTER_HELPER__RESCUE_MODE_ARG,
	},
	{
		ADAPTER_HELPER__NO_DAEMON_COMMAND,
		NoArgument,
		ADAPTER_HELPER__NO_DAEMON_ARG,
	},
	{
		ADAPTER_HELPER__CONSOLE_START_COMMAND,
		NoArgument,
		ADAPTER_HELPER__CONSOLE_START_ARG,
	},
	{
		ADAPTER_HELPER__NO_INTERFACE_COMMAND,
		NoArgument,
		ADAPTER_HELPER__NO_INTERFACE_ARG,
	},
	{
		ADAPTER_HELPER__KERNEL_DEBUG_LEVEL_COMMAND,
		RequiredArgument,
		ADAPTER_HELPER__KERNEL_DEBUG_LEVEL_ARG,
	},
	{
		NULL,
		0,
		0,
	},

};

// �������� �������, ������� ����� ����������
int main( int argc, char **argv )
{

	int iInteractive	= 0;	//������ � ������������� ������
	int iConsole		= 0;	//������ � �������� ��� �����������
	int iNoDaemon		= 0;	//������ � ��� �����������
	int iNoInterface	= 0;	//������ ��� ������ ������������ ����������
	int iResetSettings	= 0;	//������ � ������ ������ ��������
	int iAdapterDevice	= 0;	//��� �������� �� ������� ��� ���������
	int iRescueMode		= 0;	//������� � ����� �������������� ������� ��������
	int iLogging		= 0;
	int KernelDebugLvl	= DM_KERNEL_DEBUG_LEVEL;

	char *cInputFileName	= NULL; //��� �������� ����� ��� ���������

	MainSystemInitSignals();

	DBGLOG( DISPATCHER_LIB_DEBUG__START );

	//���������� �������� ���� ���������

	//��� ����� ��������� ��� ��� �����������
	//��� � �������������� ��� �����

	//� ������ �������������� �������
	//� ���� ������ �� �� ������������ � ����������
	//� �������� �������������� ���������� ����������
	//� ������

	//��� � ������ ������ ���� �������� � ����� �� ���������

	//���������� ����� ��������� ������


	//������ �� ���������� ������� ���������
	if( argc == 1 )
	{
		//������ ���� ��������� - ��������
		DBGLOG( "%s: No argument\n", __func__ );
		//�� ������� ���� �������� ������ ���� - ��� ��������
		AdapterPrintUsage( );
		return -1;
	}
	else
	{

		//���� ���� ���������
		int OptionIndex = 0;
		int c = 0;
		char *OptArg = NULL;

		//�������� ���� ������� ����������
		while( ( c = DispatcherLibGetOptions(
		                        argc, argv, options,
		                        &OptionIndex, &OptArg ) ) != EOF )
		{

			DBGLOG( "%s: Options '%d'='%s' index=%d\n", __func__, c, OptArg, OptionIndex );

			//��������� �� ������
			switch( c )
			{
				case ADAPTER_HELPER__PRINT_USAGE_ARG:
				{
					AdapterPrintUsage( );
					return 0;
				}
				case ADAPTER_HELPER__RESCUE_MODE_ARG:
				{
					iRescueMode = 1;
					KernelDebugLvl = ADAPTER_RESQUE_KERNEL_DEBUG_LEVEL;
					break;
				}
				case ADAPTER_HELPER__KERNEL_DEBUG_LEVEL_ARG:
				{
					if( OptArg )
					{
						KernelDebugLvl = atoi( OptArg );
					}
					if( !OptArg || KernelDebugLvl < 0 || KernelDebugLvl > ADAPTER_RESQUE_KERNEL_DEBUG_LEVEL )
					{
						DBGERR( "%s: Invalid kernel debug level %d (%s). Must be %u-%u\n", __func__,
											KernelDebugLvl, OptArg, ADAPTER_DEFAULT_KERNEL_DEBUG_LEVEL,
											ADAPTER_RESQUE_KERNEL_DEBUG_LEVEL );
						AdapterPrintUsage( );
						return 1;
					}
					break;
				}
				case ADAPTER_HELPER__ALONE_ARG:
				{
					iLogging = 1;
					break;
				}
				case ADAPTER_HELPER__RESET_DEFAULT_ARG:
				{
					iResetSettings = 1;
					break;
				}
				case ADAPTER_HELPER__NO_DAEMON_ARG:
				{
					iNoDaemon = 1;
					break;
				}
				case ADAPTER_HELPER__CONSOLE_START_ARG:
				{
					iConsole = 1;
					break;
				}
				case ADAPTER_HELPER__NO_INTERFACE_ARG:
				{
					iNoInterface = 1;
					break;
				}
				case ADAPTER_HELPER__INTERACTIVE_START_ARG:
				{
					iInteractive = 1;
					break;
				}
				case ADAPTER_HELPER__ADAPTER_DEVICE_ARG:
				{
					iAdapterDevice = DispatcherLibAdapterDeviceFromString( OptArg );

					if( iAdapterDevice < 0 )
					{
						DBGERR( "%s: CAn't convert adapter device name '%s'\n",
						        __func__, OptArg );
						AdapterPrintUsage( );
					}
					break;
				}
				case ADPATER_HELPER__ADAPTER_INPUT_FILE_ARG:
				{
					if( OptArg && strcmp( OptArg, ADPATER_HELPER__ADAPTER_INPUT_FILE_MODE ) == 0 )
					{
						cInputFileName = OptArg;
					}
					else if( OptArg && strcmp( OptArg, ADPATER_HELPER__ADAPTER_INPUT_FILE_STATUS ) == 0 )
					{
						cInputFileName = OptArg;
					}
					else if( OptArg && strcmp( OptArg, ADPATER_HELPER__ADAPTER_INPUT_FILE_NETWORK ) == 0 )
					{
						cInputFileName = OptArg;
					}
					else
					{
						DBGERR( "%s: Invalid file name for work '%s'\n", __func__, OptArg );
					}

					break;
				}
				default:
				{
					DBGERR( "%s: Can't process '%c'(%d) command options\n", __func__,
					        c, c );
					AdapterPrintUsage( );
					return -1;
				}
			}

		}

	}


	DBGLOG( "%s: ResetSettings = %d\n", __func__, iResetSettings );
	DBGLOG( "%s: Interactive = %d\n", __func__, iInteractive );

	if( !iAdapterDevice )
	{
		DBGERR( "%s: No AdapterDevice found from options\n", __func__ );
		AdapterPrintUsage( );
		return 1;
	}

	//���������� ����� �� ���������� ������, ����� ����� ���� ������������???
	if( AdapterGlobalCheckStart() < 0 )
	{
		DBGERR( "%s: Can't start global check thread\n", __func__ );
		return 1;
	}

	//���������� ���������� ����
	if( AdapterGlobalWail(
							iResetSettings,
							iInteractive,
							iAdapterDevice,
							iRescueMode,
							iLogging,
							iNoDaemon,
							iConsole,
							iNoInterface,
							cInputFileName,
							KernelDebugLvl ) < 0 )
	{
		while(1)
		{
			DBGERR( "%s: Global Wail end from error\n", __func__ );

			//������������ ����� �� ����� ������
			if( iInteractive )
			{
				fd_set	rdfdset;
				struct timeval tv;
				int res;

				FD_ZERO( &rdfdset );
				FD_SET( 0,  &rdfdset );

				//���������� ��������
				tv.tv_sec  = 1;
				tv.tv_usec = 0; //���� ������� = 1 sec

				res = select( 1, &rdfdset, NULL, NULL, &tv );

				if( !res )
				{
					continue;
				}//����� ��������� ��������

				return 1;
			}
			else
			{
				sleep(1);
			}
		}
		return -6;
	}

	return 0;
}

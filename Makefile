#����� ���� ���� ��� ��������� ����������

# �� ������� �� ������ GIT � ������� � ��� ��������� ����� ����

# ������� ����
CDATE := $(shell date +"%Y-%m-%d")

include .gitdate

# ���� - ��������� ����� ���� ��������� ������������� � �������� git � ���� ����� (�����)
# �� ��������� ������������ �������� �������������
.PHONY: .gitdate
.gitdate:
ifneq ($(MAKECMDGOALS),gitupdate)
ifneq ($(MAKECMDGOALS),gitadd)
ifneq ($(MAKECMDGOALS),gitcommit)
ifndef OLD_GIT_DATE
	@echo "�� ����� ������������� � �������� git �� ���� ��������!"
	@echo " make gitupdate - ��� �������� ������������� � ��������"
	@exit 1
else
ifneq ($(OLD_GIT_DATE),$(CDATE))
	@echo "���������� ������������� � �������� git : $(OLD_GIT_DATE)"
	@echo "���������� ������������ � �������� git ��������!"
	@echo " make gitupdate - ��� �������� ������������� � ��������"
#	@exit 1
endif
endif
endif
endif
endif

# � ����� ����� �� ������ ��������
GIT_BRANCH	:= $(shell git branch | grep -a '*' | sed -e 's/* //')

# ���� �� ����������
GIT_STATUS	:= $(shell LANG=C git status | grep -e 'no changes added to commit')

MPROGRAM	:= $(notdir ${shell pwd})

BUILDFILE	= .build

include $(BUILDFILE)

GIT_HEADS	:= $(shell git describe --tags --long)

GIT_BUILD	:= $(shell git show -s --format="%ct")

# ���� ������ ���������� - ����������� �������
ifneq ($(BUILD),$(GIT_HEADS))
BUILDRM		:= $(shell rm $(BUILDFILE) 2>/dev/null)
endif

# ���� ������� ������ ����, ���� ��� ���
# � ��� ������ ����� ���������� ��� ��� �������, ����� git � ����� ��� ��������
# BUILD - ��� �������
# BUILDTIME - ����� �������� ������� �����
# BUILDBRANCH - ����� �� ������� ������� ��� ����
$(BUILDFILE):
	@echo "	(BULD) $(BUILDFILE)"
	@echo "BUILD := $(GIT_HEADS)" > $(BUILDFILE)
	@echo "BUILDTIME := $(GIT_BUILD)" >> $(BUILDFILE)
	@echo "BUILDBRANCH := $(GIT_BRANCH)" >> $(BUILDFILE)


# ���� - �������� ������������� � �������� git
.PHONY: gitupdate
gitupdate:
	@echo "	(GIT) $(MPROGRAM)"
# �������� ��������� ����� ��������� � �������
	@git fetch -p
	@git pull origin $(GIT_BRANCH)
# ����������� ���������
ifneq ($(GIT_STATUS), )
	@echo ""
	@echo "	���� ������������ ��������� � ������� ������� �� ���� ��������������!"
	@echo "	make $(MPROGRAM) gitcommit - ��� ������������ �������� � �����������"
	@echo "	make $(MPROGRAM) gitadd - ��� ���������� ������ � �����������"
	@echo "	��� ��������� ����������� �������� git � �������"
	@echo ""
	@exit 1
else
	@git push --tags origin $(GIT_BRANCH)
	@echo "OLD_GIT_DATE := "$(CDATE)  > .gitdate
endif

# ���� - ������������� ��������� � �������
.PHONY: gitcommit
gitcommit:
# ���������� ������ ����������
	@echo "	(GIT) $(MPROGRAM) commit"
	@git commit -a -s

# ���� - �������� ������ ����� � ������
.PHONY: gitadd
gitadd:
# ������� ����������� �����
	@echo "	(GIT) $(MPROGRAM) add"
	@git add -A

# ���� - ���������
.PHONY: mpc8315 mpc8315_install
mpc8315 mpc8315_install:
	@echo "	(FREESCALE)"
	@mkdir -p bin/freescale
	@$(MAKE) --directory=./bin/freescale --makefile=../../projects/Makefile.freescale  $(MAKECMDGOALS)


# ���� - ��� �������
.PHONY: tms320 tms320_install
tms320 tms320_install:
	@echo "	(TEXAS)"
	@mkdir -p bin/texas
	@$(MAKE) --directory=./bin/texas --makefile=../../projects/Makefile.texas  $(MAKECMDGOALS)

# ���� - ��� ������� OMAP3530
.PHONY: omap3530 omap3530_install
omap3530 omap3530_install:
	@echo "	(TEXAS)"
	@mkdir -p bin/omap3530
	@$(MAKE) --directory=./bin/omap3530 --makefile=../../projects/Makefile.omap3530  $(MAKECMDGOALS)

# ���� - �������
.PHONY: clean clean_dep
clean clean_dep:
	@echo "	(CLEAN)"
	@mkdir -p bin/texas
	@mkdir -p bin/freescale
	@mkdir -p bin/i386
	@mkdir -p bin/omap3530
	@mkdir -p bin/yocto
	@$(MAKE) --directory=./bin/omap3530 --makefile=../../projects/Makefile.omap3530  $(MAKECMDGOALS)
	@$(MAKE) --directory=./bin/texas --makefile=../../projects/Makefile.texas  $(MAKECMDGOALS)
	@$(MAKE) --directory=./bin/freescale --makefile=../../projects/Makefile.freescale  $(MAKECMDGOALS)
	@$(MAKE) --directory=./bin/i386 --makefile=../../projects/Makefile.intel $(MAKECMDGOALS)
	@$(MAKE) --directory=./bin/yocto --makefile=../../projects/Makefile.yocto $(MAKECMDGOALS)

# ���� - i386
.PHONY: i386 i386_install exec
i386 i386_install exec:
	@echo "	(I386) $(MAKECMDGOALS)"
	@mkdir -p bin/i386
	@$(MAKE) --directory=./bin/i386 --makefile=../../projects/Makefile.intel $(MAKECMDGOALS)

# ���� - Yocto
.PHONY: yocto yocto_install
yocto yocto_install:
	@echo "	(Yocto)"
	@mkdir -p bin/yocto
	@$(MAKE) --directory=./bin/yocto --makefile=../../projects/Makefile.yocto $(MAKECMDGOALS)

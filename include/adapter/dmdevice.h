//��������� �������, ������������ �������������� ����������

#ifndef CM_ADAPTER_DMDEVICE_H
#define CM_ADAPTER_DMDEVICE_H

#include <axis_dmlibs_resource.h>
#include <adapter/global.h>
#define DEVICE_PATH_MASK "/dev/%s"


/**
 * @brief ������� ������������� ���������� ��� ���������
 * 
 * @param dev - ��������� �� ��������� ����������
 * @return int: 0 - OK, <0 - fail
 */
int AdapterDmDeviceInit(dmdev_t *dev);

/**
 * @brief ������� ��������������� ���������� ��� ���������� ������
 * 
 * @param dev - ��������� �� ��������� ����������
 * @return int: 0 - OK, <0 - fail
 */
int AdapterDmDeviceDeInit(dmdev_t *dev);

/**
 * @brief ������� �������� ��������������� ���� ��� ������� ��
 * �������������� � �������� ���������
 * 
 * @param dev - ��������� �� ��������� ����������
 * @return int: 0 - OK, <0 - fail
 */
int AdapterDmDeviceMknod( dmdev_t *dev);

/**
 * @brief ������� ���������� ������������ watchdog
 * 
 * @param dev - ��������� �� ��������� ����������
 * @return int: 0 - OK, <0 - fail
 */
int AdapterDmDeviceWatchDogEnable( dmdev_t *dev);

/**
 * @brief ������� ���������� ������������ watchdog
 * 
 * @param dev - ��������� �� ��������� ����������
 * @return int: 0 - OK, <0 - fail
 */
int AdapterDmDeviceWatchDogDisable(dmdev_t *dev);

/**
 * @brief ������� �������� ������� ��� watchdog
 * 
 * @param dev - ��������� �� ��������� ����������
 * @return int: 0 - OK, <0 - fail
 */
int AdapterDmDeviceWatchDogTick( dmdev_t *dev);


int AdapterDmDeviceGetLastKeyPress( dmdev_t* dev, time_t *time );

/**
 * @brief ������� ������������ ���������� � ����� ����������� �����������������
 * 
 * @param dev - ��������� �� ��������� ����������
 * @return int: 0 - OK, <0 - fail
 */
int AdapterDmDeviceSleepGo(dmdev_t *dev);

/**
 * @brief ������� ��������� ��������� ������ ����������
 * 
 * @param Status - ���������� ��������� ���������� ��������
 * @return int: 0 - OK, <0 - fail
 */
int AdapterDmDeviceGetSerialNumber(AdapterGlobalStatusStruct_t *Status);


int AdapterDmDeviceAdxlInit( dmdev_t *dev );

int AdapterDmDeviceLockKeyboard(dmdev_t *dev);

int AdapterDmDeviceUnlockKeyboard(dmdev_t *dev);
#endif //CM_ADAPTER_DMDEVICE_H


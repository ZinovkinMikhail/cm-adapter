//������� ��� ������ � ������� �����������

#ifndef ADAPTER_LOG_H
#define ADAPTER_LOG_H

#include <adapter/global.h>

#define LOG_DAEMON_SOCKET	"/tmp/log_socket"
#define LOG_ADAPTER_FILE_NAME	"adapter"

//��� �����������
#define LOG_ROTATE_SIZE		1048576

#define LOG_ADAPTER_PATH_1	"/tmp/logdev/logdaemon/adapter/adapter_1.log"
#define LOG_ADAPTER_PATH_2	"/tmp/logdev/logdaemon/adapter/adapter_2.log"
#define LOG_ADAPTER_PATH_3	"/tmp/logdev/logdaemon/adapter/adapter_3.log"
#define LOG_ADAPTER_PATH_4	"/tmp/logdev/logdaemon/adapter/adapter_4.log"
#define LOG_ADAPTER_PATH_5	"/tmp/logdev/logdaemon/adapter/adapter_5.log"

/**
 * @brief ������� ������������� �����������
 * 
 * @param Status - ��������� �� ���������� ��������� ��������
 * @return int: 0 - OK, <0 - fail
 */
int AdapterLogInit( AdapterGlobalStatusStruct_t *Status );

/**
 * @brief ������� ��������������� �����������
 * 
 * @param Status - ��������� �� ���������� ��������� ��������
 * @return int: 0 - OK, <0 - fail
 */
void AdapterLogDeInit( AdapterGlobalStatusStruct_t* Status );


/**
 * @brief ������� ������ ��������������� ��������� ��� ������ � � �������
 * 
 * @param Status - ��������� �� ���������� ��������� ��������
 * @param res - ������ ��� ������: 1 = [Error], 0 - [Ok]
 * @param msg - ���������
 * @param  ...
 * @return void
 */
void AdapterLogInfo( AdapterGlobalStatusStruct_t* Status, int res, const char* msg, ... );

/**
 * @brief ������� ����������� �����
 * 
 * @param Status - ��������� �� ���������� ��������� ��������
 * @param msg - ���������
 * @param  ...
 * @return void
 */
void AdapterLogLog( AdapterGlobalStatusStruct_t *Status, const char* msg, ... );

/**
 * @brief ������� ����������� ������� ���������
 * 
 * @param Status - ��������� �� ���������� ��������� ��������
 * @param msg - ���������
 * @param size - ������ ������
 * @return void
 */
void AdapterLogBuff( AdapterGlobalStatusStruct_t *Status, const char* msg, size_t size );


#endif //ADAPTER_LOG_H

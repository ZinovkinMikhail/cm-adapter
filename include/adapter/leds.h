#ifndef ADAPTER_LEDS_H
#define ADAPTER_LEDS_H

#include <adapter/global.h>

#define ADAPTER_LED_AUTO_TIME					30


//������� ��� ��������� �������� � ����������
//����:
// Status - ��������� �� ��������� ����������� ������� ����������
//�������:
// ���� �������� �� ������� ���, ��� ��� ��� ������ ������
// ��������� � ������������ ����� ��������� ������ �� �������
// � �������� ����������
int AdapterLedControlSettings( AdapterGlobalStatusStruct_t *Status);

/**
 * @brief ������� ���������� ��������� �����������
 * 
 * @param dev - ��������� ����������
 * @return int: 0 -OK, <0 - fail
 */
int AdapterLedsOn(dmdev_t *dev);

/**
 * @brief ������� ���������� ��������� �����������
 * 
 * @param dev - ��������� ����������
 * @return int: 0 -OK, <0 - fail
 */
int AdapterLedsOff(dmdev_t *dev);

/**
 * @brief ������� �������� ������� �����������
 * 
 * @param Status - ��������� �� ���������� ��������� ����������
 * @param lednum - ����� ����������
 * @param color - ���� ����������
 * @param func - ��� ���������� ������� ��� �������
 * @return int: 0 -OK, <0 - fail
 */
int AdapterLedBlinkFast( AdapterGlobalStatusStruct_t *Status, int lednum, const char *color, const char *func );

/**
 * @brief ������� ������� �����������
 *
 * @param Status - ��������� �� ���������� ��������� ����������
 * @param lednum - ����� ����������
 * @param color - ���� ����������
 * @param func - ��� ���������� ������� ��� �������
 * @return int: 0 -OK, <0 - fail
 */
int AdapterLedBlink( AdapterGlobalStatusStruct_t *Status, int lednum, const char *color, const char *func );


/**
 * @brief ������� ��������� ����������
 * 
 * @param Status - ��������� �� ���������� ��������� ����������
 * @param lednum - ����� ����������
 * @param color - ���� ����������
 * @param func - ��� ���������� ������� ��� �������
 * @return int: 0 -OK, <0 - fail
 */
int AdapterLedOn( AdapterGlobalStatusStruct_t *Status, int lednum, const char *color, const char *func );

/**
 * @brief ������� ���������� ����������
 *
 * @param Status - ��������� �� ���������� ��������� ����������
 * @param lednum - ����� ����������
 * @param color - ���� ����������
 * @param func - ��� ���������� ������� ��� �������
 * @return int: 0 -OK, <0 - fail
 */
int AdapterLedOff( AdapterGlobalStatusStruct_t *Status, int lednum, const char *color, const char *func );

/**
 * @brief ������� ������������ � ���������/���������� �������� ������
 * 
 * @param command - �������:
 * 			1 - ��� ������������
 * 			0 - ���� ������������
 * @return int: 0 -OK, <0 - fail
 */
int AdapterLedUpMonControl( uint16_t command);

/**
 * @brief �������, ������� �������������� ��� ��������� �������� ������ �����
 *
 * @param Status - ��������� �� ���������� ��������� ������� ��������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterLedInit( AdapterGlobalStatusStruct_t *Status );

/**
 * @brief �������, ������� �������� ��������� ������������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterLedHeartBeatOn( AdapterGlobalStatusStruct_t *Status );

/**
 * @brief �������, ������� ��������� ��������� ������������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterLedHeartBeatOff( AdapterGlobalStatusStruct_t *Status );

/**
 * @brief �������, ������� ������������ �������� ����������� ������������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterLedHeartBeatBlink( AdapterGlobalStatusStruct_t *Status );

/**
 * @brief �������, ������� �������� ��������� ��� ���������� ��������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterLedFirmwareOn( AdapterGlobalStatusStruct_t *Status );

/**
 * @brief �������, ������� ��������� ��������� ��� ���������� ��������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterLedFirmwareOff( AdapterGlobalStatusStruct_t *Status );

/**
 * @brief �������, ������� ������������ �������� ��� ���������� ��������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterLedFirmwareBlink( AdapterGlobalStatusStruct_t *Status );

/**
 * @brief �������, ������� �������� ��������� ��� ������ � �������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterLedHardwareOn( AdapterGlobalStatusStruct_t *Status );

/**
 * @brief �������, ������� ��������� ��������� ��� ����� � �������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterLedHardwareOff( AdapterGlobalStatusStruct_t *Status );

/**
 * @brief �������, ������� ������������ �������� ��� ������ � �������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterLedHardwareBlink( AdapterGlobalStatusStruct_t *Status );

/**
 * @brief �������, ������� �������� ��������� ��� ����������� � ����
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterLedNetworkConnectOn( AdapterGlobalStatusStruct_t *Status );

/**
 * @brief �������, ������� ��������� ��������� ��� ���������� �� ����
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterLedNetworkConnectOff( AdapterGlobalStatusStruct_t *Status );

/**
 * @brief �������, ������� ������������ �������� ���������� ��� ����������� � ����
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterLedNetworkConnectBlink( AdapterGlobalStatusStruct_t *Status );

/**
 * @brief �������, ������� �������� ��������� ��� ���������� �������� �����������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterLedNetworkNoConnectionOn( AdapterGlobalStatusStruct_t *Status );

/**
 * @brief �������, ������� ��������� ��������� ��� ���������� �������� ����������� (������� ������� ��� �������) 
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterLedNetworkNoConnectionOff( AdapterGlobalStatusStruct_t *Status );

/**
 * @brief �������, ������� ������������ �������� ����������� ��� ���������� ���� 
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterLedNetworkNoConnectionBlink( AdapterGlobalStatusStruct_t *Status );

/**
 * @brief �������, ������� �������� ��������� ���� ��� �� ������ USB ������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterLedUSBNoModemsOn( AdapterGlobalStatusStruct_t *Status );

/**
 * @brief �������, ������� ��������� ��������� ���� ��� �� ������ USB ������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterLedUSBNoModemsOff( AdapterGlobalStatusStruct_t *Status );

/**
 * @brief �������, ������� ������������ �������� ���������� � ������ ���������� USB ������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterLedUSBNoModemsBlink( AdapterGlobalStatusStruct_t *Status );

/**
 * @brief �������, ������� �������� ��������� ��� �������� � ����� ��������������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterLedRescueModeOn( AdapterGlobalStatusStruct_t *Status );

/**
 * @brief �������, ������� ��������� ��������� ��� �������� � ����� ��������������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterLedRescueModeOff( AdapterGlobalStatusStruct_t *Status );

/**
 * @brief �������, ������� ������������ �������� ����������� � ������ ��������������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterLedRescueModeBlink( AdapterGlobalStatusStruct_t *Status );

#endif //ADAPTER_LEDS_H

#ifndef CM_ADAPTER_FIRMWARE_H
#define CM_ADAPTER_FIRMWARE_H

#include <axis_dmlibs_resource.h>

#define UIMAGE_PATH_PATTERN "/tmp/mount/DM_%s/uImage"
#define VERSION_PATH_PATTERN    "/tmp/mount/DM_%s/version.ini"
#define RAMDISK_PATH_PATTERN    "/tmp/mount/DM_%s/rootdisk.%s"

#define UIMAGE_PATH_PATTERN_WIFI    "/tmp/uImage"
#define VERSION_PATH_PATTERN_WIFI   "/tmp/version.ini"
#define ROOTDISK_PATH_PATTERN_WIFI  "/tmp/rootdisk.%s"

/**
 * @brief ������� �������� � ���������� ������������ ����������� ��������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - ����� 0 � ������ ������, 0 - ���������� ������������ ����������� �� ���������, ������� �� ������ ���������� � ������ ��������� ���������� ��������
 **/
int AdapterFirmwareCheckInit( AdapterGlobalStatusStruct_t *Status );

//������� ��������� ��������
//
// @param dev - ��������� �� ��������� ����������
// @param uimagepath - ���� � ������ ����
// @param ramdiskpath - ���� � ������ ��������� �����
// @param versioninipath - ���� � ini ����� �������������
// @param flags - �������������� �����
//
// @return int: 0 - OK, <0 -fail
int AdapterFirmwareStart( dmdev_t *dev, const char *uimagepath, const char *ramdiskpath,
			  const char* versioninipath, const char *envrimentspath, int flags);

int AdapterFirmwareStop( dmdev_t *dev);

int AdapterFirmwareGetStatus( dmdev_t *dev);

int AdapterFirmwareGetProgress( dmdev_t *dev);

int AdapterFirmwareGetImageTimes(dmdev_t *dev, char *versionpath,
				 char *envirpath,time_t *uimagetotime,
				 time_t* ramdisktotime, time_t* uimageintime,
				 time_t* ramdiskintime);

// ������� ���������� � ��������
// @detailed ��������� ����� ������ �������� �� ����� � ���������� �������
// ����������
//
// @param dev - ��������� �� ��������� ����������
//
// @return int: 0 - OK, <0 - fail
int AdapterFirmwarePrepareGo(dmdev_t *dev, int mode );

#endif //CM_ADAPTER_FIRMWARE_H



#ifndef ADAPTER_SYSTEM_H
#define ADAPTER_SYSTEM_ADAPTER_H

#include <sys/types.h>
#include <adapter/global.h>


//������� ������� ��� ��������� ���� ������   ������
//����:
// Pid - ��� �������� �������� ����������� ������
// Value - ���������� ���������� ������
//������:
// 0 - ��� ��
// ����� 0 � ������ ������
//����������:
// ������� ����� ���� ������, �� �������� 2
// SIGUSER1 - ������������� ����� ������ �� ��������� ������ ���� ����� ���� �������� �����
// SIGUSER2 - ��������� �������� �� ��������� �� ��������
int AdapterSystemSendSignal( pid_t pid, int Value );

void AdapterSystemInitSignals( void );

/**
 * @brief ������� ���������������� ������� ��� ���������� ������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @param adapterdevice - ��� ���������� ������������
 * @param iinteractiveflag - ���� ������ � ������������� ������
 * @param iresquemode - ���� ����, ��� �� ����� �������� � ������ �������������� ��������
 * @param iresetvalueflag - ���� ����, ��� ���� �������� ��������� � ����� �� ���������
 * @param ilogging - ���� ������� ������ �������� ������������
 * @param iHaveConsole - ���� ����, ��� ��������� �������� � ������� �� �������
 * @param iNoInterface - ���� ����, ��� ��������� �� ������ ��������� ���������
 * @param KernelDebugLevel - ������� ������� ����
 * @return int - 0 - ��, ����� ������
 */
int AdapterSystemConfigure( AdapterGlobalStatusStruct_t *Status, int adapterdevice, int iinteractiveflag, int iresquemode,
														int iresetvalueflag, int ilogging, int iHaveConsole, int iNoInterface,
														int KernelDebugLevel );

/**
 * @brief �������, ������� ������� ��������� ��������� � ����� ������
 *
 * @param aDev - ��������� �� ��������� ����������
 * @param interractiveflag - ���� ������������� ������
 * @param iNoDaemon - ���� ������� �������� � ����� ������
 * @return int
 */
int AdapterSystemDaemonPrepare( dmdev_t* aDev, int interractiveflag, int iNoDaemon );

int adapter_keyboard_close( int console );
#endif


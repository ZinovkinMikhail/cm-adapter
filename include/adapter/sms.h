//������� ������ � SMS (���������)

//������: 2016.09.14 18:50
//�����: ��������� ����� ��� "����" ������
//�����: i.podkolzin@ross-jsc.ru

#ifndef CM_ADAPTER_INCOMMING_SMS_H
#define CM_ADAPTER_INCOMMING_SMS_H

#include <adapter/global.h>
#include <dispatcher_lib/dispatcher_lib_resource.h>

//������ ������ ��� ���������� ��������� ����� SMS ���������
#define CM_ADAPTER_SMS_COMMAND__CONNECT 					DISPATCHER_LIB_INTERFACE_INI_NAME_CONNAECT
#define CM_ADAPTER_SMS_COMMAND__REBOOT						DISPATCHER_LIB_INTERFACE_INI_NAME_REBOOT
#define CM_ADAPTER_SMS_COMMAND__SLEEP						DISPATCHER_LIB_INTERFACE_INI_NAME_SLEEP
#define CM_ADAPTER_SMS_COMMAND__WIFI_ON						"WiFiOn"
#define CM_ADAPTER_SMS_COMMAND__WIFI_OFF					"WiFiOff"
#define CM_ADAPTER_SMS_COMMAND__BAND						"Band="

/**
 * @brief ������� ������� �������� �������� SMS � ������� �� ���������
 *
 * @param StatusAdapter - ��������� �� ���������� ��������� ������� ��������
 * @param SMS - ��������� �� ��������� SMS ���������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterAddNewSMSToReciveQueue( AdapterGlobalStatusStruct_t *StatusAdapter, sms_t *SMS );

/**
 * @brief ������� �������������� SMS ���������
 *
 * @param head - ��������� �� ��������� ������ SMS
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterInitSmsInterface( struct smscontrol_s *head );

/**
 * @brief ������� ��������� �������� SMS
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @param SMSHead - ��������� �� ������ SMS
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterWorkNextSMS( AdapterGlobalStatusStruct_t *Status, smscontrol_t *SMSHead );

/**
 * @brief �������, ������� ��������� ���� �� ������������� �������� ��� �����������
 *
 * @param Status - ��������� �� ���������� ��������� ������� ��������
 * @return int - 0 ��� ���������, 1 - ���� ���� �� ���� �������, ����� 0 � ������ ������
 **/
int AdapterPhoneCheck( AdapterGlobalStatusStruct_t *Status );

/**
 * @brief ������� ������ SMS ��� �������� - ����� ������
 *
 * @param b - ��������� �� ����� ��� ������
 * @param bsize - ������ ������� ������
 * @param Event - �������, ������� �� ��������
 * @param Sn - �������� ����� ��������
 * @param SnSize - ������ ��������� ������
 * @return size_t - ���������� ���� ���������� � �����
 */
size_t AdapterEventPrintSMS( char *b, size_t bsize, DispatcherLibEvent_t *Event,
					uint8_t *Sn, size_t SnSize );
#endif

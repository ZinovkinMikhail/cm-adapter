//������� �������������� ��������� ��� ������������
#ifndef R2_ADAPTER_HELPER_H
#define R2_ADAPTER_HELPER_H

#include <adapter/global.h>

// ������� ������ �� ������ ��������� �� ������
void AdapterPrintUsage( void );

/**
 * @brief ������� ������ �� ������ ��������� �� ��������
 *
 * @param dev ��������� �� ��������� ���������
 * @return void
 **/
void AdapterPrintConsoleHelp( dmdev_t *dev );

// ������� ������ �� ������ ���������
// ��� �� ��� ���� ������
//����: 
// Satatus - ��������� �� ���������� ������ ��������
void AdapterPrintConsoleFast( AdapterGlobalStatusStruct_t *Status );

//������� ������� �������� �������� ��� ����������
//����:
// falg - ���������� �������� ��� ���
void AdapterPrintSystemInfoWail( int flag );


//������� ������� �������� ��������� � ������� ������� ���������
//��������
//����: 
// Satatus - ��������� �� ���������� ������ ��������
//�������:
// 0 - ��� ��
// ����� 0 � ������ ������
int AdapterPrintCurrentNetwork( AdapterGlobalStatusStruct_t *Status, CurrentNetworkSettingsStruct_t *CurrentNetwork );

//������� ������� �������� ��������� � ������� ��������� 
//��������
//����: 
// Satatus - ��������� �� ���������� ������ ��������
//�������:
// 0 - ��� ��
// ����� 0 � ������ ������
int AdapterPrintCurrentDevice( AdapterGlobalStatusStruct_t *Status );

//������� ������� �������� ������ IP ������� ��������� �� ��������
void AdapterPrintDeviceIP( void );

/**
 * @brief �������� � ������� ���������� �� �������� ����� ��������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - ����� 0 � ������ ������
 **/
int AdapterPrintStderrHTTPClientsInfo( AdapterGlobalStatusStruct_t *Status );

#endif //R2_ADAPTER_HELPER_H

#ifndef ADAPTER_TIMER
#define ADAPTER_TIMER

#include <dispatcher_lib/alarm_set.h>
#include <adapter/global.h>


//������� ������� ���������� ���������� �������� 
 int AdapterTimerWork( AdapterGlobalStatusStruct_t *Status );

//������� ������� ��� alarm_set
//����:
// Status - ��������� �� ���������� ��������� �������
// time_set - ��������� �� ��������� ���� ���������� ��������� ������������ �������
//�������:
// ��������� �������� � ��������
uint16_t AdapterTimerAlarmSet( AdapterGlobalStatusStruct_t *Status, struct_data *time_set );


/**
 * @brief ������� ������������� ����� � ����������� ���������� �������
 * 
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int: 0 - OK, <0 -fail
 */
int AdapterTimerClockInit( AdapterGlobalStatusStruct_t *Status );

/**
 * @brief ������� ��������� ������� � ���� � ����
 * 
 * @param dev - �������� ����������
 * @param date - ��������� � �����
 * @return int: 0 - OK, <0 - fail
 */
int AdapterTimerSetTime(dmdev_t *dev,struct_data *date);

int AdapterTimerGetTime(dmdev_t *dev,struct_data *date);

int AdapterTimerGetAlarm(dmdev_t *dev,struct_data *date);

int AdapterTimerSetAlarm(dmdev_t *dev,struct_data *date);

/**
 * @brief ������� ��������� ��������� ������ ������� �� ����������������� ������
 *
 * @param Status ...
 * @return int
 */
int AdapterTimerGetStatusFlags( AdapterGlobalStatusStruct_t *Status );

/**
 * @brief �������, ������� ���������� ��������� ���� ������� � ����������������� ������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 */
int AdapterTimerPutStatusFlags( AdapterGlobalStatusStruct_t *Status );

int AdapterTimerSyncTime(dmdev_t *dev);
#endif

#ifndef CM_ADAPTER_MODULE_H
#define CM_ADAPTER_MODULE_H

#include <axis_dmlibs_resource.h>


/**
 * @brief ������������� ���� ������� �-��, ������ ��������������� ��������
 * � ���������� ���������� ���������� ����������� dm_dev_xxx
 *
 * 
 * @param dev ��������� �� ��������� �-��
 * @return 0 - �� ����� ������
 **/
int AdapterInitDevicesModules( dmdev_t *dev);

/**
 * @brief ������������� ������� ��� ����������� �-��
 *
 * @param dev ��������� �� ��������� ����������� �-��
 * @return 0 - �� ����� ������
 **/
int AdapterInsertModule( dmdev_ctrl_hw_t *dev);

/**
 * @brief ��������������� ���� ������� �-��, ������ ��������������� �������� � ����������
 *
 * @param dev ��������� �� ��������� �-��
 * @return 0 - �� ����� ������
 **/
int AdapterRemoveDevicesModules( dmdev_t *dev );

/**
 * @brief �������� ������� ��� ����������� �-��
 *
 * @param dev ��������� �� ��������� ����������� �-��
 * @return 0 - �� ����� ������
 **/
int AdapterRemoveModule( dmdev_ctrl_hw_t *dev );
#endif //CM_ADAPTER_MODULE_H


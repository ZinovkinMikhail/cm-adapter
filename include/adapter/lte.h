// �������� ��� ������� ������ � LTE �����������

// ������: 2019.03.09 13:40
// �����: ��������� ����� �� "����" ������
// �����: i.podkolzin@ross-jsc.ru

#ifndef ADAPTER_DEVICE_LTE_H
#define ADAPTER_DEVICE_LTE_H

#include <axis_dmlibs_resource.h>

#include <adapter/global.h>

/**
 * @brief �������, ������� ���������� ������� ��� LTE ������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @param Command - ������� ������� ����� ���������� ������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 */
int AdapterLTESendCommand( AdapterGlobalStatusStruct_t *Status, uint32_t Command );

/**
 * @brief �������, ������� ���������� ����������� � USB LTE ���������
 *
 * @param Dev - ��������� �� ��������� ����������
 * @param power - ���� ���������: 1 - ��������, 0 - ���������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterLTEPower( dmdev_t *Dev, int power );

#endif

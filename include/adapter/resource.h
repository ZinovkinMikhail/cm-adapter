//����� ����� �������� ������ ������� ��� ������ ����������� �������

#ifndef R2_ADAAPTER_RESOURCE_H
#define R2_ADAAPTER_RESOURCE_H

#include <axis_keys.h>

//��������� ��������� ������
#define ADAPTER_HELPER__PRINT_USAGE_ARG					'?'	
			//������ �� ������ �����
#define ADAPTER_HELPER__RESET_DEFAULT_ARG				'r'	
			//����� ��������
#define ADAPTER_HELPER__RESCUE_MODE_ARG					'R'
			//������� � ����� ��������������
#define ADAPTER_HELPER__INTERACTIVE_START_ARG 			'i'
			//����� � ������������� ������
#define ADAPTER_HELPER__CONSOLE_START_ARG				'c'
			//����� � ��������
#define ADAPTER_HELPER__NO_INTERFACE_ARG				'C'
			//�� ��������� ����������� ���������
#define ADAPTER_HELPER__ADAPTER_DEVICE_ARG				'a'
			//��� ��������
#define ADAPTER_HELPER__ALONE_ARG						'l'
			//������ ��� ����������
#define ADAPTER_HELPER__NO_DAEMON_ARG					'D'
			//�� ������� � ����� ������
#define ADAPTER_HELPER__KERNEL_DEBUG_LEVEL_ARG			'k'
			//������� ������� ���� ��� ������ ��������

//������� ����������
#define ADAPTER_HELPER__PRINT_USAGE_COMMAND				"help" 
			//������ ������
#define ADAPTER_HELPER__RESET_DEFAULT_COMMAND			"reset"
			//����� ��������
#define ADAPTER_HELPER__INTERACTIVE_START_COMMAND		"interactive"
			//�������������
#define ADAPTER_HELPER__NO_INTERFACE_COMMAND			"interface"
			//�� ��������� ���������
#define ADAPTER_HELPER__CONSOLE_START_COMMAND			"console"
			//� ��������
#define ADAPTER_HELPER__ADAPTER_DEVICE_COMMAND			"adapter"
			//��� ��������
#define ADAPTER_HELPER__RESCUE_MODE_COMMAND				"rescue"
			//������� �  ����� �������������� ��� ������
#define ADAPTER_HELPER__ALONE_COMMAND					"log"
			//������ ��� ����������
#define ADAPTER_HELPER__NO_DAEMON_COMMAND				"daemon"
			//�� ������� � ����� ������
#define ADAPTER_HELPER__KERNEL_DEBUG_LEVEL_COMMAND		"kernel"

#define ADAPTER_DEFAULT_KERNEL_DEBUG_LEVEL				DM_KERNEL_DEBUG_LEVEL
#define ADAPTER_RESQUE_KERNEL_DEBUG_LEVEL				17

//������� ����� ��� ���������
#define ADPATER_HELPER__ADAPTER_INPUT_FILE_ARG			'I'
#define ADAPTER_HELPER__ADAPTER_INPUT_FILE_COMMAND		"input"

//����� ������� �� ������ ������������ � ������ ���������
#define ADPATER_HELPER__ADAPTER_INPUT_FILE_MODE			"mode.txt"
#define ADPATER_HELPER__ADAPTER_INPUT_FILE_STATUS		"status.txt"
#define ADPATER_HELPER__ADAPTER_INPUT_FILE_NETWORK		"network.txt"
#define ADPATER_HELPER__ADAPTER_INPUT_FILE_QCMD		"qcmd.txt"
#define ADAPTER_HELPER__ADAPTER_INPUT_FILE_NAND_TEST_LOG		"nand_test.txt"
#define ADAPTER_HELPER__ADAPTER_INPUT_FILE_NAND_DIAGNOSTIC_LOG		"diagnostic.txt"
#define ADAPTER_HELPER__ADAPTER_INPUT_FILE_FIRMWARE		"firmware.txt"

#define ADPATER_HELPER__QCMD_COMMAND_SWITCH_USB_HOST_S		"SwitchUsbHost"
#define ADPATER_HELPER__QCMD_COMMAND_SWITCH_USB_DSP_S		"SwitchUsbDsp"
#define ADPATER_HELPER__QCMD_COMMAND_SWITCH_USB_HOST_D		0x01
#define ADPATER_HELPER__QCMD_COMMAND_SWITCH_USB_DSP_D		0x02
#define ADPATER_HELPER__QCMD_COMMAND_MASK					0x0F

#define ADPATER_HELPER__QCMD_COMMAND_FREE_NAND_S		"FreeNand"
#define ADPATER_HELPER__QCMD_COMMAND_GET_NAND_S		"GetNand"
#define ADPATER_HELPER__QCMD_COMMAND_FREE_NAND_D	0x04
#define ADPATER_HELPER__QCMD_COMMAND_GET_NAND_D		0x08


#define ADPATER_HELPER__QCMD_COMMAND_BLOCK_ETHER_S              "BlockEthernet"
#define ADPATER_HELPER__QCMD_COMMAND_UNBLOCK_ETHER_S            "UnBlockEthernet"
#define ADPATER_HELPER__QCMD_COMMAND_BLOCK_ETHER_D			0x03
#define ADPATER_HELPER__QCMD_COMMAND_UNBLOCK_ETHER_D		0x05

#define ADPATER_HELPER__QCMD_COMMAND_INTERFACE_READY_100_S              "InterfaceReady"
#define ADPATER_HELPER__QCMD_COMMAND_INTERFACE_READY_D		0x06
//����� ������� �� ������ ������������ �� ����������
// #define ADPATER_HELPER__ADAPTER_INPUT_FILE_MODE_			"mode.txt"
// #define ADPATER_HELPER__ADAPTER_INPUT_FILE_STATUS		"status.txt"
// #define ADPATER_HELPER__ADAPTER_INPUT_FILE_NETWORK		"network.txt"

#define ADAPTER_INTERFACE_STATUS_TXT_PAGE				ADPATER_HELPER__ADAPTER_INPUT_FILE_STATUS
#define ADAPTER_INTERFACE_MODE_TXT_PAGE					ADPATER_HELPER__ADAPTER_INPUT_FILE_MODE
#define ADAPTER_INTERFACE_NETWORK_TXT_PAGE				ADPATER_HELPER__ADAPTER_INPUT_FILE_NETWORK
#define ADAPTER_INTERFACE_LOG_TXT_PAGE					"logfile.txt"

#define ADAPTER_HTTPD_INTERFACE_SUPPER_USER				"root"
#define ADAPTER_HTTPD_INTERFACE_SUPPER_PASSWD			"E yfc lkbyyst herb!"


//����� �������� SSL ���������� 
//�� ������ ��� i386 ����������� ��� ����������
#ifdef I386

#define AXIS_INTERFACE_SSL_CA_LIST 				"/etc/ssl/root.pem"
#define AXIS_INTERFACE_SSL_HOST					"localhost"
#define AXIS_INTERFCAE_SSL_RANDOM  				"/etc/ssl/random.pem"
#define AXIS_INTERFACE_SSL_KEYFILE 				"/etc/ssl/server.pem"
#define AXIS_INTERFACE_SSL_KEYFILE_PASSWORD 	"password"
#define AXIS_INTERFACE_SSL_DHFILE 				"/etc/ssl/dh1024.pem"

#endif

#ifdef TMS320DM355

#define AXIS_INTERFACE_SSL_CA_LIST 				"/etc/ssl/root.pem"
#define AXIS_INTERFACE_SSL_HOST					"localhost"
#define AXIS_INTERFCAE_SSL_RANDOM  				"/etc/ssl/random.pem"
#define AXIS_INTERFACE_SSL_KEYFILE 				"/etc/ssl/server.pem"
#define AXIS_INTERFACE_SSL_KEYFILE_PASSWORD 	"password"
#define AXIS_INTERFACE_SSL_DHFILE 				"/etc/ssl/dh1024.pem"

#endif

//����� ������������� �����
#define ADAPTER_NOT_SAVE_RECORDER_DATA			(1 << 0)
#define ADAPTER_NOT_SAVE_NETWORK_DATA				(1 << 1)


//���� ����� ��� ����������� �� �� ���������� ���� - ��������� �� �� ���������
#ifndef AXIS_INTERFACE_SSL_CA_LIST

#define AXIS_INTERFACE_SSL_CA_LIST 				"/etc/ssl/root.pem"
#define AXIS_INTERFACE_SSL_HOST					"localhost"
#define AXIS_INTERFCAE_SSL_RANDOM  				"/etc/ssl/random.pem"

#define AXIS_INTERFACE_SSL_KEYFILE 				"/etc/ssl/server.pem"
#define AXIS_INTERFACE_SSL_KEYFILE_PASSWORD 	"password"
#define AXIS_INTERFACE_SSL_DHFILE 				"/etc/ssl/dh1024.pem"

#endif


//���� ��������� adapter � ����������� �� ����������� �������
#define ADAPTER_LOCK_FILE					"/var/lock/adapter.lck"

#ifndef ADAPTER_LOCK_FILE
#warning - ���� ���������� �� ��������� ��� ����� �����������
#error - ���� ���������� �� ��������� ��� ����� �����������
#endif
#define ADAPTER_CONSOLE_FILE_NULL			"/dev/null"
//���� ������� ��� ����������� � ������������� �����������
#ifdef I386
#define ADAPTER_CONSOLE_FILE				"/dev/null"
#else
#ifdef i386
#define ADAPTER_CONSOLE_FILE				"adapter.log"
#endif
#endif

#ifdef TMS320DM365
#define ADAPTER_CONSOLE_FILE				"/tmp/adapter.log"
#endif

#ifdef OMAP3530
#define ADAPTER_CONSOLE_FILE				"/tmp/adapter.log"
#endif

#ifndef ADAPTER_CONSOLE_FILE
#warning ���� ������� �������� ��� ���������� � �������� �� ���������
#define ADAPTER_CONSOLE_FILE				"/tmp/adapter.log"
#endif

#define ADAPTER_KERNEL_FILE				"/proc/kmsg"

#ifndef ADAPTER_CONSOLE_FILE
#warning - ���� �����/������ �� ��������� ��� ����� �����������
#error - ���� �����/������ �� ��������� ��� ����� �����������
#endif

//������� ���������� - �������� � �������
#define ADAPTER_KEY__USER_HELP							kb_h	//�������� ���� �� ������
#define ADAPTER_KEY__USER_HELLO						kb_F2	//�������� �� ������ ���������� �������
#define ADAPTER_KEY__USER_SYSTEM_INFO					kb_i	//���������� � �������
#define ADAPTER_KEY__WAIL_SYSTEM_INFO					kb_I	//����������� ���������� � �������
#define ADAPTER_KEY__USER_EXIT							kb_x	//����� �� ���������
#define ADAPTER_KEY__USER_RECORDER_STATUS				kb_r	//�������� �� ������ ������ ����������
#define ADAPTER_KEY__USER_RECORDER_STATUS_WAIL			kb_t	//���������� ������ ���������� � ��������������
#define ADAPTER_KEY__VIEW_MODE_SCREEN					kb_m	//�������� ��������� ����������
#define ADAPTER_KEY__VIEW_NETWORK_INFO					kb_n	//�������� ���������� � ����
#define ADAPTER_KEY__VIEW_NETWORK_SCREEN				kb_N	//��������� ��������� ��������
#define ADAPTER_KEY__RESCUE_MODE						kb_R	//��������� ������� � ����� �������� �� ���������
#define ADAPTER_KEY__VIEW_ACCSTAT						kb_a	//�������� ������ ���
#define ADAPTER_KEY__VIEW_ACCSTAT_WAIL					kb_A	//���������� ������ ��� � ��������������
#define ADAPTER_KEY__VIEW_DEVSTAT						kb_c	//�������� ������ ������� ����������
#define ADAPTER_KEY__INTERFACE_SERVER_PRINT			kb_s	//�������� ������ HTTP ��������
#define ADAPTER_KEY__INTERFACE_SERVER_PRINT_WAIL		kb_S	//���������� ������ HTTP �������� � ��������������
#define ADAPTER_KEY__VIEW_HENDELS						kb_f	//�������� ������ �������� ��������
#define ADAPTER_KEY__VIEW_HENDELS_WAIL					kb_F	//���������� ������ �������� �������� � ��������������
#define ADAPTER_KEY__INC_KERN_DEBUG					kb_Add	//��������� ������� ��������� ����
#define ADAPTER_KEY__DEC_KERN_DEBUG					kb_Sub	//��������� ������� ��������� ����
#define ADAPTER_KEY__INTERFACE_DEBUG_LOG				kb_z	//???
#define ADAPTER_KEY__INTERFACE_ERR_LOG					kb_Z	//???
#define ADAPTER_KEY__INTERFACE_EVENT_PRINT				kb_e	//�������� ������ �������
#define ADAPTER_KEY__INTERFACE_PING					kb_p	//��������� ������� ping �� ������� �����������
#define ADAPTER_KEY__WIFI_ON							kb_w	//�������� WiFi �������
#define ADAPTER_KEY__WIFI_OFF							kb_W	//��������� WiFi �������
#define ADAPTER_KEY__LTE_ON								kb_l	//�������� LTE adapter
#define ADAPTER_KEY__LTE_OFF							kb_L	//��������� LTE �������
#define ADAPTER_KEY__RECORDER_ON						kb_u	//�������� Recorder
#define ADAPTER_KEY__RECORDER_OFF						kb_U	//��������� Recorder
#define ADAPTER_KEY__USB_RESET_1						kb_1	//���������� ����� ��� ������� ����� USB
#define ADAPTER_KEY__USB_RESET_2						kb_2
#define ADAPTER_KEY__USB_RESET_3						kb_3
#define ADAPTER_KEY__USB_RESET_4						kb_4
#define ADAPTER_KEY__USB_RESET_5						kb_5
#define ADAPTER_KEY__USB_RESET_6						kb_6
#define ADAPTER_KEY__USB_RESET_7						kb_7
#define ADAPTER_KEY__USB_RESET_8						kb_8
#define ADAPTER_KEY__USB_RESET_9						kb_9
#define ADAPTER_KEY__NET_MODE_AUTO						kb_TwoPoints
#define ADAPTER_KEY__NET_MODE_3G						kb_Bellow
#define ADAPTER_KEY__NET_MODE_LTE						kb_Above
#define ADAPTER_KEY__NET_PPP_CONNECT					kb_Semicolon
#define ADAPTER_KEY__SLEEP_TIME_PRINT					kb_b	//������ ����������� ������� �� ��������� ��������
#define ADAPTER_KEY__SLEEP_TIME_PRINT_WAIL				kb_B	//����������� ������ ����������� ������� �� ��������� ��������

//�������� �� ������ ���������
#define ADAPTER_TIMEOUT__RECORDER_STATUS				0x0000000F
			//����� ���������� ������� ��������� ����� ������ 
			//��� �� ������ � ��� �� ����

#define ADPATER_TIMEOUT__CRITICAL_THREAD				0x00000007
			//����� ������ ����������� �������

#define ADAPTER_TIMEOT__SYSTEM_INFO						0x0000000F
			//����� ���������� ��������� ���������� � �����

#define ADAPTER_TIMEOUT__ACC_STATUS						0x0000001F

#define ADAPTER_TIMEOUT__DIAG_STATUS						0x000000FF
			//����� ���������� ��� � ����� �������

//���� ��������� ��������� � ������� �� ����� ��������
#define ADAPTER_SOCKET_TO_DISPATCHER_FILE				DISPATCHER_LIB_SOCKET_TO_ADAPTER_FILE
#define ADAPTER_SOCKET_TO_DISPATCHER_FILE2				DISPATCHER_LIB_SOCKET_TO_ADAPTER_FILE2

//����������
#define ADAPTER_INTERFACE_NETWORK_INTERFACE_WIFI		"wlan0"
#define ADAPTER_INTERFACE_NETWORK_INTERFACE_ETH			"eth0"


//��������� ��� ini ������

#define ADAPTER_INTERFACE_INI__ALARM_SHABLON_STATUS		"%03d_Alarm"
#define ADAPTER_INTERFACE_INI__ALARM_SHABLON_STATUS_TIME "%03d_Alarm.Time"
#define ADAPTER_INTERFACE_INI__ALARM_SHABLON_STATUS_VALUE "%03d_Alarm.Value"

//��� ���������
#define PROTOCOL_TYPE_TCP_IP			(0<<0)
#define PROTOCOL_TYPE_PPPOE				(1<<0)
#define PROTOCOL_TYPE_PPTP				(2<<0)

//���� ip
#define CM_C_IP_TYPE_STATIC				( 1 << 0 )
#define CM_C_IP_TYPE_DHCP					( 0 << 0 )
//������� ����������
#define CM_C_STATUS_DISCONNECTED			( 0 << 0 )
			   //���������
#define CM_C_STATUS_CONNECTED					( 1 << 0 )
			   //����������
#define CM_C_STATUS_UNPLUG						( 1 << 1 )
			   //������ �� ���������
#define CM_C_DHCP_NO_IP							( 1 << 2 )
			   //�� �������� ip �� ���� �������

#define REC_SETS_REQUEST_TIMEOUT			108000

#define PRINT_WAIL_REC_STATUS					(1 << 0)
#define PRINT_WAIL_ADAPTER_STATUS				(1 << 1)
#define PRINT_WAIL_ACC_STATUS					(1 << 2)
#define PRINT_WAIL_SYSTEMINFO_STATUS				(1 << 3)
#define PRINT_WAIL_SYSTEMFILE_STATUS				(1 << 4)
#define PRINT_WAIL_SERVER_STATUS				(1 << 5)
#define PRINT_WAIL_SLEEP_TIME_STATUS			(1 << 6)


#define QT_IPADDRESS			"127.0.0.1"
#define QT_PORT_SSL			65535
#define QT_PORT_HTTP			65534
#define QT_MONITOR_PORT		65533
#define QT_LOGIN				"12"
#define QT_PASS				"jnltk"

#endif //R2_ADAAPTER_RESOURCE_H


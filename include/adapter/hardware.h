#ifndef CM_ADAPTER_HARD_H
#define CM_ADAPTER_HARD_H

#include <adapter/global.h>



//������ � ������������ ��� ������ ��������

//������� �� ������
#define DM365_CM_DEVICE_DRIVER					"dm365_dev_cm"
#define DM365_BANJO_DEVICE_DRIVER				"dm365_dev_banjo"
#define DM365_BANJO_DAVINCI_EMAC_DRIVER			"davinci_emac"

#define DM365_DEVICE_DRIVER_OPS_CM			"dev=CCM"
#define DM365_DEVICE_DRIVER_OPS_BANJO		"dev=BANJO"

//��� ����� ����� ���
#define DM365_CM_USB_DRIVER						"musb_hdrc"
#define DM365_CM_CICADA_USB_DRIVER				"cicada_usb"

//��� ��� ������ �������������� �������
#define MOUNTS_PROC_FILE						"/proc/mounts"

//���������� ��� ���������
#define USBFS_MOUNT_TYPE						"usbfs"

//���������� ���� ���������
#define USBFS_MOUNT_TARGET						"/proc/bus/usb/"

#define DM365_USB_MINMAL_MAJOR_NUMBER			UD2_CICADA_USB_CHAR_MAJOR
#define DM365_USB_MAXISMAL_MAJOR_NUMBER			(UD2_CICADA_USB_CHAR_MAJOR + UD2_CICADA_USB_CHAR_MAX_DEVICES)

//��� ������� ������� ���
#define DM365_CM_USBDEV_NAME 					"CICADA-CM" 		//������-��
#define DM365_BANJO_USBDEV_NAME					"BANJO"
#define DM365_BANJO_USBDEV_NAME1					"BANJO-1"
#define DM365_BANJO_USBDEV_NAME2					"BANJO-2"
#define CICADA_USB_DEVICE_FILE					"/proc/driver/cicada-usb/devices"

//����� � ��������, ��� ����� �� ����� ����� ����������� ���������� �� USB
#define ADPATER_USB_DEVICE_WAITE_TIMEOUT		20

#define ALARM_PACK_REGET_TRY					3



//������� ������� �������������� ������ � ������������
//dm365 �� ���������� Texas TMS320DM365
//����:
// Status - ��������� �� ��������� ����������� ������� ���������
//�������:
// ���� �������� �� ������� ���, ��� ��� ��� ������ ������
// ��������� � ������������ ����� ��������� ������ �� ������� 
// � �������� ����������
int AdapterGlobalDM365Init( AdapterGlobalStatusStruct_t *Status );


//������� ������� ����������������� ������� ���
// ������ ��� �������� ������� � ������ �������� 
// ���������� CC5515
//����:
// dev - ������ ��������� ���������� �������� ������������
// sn - ������ ���� ��������� �������� �����
// size - ������ ������
// AdapterDevice - ��� ��������� � ������� �� ��������
//�������:
//	�������� ����� ���������
//	����� 0 � ������ ������
int AdapterGlobalUsbSystemInit( int dev, unsigned char* sn, size_t size, int *AdapterDevice );

//������� ��������� ������� � �2� ������
//����:��������� � �����
//�������: 0 �� ����� �����
int AdapterSystemTimeSet( struct_data *date );

//������� ��������� ������� � �2� ������
//����:��������� � �����
//�������: 0 �� ����� �����
int AdapterSystemTimeGet( struct_data *date );

/**
 * @brief �������, ������� ��� ������ ������� ����� �� ������ �������� � ���������
 * ���������� ���������� � ���������� ����������, ��� ����, ���� �������� �����������
 * ������������ ���������� ���������� ���������� (���� �� ������������ � ��������)
 * �� ��������� � ������������� ������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� ��������, ����� 0 � ������ ������ (AXIS_ERROR_BUSY_DEVICE -  ����� �� ����������)
 */
int AdapterGlobalCanWorkWite( AdapterGlobalStatusStruct_t *Status );

#endif


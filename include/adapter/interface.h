//���� � ����������� ������� ���������� �� ��������� ����� HTTP/HTTPS

//�����: ��������� ����� ��� "����"
//�����: i.podkolzin@ross-jsc.ru
//������: 2010.09.15 13:55

#ifndef ADAPTER_HTTP_INTERFACE_H
#define ADAPTER_HTTP_INTERFACE_H

#include <stdio.h>

#include <axis_ssl_httpi_server.h>

#include <adapter/global.h>

#define OUTPUT_LOG_FILE "/tmp/interface_debug"
#define OUTPUT_ERR_FILE "/tmp/interface_error"

#define TIMEOUT_EXCHANGE_INTERFACE	10

//������� ������� �������� �������� status.txt
//����
//	sock - ��������� �� ��������� ������ ����� ������� ��������� ����������
//	Requests - ��������� �� ��cc�� �������� �������
//	Count - ���������� ��������� � ������ �������
//  tCount - ��������� �� ������� ������ ������� ������ ������� ���� ������� ����������
//�������
//���������� ���� ���������� ������� ��� < 0 � ������ ������
int AdapterInterfaceStatusTxtPrint( 
				axis_ssl_wrapper_ex_sock *sock, 
				axis_httpi_request *Requests, 
				int Count,
				unsigned int *tCount );

//������� ������� �������� �������� mode.txt
//����
//	sock - ��������� �� ��������� ������ ����� ������� ��������� ����������
//	Requests - ��������� �� ��cc�� �������� �������
//	Count - ���������� ��������� � ������ �������
//  tCount - ��������� �� ������� ������ ������� ������ ������� ���� ������� ����������
//�������
//���������� ���� ���������� ������� ��� < 0 � ������ ������
int AdapterInterfaceModeTxtPrint( 
				axis_ssl_wrapper_ex_sock *sock, 
				axis_httpi_request *Requests, 
				int Count,
                unsigned int *tCount );


//������� ������� �������� �������� network.txt
//����
//	sock - ��������� �� ��������� ������ ����� ������� ��������� ����������
//	Requests - ��������� �� ��cc�� �������� �������
//	Count - ���������� ��������� � ������ �������
//  tCount - ��������� �� ������� ������ ������� ������ ������� ���� ������� ����������
//�������
//���������� ���� ���������� ������� ��� < 0 � ������ ������
int AdapterInterfaceNetworkTxtPrint( 
				axis_ssl_wrapper_ex_sock *sock, 
				axis_httpi_request *Requests, 
				int Count,
                unsigned int *tCount );


//������� ������� �������� �������� network.txt
//����
//	sock - ��������� �� ��������� ������ ����� ������� ��������� ����������
//	Requests - ��������� �� ��cc�� �������� �������
//	Count - ���������� ��������� � ������ �������
//  tCount - ��������� �� ������� ������ ������� ������ ������� ���� ������� ����������
//�������
//���������� ���� ���������� ������� ��� < 0 � ������ ������
int AdapterInterfaceLogTxtPrint( 
				axis_ssl_wrapper_ex_sock *sock, 
				axis_httpi_request *Requests, 
				int Count,
                unsigned int *tCount );

//������� ������� �������� �������� �������� ������
//����
//	sock - ��������� �� ��������� ������ ����� ������� ��������� ����������
//	Requests - ��������� �� ��cc�� �������� �������
//	Count - ���������� ��������� � ������ �������
//  tCount - ��������� �� ������� ������ ������� ������ ������� ���� ������� ����������
//�������
//���������� ���� ���������� ������� ��� < 0 � ������ ������
int AdapterInterfaceUploadPrint( 
				axis_ssl_wrapper_ex_sock *sock, 
				axis_httpi_request *Requests, 
				int Count,
                unsigned int *tCount );

//������� ������� �������� �������� �������� ������
//����
//	sock - ��������� �� ��������� ������ ����� ������� ��������� ����������
//	Requests - ��������� �� ��cc�� �������� �������
//	Count - ���������� ��������� � ������ �������
//�������
//���������� ���� ���������� ������� ��� < 0 � ������ ������
int AdapterInterfaceMonitoringPrint( 
				axis_ssl_wrapper_ex_sock *sock, 
				axis_httpi_request *Requests, 
				int Count,
                unsigned int *tCount );

//������� ������� �������� �������� �������� ������
//����
//	sock - ��������� �� ��������� ������ ����� ������� ��������� ����������
//	Requests - ��������� �� ��cc�� �������� �������
//	Count - ���������� ��������� � ������ �������
//�������
//���������� ���� ���������� ������� ��� < 0 � ������ ������
int AdapterInterfaceTestPrint( 
				axis_ssl_wrapper_ex_sock *sock, 
				axis_httpi_request *Requests, 
				int Count,
                unsigned int *tCount );

//������� ��������� ��������� ������ �� ����������
//����:
// Request - ��������� �� ������ ������� ��������
// Count - ���������� ��������� � ��������
// Status - ��������� �� ���������� ��������� ������� 
//�������:
// 0 - �������� ����� �� ������
// 1 - �������� ����� ������
// ����� 0 � ������ ������
int AdapterInterfaceCheckSNRequest( axis_httpi_request *Requests, size_t Count,
									AdapterGlobalStatusStruct_t *Status );

//������� ������� �������� �������� status.txt
//����
//	sock - ��������� �� ��������� ������ ����� ������� ��������� ����������
//	Requests - ��������� �� ��cc�� �������� �������
//	Count - ���������� ��������� � ������ �������
//  tCount - ��������� �� ������� ������ ������� ������ ������� ���� ������� ����������
//	Status - ��������� �� ���������� ��������� ������ ��������
//�������
//���������� ���� ���������� ������� ��� < 0 � ������ ������
int AdapterInterfaceAdapterStatusTxtPrint( 
				axis_ssl_wrapper_ex_sock *sock, 
				axis_httpi_request *Requests, 
				int Count,
				unsigned int *tCount,
				AdapterGlobalStatusStruct_t *Status );

//������� ������� �������� �������� status.txt ��� ���������
//����
//	sock - ��������� �� ��������� ������ ����� ������� ��������� ����������
//	Requests - ��������� �� ��cc�� �������� �������
//	Count - ���������� ��������� � ������ �������
//  tCount - ��������� �� ������� ������ ������� ������ ������� ���� ������� ����������
//	Status - ��������� �� ���������� ��������� ������ ��������
//�������
//���������� ���� ���������� ������� ��� < 0 � ������ ������:
// - AXIS_ERROR_TIMEOUT_EVENT - �� �������� �� �������� (409 ������)
// - AXIS_ERROR_BUSY_DEVICE - �� ��������� �� �������� (������� NoDevice)
int AdapterInterfaceRecorderStatusTxtPrint( 
				axis_ssl_wrapper_ex_sock *sock, 
				axis_httpi_request *Requests, 
				int Count,
				unsigned int *tCount,
				AdapterGlobalStatusStruct_t *Status,struct Recorder_s **recorder );

//������� ������� �������� �������� default ��������� �����
//�������, ��� � ��� ����� ���!!!
//����
//	sock - ��������� �� ��������� ������ ����� ������� ��������� ����������
//	Requests - ��������� �� ��cc�� �������� �������
//	Count - ���������� ��������� � ������ �������
//  tCount - ��������� �� ������� ������ ������� ������ ������� ���� ������� ����������
//�������
//���������� ���� ���������� ������� ��� < 0 � ������ ������
int AdapterInterfaceDefaultTxtPrint( 
				axis_ssl_wrapper_ex_sock *sock, 
				axis_httpi_request *Requests, 
				int Count,
				unsigned int *tCount );

//������� ������� �������� �������� network.txt
//����
//	sock - ��������� �� ��������� ������ ����� ������� ��������� ����������
//	Requests - ��������� �� ��cc�� �������� �������
//	Count - ���������� ��������� � ������ �������
//  tCount - ��������� �� ������� ������ ������� ������ ������� ���� ������� ����������
//�������
//���������� ���� ���������� ������� ��� < 0 � ������ ������
int AdapterInterfaceQCmdTxtPrint(
			axis_ssl_wrapper_ex_sock *sock,
			axis_httpi_request *Requests,
			int Count,
			unsigned int *tCount );

/**
 * @brief ������� ������ ������� ����� ������
 * 
 * @param sock ��������� �� ��������� ������ ����� ������� ��������� ����������
 * @param Requests ��������� �� ��cc�� �������� �������
 * @param Count ���������� ��������� � ������ �������
 * @param tCount ��������� �� ������� ������ ������� ������ ������� ���� ������� ����������
 * @return ���������� ���� ���������� ������� ��� < 0 � ������ ������
 */
int AdapterInterfaceNandTestLogTxtPrint(
			axis_ssl_wrapper_ex_sock *sock, 
			axis_httpi_request *Requests, 
			int Count,
			unsigned int *tCount);


int AdapterInterfaceFirmware(
	axis_ssl_wrapper_ex_sock *sock,
	axis_httpi_request *Requests,
	int Count,
	unsigned int *tCount );

int AdapterInterfaceDiagnosticTxtPrint (
	axis_ssl_wrapper_ex_sock *sock,
	axis_httpi_request *Requests,
	int Count,
	unsigned int *tCount );


int AdapterInterfaceLogDspTxtPrint(
        axis_ssl_wrapper_ex_sock *sock,
        axis_httpi_request *Requests,
        int Count,
        unsigned int *tCount );

/**
 * @brief ������������� ����������������� ����������
 *
 * @param dev ��������� �� ���������� ��������� ����������
 * @param noIntterface - �� ��������� ���������, ���� ���� ����� �������
 * @return 0 - �� ����� - ������
 **/
int AdapterInterfaceInit( dmdev_t *dev, int noIntterface );

/**
 * @brief ��������������� ����������������� ����������
 *
 * @param dev ��������� �� ���������� ��������� ����������
 * @return 0 - �� ����� - ������
 **/
int AdapterInterfaceDeInit( dmdev_t *dev );

int AdaptrerInterfaceFlushSecondConnect(int ssock);

int  AdapterInterfaceGetRecorderBySN(axis_httpi_request *Requests, size_t Count,
			                AdapterGlobalStatusStruct_t *Status, struct Recorder_s ***recorder );

/**
 * @brief �������, ������ ����� � ����� ������
 *
 * @param fileout - ��������� �� ����� ������
 * @param TCount - ��������� �� ������� ������ ��� �� �� ��������
 * @param FileName - ��������� �� ��� ����� ��� ������
 * @param Message - ��������� �� ��������� ��������
 * @return int - ���������� ���������� ����, ����� 0 � ������ ������
 */
int AdapterInterfacePrintFile( FILE *Fileout, uint32_t *TCount, const char *FileName, const char *Message );

/**
 * @brief �������, ������ ����� ������� ���������� � ����� ������
 *
 * @param fileout - ��������� �� �����
 * @param TCount - ��������� �� ������� ������
 * @return int - ���������� ���������� ����, ����� 0 � ������ ������
 */
static inline int AdapterInterfacePrintLog( FILE *fileout, uint32_t *TCount )
{
	return AdapterInterfacePrintFile( fileout, TCount, OUTPUT_LOG_FILE, "INTERFACE LOG" );
}

/**
 * @brief �������, ������ ����� ������ ���������� � ����� ������
 *
 * @param fileout - ��������� �� �����
 * @param TCount - ��������� �� ������� ������
 * @return int - ���������� ���������� ����, ����� 0 � ������ ������
 */
static inline int AdapterInterfacePrintErr( FILE  *fileout, uint32_t *TCount )
{
	return AdapterInterfacePrintFile( fileout, TCount, OUTPUT_ERR_FILE, "INTERFACE ERROR" );
}

/**
 * @brief �������, ������� ���������� ������� � ��������� ������ �� ���������
 *
 * @param Status - ��������� �� ������ ���������
 * @param cmd - ������� ��� �������� � ���������
 * @param value - �������� ��� ������ �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 */
int AdapterInterfaceSendCommandAndValue( AdapterGlobalStatusStruct_t *Status, uint16_t Cmd, int Value );

/**
 * @brief �������, ������� ���������� ������� � ���������, ��� �������������� ��������
 *
 * @param Status - ��������� �� ������ ���������
 * @param cmd - ������� ��� �������� � ���������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 */
static inline int AdapterInterfaceSendCommand( AdapterGlobalStatusStruct_t *Status, int cmd )
{
	return AdapterInterfaceSendCommandAndValue( Status, cmd, 0 );
}

pid_t AdapterInterfaceStart( dmdev_t *dev, int noIntterface );

int AdapterInterfaceLcdOff( AdapterGlobalStatusStruct_t *Status );


int AdapterInterfaceLcdOn( AdapterGlobalStatusStruct_t* Status, int iNoInterface );

/**
 * @brief ������� ��������� ������� �� ������� ����������
 *
 * @param InterfacePriv - ��������� �� ��������� ������ ����������
 * @param rfds - ��������� �� ��������� ������� ��� ���������
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 */
int AdapterInterfaceWorkUnixSockets( dmdev_user_interface_t *InterfacePriv, fd_set *rfds,
									 AdapterGlobalStatusStruct_t *Status );

/**
 * @brief �������, ������� ����������� ������ ���������� ��� ������������ �������
 *
 * @param InterfacePriv - ��������� �� ��������� ������ ����������
 * @param rfds - ��������� �� ������ ���������� ��� �������
 * @param maxsel - ������������ ��������� � �������
 * @return int - ����� ������������ ��������� � �������
 */
int AdapterInterfaceSetHendelUnixSocket( dmdev_user_interface_t *InterfacePriv, fd_set *rfds, int maxsel );

#endif //ADAPTER_HTTP_INTERFACE_H

//��� ��� �������� ������������� ������� HTTP � ����� �������


#ifndef ADAPTER_HTTPD_SERVER_H
#define ADAPTER_HTTPD_SERVER_H

#include <adapter/global.h>


//������� ��������� ������� HTTPD

//�����: ��������� ����� ��� "����"
//�����: i.podkolzin@ross-jsc.ru
//������: 2010.09.15 19:15

//������� ��������� ������ HTTP
//����:
// Status - ��������� �� ��������� ������� �������
//������:
// 0 - ������� ����������
// ����� 0 � ������ ������
int AdapterHTTPdStop( AdapterHTTPDStatusStruct_t *Status );


//���������� ������� ������ �������
//����:
// Status - ����������� ���������� ��������� ������� 
//�������:
// 0 - ��� ������� ����������
// ����� 0 � ������ ������
int AdapterHTTPdStart( AdapterHTTPDStatusStruct_t *HTTPD, 
				   CurrentNetworkSettingsStruct_t *CurrentNetwork, 
				   int AdapterDevice,
				 time_t  *LastInterfaceQuire, int iQt	);

//������� ������� ���������� IP ����� �������
//� ��������� �������� �� �������
//����:
// pid - ������������� ����������� ������
//	��� ��������� ������� �������������� ����������� �������
//	axis_getpid()
// Status - ��������� �� ���������� ��������� ������� ��������
// NetSettings - ���������� ��������� �� ��������� �������� ��� ������� ��������
//�������:
// ���������� ����� ������� ������� �� ������
//	��� ������� ���� �������� � ��������� ������
// 0 - �� ����� ������
unsigned int AdapterHttpdGetClientIP( pid_t pid, AdapterGlobalStatusStruct_t *Status,CurrentNetworkSettingsStruct_t** NetSettings );

//��������������� ������� ��������� ������,
//������� �������� � ������ ��������� ���������
//����:
// User - ��������� �� ����� ���� ��������� ��� ������������
// UserSize - ������ ������� ������
// Passwd - ��������� �� ����� ���� ������� ������
// passwdSize - ������ ������� ������
//������:
// ����� 0 � ������ ������
// 0 - ��� ��
int AdapterHTTPDGetPasswordCallback( char *User, int UserSize, char *Passwd, int PasswdSize, void *data );

//��������������� ������� ��������� ������,��� ������� Qt
//������� �������� � ������ ��������� ���������
//����:
// User - ��������� �� ����� ���� ��������� ��� ������������
// UserSize - ������ ������� ������
// Passwd - ��������� �� ����� ���� ������� ������
// passwdSize - ������ ������� ������
//������:
// ����� 0 � ������ ������
// 0 - ��� ��
int AdapterHTTPDGetPasswordCallbackQt( char *User, int UserSize, char *Passwd, int PasswdSize, void *data );

//��������������� ������� ��������� ������,
//������� �������� � ������ ��������� ����� ���������
//����:
// User - ��������� �� ����� ���� ��������� ��� ������������
// UserSize - ������ ������� ������
// Passwd - ��������� �� ����� ���� ������� ������
// passwdSize - ������ ������� ������
//������:
// ����� 0 � ������ ������
// 0 - ��� ��
int AdapterHTTPDGetSPasswordCallback( char *User, int UserSize, char *Passwd, int PasswdSize, void *data );


//��������������� ������� ��������� ������, ������� ���������
//IP ����� ������������� �� ����������
//����:
// ip - ���������� IP ����� � �������� ������� ����
//������:
// 0 - IP ������� - ����� ����������
// ����� 0 - ���� �� ���
int AdapterHTTPDCheckIPCallback( uint32_t ip );

//���������� ������� ������������� ���������� ����������
//������ �� ��� ���� ����� SSL
//����:
// Control - ��������� �� ��������� �������� ��� ������� �����������
// Pages - ��������� �� ������ �������� � ������� ����� �������� ��������
// Count - ���������� ��������� ������� ������� - ����������� ����� �������
// ssl - ���� ��� ������ ������ �� ���������� ��������
//�������:
// 0 - ��� ��
// ����� 0 � ������ ������
int AdapterHTTPdInterfaceInitControls( axis_ssl_httpi_control *Control, 
										axis_ssl_httpi_page_control *Pages, size_t Count,
										int ssl );

//���������� ������� ������������� ���������� ����������
//������ �� ��� ���� ����� SSL
//����:
// Control - ��������� �� ��������� �������� ��� ������� �����������
// Pages - ��������� �� ������ �������� � ������� ����� �������� ��������
// Count - ���������� ��������� ������� ������� - ����������� ����� �������
// ssl - ���� ��� ������ ������ �� ���������� ��������
//�������:
// 0 - ��� ��
// ����� 0 � ������ ������
int AdapterHTTPdInterfaceInitControlsQt( axis_ssl_httpi_control *Control, 
										axis_ssl_httpi_page_control *Pages, size_t Count,
										int ssl );

//������� ������� ��������� ������������ ���������� �����
//������� �������������� ����� ��������
//����:
// Status - ���������� ��������� �������
//������:
// ������������ ���������� ������
// 0 - � ������ ������
int AdapterHTTPdGetMaxSessionCount( AdapterGlobalStatusStruct_t *Status );

/**
 * @brief ����� ����� �������, � ����������� �� ������������ �������� ���� ��� ��� ��� ����������
 *
 * @param Status ��������� �� ���������� ���������
 * @return 0 - �� ����� - ������
 **/
int AdapterHttpdInit( AdapterGlobalStatusStruct_t *Status );


/**
 * @brief ���� ����� �������, � ����������� �� ������������ �������� ���� ��� ��� ��� ����������
 *
 * @param Status ��������� �� ���������� ���������
 * @return 0 - �� ����� - ������
 **/
int AdapterHttpdDeinit( AdapterGlobalStatusStruct_t *Status );

int AdapterHttpdCheck( AdapterGlobalStatusStruct_t *Status );

#endif //ADAPTER_HTTPD_SERVER_H

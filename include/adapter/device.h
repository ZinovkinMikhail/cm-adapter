#ifndef ADAPTER_DEVICE_H
#define ADAPTER_DEVICE_H

#include <adapter/global.h>
#define WAIT_TEST printf("wait...\n"); sleep( 1 )


//��� ������� ��� ���������
#define R8187L 									"r8187l"
#define G_ZERO 									"g_zero"
#define RT3070STA								 "rt3070sta"

#define DM9000 "dm9000"

#define DISPATCHER_DM355_DEVICE_MODULES	"dm355_device"
#define FROM_COPY 								"/usr/share/RT2870STA/RT2870STA.dat"
#define TO_COPY									 "/var/tmp/RT2870STA.dat"
#define MAX_INTERFACE_NAME					  7

//������� �������� ������� ����������
//������� : ������
// ����� 0 ������
int adapter_open_device_hendel();

//������� �������� ������� ����������
//������� : 0
// ����� 0 ������
int adapter_close_device_hendel( int hen);


//������� ������� �������� ��� ������ � �������� 
// �� ����� ���
void adapter_device_deinit(int hen, int adaptertype);

//�������� ������� ������� ��������� USB �����
//����:  ���� ������
//������� :
// 	      0 � ������ ����� � ������ ��� ������
int wifi_device_usb_on();


//���������� ��� ����������� ������ ��� ������ � wifi
//�������:
// 0 - ��� ��
// �����  - ������
int adapter_device_insert_modules( int adaptertype );


//��������� ��� ����������� ������ ��� ������ � wifi 
//�������:
// 0 - ��� ��
// �����  - ������
int adapter_device_remove_modules( int adaptertype);

int isdevice_in(char* interface, int adaptertype);

#endif

#ifndef CM_ADAPTER_DIAGNOSTIC_H
#define CM_ADAPTER_DIAGNOSTIC_H

#include <axis_dmlibs_resource.h>


int AdapterLoadDiagnostic(dmdev_t *dev,AdapterGlobalStatusStruct_t* Status);

int AdapterSaveDiagnostic(dmdev_t *dev,AdapterGlobalStatusStruct_t* Status);


#endif //CM_ADAPTER_DIAGNOSTIC_H

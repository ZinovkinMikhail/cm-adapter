//������� ��� ������ � CDMA �������

//������: 2010.10.21 15:04
//�����: ��������� ����� ��� "����"
//�����: i.podkolzin@ross-jsc.ru

#ifndef ADAPTER_CDMA_H
#define ADAPTER_CDMA_H

#include <axis_softadapter.h>

//������� ������������� ���������� ����� CDMA �����
//����:
// Settings - ��������� �� ��������� ������� ����������
//������:
// 0 - ��� ��, �� ��� �� ������, ��� ���������� ������������
// ���� 0 � ������ ������
int AdapterCDMAConnect( SoftAdapterSettingsStruct_t *Settings );


#endif //ADAPTER_CDMA_H

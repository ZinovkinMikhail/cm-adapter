//���� �� ������� ��� ������� ������� ��������� ��� �������� 
//������ ini ������ ��� ����� ����

//�����: ��������� ����� ��� "����"
//������: 2010.09.15 13:43
//�����: i.podkolzin@ross-jsc.ru


#ifndef ADAPTER_PRINTF_INI_FILES_H
#define ADAPTER_PRINTF_INI_FILES_H

#include <adapter/global.h>

/**
 * @brief ������� ������� �������� ����� ��� ����� ����������� �� ������� ������
 *
 * @param b - ��������� �� ����� ���� �������� ���������
 * @param bsize - ������ ������ ������
 * @param Status - ��������� �� ��������� ������� ��������
 * @param ClientID - ��������� �� ������ ��� ��� �������
 * @param Recorder - ��������� �� ����������, ���� ���������� ��� NULL
 * @return int - ���������� ���������� ������ � �����
 */
int AdapterIniFileSprintfStatusNetInfo( char *b, size_t bsize, 
					AdapterGlobalStatusStruct_t *Status,
					const char *ClientID, struct Recorder_s *Recorder );

//������� ������� �������� ����� ��� ����� ����������� �� 
//������ ��������
//����:
// b - ��������� �� ������ ���� �������� ��������� ������
// bsize - ������ ������� ������
// Status - ��������� �� ��������� �� �������� ��������
//�������:
// ���������� ���� ���������� � �����
int AdapterIniFileSprintfStatusDeviceList( char *b, size_t bsize, 
					AdapterGlobalStatusStruct_t *Status );

//������� ������� �������� ����� ��� ����� ����������� �� 
//������ ������������ �������
//����:
// b - ��������� �� ������ ���� �������� ��������� ������
// bsize - ������ ������� ������
// Status - ��������� �� ��������� �� �������� ��������
// AdapterDevice - ������������� �� ������� ��� ��� ���������
//�������:
// ���������� ���� ���������� � �����
int AdapterIniFileSprintfStatusAlarm( char *b, size_t bsize, 
					AdapterGlobalStatusStruct_t *Status,
					int AdapterDevice,struct Recorder_s *recorder );

//������� ������� �������� ����� ��� ����� ����������� �� 
//������� ��������� ��� ��������
//����:
// b - ��������� �� ������ ���� �������� ��������� ������
// bsize - ������ ������� ������
// Status - ��������� �� ��������� �� �������� ��������
//�������:
// ���������� ���� ���������� � �����
int AdapterIniFileSprintfStatusInfo( char *b, size_t bsize, 
					AdapterGlobalStatusStruct_t *Status );

//������� ������� �������� ����� ��� ����� ����������� �� 
//������� ������ ����
//����:
// b - ��������� �� ������ ���� �������� ��������� ������
// bsize - ������ ������� ������
// NotSave - ���� �� ����������� ������ ��� ������� ��������
//�������:
// ���������� ���� ���������� � �����
int AdapterIniFileSprintfStatusNet( char *b, size_t bsize, 
										int NotSave );

//������� ������� �������� ����� ��� ����� ����������� �� 
//������� ������ ����
//����:
// b - ��������� �� ������ ���� �������� ��������� ������
// bsize - ������ ������� ������
// adaptertype - ��� ��������
//�������:
// ���������� ���� ���������� � �����
int AdapterIniFileSprintfStatusDevInfo( char *b, size_t bsize, 
					int adaptertype, AdapterGlobalStatusStruct_t *Status );

/**
 * @brief ������� ������� �������� ����� ��� ����� ����������� �� ������ ����� �����
 * 
 * @param b ��������� �� ������ ���� �������� ��������� ������
 * @param bsize ������ ������� ������
 * @param TestNandStatus ��������� �� ���� ��������� �� ������� ������� ������
 * @return ���������� ���� ���������� � �����
 */
int AdapterIniFileSprintfNandTestGetLog(  char *b, size_t bsize, 
					  DspRecorderExchangeFlashTestStatusStruct_t* TestNandStatus );


int AdapterIniFileSprintfDiagnostic(  char *b, size_t bsize, AdapterGlobalStatusStruct_t* Status );


#endif //ADAPTER_PRINTF_INI_FILES_H

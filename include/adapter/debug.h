//������� ������� ��������� ������ � ���������

//������: 2011.10.24 17:30
//�����: ��������� ����� ��� "����"
//�����: i.podkolzin@ross-jsc.ru

#ifndef ADAPTER_DEBUG_H
#define ADAPTER_DEBUG_H

#define ADAPTER_INTERFACE_DEBUG_TXT_PAGE		"adapterdebug.txt"
#define ADAPTER_INTERFACE_KERNEL_TXT_PAGE		"kernellog.txt"
#define ADAPTER_INTERFACE_DSP_LOG_TXT_PAGE		"logfiledsp.txt"
#define ADAPTER_DEBUG_MAX_LOG_SIZE				512000 //��������� 500� (����� ��� �����)
#include <axis_pid.h>
#include <axis_time.h>
#include <axis_ssl_httpi_request.h>
#include <adapter/log.h>

#include <adapter/global.h>
#include <adapter/log.h>

enum
{
	DBG_LOW = 1,
	DBG_MID = 2,
	DBG_HIGH = 3,
};

//������ �������������� ��������� ����:
//Info :[892 ][01.01][00:00:07] Self-destruct system initialization        [Ok]
#define INFO_LOG(a,b, ... )	{INFOs(a,b,##__VA_ARGS__);\
				AdapterLogInfo(AdapterGlobalGetStatus(),\
							a,b,##__VA_ARGS__);}
							
//����������� ���������
#define LOG(a, ... )	{AdapterLogLog(AdapterGlobalGetStatus(),a,##__VA_ARGS__);}

//��������������� ������� �����������
#define PRINTLOG(a,b)	{AdapterLogBuff(AdapterGlobalGetStatus(),a,b);}

/**
 * @brief ������� ��������� � ������ �������
 *
 * @param EStatus - ��������� �� ��������� �������
 * @param PrintFlag - ����� �� ���������� ����
 * @param Number - ����� ������� ��� ������
 * @return void
 */
void LOG_EVENT( DispatcherLibEventStatus_t *EStatus, uint32_t PrintFlag, uint32_t Number );

//������ ��������� � ������ �������
//����:
// a - ��������� �� ��������� �������
// b - ���� ���� �������� �� ���� ��� ���
// c - 
#define LOG_EVENTS(a,b,c) {	char buff[65536];\
						int count = 0;\
						count = DispatcherLibEventPrintAll( buff, sizeof(buff), a, b );\
						PRINTLOG(buff,count);\
						}

// __attribute__((format (printf, 1, 2)))
static inline int INFOs( int res, const char *msg, ... )
{

	if( !msg )
	{
		return -1;
	}

	struct struct_data sd;

	char buff[256];

	sd = system_time_get();

	fprintf( stderr, "%-5s:[%-4d][%02x.%02x][%02x:%02x:%02x] ", "Info", axis_getpid(), sd.day, sd.mon, sd.hour, sd.min, sd.sec );

	va_list ap;

	va_start( ap, msg );

	vsnprintf( buff, sizeof( buff ), msg, ap );

	va_end( ap );

	int ret = fprintf( stderr, "%-60s\t %s \n", buff, res ? "[Error]" : "[Ok]" );

//    fprintf( stderr, "\033[37m");
	fflush( stderr );

	return ret;

}

//������� ��������� ������ ������� ��� ����
//�������:
// ������� �������
// ����� 0 � ������ ������
int AdapterGetKernelDebugLevel( void );

//������� ��������� ������ ������ ��� ����
//����:
// value - �������� ������� �������
//�������:
// 0 - ��� ������
// ����� 0 � ������ ������
int AdapterSetKernelDebugLevel( int value );

//������� ������� ����� �����������
//���� �� ����� � stderr � ���������������� � ����
//� ���� ��� ������ ��������� 2�� �� ������������� ��� ������� � 0
void AdapterDebugLogRotate( void );

//������� ������ ����� ����������� ��� ����������
//���� ����� HTTP(S) ��������
//����
//	sock - ��������� �� ��������� ������ ����� ������� ��������� ����������
//	Requests - ��������� �� ��cc�� �������� �������
//	Count - ���������� ��������� � ������ �������
//  tCount - ��������� �� ������� ������ ������� ������ ������� ���� ������� ����������
//�������
//���������� ���� ���������� ������� ��� < 0 � ������ ������
int AdapterDebugTxtPrint(
        axis_ssl_wrapper_ex_sock *sock,
        axis_httpi_request *Requests,
        int Count,
        unsigned int *tCount );
//������� ������ ����� ����������� ��� ����������
//���� ����� HTTP(S) ��������
//����
//	sock - ��������� �� ��������� ������ ����� ������� ��������� ����������
//	Requests - ��������� �� ��cc�� �������� �������
//	Count - ���������� ��������� � ������ �������
//  tCount - ��������� �� ������� ������ ������� ������ ������� ���� ������� ����������
//�������
//���������� ���� ���������� ������� ��� < 0 � ������ ������
int AdapterKernelDebugTxtPrint(
        axis_ssl_wrapper_ex_sock *sock,
        axis_httpi_request *Requests,
        int Count,
        unsigned int *tCount );

//������� ������� �������� � ����� ����� ��� ����� ��� ������ �������
//����
// b - ��������� �� ����� ���� ���������� ������
// size - ������ ������ ������
// Status - ��������� �� ��������� �� �������� �������
// N - ����� ������� ������� ��������
// Flag - �������� �������� ����� ��� ���
//�������:
// ���������� ����������� ����
size_t DispatcherLibEventPrintEx( char *b, size_t bsize, DispatcherLibEventStatus_t *Status,
											unsigned int N, unsigned int Flag );

#endif //ADAPTER_DEBUG_H


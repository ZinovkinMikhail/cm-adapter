#ifndef ADAPTER_SLEEP_H
#define ADAPTER_SLEEP_H

#include <adapter/global.h>

/**
 * @brief �������, ������� ��������� ����� �� ����� �������� ��� ���
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return void
 **/
void AdapterSleepCheck( AdapterGlobalStatusStruct_t *Status );

//������� ������� ���� ��������� �������
//����:
// Status - ��������� �� ���������� ��������� �������
//�������:
// ���� ���� ��� ������ ������� ������� ������� � ����������
int AdapterCommandSleep( AdapterGlobalStatusStruct_t *Status );

/**
 * @brief �������, ������� �������� � ������� ����� ��� ���������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return void
 */
void AdapterPrintTimeToSleep( AdapterGlobalStatusStruct_t *Status );

#endif //ADAPTER_SLEEP_H

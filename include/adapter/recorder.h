//��� ��� �������� � ����������

#ifndef ADAPTER_RECORDER_H
#define ADAPTER_RECORDER_H

#include <axis_ssl_httpi_download.h>

#include <dispatcher_lib/command.h>

#include <adapter/global.h>

// #include <cicada_usb_ioctl.h>

#define RECORDER_LIB_MASK	"/lib/librecorder-%s.so"
#define RECORDER_USB_INFO_NODE_MASK	"/dev/%s"

#define RECORDER_HEADER_TYPE_ID			777

#define CHAR_INFO_IOCTL_USE_STRING			_IOW('X',1,int)
#define CHAR_INFO_IOCTL_USE_STRUCT			_IOW('X',2,int)
#define CHAR_INFO_IOCTL_USE_RESET			_IOW('X',3,int)

// struct cicada_usb_event_info
// {
// 	int major;
// 	int present;
// 	int opened;
// 	int reserve;
// 	unsigned long long serial;
// 	char name[32];
// };

int AdapterRecorderRecover(AdapterGlobalStatusStruct_t *Status);
int AdapterRecorderResetErrors(AdapterGlobalStatusStruct_t *Status);
int AdapterRecorderCheckAndRecover(AdapterGlobalStatusStruct_t *Status);

/**
 * @brief ������� ������������� ����������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @param dev - ��������� �� ��������� ���������
 * @param Recorder - ��������� �� ��������� ����������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterRecorderInit( AdapterGlobalStatusStruct_t *Status, dmdev_t *dev, struct Recorder_s *Recorder );

int AdapterRecorderDeInit( dmdev_t *dev, struct Recorder_s *Recorder);
int AdapterRecorderDeinitControlRecorders( dmdev_t* dev, struct Recorder_s *Recorder);
int AdapterRecorderInitControlRecorders( dmdev_t* dev, struct Recorder_s *Recorder);
int AdapterRecorderProcessEvent( dmdev_t *dev, struct Recorder_s *head );
int AdapterRecorderRelease( struct Recorder_s *recorder);

/**
 * @brief ������� ��������� ������� ���������� ����������
 *
 * @param dev - ��������� �� ��������� ����������
 * @param recorder - ��������� �� ��������� ����������
 * @return int - 0 ��� ��, ����� 0 � ������ ������ (AXIS_ERROR_TIMEOUT_EVENT - 409 ������, AXIS_ERROR_BUSY_DEVICE - NoDevice )
 */
int AdapterRecorderGetStatus(dmdev_t *dev, struct Recorder_s *recorder);


int AdapterRecorderPutSettings(dmdev_t *dev, struct Recorder_s *recorder);
int AdapterRecorderPutSettingsDefault(dmdev_t *dev, struct Recorder_s *recorder);

int AdapterRecorderPutTimers(dmdev_t *dev, struct Recorder_s *recorder);
int AdapterRecorderPutTimersDefault(dmdev_t *dev, struct Recorder_s *recorder);

/**
 * @brief �������, ������� �������� ������ ��� ���� ����������� � �������
 *
 * @param dev - ��������� �� ��������� ���������
 * @param head - ��������� �� ������ ������ �����������
 * @return int - 0 ��� ��, ���� 0 � ������ ������
 */
int AdapterRecorderGetAllRecordersStatus( dmdev_t *dev, struct Recorder_s *head );

int AdapterRecorderCacheAllRecordersStatus(dmdev_t *dev,struct Recorder_s *head);
int AdapterRecorderCacheAllRecordersSettings(dmdev_t *dev,struct Recorder_s *head);

/**
 * @brief - ������� ������ ������� ����������, � ������������ ����������
 *
 * @param dev - ��������� �� ��������� ����������
 * @param recorder - ��������� �� ����������, � �������� ���������� ������
 * @return int - 0 ��� ��, ����� 0 � ������ ������ (AXIS_ERROR_TIMEOUT_EVENT - 409 ������, AXIS_ERROR_BUSY_DEVICE - NoDevice )
 */
int AdapterRecorderChacheRecorderStatus(dmdev_t *dev,struct Recorder_s *recorder);

int AdapterRecorderChacheRecorderSettings(dmdev_t *dev,struct Recorder_s *recorder);

int AdapterRecorderPrintAllRecordersStatusInFile( struct Recorder_s *head, int file);

struct Recorder_s** AdapterRecorderFindAndUpRecorder(struct Recorder_s *head, uint64_t sn );

int AdapterRecorderPrintStatusIni(struct Recorder_s *recorder, char *buff,size_t size);
int AdapterRecorderSprintDeviceLists( char* b, size_t bsize, struct Recorder_s *head, int count );
//TODO - ��� ������� � ������� ������ �� ��� �� ������������
int AdapterRecorderSprintFirstRecorderInfo( char* b, size_t bsize, struct Recorder_s *head, const char *Build, time_t BuildTime );

//�������, ������� �������� ������ info ��� ���������� ����������
//����:
// b - ��������� �� ����� ���� ��������
// bsize - ������ ������� ������
// recorder - ��������� �� ��������� �������� ����������
// Build - ������ ������ ��������
// BuildTime - ����� ������ ��������
//�������:
// ���������� ������������ ����
// ����� 0 � � ����� ������
int AdapterRecorderSprintRecorderInfo( char* b, size_t bsize, struct Recorder_s *recorder,
									   const char *Build, time_t BuildTime );

int AdapterRecorderStartMonitoring(axis_httpi_request *Requests,size_t *RCount,
										AdapterGlobalStatusStruct_t *Status,struct Recorder_s *recorder);

/**
 * @brief �������, ������� ���������� � ���������� ��������� ������� ����� ����� ������� RecorderLibCommandDo �� ���������� ����������
 *
 * @param Requests - ��������� �� ������ ������� ��������
 * @param RCount - ��������� �� ���������� � ����������� ��������
 * @param Status - ��������� �� ���������� ������ ��������
 * @param recorder - ��������� �� ��������� ����������
 * @param Data - ��������� �� ������ ����������
 * @param Command - ��� ������� ��� �������� � �������
 * @param sCommand - ��������� �� ������ � ��������� ������� ��� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 */
int AdapterRecorderDoSameCommand( axis_httpi_request *Requests, size_t *RCount,
									AdapterGlobalStatusStruct_t *Status, struct Recorder_s *recorder,
									recorder_data_t *Data, uint32_t Command, const char *sCommand );

static inline int AdapterRecorderApplyFromRequest( axis_httpi_request *Requests, size_t *RCount,
										AdapterGlobalStatusStruct_t *Status,struct Recorder_s *recorder)
{
	return AdapterRecorderDoSameCommand( Requests, RCount, Status, recorder, &recorder->recorder_data, ADAPTER_COMMAND__RETURN_APPLY, "apply from request" );
}

static inline int AdapterRecorderStartErase( axis_httpi_request *Requests, size_t *RCount,
										AdapterGlobalStatusStruct_t *Status, struct Recorder_s *recorder )

{
	return AdapterRecorderDoSameCommand( Requests, RCount, Status, recorder, &recorder->recorder_data, ADAPTER_COMMAND__RETURN_START_ERASE, "start erase" );
}

static inline int AdapterRecorderWakeUp( axis_httpi_request *Requests, size_t *RCount,
										AdapterGlobalStatusStruct_t *Status, struct Recorder_s *recorder )
{
	return AdapterRecorderDoSameCommand( Requests, RCount, Status, recorder, &recorder->recorder_data, ADAPTER_COMMAND__RETURN_WAKE_UP, "wake up" );
}

static inline int AdapterRecorderClearLog( axis_httpi_request *Requests, size_t *RCount,
										AdapterGlobalStatusStruct_t *Status, struct Recorder_s *recorder )
{
	return AdapterRecorderDoSameCommand( Requests, RCount, Status, recorder, &recorder->recorder_data, ADAPTER_COMMAND__RETURN_CLEAR_LOG, "clear log" );
}

static inline int AdapterRecorderClearGPSLog( axis_httpi_request *Requests, size_t *RCount,
										AdapterGlobalStatusStruct_t *Status, struct Recorder_s *recorder )
{
	return AdapterRecorderDoSameCommand( Requests, RCount, Status, recorder, &recorder->recorder_data, ADAPTER_COMMAND__RETURN_CLEAR_GPS, "clear GPS log" );
}

static inline int AdapterRecorderStartClear( axis_httpi_request *Requests, size_t *RCount,
										AdapterGlobalStatusStruct_t *Status, struct Recorder_s *recorder )
{
	return AdapterRecorderDoSameCommand( Requests, RCount, Status, recorder, &recorder->recorder_data, ADAPTER_COMMAND__RETURN_START_CLEAR, "start clear" );
}
static inline int AdapterRecorderStopRecord( axis_httpi_request *Requests, size_t *RCount,
										AdapterGlobalStatusStruct_t *Status, struct Recorder_s *recorder )
{
	return AdapterRecorderDoSameCommand( Requests, RCount, Status, recorder, &recorder->recorder_data, ADAPTER_COMMAND__RETURN_STOP_RECORD, "stop record" );
}

static inline int AdapterRecorderStartRecord( axis_httpi_request *Requests, size_t *RCount,
										AdapterGlobalStatusStruct_t *Status, struct Recorder_s *recorder )
{
	return AdapterRecorderDoSameCommand( Requests, RCount, Status, recorder, &recorder->recorder_data, ADAPTER_COMMAND__RETURN_START_RECORD, "start record" );
}

static inline int AdapterRecorderPing( axis_httpi_request *Requests, size_t *RCount,
										AdapterGlobalStatusStruct_t *Status, struct Recorder_s *recorder )
{
	return AdapterRecorderDoSameCommand( Requests, RCount, Status, recorder, &recorder->recorder_data, ADAPTER_COMMAND__RETURN_PING_ADAPTER, "pngg adapter" );
}

static inline int AdapterRecorderDisconnect( axis_httpi_request *Requests, size_t *RCount,
										AdapterGlobalStatusStruct_t *Status, struct Recorder_s *recorder )
{
	return AdapterRecorderDoSameCommand( Requests, RCount, Status, recorder, &recorder->recorder_data, ADAPTER_COMMAND__RETURN_DISCONNECTRF, "disconnect" );
}

static inline int AdapterRecorderConnect( axis_httpi_request *Requests, size_t *RCount,
										AdapterGlobalStatusStruct_t *Status, struct Recorder_s *recorder )
{
	return AdapterRecorderDoSameCommand( Requests, RCount, Status, recorder, &recorder->recorder_data, ADAPTER_COMMAND__RETURN_CONNECTRF, "connect" );
}

static inline int AdapterRecorderResetMicState( axis_httpi_request *Requests, size_t *RCount,
										AdapterGlobalStatusStruct_t *Status, struct Recorder_s *recorder )
{
	return AdapterRecorderDoSameCommand( Requests, RCount, Status, recorder, &recorder->recorder_data, ADAPTER_INTERFACE_COMMAND__RESETMICALARM, "reset mic state" );
}

static inline int AdapterRecorderNandTestWrite( axis_httpi_request *Requests, size_t *RCount,
										AdapterGlobalStatusStruct_t *Status, struct Recorder_s *recorder )
{
	return AdapterRecorderDoSameCommand( Requests, RCount, Status, recorder, &recorder->recorder_data, ADAPTER_INTERFACE_COMMAND__NAND_TEST_WRITE, "NAND test write" );
}

static inline int AdapterRecorderNandTestRead( axis_httpi_request *Requests, size_t *RCount,
										AdapterGlobalStatusStruct_t *Status, struct Recorder_s *recorder )
{
	return AdapterRecorderDoSameCommand( Requests, RCount, Status, recorder, &recorder->recorder_data, ADAPTER_INTERFACE_COMMAND__NAND_TEST_READ, "NAND test read" );
}

static inline int AdapterRecorderNandTestStatus( axis_httpi_request *Requests, size_t *RCount,
										AdapterGlobalStatusStruct_t *Status, struct Recorder_s *recorder )
{
	return AdapterRecorderDoSameCommand( Requests, RCount, Status, recorder, &recorder->recorder_data, ADAPTER_INTERFACE_COMMAND__NAND_TEST_GET_LOG, "NAND test get log" ); 
}

static inline int AdapterRecorderNandTestStop( axis_httpi_request *Requests, size_t *RCount,
										AdapterGlobalStatusStruct_t *Status, struct Recorder_s *recorder )
{
	return AdapterRecorderDoSameCommand( Requests, RCount, Status, recorder, &recorder->recorder_data, ADAPTER_INTERFACE_COMMAND__NAND_ABORT_TEST, "NAND test abort" );
}

/**
 * @brief ������� ������� ���������� ������� ��������� ��� ����������
 *
 * @param Status - ��������� �� ���������� ��������� ������� ��������
 * @param recorder - ��������� �� ��������� ����������� ����������
 * @param Data - ��������� �� ����� ��� ������ � �����������
 * @param size - ������ ������ ��� ������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
static inline int AdapterRecorderSendControl( AdapterGlobalStatusStruct_t *Status, struct Recorder_s *recorder, uint8_t *Data, size_t size )
{
	return AdapterRecorderDoSameCommand( NULL, NULL, Status, recorder, (recorder_data_t*)Data, ADAPTER_INTERFACE_COMMAND__CONTROL, "control command" );
}

int AdapterRecorderSaveFromRequest(axis_httpi_request *Requests,size_t *RCount,
										AdapterGlobalStatusStruct_t *Status,struct Recorder_s *recorder);

int AdapterRecorderUploadStart( axis_ssl_wrapper_ex_sock *sock,
										axis_ssl_download_request *UploadRequest, struct Recorder_s *recorder, unsigned int *tCount );

int AdapterRecorderCheckWhile(struct Recorder_s *recorder);
int AdapterRecorderCheckAllRecordersWhile( struct Recorder_s *head);

/**
 * @brief ������� ��������� �����������
 *
 * @param Requests - ��������� �� ������ ��������
 * @param RCount - ��������� �� ���������� �������� � �������
 * @param Status - ��������� �� ���������� ��������� ������� ��������
 * @param recorder - ��������� �� ����������, ������� ������ ���������� ����������
 * @param ClientIP - IP ����� �������, ������� ������������� ����������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 */
int AdapterRecorderStopMonitoring( axis_httpi_request *Requests, size_t *RCount,
								   AdapterGlobalStatusStruct_t *Status,
								   struct Recorder_s *recorder, uint32_t ClientIP );

int AdapterRecorderGetStatusThreads(struct Recorder_s *recorder);
int AdapterRecorderSyncAllRecordersTime(struct Recorder_s *head);

int AdapterRecorderCheckIfSomeOneUseNet(struct Recorder_s *head);
int AdapterRecorderGetTimeFromStructNonBlock( struct Recorder_s *recorder,struct_data *date);

int AdapterRecorderPrintSettingsIni(struct Recorder_s *recorder,char *buff,size_t size);

/**
 * @brief �������, ��������� ������������� ������� ��� ��� ������� ����������
 *
 * @param dev - ��������� �� ��������� ����������
 * @param head - ��������� �� ������ ������ �����������
 * @return int - 0 ��� �� ����� 0 � ������ ������
 */
int AdapterRecorderGetFistRecorderAccStatusCashed( dmdev_t *dev, struct Recorder_s *head );

int AdapterRecorderGetAccStatus___( dmdev_t *dev, struct Recorder_s *recorder );
int AdapterRecorderPrintAccStatusIni( struct Recorder_s *recorder, char *buff, size_t size );
int AdapterRecorderPrintMoreInfo(char* b, size_t bsize, struct Recorder_s *recorder );

int AdapterRecorderSnGet( struct Recorder_s *recorder);

int AdapterRecorderHttpMonitoringStart(axis_ssl_wrapper_ex_sock *sock,
		      axis_ssl_download_request *UploadRequest,struct Recorder_s *recorder,   unsigned int *tCount,AdapterGlobalStatusStruct_t *Status,axis_httpi_request *Requests,
				int Count);

int AdapterRecorderGetVoltageAndTempInfo(uint16_t *voltage,uint16_t *temp,short* ac,struct Recorder_s *recorder);
int AdapterRecorderConvertAcctoStandartStruct(struct Recorder_s *recorder, SoftRecorderGetAccStateStruct_t* acc );
int AdapterRecorderConvertAcctoStandartStructUnlocked(struct Recorder_s *recorder, SoftRecorderGetAccStateStruct_t* acc );

/**
 * @brief ������� ������� ���������� ������� ����������
 *
 * @param Status - ��������� �� ���������� ��������� ������� ��������
 * @param recorder - ��������� �� ��������� ����������� ����������
 * @param cmd - ��� �������, ������� �������� ����������
 * @param Data - ��������� �� ����� ��� ������ ���������� �� ���������� � recorder->recorder_data.recordertempdata, ����� ���� NULL
 * @param size - ������ ������ ��� ������
 * @return int
 **/
int AdapterRecorderSendCommand(AdapterGlobalStatusStruct_t *Status,struct Recorder_s *recorder,uint32_t cmd, uint8_t *Data,size_t size);

/**
 * @brief �������, ������� ���������� ������� ���� ������������������ � ������� �����������
 *
 * @param Status - ��������� �� ��������� ����������� ������� ����������
 * @param head - ��������� �� ������ ������ �����������
 * @param cmd - ����� ������� � ����������
 * @return int - 0 ��� ��, ����� 0 � ������ ������ (���� ��� ������ � ����� �� �����������)
 * @note - ������ �� ���������� �� ��� �� ����, ��� ��� �� �����.
 * @note - ������ ������ ���������� �� �������� � ������ �������� ������� ��� ������ �����������
 */
int AdapterRecorderSendCommandAllRecorders( AdapterGlobalStatusStruct_t *Status, struct Recorder_s *head, uint32_t cmd );

int AdapterRecorderPrintAlarmIni(struct Recorder_s *recorder,char *buff,size_t size);

int AdapterRecorderSetDiagnosticData(AdapterGlobalStatusStruct_t *Status,struct Recorder_s *recorder);

int AdapterRecorderGetDiagnosticData(AdapterGlobalStatusStruct_t *Status,struct Recorder_s *recorder);

int AdapterRecorderPrintLogs(axis_ssl_wrapper_ex_sock *sock,
			     struct Recorder_s *recorder,   unsigned int *tCount);

int recorder_lock_time(pthread_mutex_t *lock);

int recorder_unlock(pthread_mutex_t *lock);

/**
 * @brief ������� ���������, � �� ��������� �� ����� ������ �� ����������� � ������ ���� �� ������� ����,
 * @brief ��� ��� ������ ���� ���������
 *
 * @param Status - ��������� �� ���������� ��������� ��������
 * @param dev - ��������� �� ��������� ����������� ���� ����������
 * @param head - ��������� �� ������ ����������� ��������� � ������ ������
 * @return int - 0 ��� �� (���� �� ���� ������� �� ������ ����������)
 **/
int AdapterRecorderCheckFullAllRecorders( AdapterGlobalStatusStruct_t *Status, dmdev_t *dev, struct Recorder_s *head );

/**
 * @brief �������, ������� �������� ������� � ���������� ��� ���������� ������������, ���� ��������
 *
 * @param Recorder - ��������� �� ��������� ����������
 * @param Color - ��������� �� ������ � ������
 * @param Command - ������� ��� ����������
 * @return int -  0 ��� �� (���� ���� ������� �� ��������), ����� 0 � ������ ������
 **/
int AdapterRecorderLedWork( struct Recorder_s *Recorder, const char *Color, RecorderLibLedCommand_t Command );


/**
 * @brief �������, ������� �������� ��������� �� ����������, ���� ��������
 *
 * @param Recorder - ��������� �� ��������� ����������
 * @param Color - ���� ��� ������
 * @return int - 0 ��� �� (���� ���� ������� �� ��������), ����� 0 � ������ ������
 **/
static inline int AdapterRecorderLedOn( struct Recorder_s *Recorder, const char *Color )
{
	return AdapterRecorderLedWork( Recorder, Color, RecorderLedCommandOn );
}

/**
 * @brief �������, ������� ��������� ��������� �� ����������, ���� ��������
 *
 * @param Recorder - ��������� �� ��������� ����������
 * @param Color - ���� ��� ������
 * @return int - 0 ��� �� (���� ���� ������� �� ��������), ����� 0 � ������ ������
 **/
static inline int AdapterRecorderLedOff( struct Recorder_s *Recorder, const char *Color )
{
	return AdapterRecorderLedWork( Recorder, Color, RecorderLedCommandOff );
}

/**
 * @brief �������, ������� ������� ��������� �� ����������, ���� ��������
 *
 * @param Recorder - ��������� �� ��������� ����������
 * @param Color - ���� ��� ������
 * @return int - 0 ��� �� (���� ���� ������� �� ��������), ����� 0 � ������ ������
 **/
static inline int AdapterRecorderLedBlink( struct Recorder_s *Recorder, const char *Color )
{
	return AdapterRecorderLedWork( Recorder, Color, RecorderLedCommandBlink );
}

/**
 * @brief �������, ������� ������ ������� ��������� �� ����������, ���� ��������
 *
 * @param Recorder - ��������� �� ��������� ����������
 * @param Color - ���� ��� ������
 * @return int - 0 ��� �� (���� ���� ������� �� ��������), ����� 0 � ������ ������
 **/
static inline int AdapterRecorderLedFastBlink( struct Recorder_s *Recorder, const char *Color )
{
	return AdapterRecorderLedWork( Recorder, Color, RecorderLedCommandFastBlink );
}

/**
 * @brief ������� �������� � ���������� ������������ ����������� ��������, ������� ��������� � ������ ����������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @param Recorder - ��������� �� ��������� ������ ������ ����������
 * @return int - ����� 0 � ������ ������, 0 ���������� �� �� ���������, ������� �� ������ ���������� � ������ ��������� ���������� ��������
 */
int AdapterFirmwareCheckInitInRecorder( AdapterGlobalStatusStruct_t *Status, struct Recorder_s *RecorderHead );

/**
 * @brief ������� ���������� ������� ������� �� ������ ����������� ��� ���� �����������, ������� ��� ������������
 * ��� ����, ������ ����� �������� ������� ���������� ������, ���� ��� ��������� � ��
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int
 */
int AdapterRecorderMonitoringSendAlarmAllRecorders( AdapterGlobalStatusStruct_t *Status );

/**
 * @brief �������, ������� ���������� ����������� � USB ������ ����������
 *
 * @param Dev - ��������� �� ��������� ����������
 * @param power - ���� ���������: 1 - ��������, 0 - ���������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterRecorderPower( dmdev_t *Dev, int Power );

/**
 * @brief �������, ��������� ������� ��������� ������� ������������������� ����������
 *
 * @param head - ��������� �� ������� �����������
 * @return int - ����� ����������, ����� 0 � ������ ������ ��� ���� ���������� ���������
 */
int AdapterRecorderGetInterfaceSleepTimeFistRecorder( struct Recorder_s *head );

/**
 * @brief �������, ������� �������� � ����� ������� ������ ����������� �� UDP
 *
 * @param recorder - ��������� �� ��������� ����������
 * @param b - ��������� �� �����
 * @param bsize - ������ ���������� �����
 * @param client_number - ����� ������ ��� ������
 * @return int - ���������� ������������ ����
 */
int AdapterRecorderPrintMonitoringUDPNetInfoStatusIni( struct Recorder_s *recorder,
													char *b, size_t bsize, int *client_number );

#endif //ADAPTER_RECORDER_H


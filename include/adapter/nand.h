#ifndef ADAPTER_NAND_H
#define ADAPTER_NAND_H
#include <adapter/global.h>

#define NAND_IDS_DRIVER				"nand_ids"
#define NAND_ECC_DRIVER				"nand_ecc"
#define NAND_NAND_DRIVER			"nand"
#define NAND_ROSS_NAND_DRIVER		"ross-nand"
#define NAND_ROSS_MTDSEQ_DRIVER	"ross-mtdseq"
#define NAND_ROSS_NAND_CM_DEV_DRIVER "ross_dev_cm"
#define NAND_ROSS_NAND_BANJO_DEV_DRIVER "ross_dev_banjo"

int AdapterNandInit( dmdev_t *dev, Nand_t *nand_s);

int AdapterNandDeInit();

int  AdapterNandGetPeakEnd(dmdev_t *dev,uint64_t *peaksize);

#endif

//�������� ��� ������� ������ � ����������� WiFi

//������: 2017.02.24 12:42
//�����: ��������� ����� ��� "����" ������
//�����: i.podkolzin@ross-jsc.ru

#ifndef ADAPTER_DEVICE_WIFI_H
#define ADAPTER_DEVICE_WIFI_H

#include <axis_dmlibs_resource.h>

#include <dispatcher_lib/dispatcher_lib_resource.h>

/**
 * @brief �������, ������� ���������� ����������� � USB WiFi ���������
 *
 * @param Dev - ��������� �� ��������� ����������
 * @param power - ���� ���������: 1 - ��������, 0 - ���������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterWiFiPower( dmdev_t *Dev, int power );

#endif

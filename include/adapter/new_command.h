//������ ���� ������ ���� ������ ��� ���� ��� �� ������� �������
//��� ��� ��� ����

//�����: ��������� ����� ��� "����"
//�����: i.podkolzin@ross-jsc.ru
//������: 2010.09.07 17:55

#ifndef ADAPTER_NEW_COMMAND_H
#define ADAPTER_NEW_COMMAND_H

#include <axis_time.h>

#include <adapter/global.h>


int AdapterCommandWork( int command, axis_httpi_request *Requests,
			size_t *RCount,AdapterGlobalStatusStruct_t *Status,
			struct Recorder_s **recorder, int *answer_flag,  axis_ssl_wrapper_ex_sock *sock);


int AdapterCommandWorkRequest(char *b, size_t bsize,
                                      axis_httpi_request *Requests,
                                      size_t Count,
                                      unsigned int CommandMask,
                                      AdapterGlobalStatusStruct_t *Status,struct Recorder_s **recorder,  axis_ssl_wrapper_ex_sock *sock );

//������� ������� ���������� ���� - ������� ������� ��������� � ���
//����
// Requests - ��������� �� ������ �������� � ������� ����� ������
// RCount - ��������� �� ���������� ��������� � �������
//	����� �������� � �������� ������ � �������� �������� ����� ������ ��������
// command_mask - ����� ����������� ������
// command - ��������� �� ��������� �� ������ � ������� ������������ ��� �������
// NewDate - ��������� �� ��������� ���� � ������� ��������� ����
//	������� ������ ���������� � ���������� ��� ��� �� ����������
//	����� ������� ������� ������ ��������� ���������� �������� ��� ����������� ��������
// Channels - ��������� �� ������ ����� ������������ ����� � �������
//  � ���� ���������� ��������� � ������� ������
//  ����� ������� ������� ������ ������ ���� ���������
// Status - ��������� �� ���������� ��������� �������
//�������
// 0 - ������ �� ���������
// > 0 - ��� �������� ������� ��� �������� (�� command.h)
// < 0 - ������ ���������� ���� ���� (�� command.h)
int AdapterInterfaceDoCommand( 
				axis_httpi_request *Requests,
				size_t *RCount, 
			    unsigned int command_mask, 
				char **command,
				struct_data *NewData,
				int *Channels,
				size_t CCount,
				AdapterGlobalStatusStruct_t *Status ,
				DspRecorderExchangeTimerSettingsStruct_t 	*TimersSettingsCache
     			);

int AdapterInterfaceGetCommand(axis_httpi_request *Requests,size_t *RCount, unsigned int command_mask,
				char **command,
				AdapterGlobalStatusStruct_t *Status );


//������� ������� ������������� �������
//����:
// Status - ��������� �� ���������� ��������� �������
//�������:
// ���� ���� ��� ������ ������� ������� ������� � ����������
int AdapterCommandReboot( AdapterGlobalStatusStruct_t *Status );

#endif //ADAPTER_NEW_COMMAND_H

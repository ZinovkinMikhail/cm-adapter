#ifndef CM_ADAPTER_POWER_H
#define CM_ADAPTER_POWER_H

#include <axis_dmlibs_resource.h>

#include <adapter/global.h>

/**
 * @brief ������� ������������� ������� ����������
 *
 * @param dev - ��������� �� ��������� ����������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterPowerInit(dmdev_t *dev);

/**
 * @brief ������� ���������� ������� ����������
 *
 * @param dev - ��������� �� ��������� ����������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterPowerDeInit(dmdev_t *dev);

/**
 * @brief �������, ������� ��������� ���������� � ��� � ����������� �� �����
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @param DevicePower - ���� ��������� �� ����������� ���������� ���������� (�������� ����������) /1 = ���������/
 * @param SleepValue - ���� Value � ������� Sleep
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterPowerGlobalLPC( AdapterGlobalStatusStruct_t *Status, int DevicePower, int SleepValue );


/**
 * @brief �������, ������� ��������� ���������� � ��� ��� ���� �������� ��� ���������� ������� ����� ����������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
static inline int AdapterPowerGlobalOff(  AdapterGlobalStatusStruct_t *Status )
{
	return AdapterPowerGlobalLPC( Status, 1, 0 );
}

/**
 * @brief �������, ������� ��������� ���������� � ��� (�������� ����������� ��� ���������� ������� ����������)
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @param SleepValue - ���� Value � ������� Sleep
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
static inline int AdapterSleepGo( AdapterGlobalStatusStruct_t *Status, int SleepValue )
{
	return AdapterPowerGlobalLPC( Status, 0, SleepValue );
}

/**
 * @brief ������� ������������ �������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterPowerGlobalReboot( AdapterGlobalStatusStruct_t *Status );

/**
 * @brief ������� ��������� ���������� ��
 *
 * @param dev - ��������� �� ��������� ����������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterPowerRcOn(dmdev_t *dev);

/**
 * @brief ������� ������� ��������� ���������� ��
 *
 * @param dev - ��������� �� ��������� ����������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterPowerRcOff(dmdev_t *dev);

/**
 * @brief �������, ������� ������������� ��������� USB ������ �� ������� ����������
 *
 * @param dev - ��������� �� ��������� ����������
 * @param Settings - ��������� �� ��������� ���������� ��������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterPowerControlUSB( dmdev_t *dev, SoftAdapterExchangeSettingsStruct_t *Settings );

/**
 * @brief �������, ������� ������������� ��������� Ethernet �������� �� ������� ����������
 *
 * @param dev - ��������� �� ��������� ����������
 * @param Settings - ��������� �� ��������� �������� �������� ��������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterPowerControlEthernet( dmdev_t *dev, SoftAdapterExchangeSettingsStruct_t *Settings);

int AdapterPowerControl( dmdev_t *dev, SoftAdapterExchangeSettingsStruct_t *Settings );

/**
 * @brief ������� ���������� �������� USB ����� (���/����)
 *
 * @param dev - ��������� �� ��������� ����������
 * @param usbnum - ����� ����� USB ��� ������
 * @param on - ���� ���������: 1 - ��������, 0 - ���������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterPowerUSB( dmdev_t *dev, uint8_t usbnum , int on );

/**
 * @brief ������� ���������� �������� USB ����� (��������� � ����� ��������)
 *
 * @param dev - ��������� �� ��������� ���������
 * @param usbnum - ����� ����� USB ��� ������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterPowerUSBReset( dmdev_t *dev, uint8_t usbnum );

/**
 * @brief �������, ������� ������������ ��������� ��������� �� �����
 *
 * @param Settings - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterPowerMask( AdapterGlobalStatusStruct_t *Status );

#endif //CM_ADAPTER_POWER_H

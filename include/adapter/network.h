//������� ���������� �� ������ ������� �������


#ifndef ADAPTER_NETWORK_H
#define ADAPTER_NETWORK_H

#include <adapter/global.h>


#define DEV_STATUS_CONNECTED							0
#define DEV_STATUS_DISCONNECTED							-1
#define DEV_STATUS_APSENT								2
#define DEV_STATUS_SECTION								"[DeviceStatus]"
#define DEV_STATUS_COUNT									"DeviceStatus.Count"
#define DEV_STATUS_STATUS_SHABLON							"%03d_DeviceStatus.Status"
#define DEV_STATUS_TYPE_SHABLON								"%03d_DeviceStatus.Type"
#define DEV_STATUS_LEVEL_SHABLON							"%03d_DeviceStatus.Level"
#define DEV_STATUS_QUALITY_SHABLON							"%03d_DeviceStatus.Quality"
#define DEV_STATUS_TIME_SHABLON								"%03d_DeviceStatus.ConnectionTime"
#define DEV_STATUS_BAND_SHABLON								"%03d_DeviceStatus.Modem"

#define DEV_STATUS_STATUS_VALUE_CONNECTED				"Connected"
#define DEV_STATUS_STATUS_VALUE_PROGRESS				"InProgress"
#define DEV_STATUS_STATUS_VALUE_SCANNING				"Scanning"
#define DEV_STATUS_STATUS_VALUE_ERROR					"Error"
#define DEV_STATUS_STATUS_VALUE_DISCONNECTED			"Disconnected"
#define DEV_STATUS_STATUS_VALUE_APSENT					"Apsent"
#define DEV_STATUS_STATUS_VALUE_NO_NETWORKS				"NoNetworks"
#define DEV_STATUS_STATUS_VALUE_BAND_AUTO				"Auto"
#define DEV_STATUS_STATUS_VALUE_BAND_LTE				"LTE"
#define DEV_STATUS_STATUS_VALUE_BAND_3G					"3G"
#define DEV_STATUS_STATUS_VALUE_BAND_2G					"2G"


#define LIB_NET_PATH_MASK								"/lib/libdmnet-%s.so"

/**
 * @brief �������, ������� ����������� ��� ���������� � ��������� �� ���������� axis
 *
 * @param type - ��� ������� ����������
 * @return int - ��������� �� ���������� axis_lib, ����� 0 � ������ ������
 */
int AdapterNetworkNetLibTypeToSoftAdapterType( libnettype_t type );

/**
 * @brief �������, ������� ���������� ������������ ���� ������� ����������
 *
 * @param type - ��� ������� ����������
 * @param lib - ���� ����, ��� ����� ������� � ������ ��������
 * @return const char* - ��������� �� ������ � ������
 */
const char *AdapterNetworkGetConnectionTypeName( libnettype_t type, int lib );

int AdapterAllNetworksStop( AdapterGlobalStatusStruct_t *Status);

/**
 * @brief �������, ������� ���������� ��������� �������� ����������� �� ����
 *
 * @param Status - ��������� �� ���������� ��������� ������� ��������
 * @param type - ��� �������� ����������� ��� �������� �������������� �����
 * @return libnetcontrol_t* - ��������� �� ���������, NULL ���� �� ������� ��� ������
 **/
libnetcontrol_t* AdapterNetworkGetLibControlByConnectionType( AdapterGlobalStatusStruct_t* Status, libnettype_t type );

libnetcontrol_t* AdapterNetworkGetFreeLibControlStruct(AdapterGlobalStatusStruct_t* Status);
void AdapterNetworkFreeLibControlStruct(AdapterGlobalStatusStruct_t *Status,libnetcontrol_t* Net);

int AdapterNetworkStop( AdapterGlobalStatusStruct_t *Status, libnetcontrol_t* Net );
int AdapterNetworkGetConnectionStatus( libnetcontrol_t* Net );
int AdapterNetworkGetConnectionStatuses( AdapterGlobalStatusStruct_t *Status );
int AdapterNetworkCheckWhile(  AdapterGlobalStatusStruct_t *Status);

/**
 * @brief �������, ������� ��������� ��������� ����� ������� USB ��������� � �������� ������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterNetworkCheckNetDevicesIn( AdapterGlobalStatusStruct_t *Status);

int AdapterNetworkPrintIniNetworkInfo(char* b,size_t bsize, AdapterGlobalStatusStruct_t* Status);

/* -------------------------------*/
/**
* @Synopsis  Check if was select signal
*
* @Param Status - Global status
* @Param rfds - fd_set pointer
*
* @Returns   0 no isset was, 1  was isset
*/
/* ---------------------------------*/
int AdapterNetworkWorkISSETPoll(AdapterGlobalStatusStruct_t* Status, 
															fd_set *rfds);


/* -------------------------------*/
/**
* @Synopsis  Set select hendel for net devices
*
* @Param Status	global struct
* @Param rfds	pointer to fd_set
* @Param max_sel	max sel
*
* @Returns   new max_sel
*/
/* ---------------------------------*/
int AdapterNetworkSetSelectHendelPoll(AdapterGlobalStatusStruct_t* Status, 
													fd_set *rfds,int max_sel);

//C�������� ��� ����� ��������� ������ ������ �������
typedef struct DeviceCurrentStatus_s
{
    	
  	int iconnected;  //���� ���� ��� �� ����������
  	int DevType;// ��� �������
	int DevStatus; // ������ ������� :���������, ��������, �����������
	int ConnectionLevel;// �������
	int ConnectionQual;// ��������
	char time[24];
}DeviceCurrentStatus;


//������� ������������� ����
//����:
// Status - ��������� �� ���������� ��������� �������
//irescue  - ���� ����������� ������
//�������: 
// 0 - ��� ������� �����������������
// ����� 0 � ������ ������
int AdapterNetworkInit( AdapterGlobalStatusStruct_t *Status, int irescue );


//������� �������� ��������� ip ������ � ����������� ����
//����:
// ip - ���������� IP ������� ������ ���� ��������
// net - ��������� �� ������ ����������� �����
// mask - ��������� �� ������ ������������ �����
// count - ���������� ��������� � �������
//�������:
// 0 - IP �� ����� � ������ �����������
// 1 - IP ����� � ������ ��������
// 0 - � ������ ������
int AdapterNetworkChaeckIPForAllowNetwork(
			uint32_t ip,
			uint32_t *net,
			uint32_t *mask,
			size_t   Count  );


//������� ������� ������������ ����������
//����: ��������� �� ��������� � �����������
//�������:
// 0 -�� �������
// ������ 0 - � ������ ������
int AdapterNetworkConnectionRestart(AdapterGlobalStatusStruct_t* Status);

/**
 * @brief �������, ������� ��������� �������� ����������� � ����
 *
 * @param StatusAdapter - ��������� �� ���������� ��������� ������� ��������
 * @param CurrenNet - ��������� �� ������� ����
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 */
int AdapterNetworkStartChildNetwork( AdapterGlobalStatusStruct_t *StatusAdapter, libnetcontrol_t *CurrenNet );

/**
 * @brief �������, ������� ������������� �������� ����, ���� ����
 *
 * @param StatusAdapter - ��������� �� ��������� ������� ��������
 * @param CurrenNet - ��������� �� ������� ����, ��� ������� ������������� ��������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 */
int AdapterNetworkStopChildNetwork( AdapterGlobalStatusStruct_t *StatusAdapter, libnetcontrol_t *CurrenNet );

/**
 * @brief ������� ��������� ������ ��� �������� �� ������� ���������� CDMA
 *
 * @param status - ��������� �� ��������� ������� �������
 * @param data - ��������� �� ������ �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 */
int AdapterNetworkEventCDMA( connection_status_t *status, void *data );

/**
 * @brief ������ ��� lte ����������
 *
 * @param status - ��������� ����������
 * @param data - �������������� ������
 * @return int: 0 - OK, <0 - fail
 */
int AdapterNetworkEventLTE( connection_status_t *status, void *data );

/**
 * @brief ������� ��������� ������ ��� ������� �� ������� ���������� WiFi
 *
 * @param status - ��������� �� ��������� ������� �������
 * @param data - ��������� �� ������ �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 */
int AdapterNetworkEventWiFi( connection_status_t *status, void *data );

/**
 * @brief ������� ��������� ������ �� ������� ���������� Ethernet
 *
 * @param status - ��������� �� ��������� ������� �������
 * @param data - ��������� �� ������ �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 */
int AdapterNetworkEventEthernet( connection_status_t *status, void *data );

int AdapterNetworkGetLedCDMANum( void );
int AdapterNetworkGetLedWiFiNum( void );
int AdapterNetworkLedFastBlink( int lednum, const char* color );
int AdapterNetworkLedBlink( int lednum, const char* color );
int AdapterNetworkLedOff( int lednum, const char* color );
int AdapterNetworkLedOn( int lednum, const char* color );

int AdapterNetworkGetLedEthernet( void );

/**
 * @brief ������� ��������� ������ ��� ������� �� ������� ���������� PPtP
 *
 * @param status - ��������� �� ��������� ������� �������
 * @param data - ��������� �� ������ �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 */
int AdapterNetworkEventPPtP( connection_status_t *status, void *data );

/**
 * @brief ������� ��������� ������ ��� ������� �� ������� ���������� PPoE
 *
 * @param status - ��������� �� ��������� ������� �������
 * @param data - ��������� �� ������ �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 */
int AdapterNetworkEventPPoE( connection_status_t *status, void *data );

/**
 * @brief �������, ������� ��������� ���� � ���������� USB
 *
 * @param Status - ��������� �� ��������� ����������� �������
 * @param Net - ��������� �� �������� �����������
 * @return int
 **/
int AdapterNetworkUsbOut( const AdapterGlobalStatusStruct_t* Status, libnetcontrol_t *Net);

/**
 * @brief �������, ������� ��������� ������������ ��������� USB ��������� �������� � ���������� ����������
 * @brief � ���������� ������ �� �������� ���� � �������
 *
 * @param dev - ��������� �� ��������� ����������
 * @param devices - ��������� �� ��������� ��������� �������
 * @param count - ���������� ��������� USB ���������
 * @param num - ����� ��� ������� ������
 * @param modnum - ����� ��� ������� ������
 * @return int - 0 ������� �� �������, 1 ����� ������, ����� 0 � ������ ������
 **/
int AdapterNetworkDevicesCheckForInModem( const dmdev_ctrl_hw_t *dev, const AxisUsbDesc_t *devices, int count,
										  int *num, int *modnum );


/**
 * @brief ������� ������� ��������� ������� ����� � �������� ���� �� ������ ������������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - ����� 0 � ������ ������, 0 - ������ �� ��������, ����� 0 - ������ ��������
 **/
int AdapterNetworkPingServerHost( AdapterGlobalStatusStruct_t *Status );

/**
 * @brief ������� ���������� ���� ��������� ������ � ������� ���������� �� �������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterNetworkPingAllStdout( AdapterGlobalStatusStruct_t *Status );

/**
 * @brief �������, ������� ��������� ��� ���������� � ������
 *
 * @param Band - ��������� �� ��� ����������
 * @return const char* - ������������ ������
 */
static inline const char *AdapterLTEModemBandToString( uint16_t Band )
{
	const char *value = NULL;
	switch( Band )
	{
		case DM_NET_BAND_LTE:
			value = DEV_STATUS_STATUS_VALUE_BAND_LTE;
			break;
		case DM_NET_BAND_3G:
			value = DEV_STATUS_STATUS_VALUE_BAND_3G;
			break;
		case DM_NET_BAND_2G:
			value = DEV_STATUS_STATUS_VALUE_BAND_2G;
			break;
		default:
			value = NULL;
	}
	return value;
}

#endif //ADAPTER_NETWORK_H

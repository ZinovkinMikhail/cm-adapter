#ifndef COPY_DATA_H
#define COPY_DATA_H

#include <axis_softadapter.h>

#include <adapter/global.h>

int AdapterCopyDataStart( SoftAdapterCopyDataStatus_t *CopyStatus, AdapterGlobalStatusStruct_t *Status );

int AdapterCopyDataCheck( SoftAdapterCopyDataStatus_t *Status );

int AdapterCopyDataGetStatus( SoftAdapterCopyDataStatus_t *CopyStatus, AdapterGlobalStatusStruct_t *Status );

int AdapterCopyDataStop( AdapterGlobalStatusStruct_t *Status );
#endif //COPY_DATA_H


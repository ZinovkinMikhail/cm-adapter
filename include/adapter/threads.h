//����� �� ������������� ������������ ������� ���������� �� ������ 
//��� ���� ����� ��� �������� ������ - ����������� �� ���� ������ �����������
// AltLinux �� ������� spn_cu

//�����: ��������� ����� ��� "����"
//�����: i.podkoolzin@ross-jsc.ru
//������: 2010.09.13 16:36

#ifndef ADAPTER_THREADS_H
#define ADAPTER_THREADS_H

#warning - ��� �� �� ��� ������!!!

#include <pthread.h>

//��������� � ������� �� ����� ������� ���������� � ������
struct AdapterThreadStruct_s
{

	int 			used;			//0: �� ������������, 1: ������������
	pthread_t		self;			// ��� ��� �����
	pthread_mutex_t	mutex;			// ��� ����� ��� ������ ������
	uint32_t		counter;		// �������� ����� (�� ������������� = �����)
	uint32_t		prev_cpunter;	// �������� ��������� ����� � ���������� ���
	void 			*private;		// ��������� �� ��������� ������ ������
	void			(*cleanup)(struct AdapterThreadStruct_s *);
									// ��������� �� ������� ������� private
	void			*retval			// � ����� ��������� ��� ������ thread_exit()
	int				status;			// THREAD_RUNNING | THREAD_EXITED | THREAD_CANCELED
	char			*name;			// ���/�������� �������� � ����������� �����

};

typedef struct AdapterThreadStruct_s AdapterThread_t;


#define THREAD_RUNNING	0
#define THREAD_EXITED	1
#define THREAD_CANCELED	2

//������� ������ �� ������
// ����:
//retval - ��������� �� ��� ��������
void AdapterThreadExit(void *retval);


//���������� - ��� �� ����� ��������� ������� - main,httpd � ����������
//httpd - � ���� ������� ����� ���� ������


#warning - ���� ��������� ������

/*
int init_threads(void);
void unexpected_exit(thread_t thread);

thread_t thread_create(void *(*start_routine)(void *), char *name, void *private, void (*cleanup)(thread_t));
thread_t Thread_create(void *(*start_routine)(void *), char *name, void *private, void (*cleanup)(thread_t));
thread_t thread_self(void);
void thread_cancel(thread_t thread);

void kill_sleeping_threads(void);
void console_print_threads(void);

void *zombie_killer(void *arg __attribute__((unused)));

 */
#endif //ADAPTER_THREADS_H

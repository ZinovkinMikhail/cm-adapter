//��� ��� �������� ����� ���������


#ifndef ADAPTER_SETTINGS_H
#define ADAPTER_SETTINGS_H

#include <adapter/global.h>

//-------------------------------------------------------------------------
// �������� ������� ������� �� ����� ������������

/**
 * @brief ������� �������� ������� ��������
 *
 * @param aDev - ��������� �� ��������� ����������
 * @param NetSettings - ��������� �� ��������� ������� ��������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterSettingsCheckForNetSetting( dmdev_t *aDev, SoftAdapterExchangeSettingsStruct_t *NetSettings );

//������� �������� ���� ��������� � ��������
//����:
// iResetFlag - ���� ���� ��� �� ���� ��������� � ���������
// Status - ���������� ��������� ������� ��������
//�������:
// 0 - ��� ������
// ����� 0 � ������ ������
int AdapterSettingsAllLoad( int iResetFlag, AdapterGlobalStatusStruct_t *Status );


//������� ��������� ������ ���������
//����:
// Status - ���������� ��������� ������� ��������
//�������:
// 0 - ��� ������ - ������ ������� �������
// ����� 0 � ������ ������
int AdapterSettingsLoadRecorderSettings( 
			AdapterGlobalStatusStruct_t *Status  );

//������� ��������� �������� ���������
//����:
// Status - ���������� ��������� ������� ��������
//�������:
// 0 - ��� ������ - ������ ������� �������
// ����� 0 � ������ ������

int AdapterSettingsLoadRecorderTimers( 
			AdapterGlobalStatusStruct_t *Status  );

int AdapterSettingsLoadAdapterTimers(AdapterGlobalStatusStruct_t *Status);

//������� ��������� ������ ��������
//����:
// Status - ���������� ��������� ������� ��������
//�������:
// 0 - ��� ������ - ������ ������� �������
// ����� 0 � ������ ������
int AdapterSettingsLoadAdapterSettings( 
			AdapterGlobalStatusStruct_t *Status  );


//������� ������ ������ ���������
//����:
// Status - ��������� �� ���������� ��������� �������
//�������:
// 0 - ��� ������ - ������ ������� �������
// ����� 0 � ������ ������
int AdapterSettingsSaveRecorderSettings( 
			AdapterGlobalStatusStruct_t *Status );

//������� ������ ������ ��������
//����:
// Status - ��������� �� ���������� ��������� �������
//�������:
// 0 - ��� ������ - ������ ������� �������
// ����� 0 � ������ ������
int AdapterSettingsSaveAdapterSettings( 
			AdapterGlobalStatusStruct_t *Status );

//������� ������ ������ ��������
//����:
// Status - ��������� �� ���������� ��������� �������
//�������:
// 0 - ��� ������ - ������ ������� �������
// ����� 0 � ������ ������
int AdapterSettingsSaveAdapterSettingsDefault(
			AdapterGlobalStatusStruct_t *Status );

//������� ������ ������ ��������
//����:
// Status - ��������� �� ���������� ��������� �������
//�������:
// 0 - ��� ������ - ������ ������� �������
// ����� 0 � ������ ������
int AdapterSettingsSaveNetTimersSettings( 
			AdapterGlobalStatusStruct_t *Status );

int AdapterSettingsSaveFromRequest( axis_httpi_request *Requests, size_t *RCount, AdapterGlobalStatusStruct_t *Status,int iapplynow );


int AdapterSettingsLoadDefault( dmdev_t *dev, SoftAdapterExchangeSettingsStruct_t* Settings);

int adapter_settings_work_new_sets(AdapterGlobalStatusStruct_t *Status,int iapplynow,int inewtime,struct_data *newdate);

#endif //ADAPTER_SETTINGS_H

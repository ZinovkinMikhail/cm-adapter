//������� ��� ������ � �����������


#ifndef R2_ADAPTER_DISPATCHER_H
#define R2_ADAPTER_DISPATCHER_H

#include <adapter/global.h>

//������� ������� ����������� ������ ��������� � ����������
//����:
// Status - ��������� �� ���������� ��������� �������
//�������:
// 0 - ��� ������� ���������
// ����� 0 � ������ ������
int AdapterDispatcherGetRecorderStatus( AdapterGlobalStatusStruct_t *Status );


//������� ������� ��������� ��������� � ��� ��� ���� ������ �������
//(������� ���������)
//����:
// Status - ��������� �� ���������� ��������� ������� ���� � ���������
//�������:
// 0 - ��� ������
// ����� 0 � ������ �������
int AdapterDispatcherRecorderStartClear( AdapterGlobalStatusStruct_t *Status );

//������� ������� ��������� ��������� � ��� ��� ���� ������ �������
//(������ ������)
//����:
// Status - ��������� �� ���������� ��������� ������� ���� � ���������
//�������:
// 0 - ��� ������
// ����� 0 � ������ �������
int AdapterDispatcherRecorderStartErase( AdapterGlobalStatusStruct_t *Status );

//������� ������� ��������� ��������� � ��� ��� ���� ����������� �������
//(������ ������)
//����:
// Status - ��������� �� ���������� ��������� ������� ���� � ���������
//�������:
// 0 - ��� ������
// ����� 0 � ������ �������
int AdapterDispatcherRecorderStopErase( AdapterGlobalStatusStruct_t *Status );

//������� ������� �������� ������� ����������
//����:
// Satstus - ��������� �� ���������� �������� �������
// Command - ���������� ������� � ��������
//������:
// 0 - ��� ��
// ����� 0 � ������ ������
int AdapterDispatcherDoCommand( AdapterGlobalStatusStruct_t *Status,
								uint16_t Command );

#endif //R2_ADAPTER_DISPATCHER_H

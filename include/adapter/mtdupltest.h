#ifndef MTD_UP_TEST_H
#define MTD_UP_TEST_H

#include <axis_ssl_httpi_server.h>

#include <adapter/global.h>

#define ADAPTER_INTERFACE_MTD_TEST_TXT_PAGE	"mtd.test"

int AdapterInterfaceUploadMtdTest(
				axis_ssl_wrapper_ex_sock *sock,
				axis_httpi_request *Requests,
				int Count,
				unsigned int *tCount );

#endif

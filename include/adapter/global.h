//��������� ��� ������� ������� ����� ������������ ����������
//�����
#ifndef R2_ADAPTER_GLOBAL_H
#define R2_ADAPTER_GLOBAL_H

#include <semaphore.h>

#include <axis_softrecorder.h>
#include <axis_softadapter.h>
#include <axis_ifconfig.h>
#include <axis_dmlibs_resource.h>
#include <axis_ssl_wrapper_ex.h>
#include <axis_ssl_httpi_download.h>
#include <axis_usb.h>

#include <dispatcher_lib/dispatcher_lib_resource.h>
#include <dispatcher_lib/alarm_config.h>
#include <dispatcher_lib/upload.h>
#include <dispatcher_lib/monitoring_status.h>
#include <dispatcher_lib/events.h>

#include <dm_lib_log_control/command_resource.h>

#include <dmxxx_lib_recorder/dmxxx_lib_recorder_cals.h>

#include <dmxxx_lib_net/dmxxx_lib_net_cals.h>

#include <adapter/httpd_status.h>
#include <adapter/resource.h>


#define NET_NETWORK_INTERFACES_COUNT			8

//������ ��������� ������
#ifdef TMS320DM355
#include <dm355_device.h>
#else
#define DM355_DEVICE_SN_SIZE 8
#endif

#define WORK_ON_POWER		0x01
#define WORK_ON_TIMER365		0x02
#define WORK_ON_TIMER5515	0x04
#define WORK_ON_ADXL		0x08
#define WORK_ON_DU1			0x10
#define WORK_ON_DU2			0x20

// extern env e;


//��������� � ������� �� ����� ������� ��������� �������� ������
//������������ �� ����������� ������
typedef struct CurrentNetworkSettingsStruct_s
{

	int				ClientMode;		//����� ������ �������

	char			ServerHost[32];	//A���� ������� �����������

	uint16_t		ControlPort;	//���� ��������
	uint16_t		UploadPort;		//���� �������
	uint16_t		MonitoringPort;	//���� �����������

	char			Login[32];		//������� �����
	char			Passwd[32];		//������� ������

	char			Alias[64];		//������� ��������� ��������

	char			SN[ DM355_DEVICE_SN_SIZE * 2 + 2];
	//������� ��������� ����� ��������

} CurrentNetworkSettingsStruct_t;



struct Recorder_s
{
	int type;
	int hen;
	time_t connecttime;
	time_t lastsettingsquery;
	time_t laststatusquery;
	int status;
	int errcount;
	int errlimit;
	pthread_mutex_t lock;
	uint64_t sn;
	sem_t 	semaphore;
	recorder_data_t recorder_data;
	int recorderscount;
	int recordernum;
	int datanotsave;
	int port;
	void *recorderlib;
	
	//������� ������������� �� ���������� ����������
	functions_recorder_print_alarm_ini_t					recorder_print_ini_alarm; 
	functions_recorder_set_diagnostic_data_t				recorder_set_diagnostic;
	functions_recorder_get_diagnostic_data_t				recorder_get_diagnostic;
	functions_recorder_init_t								recorder_init;
	functions_recorder_deinit_t								recorder_deinit;
	functions_recorder_settings_get_t						recorder_get_settings;
	functions_recorder_settings_put_t						recorder_put_settings;
	functions_recorder_status_get_t						recorder_get_status;
	functions_recorder_status_nand_get_t					recorder_get_nand_status;
	functions_recorder_timers_get_t						recorder_get_timers;
	functions_recorder_timers_put_t						recorder_put_timers;
	functions_recorder_printf_ini_status_t					recorder_print_ini_status;
	functions_recorder_printf_ini_info_status_t			recorder_print_ini_info_status;
	functions_recorder_get_sn_t								recorder_get_sn;
	functions_recorder_printf_ini_settings_t				recorder_print_ini_settings;
	functions_recorder_print_recorder_info_t				recorder_print_in_file_status;
	functions_recorder_print_monitoring_net_info_t			recorder_print_ini_monitoring_net_info_status;
	functions_recorder_get_recorder_alias_t				recorder_get_recorder_alias;
	functions_recorder_status_get_dev_version_t			recorder_get_version_date;
	functions_recorder_command_do_t						recorder_do_command;
	functions_recorder_upload_t								recorder_upload;
	functions_recorder_http_monitoring_t					recorder_http_monitor;
	functions_recorder_get_status_thread_t					recorder_get_status_threads;
	functions_recorder_check_while_t						recorder_check_while;
	functions_recorder_sync_time_t							recorder_sync_time;
	functions_recorder_get_time_from_struct_t				recorder_get_time_from_struct;
	functions_recorder_status_acc_get_t					recorder_get_acc_status;
	functions_recorder_printf_ini_acc_status_t				recorder_print_ini_acc_status;
	functions_recorder_print_more_info_t					recorder_print_more_info;
	functions_recorder_get_voltage_temp_t					recorder_get_voltage_temp;
	functions_recorder_convert_acc_to_standart_struct_t	recorder_convert_acc_to_standart;
	functions_recorder_logs_t								recorder_print_logs;
	functions_recorder_get_adapter_timers_t				recorder_get_adapter_timers;
	functions_recorder_put_adapter_timers_t				recorder_put_adapter_timers;
	functions_recorder_get_adapter_settings_t				recorder_get_adapter_settings;
	functions_recorder_put_adapter_settings_t				recorder_put_adapter_settings;
	functions_recorder_convert_status_to_standart_struct_t	recorder_convert_status_to_standart_struc;
	functions_recorder_led_command_t						recorder_led_command;
	functions_recorder_get_rcr_status_t					recorder_get_rcr_status;
	functions_recorder_get_firmware_status_t				recorder_get_firmware_status_flag;
	functions_recorder_clear_firmware_status_t				recorder_clear_firmware_status_flag;
	functions_recorder_get_adapter_firmware_t				recorder_get_adapter_firmware;
	functions_recorder_get_adapter_timers_status_flags_t	recorder_get_adapter_timers_status_flag;
	functions_recorder_put_adapter_timers_status_flags_t	recorder_put_adapter_timers_status_flag;
	functions_recorder_monitoring_send_alarm_event_t		recorder_monitoring_send_alarm;
	functions_recorder_settings_get_interface_sleep_time_t	recorder_get_interface_sleep_time;

	volatile int usecount;
	pid_t UserPID;
	struct Recorder_s *prev;
	struct Recorder_s *next;
	struct Recorder_s *head;

};

/**
 * @brief ���� ������� ��������� �� �����������, ������� �� ����� ������������ � ��������
 *
 */
typedef enum
{
	nettype_closed		= -1,	//-1	//����� �� �� ���� ���������� - ��� �������
	nettype_eth			= 0,	//0		//Ethernet - ������ ������� ����
	nettype_wifista,			//1		//WiFi - ������ WiFi ����
	nettype_modem,				//2		//Modem - ��������� �������� ����������
	nettype_lte,				//3		//LTE - �������� ���������� �� ������ LTE, ����� ���� ��� ��������, ��� � �������� ������������
	nettype_wifiap,				//4		//WiFi ����� ������� - ���������� ��� ����� ������� WiFi
	nettype_vlan,				//5		//VLAN - ������ ������� ��� WiFi � Ethernet ����������, ���������� �� VLAN
	nettype_bond,				//6		//Bonding - ����������� ���� � ����� ���������� � ����, ��� ���������/������������������
	nettype_reserv,				//7		//Reserving - �������������� ���������� ����� ��������� �������
	nettype_tun,				//8		//tun - �������������� IP �������
	nettype_tap,				//9		//tap - �������������� IP �������
	nettype_pptp,				//10	//PPtP - ������ VPN ���� �� ������ PPtP ����������
	nettype_ppoe,				//11	//PPoE - ������ VPN ���� �� ������ PPoE ���������� ��� Ethernet � WiFi (������� � ������)
	nettype_openvpn,			//12	//OpenVPN - ������ VPN ���� OpenVPN
	nettype_ipsec,				//13	//IPSec - ���������� ������� �� ���������� IPSec
	nettype_wireguard,			//14	//WireGuard - ������ VPN ���� WireGuard

	nettype_max,						//�������� ��� ���������� ��� ��������

} libnettype_t;

/**
 * @brief ���� ������� ��������� �� �������, ������� �� ����� ������������
 *
 */
typedef enum
{
	netversionold = 0,				//������ ������ ������� ��������� DM - VPN ������ ����������
	netversionSeparateVPN,			//����� ������ ������� ���������� DM - ����� VPN �������������� ��������� �����������

} libnetversion_t;

typedef struct libnetcontrol_s
{
	void* netlib;
	function_net_init					net_init;
	function_net_start_connection		net_start;
	function_net_stop_connection		net_stop;
	function_net_check_while			net_check_while;
	function_net_get_status				net_get_status;
	function_net_do_command				net_do_command;
	function_net_close					net_close;
	function_net_send_sms				net_send_sms;	
	ipconfig_t							config;
	connection_status_t					status;
	libnettype_t						net_lib_type;
	libnetversion_t						net_lib_version;
	int									EnablePollingModem;
	AxisUsbDesc_t 						PollingModem;

} libnetcontrol_t;

typedef struct Nand_s
{
	uint64_t fullsize;
	uint64_t flashpeakstart;
	uint64_t flashpeakend;
	time_t lastquery;
	uint32_t readerrors;
	uint32_t eccerrors;
	uint32_t ecccorrects;
	uint32_t bbcount;
	uint32_t reserv;

} Nand_t;

//��������� ��� �������� SMS
typedef struct smscontrol_s
{
	int smsnum;
	int smscount;
	int status;
	pthread_mutex_t lock;
	time_t timesent;
	time_t queued;
	char text[SMS_TEXT_SIZE];
	char number[SMS_PHONE_SIZE];
	struct smscontrol_s *nextsms;
	struct smscontrol_s *prevsms;
	struct smscontrol_s *head;
} smscontrol_t;


//��������� � ������ �� ����� ������� ��� � ���
typedef struct AdapterGlobalStatusStruct_s
{
	dmdev_t 	GlobalDevice_t;
	struct Recorder_s Recorder;
	struct Recorder_s *testRecorder;
	Nand_t	Nand;
	pthread_mutex_t net_mutex;
	libnetcontrol_t Net[NET_NETWORK_INTERFACES_COUNT];		// ��������� ����� � *net �����������
#if 0
	libnetcontrol_t NetSec;		// ��������� ����� � �������������� *net �����������
#endif
	int		GlobalConsole;		//������ ���������� �������
	int 	GlobalDevice;		//������ ��������� ����������
	int 	GlobalUsbMajor;
	int		GlobalDispatcher;	//������ ��� ����� � �����������
	//     int		GlobalDispatcher2;	//������ 2 ��� ����� � �����������
	time_t 	LastAccStatusGet;//����� ���������� ������� ���
	sem_t 	GlobalUsbRecorderSemaphore;

	int		SystemInfoFlag;		//���� �� �������� ��������� ����������
	int		flag_interactive;	//���� ������ � ������������� ������
	int		flag_iconsole;		//���� ����, ��� �������� � ��������
	int		flag_no_interface;	//���� ����, ��� �� ����� ���������� ���������
	int		flag_resque;		//���� ���� ��� �� � ������ �������������� ����������
	int		GlobalDataNotSave;	//���� ���� ��� ������ �� ���� ���������
	int 	flag_resetsetstodefault;
	int		RecorderSettingsAcceptFlag;
								//���� ���� ���� �� ��������� ���������
								//����������
	int 	ilogging;			//���� �����������
	int		AdapterSettingsAcceptFlag;
	//���� ���� ��� ���� �� ��������� ���������
	//��������

	int		AdapterNeedReboot;		//���� ���� ��� ������� ���� �����������
	int		AdapterNeedSleep;		//���� ���� ��� ������� ������ ������
	int		AdapterNeedLTECommand;	//����� ���� ��� ���� ��������� ������� LTE ������, �������� ��� ���������� ����������

	int		AdapterDevice;		//� ����� ����������� �� ��������
	//���� ��������� �� ����� ����� � �����

	int		HardwareManufactureYear; //��� ������� - ���� ��������
	int		HardwareManufactureMon;	//����� ������� - ���� ������
	int		HardwareManufactureVersion;	//������ - ���� ������

	int		NoUsbIgnore;			//���� ���� ��� ��� �������� ��� ���������� �������


	uint32_t	AdapterTCount;	//���������� ���������� ���� ���������
	//��� ��������

	char*	Build;				//����� ��������� ������
	time_t	BuildTime;			//����� ��������� ������

	uint32_t	RecorderStatusCount; //����� � ��������� ��� ���������� �������
	//� ��� �������

	//��� ������� �� �������� � ��� �� ���������
	SoftRecorderGetStatusStruct_t RecorderStatus;	//����� ��� ���������� ������
	SoftRecorderExchangeSettingsStruct_t	RecorderSettings;	//����� ��� ��������� ���������
	SoftAdapterExchangeSettingsStruct_t AdapterSettings;	//����� ��� ��������� ��������

	//	DispatcherLibAlarmExchangeBuffer_t
	//			TimersSettings;		//������ ��� �������
	DispatcherLibAlarmExchangeBuffer_t 	TimersSettingsNetAxis;		//������ ��� �������

	DspRecorderExchangeTimerSettingsStruct_t 	TimersSettingsNet;		//������ ��� �������

	SoftRecorderGetAccStateStruct_t
	ACCStatus;			//����� ��� ������ ���

	struct struct_net_param
			DHCPNetParam;		//����� ��� ������ ���������� �� DHCP

	unsigned char DeviceSN[DM355_DEVICE_SN_SIZE]; //�������� ����� ��������

	AdapterHTTPDStatusStruct_t			HTTPDMain; 	//������ ������� HTTP  - �������� ������ ���� �������� ���� ���������
	AdapterHTTPDStatusStruct_t			HTTPDSec;	//������ ������� HTTP - ��������������� ������� ��� ��������� �����������

	CurrentNetworkSettingsStruct_t CurrentNetwork; //������� ������� ���������
	CurrentNetworkSettingsStruct_t HTTPSecondNetwork; //������� ��������� ��� ��� ������� �������

	AdapterHTTPDStatusStruct_t HTTPDQt; //�������� ������� qt

	CurrentNetworkSettingsStruct_t CurrentNetworkInterface;
								//������� ������� ��������� qt

	struct_data					AdapterStart;

	int							RescueMode;

	DispatcherLibUploadStatusStruct_t
	AdapterSSlUpload;
	//������� ��������� �������

	DispatcherLibUploadStatusStruct_t
	AdapterSSlMonitoring;
	//������� ��������� �����������

	time_t						LastInterfaceQuire;
	//����� � ��������� ��� ���������� ������� ����������

	time_t						LastInterfaceQuireQt;
	//����� � ��������� ��� ���������� ������� ����������

	int							SleepTime;
	//������� ������� �������� ���������� ������ ���
	//������� ���������� ������� � ����� ����������
	//����� ���������� ��������� �������
	// <= 0 - ������ �� ���� ����� �����

	SoftAdapterExchangeGetEventsStruct_t	ExchangeEvents;
	//��� ������ ��������� ����� ���������

	uint16_t 					SignalSent;
	//���� ���� ��� ������ ���������
	uint16_t					SignalEthSent;
	pid_t						qt_client_pid;
	//��� ��������  �����

	pthread_mutex_t		sets_mutex;

	int GlobalEventsHendel;

	int GlobalSourceWork;			//���������� ���� ����������� �� �� ��� ��� ���������
	// 	WORK_ON_POWER
	// 	WORK_ON_TIMER365
	// 	WORK_ON_TIMER5515
	// 	WORK_ON_ADXL
	// 	WORK_ON_DU1
	// 	WORK_ON_DU2
	time_t						LastRecorderSetsQuery;

	int 							icangosleep;

	uint64_t 						AdapterTimerFlags;

	uint8_t							LedHeartBeatNum;
	uint8_t							LedFirmwareNum;
	uint8_t							LedHardwareNum;
	uint8_t							LedNetworkConnectNum;
	uint8_t							LedNetworkNoConnectionNum;
	uint8_t							LedUSBNoModemsNum;
	uint8_t							LedRescueNum;
	uint8_t							LedUploadNum;

	const char*						LedHeartBeatColour;		//����, ������� ������� ���������� ��� ������� �����, ����� ���� NULL
	const char*						LedFirmwareColour;		//����, ������� ������� ���������� ��� ���������� ������������ �����������, ����� ���� NULL
	const char*						LedHardwareColour;		//����, ������� ������� ���������� ��� �������� ����������, ����� ���� NULL
	const char*						LedNetworkConnectColour;//����, ������� ������� ���������� ��� ����������� � ����, ����� ���� NULL
	const char*						LedNetworkNoConnectionColour;	//����, ������� ������� ���������� ��� ���������� ����������� � ����, ����� ���� NULL
	const char*						LedUSBNoModemsColour;	//����, ������� ������� ���������� ��� ���������� USB �������, ����� ���� NULL
	const char*						LedRescueColour;		//����, ������� ������� ���������� ��� ������ ��������������, ����� ���� NULL
	const char*						LedUploadColour;		//����, ������� ������� ���������� ��� ������ �������� ������, ����� ���� NULL

	uint32_t						NoModemFoundCount;		//������� ��� ���� ���������� ���������� USB �������
	uint32_t						NoModemFoundCountFatal;	//������� ��������� ������
	uint32_t						NoModemFoundCountCheck;	//������� ����� �� ������������

	uint32_t						USBPortNeadReset;		//����� ����� USB, ������� ������� ������������

	int							InterfaceSocket;

	SoftAdapterCopyDataStatus_t		CopyStatus;
	
	GlobalRegistrationStruct_t 			LogRegistration;

	
	int modemsInSleep;

	int 							InterfaceSleep;
	time_t							LastInterfaceSleepEvent;

	time_t 						LastKeyPressTest;

	time_t 						LastPowerEvent;

	int 							usbswitches;

	int 							iplay;

	recorder_diagnostic_status_t RecorderDiagnostic;

	adapter_diagnostic_status_t AdapterDiagnostic;

	uint64_t 					currentRecerrors;

	pid_t 					GlobalPID;
	
	smscontrol_t			SMSControl;		//��������� �� ��������� �������� �������� SMS

	uint32_t				GlobalWhileBreakLine;
	uint32_t				GlobalTimeoutBreakLine;
	uint32_t				GlobalInitBreakLine;

	uint32_t				PowerDeviceMask;		//����� ���������, ������� ������ ���� ��������

	uint32_t				NoIncomingSMSCounter; //������� ����������� �� SMS � �������� SMS

} AdapterGlobalStatusStruct_t;

/**
 * @brief - ������� ����������� ����� ��������
 *
 * @param iResetValueFlag - ���� ����, ��� ���� �������� ��� ��������� � �������� �� ���������
 * @param iInertactiveFlag - ���� ���� ��� �� �������� � ������������� ������
 * @param AdapterDevice - ��� ����������
 * @param iRescueMode - ���� �������� � ����� ��������������
 * @param ilogging - ���� ��������������� �����������
 * @param iNoDaemon - ���� ������� �������� � ����� ������
 * @param iConsole - ���� �������� ����� �� �������
 * @param iNoInterface - ���� �� ��������� ���������
 * @param FileName - ��� ���������� ����� ��� ���������, ����� ���� NULL
 * @param KernelDebugLevel - ������� ������� ���� ��� ������
 * @return int - 0 ���������� ���� ���������� �������, ����� 0 � ������ ������
 */
int AdapterGlobalWail( int iResetValueFlag, int iInertactiveFlag, int AdapterDevice,
					   int iRescueMode, int ilogging, int iNoDaemon, int iConsole, int iNoInterface,
					   const char *FileName, int KernelDebugLevel );

/**
 * @brief ������� ���������� ������������� ��������
 *
 * @param iResetValueFlag - ���� ���� ��� ���� �������� ��� ��������� � �������� �� ���������
 * @param iInertactiveFlag - ���� ���� ��� �� �������� � ������������� ������
 * @param AdapterDevice - ��� ����������
 * @param iRescueMode - ���� �������� � ����� ��������������
 * @param ilogging - ���� ��������������� �����������
 * @param iNoDaemon - ���� ����, ��� �� ����� ���������� � ����� ������
 * @param iConsole - ���� ����, ��� ����� �������� ������� ��� �����������
 * @param iNoInterface - ���� ����, ��� �� ����� ��������� ���������
 * @param Status - ��������� �� ��������� ����������� �������
 * @param KernelDebugLevel - ������� ������� � ���� ��� ������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 */
int AdapterGlobalInit( int iResetValueFlag, int iInertactiveFlag, int AdapterDevice,
					   int iRescueMode, int ilogging, int iNoDaemon, int iConsole, int iNoInterface,
					   AdapterGlobalStatusStruct_t *Status, int KernelDebugLevel );

//������� ����������� ��������
//����:
// Status - ��������� �� R2AdapterGlobalStatusStruct_t ���������
//      ������� � ������� ��� ����� ������������
//�������:
// 0 - ���������� ���� ���������� �������
// ����� 0 � ������ ������
int AdapterGlobalClose( AdapterGlobalStatusStruct_t *Status );


//������� ��������� ��������
//����:
// sig - ���������� ����� ��������������� �������
void AdapterGlobalSignalHendler( int sig );

//������� ��������� ������� � �������
//����:
// Status - ��������� �� R2AdapterGlobalStatusStruct_t ���������
//      ������� � ������� ��� ����� ������������
//�������:
// ������ ���� ���� ������� ��������� ���������
// � ����� �����, ��������, �����
// ���������� ��� ������� �������
// 0 - ��� ������� ����������
// ����� 0 � ������ ������
int AdapterGlobalDoConsole( AdapterGlobalStatusStruct_t *Status );

//������� ������� ������������ ������� ����������� �����
//����:
// Status - ��������� �� ���������� ��������� � ������� ���
// Count - ���������� ��������� ������� ���������
void AdapterGlobalWialTime( AdapterGlobalStatusStruct_t *Status,
                            unsigned int Count );

//������� ���������� ������ �� ����������
//����:
//Message - ��������� �� ������
void AdapterGlobalError( const char *Message );

//������� ������� ���������� ��������� �� ���������� ���������
AdapterGlobalStatusStruct_t	*AdapterGlobalGetStatus( void );

//������� �������� �������� � ����� �������
//�������� �� ���������
void AdapterGlobalSetRescueMode( void );

//������� ��������� �������
//����:
// dev - ������ ��������� ���������� ������ � �����������
// Status - ��������� �� ��������� ������� ����������
//�������:
// 0 - ��� ������� ����������
// ����� 0 � ������ ������
int AdapterGlobalDoEvent( AdapterGlobalStatusStruct_t *Status );

//������� ��������� ������� ������ �� ������� �� ����������
//����:
// Status - ��������� �� ��������� ������� ����������
//�������:
// 0 - ��� ������� ����������
// ����� 0 � ������ ������
int AdapterGlobalPowerUpEvent( AdapterGlobalStatusStruct_t *Status );

int AdapterGlobalSensorWork( AdapterGlobalStatusStruct_t *Status );

/**
 * @brief �������, ������� ���������� ������ ������� � ������ � �������� ��������� type � 
 * @brief value (���� ��� value ������) � ������� value2 != 1
 *
 * @param Events - ��������� �� ���������� ��������� �������
 * @param type - ��� ������� ����� �������� �������
 * @param value - �������� �������, ������� ������, ����� ���� 0 � ���� ������ �� ������������ 
 * @return DispatcherLibEvent_t* - ��������� �� ��������� �������, NULL ���� �� ������� ��� ������
 **/
DispatcherLibEvent_t* AdapterGlobalGetFirstEventbyType( DispatcherLibEventStatus_t *Events, int type, int value );

/**
 * @brief �������, ������� ���� ������ �� ������������ ������� �����������
 *
 * @param Events - ��������� �� ��������� �������
 * @return DispatcherLibEvent_t*
 */
DispatcherLibEvent_t *AdapterGlobalGetFirstPowerUpEvent( DispatcherLibEventStatus_t *Events );

int AdapterGlobalEventCheckOnSleep( AdapterGlobalStatusStruct_t *Status );

int AdapterGlobalSensorGetState( AdapterGlobalStatusStruct_t* Status, int sensnum );

int AdapterGlobalOutputSensorPowerWork( AdapterGlobalStatusStruct_t *Status );

int AdapterGlobalSensorIsActive( AdapterGlobalStatusStruct_t* Status );

int AdapterGlobalWileSetHendel ( int dev, fd_set *rfds, int maxsel );

/**
 * @brief ������� ������ � SMS ����������� �������� �� �������, ��������� ���������� � ��
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @param iResive - ���� ����, ��� ���� ���������� �������� SMS
 * @param TCount - ������� ������� ��������� �����
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterGlobalSMS( AdapterGlobalStatusStruct_t *Status, int iResive, unsigned int TCount );

/**
 * @brief ������� ������� ������ �������� �� ���������� �������
 *
 * @param  ...
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterGlobalCheckStart( void );

/**
 * @brief ������� ������� ��������� ������� � ����� Type � ������ � ������ ��� ��� ������� � ���������� Message
 * @brief ��� ���������� ������ ���������� ����� ������ � ���������� ������������� �������� �������� ������ � �������� 0x0D 0x0A
 *
 * @param Status - ��������� �� ���������� ��������� ������� ��������
 * @param Message - ��������� �� ������ � ����������
 * @param Type - ��� ������������ �������
 * @param Value1 - ������ �������� ��� �������
 * @param Value2 - ������ �������� ��� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������.
 **/
int AdapterGlobalEventAddAndLog( AdapterGlobalStatusStruct_t *Status,  const char *Message, uint32_t	Type, int Value1, int Value2 );

/**
 * @brief �������, ������ ��������� ������� ����������� � ���� � ������ �� ���� � ������
 * ��� ���������� ������ ���������� ����� ������ � ���������� ������������� �������� �������� ������ � �������� 0x0D 0x0A
 *
 * @param Status - ��������� �� ���������� ��������� ������� ��������
 * @param Message - ��������� �� ������ � ����������
 * @param IP - IP ����� ����� ������� ��������� �����������
 * @param Level - ������� ������� ��� �����������
 * @param Band - ��� ���������� ������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 */
static inline int AdapterGlobalEventAddAndLogConnect( AdapterGlobalStatusStruct_t *Status,
													  const char *Message,
													  uint32_t IP, int16_t Level, uint16_t Band )
{
	uint32_t Value2 = ((uint32_t)Band) << 16 | (uint16_t)Level;
	return AdapterGlobalEventAddAndLog( Status, Message, DISPATCHER_LIB_EVENT__CONNECT, IP, Value2 );
}

#endif //R2_ADAPTER_GLOBAL_H


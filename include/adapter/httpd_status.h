//�������� ��� ��������� ������� HTTPD ������� ����� �� ��������

//����: ��������� ���� ��� "����"
//�����: i.podkolzin@ross-jsc.ru
//������: 2010.09.15 16:46

#ifndef ADAPTER_HTTPD_STATUS_H
#define ADAPTER_HTTPD_STATUS_H

#include <axis_ssl_httpi_server.h>


#define ADAPTER_HTTP__HTTPS_PAGE_COUNT			24
#define ADAPTER_HTTP__HTTP_PAGE_COUNT			24 //==equ https  //16

//��������� � ������� �� ����� ������� ��� ���������� � �������
typedef struct AdapterHTTPDStatusStruct_s 
{

	axis_ssl_httpi_server_client	Server; //��� ��� ��������� � ������� �� ����������
	
	uint32_t						LastTCount; //��������� ������� �����
	
	axis_ssl_httpi_control			HTTPControl;	//��� ������� �� HTTP
	axis_ssl_httpi_control			HTTPSControl;	//��� ������� �� HTTPS
	
	axis_ssl_httpi_page_control		HTTPPages[	ADAPTER_HTTP__HTTP_PAGE_COUNT ];
													//���� �������� HTTP
	axis_ssl_httpi_page_control		HTTPSPages[ ADAPTER_HTTP__HTTPS_PAGE_COUNT]; 
													//���� �������� HTTPS


} AdapterHTTPDStatusStruct_t;

#endif //ADAPTER_HTTPD_STATUS_H

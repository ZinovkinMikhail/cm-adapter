#ifndef ADAPTER_ACC_H
#define ADAPTER_ACC_H

#include <adapter/global.h>

/**
 * @brief �������, ������� �������� ��������� ��� ��� �� �� �� ���������
 * @brief � �������� ���������� ��������� � ��������� Status->AccStatus
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterAccGetStatus( AdapterGlobalStatusStruct_t *Status );

int AdapterAccControlModems( AdapterGlobalStatusStruct_t *Status );
#endif //ADAPTER_ACC_H


#ifndef CM_ADAPTER_USB_H
#define CM_ADAPTER_USB_H

#include <axis_dmlibs_resource.h>
#include <adapter/global.h>

#define DEVICE_MAX_NUM	10
#define DEVICE_MAX_NAME	32
int AdapterUsbSwithInternal( dmdev_t *dev );
int AdapterUsbSwithExternal( dmdev_t *dev );
int AdapterUsbInit( dmdev_t *dev );

/**
 * @brief ���������� ����� USB ����� �� ������� ��������� �������
 *
 * @param drvname - ��������� �� ������ � ������ �����
 * @return int - ����� ����� �� ������� ������ �������, ����� 0 � ������ ������
 **/
int AdapterUsbPortNumber( const char *drvname );

int AdapterUsbInitPolling( AxisUsbDesc_t *dev );
int AdapterUsbDeinitPolling( AxisUsbDesc_t *dev );
int AdapterUsbResetPoll( libnetcontrol_t *Net );

/**
 * @brief �������, ������� ������������� USB �������������
 *
 * @param dev - ��������� �� ��������� ���������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterUsbReSwitch( dmdev_t *dev );

/**
 * @brief ������� ������������ �������, ����� �� ������ USB ������ �� ���� ������� � ������� ������������� �������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @param Dev  - ��������� �� ��������� �������� ���������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterUSBNoModemsAtMoreTime( AdapterGlobalStatusStruct_t *Status );

/**
 * @brief ������� ������������ �������, ����� �� ������ USB ������ �� ���� ������� - � ��� ����������
 *
 * @param Status - ��������� �� ���������� ��������� �������
 * @param Dev  - ��������� �� ��������� �������� ���������
 * @return int - 0 ��� ��, ����� 0 � ������ ������
 **/
int AdapterUSBNoModemsFatal( AdapterGlobalStatusStruct_t *Status );

#endif //CM_ADAPTER_USB_H

